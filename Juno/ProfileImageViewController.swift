//
//  ProfileImageViewController.swift
//  Juno
//
//  Created by Asaad on 2/7/18.
//  Copyright © 2018 Animata Inc. All rights reserved.

import UIKit
import Vision
import CoreImage
import Firebase
import FirebaseAuth

class ProfileImageViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    let notification = UINotificationFeedbackGenerator()
    
    var ref_AuthTracker = AuthTracker()
    
    //MARK: Firebase Setup
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    
    //MARK: Global Variables
    var clearProfilePhoto: UIImage?
    
    var unblurredCIImage: CIImage?
    var blurredImageLight: CIImage?
    var blurredImageMedium: CIImage?
    var blurredImageHeavy: CIImage?
    var uiCroppedBlurredImage: UIImage?
    
    var profileImageisVisible = true
    var photoToUpload: UIImage?
    var user: User?
    var selfUID: String?
    var userName: String?
    var userAge: String?
    
    var userFirstName: String?
    var userInitial: String?
    
    //trial
    var uiCreatedImage: UIImage?
    
    //user preferences
    var isHiddenPreference = true
    
    @IBOutlet weak var lineOneLabel: UILabel!
//    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var secondLineLabel: UILabel!
//    @IBOutlet weak var noFaceView: UIImageView!
    
    override func loadView() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        mainView.backgroundColor = UIColor.white
        self.view = mainView
        
        //add views
        self.addScreenTitleLabel()
        self.addChoiceCopy()
        self.addPhotoSecurityButton()
        self.addProfileImageView()
//        self.addTransparencyView()
        self.addNewImageFaceView()
        self.addNoFaceImageView()
        
        self.addProfileDescriptorLabel()
        self.addDidNotDetectFaceTextView()
        self.addProfileToggler()
        self.addVisibilityLabel()
        self.addPulsatingLayer()
        
        //
        self.addBlurSelectorLineSeparator()
        self.addThumbnailViews()
        self.addImageChoiceLabel()
        
        //
//        self.addUserNameLabel()
//        self.addShowHideNameButton()
        
        //privacy title is set in addScreenTitle()
        //name privacy copy is set in addChoiceOpy()
        //change name settings is set in addChoiceCopy()
    }
    
    var screenTitleLabel: UILabel?
    var choiceCopyTextView: UITextView?
    var profileImageView: UIImageView?
    var profileDescriptorLabel: UILabel?
    var imageErrorTextView: UITextView?
    var profileTogglerView: UIImageView?
    var visibilityLabel: UILabel?
    var noFaceImageView: UIImageView?
    var newPhotoImageView: UIImageView?

    //
    var privacyScreenTitle: UILabel?
    var nameChoiceTextView: UITextView?
    var nameLabel: UILabel?
    var showHideButton: UIButton?
    var changeCopy: UITextView?
    
    var translateX: CGFloat!
    
    func addScreenTitleLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        self.translateX = sWidth
        
        //header frame properties
        let lW = sWidth
        let lH = 22 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let screenTitleLabel = UILabel(frame: tF)
        
        screenTitleLabel.text = "Add a Clear Profile Image"
        screenTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        screenTitleLabel.textColor = UIColor.black
        screenTitleLabel.textAlignment = .center
        
        //init Label
        self.view.addSubview(screenTitleLabel)
        self.screenTitleLabel = screenTitleLabel
        
        //
        let privacyLabelFrame = CGRect(x: lX + translateX, y: lY, width: lW, height: lH)
        let privacyLabel = UILabel(frame: privacyLabelFrame)
        
        privacyLabel.text = "Privacy"
        privacyLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        privacyLabel.textColor = UIColor.black
        privacyLabel.textAlignment = .center
        privacyLabel.backgroundColor = UIColor.clear
        
        self.view.addSubview(privacyLabel)
        self.privacyScreenTitle = privacyLabel
    }
    
    func addChoiceCopy() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 14.0 as CGFloat
        var tWidth = widthRatio * sWidth
//        let choiceCopy = "You can choose to make your Profile Picture public or private below"
//        let choiceCopy = "You can choose to SHOW or HIDE your profile after adding a photo below"
//        let choiceCopy = "You can make your profile PUBLIC or PRIVATE after adding a photo below"
        let choiceCopy = "You can make your profile public or private after adding a photo below"

        var textFieldCGSize = self.sizeOfCopyLarge(string: choiceCopy, constrainedToWidth: Double(tWidth))
        if sWidth > 350 {
//            tWidth = 330 as CGFloat
            tFontSize = 14.0 as CGFloat
        } else {
//            tWidth = 278 as CGFloat
            tFontSize = 12.0 as CGFloat
            textFieldCGSize = self.sizeOfCopySmall(string: choiceCopy, constrainedToWidth: Double(tWidth))
        }
        
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
//        let tY = superInsetHeight +  (1.5 * gridUnitHeight)
        let marginSpace = 2 as CGFloat
        let tY = self.screenTitleLabel!.frame.maxY + marginSpace

        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let choiceCopyTextView = UITextView(frame: tFrame)
        choiceCopyTextView.text = choiceCopy
        choiceCopyTextView.font = UIFont(name: "Avenir-Light", size: tFontSize)

        //
        self.view.addSubview(choiceCopyTextView)
        self.choiceCopyTextView = choiceCopyTextView

        self.choiceCopyTextView = choiceCopyTextView
        self.choiceCopyTextView!.textAlignment = .center
        self.choiceCopyTextView!.textAlignment = .center
        self.choiceCopyTextView!.isEditable = false
        self.choiceCopyTextView!.isScrollEnabled = false
        self.choiceCopyTextView!.isSelectable = false
        self.choiceCopyTextView!.textContainer.lineFragmentPadding = 0
        self.choiceCopyTextView!.textContainerInset = .zero
        self.choiceCopyTextView!.textColor = UIColor.black
        self.choiceCopyTextView!.backgroundColor = UIColor.clear
        
        
        //init copy2
        //
//        let nameCopy = "Would You Like Your Name to Be Visible on Your Profile?"
        let nameCopy = "Would You Like to Keep Your Name Visible on Your Profile?"
        let nameChoiceLabel = CGRect(x: tX + translateX, y: tY, width: tWidth, height: tH)
        let nameChoiceTextView = UITextView(frame: nameChoiceLabel)
        nameChoiceTextView.text = nameCopy
        nameChoiceTextView.font = UIFont(name: "Avenir-Light", size: 12)
        
        //
        self.view.addSubview(nameChoiceTextView)
        self.nameChoiceTextView = nameChoiceTextView
        
        //
        self.nameChoiceTextView = nameChoiceTextView
        self.nameChoiceTextView!.textAlignment = .center
        self.nameChoiceTextView!.textAlignment = .center
        self.nameChoiceTextView!.isEditable = false
        self.nameChoiceTextView!.isScrollEnabled = false
        self.nameChoiceTextView!.isSelectable = false
        self.nameChoiceTextView!.textContainer.lineFragmentPadding = 0
        self.nameChoiceTextView!.textContainerInset = .zero
        self.nameChoiceTextView!.textColor = UIColor.black
//        self.nameChoiceTextView!.backgroundColor = UIColor.red
        
        //init copy3
        //
        let changeString = "You can always change this later in Profile Editor"
        let changeLabel = CGRect(x: tX + translateX, y: (5 * superInsetHeight) + (2 * gridUnitHeight), width: tWidth, height: tH)
        let changeCopyTextView = UITextView(frame: changeLabel)
        changeCopyTextView.text = changeString
        changeCopyTextView.font = UIFont(name: "Avenir-Light", size: 12)
        
        //
        self.view.addSubview(changeCopyTextView)
        self.changeCopy = changeCopyTextView
        
        //
        self.changeCopy = changeCopyTextView
        self.changeCopy!.textAlignment = .center
        self.changeCopy!.textAlignment = .center
        self.changeCopy!.isEditable = false
        self.changeCopy!.isScrollEnabled = false
        self.changeCopy!.isSelectable = false
        self.changeCopy!.textContainer.lineFragmentPadding = 0
        self.changeCopy!.textContainerInset = .zero
        self.changeCopy!.textColor = UIColor.lightGray
//        self.changeCopy!.backgroundColor = UIColor.red
    }
    
    func addUserNameLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 35 as CGFloat
        var lX = 0 as CGFloat
        let lY = self.profileImageView!.frame.maxY + gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX + translateX, y: lY, width: lW, height: lH)
        let nameLabel = UILabel(frame: tF)
        
        let userFirstName = self.userFirstName!
        let userAge = self.userAge!
        nameLabel.text = userFirstName + ", " + userAge
        nameLabel.font = UIFont(name: "AvenirNext-Regular", size: 22)
        nameLabel.textColor = UIColor.black
        nameLabel.textAlignment = .center
//        nameLabel.backgroundColor = UIColor.red
        
        //init Label
        self.view.addSubview(nameLabel)
        self.nameLabel = nameLabel
    }
    
    func addShowHideNameButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let buttonWidth = 50 as CGFloat
        let bH = 20 as CGFloat
        let bX = (UIScreen.main.bounds.width - buttonWidth) * 0.5
        let bY = self.nameLabel!.frame.maxY + gridUnitHeight
        
        let bF = CGRect(x: bX + translateX, y: bY, width: buttonWidth, height: bH)
        let bView = UIButton(frame: bF)
        
        bView.setTitle("Hide", for: .normal)
        bView.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)!
        bView.setTitleColor(hyperBlue, for: .normal)
        
        self.view.addSubview(bView)
        self.showHideButton = bView
        
        showHideButton!.addTarget(self, action: #selector(ProfileImageViewController.showHideName(_:)), for: .touchUpInside)
        showHideButton!.isUserInteractionEnabled = true
        
        //adjust frame of choiceLabel
        self.changeCopy!.frame.origin.y = self.showHideButton!.frame.maxY
    }

    
    var nameIsHidden = false
    let hyperBlue = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
    
    @objc func showHideName(_ sender: UIButton) {
        //print("showHideName detected")
        
        if self.nameIsHidden == false {
            self.nameLabel!.text = self.userInitial! + ", " + self.userAge!
            self.nameIsHidden = true
            showHideButton!.setTitle("Show", for: .normal)
            
        } else {
            self.nameLabel!.text = self.userFirstName! + ", " + self.userAge!
            self.nameIsHidden = false
            showHideButton!.setTitle("Hide", for: .normal)
        }
    }
    
    func sizeOfCopyLarge(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 14.0)! ],
            context: nil).size
    }
    
    func sizeOfCopySmall(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 12.0)! ],
            context: nil).size
    }
    
    var verticalDisplacement: CGFloat?
    let softSpaceGray = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    
    func addProfileImageView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = 5 * gridUnitWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = 2 * gridUnitWidth
        var imageView_Y = (2 * superInsetHeight) +  (3 * gridUnitHeight)
        
        let verticalDisplacement = 2 * gridUnitHeight
        self.verticalDisplacement = verticalDisplacement
        imageView_Y = (2 * superInsetHeight) +  (3 * gridUnitHeight) - verticalDisplacement
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = self.softSpaceGray
        profileImageView.layer.cornerRadius = 15.0 as CGFloat
        profileImageView.layer.masksToBounds = true
        
        self.profileImageView = profileImageView
        self.view.addSubview(profileImageView)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.myviewTapped(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.profileImageView!.addGestureRecognizer(tapGesture)
        self.profileImageView!.isUserInteractionEnabled = true
    }
    
    var transparencyView: UIImageView?
    
    func addTransparencyView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = 5 * gridUnitWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = 2 * gridUnitWidth
        let imageView_Y = (2 * superInsetHeight) +  (3 * gridUnitHeight)
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let transparencyView = UIImageView(frame: imageViewFrame)
        
        transparencyView.contentMode = .scaleAspectFill
        transparencyView.clipsToBounds = true
        transparencyView.backgroundColor = UIColor.black
        transparencyView.layer.cornerRadius = 15.0 as CGFloat
        transparencyView.layer.masksToBounds = true
        transparencyView.alpha = 0.2

        self.transparencyView = profileImageView
        self.view.addSubview(transparencyView)
    }
    
    
    func addNewImageFaceView() {
        //
        let widthRatio = 78 / 414 as CGFloat
        let h2W = 74 / 78 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = widthRatio * sWidth
        let imageViewHeight = h2W * imageViewWidth
        let imageView_X = (self.profileImageView!.bounds.height - imageViewHeight) * 0.5
        let imageView_Y = imageView_X
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let newPhotoImageView = UIImageView(frame: imageViewFrame)
        
        newPhotoImageView.contentMode = .scaleAspectFill
        newPhotoImageView.backgroundColor = UIColor.clear
        newPhotoImageView.image = UIImage(named: "addPhotoFace")
        //
        
        self.newPhotoImageView = newPhotoImageView
        self.profileImageView!.addSubview(newPhotoImageView)
    }
    
    func addNoFaceImageView() {
        //
        let widthRatio = 100 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = widthRatio * sWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (self.profileImageView!.bounds.height - imageViewHeight) * 0.5
        let imageView_Y = imageView_X
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let noFaceView = UIImageView(frame: imageViewFrame)
        
        noFaceView.contentMode = .scaleAspectFill
        noFaceView.backgroundColor = UIColor.clear
        noFaceView.image = UIImage(named: "noFace")
        //
        
        self.noFaceImageView = noFaceView
        self.profileImageView!.addSubview(noFaceView)
    }
    
    func addProfileToggler() {
        //
        let widthRatio = 70 / 414 as CGFloat
        let heightRatio = 60 / 70 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = widthRatio * sWidth
        let imageViewHeight = heightRatio * imageViewWidth

        let imageView_X = (sWidth - imageViewWidth) * 0.5
        var imageView_Y = self.profileImageView!.frame.maxY +  (3 * gridUnitHeight)
        imageView_Y = self.profileImageView!.frame.maxY +  (1.5 * gridUnitHeight)
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileTogglerView = UIImageView(frame: imageViewFrame)
        profileTogglerView.image = UIImage(named: "publicProfile")
        
        profileTogglerView.contentMode = .scaleAspectFit
        profileTogglerView.backgroundColor = UIColor.clear
        self.profileTogglerView = profileTogglerView
        self.view.addSubview(profileTogglerView)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.toggleProfileVisibility(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.profileTogglerView!.addGestureRecognizer(tapGesture)
        self.profileTogglerView!.isUserInteractionEnabled = false
    }
    
    func addProfileDescriptorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 22 as CGFloat
        let lX = 0 as CGFloat
        let lY = self.profileImageView!.frame.maxY + (0.2 * gridUnitHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let dLabel = UILabel(frame: tF)
        
//        dLabel.text = "This is how your image will appear on your profile"
//        dLabel.text = "Choose how your image will appear on your profile"
        dLabel.text = "Please make sure your face is clearly visible"
        dLabel.textColor = UIColor.black
        dLabel.textAlignment = .center
        
        //init
        self.view.addSubview(dLabel)
        self.profileDescriptorLabel = dLabel
//        self.profileDescriptorLabel!.font = UIFont(name: "Avenir-Light", size: 12.0)!
        self.profileDescriptorLabel!.font = UIFont(name: "Avenir-Medium", size: 12.0)!
        self.profileDescriptorLabel!.textColor = UIColor(displayP3Red: 190/255, green: 190/255, blue: 190/255, alpha: 1)
    }
    
    func addVisibilityLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = self.profileTogglerView!.bounds.width
        let lH = 20 as CGFloat
        let lX = (sWidth - lW) * 0.5
        var lY = self.profileTogglerView!.frame.maxY + (0 * gridUnitHeight)
        
//        let verticalDisplacement = 2 * gridUnitHeight
//        lY = self.profileTogglerView!.frame.maxY + (0 * gridUnitHeight) - verticalDisplacement
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let visibilityLabel = UILabel(frame: tF)
        
        visibilityLabel.text = self.currentProfileSetting
        visibilityLabel.textColor = UIColor.black
        visibilityLabel.textAlignment = .center
        
        //init
        self.view.addSubview(visibilityLabel)
        self.visibilityLabel = visibilityLabel
        self.visibilityLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)!
        self.visibilityLabel!.adjustsFontSizeToFitWidth = true
    }
 
    var securityTextView: UITextView?
    
    func addPhotoSecurityButton() {
        
        //
        let sWidth = UIScreen.main.bounds.width
        
        var tFontSize = 12.0 as CGFloat
//        let buttonCopy = "How Are My Private Photos Secured?"
        let buttonCopy = "How Are Private Profiles Protected?"
        let tWidth = self.choiceCopyTextView!.frame.width
        var textFieldCGSize = self.sizeOfCopyLarge(string: buttonCopy, constrainedToWidth: Double(tWidth))
        if sWidth > 350 {
            //            tWidth = 330 as CGFloat
            tFontSize = 12.0 as CGFloat
        } else {
            //            tWidth = 278 as CGFloat
            tFontSize = 10.0 as CGFloat
            textFieldCGSize = self.sizeOfCopySmall(string: buttonCopy, constrainedToWidth: Double(tWidth))
        }
        let tH = ceil(textFieldCGSize.height)
        let tX = self.choiceCopyTextView!.frame.minX
        let marginSpace = 2 as CGFloat
        let tY = self.choiceCopyTextView!.frame.maxY + marginSpace
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let securityTextView = UITextView(frame: tFrame)
        securityTextView.text = buttonCopy
        securityTextView.font = UIFont(name: "Avenir-Medium", size: tFontSize)
        
        //
        self.view.addSubview(securityTextView)
        self.securityTextView = securityTextView
        
        self.securityTextView!.textAlignment = .center
        self.securityTextView!.textAlignment = .center
        self.securityTextView!.isEditable = false
        self.securityTextView!.isScrollEnabled = false
        self.securityTextView!.isSelectable = false
        self.securityTextView!.textContainer.lineFragmentPadding = 0
        self.securityTextView!.textContainerInset = .zero
        self.securityTextView!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.securityTextView!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.photoSecurityInfo(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.securityTextView!.addGestureRecognizer(tapGesture)
        self.securityTextView!.isUserInteractionEnabled = true
    }
    
    @objc func photoSecurityInfo(_ sender: UITapGestureRecognizer) {
        //print("photoSecurityInfo")
        
        self.performSegue(withIdentifier: "toTrustInfoVC", sender: self)
        
        if self.copyIsSystemError == true {
           self.hidePhotoUploadErrorLabel()
        }
    }
    
    func hidePhotoUploadErrorLabel() {
        UIView.transition(with: self.profileDescriptorLabel!, duration: 0.1, options: .transitionCrossDissolve, animations: {
            self.profileDescriptorLabel?.alpha = 0
        }) { (true) in
            self.profileDescriptorLabel?.isHidden = true
            self.profileDescriptorLabel?.alpha = 1
        }
    }
    
    @IBAction func prepareForUnwind2 (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toTrustInfoVC" {
            let controller = segue.destination as! TrustInfoViewController
            controller.sourceController = "profileImageVC"
        }
        
        if segue.identifier == "toServices" {
            
            let controller = segue.destination as! LocationGetterViewController
            controller.ref_AuthTracker = self.ref_AuthTracker
            controller.userName = self.userName!
        }
    }

    func addDidNotDetectFaceTextView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 12.0 as CGFloat
        var tWidth = 270 as CGFloat
//        let errorCopy = "Unable to clearly detect your face in this picture. Please try again using another image."
        let errorCopy = "Failed to detect your face in this photo clearly. Please try again using another image."

        var textFieldCGSize = self.sizeOfCopyLarge(string: errorCopy, constrainedToWidth: Double(tWidth))
        if sWidth > 350 as CGFloat {
            //print("LARGESCREEN")
            //
        } else {
            //print("SMALLSCREEN")
            tWidth = 225 as CGFloat
            tFontSize = 10.0 as CGFloat
            textFieldCGSize = self.sizeOfCopySmall(string: errorCopy, constrainedToWidth: Double(tWidth))
        }
        
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let baseLineY = 10 as CGFloat
        let tY = self.profileImageView!.frame.maxY + (gridUnitHeight * 0.5)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let imageErrorTextView = UITextView(frame: tFrame)
        
        imageErrorTextView.textColor = UIColor.red
        imageErrorTextView.text = errorCopy
        //
        self.view.addSubview(imageErrorTextView)
        
        self.imageErrorTextView = imageErrorTextView
        self.imageErrorTextView!.textAlignment = .center
        self.imageErrorTextView!.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)
        self.imageErrorTextView!.isEditable = false
        self.imageErrorTextView!.isScrollEnabled = false
        self.imageErrorTextView!.isSelectable = false
        self.imageErrorTextView!.textContainer.lineFragmentPadding = 0
        self.imageErrorTextView!.textContainerInset = .zero
        self.imageErrorTextView!.textColor = UIColor.red
        self.imageErrorTextView!.backgroundColor = UIColor.clear
        
        self.imageErrorTextView = imageErrorTextView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Configure DB and Storage
        storageRef = Storage.storage().reference()
        ref = Database.database().reference()
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.myviewTapped(_:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        profileImageView.addGestureRecognizer(tapGesture)
//        profileImageView.isUserInteractionEnabled = true
//
//        profileImageView.layer.cornerRadius = 15.0
//        profileImageView.layer.masksToBounds = true;
//        secondLineLabel.isHidden = true
//        self.noFaceView.isHidden = true
//
        setNeedsStatusBarAppearanceUpdate()
        
        //Reactivate
        self.selfUID = Auth.auth().currentUser!.uid
        
        self.profileDescriptorLabel!.isHidden = false
        self.imageErrorTextView!.isHidden = true
        self.pulsatingLayer?.isHidden = true
        self.noFaceImageView?.isHidden = true
        
        self.visibilityLabel?.textColor = UIColor.lightGray
        self.profileTogglerView?.image = UIImage(named: "visibilityControl_N")
        self.transparencyView?.isHidden = true
        
//        self.lineSeparatorView?.isHidden = true
//        self.blurChoiceLabel?.isHidden = true
//        self.lightThumbView1?.isHidden = true
//        self.mediumThumbView2?.isHidden = true
//        self.heavyThumbView3?.isHidden = true
        
        self.addActivityIndicator()
    }
    
    var didCallAuthTrackerObserver = false
    
    //ATTN: Appears as though loadView() will instantiate newVC && SB instance cause viewWillAppear to be called twice?
    override func viewWillAppear(_ animated: Bool) {
        self.privacyScreenTitle?.isHidden = true
        self.nameChoiceTextView?.isHidden = true
        self.nameLabel?.isHidden = true
        self.showHideButton?.isHidden = true
        self.changeCopy?.isHidden = true
        
        //Observe authTrackerState
        if self.didCallAuthTrackerObserver == false {
            NotificationCenter.default.addObserver(self, selector: #selector(ProfileImageViewController.batchUpdatesComplete), name: NSNotification.Name(rawValue: "authTrackerComplete"), object: nil)
            self.didCallAuthTrackerObserver = true
        }
    }

    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    var currentProfileSetting = "Public Profile"
    
//    self.blurredImageLight = ciImageObject?.applyingGaussianBlur(sigma: lightBlurSigma)
//    self.blurredImageMedium = ciImageObject?.applyingGaussianBlur(sigma: mediumBlurSigma)
//    self.blurredImageHeavy = ciImageObject?.applyingGaussianBlur(sigma: heavyBlurSigma)
//
//    //create and init UIBlurredImages
//    let blurredCICroppedImage_Light = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
//    self.uiLightImage = UIImage(ciImage: blurredCICroppedImage_Light)
//    let blurredCICroppedImage_Medium = self.blurredImageMedium!.cropped(to: (self.unblurredCIImage!.extent))
//    self.uiMediumImage = UIImage(ciImage: blurredCICroppedImage_Medium)
//    let blurredCICroppedImage_Heavy = self.blurredImageHeavy!.cropped(to: (self.unblurredCIImage!.extent))
//    self.uiHeavyImage = UIImage(ciImage: blurredCICroppedImage_Heavy)
    
    
    @objc func toggleProfileVisibility(_ sender: UITapGestureRecognizer) {
        //print("toggleProfileVisibility")
        
        if self.currentProfileSetting == "Public Profile" {
            
            self.showHideBlurBox(withState: "rising")
            self.animateTogglerView(to: "Left")
            self.currentProfileSetting = "Private Profile"
            self.visibilityLabel!.text = "Private Profile"
            self.profileTogglerView!.image = UIImage(named: "privateProfile")
            
            let ciCroppedImage = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
            self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
            let lightP = uiCroppedBlurredImage

            UIView.transition(with: self.profileImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.profileImageView!.image = lightP
            }, completion: nil)
            
            self.updateImageToUpload(withBlurSigma: lightBlurSigma)
    
        } else {
            
            self.showHideBlurBox(withState: "resting")
            self.animateTogglerView(to: "Right")
            self.currentProfileSetting = "Public Profile"
            self.visibilityLabel!.text = "Public Profile"
            self.profileTogglerView!.image = UIImage(named: "publicProfile")
            
            UIView.transition(with: self.profileImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.profileImageView!.image = self.clearProfilePhoto
            }, completion: nil)
            
            self.photoToUpload = self.clearProfilePhoto!
        }
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
    }
    
    func updateImageToUpload(withBlurSigma: Double) {
        self.contextCreateImage(withBlurSigma: withBlurSigma)
        self.profileImageisVisible = false
    }

    func animateTogglerView(to: String) {
        let buttonView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        var horizontalDisplacement = -gridUnitWidth as CGFloat
        if to == "Left" {
            horizontalDisplacement = -gridUnitWidth
        } else if to == "Right" {
            self.profileImageisVisible = true
            self.pulsatingLayer?.isHidden = true
            self.pulsatingLayer?.removeAllAnimations()
            horizontalDisplacement = 0
        }
        
        let bW = buttonView.bounds.width
        let bH = buttonView.bounds.height
        let bX = ((sWidth - bW) * 0.5) + horizontalDisplacement
        var bY = (self.profileImageView!.frame.maxY +  (3 * gridUnitHeight))
        
        bY = (self.profileImageView!.frame.maxY +  (1.5 * gridUnitHeight))
        
        let lW = self.profileTogglerView!.bounds.width
        let lH = 20 as CGFloat
        let lX = ((sWidth - lW) * 0.5) + horizontalDisplacement
        let lY = self.profileTogglerView!.frame.maxY + (0 * gridUnitHeight)
    
        //
        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            self.bView1?.isHidden = true
            buttonView.frame = CGRect(x: bX, y: bY, width: bW, height: bH)
            vLabel.frame = CGRect(x: lX, y: lY, width: lW, height: lH)
        }) { (true) in
            if to == "Left" {
                self.addBlurControlView()
                self.animatePulsatingLayer_N()
            } else if to == "Right" {
                self.bView1!.removeFromSuperview()
            }
        }
    }
    
    var bView1: UIImageView?

    func addBlurControlView() {
        let rRatio = 46.5 / 414 as CGFloat
        let togglerView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = rRatio * sWidth
        let bH = bW
        let finalX_togglerView = ((sWidth - togglerView.bounds.width) * 0.5) - gridUnitWidth
        let bX1 = finalX_togglerView + togglerView.bounds.width + (gridUnitWidth * 0.5)
//        let bY = togglerView.frame.minY + (togglerView.bounds.height - bH)
        let bY = togglerView.frame.maxY - bH

        //
        let bFrame1 = CGRect(x: bX1, y: bY, width: bW, height: bH)
        let bView1 = UIImageView(frame: bFrame1)
        bView1.contentMode = .scaleAspectFit
        bView1.image = UIImage(named: "blurLight")
        bView1.backgroundColor = UIColor.clear
        self.bView1 = bView1
 
        //final frames
        self.view.addSubview(bView1)

        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.switchBlurStates(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.bView1!.addGestureRecognizer(tapGesture)
        self.bView1!.isUserInteractionEnabled = true
    }
    
    var blurState = "lightBlur"
//    blurState = ["lightBlur", "mediumBlur", "heavyBlur"]
//    buttons: blurMedium blurHeavy blurLight
    
    @objc func switchBlurStates(_ sender: UITapGestureRecognizer) {
      //print("switchBlurStates")
        if self.blurState == "lightBlur" {
            self.bView1!.image = UIImage(named: "blurMedium")
            self.blurState = "mediumBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageMedium)
            self.updateImageToUpload(withBlurSigma: mediumBlurSigma)
            
        } else if self.blurState == "mediumBlur" {
            self.bView1!.image = UIImage(named: "blurHeavy")
            self.blurState = "heavyBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageHeavy)
            self.updateImageToUpload(withBlurSigma: heavyBlurSigma)

        } else if self.blurState == "heavyBlur" {
            self.bView1!.image = UIImage(named: "blurLight")
            self.blurState = "lightBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageLight)
            self.updateImageToUpload(withBlurSigma: lightBlurSigma)
        }
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
    }
    
    func setProfileImageView(withBlurImage: CIImage?) {
        let ciCroppedImage = withBlurImage!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
        let toImage = uiCroppedBlurredImage
        
        UIView.transition(with: self.profileImageView!, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.profileImageView!.image = toImage
        }, completion: nil)
    }
    
    var doneButtonView: UIImageView?
    var forwardArrowView: UIButton?
    
    func addForwardArrow() {
        //
        let fW = 32 as CGFloat
        let fH = fW as CGFloat
        let fX = 0 as CGFloat
        let fY = 0 as CGFloat
        
        let fF = CGRect(x: fX, y: fY, width: fW, height: fH)
        let fView = UIButton(frame: fF)
        
        fView.setImage(UIImage(named: "profileImageForwardButton")!, for: .normal)
        fView.backgroundColor = UIColor.clear
        fView.contentMode = .center
        
        fView.addTarget(self, action: #selector(ProfileImageViewController.forwardButton(_:)), for: .touchUpInside)
        fView.isUserInteractionEnabled = true

        self.view.addSubview(fView)
        self.forwardArrowView = fView
        
        let cX = (((UIScreen.main.bounds.width - self.profileImageView!.bounds.width) * 0.5 ) * 0.5) + self.profileImageView!.frame.maxX
        var centerPoint = CGPoint(x: cX, y: self.profileImageView!.center.y)
        
//        centerPoint = CGPoint(x: cX, y: (UIScreen.main.bounds.height * 0.5) - self.verticalDisplacement!)
        
        self.forwardArrowView!.layer.position = centerPoint
    }

//    if forImageSize == "gridThumb" {
//    urlNode = "gridThumb_URL"
//    storageNode = "gridThumb_Stor"
//    } else if forImageSize == "fullPhoto" {
//    urlNode = "full_URL"
//    storageNode = "userDisplayPhoto"
//    }
    
    @objc func forwardButton(_ sender: UITapGestureRecognizer) {
        
        self.showHideBlurBox(withState: "setting")
        self.profileTogglerView?.isUserInteractionEnabled = false
        self.bView1?.isUserInteractionEnabled = false
        self.securityTextView!.isUserInteractionEnabled = false
        self.forwardArrowView?.isUserInteractionEnabled = false
        self.activityIndicatorView?.startAnimating()

//        self.setDisplayProfilePhotoURL(forImageSize: "gridThumb")
        self.setDisplayProfilePhotoURL_GridThumb(forImageSize: "gridThumb")
        self.setDisplayProfilePhotoURL(forImageSize: "fullPhoto")

        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
        
//        self.setUserVisibilityPreference()

        //Note: Transfer below to photoUpdates completion block in TRUE
//        self.ref.child("users/\(Auth.auth().currentUser!.uid)/full_URL_2").setValue("")
//        self.ref.child("users/\(Auth.auth().currentUser!.uid)/full_URL_3").setValue("")
//        self.ref.child("users/\(Auth.auth().currentUser!.uid)/full_URL_4").setValue("")
        
//        if self.profileImageisVisible == true {
//            //
//        } else {
//            self.setVisibleImageURL()
//        }
//        self.performSegue(withIdentifier: "toServices", sender: self)
    }
    
    var forwardArrowSenderIsPhotoSelector = false
    var photoIsStillUploading = true
    
    /*
    @objc func forwardButton(_ sender: UITapGestureRecognizer) {
        
        let selfUID = Auth.auth().currentUser!.uid
        
        if forwardArrowSenderIsPhotoSelector == false {
            self.showHideBlurBox(withState: "setting")
            self.profileTogglerView?.isUserInteractionEnabled = false
            self.bView1?.isUserInteractionEnabled = false
            
            //begin photo upload when forward arrow is pressed
            self.setDisplayProfilePhotoURL()
            self.setUserVisibilityPreference()
            
            if self.profileImageisVisible == true {
                //hide all privacy views
                self.privacyScreenTitle!.isHidden = true
                self.nameChoiceTextView!.isHidden = true
                self.nameLabel!.isHidden = true
                self.showHideButton!.isHidden = true
                self.changeCopy!.isHidden = true
                
                self.activityIndicatorView?.startAnimating()
                self.forwardArrowView?.isUserInteractionEnabled = false
                
                self.ref.child("users/\(selfUID)/noNym").setValue("false")
                self.ref.child("users/\(selfUID)/onuma").setValue("")
                
            } else {
                //profile is not visible
                self.animateNameSelector()
//                self.setVisibleImageURL()
            }
            //        self.performSegue(withIdentifier: "toServices", sender: self)
            self.forwardArrowSenderIsPhotoSelector = true
            
        } else {
            //WHEN BUTTON IS PRESSED A SECOND TIME
            //check upload state of photo upload
            
            //send visibility info
            self.ref.child("users/\(selfUID)/userDisplayName").setValue(self.userInitial!)
            self.ref.child("users/\(selfUID)/noNym").setValue("true")
            self.ref.child("users/\(selfUID)/onuma").setValue("")
            
            if self.photoIsStillUploading == true {
                self.forwardArrowView?.isUserInteractionEnabled = false
                self.activityIndicatorView?.startAnimating()
                self.observeUploadState()
                
            } else if self.photoIsStillUploading == false {
                self.forwardArrowView?.isUserInteractionEnabled = false
                self.performSegue(withIdentifier: "toServices", sender: self)
            }
        }
    }
    */
    
    func finishUploadAndContinue() {
        
    }
    
    func observeUploadState() {
          NotificationCenter.default.addObserver(self, selector: #selector(ProfileImageViewController.observePhotoUploadComplete), name: NSNotification.Name(rawValue: "photoUploadTask"), object: nil)
    }
    
    @objc func observePhotoUploadComplete(_ notification: Notification) {
        //print("P observePhotoUploadComplete")
        guard notification.name.rawValue == "photoUploadTask" else { return }
        self.performSegue(withIdentifier: "toServices", sender: self)
    }
    
    func animateNameSelector() {
        
        let sWidth = -(UIScreen.main.bounds.width)
        
        let screenTitle = self.screenTitleLabel!
        let choiceCopy = self.choiceCopyTextView!
        let photoPrivacyButton = self.securityTextView!
        let toggler = self.profileTogglerView!
        let label = self.visibilityLabel!
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            //photo to left
            screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
            choiceCopy.transform = CGAffineTransform(translationX: sWidth, y: 0)
            photoPrivacyButton.transform = CGAffineTransform(translationX: sWidth, y: 0)
            toggler.transform = CGAffineTransform(translationX: sWidth, y: 0)
            label.transform = CGAffineTransform(translationX: sWidth, y: 0)
            
            if let blurControl = self.bView1 {
                blurControl.transform = CGAffineTransform(translationX: sWidth, y: 0)
            }
            
            //name to left
            self.privacyScreenTitle!.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.nameChoiceTextView!.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.nameLabel!.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.showHideButton!.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.changeCopy!.transform = CGAffineTransform(translationX: sWidth, y: 0)

        }, completion: nil)
    }
    
    
    //        "profileImageForwardButton"
//    func addForwardArrow() {
//        let marginInsetRatio = 19 / 414 as CGFloat
//        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
//
//        //
//        let screen = UIScreen.main.bounds
//        let sWidth = screen.width
//        let sHeight = screen.height
//        let sBarH = UIApplication.shared.statusBarFrame.size.height
//
//        //
//        let gridUnitX = 45.5 / 414 as CGFloat
//        let gridUnitY = 22.5 / 736 as CGFloat
//
//        //
//        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
//        let gridUnitWidth = gridUnitX * sWidth
//        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
//        let superInsetHeight = 4 * gridUnitHeight
//
//        //
//        let dW = 102 as CGFloat
//        let dH = 24 as CGFloat
//        let dX = (UIScreen.main.bounds.width - dW) * 0.5
//        let bottomSpace = (superInsetHeight - dH) * 0.5
//        let dY = UIScreen.main.bounds.height - bottomSpace - dH
//
//        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
//        let doneView = UIImageView(frame: dF)
//        doneView.contentMode = .center
//        doneView.image = UIImage(named: "photoDoneButton")
////        doneView.image = UIImage(named: "photoDoneButton_Inactive")
//
//        self.view.addSubview(doneView)
//        self.doneButtonView = doneView
//
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.allDone(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        self.doneButtonView!.addGestureRecognizer(tapGesture)
//        self.doneButtonView!.isUserInteractionEnabled = true
//    }
    
//    func addForwardArrow() {
//        let marginInsetRatio = 19 / 414 as CGFloat
//        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
//
//        let dW = 60 as CGFloat
//        let dH = 22 as CGFloat
//        let dX = UIScreen.main.bounds.width - dW - marginInset
//        let dY = UIScreen.main.bounds.height - dH - marginInset
//
//        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
//        let doneView = UIImageView(frame: dF)
//        doneView.contentMode = .center
//        doneView.image = UIImage(named: "allDone")
//
//        self.view.addSubview(doneView)
//        self.doneButtonView = doneView
//
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.allDone(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        self.doneButtonView!.addGestureRecognizer(tapGesture)
//        self.doneButtonView!.isUserInteractionEnabled = true
//    }
    
//    @objc func allDone(_ sender: UITapGestureRecognizer) {
//        self.setDisplayProfilePhotoURL()
//        self.setUserVisibilityPreference()
//        if self.profileImageisVisible == true {
//            //
//        } else {
////            self.setVisibleImageURL()
//        }
//        self.performSegue(withIdentifier: "toServices", sender: self)
//    }
    
    var pulsatingLayer: CAShapeLayer?
    
    func addPulsatingLayer() {
        let rRatio = 46.5 / 414 as CGFloat
        let togglerView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = rRatio * sWidth
        let bH = bW
        let finalX_togglerView = ((sWidth - togglerView.bounds.width) * 0.5) - gridUnitWidth
        let bX1 = finalX_togglerView + togglerView.bounds.width + (gridUnitWidth * 0.5)
        let bX2 = bX1 + bW + (gridUnitWidth * 0.5)
        let bX3 = bX2 + bW + (gridUnitWidth * 0.5)
        let bY = togglerView.frame.minY + (togglerView.bounds.height - bH)
        
        //
        let circularPath = UIBezierPath(arcCenter: .zero, radius: bW * 0.5, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer = CAShapeLayer()
        pulsatingLayer!.path = circularPath.cgPath
        
        pulsatingLayer!.fillColor = UIColor.clear.cgColor
        pulsatingLayer!.lineWidth = 3
        pulsatingLayer!.strokeColor = UIColor.lightGray.cgColor
        
        let cX = bX1 + (bW * 0.5)
        let cY = bY + (bW * 0.5)
        let cPoint = CGPoint(x: cX, y: cY)
        
        pulsatingLayer!.position = cPoint
        
        self.view.layer.addSublayer(pulsatingLayer!)
    }
    
    func animatePulsatingLayer_N() {
        //print("animatePulsatingLayer_N")
        //should also call animator when textFieldBeginsEditing
        
        //faster ring animation expand
        //slower ring animation contract
        //MUST add pause between flashes. It's currently too fast and tells the user "you MUST reveal your profile". It needs to be slower and more suggestive.
        
        self.pulsatingLayer!.isHidden = false
        
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.2
        pulse.duration = 0.35
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = false
        pulse.repeatCount = 3
        
        let fade = CABasicAnimation(keyPath: "opacity")
        fade.fromValue = 1
        fade.toValue = 0
        fade.duration = 0.35
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        fade.autoreverses = false
        fade.repeatCount = 3
        
        pulsatingLayer!.add(pulse, forKey: "pulse")
        pulsatingLayer!.add(fade, forKey: "fade")
        
        let when = DispatchTime.now() + (3 * 0.27)
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.pulsatingLayer!.isHidden = true
        }
    }
    
    var lineSeparatorView: UIView?
    var blurBoxHeight: CGFloat?
    
    func addBlurSelectorLineSeparator() {
        
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        var lineY = sHeight - (2 * superInsetHeight)
        self.blurBoxHeight = (2 * superInsetHeight)
        
        lineY = lineY + self.blurBoxHeight!
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        self.lineSeparatorView = lineView
        
        lineView.backgroundColor = UIColor.lightGray
        self.view.addSubview(lineView)
    }
    
    var lightThumbView1: UIImageView?
    var mediumThumbView2: UIImageView?
    var heavyThumbView3: UIImageView?

    func addThumbnailViews() {
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let totalWidth = sWidth - (2 * gridUnitWidth)
        let negSpace = gridUnitWidth

        //coordinates
        let imageWidth = (totalWidth - negSpace) / 3
        let imageH = imageWidth
        let imageX1 = gridUnitWidth
        let imageX2 = (3.5 * gridUnitWidth)
        let imageX3 = 6 * gridUnitWidth
        var imageY = (sHeight - superInsetHeight) - (imageH * 0.5)
        
        imageY = imageY + self.blurBoxHeight!
        
        //frames
        let lightFrame = CGRect(x: imageX1, y: imageY, width: imageWidth, height: imageH)
        let mediumFrame = CGRect(x: imageX2, y: imageY, width: imageWidth, height: imageH)
        let heavyFrame = CGRect(x: imageX3, y: imageY, width: imageWidth, height: imageH)

        //views
        let lightView = UIImageView(frame: lightFrame)
        let mediumView = UIImageView(frame: mediumFrame)
        let heavyView = UIImageView(frame: heavyFrame)
        
        //
        lightView.contentMode = .scaleAspectFill
        mediumView.contentMode = .scaleAspectFill
        heavyView.contentMode = .scaleAspectFill

        //
        lightView.layer.cornerRadius = 5.0
        lightView.layer.masksToBounds = true
        mediumView.layer.cornerRadius = 5.0
        mediumView.layer.masksToBounds = true
        heavyView.layer.cornerRadius = 5.0
        heavyView.layer.masksToBounds = true
        
        //
        lightView.backgroundColor = self.softSpaceGray
        mediumView.backgroundColor = self.softSpaceGray
        heavyView.backgroundColor = self.softSpaceGray

        //
        self.view.addSubview(lightView)
        self.view.addSubview(mediumView)
        self.view.addSubview(heavyView)

        //
        self.lightThumbView1 = lightView
        self.mediumThumbView2 = mediumView
        self.heavyThumbView3 = heavyView
        
        //add gesture recognizers
        let tapGesture_L = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.chooseLightThumbnail(_:)))
        tapGesture_L.delegate = self
        tapGesture_L.numberOfTapsRequired = 1
        tapGesture_L.numberOfTouchesRequired = 1
        self.lightThumbView1!.addGestureRecognizer(tapGesture_L)
        self.lightThumbView1!.isUserInteractionEnabled = true
        
        let tapGesture_M = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.chooseMediumThumbnail(_:)))
        tapGesture_M.delegate = self
        tapGesture_M.numberOfTapsRequired = 1
        tapGesture_M.numberOfTouchesRequired = 1
        self.mediumThumbView2!.addGestureRecognizer(tapGesture_M)
        self.mediumThumbView2!.isUserInteractionEnabled = true
        
        let tapGesture_H = UITapGestureRecognizer(target: self, action: #selector(ProfileImageViewController.chooseHeavyThumbnail(_:)))
        tapGesture_H.delegate = self
        tapGesture_H.numberOfTapsRequired = 1
        tapGesture_H.numberOfTouchesRequired = 1
        self.heavyThumbView3!.addGestureRecognizer(tapGesture_H)
        self.heavyThumbView3!.isUserInteractionEnabled = true
    }

//    blurState = ["lightBlur", "mediumBlur", "heavyBlur"]
    //    buttons: blurMedium blurHeavy blurLight
    
    @objc func chooseLightThumbnail(_ sender: UITapGestureRecognizer) {
        //tap gesture should: (1) update main view, (2) set the most recent picture for upload (3) set chosen blur state variable, (4) change the toggler switch
        
        //1
        UIView.transition(with: self.profileImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.profileImageView!.image = self.uiLightImage!
        }, completion: nil)
        
        
        //2
        self.updateImageToUpload(withBlurSigma: lightBlurSigma)
        
        //3
        self.blurState = "lightBlur"
    
        //4
        self.bView1!.image = UIImage(named: "blurLight")
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
    }
    
    @objc func chooseMediumThumbnail(_ sender: UITapGestureRecognizer) {
        
        //1
        self.profileImageView?.image = self.uiMediumImage!
        
        //2
        self.updateImageToUpload(withBlurSigma: mediumBlurSigma)
        
        //3
        self.blurState = "mediumBlur"
        
        //4
        self.bView1!.image = UIImage(named: "blurMedium")
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
    }
    
    @objc func chooseHeavyThumbnail(_ sender: UITapGestureRecognizer) {
        
        //1
        self.profileImageView?.image = self.uiHeavyImage!
        
        //2
        self.updateImageToUpload(withBlurSigma: heavyBlurSigma)
        
        //3
        self.blurState = "heavyBlur"
        
        //4
        self.bView1!.image = UIImage(named: "blurHeavy")
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
    }
    
    var blurChoiceLabel: UITextView?
//    var withState = "rising" "resting"
    
    func showHideBlurBox(withState: String) {
        var yPrime = 0 as CGFloat
        var animationOption: UIViewAnimationOptions = .curveEaseInOut
        
        if withState == "rising" {
            yPrime = -self.blurBoxHeight!
        } else {
            yPrime = self.blurBoxHeight!
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: animationOption, animations: {
            self.lineSeparatorView!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.blurChoiceLabel!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.lightThumbView1!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.mediumThumbView2!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.heavyThumbView3!.transform = CGAffineTransform(translationX: 0, y: yPrime)
        }, completion: nil)
    }
    
    func pushPullProfile(withState: String) {
        
        var yPrime = 0 as CGFloat
        var animationOption: UIViewAnimationOptions = .curveEaseIn
        
        if withState == "rising" {
            yPrime = -self.verticalDisplacement!
        } else {
            yPrime = self.verticalDisplacement!
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: animationOption, animations: {
            self.profileImageView!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.profileTogglerView!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.visibilityLabel!.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.bView1?.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.forwardArrowView?.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.imageErrorTextView?.transform = CGAffineTransform(translationX: 0, y: yPrime)
            self.profileDescriptorLabel?.transform = CGAffineTransform(translationX: 0, y: yPrime)
        }, completion: nil)

    }
    
    func addImageChoiceLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let tFontSize = 12.0 as CGFloat
        let tWidth = 7 * gridUnitWidth
        let choiceCopy = "Choose How Your Photo Will Appear to Others"
        let textFieldCGSize = self.sizeOfCopySmall(string: choiceCopy, constrainedToWidth: Double(tWidth))
        
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        var tY = self.lineSeparatorView!.frame.maxY + gridUnitHeight - tH
        
        //
//        let upperSpace = self.lineSeparatorView!.frame.maxY - self.lightThumbView1!.frame.minY
//        let centerY = self.lineSeparatorView!.frame.maxY + (upperSpace * 0.5)
//        tY = centerY - (0.5 * tH)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let blurChoiceLabel = UITextView(frame: tFrame)
        blurChoiceLabel.text = choiceCopy
        blurChoiceLabel.font = UIFont(name: "Avenir-Light", size: tFontSize)
        //
        self.view.addSubview(blurChoiceLabel)
        self.blurChoiceLabel = blurChoiceLabel
        
        self.blurChoiceLabel = blurChoiceLabel
        self.blurChoiceLabel!.textAlignment = .center
        self.blurChoiceLabel!.textAlignment = .center
        self.blurChoiceLabel!.isEditable = false
        self.blurChoiceLabel!.isScrollEnabled = false
        self.blurChoiceLabel!.isSelectable = false
        self.blurChoiceLabel!.textContainer.lineFragmentPadding = 0
        self.blurChoiceLabel!.textContainerInset = .zero
        self.blurChoiceLabel!.textColor = UIColor.black
        self.blurChoiceLabel!.backgroundColor = UIColor.clear
        
        let upperSpace =  self.lightThumbView1!.frame.minY - self.lineSeparatorView!.frame.maxY
        let centerY = self.lightThumbView1!.frame.minY - (0.5 * upperSpace)
        self.blurChoiceLabel?.layer.position = CGPoint(x: 0.5 * sWidth, y: centerY)
    }

//    let lightBlurSigma = 20.0
//    let mediumBlurSigma = 35.0
//    let heavyBlurSigma = 50.0

    let lightBlurSigma = 20.0
    let mediumBlurSigma = 40.0
    let heavyBlurSigma = 70.0
    
    var uiLightImage: UIImage?
    var uiMediumImage: UIImage?
    var uiHeavyImage: UIImage?
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //exception
        if self.copyIsSystemError == true {
            self.hidePhotoUploadErrorLabel()
        }
        
        //needs to preserve current usability setting
        if let clearProfilePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.profileImageView!.image = clearProfilePhoto
            self.clearProfilePhoto = clearProfilePhoto
        }
        
        self.visibilityLabel?.text = "Public Profile"
        
        let ciImageObject = CIImage(image: info[UIImagePickerControllerOriginalImage] as! UIImage)
        
        self.unblurredCIImage = ciImageObject
        self.blurredImageLight = ciImageObject?.applyingGaussianBlur(sigma: lightBlurSigma)
        self.blurredImageMedium = ciImageObject?.applyingGaussianBlur(sigma: mediumBlurSigma)
        self.blurredImageHeavy = ciImageObject?.applyingGaussianBlur(sigma: heavyBlurSigma)
    
        //create and init UIBlurredImages
        let blurredCICroppedImage_Light = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiLightImage = UIImage(ciImage: blurredCICroppedImage_Light)
        let blurredCICroppedImage_Medium = self.blurredImageMedium!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiMediumImage = UIImage(ciImage: blurredCICroppedImage_Medium)
        let blurredCICroppedImage_Heavy = self.blurredImageHeavy!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiHeavyImage = UIImage(ciImage: blurredCICroppedImage_Heavy)
        
        //set imageViews
        self.lightThumbView1?.image = self.uiLightImage!
        self.mediumThumbView2?.image = self.uiMediumImage!
        self.heavyThumbView3?.image = self.uiHeavyImage!

        picker.dismiss(animated: true, completion: nil)
        if #available(iOS 11.0, *) {
            self.faciemAspicio()
        } else {
            self.fdCoreImage()
        }
        
        self.profileImageisVisible = true
        self.bView1?.image = UIImage(named: "blurLight")!
    }
    
    @available(iOS 11.0, *)
    //import Vision SDK
    func faciemAspicio() {
        //print("CREATING face detection request")
        //create detect face request
        let faceDetectRequest = VNDetectFaceRectanglesRequest { (vnRequest, error) in
            if let error = error {
                //print("\(error) could not detect face")
            }
            if (vnRequest.results?.isEmpty)! {
                //print("NO FACE DETECTED")
                self.configurePhotoError_UI()
            } else {
                vnRequest.results?.forEach({ (detectionResult) in
                    //print("FACE SUCCESSFULLY DETECTED. DETECTION RESULT IS:", detectionResult)
                    self.configurePhotoSuccess_UI()
//                    self.addForwardArrow()
                })
            }
        }
        self.handleRequest(visionRequest: faceDetectRequest)
    }
    
    func configurePhotoSuccess_UI() {
        self.profileTogglerView?.isHidden = false
        self.visibilityLabel?.isHidden = false
        self.bView1?.isHidden = false
        self.showHideBlurBoxViews(bool: false)

//        self.transparencyView!.isHidden = true
        self.newPhotoImageView!.isHidden = true
        self.profileDescriptorLabel!.isHidden = true
        self.imageErrorTextView!.isHidden = true
        UIView.transition(with: self.profileTogglerView!, duration: 0.25, options: .transitionCrossDissolve, animations: {
             self.profileTogglerView!.image = UIImage(named: "publicProfile")
        }, completion: nil)
        self.profileTogglerView!.isUserInteractionEnabled = true
        self.noFaceImageView!.isHidden = true
        self.bView1?.isUserInteractionEnabled = true

        if self.forwardArrowView != nil {
            self.forwardArrowView!.setImage(UIImage(named: "profileImageForwardButton")!, for: .normal)
            self.forwardArrowView!.isUserInteractionEnabled = true
        } else {
            self.addForwardArrow()
        }
    }
    
    func showHideBlurBoxViews(bool: Bool) {
        self.lineSeparatorView?.isHidden = bool
        self.blurChoiceLabel?.isHidden = bool
        self.lightThumbView1?.isHidden = bool
        self.mediumThumbView2?.isHidden = bool
        self.heavyThumbView3?.isHidden = bool
    }
    
    func configurePhotoError_UI() {
//        self.transparencyView!.isHidden = false
        
        self.showHideBlurBoxViews(bool: true)
        
        self.profileTogglerView?.isHidden = true
        self.visibilityLabel?.isHidden = true
        self.bView1?.isHidden = true
        
        self.newPhotoImageView!.isHidden = false
        self.notification.notificationOccurred(.error)
        self.profileDescriptorLabel!.isHidden = true
        self.imageErrorTextView!.isHidden = false
        self.profileTogglerView!.image = UIImage(named: "visibilityControl_N")
        self.profileTogglerView!.isUserInteractionEnabled = false
        self.bView1?.isUserInteractionEnabled = false
        self.noFaceImageView!.isHidden = false
        if self.forwardArrowView != nil {
            self.forwardArrowView!.setImage(UIImage(named: "profileImageForwardButton_Inactive")!, for: .normal)
            self.forwardArrowView!.isUserInteractionEnabled = false
//            self.doneButtonView!.removeFromSuperview()
        } else {
//            self.addForwardArrow()
        }
        
    }
    
    //need to handle our request
    @available(iOS 11.0, *)
    func handleRequest(visionRequest: VNDetectFaceRectanglesRequest) {
        //print("HANDLING face detection request")
        guard let cgImage = self.profileImageView!.image?.cgImage else { return }
        let requestHandler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        do {
            try requestHandler.perform([visionRequest])
        } catch let requestError {
            //print("Failed to perform request:", requestError)
        }
    }
    
    func fdCoreImage() {
        //print("Detecting face using Core Image")
        let context = CIContext()
        //guard let ciImage = self.profileImageView.image?.ciImage else { return }
        guard let ciImage = CIImage(image: (profileImageView!.image)!) else { return }
        //print("image succesfully retrieved")
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: context, options: accuracy)
        let faces = faceDetector?.features(in: ciImage)
        //print("checking array values")
        if (faces?.isEmpty)! {
            //print("CORE IMAGE RESULT: no face detected")
            self.configurePhotoError_UI()
        } else {
            //print("CORE IMAGE RESULT: Face successfully detected")
            self.configurePhotoSuccess_UI()
//            self.addForwardArrow()
        }
    }
    
    func createImageSizes() {
        //Create Grid Thumbnail for largest screensize - set in clear & blurred & upload as clear & blurred
        
        //Create Profile Image - set in clear & blurred & upload as clear & blurred
        
        //clear vs blurred in separate location. Location of clear image can only be retrieved once other user approves profile swap. Does this pose any security threats to the privacy of the clear image?
    }
    
    //1. need to shift buttons left
    //2. need to show blur toggler
    //3. need to fan out options then restore
    //4. cycle through options
    
    @IBAction func blurImage(_ sender: Any) {
        //func facioAbscondo()
        let ciCroppedImage = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
        self.profileImageView?.image = uiCroppedBlurredImage
        
        let toImage = uiCroppedBlurredImage
        UIView.transition(with: self.profileImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.profileImageView!.image = toImage
        }, completion: nil)
        
        self.profileImageisVisible = false
//        self.contextCreateImage(withBlurSigma: )
    }
    

   
    func contextCreateImage(withBlurSigma: Double) {
        let context = CIContext()                                           // 1
        
        let filter = CIFilter(name: "CIGaussianBlur")!                         // 2
        filter.setValue(withBlurSigma, forKey: "inputRadius")
        
        let image = self.unblurredCIImage                                    // 3
        filter.setValue(image, forKey: kCIInputImageKey)
        
        let result = filter.outputImage!                                    // 4
        
        //test
        let croppedResult = result.cropped(to: (self.unblurredCIImage!.extent))
        let cgImage = context.createCGImage(croppedResult, from: croppedResult.extent)// 5
        self.uiCreatedImage = UIImage(cgImage: cgImage!)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
//    @IBAction func passPhotoButton(_ sender: Any) {
//        //self.aciaTotum()
//        //self.aciaObfuscare()
//        self.setDisplayProfilePhotoURL()
//        self.setUserVisibilityPreference()
//        if self.profileImageisVisible == true {
//            //
//        } else {
////            self.setVisibleImageURL()
//        }
//    }
    
    /*
    //Pass photo info
    func setDisplayProfilePhotoURL() {
        //print("CLEAR PHOTO SENDER IS setDisplayProfilePhotoURL")
        
        var imageToUpload: UIImage?
        
        
        if self.profileImageisVisible == true {
            //print("setDisplayProfilePhotoURL willSET photo from clearPhoto")
//            self.photoToUpload = self.clearProfilePhoto!
            imageToUpload = self.clearProfilePhoto!
        } else {
            //print("setDisplayProfilePhotoURL willSET photo from uiCreatedImage")
//            self.photoToUpload = self.uiCreatedImage!
            imageToUpload = self.uiCreatedImage!
            self.photoVault()
        }
        
        
        guard let imageData = UIImageJPEGRepresentation(self.photoToUpload!, 0.4) else { return }
            
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = storageRef!.child(imageStoragePath).putData(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                //print("Error uploading: \(error)")
                self.forwardArrowView?.isUserInteractionEnabled = true
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/userDisplayPhoto").setValue(self.storageRef!.child((metadata?.path)!).description)
                        
                        let fullURL = metadata?.downloadURLs?[0].absoluteString
                        //let fullstorageUri = self.storageRef.child((metadata?.path!)!).description
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/full_URL").setValue(fullURL)
                    }
                }
            }
        }
        
        //print("first image sent")
        
        uploadTask.observe(.success) { (snapshot) in
            //print("UPLOAD snapshot SUCCESS")
            self.activityIndicatorView?.stopAnimating()
            self.performSegue(withIdentifier: "toServices", sender: self)
        
//            if self.profileImageisVisible == true {
//                self.activityIndicatorView?.stopAnimating()
//                self.performSegue(withIdentifier: "toServices", sender: self)
//            } else {
//                //print("setDisp will post success completion notification")
//                self.photoIsStillUploading = false
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "photoUploadTask"), object: self)
//            }
        }
    }
    */
    
    //need to set fullPhoto url
    //need to set thumbURL
    
    func setDisplayProfilePhotoURL_GridThumb(forImageSize: String) {
        //print("CLEAR PHOTO SENDER IS setDisplayProfilePhotoURL_GridThumb")
        
        var photoToUpload: UIImage?
        var imageData: Data?
        var urlNode = "full_URL"
        var storageNode = "userDisplayPhoto"
        let dimension = 126 as CGFloat
        
        //1. Initialize UIImage
        if self.profileImageisVisible == true {
            //print("setDisplayProfilePhotoURL willSET photo from clearPhoto")
            //            self.photoToUpload = self.clearProfilePhoto!
            photoToUpload = self.clearProfilePhoto!
        } else {
            //print("setDisplayProfilePhotoURL willSET photo from uiCreatedImage")
            //            self.photoToUpload = self.uiCreatedImage!
            photoToUpload = self.uiCreatedImage!
            self.photoVault()
        }
        
        //2. Set photo storage path metadata
        if forImageSize == "fullPhoto" {
            urlNode = "full_URL"
            storageNode = "userDisplayPhoto"
        } else if forImageSize == "gridThumb" {
            urlNode = "gridThumb_URL"
            storageNode = "gridThumb_Stor"
        }
        
        //3. Resize image
        if forImageSize == "fullPhoto" {
            imageData = UIImageJPEGRepresentation(photoToUpload!, 0.9)
        } else if forImageSize == "gridThumb" {
            imageData = self.resizeImage(dimension, with: 0.9, forImage: photoToUpload)!
        }
        
        guard let data = imageData else { return }
        
        //make sure this doesn't overwrite existing file when saved twice
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = storageRef!.child(imageStoragePath).putData(data, metadata: metadata) { (metadata, error) in
            if let error = error {
                
                self.reenablePhotoUpload()
                
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        let thumb_URLPath = metadata?.downloadURLs?[0].absoluteString
                        let thumb_StoragePath = self.storageRef!.child((metadata?.path)!).description
                        
                        self.thumb_URLPath = thumb_URLPath!
                        self.thumb_StoragePath = thumb_StoragePath
                        
                        self.performBatchUpdates_Photo()
                        
                        //                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/\(urlNode)").setValue(thumb_URLPath!)
                        //                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/\(storageNode)").setValue(self.storageRef!.child((metadata?.path)!).description)
                        //
                        
                        //                        let fullURL = metadata?.downloadURLs?[0].absoluteString
                        //                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/\(urlNode)").setValue(fullURL)
                    }
                }
            }
        }
        
        //print("first image sent")
        
        uploadTask.observe(.success) { (snapshot) in
            //print("UPLOAD snapshot SUCCESS")
            //BEFORE
            //            self.activityIndicatorView?.stopAnimating()
            //            self.performSegue(withIdentifier: "toServices", sender: self)
            
            //AFTER
            self.performBatchUpdates_Photo()
        }
    }
    
    var copyIsSystemError = false
    
    func reenablePhotoUpload() {
        
        if self.profileImageisVisible == false {
            self.showHideBlurBox(withState: "rising")
        }
        
        self.activityIndicatorView?.stopAnimating()
        self.profileDescriptorLabel?.font = UIFont(name: "AvenirNext-Regular", size: 12)
        self.profileDescriptorLabel?.isHidden = false
        self.profileDescriptorLabel?.text = "We've run into an error. Please try again."
        self.profileDescriptorLabel?.textColor = UIColor.red
        self.copyIsSystemError = true
        
        self.forwardArrowView?.isUserInteractionEnabled = true
        self.profileTogglerView?.isUserInteractionEnabled = true
        self.bView1?.isUserInteractionEnabled = true
    }
    
    func setDisplayProfilePhotoURL(forImageSize: String) {
        //print("CLEAR PHOTO SENDER IS setDisplayProfilePhotoURL")
        
        var photoToUpload: UIImage?
        var imageData: Data?
        var urlNode = "full_URL"
        var storageNode = "userDisplayPhoto"
        let dimension = 126 as CGFloat
        
        //1. Initialize UIImage
        if self.profileImageisVisible == true {
            //print("setDisplayProfilePhotoURL willSET photo from clearPhoto")
            //            self.photoToUpload = self.clearProfilePhoto!
            photoToUpload = self.clearProfilePhoto!
        } else {
            //print("setDisplayProfilePhotoURL willSET photo from uiCreatedImage")
            //            self.photoToUpload = self.uiCreatedImage!
            photoToUpload = self.uiCreatedImage!
            self.photoVault()
        }
    
        //2. Set photo storage path metadata
        if forImageSize == "fullPhoto" {
            urlNode = "full_URL"
            storageNode = "userDisplayPhoto"
        } else if forImageSize == "gridThumb" {
            urlNode = "gridThumb_URL"
            storageNode = "gridThumb_Stor"
        }
        //3. Resize image
        if forImageSize == "fullPhoto" {
            imageData = UIImageJPEGRepresentation(photoToUpload!, 0.9)
        } else if forImageSize == "gridThumb" {
            imageData = self.resizeImage(dimension, with: 0.9, forImage: photoToUpload)!
        }
        
        guard let data = imageData else { return }
        
        //make sure this doesn't overwrite existing file when saved twice
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = storageRef!.child(imageStoragePath).putData(data, metadata: metadata) { (metadata, error) in
            if let error = error {
                
                self.reenablePhotoUpload()
                
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        
                        let full_URLPath = metadata?.downloadURLs?[0].absoluteString
                        let full_StoragePath = self.storageRef!.child((metadata?.path)!).description
                        
                        self.full_URLPath = full_URLPath!
                        self.full_StoragePath = full_StoragePath

//                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/\(storageNode)").setValue(self.storageRef!.child((metadata?.path)!).description)
                        
                        self.performBatchUpdates_Photo()

//                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/\(urlNode)").setValue(fullURL)
                    }
                }
            }
        }
        
        //print("first image sent")
        
        uploadTask.observe(.success) { (snapshot) in
            //print("UPLOAD snapshot SUCCESS")
            
//            self.activityIndicatorView?.stopAnimating()
//            self.performSegue(withIdentifier: "toServices", sender: self)
            
            //
            self.performBatchUpdates_Photo()

        }
    }
    
    var full_URLPath = ""
    var full_StoragePath = ""
    
    var thumb_URLPath = ""
    var thumb_StoragePath = ""
    
    var performBatcheUpdatesIsStillProcessing = false
    var didCompleteUpdates_Photo = false

    var batchUpdatesCounter_Photo = 0
    
    func performBatchUpdates_Photo() {
        
        //all fields have to be ready
        guard full_URLPath != "" else { return }
        guard full_StoragePath != "" else { return }
        guard thumb_URLPath != "" else { return }
        guard thumb_StoragePath != "" else { return }

        //Exit method call if (1) updates have been called and are still processing, (2) updates have been successfully performed
        guard self.performBatcheUpdatesIsStillProcessing == false else { return }
        guard self.didCompleteUpdates_Photo == false else { return }
        
        self.performBatcheUpdatesIsStillProcessing = true
        
        var visus = ""
        var blurState = ""
        
        if self.profileImageisVisible == true {
            visus = "true"
            blurState = "nil"
        } else {
            visus = "false"
            blurState = self.blurState
        }
        
        let selfUID = Auth.auth().currentUser!.uid
        
        //path updates
        let childUpdates = [
            "/users/\(selfUID)/full_URL": full_URLPath,
            "/users/\(selfUID)/userDisplayPhoto": full_StoragePath,
            
            "/users/\(selfUID)/gridThumb_URL": thumb_URLPath,
            "/users/\(selfUID)/gridThumb_Stor": thumb_StoragePath,
            
            "/users/\(selfUID)/visus": visus,
            "/users/\(selfUID)/blurState": blurState,
        ]
       
        ref.updateChildValues(childUpdates) { (error, forReference_Photo) in
            if error != nil {
                //ERROR DETECTED
                //x3 Max Re-trys
                
                //reset logic
                self.performBatcheUpdatesIsStillProcessing = false
                
                if self.batchUpdatesCounter_Photo < 4 {
                    self.batchUpdatesCounter_Photo = self.batchUpdatesCounter_Photo + 1
                    self.ref_AuthTracker.didCompleteUpdates_Photo = false
                    
                    //Call updates again
                    self.performBatchUpdates_Photo()
                    
                } else {
                    //After x4 trials -> perform final push in HOLD controller
                    self.didCompleteUpdates_Photo = false
                    self.ref_AuthTracker.didCompleteUpdates_Photo = false
                }
                
            } else {
                //
                self.performBatcheUpdatesIsStillProcessing = false
                self.didCompleteUpdates_Photo = true
                
                self.ref_AuthTracker.didCompleteUpdates_Photo = true
                
                //Retrieve current AuthTracker State
                self.ref_AuthTracker.throttleBatchUpdates_PhotoVC()

            }
        }
    }
    
    @objc func batchUpdatesComplete(_ notification: Notification) {
        
        //print("UPVC batchUpdatesComplete")
        
        if notification.name.rawValue == "authTrackerComplete" {
            
            self.activityIndicatorView?.stopAnimating()
            self.performSegue(withIdentifier: "toServices", sender: self)
        }
    }
    
    
    func resizeImage(_ dimension: CGFloat, with quality: CGFloat, forImage: UIImage?) -> Data? {
        //    UIImageJPEGRepresentation(_ image: UIImage, _ compressionQuality: CGFloat) -> Data?
        return UIImageJPEGRepresentation(resizeImage(dimension, forImage: forImage!), quality)
    }
    
    func resizeImage(_ dimension: CGFloat, forImage: UIImage) -> UIImage {
        var width: CGFloat
        var height: CGFloat
        var newImage: UIImage
        
        var croppedImage: UIImage
        let oldImage = forImage
        let size = oldImage.size
        
        let aspectRatio = size.width / size.height
        
        if aspectRatio > 1 {                            // Landscape image
            width = dimension
            height = dimension / aspectRatio
        } else {                                        // Portrait image
            height = dimension
            width = dimension * aspectRatio
        }
        
        if #available(iOS 10.0, *) {
            let renderFormat = UIGraphicsImageRendererFormat.default()
            let renderer = UIGraphicsImageRenderer(size: CGSize(width: width, height: height), format: renderFormat)
            newImage = renderer.image { _ in
                oldImage.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            }
            
        } else {
            UIGraphicsBeginImageContext(CGSize(width: width, height: height))
            oldImage.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
            newImage = UIGraphicsGetImageFromCurrentImageContext()!
            
            UIGraphicsEndImageContext()
        }
        
        return newImage
    }
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        let radiusRatio = 14 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = (gridUnitWidth - aW) * 0.5
        let aY = aX
        
        let centerX = self.profileImageView!.center.x
        let centerY = self.profileImageView!.center.y
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.white
        activityIndicatorView.hidesWhenStopped = true
        
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
        
        self.activityIndicatorView!.layer.position = CGPoint(x: centerX, y: centerY)
        //        self.activityIndicatorView!.startAnimating()
    }
    
    func photoVault() {
        //print("PV1. writing to vault")
        guard let localPhoto = self.clearProfilePhoto else { return }
        let fileName = "\(selfUID!).jpeg"
        if let data = UIImageJPEGRepresentation(localPhoto, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("PV2. photo written to vault")
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    /*
    func setVisibleImageURL() {
        //print("BLUR PHOTO SENDER IS setVisibleImageURL")
        
        //called when profileImageisVisible == true
        //print("sending second image")
        let imageData = UIImageJPEGRepresentation(self.clearProfilePhoto!, 0.4)!
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = storageRef!.child(imageStoragePath).putData(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                //print("Error uploading: \(error)")
                return
            } else {
                //print("storing file")
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                        self.forwardArrowView?.isUserInteractionEnabled = true
                        
                    } else {
                        //self.ref.child("users/\(Auth.auth().currentUser!.uid)/userDisplayPhoto").setValue(self.storageRef!.child((metadata?.path)!).description)
                        
                        //should array value be 0,1?
                        let visibleImageURL = metadata?.downloadURLs?[0].absoluteString
                        //let fullstorageUri = self.storageRef.child((metadata?.path!)!).description
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/visibleImageURL").setValue(visibleImageURL)
                    }
                }
            }
        }
        
        //print("second image sent")
        uploadTask.observe(.success) { (snapshot) in
            //print("UPLOAD snapshot SUCCESS")
//            self.activityIndicatorView?.stopAnimating()
//            self.performSegue(withIdentifier: "toServices", sender: self)
        }
    }
    */
    
    func setUserVisibilityPreference() {
        guard let user = Auth.auth().currentUser else { return }
        let blurState = self.blurState
        if self.profileImageisVisible == true {
            self.ref.child("users/\(user.uid)/visus").setValue("true")
            self.ref.child("users/\(user.uid)/blurState").setValue("nil")
        } else {
            self.ref.child("users/\(user.uid)/visus").setValue("false")
            self.ref.child("users/\(user.uid)/blurState").setValue("\(blurState)")
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
