//
//  TrustViewController
//  Ximo
//
//  Created by Asaad on 4/24/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import FBSDKCoreKit
import Shimmer

class TrustViewController: UIViewController, UIGestureRecognizerDelegate, UITextViewDelegate, UIScrollViewDelegate {

    var ref: DatabaseReference!
    var user: User?
    
    var ref_AuthTracker = AuthTracker()

    var kbHeight: CGFloat?

    var userMinAge: Int?
    
    var yesFBPermissions = false

    override func loadView() {
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        let bColor = UIColor(hue: 0, saturation: 0, brightness: 0.3, alpha: 1)
        mainView.backgroundColor = bColor
        self.view = mainView
        
//        self.bottomPadding = 34 as CGFloat
        
        //add views
        self.addTrustLabel()
        self.addVeriCopy()
        self.addStorageCopy()
        self.addInfoButton()
        self.addFacebookLoginButton()
//        self.addFacebookButtonShimmerLayer()
        self.addUnlinkCheckText()
        self.addChooseUnlinkSelector()
//                self.addunlinkFBTextView()
        
        if self.yesFBPermissions == false {
            self.addShimmerViews_noFB()
            self.addNameAgeLabels_noFB()
            self.addNameAgeHeaders_noFB()
        } else {
            self.addShimmerViews()
            self.addNameAgeLabels()
            self.addNameAgeHeaders()
        }

        self.configureShimmerViews()
//        self.addNameVisibilityLabel()
//        self.addUnlinkButtonView()
//        self.addunlinkFBTextView()
        self.addMoreInfoScrollView()
        self.addScrollViewPages()
        self.addUnlinkButtonView()
//        self.addunlinkFBTextView()
        self.addPageControl()
        self.addPageControlSubviews()

        self.addCompletionRing()
        self.addActivityIndicator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()

        // Do any additional setup after loading the view.
        self.nameSubtitle?.isHidden = true
        self.ageSubtitle?.isHidden = true
        self.nameVisibilityLabel?.isHidden = true
        self.checkRingLayer?.isHidden = true
        self.checkMarkView?.isHidden = true
        
        self.nameLabel?.isHidden = true
        self.ageRangeLabel?.isHidden = true
//        self.unlinkBarButton?.isHidden = true
    
        NotificationCenter.default.addObserver(self, selector: #selector(TrustViewController.appWillTerminate(_:)), name: .UIApplicationWillTerminate, object: nil)
        
        if self.ref_AuthTracker.phoneAuthSource == true {
            if let didAttemptEmailAuth = UserDefaults.standard.string(forKey: "didAttemptEmailAuth") {
                if didAttemptEmailAuth == "true" {
                    if let didSeeNoFacebookPrompt = UserDefaults.standard.string(forKey: "didSeeNoFacebookPrompt") {
                        if didSeeNoFacebookPrompt == "true" {
                            self.addSkipButton()
                        }
                    } else {
                        //print("nodidSeeNoFacebookPrompt")
                    }
                } else {
                    //print("falsedidAttemptEmailAuth")
                }
            } else {
                //print("noDidAttemptEmailAuth")
            }
        }
        
        self.animatePagesHint()
    }
    
    var willLoopPages = false
    var arrestAutoScroll = false
    var resetFullCycleFromManual = false
    let timeToWait = 2.0

    var didInitializeLoop = false
    let presentAfter = 1.0
    
    func animatePagesHint() {
        
        //to page 2
        var when = DispatchTime.now() + timeToWait
        if willLoopPages == true {
            when = DispatchTime.now()
        }
        
        if self.didInitializeLoop == false {
            when = DispatchTime.now() + presentAfter
            self.didInitializeLoop = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            if self.arrestAutoScroll == true { return } //arrest on manual input
            if self.resetFullCycleFromManual == true { return }
            
            self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame2!.frame), animated: true)
            self.pageControl?.currentPage = Int(1)
            self.currentPageIndex = Int(1)
            
            //to page 3
            when = DispatchTime.now() + self.timeToWait
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                if self.arrestAutoScroll == true { return }
                if self.resetFullCycleFromManual == true { return }

                self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame3!.frame), animated: true)
                self.pageControl?.currentPage = Int(2)
                self.currentPageIndex = Int(2)
                
                //BACK to page 1
                when = DispatchTime.now() + self.timeToWait
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    if self.arrestAutoScroll == true { return }
                    if self.resetFullCycleFromManual == true { return }

                    self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame1!.frame), animated: true)
                    self.pageControl?.currentPage = Int(0)
                    self.currentPageIndex = Int(0)
                    
                    //loop animation
                    when = DispatchTime.now() + self.timeToWait
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.willLoopPages = true
                        self.animatePagesHint()
                    }
                }
            }
        }
    }
    
    func animateFromPage1() {
        //~Arrest the current animation cycle
//        self.resetFullCycleFromManual = true

        //to page 2
        var when = DispatchTime.now() + timeToWait
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.arrestAutoScroll = false
            if self.arrestAutoScroll == true { return }
            
            self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame2!.frame), animated: true)
            self.pageControl?.currentPage = Int(1)
            self.currentPageIndex = Int(1)
            
            //to page 3
            when = DispatchTime.now() + self.timeToWait
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                if self.arrestAutoScroll == true { return }
                
                self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame3!.frame), animated: true)
                self.pageControl?.currentPage = Int(2)
                self.currentPageIndex = Int(2)
                
                //BACK to page 1
                when = DispatchTime.now() + self.timeToWait
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    if self.arrestAutoScroll == true { return }
                    
                    self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame1!.frame), animated: true)
                    self.pageControl?.currentPage = Int(0)
                    self.currentPageIndex = Int(0)
                    
                    //loop animation
                    when = DispatchTime.now() + self.timeToWait
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.willLoopPages = true
                        //~Enable animation cycle to reset
//                        self.resetFullCycleFromManual = false
                        self.animatePagesHint()
                    }
                }
            }
        }
    }
    
    func animateFromPage2() {
        var when = DispatchTime.now() + self.timeToWait
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.arrestAutoScroll = false
            if self.arrestAutoScroll == true { return }
            
            self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame3!.frame), animated: true)
            self.pageControl?.currentPage = Int(2)
            self.currentPageIndex = Int(2)
            
            //back to one
            when = DispatchTime.now() + self.timeToWait
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                if self.arrestAutoScroll == true { return }
                
                self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame1!.frame), animated: true)
                self.pageControl?.currentPage = Int(0)
                self.currentPageIndex = Int(0)
                
                //loop animation
                when = DispatchTime.now() + self.timeToWait
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.willLoopPages = true
                    self.animatePagesHint()
                }
            }
        }
    }
    
    func animateFromPage3() {
        
        var when = DispatchTime.now() + self.timeToWait
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.arrestAutoScroll = false
            if self.arrestAutoScroll == true { return }
            
            self.moreInfoScrollView!.scrollRectToVisible((self.scrollFrame1!.frame), animated: true)
            self.pageControl?.currentPage = Int(0)
            self.currentPageIndex = Int(0)
            
            //loop animation
            when = DispatchTime.now() + self.timeToWait
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.willLoopPages = true
                self.animatePagesHint()
            }
        }
    }
    
    var skipButton: UIButton?
    
    func addSkipButton() {
        let sWidth = UIScreen.main.bounds.width
        let sHeight = UIScreen.main.bounds.height
        var sBarH = UIApplication.shared.statusBarFrame.size.height
        
        let rightInsetRatio = 16 / 414 as CGFloat
        let topInsetRatio = 22 / 736 as CGFloat
        sBarH = 20 as CGFloat
        
        var topInset = (topInsetRatio * sHeight) + sBarH
        
        if sHeight > 750 {
            sBarH = 44 as CGFloat
            topInset = (topInsetRatio * 736) + sBarH
        }
        
        let rightInset = (rightInsetRatio * sWidth) * 0.5
        
        let bWidth = 50 as CGFloat
        let bHeight = 20 as CGFloat
        let bX = sWidth - rightInset - bWidth
        let bY = topInset * 0.5
        
        let skipButton = UIButton()
        skipButton.frame = CGRect(x: bX, y: bY, width: bWidth, height: bHeight)
//        skipButton.backgroundColor = UIColor.red
        skipButton.setTitle("SKIP", for: .normal)
        skipButton.titleLabel!.addCharacterSpacing(withValue: 0.5)
        skipButton.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 16.0)!
        skipButton.setTitleColor(self.hyperBlue, for: .normal)
        skipButton.titleLabel!.textAlignment = .right
        skipButton.addTarget(self, action: #selector(TrustViewController.skipFBVerification(_:)), for: .touchUpInside)
        self.view.addSubview(skipButton)
        self.skipButton = skipButton
    }
    
    var didSkipFacebookVerification = false
    
    @objc func skipFBVerification(_ sender: UIButton) {
        //print("skipFBVerification tap detected")
        
        self.didSkipFacebookVerification = true
        self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)
        
    }
    
    var userBeforeLogout: User?
    
    //ATTN:
    /*
     1. Need to write a cloud function that observes the "deletedUsers_Onboard" FB node, to delete user emails from (Authentication) using. user.delete method
     2. OR write an iOS method that observes "deletedUsers_Onboard" & instructs deletion of accounts
     3. BTW, can someone inject code into the app that deletes user accounts? i.e. How do you ensure that someone can't just delete user accounts? Make sure only server admins can do this from console... Perhaps by writing security rules that prevent anyone except for admin from deleting data in the DB
     */
    
    @objc func appWillTerminate(_ notification: Notification) {
        //print("SecurityVC Detected UIApplicationWillTerminate notification")
        self.deleteAuthOnTerminate()
    }
    
    func deleteAuthOnTerminate() {
        if let user = Auth.auth().currentUser {
            let selfUID = user.uid
            let firebaseAuth = Auth.auth()
            
            self.userBeforeLogout = user
            
            self.ref.child("deletedUsers_Onboard/\(selfUID)").setValue("true")
            //Note: not absolutely necessary to removeUser node if termiantesAuth, since user will not show up in popArrayDict when geoFire key is not set
            self.ref.child("users/\(selfUID)").removeValue() //NEWdel - was disabled
            
            do {
                try firebaseAuth.signOut()
                //print("signout  successful")
            } catch let signOutError as NSError {
                print ("signout  unsuccessful")
            }
            
            //fails to continue since user has been logged out
//            self.userBeforeLogout?.delete { error in
//                if let error = error {
//                    // An error happened.
//                    //print("appWillTerminate delete error is", error)
//                } else {
//                    // Account deleted.
//                    //print("appWillTerminate account deleted successfully")
//                    let firebaseAuth = Auth.auth()
//                }
//            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        //print("TrustVC Application willterminate")
        
    }
    
    var bottomPadding: CGFloat?
    
    override func viewWillAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
//            self.bottomPadding = bottomPadding
            //print("TVC iPhoneX bottomPadding,", bottomPadding)
        }
        
//        self.configureFBButtonShimmer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var trustLabel: UILabel?
    var veriCopyTextView: UITextView?
    var unlinkFBTextView: UITextView?
    var unlinkBarButton: UIButton?

    func addTrustLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 32 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let trustLabel = UILabel(frame: tF)
        
        trustLabel.text = "Trust"
        trustLabel.font = UIFont(name: "AvenirNext-Regular", size: 22)!
        trustLabel.textColor = UIColor.white
        trustLabel.textAlignment = .center
        
        //init
        self.view.addSubview(trustLabel)
        self.trustLabel = trustLabel
    }
    
    var attentionSymbol: UIImageView?
    
    func addAttentionSymbol() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageW = 23 as CGFloat
        let imageH = imageW
        let iX = (sWidth - imageW) * 0.5
        let iY = (1.5 * superInsetHeight) - (0.5 * imageH)
        
        let imageFrame = CGRect(x: iX, y: iY, width: imageW, height: imageH)
        let attnSymb = UIImageView(frame: imageFrame)
        attnSymb.image = UIImage(named: "attentionSymbol")!

        
        self.attentionSymbol = attnSymb
        attnSymb.contentMode = .scaleAspectFill
        
        self.view.addSubview(attnSymb)
  
    }
    
    var friendsDeniedCopy: UITextView?
    
    var superInsetHeight: CGFloat?
    
    func addFriendsDeniedCopy() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        self.superInsetHeight = superInsetHeight
        let tWidthRatio = 365 / 414 as CGFloat
        
        var tFontSize = 14.0 as CGFloat
        var tWidth = 330 as CGFloat
        if sWidth > 350 {
            tWidth = 330 as CGFloat
            tFontSize = 14.0 as CGFloat
        } else {
            tWidth = 278 as CGFloat
            tFontSize = 12.0 as CGFloat
        }
        
        tWidth = tWidthRatio * sWidth
        
        let veriCopy = "To ensure that every profile on Ximo is real, we require that all users grant access to their \"friends list.\"" + "\n" + "\n" + "We never access or store any personal information about your friends or who they are. We simply fact-check that users have a “friends list” to filter out any fake profiles."
        
        let textFieldCGSize = self.sizeOfCopy(string: veriCopy, constrainedToWidth: Double(tWidth), fontName: "Avenir-Light", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let tY = (2 * superInsetHeight) - (1.0 * gridUnitHeight)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let friendsDeniedCopy = UITextView(frame: tFrame)
        
        friendsDeniedCopy.font = UIFont(name: "Avenir-Light", size: tFontSize)
            
        //
        self.view.addSubview(friendsDeniedCopy)
        
        self.friendsDeniedCopy = friendsDeniedCopy
        self.friendsDeniedCopy!.text = veriCopy
        self.friendsDeniedCopy!.textAlignment = .center
        self.friendsDeniedCopy!.isEditable = false
        self.friendsDeniedCopy!.isScrollEnabled = false
        self.friendsDeniedCopy!.isSelectable = false
        self.friendsDeniedCopy!.textContainer.lineFragmentPadding = 0
        self.friendsDeniedCopy!.textContainerInset = .zero
        self.friendsDeniedCopy!.textColor = UIColor.white
        self.friendsDeniedCopy!.backgroundColor = UIColor.clear
    }
    
    var transparencyView: UIView?
    
    func configureInvalidProfileUI() {
        //print("configureInvalidProfileUI")
        self.veriCopyTextView?.isHidden = true
        self.storageCopyTextView?.isHidden = true
        self.addInvalidProfileCopy()
        
        if self.attentionSymbol != nil {
            //print("attentionSymbol is not nil")
//            self.attentionSymbol?.isHidden = false
        } else {
            //print("attentionSymbol IS nil")
            self.addAttentionSymbol()
        }
    }
    
    var invalidCopyTextView: UITextView?
    
    func addInvalidProfileCopy() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 14.0 as CGFloat
        var tWidth = 330 as CGFloat
        if sWidth < 400 {
            tFontSize = 12.0 as CGFloat
        }
    
        let veriCopy = "Unable to confirm the validity of this profile." + "\n" + "Please try signing in using a different account."
        let textFieldCGSize = self.sizeOfCopy(string: veriCopy, constrainedToWidth: Double(UIScreen.main.bounds.width), fontName: "Avenir-Light", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let tY = superInsetHeight +  (3 * gridUnitHeight)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let invalidCopyTextView = UITextView(frame: tFrame)
        invalidCopyTextView.font = UIFont(name: "Avenir-Light", size: tFontSize)
        
        
        invalidCopyTextView.text = veriCopy
        self.view.addSubview(invalidCopyTextView)
        self.invalidCopyTextView = invalidCopyTextView
        self.invalidCopyTextView!.textAlignment = .center
        self.invalidCopyTextView!.isEditable = false
        self.invalidCopyTextView!.isScrollEnabled = false
        self.invalidCopyTextView!.isSelectable = false
        self.invalidCopyTextView!.textContainer.lineFragmentPadding = 0
        self.invalidCopyTextView!.textContainerInset = .zero
        self.invalidCopyTextView!.textColor = UIColor.white
        self.invalidCopyTextView!.backgroundColor = UIColor.clear
        
        self.invalidCopyTextView = invalidCopyTextView
    }

    func configureFriendsDeniedUI() {
        //1 add transparency view
        let tWidth = UIScreen.main.bounds.width
        let tHeight = UIScreen.main.bounds.height
        
        let tView = UIView(frame: CGRect(x: 0, y: 0, width: tWidth, height: tHeight))
        self.transparencyView = tView
//        tView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(0.8)
        tView.backgroundColor = UIColor.black
        tView.alpha = 0.8
        self.view.addSubview(tView)
        
        //2 add attentino symbol
        self.addAttentionSymbol()
        
        //3 add denied copy
        self.nameLabel?.isHidden = true
        self.ageRangeLabel?.isHidden = true
        self.chooseUnlinkRing?.isHidden = true
        self.chooseUnlinkCheck?.isHidden = true
        self.unlinkCopyTextView?.isHidden = true
        self.unlinkDescription?.isHidden = true
        self.storageCopyTextView?.isHidden = true
        self.veriCopyTextView?.isHidden = true
        self.addFriendsDeniedCopy()
        
        //4 shift info button
        let yDisp = self.superInsetHeight!
//        self.infoLabelButton?.transform = CGAffineTransform(translationX: 0, y: yDisp)
        let newInfoButtonY = self.friendsDeniedCopy!.frame.maxY
        self.infoLabelButton?.frame.origin.y = newInfoButtonY + 3
        
        //5 updated info button text
        self.infoLabelButton?.text = "How Does Ximo Use my Facebook Friends Info?"
        
        //5 shift fb button
        self.fbLoginButton?.transform = CGAffineTransform(translationX: 0, y: yDisp)
        self.addPleaseVerifyYourProfileWithFacebook()
        
        if UIScreen.main.bounds.height < 700 {
            self.fbLoginButton?.frame.origin.y = self.infoLabelButton!.frame.maxY + (self.infoLabelButton!.bounds.height * 0.5)
            self.tryAgainLabel!.frame.origin.y = (self.fbLoginButton?.frame.maxY)!
        } else if UIScreen.main.bounds.height > 750 {
            self.fbLoginButton?.frame.origin.y = self.infoLabelButton!.frame.maxY + (self.infoLabelButton!.bounds.height * 0.5)
            self.tryAgainLabel!.frame.origin.y = (self.fbLoginButton?.frame.maxY)!
        }
      
        self.view.bringSubview(toFront: self.trustLabel!)
        self.view.bringSubview(toFront: self.fbLoginButton!)
        self.view.bringSubview(toFront: self.infoLabelButton!)
        //
    }
    
    func removeFriendListDeniedUI() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let bYPaddingError = 4 as CGFloat
        let bY = 2 * superInsetHeight + gridUnitHeight - bYPaddingError
        let bannerView_Y = 2.5 * superInsetHeight

        //
        self.chooseUnlinkRing?.isHidden = false
        if self.didCheckFBUnlink == true {
            self.chooseUnlinkCheck?.isHidden = false
        } else {
            
        }
        self.unlinkCopyTextView?.isHidden = false
        self.unlinkDescription?.isHidden = false
        
        self.attentionSymbol?.removeFromSuperview()
        self.transparencyView?.removeFromSuperview()
        self.friendsDeniedCopy?.removeFromSuperview()
        self.tryAgainLabel?.removeFromSuperview()
        
        self.infoLabelButton?.frame.origin.y = bY
        self.fbLoginButton?.frame.origin.y = bannerView_Y
    
        self.veriCopyTextView?.isHidden = false
        self.storageCopyTextView?.isHidden = false
        self.infoLabelButton?.text = "How does Ximo use my Facebook information?"
    }
    
    var tryAgainLabel: UILabel?
    
    func addPleaseVerifyYourProfileWithFacebook() {
        
        let labelWidth = 250 as CGFloat
        let lH = 35 as CGFloat
        let lX = (UIScreen.main.bounds.width - labelWidth) * 0.5
        let lY = self.fbLoginButton!.frame.maxY
        
        let label = UILabel(frame: CGRect(x: lX, y: lY, width: labelWidth, height: lH))
        label.text = "Review Info"
        label.font = UIFont(name: "AvenirNext-UltraLight", size: 12)
        label.textColor = UIColor.white
        label.textAlignment = .center
        
        self.view.addSubview(label)
        self.tryAgainLabel = label
    }
    
    func addVeriCopy() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
  
        var tFontSize = 12.0 as CGFloat
        var tWidth = 330 as CGFloat
        if sWidth > 350 {
            tWidth = 330 as CGFloat
            tFontSize = 12.0 as CGFloat
        } else {
            tWidth = 278 as CGFloat
            tFontSize = 10.0 as CGFloat
        }
    
        let veriCopy = "To ensure that users only communicate with trusted profiles," + "\n" + "please use the authentication method below to" + "\n" + "verify your information."
        let textFieldCGSize = self.sizeOfCopyLarge(string: veriCopy, constrainedToWidth: Double(tWidth))
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let tY = superInsetHeight +  (1.5 * gridUnitHeight)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let tView = UITextView(frame: tFrame)
        tView.delegate = self
        tView.font = UIFont(name: "Avenir-Light", size: tFontSize)
        
        //configure text
        
        let tBold = "verify your information."
        let tReg1 = "To ensure that users only communicate with trusted profiles,"
        let tReg2 = "please use the authentication method below to"
        let range = (veriCopy as NSString).range(of: tBold)
        let r1 = (veriCopy as NSString).range(of: tReg1)
        let r2 = (veriCopy as NSString).range(of: tReg2)
        
        let attributedString = NSMutableAttributedString(string: veriCopy)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r2)
        
        tView.attributedText = attributedString
        
        //
        self.view.addSubview(tView)
        
        self.veriCopyTextView = tView
        self.veriCopyTextView!.textAlignment = .center
        self.veriCopyTextView!.isEditable = false
        self.veriCopyTextView!.isScrollEnabled = false
        self.veriCopyTextView!.isSelectable = false
        self.veriCopyTextView!.textContainer.lineFragmentPadding = 0
        self.veriCopyTextView!.textContainerInset = .zero
        self.veriCopyTextView!.textColor = UIColor.white
        self.veriCopyTextView!.backgroundColor = UIColor.clear
        
        self.veriCopyTextView = tView
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        
        //print("textViewDidChangeSelection")
        let selectedRange: UITextRange? = textView.selectedTextRange
        var selectedText: String? = "verify your information"
        if let aRange = selectedRange {
            selectedText = textView.text(in: aRange)
            //print("selectedText is", selectedText)
            
        }
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    func sizeOfCopyLarge(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 12.0)! ],
            context: nil).size
    }
    
    func sizeOfCopySmall(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 10.0)! ],
            context: nil).size
    }
    
    var storageCopyTextView: UITextView?
    
    func addStorageCopy() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 12.0 as CGFloat
        var tWidth = 330 as CGFloat
        if sWidth > 350 {
            tWidth = 330 as CGFloat
            tFontSize = 12.0 as CGFloat
        } else {
            tWidth = 278 as CGFloat
            tFontSize = 10.0 as CGFloat
        }
        
        //noFBUI
        var veriCopy = "Ximo only stores your First Name and Age Range."
        if self.yesFBPermissions == true {
            
        } else {
            veriCopy = "Ximo only stores your First Name."
        }
        
        let textFieldCGSize = self.sizeOfCopyLarge(string: veriCopy, constrainedToWidth: Double(tWidth))
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let yPaddingError = 2 as CGFloat
        let tY = 2 * superInsetHeight - yPaddingError
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let tView = UITextView(frame: tFrame)
        
        tView.font = UIFont(name: "Avenir-Light", size: tFontSize)
        
        //configure text
        
        //
        /*
        let tBold = "First Name"
        let tBold2 = "Age Range"
        let tReg1 = "Ximo only stores your"
        let tReg2 = "and"
        
        let r1 = (veriCopy as NSString).range(of: tBold)
        let r2 = (veriCopy as NSString).range(of: tBold2)
        let r3 = (veriCopy as NSString).range(of: tReg1)
        let r4 = (veriCopy as NSString).range(of: tReg2)

        let attributedString = NSMutableAttributedString(string: veriCopy)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: r1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: r2)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r3)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r4)
        */
        
        //noFBUI
        let tBold = "First Name"
        let tReg1 = "Ximo only stores your"
        
        let r1 = (veriCopy as NSString).range(of: tBold)
        let r3 = (veriCopy as NSString).range(of: tReg1)
        
        let attributedString = NSMutableAttributedString(string: veriCopy)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: r1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r3)
        tView.attributedText = attributedString
        
        //
        self.view.addSubview(tView)
        
        self.storageCopyTextView = tView
        self.storageCopyTextView!.textAlignment = .center
        self.storageCopyTextView!.isEditable = false
        self.storageCopyTextView!.isScrollEnabled = false
        self.storageCopyTextView!.isSelectable = false
        self.storageCopyTextView!.textContainer.lineFragmentPadding = 0
        self.storageCopyTextView!.textContainerInset = .zero
        self.storageCopyTextView!.textColor = UIColor.white
        self.storageCopyTextView!.backgroundColor = UIColor.clear
        
        self.storageCopyTextView = tView
    }
    
    var infoLabelButton: UILabel?
    var fbLoginButton: UIImageView?
    
    func addInfoButton() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let buttonString = "How Does Ximo Use my Facebook Information?"
        
//        var bW = 200 as CGFloat
//        var bW = 280 as CGFloat
        var bW = 310 as CGFloat

        var buttonCGSize = self.sizeOfCopyLarge(string: buttonString, constrainedToWidth: Double(bW))
        var bH = ceil(buttonCGSize.height)
        var bFontSize = 12 as CGFloat
        
        if sWidth > 350 {
            bW = 310 as CGFloat
        } else {
//            bW = 250 as CGFloat
            bW = 290 as CGFloat
            bFontSize = 10 as CGFloat
            buttonCGSize = self.sizeOfCopySmall(string: buttonString, constrainedToWidth: Double(bW))
            bH = ceil(buttonCGSize.height)
        }
        
        let bX = (sWidth - bW) * 0.5
        let bYPaddingError = 4 as CGFloat
        let bY = 2 * superInsetHeight + gridUnitHeight - bYPaddingError
       
        let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
        let infoLabelButton = UILabel(frame: bFrame)
        
        //
        let buttonFont = UIFont(name: "Avenir-Light", size: bFontSize)!
        let bColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.view.addSubview(infoLabelButton)
        self.infoLabelButton = infoLabelButton
        self.infoLabelButton!.text = buttonString
        self.infoLabelButton!.font = buttonFont
        self.infoLabelButton!.textColor = bColor
        self.infoLabelButton!.textAlignment = .center
        
        //add gesture
        //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.infoButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.infoLabelButton!.addGestureRecognizer(tapGesture)
        self.infoLabelButton!.isUserInteractionEnabled = true
    }


    func addFacebookLoginButton() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //set view properties
        let bannerViewWidth = sWidth
        let bannerViewHeight = 2 * gridUnitHeight
        let bannerView_X = 0 as CGFloat
        let bannerView_Y = 2.5 * superInsetHeight
        
        //initialize view
        let bannerViewFrame = CGRect(x: bannerView_X, y: bannerView_Y, width: bannerViewWidth, height: bannerViewHeight)
        let fbLoginButton = UIImageView(frame: bannerViewFrame)
        fbLoginButton.image = UIImage(named: "fbButton")
        fbLoginButton.contentMode = .scaleAspectFill
        
        self.view.addSubview(fbLoginButton)
        self.fbLoginButton = fbLoginButton
        
//        fbLoginButton.layer.shadowOpacity = 0.8
//        fbLoginButton.layer.shadowOffset = CGSize(width: 0, height: 0)
//        fbLoginButton.layer.shadowRadius = 10
//        fbLoginButton.layer.shadowColor = UIColor.white.cgColor
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.fbButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.fbLoginButton!.addGestureRecognizer(tapGesture)
        self.fbLoginButton!.isUserInteractionEnabled = true
        
        self.fbLoginButton!.layer.shadowColor = UIColor.darkGray.cgColor
    }
    
    var facebookShimmeringLayer: FBShimmeringView!
    
    func addFacebookButtonShimmerLayer() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //set view properties
        let bannerViewWidth = sWidth
        let bannerViewHeight = 2 * gridUnitHeight
        let bannerView_X = 0 as CGFloat
        let bannerView_Y = 2.5 * superInsetHeight
        
        //initialize view
        let bannerViewFrame = CGRect(x: bannerView_X, y: bannerView_Y, width: bannerViewWidth, height: bannerViewHeight)
        let bannerShimmerView = FBShimmeringView(frame: bannerViewFrame)
        
        //shimmer layer
        self.view.addSubview(bannerShimmerView)
        self.facebookShimmeringLayer = bannerShimmerView
        
        //buttonView
        let buttonViewFrame = CGRect(x: 0, y: 0, width: bannerViewWidth, height: bannerViewHeight)
        let fbLoginButton = UIImageView(frame: bannerViewFrame)
        fbLoginButton.image = UIImage(named: "fbButton")
        fbLoginButton.contentMode = .scaleAspectFill
        
        self.fbLoginButton = fbLoginButton
        self.facebookShimmeringLayer.addSubview(fbLoginButton)
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.fbButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.fbLoginButton!.addGestureRecognizer(tapGesture)
        self.fbLoginButton!.isUserInteractionEnabled = true
        
    }
    
    func configureFBButtonShimmer() {
        facebookShimmeringLayer.contentView = self.fbLoginButton!
        facebookShimmeringLayer.isShimmering = true
        
        //opacity before shimmer
        facebookShimmeringLayer.shimmeringOpacity = 0.8
        //opacity while shimmer
        facebookShimmeringLayer.shimmeringAnimationOpacity = 1.0
        
        facebookShimmeringLayer.shimmeringBeginTime = 2
        facebookShimmeringLayer.shimmeringSpeed = 3000
        facebookShimmeringLayer.shimmeringPauseDuration = 2

        facebookShimmeringLayer.shimmeringHighlightLength = 0.1
    }
    
    
//    self.nameShimmeringView!.addSubview(nameTitleLabel)
//    self.ageShimmeringView!.addSubview(ageRangeTitle)
//
//    func configureShimmerViews() {
//        let nameShimmeringView = self.nameShimmeringView!
//        let ageShimmeringView = self.ageShimmeringView!
//
//        nameShimmeringView.contentView = self.nameLabel!
//        nameShimmeringView.isShimmering = false
//
//        nameShimmeringView.shimmeringPauseDuration = 0.6
//        nameShimmeringView.shimmeringSpeed = 100
//        nameShimmeringView.shimmeringOpacity = 0
//        nameShimmeringView.shimmeringAnimationOpacity = 0.5
//
//        let when = DispatchTime.now() + 0.6
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            ageShimmeringView.shimmeringOpacity = 0
//            ageShimmeringView.contentView = self.ageRangeLabel!
//            ageShimmeringView.isShimmering = false
//            ageShimmeringView.shimmeringPauseDuration = 0.6
//            ageShimmeringView.shimmeringSpeed = 100
//            ageShimmeringView.shimmeringAnimationOpacity = 0.5
//        }
//    }
    
    var chooseUnlinkRing: CAShapeLayer?
    var chooseUnlinkCheck: UIImageView?
    
    func addChooseUnlinkSelector() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 10/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: cR, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        let cLayer = CAShapeLayer()
        cLayer.path = circularPath.cgPath
        cLayer.fillColor = UIColor.clear.cgColor
        cLayer.lineWidth = 1
        cLayer.strokeColor = textGray.cgColor
        cLayer.opacity = 1
        
        let space = (8 / 414) * sWidth
        let cX = self.unlinkCopyTextView!.frame.minX - space - cR
        let cY = self.unlinkCopyTextView!.center.y
        
        let cPoint = CGPoint(x: cX, y: cY)
        cLayer.position = cPoint
        self.chooseUnlinkRing = cLayer
        self.view.layer.addSublayer(cLayer)
        
        self.chooseUnlinkRing?.position = cPoint
        
        //add checkmark
        let cW = cR * 2
        let cH = cW
        let cX2 = 0 as CGFloat
        let cY2 = 0 as CGFloat
        
        let cFrame = CGRect(x: cX2, y: cY2, width: cW, height: cH)
        let checkView = UIImageView(frame: cFrame)
        checkView.contentMode = .center
        checkView.image = UIImage(named: "checkMark")
        self.chooseUnlinkCheck = checkView
        self.chooseUnlinkCheck!.layer.position = cPoint
        
        self.view.addSubview(chooseUnlinkCheck!)
        self.chooseUnlinkCheck!.isHidden = true
    }
    
    var unlinkCopyTextView: UITextView?
    var unlinkTapFrame: UIView?
    var unlinkDescription: UITextView?
    
    //ATTN: PUSH THIS VIEW ON FB ERROR VIEW
    let textGray = UIColor(displayP3Red: 153/255, green: 153/255, blue: 153/255, alpha: 1)

    func addUnlinkCheckText() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        self.superInsetHeight = superInsetHeight
        let tWidthRatio = 220 / 414 as CGFloat
        
        let tFontSize = 14.0 as CGFloat
        let tWidth = 220 as CGFloat

        let unlinkCopy = "Unlink Facebook After Verification"
        
        let textFieldCGSize = self.sizeOfCopy(string: unlinkCopy, constrainedToWidth: Double(tWidth), fontName: "AvenirNext-Regular", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let radiusRatio = 10/414 as CGFloat
        let cR = radiusRatio * sWidth
        let space = (8 / 414) * sWidth
        let tX = (sWidth - tWidth) * 0.5
        let tY = (self.fbLoginButton!.frame.maxY + (2 * gridUnitHeight)) - (0.5 * tH)

//        let tX = self.chooseUnlinkRing!.position.x + cR + space
//        let tY = (self.fbLoginButton!.frame.maxY + gridUnitHeight) - (0.5 * tH)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let unlinkCopyTextView = UITextView(frame: tFrame)
        
        unlinkCopyTextView.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)
        
        //
        self.view.addSubview(unlinkCopyTextView)
        
        self.unlinkCopyTextView = unlinkCopyTextView
        self.unlinkCopyTextView!.text = unlinkCopy
        self.unlinkCopyTextView!.textAlignment = .center
        self.unlinkCopyTextView!.isEditable = false
        self.unlinkCopyTextView!.isScrollEnabled = false
        self.unlinkCopyTextView!.isSelectable = false
        self.unlinkCopyTextView!.textContainer.lineFragmentPadding = 0
        self.unlinkCopyTextView!.textContainerInset = .zero
        self.unlinkCopyTextView!.textColor = textGray
        self.unlinkCopyTextView!.backgroundColor = UIColor.clear
        self.unlinkCopyTextView!.alpha = 1

        //add descText
//        let dW = (234 / 414) * sWidth as CGFloat
        let dW = 234 as CGFloat

        let dH = tH
        let dX = 0 as CGFloat
        let dY = self.unlinkCopyTextView!.frame.maxY - 2 as CGFloat
        let dFrame = CGRect(x: dX, y: dY, width: dW, height: dH)
        let dView = UITextView(frame: dFrame)

        dView.font = UIFont(name: "AvenirNext-UltraLight", size: tFontSize)!
        
        //
        self.view.addSubview(dView)
        
        self.unlinkDescription = dView
        self.unlinkDescription!.layer.position = CGPoint(x: self.unlinkCopyTextView!.center.x, y: dY + (0.5 * tH))
        self.unlinkDescription!.text = "Worried About Linking Social Media?"
        self.unlinkDescription!.textAlignment = .center
        self.unlinkDescription!.isEditable = false
        self.unlinkDescription!.isScrollEnabled = false
        self.unlinkDescription!.isSelectable = false
        self.unlinkDescription!.textContainer.lineFragmentPadding = 0
        self.unlinkDescription!.textContainerInset = .zero
        self.unlinkDescription!.textColor = textGray
        self.unlinkDescription!.backgroundColor = UIColor.clear
        self.unlinkDescription!.alpha = 1
        
        //add tapFrame
        let tapWidth = (290 / 414) * sWidth as CGFloat
        let tapFrame = CGRect(x: ((sWidth - tapWidth) * 0.5) - cR, y: self.unlinkCopyTextView!.frame.minY, width: tapWidth, height: tH * 2)
        let tapView = UIView(frame: tapFrame)
        tapView.backgroundColor = UIColor.clear
//        tapView.layer.borderWidth = 1
//        tapView.layer.borderColor = UIColor.red.cgColor
        
        self.unlinkTapFrame = tapView
        self.view.addSubview(tapView)
        
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.toggleFBUnlink_tapCheckUnlink))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.unlinkTapFrame!.addGestureRecognizer(tapGesture)
        self.unlinkTapFrame!.isUserInteractionEnabled = true
    }

    var didCheckFBUnlink = false
    
    @objc func toggleFBUnlink_tapCheckUnlink(_ sender: UITapGestureRecognizer) {

        //print("didCheckFBUnlinkVerification")
        
        self.arrestAutoScroll = true
        
        if self.didCheckFBUnlink == false {
            self.animateGrowRing1()
            self.unlinkCopyTextView?.alpha = 1
            self.unlinkBarButton?.setTitle("Unlink Facebook and Continue", for: .normal)
            self.chooseUnlinkRing?.strokeColor = UIColor.white.cgColor
            self.unlinkCopyTextView?.textColor = UIColor.white
            self.didCheckFBUnlink = true
            self.moreInfoScrollView?.scrollRectToVisible(self.scrollFrame2!.frame, animated: true)
            let when = DispatchTime.now() + 0.35
            DispatchQueue.main.asyncAfter(deadline: when) {
               self.pageControl?.currentPage = 1
            }
            
        } else {
            self.chooseUnlinkCheck!.isHidden = true
            self.chooseUnlinkRing?.strokeColor = textGray.cgColor
            self.unlinkCopyTextView?.textColor = textGray
            self.unlinkBarButton?.setTitle("Continue", for: .normal)
            self.didCheckFBUnlink = false
            self.moreInfoScrollView?.scrollRectToVisible(self.scrollFrame1!.frame, animated: true)
            let when = DispatchTime.now() + 0.35
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.pageControl?.currentPage = 0
                
                //reset animation loop
                self.willLoopPages = false //to reset the loop timer to 1.5
                self.arrestAutoScroll = false
                self.animatePagesHint()
            }
        }
    }
    
    func animateGrowRing1() {
        let ringLayer = self.chooseUnlinkRing!
        let checkMark = self.chooseUnlinkCheck!
        
        ringLayer.isHidden = false
        checkMark.isHidden = false
        
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.0
        grow.toValue = 1.0
        grow.duration = 0.3
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
//        ringLayer.add(grow, forKey: "grow")
        checkMark.layer.add(grow, forKey: "grow2")
    }
    
    func addNameAgeHeaders() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = nW
        var nY = 4 * superInsetHeight
        //new yPos with hide Label
//            nY = (4 * superInsetHeight) - gridUnitHeight
        
        let aW = nW
        let aH = nH
        let aX = 5 * gridUnitWidth
        let aY = nY
        
        //1
        let nameLabelFrame = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameTitleLabel = UILabel(frame: nameLabelFrame)
        //
        nameTitleLabel.text = "First Name"
        nameTitleLabel.font = UIFont(name: "Avenir-Medium", size: 14)
        nameTitleLabel.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        nameTitleLabel.textAlignment = .center

        //2
        let ageRangeFrame = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageRangeTitle = UILabel(frame: ageRangeFrame)
        //
        ageRangeTitle.text = "Age Range"
        ageRangeTitle.font = UIFont(name: "Avenir-Medium", size: 14)
        ageRangeTitle.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        ageRangeTitle.textAlignment = .center
        
        self.view.addSubview(nameTitleLabel)
        self.view.addSubview(ageRangeTitle)
        
        self.nameSubtitle = nameTitleLabel
        self.ageSubtitle = ageRangeTitle
    }
    
    func addNameAgeHeaders_noFB() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = nW
        var nY = 4 * superInsetHeight
        
        //1
        let nameLabelFrame = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameTitleLabel = UILabel(frame: nameLabelFrame)
        //
        nameTitleLabel.text = "First Name"
        nameTitleLabel.font = UIFont(name: "Avenir-Medium", size: 14)
        nameTitleLabel.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        nameTitleLabel.textAlignment = .center
        
        self.view.addSubview(nameTitleLabel)
        self.nameSubtitle = nameTitleLabel

        let centerX = sWidth * 0.5
        let centerY = nY + (0.5 * nH)
        let centerPoint = CGPoint(x: centerX, y: centerY)
        
        self.nameSubtitle!.layer.position = centerPoint
        
        //Age Header
        let aW = nW
        let aH = nH
        let aX = 5 * gridUnitWidth
        let aY = nY
        
        //2
        let ageRangeFrame = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageRangeTitle = UILabel(frame: ageRangeFrame)
        //
        ageRangeTitle.text = "Age Range"
        ageRangeTitle.font = UIFont(name: "Avenir-Medium", size: 14)
        ageRangeTitle.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        ageRangeTitle.textAlignment = .center
        
        self.view.addSubview(ageRangeTitle)
        self.ageSubtitle = ageRangeTitle
        self.ageSubtitle?.alpha = 0
        self.ageSubtitle?.isHidden = true
    }
    
    var nameVisibilityLabel: UITextView?
    
    func addNameVisibilityLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
       //
        let titleString = "Visible on Profile"
        let fontName = "Avenir-Light"
        let fontSize = 12.0 as CGFloat
        
        let vW = 4 * gridUnitWidth
        let cgSizeOfCopy = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(vW), fontName: fontName, fontSize: fontSize)
        let vH = ceil(cgSizeOfCopy.height)
        let vX = gridUnitWidth
        let vY = self.nameSubtitle!.frame.maxY
        
        //
        let tFrame = CGRect(x: vX, y: vY, width: vW, height: vH)
        let tView = UITextView(frame: tFrame)
        
        tView.font = UIFont(name: fontName, size: fontSize)!
        tView.text = titleString
        //
        self.view.addSubview(tView)
        self.nameVisibilityLabel = tView
        
        self.nameVisibilityLabel!.textAlignment = .center
        self.nameVisibilityLabel!.isEditable = false
        self.nameVisibilityLabel!.isScrollEnabled = false
        self.nameVisibilityLabel!.isSelectable = false
        self.nameVisibilityLabel!.textContainer.lineFragmentPadding = 0
        self.nameVisibilityLabel!.textContainerInset = .zero
        self.nameVisibilityLabel!.textColor = UIColor(displayP3Red: 28/255, green: 28/255, blue: 28/255, alpha: 1)
        self.nameVisibilityLabel!.backgroundColor = UIColor.clear
    }
    
    var nameShimmeringView: FBShimmeringView?
    var ageShimmeringView: FBShimmeringView?
    var nameLabel: UILabel?
    var ageRangeLabel: UILabel?
    var nameSubtitle: UILabel?
    var ageSubtitle: UILabel?
    
    func addShimmerViews() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
    
        //name label props
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = nW
        var nY = 4 * superInsetHeight + gridUnitHeight
        
        //name label props
        let aW = nW
        let aH = nH
        let aX = 5 * gridUnitWidth
        let aY = nY
        
        //1
        let nF = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameShimmerView = FBShimmeringView(frame: nF)
        //
    
        //2
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageShimmerView = FBShimmeringView(frame: aF)
        //
        
        self.view.addSubview(nameShimmerView)
        self.view.addSubview(ageShimmerView)
        
        self.ageShimmeringView = ageShimmerView
        self.nameShimmeringView = nameShimmerView
    }
    
    func addShimmerViews_noFB() {
        //Note: Name label is Screen-Center && Age Range is Hidden
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //name label props
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = nW
        var nY = 4 * superInsetHeight + gridUnitHeight

        //1
        let nF = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameShimmerView = FBShimmeringView(frame: nF)
        //
  
        self.view.addSubview(nameShimmerView)
        self.nameShimmeringView = nameShimmerView
        
        //
        let centerX = sWidth * 0.5
        let centerY = nY + (0.5 * nH)
        let centerPoint = CGPoint(x: centerX, y: centerY)
        
        self.nameShimmeringView!.layer.position = centerPoint
        
        //For name
        //name label props
        let aW = nW
        let aH = nH
        let aX = 5 * gridUnitWidth
        let aY = nY
        
        //2
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageShimmerView = FBShimmeringView(frame: aF)
        //
        
        self.view.addSubview(ageShimmerView)
        self.ageShimmeringView = ageShimmerView
        self.ageShimmeringView!.isHidden = true
    }
  
    func addNameAgeLabels() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = 0 as CGFloat
        let nY = 0 as CGFloat
//        let nX = nW
//        let nY = 4 * superInsetHeight
        
        let aW = nW
        let aH = nH
        let aX = 0 as CGFloat
        let aY = 0 as CGFloat
        
        //1
        let nameLabelFrame = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameTitleLabel = UILabel(frame: nameLabelFrame)
        
        //
        nameTitleLabel.text = "First Name"
        nameTitleLabel.font = UIFont(name: "Avenir-Light", size: 18)
//        nameTitleLabel.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        nameTitleLabel.textColor = textGray
        nameTitleLabel.textAlignment = .center
        nameTitleLabel.adjustsFontSizeToFitWidth = true

        //2
        let ageRangeFrame = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageRangeTitle = UILabel(frame: ageRangeFrame)
        //
        ageRangeTitle.text = "Age Range"
        ageRangeTitle.font = UIFont(name: "Avenir-Light", size: 18)
//        ageRangeTitle.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        ageRangeTitle.textColor = textGray
        ageRangeTitle.textAlignment = .center
        ageRangeTitle.adjustsFontSizeToFitWidth = true
        
        self.nameShimmeringView!.addSubview(nameTitleLabel)
        self.ageShimmeringView!.addSubview(ageRangeTitle)
        
        self.nameLabel = nameTitleLabel
        self.ageRangeLabel = ageRangeTitle
    }

    func addNameAgeLabels_noFB() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let nW = 2 * gridUnitWidth
        let nH = gridUnitHeight
        let nX = 0 as CGFloat
        let nY = 0 as CGFloat
        //        let nX = nW
        //        let nY = 4 * superInsetHeight
        
        //1
        let nameLabelFrame = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameTitleLabel = UILabel(frame: nameLabelFrame)
        
        //
        nameTitleLabel.text = "First Name"
        nameTitleLabel.font = UIFont(name: "Avenir-Light", size: 18)
        //        nameTitleLabel.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        nameTitleLabel.textColor = textGray
        nameTitleLabel.textAlignment = .center
        nameTitleLabel.adjustsFontSizeToFitWidth = true

        self.nameShimmeringView!.addSubview(nameTitleLabel)
        self.nameLabel = nameTitleLabel
        
        //2
        let aW = nW
        let aH = nH
        let aX = 0 as CGFloat
        let aY = 0 as CGFloat
        
        let ageRangeFrame = CGRect(x: aX, y: aY, width: aW, height: aH)
        let ageRangeTitle = UILabel(frame: ageRangeFrame)
        //
        ageRangeTitle.text = "Age Range"
        ageRangeTitle.font = UIFont(name: "Avenir-Light", size: 18)
        //        ageRangeTitle.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.5, alpha: 1)
        ageRangeTitle.textColor = textGray
        ageRangeTitle.textAlignment = .center
        ageRangeTitle.adjustsFontSizeToFitWidth = true
        
        self.ageShimmeringView!.addSubview(ageRangeTitle)
        self.ageRangeLabel = ageRangeTitle
        self.ageRangeLabel!.isHidden = true
        self.ageRangeLabel!.alpha = 0
    }
    
    func addShowHideNameButton() {
        //method called after FBMkRequest
        let gridUnitX = 45.5 / 414 as CGFloat
        let sWidth = UIScreen.main.bounds.width
        let gridUnitWidth = gridUnitX * sWidth

        let buttonWidth = 50 as CGFloat
        let bH = 20 as CGFloat
        let bX = (gridUnitWidth * 3) - (0.5 * buttonWidth)
        let bY = self.nameShimmeringView!.frame.maxY + 2 as CGFloat
        
        let bF = CGRect(x: bX, y: bY, width: buttonWidth, height: bH)
        let bView = UIButton(frame: bF)
        
        bView.setTitle("Hide", for: .normal)
        bView.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)!
        bView.setTitleColor(hyperBlue, for: .normal)
        
        self.view.addSubview(bView)
        self.showHideButton = bView

        showHideButton!.addTarget(self, action: #selector(TrustViewController.showHideName(_:)), for: .touchUpInside)
        showHideButton!.isUserInteractionEnabled = true
    }
    
    var showHideButton: UIButton?
    var nameIsHidden = false
    let hyperBlue = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
    var userFirstName: String?
    var userFirstInitial: String?
    
    @objc func showHideName(_ sender: UIButton) {
        //print("showHideName detected")
    
        if self.nameIsHidden == false {
            self.nameLabel!.text = userFirstInitial!
            self.nameIsHidden = true
            self.nameVisibilityLabel?.text = "Hidden"
            showHideButton!.setTitle("Show", for: .normal)

        } else {
            self.nameLabel!.text = userFirstName!
            self.nameIsHidden = false
            self.nameVisibilityLabel?.text = "Visible on Profile"
            showHideButton!.setTitle("Hide", for: .normal)
        }
    }
    
    func configureShimmerViews() {
        let nameShimmeringView = self.nameShimmeringView!
        let ageShimmeringView = self.ageShimmeringView!
        
        nameShimmeringView.contentView = self.nameLabel!
        nameShimmeringView.isShimmering = false
        
        nameShimmeringView.shimmeringPauseDuration = 0.6
        nameShimmeringView.shimmeringSpeed = 100
        nameShimmeringView.shimmeringOpacity = 0
        nameShimmeringView.shimmeringAnimationOpacity = 0.5
        
        let when = DispatchTime.now() + 0.6
        DispatchQueue.main.asyncAfter(deadline: when) {
            ageShimmeringView.shimmeringOpacity = 0
            ageShimmeringView.contentView = self.ageRangeLabel!
            ageShimmeringView.isShimmering = false
            ageShimmeringView.shimmeringPauseDuration = 0.6
            ageShimmeringView.shimmeringSpeed = 100
            ageShimmeringView.shimmeringAnimationOpacity = 0.5
        }
    }

    func addUnlinkButtonView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = widthRatio * sWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
//        let bY = sHeight - superInsetHeight - bH
        var bY = sHeight - superInsetHeight
        if let bottomPadding = self.bottomPadding {
            bY = sHeight - superInsetHeight - bottomPadding
        }
        
        
        let bBarFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
        let unlinkBarButton = UIButton(frame: bBarFrame)
        
//        let bGreen = 74/255 as CGFloat
        
        unlinkBarButton.backgroundColor = UIColor.clear
        unlinkBarButton.layer.cornerRadius = 12
        unlinkBarButton.layer.masksToBounds = true
        unlinkBarButton.layer.borderWidth = 0.5
        unlinkBarButton.layer.borderColor = textGray.cgColor
        unlinkBarButton.layer.opacity = 0.5
        unlinkBarButton.setTitle("Continue", for: .normal)
        
        self.unlinkBarButton = unlinkBarButton
        
        self.unlinkBarButton!.setTitle("Continue", for: .normal)
//        self.unlinkBarButton!.titleLabel!.font = UIFont(name: "AvenirNext-UltraLight", size: 14.0)!
        self.unlinkBarButton!.titleLabel!.font = UIFont(name: "Avenir-Light", size: 14.0)!

        self.unlinkBarButton!.setTitleColor(textGray, for: .normal)
        
        self.view.addSubview(unlinkBarButton)

        
        //
//        let bBarFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
//        let unlinkBarButton = UIImageView(frame: bBarFrame)
////        unlinkBarButton.image = UIImage(named: "unlinkBarButtonDisabled")
//        unlinkBarButton.image = UIImage(named: "unlinkBarButtonEnabled")!
//
//        //
//        self.view.addSubview(unlinkBarButton)
//        self.unlinkBarButton = unlinkBarButton
//
        //add tap
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.trustComplete(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.unlinkBarButton!.addGestureRecognizer(tapGesture)
        self.unlinkBarButton!.isUserInteractionEnabled = false
    }
    
    func animateunlinkBarButtonEnabled() {
        let unlinkBarButtonView = self.unlinkBarButton!
        let when = DispatchTime.now() + 0.3
        DispatchQueue.main.asyncAfter(deadline: when) {
            //change orange to blue
            UIView.transition(with: unlinkBarButtonView, duration: 0.4, options: .transitionCrossDissolve, animations: {
//                unlinkBarButtonView.image = UIImage(named: "unlinkBarButtonEnabled")
            }, completion: nil)
        }
    }
    
    func addunlinkFBTextView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 12.0 as CGFloat
        var tWidth = 330 as CGFloat
        if sWidth > 350 {
            tWidth = 330 as CGFloat
            tFontSize = 12.0 as CGFloat
        } else {
            tWidth = 278 as CGFloat
            tFontSize = 10.0 as CGFloat
        }
        
        let privacyText = "To ensure privacy, your Facebook profile will be permanently unlinked from Ximo following this step."
        let textFieldCGSize = self.sizeOfCopyLarge(string: privacyText, constrainedToWidth: Double(tWidth))
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
//        let tY = self.unlinkBarButton!.frame.maxY + (gridUnitHeight * 0.5)
        let tY = 0 as CGFloat

        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let tView = UITextView(frame: tFrame)

        tView.font = UIFont(name: "Avenir-Light", size: tFontSize)

        //configure text

        let boldT = "permanently unlinked"
        let regT2 = "To ensure privacy, your Facebook profile will be"
        let regT3 = "from Ximo following this step."

        let range = (privacyText as NSString).range(of: boldT)
        let range2 = (privacyText as NSString).range(of: regT2)
        let range3 = (privacyText as NSString).range(of: regT3)

        let attributedString = NSMutableAttributedString(string: privacyText)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: range2)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: range3)

        tView.attributedText = attributedString

        //
//        self.view.addSubview(tView)
        self.scrollFrame2!.addSubview(tView)
        
        self.unlinkFBTextView = tView
        self.unlinkFBTextView!.textAlignment = .center
        self.unlinkFBTextView!.isEditable = false
        self.unlinkFBTextView!.isScrollEnabled = false
        self.unlinkFBTextView!.isSelectable = false
        self.unlinkFBTextView!.textContainer.lineFragmentPadding = 0
        self.unlinkFBTextView!.textContainerInset = .zero
        self.unlinkFBTextView!.textColor = UIColor.white
        self.unlinkFBTextView!.backgroundColor = UIColor.clear
    }
    
    var verifyUnlinkInfoButton: UILabel!
    
    var moreInfoScrollView: UIScrollView?
    
    func addMoreInfoScrollView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //set view properties
//        let scrollWidth = sWidth - (2 * gridUnitWidth)
        let scrollWidth = sWidth
        let scrollHeight = superInsetHeight
        let scrollX = (sWidth - scrollWidth) * 0.5
        var scrollY = sHeight - scrollHeight
        if let bottomPadding = self.bottomPadding {
            scrollY = sHeight - scrollHeight - bottomPadding
        }
        
        //initialize view
        let scrollFrame = CGRect(x: scrollX, y: scrollY, width: scrollWidth, height: scrollHeight)
        let infoScrollView = UIScrollView(frame: scrollFrame)
        infoScrollView.contentSize = CGSize(width: 3 * scrollWidth, height: scrollHeight)
        infoScrollView.backgroundColor = UIColor.clear
        
        self.view.addSubview(infoScrollView)
        self.moreInfoScrollView = infoScrollView
        self.moreInfoScrollView!.delegate = self
        self.moreInfoScrollView!.isPagingEnabled = true
        self.moreInfoScrollView!.showsHorizontalScrollIndicator = false
    }
    
    var scrollFrame1: UIView?
    var scrollFrame2: UIView?
    var scrollFrame3: UIView?

    func addScrollViewPages() {
        
        let scrollW = self.moreInfoScrollView!.frame.width
        
        for i in 0...2 {
            //print("iii is,", i)
            let pageView = UIView(frame: CGRect(x: CGFloat(i) * scrollW, y: 0, width: scrollW, height: self.moreInfoScrollView!.frame.height))
            
            if i == 0 {
                pageView.backgroundColor = UIColor.clear
                self.scrollFrame1 = pageView
            
            } else if i == 1 {
                pageView.backgroundColor = UIColor.clear
                self.scrollFrame2 = pageView

            } else if i == 2 {
                pageView.backgroundColor = UIColor.clear
                self.scrollFrame3 = pageView
            }
            
            self.moreInfoScrollView!.addSubview(pageView)
        }
    }
    
    var pageControl: UIPageControl?
    var numberOfPages = 3
    var currentPageIndex = 0 
    
    func addPageControl() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
//        let pageControl = UIPageControl()
//        pageControl.numberOfPages = self.numberOfPages
//
//        let pageW = pageControl.frame.width
//        let pageH = pageControl.frame.height
//        let pageX = (sWidth - pageW) * 0.5
//        let pageY = sHeight - (pageH)
//
//        pageControl.frame = CGRect(x: pageX, y: pageY, width: pageW, height: pageH)
//        pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
//        pageControl.backgroundColor = UIColor.green
//
//        self.view!.addSubview(pageControl)
//        self.pageControl = pageControl
        
        let pageControl = UIPageControl()
        pageControl.numberOfPages = self.numberOfPages
        
        let pageW = sWidth
        let pageH = 10 as CGFloat
        //print("pageH is,", pageH)
        let pageX = (sWidth - pageW) * 0.50
        let pageY = sHeight - pageH
        
        //        let pageFrame = CGRect(x: pageX, y: pageY, width: pageW, height: pageH)
        //        pageControl.backgroundColor = UIColor.red
        pageControl.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        
        self.view!.addSubview(pageControl)
        self.pageControl = pageControl
        
        //y position relative to textButton
//        let bottomDoubleSpace = (sHeight - self.verifyUnlinkInfoButton!.frame.maxY)
//        //print("bottomDoubleSpace is,", bottomDoubleSpace)
//        let marginError = 2 as CGFloat
//        let yPos = self.verifyUnlinkInfoButton!.frame.maxY + (0.5 * bottomDoubleSpace) - marginError
//        self.pageControl!.layer.position = CGPoint(x: sWidth * 0.5, y: yPos)
        
        //y position relative to screenBottom
        var yPos = sHeight - (0.5 * gridUnitHeight)
        if let bottomPadding = self.bottomPadding {
            yPos = sHeight - (0.5 * gridUnitHeight) - bottomPadding
        }
        
        let marginError = 2 as CGFloat
        self.pageControl!.layer.position = CGPoint(x: sWidth * 0.5, y: yPos)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        // Switch the indicator when more than 50% of the previous/next page is visible.
        //print("scrollViewDidEndDecelerating ")
        
        let pageWidth = scrollView.frame.width
        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        pageControl?.currentPage = Int(page)
        self.currentPageIndex = Int(page)
        
//        if self.currentPageIndex == 0 {
//            //start over
//            self.arrestAutoScroll = true
//            self.animateFromPage1()
//
//        } else if self.currentPageIndex == 1 {
//            //at page 2
//            self.arrestAutoScroll = true
//            self.animateFromPage2()
//
//        } else if self.currentPageIndex == 2 {
//            //at page 3
//            self.arrestAutoScroll = true
//            self.animateFromPage3()
//        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.arrestAutoScroll = true
    }
    
    
    var infoView1: UILabel?
    var infoView2: UILabel?
    var infoView3: UILabel?
    
    func addPageControlSubviews() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var bW = 200 as CGFloat
//        var buttonString = "How Can I Verify that This is True?"
        var buttonString = "How Can I Verify that Facebook Was Unlinked"
        var tapGesture = UITapGestureRecognizer()
     
        //adding button wrt scrollView
        //info for page control
//        let yPos = sHeight - (0.5 * gridUnitHeight)
//        let marginError = 2 as CGFloat
//        self.pageControl!.layer.position = CGPoint(x: sWidth * 0.5, y: yPos)
    
        for i in 0...2 {
            
            if i == 0 {
                tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.veriVsFB(_:)))
                tapGesture.delegate = self
                tapGesture.numberOfTapsRequired = 1
                tapGesture.numberOfTouchesRequired = 1
                
                bW = 300 as CGFloat
                buttonString = "How is this Different than Signing Up With Facebook?"

            } else if i == 1 {
                tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.howCanIKnow(_:)))
                tapGesture.delegate = self
                tapGesture.numberOfTapsRequired = 1
                tapGesture.numberOfTouchesRequired = 1
                
                bW = 250 as CGFloat
                buttonString = "How Can I Verify that Facebook Was Unlinked?"
                
            } else if i == 2 {
  
                //Default Trust UI
                if self.ref_AuthTracker.emailAuthSource == true && self.ref_AuthTracker.phoneAuthSource == false {
                    bW = 200 as CGFloat

                    buttonString = "I Don't Have a Facebook Account"
                    
                    tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.noFacebook(_:)))
                    tapGesture.delegate = self
                    tapGesture.numberOfTapsRequired = 1
                    tapGesture.numberOfTouchesRequired = 1
                }
                
                //Subtle Manual Enter Info UI
                if self.ref_AuthTracker.emailAuthSource == false && self.ref_AuthTracker.phoneAuthSource == true {
                    bW = 250 as CGFloat
                    
//                    buttonString = "I Want to Fill in My Information Manually"
                    buttonString = "Skip this Step"

                    tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.fillInfoManually(_:)))
                    tapGesture.delegate = self
                    tapGesture.numberOfTapsRequired = 1
                    tapGesture.numberOfTouchesRequired = 1
                }
            }
            
            var buttonCGSize = self.sizeOfCopyLarge(string: buttonString, constrainedToWidth: Double(bW))
            var bH = ceil(buttonCGSize.height)
            var bFontSize = 12 as CGFloat
            
            let bX = (sWidth - bW) * 0.5
            let bYPaddingError = 2 as CGFloat
    //        let bY = self.unlinkFBTextView!.frame.maxY + (0.3 * gridUnitHeight) - bYPaddingError
//            let bY = (2 * gridUnitHeight)
            let posError = 2 as CGFloat
            var bY = self.scrollFrame1!.frame.height - gridUnitHeight - bH + posError
//            if let bottomPadding = self.bottomPadding {
//                bY = self.scrollFrame1!.frame.height - gridUnitHeight - bH + posError - bottomPadding
//            }
            
            let buttonFont = UIFont(name: "Avenir-Light", size: bFontSize)!
            let bColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
            
            let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
            let infoButton = UILabel(frame: bFrame)
            infoButton.text = buttonString
            infoButton.font = buttonFont
            infoButton.textColor = bColor
            infoButton.textAlignment = .center
            
            //
            self.scrollFrame1?.addSubview(infoButton)
            self.verifyUnlinkInfoButton = infoButton
        
            //add gesture
            //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
            infoButton.addGestureRecognizer(tapGesture)
            infoButton.isUserInteractionEnabled = true

            if i == 0 {
                self.scrollFrame1?.addSubview(infoButton)
                self.infoView1 = infoButton
            } else if i == 1 {
                self.infoView2 = infoButton
                self.scrollFrame2?.addSubview(infoButton)
            } else if i == 2 {
                self.infoView3 = infoButton
                self.scrollFrame3?.addSubview(infoButton)
            }
            
        }
        //        if sWidth > 350 {
        //            bW = 180 as CGFloat
        //        } else {
        //            bW = 150 as CGFloat
        //            bFontSize = 10 as CGFloat
        //            buttonCGSize = self.sizeOfCopySmall(string: buttonString, constrainedToWidth: Double(bW))
        //            bH = ceil(buttonCGSize.height)
        //        }
    }
    
    var willEnterInfoManually = false
    
    @objc func fillInfoManually(_ sender: UITapGestureRecognizer) {
        
        //1. guard selfDidCompleteFacebookVerification == false
        //2. segue to My Info iff selfDidCompleteFacebookVerification == false
        //ALWAYS DISABLE MANUAL INFO IF USER VERIFIES FB -> TRANSFORM TO I DONT HAVE FB ACCT
        self.arrestAutoScroll = true
        self.willEnterInfoManually = true
        self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)

    }
    
    var didSetDeniedPermissionsUI = false
    
    func facebookLogin() {
        let readPermissions = ["public_profile", "user_friends"]
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: readPermissions, from: self) { (result, error) in
            if let error = error {
                //print("login failed with error: \(error)")
                
            } else if (result?.isCancelled)! {
                //print("login cancelled")
                
            } else if let result = result {
                //get token
                let accessToken = result.token
                //print("making graph request")
//                self.makeRequest()
            }
            
            if let result = result {
                //print("FB loginManager result is,", result)
                
                if let declinePermissions = result.declinedPermissions {
                    if declinePermissions.isEmpty {
                        //print("Full access granted")
                        self.removeFriendListDeniedUI()
                        self.didSetDeniedPermissionsUI = false
                        self.makeRequest()

                    } else if declinePermissions.contains("user_friends") {
                        //print("ATTN: User declined friends list access")
                        if self.didSetDeniedPermissionsUI  == false {
                            self.configureFriendsDeniedUI()
                            self.didSetDeniedPermissionsUI = true
                        }
                    }
                }
            }
        }
    
        loginManager.logOut()
    }
 
    //ATTENTION: APP WILL CRASH IF USER CHANGES FACEBOOK ACCOUNTS ON THE SAME DEVICE.
    //NEED TO MAKE SURE THAT APP DOES NOT CRASH WHEN USER CHANGES ACCOUNTS ON THE SAME DEVICE.
    //CALL LOGOUT TO RESOLVE?
    //ERROR CODE: ios facebook sdk 4.0 login error code 304
    //REFER TO LINK: https://stackoverflow.com/questions/29408299/ios-facebook-sdk-4-0-login-error-code-304
    
    var finalFirstName: String?
    var finalInitial: String?
    
    /*
    func makeRequest() {
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name, first_name, age_range, friends"])
        graphRequest?.start(completionHandler: { (_, response, error) in
            if let error = error {
                //print("Error fetching Facebook user data: \(error)")
            } else {
                //result is saved into dictionary before passed to completion handler
                let resultDictionary = response as? [String: Any]
                //print("\(resultDictionary!)")
                
                guard let id = resultDictionary!["id"] as? String else { return }
                //print("id is \(id)")
                
                let firstName = resultDictionary!["first_name"] as? String
                //print("first name is \(firstName!)")
                
                let ageRangeDictionary = resultDictionary!["age_range"] as? [String: Any]
                //print("age_range is \(ageRangeDictionary!)")
                let minAge = ageRangeDictionary!["min"] as? Int //this is your final age range
                //print("min age is \(minAge!)")
                self.userMinAge = minAge
                
                //self.animateunlinkBarButtonEnabled()
                self.userFirstName = firstName
                self.userFirstInitial = String(firstName!.first!)
                
                self.finalFirstName = firstName
                self.finalInitial = String(firstName!.first!)
                
                let selfUID = Auth.auth().currentUser!.uid
                UserDefaults.standard.set(firstName, forKey: "\(selfUID)_myFullName")
//                UserDefaults.standard.set(String(firstName!.first!), forKey: "\(selfUID)_myInitial")
                
                if let myFullName = UserDefaults.standard.string(forKey: "\(selfUID)_myFullName") {
                    //print("USERDEFAULTS myFullName is,", myFullName)
                }
                
                //
                if let friendsArray = resultDictionary?["friends"] as? [String: Any] {
                    //print("friends array is \(friendsArray)")
                    let summary = friendsArray["summary"] as? [String: Any]
                    //print("\(summary!)")
                    let totalFriendCount = summary!["total_count"] as? Int //this is your final friend count
                    //print("total friend count is \(totalFriendCount!)")
                    
                    if let totalFriendCount = totalFriendCount {
                        if totalFriendCount < 100 {
                            //print("Threshold not reached - account invalid")
                            self.configureInvalidProfileUI()
                        } else {
                            
                            //print("Threshold attained - account is valid")
                            self.invalidCopyTextView?.isHidden = true
                            self.invalidCopyTextView?.removeFromSuperview()
                            self.configureNewValidProfileUI()
                            self.updateLabelValues(firstName: firstName!, ageRange: minAge!)
                            self.unlinkBarButton?.isHidden = false
                        }
                    }
                }
                //
            }
        })
    }
    */
    
    func makeRequest() {
        
        guard let selfUID = Auth.auth().currentUser?.uid else { return }

        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name, first_name, age_range, friends"])
        graphRequest?.start(completionHandler: { (_, response, error) in
            if let error = error {
                //print("Error fetching Facebook user data: \(error)")
            } else {
                //result is saved into dictionary before passed to completion handler
                let resultDictionary = response as? [String: Any]
                //print("\(resultDictionary!)")
                
                guard let id = resultDictionary!["id"] as? String else { return }
                //print("id is \(id)")
                
                if let ageRangeDictionary = resultDictionary!["age_range"] as? [String: Any] {
                    if let minAge = ageRangeDictionary["min"] as? Int {
                        //this is your final age range
                        self.userMinAge = minAge
//                        print("fbAgeRange,", minAge)

                    }
                }
                
                if let firstName = resultDictionary!["first_name"] as? String {
                    //self.animateunlinkBarButtonEnabled()
                    self.userFirstName = firstName
                    self.userFirstInitial = String(firstName.first!)
                    
                    self.finalFirstName = firstName
                    self.finalInitial = String(firstName.first!)
                    
                    UserDefaults.standard.set(firstName, forKey: "\(selfUID)_myFullName")
    //                UserDefaults.standard.set(String(firstName!.first!), forKey: "\(selfUID)_myInitial")
                }
                
                //CHECK
                if let myFullName = UserDefaults.standard.string(forKey: "\(selfUID)_myFullName") {
                    //print("USERDEFAULTS myFullName is,", myFullName)
                }
                
                //FBCondition
                var finalMinAge = 18
                if let minAgeFromFB = self.userMinAge {
                    finalMinAge = minAgeFromFB
                } else {
                    //ATTN: REMOVE MIN AGE CONDITION IF FB AGE RANGE APPROVED - OTHERWISE NEED TO READJUST UI TO EXCLUDE AGE RANGE
                    self.userMinAge = 18
                }
                
                if let friendsArray = resultDictionary?["friends"] as? [String: Any] {
                    //print("friends array is \(friendsArray)")
                    let summary = friendsArray["summary"] as? [String: Any]
                    //print("\(summary!)")
                    let totalFriendCount = summary!["total_count"] as? Int //this is your final friend count
                    //print("total friend count is \(totalFriendCount!)")
                    
                    if let totalFriendCount = totalFriendCount {
                        if totalFriendCount < 100 {
                            //print("Threshold not reached - account invalid")
                            self.configureInvalidProfileUI()
                        } else {
                            
                            //print("Threshold attained - account is valid")
                            self.invalidCopyTextView?.isHidden = true
                            self.invalidCopyTextView?.removeFromSuperview()
                            self.configureNewValidProfileUI()
                            self.convertPageView3()
                            self.updateLabelValues(firstName: self.finalFirstName!, ageRange: finalMinAge)
                            self.unlinkBarButton?.isHidden = false
                        }
                    }
                } else {
                    //NEW  //FBCondition
                    self.convertPageView3()
//                    print("finalMinAge,", finalMinAge)
                    self.updateLabelValues(firstName: self.finalFirstName!, ageRange: finalMinAge)  
                }
                //
            }
        })
    }
    
    func configureNewValidProfileUI() {
        //print("configureNewValidProfileUI")
        self.attentionSymbol?.isHidden = true
        self.invalidCopyTextView?.isHidden = true
        self.invalidCopyTextView?.removeFromSuperview()
        self.veriCopyTextView?.isHidden = false
        self.storageCopyTextView?.isHidden = false
    }
    
    let goGreen = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    
    func updateLabelValues(firstName: String, ageRange: Int) {
//        self.nameShimmeringView!.isShimmering = false
//        self.ageShimmeringView!.isShimmering = false
        
        self.skipButton?.removeFromSuperview()
        
        self.nameLabel!.text = firstName as String
        self.ageRangeLabel!.text = String("\(ageRange)" + "+")
        
        self.nameSubtitle!.isHidden = false
        self.ageSubtitle!.isHidden = false
        self.nameVisibilityLabel?.isHidden = false
        
//        self.unlinkBarButton!.isHidden = false
        self.unlinkBarButton?.layer.opacity = 1
        self.unlinkBarButton?.backgroundColor = goGreen
        self.unlinkBarButton!.setTitleColor(UIColor.white, for: .normal)
        self.unlinkBarButton!.layer.borderColor = UIColor.clear.cgColor
        self.unlinkBarButton!.layer.borderWidth = 0
        self.unlinkBarButton!.isUserInteractionEnabled = true
        
        self.nameLabel!.isHidden = false
        self.ageRangeLabel!.isHidden = false

        self.nameLabel!.textColor = UIColor.white
        self.ageRangeLabel!.textColor = UIColor.white
        
//        self.updateUserProfile(firstName: firstName, ageRange: ageRange)
//        self.addShowHideNameButton()
    }
    
    func updateUserProfile(firstName: String, ageRange: Int) {
        guard let user = Auth.auth().currentUser else { return }
        
        self.ref.child("users/\(user.uid)/userDisplayName").setValue(firstName)
        self.ref.child("users/\(user.uid)/userAgeRange").setValue(ageRange)
        
        /*
        //store user name on local drive
        UserDefaults.standard.set(self.userFirstName!, forKey: "nameVault")
        //retrieve user name from local drive
        if let clearName = UserDefaults.standard.string(forKey: "nameVault") {
            //print("clearName from vault is,", clearName)
        }
        if self.nameIsHidden == false {
            //user name is visible
            self.ref.child("users/\(user.uid)/userDisplayName").setValue(userFirstName!)
            self.ref.child("users/\(user.uid)/userAgeRange").setValue(ageRange)
            self.ref.child("users/\(user.uid)/clearName").setValue("")
            self.ref.child("users/\(user.uid)/nameVault").setValue("false")

        } else {
            //user name is hidden
            self.ref.child("users/\(user.uid)/userDisplayName").setValue(userFirstInitial!)
            self.ref.child("users/\(user.uid)/userAgeRange").setValue(ageRange)
            self.ref.child("users/\(user.uid)/clearName").setValue("")
            self.ref.child("users/\(user.uid)/nameVault").setValue("true")
        }
//        self.ref.child("users/\(user.uid)/userDisplayName").setValue(firstName)
//        self.ref.child("users/\(user.uid)/userAgeRange").setValue(ageRange)
         */
        
//        self.confirmProfileUpdate()
    }
    
    func convertPageView3() {
        //when user verifies with facebook, they should no longer have the option of manually using info
        
        //remove skip button
//        self.skipButton?.removeFromSuperview()
        
        //convert infoView3
        self.infoView3?.text = "I Don't Have a Facebook Account"

        var tapGesture = UITapGestureRecognizer()
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(TrustViewController.noFacebook(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        
        self.infoView3?.addGestureRecognizer(tapGesture)

    }
    
    var checkRingLayer: CAShapeLayer?
    var checkMarkView: UIImageView?
    
    func addCompletionRing() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 9/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: cR, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        let cLayer = CAShapeLayer()
        cLayer.path = circularPath.cgPath
        cLayer.fillColor = UIColor.clear.cgColor
        cLayer.lineWidth = 1
        cLayer.strokeColor = UIColor.white.cgColor
        
        let cPoint = CGPoint(x: gridUnitWidth * 0.5, y: gridUnitWidth * 0.5)
        cLayer.position = cPoint
        self.unlinkBarButton!.layer.addSublayer(cLayer)
        self.checkRingLayer = cLayer
        
        //add checkmark
        
        let cW = cR * 2
        let cH = cW
        let cX = (gridUnitWidth - cW) * 0.5
        let cY = cX
        
        let cFrame = CGRect(x: cX, y: cY, width: cW, height: cH)
        let checkView = UIImageView(frame: cFrame)
        checkView.contentMode = .center
        checkView.image = UIImage(named: "checkMark")
        self.checkMarkView = checkView

        self.unlinkBarButton!.addSubview(checkView)
    }
    
    func animateGrowRing2() {
        let ringLayer = self.checkRingLayer!
        let checkMark = self.checkMarkView!
        
        ringLayer.isHidden = false
        checkMark.isHidden = false
        
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.0
        grow.toValue = 1.0
        grow.duration = 0.3
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)

        ringLayer.add(grow, forKey: "grow")
        checkMark.layer.add(grow, forKey: "grow2")
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)
        }
    }
    
    //    self.activityIndicatorView!.startAnimating()
    //    self.activityIndicatorView!.stopAnimating()
    //    self.animateGrowRing()
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 9/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = (gridUnitWidth - aW) * 0.5
        let aY = aX
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.white
        activityIndicatorView.hidesWhenStopped = true
        
        self.unlinkBarButton!.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
    }
    
    //
    var didResolveUnlinkFB = false
    
    func unlinkFB() {
        
        let uid = Auth.auth().currentUser!.uid
        
        self.activityIndicatorView!.startAnimating()
        self.unlinkBarButton?.isUserInteractionEnabled = false
        
        let revokePermissionsRequest = FBSDKGraphRequest(graphPath: "me/permissions", parameters: nil, httpMethod: "DELETE")
        
        revokePermissionsRequest?.start(completionHandler: { (_, response, error) in
            //print("FBUNLINK RESPONSE,", response)
            //print(error)
            
            if error == nil {
                self.activityIndicatorView!.stopAnimating()
                
                //Attempt perform batch updates
                self.ref_AuthTracker.performBatchUpdates_Trust(withName: self.userFirstName!, withAgeRange: String(self.userMinAge!), didAuthFB: "true", didUnlinkFB: "true", didResolveUnlinkFB: "true")
                
                self.animateGrowRing2()
                
            } else {
                self.activityIndicatorView!.stopAnimating()

                //Attempt perform batch updates
                self.ref_AuthTracker.performBatchUpdates_Trust(withName: self.userFirstName!, withAgeRange: String(self.userMinAge!), didAuthFB: "true", didUnlinkFB: "true", didResolveUnlinkFB: "false")

                self.animateGrowRing2()
            }
        })
        //this code successfully unlinks app from facebook - need to present screen or signal that shows unlinking is successful to reassure user
    }
    
    func confirmProfileUpdate() {
        //add confirmation label later
    }
    
    //MARK: Action-Target Methods
    
    @objc func trustComplete(_ sender: UITapGestureRecognizer) {
        //print("trustComplete")
        //unlink FB
        //animate completion verification
        //segue to next screen
        
        let uid = Auth.auth().currentUser!.uid

        //Freeze UI
        self.unlinkDescription?.isUserInteractionEnabled = false
        self.unlinkTapFrame?.isUserInteractionEnabled = false
        self.unlinkBarButton?.isUserInteractionEnabled = false
        
        guard let displayName = self.userFirstName else { return }
        guard let ageRange = self.userMinAge else { return }
        var didUnlinkFb = "false"
        
        if self.didCheckFBUnlink == true {
            self.unlinkFB()
            didUnlinkFb = "true"
            //perform batch updates after unlink resolves
            
        } else {
//            self.ref.child("users/\(uid)/didResolveFBUnlink").setValue("nil")
            didUnlinkFb = "false"

            FBSDKLoginManager().logOut()
            
            //perform batch updates now
            self.ref_AuthTracker.performBatchUpdates_Trust(withName: self.userFirstName!, withAgeRange: String(ageRange), didAuthFB: "true", didUnlinkFB: didUnlinkFb, didResolveUnlinkFB: "nil")
            self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)
        }
        

    }
    
    //Logic:
    /*
     1. Any user who verifies with FB will no longer have the option to input information manually
     2.
 
 
     */
    
    func attemptPerformBatchUpdates() {
        
        //Case 1. User signs up with email
        if self.ref_AuthTracker.emailAuthSource == true && self.ref_AuthTracker.phoneAuthSource == false {
            
            //when user signs up with email, skipping should never be an option, therefore, user will have needed to verify FB to continue
        }
    }
    
    
    
    @objc func fbButton(_ sender: UITapGestureRecognizer) {
        //print("fbButton")
        self.arrestAutoScroll = true
        self.facebookLogin()
        self.nameShimmeringView!.isShimmering = false
        self.ageShimmeringView!.isShimmering = false
    }
    
    var withInfo = ""
    
//    if self.withInfo == "verifyUnlink" {
//    self.copyContainer.image = UIImage(named: "unlinkInfo")!
//    } else if self.withInfo == "whatFBInfo"  {
//    self.copyContainer.image = UIImage(named: "facebookInfo")!
//    } else if self.withInfo == "veriVSFB" {
//    self.copyContainer.image = UIImage(named: "verivsFB")!
//    } else if self.withInfo == "noFacebook" {
//    self.copyContainer.image = UIImage(named: "noFacebook")!
//    }
    
    @objc func infoButton(_ sender: UITapGestureRecognizer) {
        //print("infoButton")
        self.arrestAutoScroll = true
        self.withInfo = "whatFBInfo"
        self.performSegue(withIdentifier: "trustToCopy", sender: self)
    }
    
    @objc func howCanIKnow(_ sender: UITapGestureRecognizer) {
        //print("howCanIKnow")
        self.arrestAutoScroll = true
        self.withInfo = "verifyUnlink"
        self.performSegue(withIdentifier: "trustToCopy", sender: self)
    }
    
    @objc func veriVsFB(_ sender: UIGestureRecognizer) {
        //print("veriVsFB")
        self.arrestAutoScroll = true
        self.withInfo = "veriVSFB"
        self.performSegue(withIdentifier: "trustToCopy", sender: self)
    }
    
    @objc func noFacebook(_ sender: UIGestureRecognizer) {
        //print("noFacebook")
        self.arrestAutoScroll = true
        self.withInfo = "noFacebook"
        self.performSegue(withIdentifier: "trustToCopy", sender: self)
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.ref_AuthTracker.aRef = self
        
        if segue.identifier == "trustVCtoInfoVC" {
            let destinationController = segue.destination as! MyInfoViewController
//            destinationController.kbHeight = self.kbHeight!
            
            //SKIP to info
            if self.didSkipFacebookVerification == true || self.willEnterInfoManually == true {
                destinationController.shouldPresentNameView = true
            }
            
            destinationController.minAge = self.userMinAge
            
//            destinationController.userName = self.nameLabel!.text!
//            destinationController.userFirstName = self.finalFirstName!
//            destinationController.userInitial = self.finalInitial!
            
            destinationController.ref_AuthTracker = self.ref_AuthTracker
        }
    
        if segue.identifier == "trustToCopy" {
            let destinationController = segue.destination as! TrustInfoViewController
            destinationController.yesFBPermissions = self.yesFBPermissions
            destinationController.withInfo = self.withInfo
            destinationController.sourceController = "trustVC"
            destinationController.ref_AuthTracker = self.ref_AuthTracker
        }
    }
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
//    @objc func textTapped(recognizer: UITapGestureRecognizer) {
//
//        //print("textTapped")
//        var location = recognizer.location(in: self.view!)
//        location.x -= self.veriCopyTextView!.left
//        location.y -= self.veriCopyTextView!.top
//        let cPosition = layoutManager.characterIndex(for: location, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
//        let cChar = text[Range(NSRange(location: cPosition, length: 1), in: text)!]
//        //print(cChar)
//    }
    
}

//class textViewPoint: UITextView {
//
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//
//        guard let pos = closestPosition(to: point) else { return false }
//        //print("pos is,", pos)
//
//        guard let range = tokenizer.rangeEnclosingPosition(pos, with: .character, inDirection: UITextLayoutDirection.left.rawValue) else { return false }
//        //print("range is,", range)
//
//
//        let startIndex = offset(from: beginningOfDocument, to: range.start)
//        //print("startIndex is,", range)
//
//        return attributedText.attribute(NSAttributedStringKey.link, at: startIndex, effectiveRange: nil) != nil
//    }
//}


