//
//  LocationGetterViewController.swift
//  Juno
//
//  Created by Asaad on 2/9/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseStorage
import GeoFire

class LocationGetterViewController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    override func loadView() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        mainView.backgroundColor = UIColor.white
        self.view = mainView
        
        //add views
        self.addScreenTitleLabel()
        self.addEnableButton()
        self.addLocationCopy()
        self.addContinueButton()
        self.addLocationInfo()
        self.addCompletionRing()
        self.addActivityIndicator()
    }
    
    var ref: DatabaseReference!
    
    var ref_AuthTracker = AuthTracker()
    
    //create instance of CL location manager class
    
    let locationManager = CLLocationManager() //~1
   
    var myLocation: CLLocation?
    var userLongitude = ""
    var userLatitude = ""
    var userTimeStamp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self //~2
        self.checkMarkView?.isHidden = true
        self.checkRingLayer?.isHidden = true
    }
    
    var screenTitleLabel: UILabel?
    var enableLocationView: UITextView?
    var locationCopyView: UITextView?
    var continueButton: UIButton?
    var locationPrivacyInfo: UITextView?
    var userName: String?

    //MARK: Drawing Methods
    
    override func viewWillAppear(_ animated: Bool) {
//        self.continueButton?.isUserInteractionEnabled = false
    }

    func addScreenTitleLabel() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        
        //header frame properties
        let lW = sWidth
        let lH = 22 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let screenTitleLabel = UILabel(frame: tF)
        
        screenTitleLabel.text = "Location Services"
        screenTitleLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 22)
        screenTitleLabel.textColor = UIColor.black
        screenTitleLabel.textAlignment = .center
        
        //init
        self.view.addSubview(screenTitleLabel)
        self.screenTitleLabel = screenTitleLabel
    }
    
    func addEnableButton() {
        //
        let widthRatio = 75 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 16.0 as CGFloat
        let buttonCopy = "Enable Location Services"
        let tWidth = self.screenTitleLabel!.frame.width
        if sWidth > 350 {
            tFontSize = 16.0 as CGFloat
        } else {
            tFontSize = 14.0 as CGFloat
        }
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = self.screenTitleLabel!.frame.minX
        let marginSpace = 2 as CGFloat
        let tY = superInsetHeight + (3 * gridUnitHeight)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let enableLocationView = UITextView(frame: tFrame)
        enableLocationView.text = buttonCopy
        enableLocationView.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)
        
        //
        self.view.addSubview(enableLocationView)
        self.enableLocationView = enableLocationView
        
        self.enableLocationView!.textAlignment = .center
        self.enableLocationView!.isEditable = false
        self.enableLocationView!.isScrollEnabled = false
        self.enableLocationView!.isSelectable = false
        self.enableLocationView!.textContainer.lineFragmentPadding = 0
        self.enableLocationView!.textContainerInset = .zero
        self.enableLocationView!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.enableLocationView!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(LocationGetterViewController.enableLocationServices(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.enableLocationView!.addGestureRecognizer(tapGesture)
        self.enableLocationView!.isUserInteractionEnabled = true
    }
    
    @objc func enableLocationServices(_ sender: UITapGestureRecognizer) {
        //print("enableLocationServices tap")
        
        self.activityIndicatorView!.startAnimating()
        locationManager.requestWhenInUseAuthorization()
        
        self.checkLocationPermissions()
        
        if CLLocationManager.locationServicesEnabled() {
            self.setLocationParameters()
            
        }
        
    }
    
    let grayTextColor = UIColor(displayP3Red: 150/255, green: 150/255, blue: 150/255, alpha: 1)
    let activeGreenColor = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    var inactiveGrayColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    
    func addLocationCopy() {
        //
        let widthRatio = 276 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 14.0 as CGFloat
        let buttonCopy = "Enabling location services allows the app to find people who are near you"
        let tWidth = widthRatio * sWidth
        if sWidth > 350 {
            tFontSize = 14.0 as CGFloat
        } else {
            tFontSize = 12.0 as CGFloat
        }
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontName: "Avenir-Light", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let marginSpace = 2 as CGFloat
        let tY = self.enableLocationView!.frame.maxY + marginSpace
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let locationCopyView = UITextView(frame: tFrame)
        locationCopyView.text = buttonCopy
        locationCopyView.font = UIFont(name: "Avenir-Light", size: tFontSize)
        
        //
        self.view.addSubview(locationCopyView)
        self.locationCopyView = locationCopyView
        
        self.locationCopyView!.textAlignment = .center
        self.locationCopyView!.isEditable = false
        self.locationCopyView!.isScrollEnabled = false
        self.locationCopyView!.isSelectable = false
        self.locationCopyView!.textContainer.lineFragmentPadding = 0
        self.locationCopyView!.textContainerInset = .zero
        self.locationCopyView!.textColor = self.grayTextColor
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    func addContinueButton() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        //
        let bW = 3 * gridUnitWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
        let bY = sHeight - superInsetHeight - bH
        
        //
        let f = CGRect(x: bX, y: bY, width: bW, height:bH)
        let continueButton = UIButton(frame: f)
        
        continueButton.addTarget(self, action: #selector(LocationGetterViewController.continueButton(_:)), for: .touchUpInside)
        continueButton.backgroundColor = self.inactiveGrayColor
        continueButton.isUserInteractionEnabled = false
        self.view.addSubview(continueButton)
        self.continueButton = continueButton
        
        self.continueButton!.layer.cornerRadius = 8
        self.continueButton!.layer.masksToBounds = true
        
        //
        self.continueButton!.setTitle("Continue", for: .normal)
        self.continueButton!.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 16.0)!
        self.continueButton!.setTitleColor(UIColor.white, for: .normal)
    }

    func checkLocationPermissions() {
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            //print("location notDetermined")
            
        case .restricted, .denied:
            // Disable location features
            print("location denied")
    
        case .authorizedWhenInUse, .authorizedAlways:
            //
            print("location authrized")
            
        }
    }
    
    @objc func continueButton(_ sender: UIButton) {
        //print("willSET DB finalCLLocation is,", mirrorLocation)

//        self.performBatchUpdates_Location(withLocation: mirrorLocation)
//        self.performBatchUpdates_GFLocation(withLocation: mirrorLocation)
        
        //Note: Button will only activate once location updates have been performed successfully
        
        if self.ref_AuthTracker.emailAuthSource == true && self.ref_AuthTracker.phoneAuthSource == false {
            UserDefaults.standard.set("true", forKey: "emailAuthSource")
        } else if self.ref_AuthTracker.emailAuthSource == false && self.ref_AuthTracker.phoneAuthSource == true {
            UserDefaults.standard.set("true", forKey: "phoneAuthSource")
        }
        
        //
        self.performSegue(withIdentifier: "toWelcomeVC", sender: self)
    }

    func addLocationInfo() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let tFontSize = 12.0 as CGFloat
        let buttonCopy = "How is my Location Data Protected?"
        let tWidth = self.screenTitleLabel!.frame.width
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontName: "Avenir-Medium", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = self.screenTitleLabel!.frame.minX
        let marginSpace = 2 as CGFloat
        let tY = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let locationPrivacyInfo = UITextView(frame: tFrame)
        locationPrivacyInfo.text = buttonCopy
        locationPrivacyInfo.font = UIFont(name: "Avenir-Medium", size: tFontSize)
        
        //
        self.view.addSubview(locationPrivacyInfo)
        self.locationPrivacyInfo = locationPrivacyInfo
        
        self.locationPrivacyInfo!.textAlignment = .center
        self.locationPrivacyInfo!.isEditable = false
        self.locationPrivacyInfo!.isScrollEnabled = false
        self.locationPrivacyInfo!.isSelectable = false
        self.locationPrivacyInfo!.textContainer.lineFragmentPadding = 0
        self.locationPrivacyInfo!.textContainerInset = .zero
        self.locationPrivacyInfo!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.locationPrivacyInfo!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(LocationGetterViewController.locationInfo(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.locationPrivacyInfo!.addGestureRecognizer(tapGesture)
        self.locationPrivacyInfo!.isUserInteractionEnabled = true
    }
 
    @objc func locationInfo(_ sender: UITapGestureRecognizer) {
        //print("locationInfo")
        
        self.performSegue(withIdentifier: "toLocationInfo", sender: self)
    }
    
    @IBAction func prepareForUnwind3 (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toLocationInfo" {
            let controller = segue.destination as! TrustInfoViewController
            controller.sourceController = "locationVC"
        }
        
        if segue.identifier == "toWelcomeVC" {
            
            let controller = segue.destination as! ReadyViewController
            controller.userName = self.userName!
            controller.ref_AuthTracker = self.ref_AuthTracker
        }
        
    }
    
    var checkRingLayer: CAShapeLayer?
    var checkMarkView: UIImageView?
    let ringGray = UIColor(displayP3Red: 110/255, green: 110/255, blue: 110/255, alpha: 1)
    
    var ringCenter: CGPoint?
    
    func addCompletionRing() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        
        let radiusRatio = 13 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: cR, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        let cLayer = CAShapeLayer()
        cLayer.path = circularPath.cgPath
        cLayer.fillColor = UIColor.clear.cgColor
        cLayer.lineWidth = 1
        cLayer.strokeColor = self.ringGray.cgColor
        
        let cY = (self.locationCopyView!.frame.maxY + 6) + cR
        let cPoint = CGPoint(x: sWidth * 0.5, y: cY)
        self.ringCenter = cPoint
        cLayer.position = cPoint
        self.view.layer.addSublayer(cLayer)
        self.checkRingLayer = cLayer
        
        //add checkmark
        let cW = cR * 2
        let cH = cW
        let cX = 0 as CGFloat
        let chY = 0 as CGFloat
        
        let cFrame = CGRect(x: cX, y: cY, width: cW, height: chY)
        let checkView = UIImageView(frame: cFrame)
        checkView.contentMode = .center
        checkView.image = UIImage(named: "locationCheckMark")!
        self.checkMarkView = checkView

        let chPoint = CGPoint(x: sWidth * 0.5, y: cY)
        self.view.addSubview(checkView)
        
        checkView.layer.position = chPoint
    }
    
    
    //    self.activityIndicatorView!.startAnimating()
    //    self.activityIndicatorView!.stopAnimating()
    //    self.animateGrowRing()
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        
        let radiusRatio = 14 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = (gridUnitWidth - aW) * 0.5
        let aY = aX
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
        
        self.activityIndicatorView!.layer.position = self.ringCenter!
//        self.activityIndicatorView!.startAnimating()
    }
    
    //MARK: Configure Location Services
    
//    @IBAction func enableLocation(_ sender: Any) {
//        locationManager.requestWhenInUseAuthorization()
//        if CLLocationManager.locationServicesEnabled() {
//            self.setLocationParameters()
//        }
//    }
//
    func setLocationParameters() {
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //~3
        locationManager.distanceFilter = 500 //The minimum distance (measured in meters) a device must move horizontally before an update event is generated. //~4
        locationManager.startUpdatingLocation() //~5
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lastLocation = locations.last {
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let time = lastLocation.timestamp
            
            self.userLongitude = String(describing: long)
            self.userLatitude = String(describing: lat)
            self.userTimeStamp = String(describing: time)
            
            //print(self.userLongitude)
            //print(self.userLatitude)
            //print(self.userTimeStamp)
            
            //the user's true location parameters
            self.myLocation = CLLocation(latitude: lat, longitude: long)
            //print("LOCATIONGETTER true user location is,", self.myLocation)
            
            //the user's guarded location parameters
            self.locusObfuscare(withLocation: lastLocation)
        }
    }
    
//    @IBAction func continueButton(_ sender: Any) {
//        //print("willSET DB finalCLLocation is,", mirrorLocation)
//
//        self.performBatchUpdates_Location(withLocation: mirrorLocation)
//        //self.performSegue(withIdentifier: "toWelcomeVC", sender: self)
//    }
    
    func locusObfuscare(withLocation: CLLocation) {
        //obfuscate the user's real location using a random number generator with 300m radius on client side so user's location can't be reverse-triangulated
        //Tinder reports user_distance value instead of long and latitude values\

        //generate randomized distance between 100 & 300 meters
        var randDistance = Random.number8bit()
        //print("testing randDistance", randDistance)
        
        //max of number8bit == 2^8 == 256
        while randDistance < 200 {
            //try again
            randDistance = Random.number8bit()
        }
        
        let randDistance_Double = Double(randDistance)
        //print("FINAL RAND DISTANCE IS,", randDistance_Double)

        //ATTENTION: WILL ONLY RETURN value between 0 - 256. Fix later to return between 0 - 360
        var randBearing = Random.number8bit()
        //print("testing randBearing", randBearing)
//        while randBearing < 360 {
//            randBearing = Random.number8bit()
//        }
        let randBearing_Double = Double(randBearing)
        //print("FINAL RAND BEARING IS,", randBearing_Double)

        let Location2D = withLocation.coordinate
        self.locationWithBearing(bearing: randBearing_Double, distanceMeters: randDistance_Double, origin: Location2D)
    }
    
    var mirrorLocation: CLLocation! 
    
    var mirrorLon = ""
    var mirrorLat = ""
    
    var didAnimateGrowRing = false
    
    //Bearing refers to the direction that you want to advance, in degrees, so for north: bearing = 0, for east: bearing = 90, for southwest: bearing = 225, etc...
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) {
        //print("in locationWithBearing")
        let distRadians = distanceMeters / (6372797.6) // earth radius in meters
        
        let realLat = origin.latitude * Double.pi / 180
        let realLon = origin.longitude * Double.pi / 180
        
        let mirrorLat = asin(sin(realLat) * cos(distRadians) + cos(realLat) * sin(distRadians) * cos(bearing))
        let mirrorLon = realLon + atan2(sin(bearing) * sin(distRadians) * cos(realLat), cos(distRadians) - sin(realLat) * sin(mirrorLat))
        
        let finalLat = mirrorLat * 180 / Double.pi
        let finalLon = mirrorLon * 180 / Double.pi
        
        let finalCLLocation = CLLocation(latitude: finalLat, longitude: finalLon)
        
        self.mirrorLon = String(describing: finalCLLocation.coordinate.longitude)
        self.mirrorLat = String(describing: finalCLLocation.coordinate.latitude)
        
        //print("finalCLLocation is,", finalCLLocation)
        self.mirrorLocation = finalCLLocation
        
        //No longer writing user lat lon to user node in DB for security
//        self.performBatchUpdates_Location(withLocation: self.mirrorLocation)
        self.performBatchUpdates_GFLocation(withLocation: self.mirrorLocation)
        
//        self.activityIndicatorView!.stopAnimating()
//
//        if self.didAnimateGrowRing == false {
//            self.animateGrowRing()
//            self.continueButton?.isUserInteractionEnabled = true
//            self.continueButton?.backgroundColor = self.activeGreenColor
//            self.didAnimateGrowRing = true
//        }
    }
 
    func animateGrowRing() {
        let ringLayer = self.checkRingLayer!
        let checkMark = self.checkMarkView!
        
        ringLayer.isHidden = false
        checkMark.isHidden = false
        
        //        let shrink = CABasicAnimation(keyPath: "transform.scale")
        //        shrink.fromValue = 1.0
        //        shrink.toValue = 0.0
        //        shrink.duration = 0
        //        shrink.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.0
        grow.toValue = 1.0
        grow.duration = 0.3
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        ringLayer.add(grow, forKey: "grow")
        checkMark.layer.add(grow, forKey: "grow2")
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
//            self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)
        }
    }
    
    var batchUpdatesCounter_Location = 1
    var batchUpdatesCounter_GFLocation = 1
    var didCompleteUpdates_Security = false
    
    var tryUpTo = 4
    
    func performBatchUpdates_Location(withLocation: CLLocation) {
        if let user = Auth.auth().currentUser {
            self.ref = Database.database().reference()
            let selfUID = user.uid
            
            //update geofire location
            let geoFireRef = Database.database().reference().child("userLocationGF")
            let geoFire = GeoFire(firebaseRef: geoFireRef)
            
            //DO NOT UPDATE REAL VALUES
//            self.ref.child("users/\(user.uid)/userLongitude").setValue(self.userLongitude)
//            self.ref.child("users/\(user.uid)/userLatitude").setValue(self.userLatitude)
//            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(self.userTimeStamp)
//            geoFire.setLocation(self.myLocation!, forKey: "\(user.uid)")

            //UPDATE PHANTOM VALUES INSTEAD
//            self.ref.child("users/\(user.uid)/userLongitude").setValue(self.mirrorLon)
//            self.ref.child("users/\(user.uid)/userLatitude").setValue(self.mirrorLat)
//            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(self.userTimeStamp)
//            geoFire.setLocation(self.mirrorLocation, forKey: "\(user.uid)")
            
            let childUpdates = [
                "/users/\(selfUID)/userLongitude": self.mirrorLon,
                "/users/\(selfUID)/userLatitude": self.mirrorLat,
                "/users/\(selfUID)/userLocationTimeStamp": self.userTimeStamp,
            ]
            
            //
            ref.updateChildValues(childUpdates) { (error, forReference_Location) in
                if error != nil {
                    //x3 Max Re-trys
                    if self.batchUpdatesCounter_Location < self.tryUpTo {
                        
                        self.batchUpdatesCounter_Location = self.batchUpdatesCounter_Location + 1
                        self.performBatchUpdates_Location(withLocation: self.mirrorLocation)
                        
                    } else {
                        //After x4 tries -> perform final push in HOLD controller
                        
                       
                    }
                    
                } else {
                    
                    self.userNodeDidFinishUpdating = true
                    self.confirmBatchUpdates_Location()
                    
                }
            }
            
        }
    }
    
    func performBatchUpdates_GFLocation(withLocation: CLLocation) {
        if let user = Auth.auth().currentUser {
            self.ref = Database.database().reference()
            let selfUID = user.uid
            
            //update geofire location
            let geoFireRef = Database.database().reference().child("userLocationGF")
            let geoFire = GeoFire(firebaseRef: geoFireRef)
            
            //
            geoFire.setLocation(self.mirrorLocation, forKey: "\(user.uid)") { (error) in
                if error != nil {
                    
                    if self.batchUpdatesCounter_GFLocation < self.tryUpTo {
                        
                        self.batchUpdatesCounter_GFLocation = self.batchUpdatesCounter_GFLocation + 1
                        self.performBatchUpdates_GFLocation(withLocation: self.mirrorLocation)
                        
                        let buttonCopy = "Enabling location services allows the app to find people who are near you"
                        self.locationCopyView!.text = buttonCopy
                        self.locationCopyView!.textColor = self.grayTextColor
                        
                    } else {
                        //After x4 tries -> perform final push in HOLD controller
                        
                        let errorCopy = "We've run into a problem. Please try again."
                        self.locationCopyView!.text = errorCopy
                        self.locationCopyView!.textColor = self.grayTextColor
                    }
                    
                } else {
                    
                    //
                    self.geoFireDidFinishUpdating = true
                    self.confirmBatchUpdates_Location()

                }
            }
        }
    }
    
    var userNodeDidFinishUpdating = false
    var geoFireDidFinishUpdating = false
    
    func confirmBatchUpdates_Location() {
        
        //when both userNode and gfNode are saving location info
//        if self.userNodeDidFinishUpdating == true && self.geoFireDidFinishUpdating == true {
//
//            self.didCompleteUpdates_Security = true
//            self.ref_AuthTracker.didCompleteUpdates_Security = true
//
//            self.activityIndicatorView!.stopAnimating()
//            self.enableLocationView?.isUserInteractionEnabled = false
//
//            if self.didAnimateGrowRing == false {
//                self.animateGrowRing()
//                self.continueButton?.isUserInteractionEnabled = true
//                self.continueButton?.backgroundColor = self.activeGreenColor
//                self.didAnimateGrowRing = true
//            }
        
        
        //when ONLY gfNode is saving location info
        if self.geoFireDidFinishUpdating == true {
            
            self.didCompleteUpdates_Security = true
            self.ref_AuthTracker.didCompleteUpdates_Security = true
            
            self.activityIndicatorView!.stopAnimating()
            self.enableLocationView?.isUserInteractionEnabled = false
            
            if self.didAnimateGrowRing == false {
                self.animateGrowRing()
                self.continueButton?.isUserInteractionEnabled = true
                self.continueButton?.backgroundColor = self.activeGreenColor
                self.didAnimateGrowRing = true
            }
        
        }
        
    }
    
    
    
//    func performBatchUpdates_Location() {
//        if let user = Auth.auth().currentUser {
//            self.ref = Database.database().reference()
//            self.ref.child("users/\(user.uid)/userLongitude").setValue(self.userLongitude)
//            self.ref.child("users/\(user.uid)/userLatitude").setValue(self.userLatitude)
//            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(self.userTimeStamp)
//
//            //update geofire location
//            let geoFireRef = Database.database().reference().child("userLocationGF")
//            let geoFire = GeoFire(firebaseRef: geoFireRef)
//
//            //set location of user in GeofireFolder in DB
//            geoFire.setLocation(self.myLocation!, forKey: "\(user.uid)")
//        }
//    }
    
    //MARK: Configure Notification Services
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Randomizer Struct
    //Courtesy, Anthony Levings
    
    struct Random {
        static func randomBytes(numberOfBytes:Int) -> [UInt8] {
            var randomBytes = [UInt8](repeating: 0, count: numberOfBytes) // array to hold randoms bytes
            SecRandomCopyBytes(kSecRandomDefault, numberOfBytes, &randomBytes)
            return randomBytes
        }
        static func numberToRandom<T:ExpressibleByIntegerLiteral>(num:T) -> T {
            
            var num = num
            let randomBytes = Random.randomBytes(numberOfBytes: MemoryLayout<T>.size)
            // Turn bytes into data and pass data bytes into int
            NSData(bytes: randomBytes, length: MemoryLayout<T>.size).getBytes(&num, length: MemoryLayout<T>.size)
            return num
        }
        static func number64bit() -> UInt64 {
            
            return Random.numberToRandom(num: UInt64())  // variable for random unsigned 64
        }
        
        static func number32bit() -> UInt32 {
            return Random.numberToRandom(num: UInt32())  // variable for random unsigned 32
        }
        
        static func number16bit() -> UInt16 {
            
            return Random.numberToRandom(num: UInt16())  // variable for random unsigned 16
        }
        
        static func number8bit() -> UInt8 {
            
            return Random.numberToRandom(num: UInt8())  // variable for random unsigned 8
        }
        
        static func int() -> Int {
            
            return Random.numberToRandom(num: Int())  // variable for random Int
        }
        static func double() -> Double {
            
            return Random.numberToRandom(num: Double())  // variable for random Double
        }
        static func float() -> Float {
            
            return Random.numberToRandom(num: Float())  // variable for random Double
        }
    }
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }

    func randomize() {
        let r1 = Random.number64bit()
        //print("r1 is,", r1)
        
        let r2 =  Random.number32bit()
        //print("r2 is,", r2)
        
        let r3 = Random.number16bit()
        //print("r3 is,", r3)
        
        let r4 =  Random.number8bit()
        //print("r4 is,", r4)
        
        let r5 =  Random.int()
        //print("r5 is,", r5)
        
        let r6 =  Random.double()
        //print("r6 is,", r6)
        
        let r7 = Random.float()
        //print("r6 is,", r6)
        
    }
    
}
