//
//  ChatScreenViewController.swift
//  Juno
//
//  Created by Asaad on 2/15/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Shimmer

class ChatScreenViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate {
    //MARK: ALL VARIABLES ...........................................................................................................................................................
    
    //MARK: DB Setup
    //var shared chatList object model
    var chatListObjects = [[String : String]]()
    var chatListObjectIDs = [String]() //Array of unique list object ID's (swapID, chatRequestID, or ThreadID)
    var allUniqueUIDsinChatList = [String]()
    
    //var unlockedUsers = ["NHiJw1SL13Yor2s6zWvU32EAz662", "yEM98NGGXfUTYGAdhQSJIpvD7bn1"]
    var unlockedUsers = ["", ""]
    var incomingDismissedUIDs = [String]()
    
    fileprivate var _refHandle: DatabaseHandle!
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    var user: User?
    
    var presentingSegueIdentifier = "" //chatListVCtochatScreenVC || profileVCtochatScreenVC
    var importedChatUser = [String : String]()
    
    //myUserInfo //Self Info should always be retrieved from selfObject instantiated in GridVC    
    var selfUID: String?
    var selfDisplayName: String?
    var selfVisus: String? //always inherited from GridVC at source
    var selfDefaultPhotoURL: String?
    var selfPrivacy: String?
    
    var visibleDisplayPhoto: UIImage? //rename to visibleDisplayPhotoImageData
    var visibleDisplayPhotoURL: String?
    var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    var localImageURL = "privatephotourl.jpeg"
    
    //....2. Selected User Profile Info
    //need: (2). otherUID (2)
    var otherUID: String? //otherUID is passed directly from CLVC or parsed from passed selectedUser["UID"] from ProfileVC //
    var otherDisplayName: String?
    var otherVisus: String? //always inherited from GridVC at source
    var otherDefaultPhotoURL: String?
    var otherPrivacy: String?
    var otherPushID = ""
    
    var otherVisibleDisplayPhoto: UIImage?
    var revelaroURL: String?
    
    //Load imported Data Structs
    //....3. Imported Data Structs
    var myProfileInfoDict = [String : String]()
    
    var swapRequestStatusDictionary = [String:String]() //doesnt need a live update
    var selectedUser = [String: String]() //when is this dictionary ever used?
    var chatListVCSelectedUID: String?
    
    //....3. Selected User Swap Info & Swap Status
    //Load swap values: (1) swapRequestDoesExist, (2) absExistingSwapRequestID, (3) selfInitiatedSwapRequest, (4) swapRequestStatus
    //live update WHEN (a) swapInitiatorID == selectedUID || (b) (initiatorID = selfID && recipientID == selectedID. CHECK.
    
    var swapRequestDoesExist: Bool?
    var absExistingSwapRequestID: String?
    //live update when (a) -> True || (b) -> False || self.createNewSwapID()
    var selfInitiatedSwapRequest: Bool?
    //live update for observer (live observer only active if swapRequestDoesExist == true
    var swapRequestStatus = ""
    var selfDidOpen = ""
    var otherDidOpen = ""
    
    ////Other
    //imported from: (1) GridVC -> ProfileVC -> ChatScreenVC; (2) GridVC -> ChatListVC -> ChatScreenVC
    //var visus = ""
    
    //DidSet should ONLY fire when: (1), swapID detected in previous VCs, (2) swapID detected in observeLiveRequests
    //status observer fires when: (1) swapRequestID is inherited previously, (2) newSwapRequestID is detected internally
    
    //....Other
    var profileChangesUIDsInProximity = [String]()

    //Chat Properties
    var messageThreadIDDoesExist: Bool?
    //is not being set properly from newChatRequestObject
    var absExistingMessageThreadID = ""

    var messagesArray: [DataSnapshot]! = []
    var temporaryMessageThreadID = ""
    
    //MARK: VIEWDIDLOAD ..................................................................................................................................................................
    
    var importedStatusBarHeight: CGFloat?
    
    override func loadView() {
        //screen params
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        //print("loadVIewimportedStatusBarHeight ,", self.importedStatusBarHeight)
        let myInputView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        //print("loadview screenHeight is,", screenHeight)
        myInputView.backgroundColor = UIColor.white
        self.view = myInputView

        //set meta info
        self.initializeStruct()
        
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        self.initializeSelfProfileInfo()
        self.initializeOtherProfileInfo()
        
        self.drawView()
    }
    
    var backGroundLayer: UIImageView?
    
    func drawView() {
        
        //initView
        //print("drawing View")
        
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //add background
        let bgLayerFrame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        let backGroundLayer = UIImageView(frame: bgLayerFrame)
        backGroundLayer.image = UIImage(named: "backgroundLayer")
        self.backGroundLayer = backGroundLayer
        
        self.view.addSubview(backGroundLayer)
        
        //add Subviews
        self.addChatSpaceView()
        self.addLineSeparator()
        self.addTextField()
        self.addTableView()
        self.addThumbnailImageView()
        self.addNameLabel()
        self.addStatusLabel()
        self.addSliderMask()
        self.addSliderBoxView()
        self.addShimmerView()
        self.addStateLabel()
        self.swapUIControlFlow(fromLiveObserver: false)
        //print("swapUIControlFlow drawView")
        self.addBackButton()
        self.addBlurView()
        self.addImageLoader()
        self.adjustUIViewIfUnlockedUser()
        self.addReportIcon()
        self.addActionSheet()
    }
    
    var reportButton: UIImageView!
    
    func addReportIcon() {
        let x = 0 as CGFloat
        let y = 0 as CGFloat
        let w = 38 as CGFloat
        let h = 34 as CGFloat
        
        //init frame & view
        let bFrame = CGRect(x: x, y: y, width: w, height: h)
        let reportButton = UIImageView(frame: bFrame)
        
        //init view properties
        reportButton.image = UIImage(named: "reportIcon")!
        
        //add to subview
        self.view.addSubview(reportButton)
        self.reportButton = reportButton
        
        let centerX = self.chatSpaceView!.frame.maxX - (w * 0.5)
        let centerY = self.backButtonImageView!.center.y
        
        self.reportButton.layer.position = CGPoint(x: centerX, y: centerY)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.reportButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.reportButton!.addGestureRecognizer(tapGesture)
        self.reportButton!.isUserInteractionEnabled = true
    }
    
    var reportAlert: UIAlertController!
    
//    To resolve this issue, please revise your app to implement all of the following precautions:
//    1 Require that users agree to terms (EULA) and these terms must make it clear that there is no tolerance for objectionable content or abusive users
//    2 A method for filtering objectionable content
//    3 A mechanism for users to flag objectionable content
//    4 A mechanism for users to block abusive users
//    5 The developer must act on objectionable content reports within 24 hours by removing the content and ejecting the user who provided the offending content
    
    func addActionSheet() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
   
        //
        let reportAction = UIAlertAction(title: "Report Speech", style: .default) { (action) in
            self.reportUser(forReason: "Speech")
        }
        alert.addAction(reportAction)
        
        //
        let reportAction2 = UIAlertAction(title: "Report Photo", style: .default) { (action) in
            self.reportUser(forReason: "Photo")
        }
        alert.addAction(reportAction2)
        
        //
        let blockAction = UIAlertAction(title: "Block", style: .destructive) { (action) in
            self.blockUser()
        }
        alert.addAction(blockAction)
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.reportAlert.dismiss(animated: true, completion: nil)
        }
        
//        alert.view.tintColor = UIColor.black
        alert.addAction(cancelAction)
        self.reportAlert = alert
    }
    
    func reportUser(forReason: String) {
        //print("reportUser press")
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        let reportIncidentID = self.ref.child("reportedUsers").childByAutoId().key
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        if forReason == "Speech" {
            //print("reportReasonSpeech")
            var reportData = [String : String]()
            reportData["issuerID"] = selfUID
            reportData["targetID"] = otherUID
            reportData["forReason"] = "Speech"
            reportData["timeStamp"] = timeStamp
            
            let childUpdates = [
                "/reportedUsers/\(otherUID)/\(reportIncidentID)": reportData,
                "/users/\(selfUID)/myFiledReports/\(reportIncidentID)" : reportData,
                "/users/\(otherUID)/incidentReports/\(reportIncidentID)" : reportData,
                ]
            
            //create array of users who have requested swaps
            ref.updateChildValues(childUpdates)
        }
        
        if forReason == "Photo" {
            //print("reportReasonPhoto")
            var reportData = [String : String]()
            reportData["issuerID"] = selfUID
            reportData["targetID"] = otherUID
            reportData["forReason"] = "Photo"
            reportData["timeStamp"] = timeStamp
            
            let childUpdates = [
                "/reportedUsers/\(otherUID)/\(reportIncidentID)": reportData,
                "/users/\(selfUID)/myFiledReports/\(reportIncidentID)" : reportData,
                "/users/\(otherUID)/incidentReports/\(reportIncidentID)" : reportData,
                ]
            
            //create array of users who have requested swaps
            ref.updateChildValues(childUpdates)
        }
    }
    
    var didBlockUser = false
    
    func blockUser() {
        //print("blockUser press")

        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        //        self.ref.child("users/\(selfUID)/blockedUsers/\(otherUID)").setValue("true")
        //        self.ref.child("users/\(otherUID)/blockedBy/\(selfUID)").setValue("true")
        
        let metaUpdates = [
            "users/\(selfUID)/blockedUsers/\(otherUID)" : "true",
            "users/\(otherUID)/blockedBy/\(selfUID)" : "true",
            ]
        
        self.ref.updateChildValues(metaUpdates)
        
        
        //need to remove chat id
        if self.absExistingMessageThreadID != "" {
            let chatThreadID = self.absExistingMessageThreadID
//            self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
//            self.ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
            
            let blockChatUpdates = [
                "users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/chatIsActive" : "nil",
                "users/\(otherUID)/myActiveChatThreads/\(chatThreadID)/chatIsActive" : "nil",
                ]
        
            self.ref.updateChildValues(blockChatUpdates)
            
        }
        
        //need to remove swap id
        if let swapID = self.absExistingSwapRequestID {
//            self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//            self.ref.child("globalSwapRequests/\(otherUID)/\(swapID)").removeValue()
            
            let blockSwapUpdates = [
                "globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive" : "nil",
                "globalSwapRequests/\(otherUID)/\(swapID)/swapIsActive" : "nil",
                ]
        
            self.ref.updateChildValues(blockSwapUpdates)
        }
        
//        UserDefaults.standard.set("true", forKey: "selfDidBlockOrDismissUser")
        
        self.didBlockUser = true
        self.goBackFromBlock()
        
        let userInfoDict = ["outgoingBlockUID" : otherUID]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "outgoingBlockObserved"), object: self, userInfo: userInfoDict)
    }
    
    func goBackFromBlock() {
        
        //print("CSVC will goBackFromBlock")
        self.textFieldView!.resignFirstResponder()

        if self.presentingSegueIdentifier == "profileVCtochatScreenVC" {
            
            self.textFieldView?.resignFirstResponder()
            NotificationCenter.default.post(name: Notification.Name(rawValue: "clearProfileOnDismissal"), object: self)
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

            //needs to unwind profileVC simultaneously
            
            //post is called to early move elsewhere
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "selfDidBlock_SourceIsProfileVC"), object: self)
            
//            //print("WELD-0 CSVC WILL EXECUTE backToProfileVC")
//            self.performSegue(withIdentifier: "backToProfileVC", sender: self)
        
        } else {
            self.textFieldView?.resignFirstResponder()
            self.performSegue(withIdentifier: "backtoChatListView", sender: self)
        }
    }
    
    @objc func reportButton(_ sender: UITapGestureRecognizer) {
        //print("reportButton")
        self.present(self.reportAlert, animated: true, completion: nil)
    }
 
    override func viewDidLoad() {
        //print("CSVC viewDidLoad")
        //print("IMPORTED CHAT USER IS,", self.importedChatUser)

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)

        //Database Configuration
        self.storageRef = Storage.storage().reference()
        self.ref = Database.database().reference()
        
        //Initialize user properties //should always be inherited from GridVC selfInitializedObject
        self.inheritChatListChatData()
        self.inheritChatListSwapData()

        //Load Data to Memory
        self.loadClearPhotoFromDisk_Self()
//        self.getCurrentUserInfo() //deactivated
        
        //Sync messages
        //execute when absExMessageID not inherited
        if self.messagesArray.count == 0 {
            self.messageThreadIDDoesExist = false
        }
        
        if self.messageThreadIDDoesExist! == false {
            self.observeActiveMessageThreads()
        }
        
        //
        if self.tempDeletedMessageThreadID != nil {
            self.absExistingMessageThreadID = self.tempDeletedMessageThreadID!
        }
        
        self.configureDatabaseMessages()
        self.configureUserInfoUI()
        
        //Initialize Observers
        if self.swapRequestDoesExist == false { self.observeIncomingLiveSwapRequests(); /*~self.observeSwapRequestStatusChanges()*/ }
        if self.swapRequestDoesExist == true { self.observeSwapRequestStatusChanges() }
        self.observeUnlockedUsers()

        //didRead last message - moved from VWA
        self.updateDidReadLastMessage()

        //update didOpen
        self.updateSelfDidOpenChatRequest()
//        self.updateSelfDidOpenCompletedSwapRequest() // moved to ViewDidAppear-
        
        //UI
        subscribeToKeyboardNotifications()
        self.activateSlider()
        
//        self.setUserStatusIndicator(withOnlineStatus: <#T##Bool#>)
        
        let shadowWidthR = -7 / 414 as CGFloat
        let shadowWidth = shadowWidthR * UIScreen.main.bounds.width
        self.view.layer.shadowOpacity = 0.2
        self.view.layer.shadowOffset = CGSize(width: shadowWidth, height: 0)
        self.view.layer.shadowRadius = 2
        
//        An app is active when it is receiving events. An active app can be said to have focus. It gains focus after being launched, loses focus when an overlay window pops up or when the device is locked, and gains focus when the device is unlocked.
        NotificationCenter.default.addObserver(self, selector: #selector(exitChats_willResignActive), name: .UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(exitChats_didEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(exitChats_appWillTerminate), name: .UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(exitChats_firebaseDisconnect), name: NSNotification.Name(rawValue: "firebaseDisconnect"), object: nil)

    
        NotificationCenter.default.addObserver(self, selector: #selector(ChatScreenViewController.willEnterForeground), name: .UIApplicationWillEnterForeground , object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatScreenViewController.unwindFromDismissUser), name: NSNotification.Name(rawValue: "selfDidDismissChat"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatScreenViewController.grayOutChatView), name: NSNotification.Name(rawValue: "incomingDismissedByUID"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatScreenViewController.updateUIOnIncomingUnreadMessage), name: NSNotification.Name(rawValue: "unreadMessageObserved"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatScreenViewController.observeIncomingBlockedUser), name: NSNotification.Name(rawValue: "incomingBlockObserved"), object: nil)

        self.addAlert_clearPhotoDeletedFromStorage()
        self.addAskToShowAlert()
        self.addTwoWaySwapAlert()
        self.addOneWayShowAlert()
        self.add1WayInAlert()
        
        
        self.getCurrentUserProfileInfo(withUID: self.otherUID!)
    }
    
    @objc func exitChats_didEnterBackground(_ notification: Notification) {
        self.setUserCSVCPresenceFalse(withMethodCallSource: "CSVC willResignActive")
    }
    
    @objc func exitChats_willResignActive(_ notification: Notification) {
        self.setUserCSVCPresenceFalse(withMethodCallSource: "CSVC willResignActive")
    }
    
    @objc func exitChats_firebaseDisconnect(_ notification: Notification) {
        self.setUserCSVCPresenceFalse(withMethodCallSource: "CSVC willResignActive")
    }
    
    @objc func exitChats_appWillTerminate(_ notification: Notification) {
        self.setUserCSVCPresenceFalse(withMethodCallSource: "CSVC willResignActive")
    }
    
    @objc func willEnterForeground(_ notification: Notification) {
        //print("app willEnterForeground state")
        self.setUserCSVCPresenceTrue(withMethodCallSource: "CSVC willEnterForeground")
    }
    
//    let selfVC = "CSVC"
//
//    @objc func removeFreezeFrame(_ notification: Notification) {
//
//        if notification.name.rawValue == "backFromVoila" {
//            //print("backFromVoila")
//
//            let when = DispatchTime.now() + 1
//            DispatchQueue.main.asyncAfter(deadline: when) {
//                //print("selfVC,is", self.selfVC)
//                self.freezeFrame!.removeFromSuperview()
//            }
//        }
//    }
    
    @objc func unwindFromDismissUser(_ notification: Notification) {
        
        if notification.name.rawValue == "selfDidDismissChat" {
            self.blockUser()
        }
    }
    
    var selfWasDismissed = false
    
    @objc func grayOutChatView(_ notification: Notification) {
        
        //print("grayOutChatView 1")
        
        if notification.name.rawValue == "incomingDismissedByUID" {
            //print("grayOutChatView 2")

            if let dismissedByUID = notification.userInfo?["incomingDismissedByUID"] as? String {
                //print("grayOutChatView 3")

                //print("dismissedByUID", dismissedByUID)

                if dismissedByUID == self.otherUID! {
                    //print("grayOutChatView SUCCESS, can now change UI")
                    
                    self.updateSelfWasDismissedUI_Live()
                    self.selfWasDismissed = true
                }
            }
            
            if self.sourceVC == "UserProfileViewController" {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "clearProfileOnDismissal"), object: self)
            }
            //

        }
    }
    
    func updateSelfWasDismissedUI() {
        
        //1. gray out table view & textview
        self.textFieldView?.textColor = textGray
        self.textFieldView?.tintColor = UIColor.lightGray
        
        //2. disable send button
        //CHECK
        //achieved by disabling sendMessage() in textFieldShouldReturn()
        
        //3. add thanks message
        //CHECK
        
        //4. relock user image
        self.thumbImageView?.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))

        //5. hide online/offline status
        self.userStatusLabel?.isHidden = true
    }
    
    func updateSelfWasDismissedUI_Live() {
        self.selfWasDismissed = true
        
        //1. gray out table view & textview
        self.messagesTableView?.reloadData()
        self.textFieldView?.textColor = textGray
        self.textFieldView?.tintColor = UIColor.lightGray
        
        //2. disable send button
        //CHECK
        //achieved by disabling sendMessage() in textFieldShouldReturn()
        
        //3. add thanks message
        //CHECK
        
        //4. relock user image
        self.thumbImageView?.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))

        //5. hide online/offline status
        self.userStatusLabel?.isHidden = true
    }

    var sourceVC: String?
    
    var userWasDismissed: Bool? { didSet {
            if userWasDismissed == true {
                self.goBackFromBlock()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.messagesTableView?.register(TableViewCell.self, forCellReuseIdentifier: "chatCell")
//        self.messagesTableView!.rowHeight = UITableViewAutomaticDimension
//        self.messagesTableView!.estimatedRowHeight = 31
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        //print("chatScreenViewWillAppear")
        //print("CSVC viewWillAppear")
        
        if let tView = self.transparencyView {
            //print("chatScreenWillAnimateTransparencyToZERO")
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                tView.alpha = 0
            }) { (true) in
                //print("chatScreenWillRmvView")
                tView.removeFromSuperview()
            }
        }
        
        self.observeSelectedUserPresence()
        
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            //print("iPhoneX bottomPadding,", bottomPadding)
        }
        
        self.setUserCSVCPresenceTrue(withMethodCallSource: "CSVC viewWillAppear")
        self.observeCSVCUserPresence()
//        self.updateDidReadLastMessage_SingleInstance() //note: this is called once in VWA and once in VWD
//        self.updateDidReadLastMessage()
        //method called to clear any swap requests that are imported i.e. from ProfileVC && not directly cleared through observerMethods
        self.updateViewWillAppear_ClearSwapAlert()
        
        
        //print("CSVC ViewWillAppear myProfileInfoDict", self.myProfileInfoDict)
        
        self.view?.sendSubview(toBack: self.maskTail!)
        self.view?.sendSubview(toBack: self.backGroundLayer!)
        
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        //print("csvc viewwillappear,sBarH", sBarH)
        
        if self.sourceVC == "UserProfileViewController" {
            UIView.animate(withDuration: 0.20) {
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
        
        if self.userWasDismissed == true {
            //print("CSVC Will gobackgromblock", self.userWasDismissed)
//            self.goBackFromBlock()
        }
        
//        let when = DispatchTime.now() + 1.0
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            self.videoAnimation()
//        }
//        self.getMostRecentDismissedList()
        
        //print("CSVC imported incomingDismissedUIDs is,", incomingDismissedUIDs)
        
        if self.incomingDismissedUIDs.contains(self.otherUID!) {
            //print("viewWIllAppear will updateSelfWasDismissedUI")
            self.selfWasDismissed = true
            self.updateSelfWasDismissedUI()
        }
        
        //print("CSVC vWA willRmv self.freezeFrame")
        self.freezeFrame?.alpha = 0.0
        self.freezeFrame?.isHidden = true
        self.freezeFrame?.removeFromSuperview()
        
        backButtonImageView!.image = UIImage(named: "backChats_Reg")

//        timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
    }
    
//    var timer: Timer!

//    @objc func runTimedCode() {
//        //print("CSVC willrepeatSS")
//        self.takeScreenshot()
//    }
 
    

    func getMostRecentDismissedList() {
        //print("CSVC will getMostRecentDismissedList")
        
        var collectionVC = CollectionViewController()
        var incomingDismissedUIDs = collectionVC.incomingDismissedUIDs
        
        //print("CSVC loadedndimissedByUsersArray IS,", incomingDismissedUIDs)
        //why isn't this getting value
        
        if incomingDismissedUIDs.contains(self.otherUID!) {
            //print("CSVC dimissedByUsersArray doesContain self.otherUID")
            self.updateSelfWasDismissedUI()
        }
    }
    
    @objc func observeIncomingBlockedUser(_ notification: Notification) {
        //print("CSVC observeIncomingBlockedUser")
        
        guard notification.name.rawValue == "incomingBlockObserved" else { return }
        
        if let incomingBlockUID = notification.userInfo?["incomingBlockUID"] as? String {
            //print("CSVC incomingBlockUID", incomingBlockUID)
            
            if incomingBlockUID == self.otherUID! {
                //print("CSVC blockedBy... will dismissVC")
                self.didBlockUser = true
//                self.dismiss(animated: false, completion: nil)
                self.goBackFromBlock()
            }
        }
    }
    
    @objc func lumosFenestra(_ notification: Notification) {
        //print("APPARO lumosFenestra")
    }
    
    @objc func userDidSS(_ notification: Notification) {
        //print("APPARO userDidSS")
    }
    
    @objc func updateUIOnIncomingUnreadMessage(_ notification: Notification) {
        //print("CSVC updateUIOnIncomingUnreadMessage")
        guard self.sourceVC == "ChatListViewController" else { return }
        
        if let messageSenderID = notification.userInfo?["withMessageSenderUID"] as? String {
            //print("CSVC updateUIOnIncomingUnreadMessage with messageSenderID", messageSenderID)
            
            if messageSenderID != self.otherUID! {
                //print("CSVC RECEIVED NEW UNREAD MESSAGE NOT FROM LIVE OTHER")
                //how should the button respond when new messages keep coming in?
                UIView.transition(with: backButtonImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
//                    self.backButtonImageView!.image = UIImage(named: "chatScreenBackButton_Unread")
                    self.backButtonImageView!.image = UIImage(named: "backChats_White")

                }, completion: nil)
            }
        }
    }
    
    
    
    /*
     
 Still Need:
     
 0. need to update UI cases for type of chatlist object for unread messages
 1. one way swap notifications
 2. 2 way swap notifications
 3. unlock to all users if user decides to switch profile visibility (or temporarily disable user's ability to toggle visibility until this feature is ready)
 4. multiple photos per profile
 5. give the outoging/incoming one way swap symbols a white outline with more rouinded corners / appearing jagged in UI
 6. Shift table view line to edge of pictureFrameHolder & not pictureViewFrame (intercepting swap symbols)
 7. fix the general chat threads only appearing in ChatLIst and not loading from CSVC previously
 8. hiding the user's name
    
     for swap request observers - if unlockedUserArray.doesContain(uid) == false , then create UI notification to notify that new incoming swap request - but then what if user views the request but doesn't respond... i.e. they wait to unlock later, they shouldn't be notified again
     notify on: 1, incoming reveal request - 2, outgoing reveal request approved, 3 - incoming swap request, 4 - outgoing swap request approved
    */
    
    var didLoadUserFromDB = false
    var userInfoDict_fromDB = [String:String]()
    
    func getCurrentUserProfileInfo(withUID: String) {
        
        self.ref.child("users").child(withUID).observe(.value) { (snapshot) in
            
            guard let value = snapshot.value as? [String: Any] else { return }
            
            var userGridThumbInfoDict = [String: String]()
            
            //Note: Sometimes the userObject will remain in your chatlist, even if userObject in DB does not have complete information. This is because your code isn't properly deleting all interactions with user after user deletes account. I.e. if Travis deletes account, but you still see him, you can still message him, and this will create a new TravisUID node in Firebase with ChatID Only. But will still show up in chatlist objects. So make sure you exit the userObject if user is deleted && if object is incomplete.
            
            let userUID = snapshot.key
            userGridThumbInfoDict["UID"] = userUID

            //userName
            if let userDisplayName = value["userDisplayName"] as? String {
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
            } else {
                //incomplete user object when...
                self.didLoadUserFromDB = false
                return
            }
            
            //userFullURL
            if let userThumbnailURLString = value["full_URL"] as? String {
                userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                //print("populateArrayofDictionaries userThumbnailURLString", userThumbnailURLString)
            } else {
                //incomplete user object when...
                self.didLoadUserFromDB = false
                return
            }
            
            //userAge
            if let uAge = value["userAge"] as? String {
                userGridThumbInfoDict["userAge"] = uAge
            } else {
                userGridThumbInfoDict["userAge"] = " "
            }
            
            //userGridURL
            if let gridThumbURL = value["gridThumb_URL"] as? String {
                //print("CHECK gridThumbURL")
                userGridThumbInfoDict["gridThumb_URL"] = gridThumbURL
            } else {
                //print("FAIL gridThumbURL")
            }
            
            //userVisus
            if let visus = value["visus"] as? String {
                userGridThumbInfoDict["visus"] = visus
            } else {
                
            }
            
            //AboutMe
            if let aboutMe = value["AboutMe"] as? String {
                userGridThumbInfoDict["aboutMe"] = aboutMe
            } else {
                userGridThumbInfoDict["aboutMe"] = ""
            }
            
            //School
            if let school = value["School"] as? String {
                userGridThumbInfoDict["school"] = school
            } else {
                userGridThumbInfoDict["school"] = ""
            }
            
            //Work
            if let work = value["Work"] as? String {
                userGridThumbInfoDict["work"] = work
            } else {
                userGridThumbInfoDict["work"] = ""
            }
            
            //Height
            if let height = value["Height"] as? String {
                userGridThumbInfoDict["height"] = height
            } else {
                userGridThumbInfoDict["height"] = ""
            }
            
            //HereFor
            if let lookingFor = value["LookingFor"] as? String {
                userGridThumbInfoDict["lookingFor"] = lookingFor
            } else {
                userGridThumbInfoDict["lookingFor"] = ""
            }
            
            //Privacy
            if let userPrivacyPreference = value["privacy"] as? String {
                userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
            } else {
                userGridThumbInfoDict["userPrivacyPreference"] = ""
            }
            
            //BlurDegree
            if let blurState = value["blurState"] as? String {
                userGridThumbInfoDict["blurState"] = blurState
            } else {
                userGridThumbInfoDict["blurState"] = ""
            }
            
            //Presence
            if let isOnline = value["isOnline"] as? String {
                userGridThumbInfoDict["isOnline"] = isOnline
            } else {
                userGridThumbInfoDict["isOnline"] = ""
            }
            
            //additional photo URLs
            if let photoURL2 = value["full_URL_2"] as? String {
                userGridThumbInfoDict["full_URL_2"] = photoURL2
            }
            
            if let photoURL3 = value["full_URL_3"] as? String {
                userGridThumbInfoDict["full_URL_3"] = photoURL3
            }
            
            if let photoURL4 = value["full_URL_4"] as? String {
                userGridThumbInfoDict["full_URL_4"] = photoURL4
            }
            
            //ATTN: add distanceTo from CVC dictionary ELSE return nil as "empty space"
            
            if self.ref_SelfModelController.distanceToByUID.keys.contains(self.otherUID!) {
                userGridThumbInfoDict["distanceTo"] = self.ref_SelfModelController.distanceToByUID[self.otherUID!]
            } else {
                userGridThumbInfoDict["distanceTo"] = ""
            }
            
            self.userInfoDict_fromDB = userGridThumbInfoDict
            self.didLoadUserFromDB = true
        }
    }
    
    
    func setUserCSVCPresenceTrue(withMethodCallSource: String) {
        //print("CSVC WILLSET setUserCSVCPresenceTrue withMethodCallSource", withMethodCallSource)
        
        var chatThreadID = ""
        
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else {
            chatThreadID = self.temporaryMessageThreadID
        }

        let selfUID = Auth.auth().currentUser!.uid
        
        let otherUID = self.otherUID!
        
        ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent").setValue("true")
        ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent").setValue("true")
    }
    
    func setUserCSVCPresenceFalse(withMethodCallSource: String) {
        //print("CSVC WILLSET setUserCSVCPresenceFalse withMethodCallSource", withMethodCallSource)
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else {
            chatThreadID = self.temporaryMessageThreadID
        }
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent").setValue("false")
        ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent").setValue("false")
        
        /*
        let path_Self = ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent")
        let path_Other = ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_isPresent")
        
        path_Other.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
    
                // Set value and report transaction success
                currentData.value = post
                
                return TransactionResult.success(withValue: currentData)
      
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                //print(error.localizedDescription)
            }
        }
        */
    }

    //Objective: We want to add a transparency view whenever the user unlocks another user
    override func viewDidAppear(_ animated: Bool) {
        //print("chatScreenViewDidAppear")

        self.updateSelfDidOpenCompletedSwapRequest() // moved to ViewDidAppear-
        //print("viewDidAppear swapRequestStatus", self.swapRequestStatus)
    }
    
    //MARK: Initial Data Setup........................................................................................................................................................
    
    func confirmSwapValues() {
        //print("Confirming swap variables")
        //print(self.swapRequestDoesExist)
        //print(self.absExistingSwapRequestID)
        //print(self.selfInitiatedSwapRequest)
        //print(self.swapRequestStatus)
    }
    
    func initializeSelfProfileInfo() {
        //initliazed from same source regardless of presenting
        self.selfUID = Auth.auth().currentUser!.uid
        self.selfDisplayName = myProfileInfoDict["selfDisplayName"]!
        self.selfVisus = myProfileInfoDict["selfVisus"]!
        self.selfDefaultPhotoURL = myProfileInfoDict["selfDefaultPhotoURL"]!
        self.selfPrivacy = myProfileInfoDict["privacy"]!
    }
    
    func initializeOtherProfileInfo() {
        
        if self.presentingSegueIdentifier == "chatListVCtochatScreenVC" {
            if self.importedChatUser.keys.contains("swapWithUID") {
                //print("imported user data object is of type swap")
                self.otherUID = importedChatUser["swapWithUID"]!
                self.otherDisplayName = importedChatUser["swapWithDisplayName"]!
                self.otherVisus = importedChatUser["swapWithVisus"]!
                self.otherDefaultPhotoURL = importedChatUser["swapWithDefaultPhotoURL"]!
                self.swapRequestStatus = importedChatUser["requestStatus"]!
                
                //set chat data
                ////print("setting absExistingSwapRequestID")
                //self.absExistingMessageThreadID = importedChatUser["chatThreadID"]!
                ////print("absExistingMessageThreadID", self.absExistingMessageThreadID)
                
            } else if self.importedChatUser.keys.contains("chatRequestID") {
                //print("imported user data object is of type chatRequest")
                self.otherUID = importedChatUser["requestToUID"]!
                self.otherDisplayName = importedChatUser["requestToDisplayName"]!
                self.otherVisus = importedChatUser["requestToVisus"]!
                self.otherDefaultPhotoURL = importedChatUser["chatRequestWithDefaultPhotoURL"]!
                
                /*
                 //chatRequestObjects always start out without a swap Request...
                 //self.otherPrivacy = importedChatUser["chatRequestWithDefaultPhotoURL"]!
                 self.swapRequestDoesExist = false*/
                
            } else if self.importedChatUser["objectType"]! == "generalChatThread" {
                //print("imported user data object is of type chatRequest")
                self.otherUID = importedChatUser["chatWithUID"]!
                self.otherDisplayName = importedChatUser["chatWithDisplayName"]!
                self.otherVisus = importedChatUser["chatWithVisus"]!
                self.otherPrivacy = "false"
                self.otherDefaultPhotoURL = importedChatUser["chatWithPhotoURL"]!
                
            } else if self.importedChatUser.keys.contains("oneWayRequestID") {
                //print("imported user data object is of type oneWayRequest")
                
                //set profile data
                self.otherUID = importedChatUser["revealToUID"]!
                self.otherDisplayName = importedChatUser["revealToDisplayName"]!
                self.otherVisus = importedChatUser["revealToVisus"]!
                //self.otherPrivacy = "false"
                self.otherDefaultPhotoURL = importedChatUser["revealToDefaultPhotoURL"]!
            }
            
            if self.importedChatUser.keys.contains("pushID") {
                self.otherPushID = importedChatUser["pushID"]!
            } else {
                print("csvc no push id")
            }
            
            //PROFILE VC TO CHAT LIST VC
        
        } else if self.presentingSegueIdentifier == "profileVCtochatScreenVC" {
            //print("initializeOtherProfileInfo profileVCtochatScreenVC")
            self.otherUID = importedChatUser["UID"]!
            self.otherDisplayName = importedChatUser["userDisplayName"]!
            self.otherVisus = importedChatUser["visus"]!
            self.otherPrivacy = importedChatUser["userPrivacyPreference"]!
//            self.otherDefaultPhotoURL = !
            
            if let gridURL = importedChatUser["gridThumb_URL"] {
                self.otherDefaultPhotoURL = gridURL
            } else {
                if let url = importedChatUser["userThumbnailURLString"] {
                    self.otherDefaultPhotoURL = url
                }
            }
            
            if self.importedChatUser.keys.contains("pushID") {
                self.otherPushID = importedChatUser["pushID"]!
            } else {
                print("csvc no push id")
            }
            
        }
        
        self.observeUserBadgeCount()
    }
    
    func inheritChatListChatData() {
        
        guard self.presentingSegueIdentifier == "chatListVCtochatScreenVC" else { return }
        
        if self.importedChatUser.keys.contains("swapWithUID") {
            //swap object is 2WaySwap
            self.messageThreadIDDoesExist = true
            self.absExistingMessageThreadID = importedChatUser["chatThreadID"]!
            
        } else if self.importedChatUser.keys.contains("oneWayRequestID") {
            //swap object is oneWayRequest
            self.messageThreadIDDoesExist = true
            self.absExistingMessageThreadID = importedChatUser["chatThreadID"]!
            
        } else if self.importedChatUser.keys.contains("chatRequestID") {
            //swap object is chatRequest
            if importedChatUser["chatThreadDoesExist"] == "false" {
                self.absExistingMessageThreadID = self.temporaryMessageThreadID
            } else if importedChatUser["chatThreadDoesExist"] == "true" {
                self.absExistingMessageThreadID = importedChatUser["existingChatThreadID"]!
            }
            //
            //            if let absExistingMessageThreadID = importedChatUser["existingChatThreadID"] {
            //                self.messageThreadIDDoesExist = true
            //                self.absExistingMessageThreadID = absExistingMessageThreadID
            //            }
            
        } else if self.importedChatUser["objectType"]! == "generalChatThread" {
            //swap object is generalChatThread
            self.messageThreadIDDoesExist = true
            self.absExistingMessageThreadID = importedChatUser["chatThreadID"]!
        }
    }
    
    func inheritChatListSwapData() {
        
        guard self.presentingSegueIdentifier == "chatListVCtochatScreenVC" else { return }
        
        //swap object is 2WaySwap
        if self.importedChatUser.keys.contains("swapWithUID") {
            self.swapRequestDoesExist = true
            self.absExistingSwapRequestID = importedChatUser["swapRequestID"]!
            if importedChatUser["swapInitiatorID"] == self.selfUID! {
                self.selfInitiatedSwapRequest = true
                self.selfDidOpen = importedChatUser["swapInitiatorDidOpen"]!
                self.otherDidOpen = importedChatUser["swapRecipientDidOpen"]!
            } else {
                self.selfInitiatedSwapRequest = false
                self.selfDidOpen = importedChatUser["swapRecipientDidOpen"]!
                self.otherDidOpen = importedChatUser["swapInitiatorDidOpen"]!
            }
            self.swapRequestStatus = importedChatUser["requestStatus"]!
            self.selfDidOpen = importedChatUser["selfDidOpen"]!
            
            //swap object is oneWayRequest
        } else if self.importedChatUser.keys.contains("oneWayRequestID") {
            self.swapRequestDoesExist = true
            self.absExistingSwapRequestID = importedChatUser["oneWayRequestID"]!
            if importedChatUser["requestInitiatorID"]! == self.selfUID! {
                self.selfInitiatedSwapRequest = true
            } else {
                self.selfInitiatedSwapRequest = false
                //otherDidOpen?
            }
            self.swapRequestStatus = importedChatUser["requestStatus"]!
            
            //swap object is chatRequest
        } else if self.importedChatUser.keys.contains("chatRequestID") {
            self.swapRequestDoesExist = false
            
            //swap object is generalChatThread
        } else if self.importedChatUser["objectType"]! == "generalChatThread" {
            self.swapRequestDoesExist = false
        }
    }
    
    func configureUserInfoUI() {
        let selectedUID = self.otherUID!
        let profileThumbnail = self.thumbImageView!
        
        if self.unlockedUsers.contains(selectedUID) {
            if self.otherVisus! == "false" {
                //print("configureUserInfoUI unlockedUsers", self.unlockedUsers, "DOESContain", selectedUID)
                if let clearImage = self.loadClearPhotoFromDisk_Other() {
                    profileThumbnail.image = clearImage
                } else {
                    profileThumbnail.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))
                }
            } else {
                profileThumbnail.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))
            }
        } else {
            //print("configureUserInfoUI unlockedUsers", self.unlockedUsers, "DOESNOTContain", selectedUID)
            profileThumbnail.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))
        }
        
        if self.incomingDismissedUIDs.contains(self.otherUID!) {
            profileThumbnail.sd_setImage(with: URL(string: self.otherDefaultPhotoURL!))
        }
        
        self.nameLabel!.text = self.otherDisplayName!
    }
    
    //    var userStatusLabel: UILabel?
    
    //ATTENTION: need to set initial state by passing data from sourceVC. You're currently only settings state from DB directly, which may take too long to laod sometimes.
    
    func observeSelectedUserPresence() {
        //print("CSVC observeSelectedUserPresence")
        let otherUID = self.otherUID!
        //print("otherUID is,", otherUID)
        
        self.ref.child("globalUserPresence/\(otherUID)").observe(.value) { (snapshot) in
            //print("CSVC observeSelectedUserPresence snapshot is,", snapshot)

            guard let snapshotValue = snapshot.value as? [String : String] else { return }
            //print("CSVC observeSelectedUserPresence snapshotValue is,", snapshotValue)
            guard let userPresence = snapshotValue["isOnline"] as? String else { return }
            //print("CSVC userPresence is,", userPresence)
            
            if userPresence == "true" {
                self.setUserStatusIndicator(withOnlineStatus: true)
            } else {
                self.setUserStatusIndicator(withOnlineStatus: false)
            }
        }
    }
    
    var otherUserIsReadingMessage = false
    
    func observeCSVCUserPresence() {
        //print("CSVC observeCSVCUserPresence")

        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else if self.temporaryMessageThreadID != "" {
            chatThreadID = self.temporaryMessageThreadID
        }
        
        //print("CSVC observeCSVCUserPresence at chatThreadID", chatThreadID)
        //print("CSVC otherUID)_isPresen", "\(otherUID)_isPresent)")
        self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(otherUID)_isPresent").observe(.value) { (snapshot) in
            //print("CSVC observeCSVCUserPresence snapshot is,", snapshot )
            //print("CSVC observeCSVCUserPresence snapshot value,", snapshot.value)
            
            guard let userIsReadingMessages = snapshot.value as? String else { return }
            //print("CSVC userIsReadingMessages", userIsReadingMessages)
            if userIsReadingMessages == "true" {
                self.otherUserIsReadingMessage = true
            } else {
                self.otherUserIsReadingMessage = false
            }
        }
        
    }
    
    var didReadLastMessage_Handle: DatabaseHandle!
    
    var updateDidReadLastMessage_DidFire = false
    
    func updateDidReadLastMessage() {
        //print("CSVC updateDidReadLastMessage")
        //VWA Updater of last messages - fires everytime last message new incoming message is observed
        //ERROR: observer is calling child methods in this method EVEN AFTER user exits ViewController. MUST DEALLOCATE OBSERVER AFTER LEAVING VC.
        
        let user = Auth.auth().currentUser!
        let selfUID = self.selfUID!
        
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else if self.temporaryMessageThreadID != "" {
            chatThreadID = self.temporaryMessageThreadID
        }
        
        //Note: to make sure observer is only ever INITIALIZED once per VC lifecylce
        if self.updateDidReadLastMessage_DidFire == false {
            //resume
            self.updateDidReadLastMessage_DidFire = true
        } else {
            return
        }
    
        didReadLastMessage_Handle = self.ref.child("globalChatThreadMetaData/\(chatThreadID)").observe(.value , with: { (snapshot: DataSnapshot) in
            //print("CSVC observeLastMessages snapshot is,", snapshot)
            
            //print("Q1 Pass")
            
            guard let messageThreadBody = snapshot.value as? [String:String] else { return }
            //print("Q2 Pass")

            guard let lastMessage = messageThreadBody["chatMessage"] else { return }
            //print("Q3 Pass")
            guard let messageRecipientUID = messageThreadBody["messageRecipientUID"] else { return }
            //print("Q4 Pass")

            guard let messageSenderUID = messageThreadBody["messageSenderUID"] else { return }
            //print("Q5 Pass")

            guard let recipientDidReadLastMessage = messageThreadBody["recipientDidReadLastMessage"] else { return }
            //print("Q6 Pass")

//            //print("CURRENT user didReadLast Message, ", self.userIs)
            if messageRecipientUID == selfUID {
                //print("Q7 Pass")
              
                //if most recent message is sent for self, then read is true
                //print("XP1 recipientDidReadLastMessage")
                self.ref.child("globalChatThreadMetaData/\(chatThreadID)/recipientDidReadLastMessage").setValue("true")

            } else if messageSenderUID != selfUID {
                //print("Q8 Pass")
                
                //print("XP2 recipientDidReadLastMessage")

                self.ref.child("globalChatThreadMetaData/\(chatThreadID)/recipientDidReadLastMessage").setValue("false")

            }
        })
    }
    
    func updateDidReadLastMessage_SingleInstance() {
        
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else if self.temporaryMessageThreadID != "" {
            chatThreadID = self.temporaryMessageThreadID
        }
        
        if self.messagesArray.isEmpty != true {
            
            guard let lastMessageBody = self.messagesArray.last?.value as? [String:String] else { return }
            
            guard let lastMessage = lastMessageBody["chatMessage"] else { return }
            guard let messageRecipientUID = lastMessageBody["messageRecipientUID"] else { return }
            guard let messageSenderUID = lastMessageBody["messageSenderUID"] else { return }
            guard let recipientDidReadLastMessage = lastMessageBody["recipientDidReadLastMessage"] else { return }
        
            self.ref.child("globalChatThreadMetaData/\(chatThreadID)/recipientDidReadLastMessage").setValue("true")
        }
    }
    
    func setUserStatusIndicator(withOnlineStatus: Bool) {
        if withOnlineStatus == true {
            self.userStatusLabel!.text = "Online"
        } else if withOnlineStatus == false {
            self.userStatusLabel!.text = "Offline"
        }
    }
    
    
    //MARK: View REFerences..........................................................................................................................................................
    var chatSpaceView: UIView?
    var chatSpaceViewFrame: CGRect?
    var textFieldView: UITextField?
    var lineSeparatorView: UIView?
    var backButtonImageView: UIImageView?
    var blurView: UIView?
    var transparencyView: UIView?
    var imageLoaderView: UIImageView?
    var stateLabel: UILabel?

    var thumbImageView: UIImageView?
    var nameLabel: UITextView?
    var sliderMaskImageView: UIImageView?
    var sliderButtonImageView: UIImageView?
    
    var keyImageView: UIImageView?
    var wImageViewLHS_i: UIImageView?
    var wImageViewRHS_i: UIImageView?
    
    var messagesTableView: UITableView?
    
    //Drawing Constants
    var messageFieldBorderInsetFinal: CGFloat?
    var messageFieldBorderInset_Lo: CGFloat?
    var messageFieldBorderInset_Hi: CGFloat?
    var cornerRadius: CGFloat?
    
    struct ViewParameters {
        let screen: CGRect?
        let screenWidth: CGFloat?
        let screenHeight: CGFloat?
        let insetRatio: CGFloat?
        let insetUnit: CGFloat?
        let topInset: CGFloat?
        let bottomInset: CGFloat?
        var keyBoardHeight: CGFloat?
    }
    
    var viewParameters: ViewParameters?
    
    func initializeStruct() {
        
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        let insetRatio = 19/414 as CGFloat
        let insetUnit = insetRatio * screenWidth
        let topInset = 4 * insetUnit
        let bottomInset = 1 * insetUnit
        let kbHeight = CGFloat(200.00)
        
        self.viewParameters = ViewParameters(screen: screen, screenWidth: screenWidth, screenHeight: screenHeight, insetRatio: insetRatio, insetUnit: insetUnit, topInset: topInset, bottomInset: bottomInset, keyBoardHeight: kbHeight)
    }
    
    //control flow variables
    var swap = false
    var keyPosition = "L"
    var swapState = "P" //PRNDL
    
    //MARK: Draw-View Functions......................................................................................................................................................
   /*
    func addChatSpaceView() {
        //
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
     
        let insetRatio = 19/414 as CGFloat
        let insetUnit = insetRatio * screenWidth
        
        let topInset = 4 * insetUnit
        let bottomInset = 1 * insetUnit
        
        let xPos = insetUnit
        let yPos = 4 * insetUnit
        
        let cWidth = screenWidth - (2 * insetUnit)
        let cHeight = screenHeight - topInset - bottomInset
        
        let chatSpaceViewFrame = CGRect(x: xPos, y: yPos, width: cWidth, height: cHeight)
        self.chatSpaceViewFrame = chatSpaceViewFrame
        self.chatSpaceView = UIView(frame: chatSpaceViewFrame)
        chatSpaceView?.backgroundColor = UIColor.white
        
        let cornerRadiusRatio = 20 / 376 as CGFloat
        self.cornerRadius = cornerRadiusRatio * cWidth
        let cornerRadius = self.cornerRadius!
        chatSpaceView!.layer.cornerRadius = cornerRadius
        chatSpaceView!.layer.masksToBounds = true
        
        self.view.addSubview(chatSpaceView!)
    }
    */
    
    var maskTail: UIImageView?

    var finalsBarH: CGFloat?
    
    func addChatSpaceView() {
        //
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let sHeight = screen.height
        var sBarH = UIApplication.shared.statusBarFrame.size.height
        
        if let sourceVC = self.sourceVC {
            if sourceVC == "UserProfileViewController" {
                sBarH = self.importedStatusBarHeight!
            }
        }

        let absoluteNavSpaceRatio = 56 / 734 as CGFloat
        var absoluteNavSpace = absoluteNavSpaceRatio * sHeight
        if sHeight >= 734 as CGFloat {
            //constrain the navSpace to a fixed amount for iPhoneX
            absoluteNavSpace = 56 as CGFloat
        } else {
            //
        }
        
        let insetRatio = 19/414 as CGFloat
        let insetUnit = insetRatio * screenWidth
        
        let topInset = sBarH + absoluteNavSpace
        let bottomInset = 1 * insetUnit
        
        let xPos = insetUnit
        let yPos = topInset

        let cWidth = screenWidth - (2 * insetUnit)
        let cHeight = sHeight - topInset - bottomInset
        
        let chatSpaceViewFrame = CGRect(x: xPos, y: yPos, width: cWidth, height: cHeight)
        self.chatSpaceViewFrame = chatSpaceViewFrame
        self.chatSpaceView = UIView(frame: chatSpaceViewFrame)
        chatSpaceView?.backgroundColor = UIColor.white
        
        let cornerRadiusRatio = 20 / 376 as CGFloat
        self.cornerRadius = cornerRadiusRatio * cWidth
        let cornerRadius = self.cornerRadius!
        chatSpaceView!.layer.cornerRadius = cornerRadius
        chatSpaceView!.layer.masksToBounds = true
        
        self.view.addSubview(chatSpaceView!)
        
        //add Mask tail
        //this may need to be designed according to radius porportions. Check performance on different screen sizes.
//        let outerSpace = 12.95 as CGFloat
//        let safeSpace = 25.0 as CGFloat
//        let maskW = outerSpace + safeSpace
//        let maskH = 18.82 as CGFloat
//
//        let maskX = self.chatSpaceView!.frame.minX - outerSpace
//        let maskY = self.chatSpaceView!.frame.minY
//
//        let frame = CGRect(x: maskX, y: maskY, width: maskW, height: maskH)
//        let maskView = UIImageView(frame: frame)
//        maskView.image = UIImage(named: "maskTail")
//        maskView.contentMode = .center
//
//        self.view.addSubview(maskView)
        
        //REACTIVATE
//        let outerSpaceRatio = 12.95 / screenWidth as CGFloat
//        let safeSpaceRatio = 25 / screenWidth as CGFloat
//        let maskHRatio = 18.82 / 736 as CGFloat
//
//        let outerSpace = outerSpaceRatio * screenWidth
//        let safeSpace = safeSpaceRatio * screenWidth
//        let maskW = outerSpace + safeSpace
//
//        var maskH = maskHRatio * sHeight
//        if sHeight > 736 {
//            maskH = (maskHRatio * 736) as CGFloat
//        }

        let outerSpaceRatio = 12.95 / 376 as CGFloat
        let safePaceRatio = 25.0 / 376 as CGFloat
        let maskHtWRatio = 18.82 / 37.95 as CGFloat

        let outerSpace = outerSpaceRatio * self.chatSpaceView!.frame.width
        let safeSpace = safePaceRatio * self.chatSpaceView!.frame.width
        let maskW = outerSpace + safeSpace

        var maskH = maskHtWRatio * maskW
        //print("final mask H is,", maskH)
        if sHeight > 736 {
            maskH = 18.82 as CGFloat
        }

        let maskX = self.chatSpaceView!.frame.minX - outerSpace
        //print("maskX is,", maskX)
        let maskY = self.chatSpaceView!.frame.minY
//        let maskX = -outerSpace
//        let maskY = 0 as CGFloat
    //
        
        let mFrame = CGRect(x: maskX, y: maskY, width: maskW, height: maskH)
        let maskView = UIImageView(frame: mFrame)
        maskView.image = UIImage(named: "maskTail")
        maskView.contentMode = .scaleAspectFill
        
        maskView.backgroundColor = UIColor.clear
        self.view.addSubview(maskView)
        
        self.maskTail = maskView
        
        //print("maskTail Y is,", self.maskTail!.frame.minY)
        //print("chatFrame Y is,", self.chatSpaceView!.frame.minY)

    }
    
    //ATTENTION: REMEMBER THAT YOU"RE GETTING THE KEYBOARD HEIGHT FROM DATABASE AND NOT FROM SCREEN> MAKE SURE THIS DATA IS AVALABLE
    func addLineSeparator() {
        let chatSpaceView = self.chatSpaceView!
        
        let chatSpaceHeight = chatSpaceView.bounds.height
        let chatSpaceWidth = chatSpaceView.bounds.width
        
        let messageFieldAreaRatio_Lo = 52/414 as CGFloat
        let messageFieldAreaRatio_Hi = 97/414 as CGFloat
        
        let keyBoardHeight = 226 as CGFloat //this keyboard height is retrieved as an initial instance of kBHeight (set from account creationVC & updated everytime a user calls keyboard
        let chatSpaceHeightWithKeyboard = chatSpaceHeight - keyBoardHeight
        
        if self.selfVisus! == "true" && self.otherVisus! == "true" {
            self.swap = false
        } else {
            self.swap = true
        }
        
        //ATTN: inset values are initialized from new Constants and NOT DYNAMIC VALUES
//        let messageFieldBorderInset_Lo = (messageFieldAreaRatio_Lo * chatSpaceHeightWithKeyboard) as CGFloat
//        let messageFieldBorderInset_Hi = (messageFieldAreaRatio_Hi * chatSpaceHeightWithKeyboard) as CGFloat
//        self.messageFieldBorderInset_Lo = messageFieldBorderInset_Lo
//        self.messageFieldBorderInset_Hi = messageFieldAreaRatio_Hi
        
//        if self.swap == false {
//            //if general chat
//            self.messageFieldBorderInsetFinal = (messageFieldAreaRatio_Lo * chatSpaceHeightWithKeyboard) as CGFloat
//        } else {
//            //if swap type
//            if self.unlockedUsers.contains(self.otherUID!) {
//                self.messageFieldBorderInsetFinal = (messageFieldAreaRatio_Lo * chatSpaceHeightWithKeyboard) as CGFloat
//            } else {
//                self.messageFieldBorderInsetFinal = (messageFieldAreaRatio_Hi * chatSpaceHeightWithKeyboard) as CGFloat
//            }
//        }
        
        //CODE FOR LINE VIEW
//        let x = 0 as CGFloat
//        let y = chatSpaceHeight - self.messageFieldBorderInsetFinal!
//        let lineFrame = CGRect(x: x, y: y, width: chatSpaceWidth, height: 0.5)
//        let lineSeparatorView = UIView(frame: lineFrame)
//        lineSeparatorView.backgroundColor = UIColor.lightGray
//        chatSpaceView.addSubview(lineSeparatorView)
//
//        self.lineSeparatorView = lineSeparatorView
        
        let newBottomInsetConstant_Hi = 97 as CGFloat
        let newBottomInsetConstant_Lo = 51 as CGFloat
        
        self.messageFieldBorderInset_Hi = newBottomInsetConstant_Hi
        self.messageFieldBorderInset_Lo = newBottomInsetConstant_Lo
        
        if self.swap == false {
            //if general chat
            self.messageFieldBorderInsetFinal = newBottomInsetConstant_Lo
        } else {
            //if swap type
            if self.unlockedUsers.contains(self.otherUID!) {
                self.messageFieldBorderInsetFinal = newBottomInsetConstant_Lo
            } else {
                self.messageFieldBorderInsetFinal = newBottomInsetConstant_Hi
            }
        }

        let x = 0 as CGFloat
        let y = chatSpaceHeight - self.messageFieldBorderInsetFinal!
        let lineFrame = CGRect(x: x, y: y, width: chatSpaceWidth, height: 0.5)
        let lineSeparatorView = UIView(frame: lineFrame)
        lineSeparatorView.backgroundColor = UIColor.lightGray
        chatSpaceView.addSubview(lineSeparatorView)
        
        self.lineSeparatorView = lineSeparatorView
    }
    
    func addTextField() {
        let chatSpaceView = self.chatSpaceView!
        let insetUnit = (viewParameters?.insetUnit)!
        
        let tFrameWidth = chatSpaceView.frame.width - (2 * insetUnit)
        let tfFrame = CGRect(x: 0, y: 0, width: tFrameWidth, height: 31)
        let textField = UITextField(frame: tfFrame)
        self.textFieldView = textField
        
        let messageFieldBorderInsetFinal = self.messageFieldBorderInsetFinal!
        
        textField.frame.origin.x = insetUnit
        textField.frame.origin.y = chatSpaceView.frame.height - messageFieldBorderInsetFinal
        //        textField.frame.origin.y = chatSpaceView.frame.height - insetUnit - textField.frame.height
        
        textField.backgroundColor = UIColor.clear
        //        textField.adjustsFontSizeToFitWidth
        textField.minimumFontSize = 14
        textField.font = UIFont(name: "Avenir Next", size: 14)
        textField.returnKeyType = .send
        textField.enablesReturnKeyAutomatically = true
        
        chatSpaceView.addSubview(textField)
        textField.delegate = self
        self.lineSeparatorView!.bringSubview(toFront: chatSpaceView)
    }
    
    
    
    
    func addTableView() {
        let imageGridUnit = chatSpaceView!.bounds.height / 69
        let imageWidthRatio = 42 / 414 as CGFloat
        let imageWidth = imageWidthRatio * UIScreen.main.bounds.width
        
        let tableX = (viewParameters?.insetUnit)!
        
//        let tableY = (imageGridUnit * 6) + (1.5 * (viewParameters?.insetUnit)!)
        let tableY = imageWidth + (1.5 * (viewParameters?.insetUnit)!)
        let tableWidth = chatSpaceView!.bounds.width - (1.2 * (viewParameters?.insetUnit)!)
        let tableHeight = chatSpaceView!.bounds.height - tableY - messageFieldBorderInsetFinal! - ((viewParameters?.insetUnit)! * 0)
        
        let tableViewFrame = CGRect(x: tableX, y: tableY, width: tableWidth, height: tableHeight)
        let messagesTableView = UITableView(frame: tableViewFrame)
        
        messagesTableView.separatorStyle = .none
        messagesTableView.backgroundColor = UIColor.white
//        var footer = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 45))
//        footer.backgroundColor = UIColor.clear
//        messagesTableView.tableFooterView = footer
        
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        messagesTableView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        messagesTableView.reloadData()
        messagesTableView.allowsSelection = false
        messagesTableView.backgroundColor = UIColor.white
        
        self.chatSpaceView!.addSubview(messagesTableView)
        self.messagesTableView = messagesTableView
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func addThumbnailImageView() {
        let chatSpaceView = self.chatSpaceView!
        
        let chatSpaceHeight = chatSpaceView.bounds.height
        let imageGridUnit = chatSpaceHeight / 69
        
        let imageWidthRatio = 42 / 414 as CGFloat
        let imageWidth = imageWidthRatio * UIScreen.main.bounds.width
        
        let imageX = (viewParameters?.insetUnit)!
        let imageY = (viewParameters?.insetUnit)!
//        let imageWidth = (imageGridUnit * 6) as CGFloat
        let imageHeight = imageWidth
        
        let imageFrame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        let thumbImageView = UIImageView(frame: imageFrame)
        thumbImageView.contentMode = .scaleAspectFill
        thumbImageView.layer.cornerRadius = 3
        thumbImageView.layer.masksToBounds = true
        thumbImageView.clipsToBounds = true
        
        chatSpaceView.addSubview(thumbImageView)
        self.thumbImageView = thumbImageView
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.showUserProfileThumbnail_Segue(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.thumbImageView!.addGestureRecognizer(tapGesture)
        self.thumbImageView!.isUserInteractionEnabled = true
    }
    
    @objc func showUserProfileThumbnail_Segue(_ sender: UITapGestureRecognizer) {
        
//        self.selectedUser ["aboutMe": "",
//        "userThumbnailURLString": "https://firebasestorage.googleapis.com/v0/b/ximo-app.appspot.com/o/userDisplayPhotos%2FjZJX8nUCiSOh3UVPVLsvqDMLeTA2%2F556089518761.305.jpg?alt=media&token=b78b5f15-5fa7-4fe3-ba9c-4479db0c5da4",
//        "blurState": "mediumBlur",
//        "visus": "false",
//        "school": "",
//        "userDisplayName": "Cliff",
//        "lookingFor": "", "userPrivacyPreference": "false", "userMinAge": "", "UID": "jZJX8nUCiSOh3UVPVLsvqDMLeTA2", "height": "", "isOnline": "false", "distanceTo": "100 ft", "userMaxAge": "", "work": "", "userAge": "25", "gridThumb_URL": "https://firebasestorage.googleapis.com/v0/b/ximo-app.appspot.com/o/userDisplayPhotos%2FjZJX8nUCiSOh3UVPVLsvqDMLeTA2%2F556089518697.563.jpg?alt=media&token=074aef79-56b5-4570-962f-8dc43cca7c5f"]
////
        
        /*
         let otherUID = selectedUser["UID"]!
         if let gridURL = selectedUser["gridThumb_URL"] {
         self.selectedUser["full_URL_4"]
         self.selectedUser["full_URL_3"]
         self.selectedUser["full_URL_2"]
         selectedUser["userThumbnailURLString"]!))
         let displayName = self.selectedUser["userDisplayName"]!
         let userAge = self.selectedUser["userAge"]!
         lectedUser["school"]
         f.selectedUser["distanceTo"]!
         edUser["aboutMe"]!
         self.selectedUser["isOnline"]!
         self.selectedUser["height"] {
         self.selectedUser["lookingFor"] {
         f.otherPrivacy = selectedUser["userPrivacyPreference"]!
         */
        
//        let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
//            userInfoDict["UID"] == blockedUID
//        })
        
        //configure user object AS Selected user
        if self.didPresentFromChatList == true {
            if self.didLoadUserFromDB == true {
                self.performSegue(withIdentifier: "showProfileFromThumbnail", sender: self)
            }
        } else {
//            self.performSegue(withIdentifier: "showProfileFromThumbnail", sender: self)
            
            //LOGIC: if ur segueing from profile to chat then back to profile, ou shouldnt present profile again (will create a 2profile sandwich with chat inbetween)
            self.backButtonSegue_Helper()
        }
            

        
    }
    

    func addNameLabel() {
        let chatSpaceView = self.chatSpaceView!
        let chatSpaceHeight = chatSpaceView.bounds.height
        let thumbImageView = self.thumbImageView!
        
        let imageGridUnit = chatSpaceHeight / 69
        let imageWidthRatio = 42 / 414 as CGFloat
        let imageWidth = imageWidthRatio * UIScreen.main.bounds.width
        
        //get height using
        let textFieldCGSize = self.sizeOfCopy(string: "Hello", constrainedToWidth: 200, fontName: "AvenirNext-DemiBold", fontSize: 17)
        let tH = ceil(textFieldCGSize.height)
        
        let insetUnit = (viewParameters?.insetUnit)!
//        let nLabelX = insetUnit + (6 * imageGridUnit) + (insetUnit * 0.33)
        let nLabelX = insetUnit + imageWidth + (insetUnit * 0.33)
        let topPaddingError = 2 as CGFloat
        let nLabelY = insetUnit - topPaddingError
        let nLabelW = 120 as CGFloat
        var nLabelH = 31 as CGFloat
        nLabelH = tH
        
        //
        let f = CGRect(x: nLabelX, y: nLabelY, width: nLabelW, height: nLabelH)
        let nameLabel = UITextView(frame: f)
        
        //
        nameLabel.text = "JohnSmith"
        chatSpaceView.addSubview(nameLabel)
        self.nameLabel = nameLabel
        
        self.nameLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 17)!
        self.nameLabel!.textAlignment = .left
        self.nameLabel!.isEditable = false
        self.nameLabel!.isScrollEnabled = false
        self.nameLabel!.isSelectable = false
        self.nameLabel!.textContainer.lineFragmentPadding = 0
        self.nameLabel!.textContainerInset = .zero
        self.nameLabel!.textColor = UIColor.black
        self.nameLabel!.backgroundColor = UIColor.clear
        self.nameLabel!.addCharacterSpacing(withValue: 0.3)
        
//        let nameLabelFrame = CGRect(x: nLabelX, y: nLabelY, width: nLabelW, height: nLabelH)
//        let nameLabel = UILabel(frame: nameLabelFrame)
//        nameLabel.font = UIFont(name: "Avenir Next", size: 17)
//        nameLabel.textColor = UIColor.black
//        nameLabel.text = "Luke"
//        chatSpaceView.addSubview(nameLabel)
//        self.nameLabel = nameLabel
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }

    var userStatusLabel: UILabel?
    
    func addStatusLabel() {
        let chatSpaceView = self.chatSpaceView!
        let chatSpaceHeight = chatSpaceView.bounds.height
        let nameLabel = self.nameLabel!
        
        let imageGridUnit = chatSpaceHeight / 69
        let imageWidthRatio = 42 / 414 as CGFloat
        let imageWidth = imageWidthRatio * UIScreen.main.bounds.width
        
        let insetUnit = (viewParameters?.insetUnit)!
//        let sLabelX = insetUnit + (6 * imageGridUnit) + (insetUnit * 0.33)
        let sLabelX = insetUnit + imageWidth + (insetUnit * 0.33)
        let sLabelY = insetUnit + nameLabel.bounds.height * 0.5
        let sLabelW = 120 as CGFloat
        let sLabelH = 31 as CGFloat
        
        let sLabelFrame = CGRect(x: sLabelX, y: sLabelY, width: sLabelW, height: sLabelH)
        let sLabel = UILabel(frame: sLabelFrame)
        sLabel.font = UIFont(name: "Avenir Next", size: 10)
        sLabel.textColor = UIColor.lightGray
//        sLabel.text = "Online"
        chatSpaceView.addSubview(sLabel)
        self.userStatusLabel = sLabel
    }
    
    func addSliderMask() {
        let chatSpaceView = self.chatSpaceView!
        let chatSpaceHeight = chatSpaceView.bounds.height
        let chatSpaceWidth = chatSpaceView.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        
        //create a 68x176 view to hold new mask layer
        let maskWidth = 176 as CGFloat
        let maskHeight = 68 as CGFloat
        let maskX = (chatSpaceWidth - maskWidth) * 0.5
        
        
//        let yError = 0.75 as CGFloat
//        let yErrorRatio = 0.75 / screenHeight as CGFloat
//        var yError = yErrorRatio * screenHeight
//        if screenHeight > 750 as CGFloat {
//            yError = 0.75 as CGFloat
//        }
        
        
        let yError = 0.75 as CGFloat
        
        
        let maskY = chatSpaceHeight - maskHeight + yError
        
        let sliderMaskFrame = CGRect(x: maskX, y: maskY, width: maskWidth, height: maskHeight)
        let sliderMaskView = UIImageView(frame: sliderMaskFrame)
        
        sliderMaskView.backgroundColor = UIColor.white
        sliderMaskView.image = UIImage(named: "sliderMask_WithError")
        chatSpaceView.addSubview(sliderMaskView) ////
        sliderMaskView.contentMode = .scaleAspectFit
        
        self.sliderMaskImageView = sliderMaskView
        
        if self.swap != true {
            self.sliderMaskImageView!.isHidden = true
        } else {
            
        }
    }
    
    func addSliderBoxView() {
        let chatSpaceView = self.chatSpaceView!
        let chatSpaceHeight = chatSpaceView.bounds.height
        let chatSpaceWidth = chatSpaceView.bounds.width
    
        //slider image view
        let frameWidth = 100 as CGFloat
        let frameHeight = 50 as CGFloat
        
        let xFrame = ( chatSpaceWidth - frameWidth ) * 0.5
        let yFrame = chatSpaceHeight - frameHeight
        let wFrame = 100 as CGFloat
        let hFrame = 50 as CGFloat
        
        let sliderButtonBox = CGRect(x: xFrame, y: yFrame, width: wFrame, height: hFrame)
        let sliderImageHolder = UIImageView(frame: sliderButtonBox)
        self.sliderButtonImageView = sliderImageHolder
        
        sliderImageHolder.image = UIImage(named: "sliderOutlineEmpty")
        chatSpaceView.addSubview(sliderImageHolder) ////
    }
    
    func addRotationAnimation() {
        
        let duration: CFTimeInterval = 1.0
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = .infinity
        
        self.rotatingLayer?.add(rotateAnimation, forKey: "rotateRing")

    }
    
    var rotatingLayer: CAShapeLayer?
    
    func addRotatingLayer(shouldGrow: Bool, LRC: String) {
        //print("will addRotatingLayer")

        let circularPath = UIBezierPath(arcCenter: .zero, radius: 15, startAngle: 0, endAngle: 0.76 * (2 * CGFloat.pi), clockwise: true)
        
        pulsatingLayer = CAShapeLayer()
        pulsatingLayer!.path = circularPath.cgPath
        
        pulsatingLayer!.fillColor = UIColor.clear.cgColor
        pulsatingLayer!.lineWidth = 1.5
        pulsatingLayer!.strokeColor = UIColor.white.cgColor
        pulsatingLayer!.lineCap = kCALineCapRound
        
        var cX = 25 as CGFloat

        if LRC == "L" {
            //
        } else if LRC == "C"  {
            cX = 50 as CGFloat
        } else {
            cX = 72 as CGFloat
        }
        
        //print("cv final ", cX)
        let cY = 25 as CGFloat
        let cPoint = CGPoint(x: cX, y: cY)
       
        pulsatingLayer!.position = cPoint
        self.rotatingLayer = pulsatingLayer
        self.sliderButtonImageView!.layer.addSublayer(pulsatingLayer!)
        
        let duration: CFTimeInterval = 1.0
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(Double.pi * 2.0)
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount = .infinity
        
        if shouldGrow == true {
            
                let grow = CABasicAnimation(keyPath: "transform.scale")
                grow.fromValue = 0.2
                grow.toValue = 1.0
                grow.duration = 0.2
                grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
                self.rotatingLayer?.add(grow, forKey: "growRotatingRing")
        }
        
        // add loading state for one way show or reveal loading
        if LRC == "C" {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.transitionCrossDissolve], animations: {
                self.stateLabel!.textColor = UIColor.white
                self.stateLabel!.alpha = 1
                self.stateLabel!.text = "Unlocking Profile..."
            }) { (true) in
                //            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                //                stateLabel.alpha = 0
                //            }, completion: nil)
            }
        }
        
        self.rotatingLayer?.add(rotateAnimation, forKey: "rotateRing")
    }
    
    
    func addSliderKey_LHS() {
        //Called from within SwapUIControlFlow
        let chatSpaceView = self.chatSpaceView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        let kX = kInset
        let kY = kInset
        
        //
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        let kImageView = UIImageView(frame: kFrame)
        
        //
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
        //
        sliderButtonImageView.addSubview(kImageView)
        self.keyImageView = kImageView
        
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wX_i = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")

        sliderButtonImageView.addSubview(wImageViewLHS_i)
        self.wImageViewLHS_i = wImageViewLHS_i
        
    }
    
    
    func addSliderKey_N() {
        //Called from within SwapUIControlFlow
        let chatSpaceView = self.chatSpaceView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        
        let kInset = 7 as CGFloat
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        //...add slider key frame
        let kX = kInset
        let kY = kInset
        
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        //as UIImage
        let kImageView = UIImageView(frame: kFrame)
        
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
//        //add slider key tapRecognizer
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.slideToSwap(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        kImageView.addGestureRecognizer(tapGesture)
//        kImageView.isUserInteractionEnabled = true
        //***** IMPORTANT NOTICE:
        /*
         1. Observed Error: kImageView was not receiving touches
         2. Error Solution: setting the .isUserInteractionEnabled property of the parent view of the kImageView resolved the issue
         */
        
        sliderButtonImageView.addSubview(kImageView) /////////
//        sliderButtonImageView.isUserInteractionEnabled = true //*****
        self.keyImageView = kImageView
        
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wX_i = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        
        //define the starting position for the right wiggler
        let wXRHS_i = kInset + kWidth + mRo + cornerRadiusInset
        
        let wFrameRHS_i = CGRect(x: wXRHS_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_i = UIImageView(frame: wFrameRHS_i)
        wImageViewRHS_i.image = UIImage(named: "sliderWiggler_f")
        
        sliderButtonImageView.addSubview(wImageViewLHS_i)
        self.wImageViewLHS_i = wImageViewLHS_i
        
        sliderButtonImageView.addSubview(wImageViewRHS_i)
        self.wImageViewRHS_i = wImageViewRHS_i
        
        wImageViewRHS_i.alpha = 0 as CGFloat
    }
    
    //Should be RHS
    func addSliderKey_Outgoing_P() {
        //Called from within SwapUIControlFlow
        let chatSpaceView = self.chatSpaceView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        
        let kInset = 7 as CGFloat
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        //...add slider key frame
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrame = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        //as UIImage
        let kImageView = UIImageView(frame: kFrame)
        
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
        sliderButtonImageView.addSubview(kImageView) /////////
        //        sliderButtonImageView.isUserInteractionEnabled = true //*****
        self.keyImageView = kImageView
        
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wWidth = mRi + mRo
        let wHeight = mRh
        let wXRHS_f = kInset + kWidth
        let wY = kHeight - mRh + sI + wYError
        
        //...2. RHS Wiggler
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_f = UIImageView(frame: wImageViewFrameRHS_f)
        wImageViewRHS_f.image = UIImage(named: "sliderWiggler_f")
        
        self.sliderButtonImageView!.addSubview(wImageViewRHS_f)
        self.wImageViewRHS_i = wImageViewRHS_f
    }
    
//    //Adding slider key to LHS
//    func addSliderKey_Outgoing_P() {
//        //Called from within SwapUIControlFlow
//        let chatSpaceView = self.chatSpaceView!
//        let sliderButtonImageView = self.sliderButtonImageView!
//
//        //init |K
//        let sWidth = 100 as CGFloat
//        let sHeight = 50 as CGFloat
//        let sI = 7 as CGFloat
//
//        let kInset = 7 as CGFloat
//        let kWidth = 39.5 as CGFloat
//        let kHeight = 36 as CGFloat
//
//        //...add slider key frame
//        let kX = kInset
//        let kY = kInset
//
//        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
//        //as UIImage
//        let kImageView = UIImageView(frame: kFrame)
//
//        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
//
//
//        sliderButtonImageView.addSubview(kImageView) /////////
//        //        sliderButtonImageView.isUserInteractionEnabled = true //*****
//        self.keyImageView = kImageView
//
//        //add slider key wiggler
//        //...wiggler |K
//        let mRo = 7 as CGFloat
//        let mRi = 12 as CGFloat
//        let mRh = 10.3604 as CGFloat
//        let cornerRadiusInset = mRh
//
//        //...
//        let wWidth = mRi + mRo
//        let wHeight = mRh
//
//        let wXError = -0.1000 as CGFloat
//        let wYError = 0.3604 as CGFloat
//        let wX_i = kInset + kWidth - mRi + wXError
//        let wY = kHeight - mRh + sI + wYError
//
//        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
//        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
//        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
//
//        //define the starting position for the right wiggler
//        let wXRHS_i = kInset + kWidth + mRo + cornerRadiusInset
//
//        let wFrameRHS_i = CGRect(x: wXRHS_i, y: wY, width: wWidth, height: wHeight)
//        let wImageViewRHS_i = UIImageView(frame: wFrameRHS_i)
//        wImageViewRHS_i.image = UIImage(named: "sliderWiggler_f")
//
//        sliderButtonImageView.addSubview(wImageViewLHS_i)
//        self.wImageViewLHS_i = wImageViewLHS_i
//
//        sliderButtonImageView.addSubview(wImageViewRHS_i)
//        self.wImageViewRHS_i = wImageViewRHS_i
//
//        wImageViewRHS_i.alpha = 0 as CGFloat
//    }

    func addSliderKey_Incoming_P() {
        //Called from within SwapUIControlFlow
        let chatSpaceView = self.chatSpaceView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        
        let kInset = 7 as CGFloat
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        //...add slider key frame
        let kX = kInset
        let kY = kInset
        
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        //as UIImage
        let kImageView = UIImageView(frame: kFrame)
        
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
        sliderButtonImageView.addSubview(kImageView) /////////
        //        sliderButtonImageView.isUserInteractionEnabled = true //*****
        self.keyImageView = kImageView
        
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wX_i = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        
        //define the starting position for the right wiggler
        let wXRHS_i = kInset + kWidth + mRo + cornerRadiusInset
        
        let wFrameRHS_i = CGRect(x: wXRHS_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_i = UIImageView(frame: wFrameRHS_i)
        wImageViewRHS_i.image = UIImage(named: "sliderWiggler_f")
        
        sliderButtonImageView.addSubview(wImageViewLHS_i)
        self.wImageViewLHS_i = wImageViewLHS_i
        
        sliderButtonImageView.addSubview(wImageViewRHS_i)
        self.wImageViewRHS_i = wImageViewRHS_i
        
        wImageViewRHS_i.alpha = 0 as CGFloat
    }
    
    func addSliderKey_Outgoing_D() {
        //Called from within SwapUIControlFlow
        let chatSpaceView = self.chatSpaceView!
        let sliderButtonImageView = self.sliderButtonImageView!

        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat

        let kInset = 7 as CGFloat
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat

        //...add slider key frame
        let kXR = kInset + kWidth + kInset
        let kY = kInset

        let kFrame = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        //as UIImage
        let kImageView = UIImageView(frame: kFrame)

        kImageView.image = UIImage(named: "sliderKeyWhiteFill")

        sliderButtonImageView.addSubview(kImageView) /////////
        //        sliderButtonImageView.isUserInteractionEnabled = true //*****
        self.keyImageView = kImageView

        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh

        //...
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat

        let wWidth = mRi + mRo
        let wHeight = mRh
        let wXRHS_f = kInset + kWidth
        let wY = kHeight - mRh + sI + wYError

        //...2. RHS Wiggler
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_f = UIImageView(frame: wImageViewFrameRHS_f)
        wImageViewRHS_f.image = UIImage(named: "sliderWiggler_f")

        self.sliderButtonImageView!.addSubview(wImageViewRHS_f)
        self.wImageViewRHS_i = wImageViewRHS_f
        
//        //
//        //deactivate old
//        kImageView.isUserInteractionEnabled = false
//
//        //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        sliderButtonImageView.addGestureRecognizer(tapGesture)
//        sliderButtonImageView.isUserInteractionEnabled = true
    
    }
    
//    //for LHS slideToUnlock & NOT slideTOSWAP
//    func addSliderKey_Outgoing_D() {
//        //Called from within SwapUIControlFlow
//        let chatSpaceView = self.chatSpaceView!
//        let sliderButtonImageView = self.sliderButtonImageView!
//
//        //init |K
//        let sWidth = 100 as CGFloat
//        let sHeight = 50 as CGFloat
//        let sI = 7 as CGFloat
//
//        let kInset = 7 as CGFloat
//        let kWidth = 39.5 as CGFloat
//        let kHeight = 36 as CGFloat
//
//        //...add slider key frame
//        let kX = kInset
//        let kY = kInset
//
//        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
//        //as UIImage
//        let kImageView = UIImageView(frame: kFrame)
//
//        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
//
//        sliderButtonImageView.addSubview(kImageView) /////////
//        //        sliderButtonImageView.isUserInteractionEnabled = true //*****
//        self.keyImageView = kImageView
//
//        //add slider key wiggler
//        //...wiggler |K
//        let mRo = 7 as CGFloat
//        let mRi = 12 as CGFloat
//        let mRh = 10.3604 as CGFloat
//        let cornerRadiusInset = mRh
//
//        //...
//        let wXError = -0.1000 as CGFloat
//        let wYError = 0.3604 as CGFloat
//
//        let wWidth = mRi + mRo
//        let wHeight = mRh
//        let wXRHS_f = kInset + kWidth
//        let wY = kHeight - mRh + sI + wYError
//
//        //...2. RHS Wiggler
//        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
//        let wImageViewRHS_f = UIImageView(frame: wImageViewFrameRHS_f)
//        wImageViewRHS_f.image = UIImage(named: "sliderWiggler_f")
//
//        self.sliderButtonImageView!.addSubview(wImageViewRHS_f)
//        self.wImageViewRHS_i = wImageViewRHS_f
//    }
    
    func addSliderKey_Incoming_D() {
        self.addSliderKey_Outgoing_D()
        
        //
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //deactivate old
        kImageView.isUserInteractionEnabled = false

        //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
    }
    
    func addBackButton() {
        
        var sBarH = UIApplication.shared.statusBarFrame.size.height
        let insetFromStatusBar = 5 as CGFloat
        
        if let sourceVC = self.sourceVC {
            if sourceVC == "UserProfileViewController" {
                sBarH = self.importedStatusBarHeight!
            }
        }
        
    
        
        //init properties
        let bW = 30 as CGFloat
        let bH = 30 as CGFloat
        let bX = (self.viewParameters?.insetUnit)!
//        let bY = ((self.viewParameters?.topInset)! - bH) * 0.5
        let bY = sBarH + insetFromStatusBar
        
        //init frame & view
        let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
        let backButtonImageView = UIImageView(frame: bFrame)
        
        //init view properties
//        backButtonImageView.image = UIImage(named: "chatScreenBackButton")
        backButtonImageView.image = UIImage(named: "backChats_Reg")

        //add to subview
        self.view.addSubview(backButtonImageView)
        self.backButtonImageView = backButtonImageView
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.backButtonSegue(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.backButtonImageView!.addGestureRecognizer(tapGesture)
        self.backButtonImageView!.isUserInteractionEnabled = true
    }
    
    func addShimmerView() {
        let cView = self.chatSpaceView!
        let mView = self.sliderMaskImageView!
        
        let sW = self.sliderMaskImageView!.bounds.width
        let sH = 17 as CGFloat
        let sX = ((self.viewParameters?.screenWidth)! - sW) * 0.5
        let sY = cView.frame.maxY
        
        let shimmerFrame = CGRect(x: sX, y: sY, width: sW, height: sH)
        let shimmerView = FBShimmeringView()
        shimmerView.frame = shimmerFrame
        self.view.addSubview(shimmerView)
        
        self.shimmerView = shimmerView
    }
    
    func addStateLabel() {
        
        let cView = self.chatSpaceView!
        let mView = self.sliderMaskImageView!
        let shimmerView = self.shimmerView!
        
        let sW = shimmerView.bounds.width
        let sH = shimmerView.bounds.height
        let sX = 0 as CGFloat
        let sY = 0 as CGFloat
        
        let sFrame = CGRect(x: sX, y: sY, width: sW, height: sH)
        let sLabel = UILabel(frame: sFrame)
        sLabel.font = UIFont(name: "Avenir-Light", size: 10)
//        sLabel.textColor = UIColor.lightText
        sLabel.textColor = UIColor.white
        sLabel.textAlignment = .center
        
        shimmerView.addSubview(sLabel)
        self.stateLabel = sLabel
    }
    
    func adjustUIViewIfUnlockedUser() {
        //print("CSVC adjustUIViewIfUnlockedUser", self.unlockedUsers)
        if self.unlockedUsers.contains(self.otherUID!) {
            
            //print("willRemoveSubviewsMasks")
            let maskView = self.sliderMaskImageView!
            let sliderButtonView = self.sliderButtonImageView!
           
            //remove slider views
            maskView.removeFromSuperview()
            sliderButtonView.removeFromSuperview()
            if let kView = self.keyImageView {
                kView.removeFromSuperview()
            }
            
            if let wView = self.wImageViewRHS_i {
                wView.removeFromSuperview()
            }
            
            if let stateLabel = self.stateLabel {
                stateLabel.removeFromSuperview()
            }
            
        }
    }
    
    func addImageLoader() {
        let imageX = 0 as CGFloat
        let imageY = 0 as CGFloat
        let imageWidth = 250 as CGFloat
        let imageHeight = 250 as CGFloat
        
        let imageFrame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        let imageLoaderView = UIImageView(frame: imageFrame)
        
        view.addSubview(imageLoaderView)
        self.imageLoaderView = imageLoaderView
        self.imageLoaderView!.isHidden = true
    }
    
    func addBlurView() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = UIScreen.main.bounds
        
        view.addSubview(blurView)
        self.blurView = blurView
        self.blurView!.alpha = 0
    }
    
    func addTransparencyView() {
        
//        self.arrestoMomento()
        
        let transparencyView = UIView()
        transparencyView.frame = UIScreen.main.bounds
        transparencyView.backgroundColor = UIColor.black
        transparencyView.alpha = 0
        
        view.addSubview(transparencyView)
        self.transparencyView =  transparencyView
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            //print("UIViewWillAnimateAlphaTransparency to 0.8")
            transparencyView.alpha = 0.8
        }, completion: nil)
        
        //        self.transparencyView!.alpha = 1
    }
    
    //MARK: FINISH Drawing Views & Subviews
    
   
   
    //MARK: SWAP UI CONTROL FLOW ....................................................................................................................................................
    
    func swapUIControlFlow(fromLiveObserver: Bool) { //called when viewDidLoad & everytime self.vars update
        //SET ALL CASES HERE THEN CONFIGURE UI CASES INDEPENDENTLY
        //print("IN swapUIControlFlow")
        
        //B. if no one has previously initiated a swap request
        //PROBLEM: CHATLISTVC NOT PASSING CASE
        if self.swapRequestDoesExist! == false {
            //B1
            //If my profile is hidden
            //print("IN B1")
            
            if self.selfVisus! == "false" {
                //B1.1
                //print("SWAPUICONTROLFLOW B1.1, selfVisus is false")
                
                //&& if other user's profile is hidden
                if self.otherVisus! == "false" {
                    //print("B1.1.a")
                    //THEN self has to instantiate a new swapRequest & has to pass selfPPrivatePhoto
                    //                    self.swapButton.setImage(UIImage(named: "chatRequestSlider"), for: .normal)
                    //                    self.sliderLabel.text = "Swap Profiles"
                    self.swapUI_2Way_N()
                }
                    
                    //&& if other user's profile is visible
                else if self.otherVisus! == "true" {
                    //print("B1.1.b")
                    self.swapUI_1WayShow_N()
                }
                
                //If my profile is visible
            } else if self.selfVisus! == "true"  {
                //B1.2
                //print("B1.2")
                
                //&& if other user's profile is hidden:                                                                      O.K.!
                if self.otherVisus! == "false" {
                    //print("B1.2.1")
                    //THEN self can ask the hidden user to reveal
                    self.swapUI_1WayReveal_N()
                    
                    //&& if other user's profile is visible                                                                      O.K.!
                } else if self.otherVisus! == "true" {
                    //THEN
                    //print("B1.2.2")
                    
                    //set UI CASE WHERE THERE ARE NO SWAPS
                    
                }
            }
        }
        
        //A. if either user has previously initiated a swap request
        //print("swapUIControlFlow")
        
        if self.swapRequestDoesExist! == true {
            
            //if self initiated request
            //print("A1")
            
            if selfInitiatedSwapRequest! == true {
                //print("A1.1")
                self.selfSwitchSwapResponseCasesUI(fromLiveObserver: fromLiveObserver)
                
                //if other initiated request
                
            } else if selfInitiatedSwapRequest! == false {
                //print("A1.2")
                self.othSwitchResponseCasesUI(fromLiveObserver: fromLiveObserver)
            }
        }
    }
    
    //MARK: SelfSwitchResponseCases
    
    func selfSwitchSwapResponseCasesUI(fromLiveObserver: Bool) {
        //Function called whenever self is initiator of swap request
        //print("CSVC. selfSwitchSwapResponseCasesUI NOW Switching where initiator is self")
        switch self.swapRequestStatus {
            //when self has initiated swap
            
        //&& swap response is true
        case "true":
            //&& when self is hidden and other user is visible
            if self.selfVisus! == "false" && self.otherVisus! == "true" {
                //print("CASE TRUE. ")
                //                self.swapButton.setImage(UIImage(named: "askToShowApproved"), for: .normal)
                //                self.sliderLabel.text = "You Showed"
                if fromLiveObserver == true {
                    //print("XYZ")
                    //
                } else {
                    //print("AXYZ")
                    self.swapUI_1WayShow_D() //the result of your previous oneway show is complete
                }
                
                //&& when self is visible and other user is hidden
            } else if self.selfVisus! == "true" && self.otherVisus! == "false" {
                
                //result of previous oneWayAsk() is successful
                //                self.swapButton.setImage(UIImage(named: "askToShowApproved"), for: .normal)
                //                self.sliderLabel.text = "Approved"
                
                if fromLiveObserver == true {
                    self.swapUI_1WayReveal_Outgoing_Live_D()
                } else {
                    self.swapUI_1WayReveal_D()
                }
                
                //&& when both users are hidden
            } else if self.selfVisus! == "false" && self.otherVisus! == "false" {
                //swap complete
                //                self.swapButton.setImage(UIImage(named: "approvedRequestSlider"), for: .normal)
                //                self.sliderLabel.text = "User Approved"
                if fromLiveObserver != true {
                    //print("selfSwitchSwapResponseCasesUI FIRE swapUI_2Way_Outgoing_D")
                    self.swapUI_2Way_Outgoing_D()
                } else {
                    //print("Calling animate2WayOutgoingApproved_FadeToBlue from selfSwitchSwapResponseCasesUI ")
                    self.animate2WayOutgoingApproved_FadeToBlue()
                }
                
            } else {
                //
            }
            
        //&& swap response is nil
        case "false":
            //print("CVSC Case. False")
            //&& when self is hidden and other user is visible
            if self.selfVisus == "false" && self.otherVisus! == "true" {
                //empty: would be one-way reveal
                //&& when self is visible and other user is hidden
                
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //result of your previous oneWayAsk() is pending
                
                //                self.swapButton.setImage(UIImage(named: "askToShowPending"), for: .normal)
                //                self.sliderLabel.text = "Pending Approval"
                self.swapUI_1WayReveal_P() //CHANGE
                
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap request pending
                //                self.swapButton.setImage(UIImage(named: "pendingRequestSlider"), for: .normal)
                //                self.sliderLabel.text = "You Requested a Swap"
                
                if fromLiveObserver == true {
                    //                    self.animateSlideToAcceptSwap()
                } else {
                    self.swapUI_2Way_Outgoing_P()
                }
                
            } else {
                //
            }
            
        //&& swap response is default = "nil"
        default:
            
            break
        }
    }
    
    //BUG: CVC not updating on user did change visus, upon reveal
    
    //MARK: OthSwitchResponseCases
    
    func animateIncomingOneWayShow_Loading() {
        if self.didAnimateLiveFillCircle == false {
            self.sliderButtonImageView?.isUserInteractionEnabled = false
            self.pulsatingLayer?.isHidden = true
            self.ringLayer?.isHidden = true
            self.addRotatingLayer(shouldGrow: false, LRC: "C")
        }
    }
    
    func othSwitchResponseCasesUI(fromLiveObserver: Bool) { //REMEMBER: To call these functions when the didSet variables are updated
        //Function called for all cases where other is initiator of swapRequestID
        
        switch self.swapRequestStatus {
            //when self has initiated swap
            
        //&& swap response is true
        case "true":
            //&& when self is hidden and other user is visible
            if self.selfVisus == "false" && self.otherVisus! == "true" {
                //&& when self is visible and other user is hidden
                
                //you previously revealed your profile to this user
                //need to remove the key view and replace with symbol that revealed previously.
                
                if fromLiveObserver == true {
//                    self.swapUI_1WayReveal_Outgoing_Live_D()
                } else {
//                    self.swapUI_1WayReveal_D()
                }
                
                
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //result of your previous oneWayAsk() is successful
                //button activates...
                //                self.swapButton.setImage(UIImage(named: "askToShowApproved"), for: .normal)
                //                self.sliderLabel.text = "User Revealed Their Profile"
                
                //print("UNIVERSAL incomingOneWayShow")
                //print("addRotatingLayer")
                
                self.animateIncomingOneWayShow_Loading()
            
                if fromLiveObserver == true {
                    //if live incomingOneWayShow, animate circle to completion
                    
                    
                } else {
                    self.swapUI_1Way_OtherInit_D()
                }
                
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap complete
                
                if fromLiveObserver == true {
                    //print("Z  fromLiveObserver", fromLiveObserver)
                    
                } else {
                    //print("X  fromLiveObserver", fromLiveObserver)
                    self.swapUI_2Way_Incoming_D()
                }
                
            } else {
                //
            }
            
        //&& swap response is nil
        case "false":
            //print("CaseCheck. False")
            //&& when self is hidden and other user is visible
            //print("CHECK1 self.selfvisus,", self.selfVisus!)
            //print("CHECK2,", self.otherVisus!)
            if self.selfVisus! == "false" && self.otherVisus! == "true" {
                //empty: would be one-way reveal
                //                self.swapButton.setImage(UIImage(named: "askToShowPending"), for: .normal)
                //                self.sliderLabel.text = "Show Profile"
                
                //&& when self is visible and other user is hidden
                self.swapUI_1Way_Incoming_P()
                
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //result of your previous oneWayAsk() is pending
                
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap request pending
                //print("HERO")
                
                if fromLiveObserver == true {
                    //why is this method being called after swapIsAccepted &
                    //.value observer method is observing status as false, and hence calling this branch. HMW switch off this branch from being called?
                    //print("ACCIO animateLive2WayIncomingRequest")
                    //Work the problem: what's the problem this method should not be calling when outgoing request is approved. Error happening because observerMethod is using .Value vs. .childCHanged, and calling swapUIControlFlow everytime .value is observed in DB. Need to only call this method when modifiedSwapStatus is true. If we only filter for true values, we'll negate false values which are needed to observe for requests. HOw to fix? Change .value to .childChanged methods... Do a test run with new & updated observers.
                    
                    self.animateLive2WayIncomingRequest()
                    
                } else {
                    self.swapUI_2Way_Incoming_P()
                }
                
            } else {
                //
            }
            
        //&& swap response is default = "false"
        default:
            //print("neither true nor false detecgted")
            break
        }
    }
    
    //MARK: 2Way Swap UI Functions....................................................................................................................................................
    
    struct sliderStateCopy {
        
    }
    
    func swapUI_2Way_N() {
        //print("swapUI_2Way_N")
        
        //add Slider Key
        self.addSliderKey_N()

        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        let s = self.stateLabel!
        
        //set slider box state
        sliderButtonImageView.image = UIImage(named: "sliderOutlineEmptyWhite")
        if self.messagesArray.isEmpty {
            
            s.text = "Say Something"
//            s.text = "Say Hello"
//            s.text = "Start Talking"
        } else  {
            s.text = "Swap Profiles"
        }
        
        s.alpha = 0
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToSwap_2Way_N))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        kImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        kImageView.isUserInteractionEnabled = true
        //condition is false until messagesArray.count > 0
        
    }
    
    func swapUI_2Way_Outgoing_P() {
        //print("swapUI_2Way_Outgoing_P")
        
        //add Slider Key
        self.addSliderKey_Outgoing_P()
        
        //the code below was needed when key was positioned LHS
        //        //
        //        let kImageView = self.keyImageView!
        //        let wImageViewLHS_i = self.wImageViewLHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        //
        //        //
        //        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        //        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        //        //print("ACCIO d")
        
        //
        //no action necessary, no tap gesture required, button disabled
        self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
        self.addRotatingLayer(shouldGrow: false, LRC: "L")
    }
    
    func swapUI_2Way_Outgoing_D() {
        //print("swapUI_2Way_Outgoing_D")
        
        //add Slider Key
        self.addSliderKey_Outgoing_D()
        
        //
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        
        //
        kImageView.isUserInteractionEnabled = false
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Outgoing(_:)))
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.animateRevealProfile()
    }
    
    func swapUI_2Way_Incoming_P() {
        //print("swapUI_2Way_Incoming_P")
        
        //add Slider Key
        self.addSliderKey_Incoming_P()
        
        //
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        //print("ACCIO e")
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.slideToAccept(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        kImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        kImageView.isUserInteractionEnabled = true
        
        self.addRotatingLayer(shouldGrow: false, LRC: "R")
        self.animate2WayRequest_Incoming_FadeInFadeOut_P()
    }
    
    func swapUI_2Way_Incoming_D() {
        //print("swapUI_2Way_Incoming_D")
        
        //add Slider Key
        self.addSliderKey_Incoming_D()
        
        //
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        
        //deactive old tap gesture
        kImageView.isUserInteractionEnabled = false
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        kImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.animateRevealProfile()
    }
    
    //MARK: 2WAY Action-Target Methods
    
    @objc func tapToSwap_2Way_N(_ sender: UITapGestureRecognizer) {
        //print("tapToSwap_2Way_N tap observed")
        
        guard self.guardForClearPhotoFirst() == true else { return }
        
        if self.messagesArray.isEmpty {
            self.animateStartTalking_Hint()
        } else {
            
            if let swapAlertSeen = UserDefaults.standard.string(forKey: "swapAlert_Seen") {
                if swapAlertSeen == "true" {

                    self.animateOutgoing2WaySwapRequest_Slide()
                    self.animatePendingReveal_Outgoing_Hint()
                    self.swapControlFlow()
                    self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
                    self.keyImageView!.isUserInteractionEnabled = false
                    self.sendRowToTopChatListForUID(withSourceCall: "tapToSwap_2Way_N")
                } else {

                    self.animateOutgoing2WaySwapRequest_Slide()
                    self.animatePendingReveal_Outgoing_Hint()
                    self.swapControlFlow()
                    self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
                    self.keyImageView!.isUserInteractionEnabled = false
                    self.sendRowToTopChatListForUID(withSourceCall: "tapToSwap_2Way_N")
                }
            } else {
                self.present(self.twoWaySwapAlert!, animated: true)
            }
        }
    }
    
    func tapToSwap_2Way_N_Helper() {
        self.animateOutgoing2WaySwapRequest_Slide()
        self.animatePendingReveal_Outgoing_Hint()
        self.swapControlFlow()
        self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
        self.keyImageView!.isUserInteractionEnabled = false
        self.sendRowToTopChatListForUID(withSourceCall: "tapToSwap_2Way_N")
    }
    
    func guardForClearPhotoFirst() -> Bool {
        if self.visibleDisplayPhoto != nil {
           return true
        } else {
            self.present(self.clearPhotoDeletedAlert!, animated: true, completion: nil)
            return false
        }
    }
    
    @objc func slideToAccept(_ sender: UITapGestureRecognizer) {
        //print("slideToAccept incoming swap tap observed")
        
        guard self.guardForClearPhotoFirst() == true else { return }

        self.swapControlFlow()
//        self.animateSlideToAcceptSwap()
        
        self.animateSlideToAcceptIncomingSwap() //reactivate
        
        //
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //deactivate old
        kImageView.isUserInteractionEnabled = false
        
        //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.sendRowToTopChatListForUID(withSourceCall: "slideToAccept")
    }
    
    @objc func slideToUnlock_Outgoing(_ sender: UITapGestureRecognizer) {
        //print("slideToUnlock_Outgoing")

        self.animateSlideToAcceptSwap() //use same animation as before
        self.swapControlFlow()
        
        self.sendRowToTopChatListForUID(withSourceCall: "slideToUnlock_Outgoing")
    }
    
    @objc func tapToUnlock_Incoming(_ sender: UITapGestureRecognizer) {
        //print("tapToUnlock_Incoming tap observed")
        
        //UI prepartion
        self.addTransparencyView()
        let transparencyView = self.transparencyView!
        
        //DB info changes
        self.updateSeenReceipt()
        self.updateUnlockedUsersArray()
        
        //Animate Views
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            //print("UIViewWillAnimateAlphaTransparency to 0.8")
            transparencyView.alpha = 0.8
        }, completion: nil)
        
        //Present DestinationVC
        self.stateLabel!.layer.removeAllAnimations()
        self.performSegue(withIdentifier: "toVoilaVC", sender: self)
        self.clearPhotoURLPath(withSource: "unlockIncomingSwap_2Way")

        self.sendRowToTopChatListForUID(withSourceCall: "tapToUnlock_Incoming")
    }
    
    func clearPhotoURLPath(withSource: String) {
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        var withRequestID = " "
        if let swapID = self.absExistingSwapRequestID {
            withRequestID = swapID
        }
        
        let newTimeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        //clear url on one-way reveal
        if withSource == "unlockOutgoingReveal_Approved" {
            //print("clearPhotoURLPath withSource unlockOutgoingReveal_Approved")
            let photoChildNode = "swapRecipientPhotoURL"
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
        
        //clear url on outgoing2WaySwap
        } else if withSource == "unlockOutgoingSwap_2Way" {
            //print("clearPhotoURLPath withSource unlockOutgoingSwap_2Way")
            let photoChildNode = "swapRecipientPhotoURL"
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            
        //clear url on incoming2WaySwap
        } else if withSource == "unlockIncomingSwap_2Way" {
            //print("clearPhotoURLPath withSource unlockIncomingSwap_2Way")
            let photoChildNode = "swapInitiatorPhotoURL"
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            
        //clear url on onewayshow
        } else if withSource == "incomingOneWayShow_Live" {
            //print("clearPhotoURLPath withSource incomingOneWayShow_Live")
            let photoChildNode = "swapInitiatorPhotoURL"
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
            self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
        }
        
        //update unlock timestamp (to update row position in chatlist)
        self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/swapRequestTimeStamp").setValue(newTimeStamp)
        self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/swapRequestTimeStamp").setValue(newTimeStamp)
    }
    
    @objc func tapToUnlock_Outgoing(_ sender: UITapGestureRecognizer) {
        //print("tapToUnlock_Outgoing tap observed")
        
        //add views
        self.addTransparencyView()
        let transparencyView = self.transparencyView!
        
        //update DB info
        self.updateSeenReceipt()
        self.updateUnlockedUsersArray()
        
        //Animate Views
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            //print("UIViewWillAnimateAlphaTransparency to 0.8")
            transparencyView.alpha = 0.8
        }, completion: nil)
        
        //Present DestinationVC
        self.stateLabel!.layer.removeAllAnimations()
        self.performSegue(withIdentifier: "toVoilaVC", sender: self)
        
        self.clearPhotoURLPath(withSource: "unlockOutgoingSwap_2Way")
        
        self.sendRowToTopChatListForUID(withSourceCall: "tapToUnlock_Outgoing")
        
    }

    //MARK: 2Way Animator Functions
    
    //start talking flash-fades once when viewController loads & each time slider is pressed while inactive
    
    func animate2WayRequest_Outgoing_FadeInFadeOut_P() {
        //print("animate2WayRequest_Outgoing_P")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Pending Approval..."
//        stateLabel.alpha = 1
        
        //before changing
//        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
//            stateLabel.alpha = 0
//        }) { (true) in
//
//        }
        
        //no repeat
        stateLabel.alpha = 0

        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            stateLabel.alpha = 1
        }) { (true) in

        }
        
//        UIView.animate(withDuration: 0.3, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
//            stateLabel.alpha = 1
//        }) { (true) in
//
//        }
    }
    
    func animate2WayRequest_Incoming_FadeInFadeOut_P() {
        //print("animate2WayRequest_Incoming_FadeInFadeOut_P")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Slide to Accept"
//        stateLabel.alpha = 1
        
        //with fadein fade out
//        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
//            stateLabel.alpha = 0
//        }) { (true) in
//
//        }
        
        //
        stateLabel.alpha = 0

        //without fadeinfade out
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseInOut], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.font = UIFont(name: "Avenir-Light", size: 10)!
            stateLabel.alpha = 1
        }) { (true) in
            
        }
        
    }
    
    var shimmerView: FBShimmeringView!
    
    func animateRevealProfile() {
        //print("IN animateRevealProfile")
        let cView = self.chatSpaceView!
        let mView = self.sliderMaskImageView!
        
        let sW = self.sliderMaskImageView!.bounds.width
        let sH = 17 as CGFloat
        let sX = ((self.viewParameters?.screenWidth)! - sW) * 0.5
        let sY = cView.frame.maxY
    
        let sFrame = CGRect(x: 0, y: 0, width: sW, height: sH)
        let sLabel = UILabel(frame: sFrame)
        sLabel.font = UIFont(name: "Avenir-Light", size: 10)
//        sLabel.textColor = UIColor.lightText
        sLabel.textColor = UIColor.white
        sLabel.textAlignment = .center
//        sLabel.text = "Reveal Profile"
        sLabel.text = "Tap to Open"

        self.stateLabel = sLabel

        //
        self.shimmerView!.addSubview(sLabel)
        
        self.shimmerView!.contentView = self.stateLabel!
        self.shimmerView!.isShimmering = false
//        self.shimmerView!.shimmeringPauseDuration = 0.3
//        self.shimmerView!.shimmeringSpeed = 100
//        self.shimmerView!.shimmeringOpacity = 0.2
//        self.shimmerView!.shimmeringOpacity = 0.8
//        self.shimmerView!.shimmeringDirection = .right
//        self.shimmerView!.shimmeringAnimationOpacity = 1.0
    }
    
    func animateSlideKey() {
        //1. Animate Key
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kInset
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        /*
         1. When key crosses tail-end, tail_L hides
         2. When key reaches right edge + corner radius, tail_R appears
         */
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kInset + kWidth
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        if self.swapState == "N" {
            //change key to white
            UIView.transition(with: kImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                kImageView.image = UIImage(named: "sliderKeyWhiteFill")
            }, completion: nil)
            
            //change wLHS to white
            UIView.transition(with: wImageViewLHS_i, duration: 0.3, options: .transitionCrossDissolve, animations: {
                wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
            }, completion: nil)
            
            //change outline to solid box orange
            UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
            }, completion: nil)
            //print("ACCIO c")

            //need to add dot animations later
        }
        
        if self.swapState == "P" {
            //change box to blue fill
            UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
            }, completion: nil)
        }
        
        //RUN ANIMATION
        if self.keyPosition == "L" {
            
            //KL -> KR
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
                kImageView.frame = kFrameR
                self.keyPosition = "R"
            }, completion: nil)
            
            //W_LHS = 0
            UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
                wImageViewLHS_i.alpha = 0
            }, completion: nil)
            
            //W_RHS = 1
            UIView.animate(withDuration: 0, delay: 0.12, options: .curveLinear, animations: {
                wImageViewRHS_i.alpha = 1
            }, completion: nil)
            
            //W_RHS_i -> W_RHS_f
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
                wImageViewRHS_i.frame = wImageViewFrameRHS_f
            }, completion: nil)
            
            
            //NOW ANIMATE FROM RIGHT TO LEFT
        } else if self.keyPosition == "R" {
            //slide key to left
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                kImageView.frame = kFrameL
                self.keyPosition = "L"
            }, completion: nil)
            
            
            //set right wiggler alpha to zero
            UIView.animate(withDuration: 0, delay: 0.08, options: .curveEaseInOut, animations: {
                wImageViewRHS_i.alpha = 0 as CGFloat
            }, completion: nil)
            
            //present right wiggler
            //W_RHS = 1
            UIView.animate(withDuration: 0, delay: 0.12, options: .curveLinear, animations: {
                wImageViewLHS_i.alpha = 1
            }, completion: nil)
        }
    }
    
    func animate2WaySwapRequest_Fade() {
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //change key to white
        UIView.transition(with: kImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        }, completion: nil)
        
        //change wLHS to white
        UIView.transition(with: wImageViewLHS_i, duration: 0.3, options: .transitionCrossDissolve, animations: {
            wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        }, completion: nil)
        
        //change outline to solid box orange
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        }, completion: nil)
        //print("ACCIO f")
    }
    
    //THIS
    func animateOutgoing2WaySwapRequest_Slide() {
        //print("animateOutgoing2WaySwapRequest_Slide")
        
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //animation requires that LHS & RHS be present simultaneously
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kInset
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        /*
         1. When key crosses tail-end, tail_L hides
         2. When key reaches right edge + corner radius, tail_R appears
         */
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kInset + kWidth
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        //...Fade after slide
//        //change key to white
//        UIView.transition(with: kImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            kImageView.image = UIImage(named: "sliderKeyWhiteFill")
//        }, completion: nil)
//
//        //change wLHS to white
//        UIView.transition(with: wImageViewLHS_i, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
//        }, completion: nil)
//
//        //change outline to solid box orange
//        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
//            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
//        }, completion: nil)
//        //print("ACCIO f")
//
//        //...TRANSLATE VIEWS
//        //KL -> KR
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
//            kImageView.frame = kFrameR
//            self.keyPosition = "R"
//        }, completion: nil)
//
//        //W_LHS = 0
//        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
//            wImageViewLHS_i.alpha = 0
//        }, completion: nil)
//
//        //W_RHS = 1
//        UIView.animate(withDuration: 0, delay: 0.12, options: .curveLinear, animations: {
//            wImageViewRHS_i.alpha = 1
//        }, completion: nil)
//
//        //W_RHS_i -> W_RHS_f
//        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
//            wImageViewRHS_i.frame = wImageViewFrameRHS_f
//        }, completion: nil)
//
        //Fade before slide
        //SUBVIEW IMAGE CHANGES
        //change outline to solid box orange
        UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
        sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        }, completion: nil)
    
        //change key to white
        UIView.transition(with: kImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        }, completion: nil)
        //
    
        //change wLHS to white
        UIView.transition(with: wImageViewLHS_i, duration: 0.2, options: .transitionCrossDissolve, animations: {
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        }, completion: nil)
    
        var t = 0.2 - 0.05
    
        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0 + t , options: .curveLinear, animations: {
        wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }) { (true) in
            self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
            self.addRotatingLayer(shouldGrow: true, LRC: "L")
        }
    
        //FRAME POSITION TRANSLATIONS
        //KL -> KR
        UIView.animate(withDuration: 0.2, delay: 0 + t, options: .curveLinear , animations: {
        kImageView.frame = kFrameR
        }, completion: nil)
    
        //W_LHS = 0
        UIView.animate(withDuration: 0, delay: 0.08 + t, options: .curveLinear, animations: {
        wImageViewLHS_i.alpha = 0
        }, completion: nil)
    
        //W_RHS = 1
        UIView.animate(withDuration: 0, delay: 0.12 + t, options: .curveLinear, animations: {
        wImageViewRHS_i.alpha = 1
        }, completion: nil)
        //
    }

    func animateSlideToAcceptSwap() {
        
        //print("animateSlideToAcceptSwap")
        //1. Animate Key
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kInset
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        /*
         1. When key crosses tail-end, tail_L hides
         2. When key reaches right edge + corner radius, tail_R appears
         */
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kInset + kWidth
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        //
        //change box to blue fill
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        }, completion: nil)
        
        //KL -> KR
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
            kImageView.frame = kFrameR
            self.keyPosition = "R"
        }, completion: nil)
        
        //W_LHS = 0
        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
            wImageViewLHS_i.alpha = 0
        }, completion: nil)
        
        //W_RHS = 1
        UIView.animate(withDuration: 0, delay: 0.12, options: .curveLinear, animations: {
            wImageViewRHS_i.alpha = 1
        }, completion: nil)
        
        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
            wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }) { (true) in
            self.stateLabel!.layer.removeAllAnimations()
            self.animateRevealProfile()
        }
        
        //deactivate old gesture and put in new one to unlock profile
        kImageView.isUserInteractionEnabled = false
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        //
        
    }
    
    //THIS
    /*
    func animateSlideToAcceptIncomingSwap() {
        
        //print("animateSlideToAcceptIncomingSwap")
        //1. Animate Key
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kInset
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        /*
         1. When key crosses tail-end, tail_L hides
         2. When key reaches right edge + corner radius, tail_R appears
         */
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kInset + kWidth
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        self.stateLabel?.text = ""

        //
        //change box to blue fill
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        }, completion: nil)
        
        //KL -> KR
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
            kImageView.frame = kFrameR
            self.keyPosition = "R"
        }, completion: nil)
        
        //W_LHS = 0
        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
            wImageViewLHS_i.alpha = 0
        }, completion: nil)
        
        //W_RHS = 1
        UIView.animate(withDuration: 0, delay: 0.12, options: .curveLinear, animations: {
            wImageViewRHS_i.alpha = 1
        }, completion: nil)
        
        
        self.rotatingLayer?.removeAllAnimations()
        self.rotatingLayer?.removeFromSuperlayer()
        
        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
            wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }) { (true) in
            self.sliderButtonImageView?.isUserInteractionEnabled = false
            self.stateLabel!.layer.removeAllAnimations()
//            self.addRotatingLayer(shouldGrow: true, LRC: "L")
        }
        
        //deactivate old gesture and put in new one to unlock profile
        kImageView.isUserInteractionEnabled = false
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Incoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = false
    }
 */
    
    //looking to get either fucked or murdered
    
    var didSetUnlockingProfileText = false
    
    func animateSlideToAcceptIncomingSwap() {
        //print("animateSlideToAcceptIncomingSwap")
    
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //animation requires that LHS & RHS be present simultaneously
        //init |K
        let sWidth = 100 as CGFloat
        let sHeight = 50 as CGFloat
        let sI = 7 as CGFloat
        let kInset = 7 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kInset
        let kXR = kInset + kWidth + kInset
        let kY = kInset
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        /*
         1. When key crosses tail-end, tail_L hides
         2. When key reaches right edge + corner radius, tail_R appears
         */
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + sI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kInset + kWidth
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        //Fade before slide
        //SUBVIEW IMAGE CHANGES
        //change outline to solid box orange
        UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        }, completion: nil)
        
        //change key to white
        UIView.transition(with: kImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        }, completion: nil)
        //
        
        //change wLHS to white
        UIView.transition(with: wImageViewLHS_i, duration: 0.2, options: .transitionCrossDissolve, animations: {
            wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        }, completion: nil)
        
        var t = 0.2 - 0.05
        
        self.rotatingLayer?.removeAllAnimations()
        self.rotatingLayer?.removeFromSuperlayer()
        
        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0 + t , options: .curveLinear, animations: {
            wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }) { (true) in
            self.sliderButtonImageView?.isUserInteractionEnabled = false
            self.stateLabel!.layer.removeAllAnimations()
            if self.didSetUnlockingProfileText == false {
                //print("1 wilLSet Unlocking profile from animateSlideToAcceptIncomingSwap")
                self.stateLabel!.text = "Unlocking Profile..."
                self.didSetUnlockingProfileText = true
            } else {
                //print("Needs to run true")
                self.stateLabel!.text = "Tap to Unlock"
            }
            
            self.stateLabel!.alpha = 1
            self.addRotatingLayer(shouldGrow: true, LRC: "L")
        }
        
        //FRAME POSITION TRANSLATIONS
        //KL -> KR
        UIView.animate(withDuration: 0.2, delay: 0 + t, options: .curveLinear , animations: {
            kImageView.frame = kFrameR
        }, completion: nil)
        
        //W_LHS = 0
        UIView.animate(withDuration: 0, delay: 0.08 + t, options: .curveLinear, animations: {
            wImageViewLHS_i.alpha = 0
        }, completion: nil)
        
        //W_RHS = 1
        UIView.animate(withDuration: 0, delay: 0.12 + t, options: .curveLinear, animations: {
            wImageViewRHS_i.alpha = 1
        }, completion: nil)
        //
    }
    
    
    
    func animateLive2WayIncomingRequest() {
        
        //print("animateLive2WayIncomingRequest")
        
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //change key to white
        UIView.transition(with: kImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        }, completion: nil)
        
        //change wLHS to white
        UIView.transition(with: wImageViewLHS_i, duration: 0.3, options: .transitionCrossDissolve, animations: {
            wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        }, completion: nil)
        
        //change outline to solid box orange
        //print("ACCIO a")
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        }, completion: nil)
        
        //remove old tapGestureRecognizer
        kImageView.isUserInteractionEnabled = false
        
        //add new tap gesture that allows the user to accept the request: add Swap Profiles
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.slideToAccept(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        kImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        kImageView.isUserInteractionEnabled = true
        
        self.addRotatingLayer(shouldGrow: true, LRC: "R")
        self.animate2WayRequest_Incoming_FadeInFadeOut_P()
    }
    
    //BUG: animate2WayOutgoingApproved_FadeToBlue is getting called when: selfInit's 2WaySwapRequest, other accepts, self goes back to chatListVC, and forward to CSVC, drawView is calling method as if from observer which it SHOULDNT.
    
    var didAnimateFadeToBlue = false
    
    
    
    func animate2WayOutgoingApproved_FadeToBlue() {
        //print("animate2WayOutgoingApproved_FadeToBlue")
        
        guard didAnimateFadeToBlue == false else { return }
        
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //change orange to blue
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        }) { (true) in

        }
        
        //remove old tapGestureRecognizer
        kImageView.isUserInteractionEnabled = false
        
        //add new tap gesture that allows the user to accept the request: add Swap Profiles
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Outgoing(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = false
        
        self.rotatingLayer?.removeAllAnimations()
        self.rotatingLayer?.removeFromSuperlayer()
        
//        self.animateRevealProfile()
        self.didAnimateFadeToBlue = true
    }
    
    func addTapToOpenOutgoingRevealApproved_Hint() {
        //print("addTapToOpenOutgoingRevealApproved_Hint")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Tap to Open"
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.5, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animateStartTalking_Hint() {
        
        let stateLabel = self.stateLabel!
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animateSlideToSwap_Hint() {
        let stateLabel = self.stateLabel!
        stateLabel.text = "Swap Profiles"
        
        let del = 0.5
        
        UIView.animate(withDuration: 0.3, delay: 0 + del, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8 + del, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animatePendingReveal_Outgoing_Hint() {
        //print("animate2WayRequest_Outgoing_P")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Pending Approval"
        stateLabel.alpha = 1
        
        let del = 0.0
        
        UIView.animate(withDuration: 3, delay: 0 + del, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 3, delay: 0.8 + del, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animateAskToReveal_Hint() {
        //print("animate2WayRequest_Outgoing_P")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Ask to Show"
        stateLabel.alpha = 1
        
        let del = 0.0
        
        UIView.animate(withDuration: 3, delay: 0 + del, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 3, delay: 0.8 + del, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    //MARK: 1WAY Swap UI Functions................................................................................................................................................
    
    //MARK: SWAP UI . swapRequestDoesExist == TRUE
    
    var didAnimateLiveFillCircle = false

    func swapUI_1WayShow_Incoming_Live(withSource: String) {
        //print("swapUI_1WayShow_Incoming_Live withSource,", withSource)
        let s = self.sliderButtonImageView!
        
        if self.didAnimateLiveFillCircle == false {
            self.rotatingLayer?.removeAllAnimations()
            self.rotatingLayer?.removeFromSuperlayer()
            self.ringLayer?.isHidden = false
//            self.animateShowProfile_FillCircle()
            self.animateIncomingOneWayShow_FillCircle()
            self.animatePulsatingLayer_N()
            self.didAnimateLiveFillCircle = true
        }
        
        s.isUserInteractionEnabled = false
        
        //2. add tap gesture with reveal functionality
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.unlockIncomingOneWay_Live(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        s.addGestureRecognizer(tapGesture)
        s.isUserInteractionEnabled = true
    }
    
    func swapUI_Outgoing1WayRevealApproved_D_Live(withSource: String) {
        //print("swapUI_Outgoing1WayRevealApproved_D_Live withSource,", withSource)
        let s = self.sliderButtonImageView!
        
        if self.didAnimateLiveFillCircle == false {
            self.rotatingLayer?.removeAllAnimations()
            self.rotatingLayer?.removeFromSuperlayer()
            self.ringLayer?.isHidden = false
            //            self.animateShowProfile_FillCircle()
            self.animateIncomingOneWayShow_FillCircle()
            self.animatePulsatingLayer_N()
            self.didAnimateLiveFillCircle = true
        }
        
        s.isUserInteractionEnabled = false
        
        //2. add tap gesture with reveal functionality
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.unlockOutgoing_OneWay(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        s.addGestureRecognizer(tapGesture)
        s.isUserInteractionEnabled = true
    }
    
    func animateIncomingOneWayShow_FillCircle() {
        //print("animateIncomingOneWayShow_FillCircle")
        
        self.sliderButtonImageView?.isUserInteractionEnabled = false
        self.pulsatingLayer?.removeFromSuperlayer()
        
        ringLayer?.fillColor = UIColor.white.cgColor
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.2
        grow.toValue = 1.0
        grow.duration = 0.4
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        ringLayer?.add(grow, forKey: "grow")
        
        UIView.animate(withDuration: 0.3, delay: 0.4, options: [.transitionCrossDissolve], animations: {
            self.stateLabel!.textColor = UIColor.white
            self.stateLabel!.alpha = 1
            self.stateLabel!.text = "Tap to Open"
        }) { (true) in
            //            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
            //                stateLabel.alpha = 0
            //            }, completion: nil)
        }
    }
    
    func swapUI_1WayShow_N() {
        //print("swapUI_1WayShow_N")
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "oneWayButtonOrangeFill")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.oneWayShow(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.addRingView_N()
        self.addPulsatingLayer()
        self.animatePulsatingLayer_N()
        self.animateShowProfile_Hint()
    }
    
    func swapUI_1WayReveal_N() {
        //print("swapUI_1WayReveal_N")
        
        
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "oneWayButtonOrangeFill")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.askToReveal(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.addRingView_N()
        self.addPulsatingLayer()
        self.animatePulsatingLayer_Ask_N()
        self.animateAskToReveal_Hint()
        
    }
    

    
    func swapUI_1Way_Incoming_P() {
        //print("swapUI_1Way_Incoming_P")
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "oneWayButtonOrangeFill")
        
        //1. add animation to the button ("Pulse_Out": expand circle outward like a pulse to impell user to reveal)
        self.addRingView_N()
        self.addPulsatingLayer()
        
        //2. add tap gesture with reveal functionality
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.acceptIncomingRevealRequest(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        self.animate1Way_Incoming_P()
        self.animateApproveIncomingRevealRequest()
    }
    
    func swapUI_1WayShow_D() {
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "askToShowApproved")
        self.animateShowProfile_Affirm()
    }
    
    func swapUI_1WayReveal_D() {
        //print("swapUI_1WayReveal_D")
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "askToShowApproved")
        
        //1. add animation to the button
        self.addRingView_N()
        self.addPulsatingLayer()
        self.animateUnlockProfile_PulseOut()
        self.addTapToOpenOutgoingRevealApproved_Hint()
        
        
        //2. add tap gesture with reveal functionality
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.unlockOutgoing_OneWay(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
    }
    
    func swapUI_1WayReveal_Outgoing_Live_D() {
        //print("swapUI_1WayReveal_Outgoing_Live_D")
        let s = self.sliderButtonImageView!
        
        self.pulsatingLayer!.removeAllAnimations()
        
        self.animateProfileRevealed_FillCircle()
        self.animateUnlockProfile_PulseOut()
//        self.animateProfileNowVisible_Hint()
        self.animateStateLabelHint_forOutgoingApprovedLive()
        
        s.isUserInteractionEnabled = false
        
        //2. add tap gesture with reveal functionality
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.unlockOutgoing_OneWay(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        s.addGestureRecognizer(tapGesture)
        s.isUserInteractionEnabled = true
    }
    
    func animateStateLabelHint_forOutgoingApprovedLive() {
        //print("animateStateLabelHint_forOutgoingApproved")
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.transitionCrossDissolve], animations: {
            self.stateLabel!.textColor = UIColor.white
            self.stateLabel!.alpha = 1
            self.stateLabel!.text = "Tap to Open"
        }) { (true) in
//            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
//                stateLabel.alpha = 0
//            }, completion: nil)
        }
    }
    
    func swapUI_1Way_OtherInit_D() {
        //print("CSVC swapUI_1Way_OtherInit_D")
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "askToShowApproved")
        
        self.pulsatingLayer?.removeAllAnimations()
        
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.unlockIncomingOneWay_Live(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        sliderButtonImageView.addGestureRecognizer(tapGesture)
        sliderButtonImageView.isUserInteractionEnabled = true
        
        //this UI case shouldn't animate anything, instead view should update normally with small symbol that user unlocked their profile voluntarily. Because we don't want any "unsolicited reveals"
    }
    
    func swapUI_1WayReveal_P() {
        //result of your previous oneWayAsk() is pending
        //print("swapUI_1WayReveal_P")
        let sliderButtonImageView = self.sliderButtonImageView!
        sliderButtonImageView.image = UIImage(named: "oneWayButtonOrangeFill")
        
        self.addRingView_N()
        self.addPulsatingLayer()
        self.animateAskToShow_GrowShrink_P()
        self.animatePendingReveal_Outgoing_Hint()
    }
    
    //MARK: 1WAY Swap Action-Target Methods

    @objc func askToReveal(_ sender: UITapGestureRecognizer) {
        //print("askToReveal tap observed")
        
        if let swapAlertSeen = UserDefaults.standard.string(forKey: "askToShowAlert_Seen") {
            if swapAlertSeen == "true" {
                self.swapUI_1WayReveal_N_Helper()
            } else {
                self.swapUI_1WayReveal_N_Helper()
            }
        } else {
            self.present(self.askToShowAlert!, animated: true)
        }
    }
    
    func swapUI_1WayReveal_N_Helper() {
        self.swapControlFlow()
        self.animateAskToShow_GrowShrink_P()
        self.animate1WayLabel_Outgoing_P()
        
        self.sendRowToTopChatListForUID(withSourceCall: "askToReveal")
    }
    
    //should oneWayShow option even be available? Can make this a paid option, to reveal profile to specific people only. And make oneway revealAsk the only option.
    //or people can reveal profile while in conversation. would register as one way show, if from liveObserver == true, then shows the unlock. Seems that would be unsolociitied makes people uncomfortable. So deactivate one-way show completely - a user can only reveal profile upon request.
    //people can voluntarily reveal profile, will unlock automatically to other user if not within chat
    //but then, if in chat, then will reveal profile.
    
    @objc func oneWayShow(_ sender: UITapGestureRecognizer) {
        //print("oneWayShow observed")
        guard self.guardForClearPhotoFirst() == true else { return }

        if let swapAlertSeen = UserDefaults.standard.string(forKey: "oneWayShowAlert_Seen") {
            if swapAlertSeen == "true" {
                self.oneWayShow_Helper()
            } else {
                self.oneWayShow_Helper()
            }
        } else {
            self.present(self.oneWayShowAlert!, animated: true)
        }
     
    }
    
    func oneWayShow_Helper() {
        self.sliderButtonImageView?.isUserInteractionEnabled = false
        self.sliderMaskImageView?.isUserInteractionEnabled = false
        self.pulsatingLayer!.removeAllAnimations()
        self.animateShowProfile_FillCircle()
        self.updateUnlockedUsersArray()
        self.createNewSwapRequest(withStatus: "true")
        self.oneWayShow()
        self.animateProfileNowVisible_Hint()
        
        self.sendRowToTopChatListForUID(withSourceCall: "oneWayShow")
    }
    
    @objc func acceptIncomingRevealRequest(_ sender: UITapGestureRecognizer) {
        //print("acceptIncomingRevealRequest tap observed")
        //
        guard self.guardForClearPhotoFirst() == true else { return }

        if let swapAlertSeen = UserDefaults.standard.string(forKey: "approveIncomingOneWayAlert_Seen") {
            if swapAlertSeen == "true" {
                self.acceptIncomingRevealRequest_Helper()
            } else {
                self.acceptIncomingRevealRequest_Helper()
            }
        } else {
            self.present(self.acceptIncomingOneWayAlert!, animated: true)
        }
    }
    
    func acceptIncomingRevealRequest_Helper() {
        self.swapControlFlow()
        
        self.sliderButtonImageView?.isUserInteractionEnabled = false
        self.sliderMaskImageView?.isUserInteractionEnabled = false
        
        self.pulsatingLayer!.removeAllAnimations()
        self.animateShowProfile_FillCircle()
        self.animateProfileNowVisible_Hint()
        self.updateUnlockedUsersArray()
        
        var swapRequestID: String?
        
        if let oneWayRequestID = importedChatUser["oneWayRequestID"] {
            swapRequestID = oneWayRequestID
        } else if let oneWayRequestID2 = self.absExistingSwapRequestID {
            swapRequestID = oneWayRequestID2
        }
        
        if let swapID = swapRequestID {
            self.ref.child("/users/\(self.selfUID!)/pendingSwapRequests/\(swapID)/swapStatus").setValue("true")
        }
        
        self.sendRowToTopChatListForUID(withSourceCall: "acceptIncomingRevealRequest")
    }
    
    @objc func unlockOutgoing_OneWay(_ sender: UITapGestureRecognizer) {
        //print("unlockOutgoing_OneWay observed")
        //
//        self.swapControlFlow()
        
        self.pulsatingLayer?.removeAllAnimations()
        self.pulsatingLayer?.removeFromSuperlayer()
        self.addTransparencyView()
        self.performSegue(withIdentifier: "toVoilaVC", sender: self)
        self.updateUnlockedUsersArray()
        
        var swapRequestID: String?
        
        if let oneWayRequestID = importedChatUser["oneWayRequestID"] {
            swapRequestID = oneWayRequestID
        } else if let oneWayRequestID2 = self.absExistingSwapRequestID {
            swapRequestID = oneWayRequestID2
        }
        
        ref.child("globalSwapRequests/\(selfUID!)/\(swapRequestID!)/swapInitiatorDidOpen").setValue("true")
        ref.child("globalSwapRequests/\(otherUID!)/\(swapRequestID!)/swapInitiatorDidOpen").setValue("true")
        
        self.ref.child("users/\(selfUID!)/outgoingSwapRequests/\(swapRequestID!)/swapRequestStatus").setValue("true")

        self.clearPhotoURLPath(withSource: "unlockOutgoingReveal_Approved")
        self.sendRowToTopChatListForUID(withSourceCall: "unlockOutgoing_OneWay observed")
    }
    
    @objc func unlockIncomingOneWay_Live(_ sender: UITapGestureRecognizer) {
        //print("unlockIncomingOneWay_Live observed")
        //
        //        self.swapControlFlow()
        self.stateLabel?.isHidden = true
        self.pulsatingLayer?.removeAllAnimations()
        self.addTransparencyView()
        self.performSegue(withIdentifier: "toVoilaVC", sender: self)
        self.updateUnlockedUsersArray()
        
        var swapRequestID: String?
        
        if let oneWayRequestID = importedChatUser["oneWayRequestID"] {
            swapRequestID = oneWayRequestID
        } else if let oneWayRequestID2 = self.absExistingSwapRequestID {
            swapRequestID = oneWayRequestID2
        }
        
        
        let childUpdates = [
            "globalSwapRequests/\(selfUID!)/\(swapRequestID!)/swapInitiatorDidOpen" : "true",
            "globalSwapRequests/\(otherUID!)/\(swapRequestID!)/swapInitiatorDidOpen" : "true",
            "/users/\(self.selfUID!)/pendingSwapRequests/\(swapRequestID!)/swapStatus": "true",
        ]
        
        self.ref.updateChildValues(childUpdates)
        
//        ref.child("globalSwapRequests/\(selfUID!)/\(swapRequestID!)/swapInitiatorDidOpen").setValue("true")
//        ref.child("globalSwapRequests/\(otherUID!)/\(swapRequestID!)/swapInitiatorDidOpen").setValue("true")
        
        self.clearPhotoURLPath(withSource: "incomingOneWayShow_Live")
        self.sendRowToTopChatListForUID(withSourceCall: "unlockIncomingOneWay_Live")
        
        
    }
    
    //MARK: 1WAY Swap Animator Methods
    
    var pulsatingLayer: CAShapeLayer?
    var ringLayer: CAShapeLayer?
    
    func addRingView_N() {
        let s = self.sliderButtonImageView!.bounds
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 15, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        ringLayer = CAShapeLayer()
        ringLayer!.path = circularPath.cgPath
        
        ringLayer!.fillColor = UIColor.clear.cgColor
        ringLayer!.lineWidth = 2
        ringLayer!.strokeColor = UIColor.white.cgColor
        
        let cX = s.width * 0.5
        let cY = s.height * 0.5
        let cPoint = CGPoint(x: cX, y: cY)
        
        ringLayer!.position = cPoint
        
        self.sliderButtonImageView!.layer.addSublayer(ringLayer!)
    }
    
    func addPulsatingLayer() {
        let s = self.sliderButtonImageView!.bounds
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 15, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer = CAShapeLayer()
        pulsatingLayer!.path = circularPath.cgPath
        
        pulsatingLayer!.fillColor = UIColor.clear.cgColor
        pulsatingLayer!.lineWidth = 1.5 as CGFloat
        pulsatingLayer!.strokeColor = UIColor.white.cgColor
        
        let cX = s.width * 0.5
        let cY = s.height * 0.5
        let cPoint = CGPoint(x: cX, y: cY)
        
        pulsatingLayer!.position = cPoint
        
        self.sliderButtonImageView!.layer.addSublayer(pulsatingLayer!)
    }
    
    /*
    func animateUnlockProfile_PulseOut() {
        //print("animateUnlockProfile_PulseOut")
        
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.3
        pulse.duration = 0.4
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = false
        pulse.repeatCount = Float.infinity
        
        let fade = CABasicAnimation(keyPath: "opacity")
        fade.fromValue = 0
        fade.toValue = 1
        fade.duration = 0.2
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        fade.autoreverses = true
        fade.repeatCount = Float.infinity
        
        pulsatingLayer!.add(pulse, forKey: "pulse")
        pulsatingLayer!.add(fade, forKey: "fade")
    }
*/
 
    func animateUnlockProfile_PulseOut() {
        //print("animateUnlockProfile_PulseOut")
        
        self.pulsatingLayer?.isHidden = false
        
        self.pulsatingLayer!.lineWidth = 2.5 as CGFloat
        self.pulsatingLayer!.opacity = 0

        let fadeOut = CABasicAnimation(keyPath: "opacity")
        fadeOut.fromValue = 1
        fadeOut.toValue = 0
        fadeOut.duration = 0.5

        let expandScale = CABasicAnimation()
        expandScale.keyPath = "transform"
        expandScale.valueFunction = CAValueFunction(name: kCAValueFunctionScale)
        expandScale.fromValue = [1, 1, 1]
        expandScale.toValue = [1.3, 1.3, 1.3]
        expandScale.duration = 0.5
        
        let fadeAndScale = CAAnimationGroup()
        fadeAndScale.animations = [fadeOut, expandScale]
        fadeAndScale.duration = 0.8
        fadeAndScale.repeatCount = .infinity

        pulsatingLayer!.add(fadeAndScale, forKey: "pulseGroup")
    }
    
    func animateShowProfile_FillCircle() {
        //print("animateShowProfile_FillCircle")
        
        self.sliderButtonImageView?.isUserInteractionEnabled = false
        self.pulsatingLayer?.removeFromSuperlayer()

        ringLayer?.fillColor = UIColor.white.cgColor
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.2
        grow.toValue = 1.0
        grow.duration = 0.4
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        ringLayer?.add(grow, forKey: "grow")
    }
    
    func animateProfileRevealed_FillCircle() {
        //print("animateProfileRevealed_FillCircle")
        
        ringLayer!.fillColor = UIColor.white.cgColor
        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.2
        grow.toValue = 1.0
        grow.duration = 0.4
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        ringLayer!.add(grow, forKey: "grow")
    }
    
    func animate1Way_Incoming_P() {
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.3
        pulse.duration = 0.4
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = true
        pulse.repeatCount = Float.infinity
        
        pulsatingLayer!.add(pulse, forKey: "pulse")
    }
    
    func animateShowProfile_Affirm() {
        let stateLabel = self.stateLabel!
        stateLabel.text = "Profile Visible"
        
        UIView.animate(withDuration: 3.2, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animate1WayLabel_Outgoing_P() {
        //print("animate2WayRequest_Outgoing_P")
        let stateLabel = self.stateLabel!
        stateLabel.text = "Pending Approval"
        stateLabel.alpha = 1
        
        UIView.animate(withDuration: 3.2, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
        
        
    }
    
    func animatePulsatingLayer_N() {
        //print("animatePulsatingLayer_N")
        //should also call animator when textFieldBeginsEditing
        
        //faster ring animation expand
        //slower ring animation contract
        //MUST add pause between flashes. It's currently too fast and tells the user "you MUST reveal your profile". It needs to be slower and more suggestive.
        
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.3
        pulse.duration = 0.4
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = false
        pulse.repeatCount = 3

        let fade = CABasicAnimation(keyPath: "opacity")
        fade.fromValue = 0
        fade.toValue = 1
        fade.duration = 0.2
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        fade.autoreverses = true
        fade.repeatCount = 3

        pulsatingLayer?.add(pulse, forKey: "pulse")
        pulsatingLayer?.add(fade, forKey: "fade")
    }
    
    func animate1Way_Outgoing_FillCirlce_D() {
        self.animateShowProfile_FillCircle()
        self.animate1Way_ReadytoOpen()
    }
    
    func animate1Way_ReadytoOpen() {
        self.animatePulsatingLayer_N()
    }

    func animatePulsatingLayer_Ask_N() {
        //print("animatePulsatingLayer_Ask_N")
        //should also call animator when textFieldBeginsEditing
        
        //faster ring animation expand
        //slower ring animation contract
        //MUST add pause between flashes. It's currently too fast and tells the user "you MUST reveal your profile". It needs to be slower and more suggestive.
        
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.3
        pulse.duration = 0.4
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = true
        pulse.repeatCount = 4
        
        pulsatingLayer!.add(pulse, forKey: "pulse")

//        let fade = CABasicAnimation(keyPath: "opacity")
//        fade.fromValue = 0
//        fade.toValue = 1
//        fade.duration = 0.2
//        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
//        fade.autoreverses = true
//        fade.repeatCount = 4
//
//        pulsatingLayer.add(fade, forKey: "fade")
    }
    
    func animateAskToShow_GrowShrink_P() {
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.3
        pulse.duration = 0.4
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = true
        pulse.repeatCount = Float.infinity
        
        pulsatingLayer!.add(pulse, forKey: "pulse")
    }
    
    func animateProfileNowVisible_Hint() {
        let stateLabel = self.stateLabel!
        stateLabel.text = "Profile Visible"
        
        UIView.animate(withDuration: 0.3, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
        
    }
    
    func animateShowProfile_Hint() {
        let stateLabel = self.stateLabel!
        stateLabel.text = "Show Profile"
        
        UIView.animate(withDuration: 3.2, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }
    
    func animateApproveIncomingRevealRequest() {
        //print("animateApproveIncomingRevealRequest")
        
        let stateLabel = self.stateLabel!
//        stateLabel.text = "Reveal Profile"
        stateLabel.text = "Tap to Show"

        UIView.animate(withDuration: 0.3, delay: 0, options: [.transitionCrossDissolve], animations: {
            stateLabel.textColor = UIColor.white
            stateLabel.alpha = 1
        }) { (true) in
            UIView.animate(withDuration: 0.3, delay: 0.8, options: .transitionCrossDissolve, animations: {
                stateLabel.alpha = 0
            }, completion: nil)
        }
    }

    //MARK: Keyboard UI Configuration ............................................................................................................................................
    
    var observedKeyboardHeight: CGFloat?
    var mostRecentKeyboardHeight = 0 as CGFloat
    var duration: Double?
    var curveOption: UIViewAnimationOptions?

    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    //WARNING!: THE ENTIRE FUNCTION BELOW DEPENDS ON THE PROPER RETRIEVAL OF THE DEFAULT KEYBOARD HEIGHT BEFORE THE TEXTFIELD EVER BEGINS EDITING. ASCERTAIN THAT YOU ARE PROPERLY RETRIEVING THIS VALUE IN TIME FOR DRAWING THE VIEW.
    
    //PROBLEM: Keyboard frame adjustment (i.e. emoji keyboard) is shifting topMargin of chatTable to behind thumbnai
    @objc func keyboardWillShow(_ notification:Notification) {
        //print("calling keyboardWillShow")
        
        let chatSpaceView = self.chatSpaceView!
        let textFieldView = self.textFieldView!
        let lineSeparatorView =  self.lineSeparatorView!
        let messagesTableView = self.messagesTableView!
        let sliderButtonImageView = self.sliderButtonImageView!
        let stateLabel = self.stateLabel!
        let shimmerView = self.shimmerView!
        
        let userInfo = notification.userInfo
        let duration = userInfo![UIKeyboardAnimationDurationUserInfoKey]! as! Double
        //print("duration is,", duration)
        let curve = userInfo![UIKeyboardAnimationCurveUserInfoKey]! as! NSInteger
        let curveOption = UIViewAnimationOptions(rawValue: UInt(curve))
        
        //
        self.duration = duration
        self.curveOption = curveOption
        
        let keyboardFrameEnd = userInfo![UIKeyboardFrameEndUserInfoKey]! as! CGRect
        //let keyboardFrameEndRectFromView = self.view.convert(keyboardFrameEnd, from: nil)
        
        let currentKeyboardHeight = getKeyboardHeight(notification)
        let yFrameKeyboardHeight = currentKeyboardHeight - self.mostRecentKeyboardHeight
        self.mostRecentKeyboardHeight = currentKeyboardHeight

        let defaultKH = (self.viewParameters?.keyBoardHeight)!
        let deltaKH = currentKeyboardHeight - defaultKH
        
        let insetUnit = (viewParameters?.insetUnit)!
        let screenWidth = (viewParameters?.screenWidth)!
        let screenHeight = (viewParameters?.screenHeight)!
//        let topInset = (viewParameters?.topInset)!
        
        //newChatFrame properties
        //new code
        let screen = UIScreen.main.bounds
        let sHeight = screen.height
//        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        let sBarH = self.ref_SelfModelController.finalsBarH!
        
        let absoluteNavSpaceRatio = 56 / 734 as CGFloat
        var absoluteNavSpace = absoluteNavSpaceRatio * sHeight
        if sHeight >= 734 as CGFloat {
            //constrain the navSpace to a fixed amount for iPhoneX
            absoluteNavSpace = 56 as CGFloat
        } else {
            //
        }
        let insetRatio = 19/414 as CGFloat
        let topInset = sBarH + absoluteNavSpace
        let bottomInset = 1 * insetUnit
        
        let xPos = insetUnit
        let yPos = topInset
        let cWidth = screenWidth - (2 * insetUnit)
        var newCHeight = screenHeight - topInset - insetUnit - defaultKH - deltaKH

        if sHeight >= 734 as CGFloat {
            //constrain the navSpace to a fixed amount for iPhoneX
            absoluteNavSpace = 56 as CGFloat
            let insetRatio = 19/414 as CGFloat
            let insetUnit = insetRatio * screenWidth
//            let sBarH = UIApplication.shared.statusBarFrame.size.height

            let topInset = sBarH + absoluteNavSpace
            let bottomInset = 1 * insetUnit
            
            let xPos = insetUnit
            let yPos = topInset
            
            let cWidth = screenWidth - (2 * insetUnit)
            newCHeight = sHeight - topInset - bottomInset - defaultKH - deltaKH
        } else {
            //
        }
    
        //newTextFrame properties
        let tFrameX = insetUnit
        var tFrameY = chatSpaceView.frame.height - messageFieldBorderInsetFinal! - yFrameKeyboardHeight
        let tFrameWidth = chatSpaceView.frame.width - (2 * insetUnit)
        let newTHeight = 31 as CGFloat
        
        //lineViewProperties
        let lineX = 0 as CGFloat
        var lineY = chatSpaceView.frame.height - yFrameKeyboardHeight - messageFieldBorderInsetFinal!
        let lineWidth = cWidth
        let lineHeight = 0.5 as CGFloat
        
        //tableView
        let imageGridUnit = chatSpaceView.bounds.height / 69
        let imageWidthRatio = 42 / 414 as CGFloat
        let imageWidth = imageWidthRatio * UIScreen.main.bounds.width
        
        let tableX = (viewParameters?.insetUnit)!
        //        let tableY = (imageGridUnit * 6) + (2 * (viewParameters?.insetUnit)!)
        let tableY = imageWidth + (1.5 * (viewParameters?.insetUnit)!)
        //print("TABLEY IS,", tableY)
        let tableWidth = chatSpaceView.bounds.width - (1.2 * (viewParameters?.insetUnit)!)
        var tableHeight = chatSpaceView.bounds.height - tableY - messageFieldBorderInsetFinal! - yFrameKeyboardHeight
        
        //update the lineView properties when user is unlocked and after sliderview is removed from superview
        //unless user just unlocked their profile while in chat
        if self.unlockedUsers.contains(self.otherUID!) {
            
            if self.selfVisus! == "false" && self.otherVisus! == "true" {
                
            } else {
                lineY = chatSpaceView.frame.height - yFrameKeyboardHeight - self.messageFieldBorderInset_Lo!
                tFrameY = chatSpaceView.frame.height - self.messageFieldBorderInset_Lo! - yFrameKeyboardHeight
                tableHeight = chatSpaceView.bounds.height - tableY - messageFieldBorderInset_Lo! - yFrameKeyboardHeight
            }
            
           
        } else {
        //
        }
        
        
        
//        let tableX = (viewParameters?.insetUnit)!
//        let tableY = (imageGridUnit * 6) + (1.5 * (viewParameters?.insetUnit)!)
//        let tableWidth = chatSpaceView!.bounds.width - (2 * (viewParameters?.insetUnit)!)
//        let tableHeight = chatSpaceView!.bounds.height - tableY - messageFieldBorderInsetFinal! - ((viewParameters?.insetUnit)! * 0)
        
        
        
        //mask view
        let maskImageView = self.sliderMaskImageView!
        let maskWidth = 176 as CGFloat
        let maskHeight = 68 as CGFloat
        let maskX = (cWidth - maskWidth) * 0.5
//        let yError = 0.4 as CGFloat

        let yErrorRatio = 0.4 / screenHeight as CGFloat
        var yError = yErrorRatio * screenHeight
        if screenHeight > 750 as CGFloat {
            yError = 0.4 as CGFloat
        } else if screenHeight < 700 {
             yError = 0.3 as CGFloat
        }
        let maskY = chatSpaceView.bounds.height - maskHeight + 1 - yFrameKeyboardHeight - yError

        //sliderButton image view
        let frameWidth = 100 as CGFloat
        let frameHeight = 50 as CGFloat

        let xFrame = ( cWidth - frameWidth ) * 0.5
        let yFrame = chatSpaceView.frame.height - frameHeight - yFrameKeyboardHeight
        let wFrame = 100 as CGFloat
        let hFrame = 50 as CGFloat
        
        //new state label properties
        let sW = self.sliderMaskImageView!.bounds.width
        let sH = 17 as CGFloat
        let sX = ((self.viewParameters?.screenWidth)! - sW) * 0.5
        let sY = chatSpaceView.frame.maxY - yFrameKeyboardHeight
        
        //animate
        UIView.animate(withDuration: duration, delay: 0, options: [curveOption, .beginFromCurrentState], animations: {
            //animate change in frame positions
            chatSpaceView.frame = CGRect(x: xPos, y: yPos, width: cWidth, height: newCHeight)
            textFieldView.frame = CGRect(x: tFrameX, y: tFrameY, width: tFrameWidth, height: newTHeight)
            lineSeparatorView.frame = CGRect(x: lineX, y: lineY, width: lineWidth, height: lineHeight)
            messagesTableView.frame = CGRect(x: tableX, y: tableY, width: tableWidth, height: tableHeight)
            maskImageView.frame = CGRect(x: maskX, y: maskY, width: maskWidth, height: maskHeight)
            sliderButtonImageView.frame = CGRect(x: xFrame, y: yFrame, width: wFrame, height: hFrame)
            shimmerView.frame = CGRect(x: sX, y: sY, width: sW, height: sH)
            //            self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
            //            stateLabel.frame = CGRect(x: sX, y: sY, width: sW, height: sH)
            //animate change in slider mask
            self.scrollToBottomMessage()

            //new
//            messagesTableView.transform = CGAffineTransform(translationX: 0, y: -yFrameKeyboardHeight)
        
            UIView.transition(with: maskImageView, duration: duration, options: .curveLinear, animations: {
                self.sliderMaskImageView!.image = UIImage(named: "sliderMask")
            }, completion: nil)
            
        }, completion: { (true) in
//            self.scrollToBottomMessage()
        })
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        //print("detected Keyboard WillHide")
//
//        let chatSpaceView = self.chatSpaceView!
//        let textFieldView = self.textFieldView!
//        let lineSeparatorView =  self.lineSeparatorView!
//        let messagesTableView = self.messagesTableView!
//        let sliderButtonImageView = self.sliderButtonImageView!
//
//        let userInfo = notification.userInfo
//        let duration = userInfo![UIKeyboardAnimationDurationUserInfoKey]! as! Double
//        //print("duration is,", duration)
//        let curve = userInfo![UIKeyboardAnimationCurveUserInfoKey]! as! NSInteger
//        let curveOption = UIViewAnimationOptions(rawValue: UInt(curve))
//
//        let keyboardFrameEnd = userInfo![UIKeyboardFrameEndUserInfoKey]! as! CGRect
//        //let keyboardFrameEndRectFromView = self.view.convert(keyboardFrameEnd, from: nil)
//
//        let currentKeyboardHeight = getKeyboardHeight(notification)
//        let yFrameKeyboardHeight = currentKeyboardHeight - self.mostRecentKeyboardHeight
//        self.mostRecentKeyboardHeight = currentKeyboardHeight
//
//        let defaultKH = (self.viewParameters?.keyBoardHeight)!
//        let deltaKH = currentKeyboardHeight - defaultKH
//
//        let insetUnit = (viewParameters?.insetUnit)!
//        let screenWidth = (viewParameters?.screenWidth)!
//        let screenHeight = (viewParameters?.screenHeight)!
//        let topInset = (viewParameters?.topInset)!
//
//        //newChatFrame properties
//        let xPos = insetUnit
//        let yPos = 4 * insetUnit
//        let cWidth = screenWidth - (2 * insetUnit)
//        let newCHeight = screenHeight - topInset - insetUnit - defaultKH - deltaKH
//
//        //newTextFrame properties
//        let tFrameX = insetUnit
//        let tFrameY = chatSpaceView.frame.height - messageFieldBorderInsetFinal! - yFrameKeyboardHeight
//        let tFrameWidth = chatSpaceView.frame.width - (2 * insetUnit)
//        let newTHeight = 31 as CGFloat
//
//        //lineViewProperties
//        let lineX = 0 as CGFloat
//        let lineY = chatSpaceView.frame.height - yFrameKeyboardHeight - messageFieldBorderInsetFinal!
//        let lineWidth = cWidth
//        let lineHeight = 0.5 as CGFloat
//
//        //tableView
//        let imageGridUnit = chatSpaceView.bounds.height / 69
//
//        let tableX = (viewParameters?.insetUnit)!
//        let tableY = (imageGridUnit * 6) + (2 * (viewParameters?.insetUnit)!)
//        //print("TABLEY IS,", tableY)
//        let tableWidth = chatSpaceView.bounds.width - (1.2 * (viewParameters?.insetUnit)!)
//        let tableHeight = chatSpaceView.bounds.height - tableY - messageFieldBorderInsetFinal! - ((viewParameters?.insetUnit)! * 0.5) - yFrameKeyboardHeight
//
//        //mask view
//        let maskImageView = self.sliderMaskImageView!
//        let maskWidth = 176 as CGFloat
//        let maskHeight = 68 as CGFloat
//        let maskX = (cWidth - maskWidth) * 0.5
//        let maskY = chatSpaceView.bounds.height - maskHeight + 1 - yFrameKeyboardHeight
//
//        //sliderButton image view
//        let frameWidth = 100 as CGFloat
//        let frameHeight = 50 as CGFloat
//
//        let xFrame = ( cWidth - frameWidth ) * 0.5
//        let yFrame = chatSpaceView.frame.height - frameHeight - yFrameKeyboardHeight
//        let wFrame = 100 as CGFloat
//        let hFrame = 50 as CGFloat
//
//        //animate
//        UIView.animate(withDuration: duration, delay: 0, options: [curveOption, .beginFromCurrentState], animations: {
//            //            self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
//            chatSpaceView.frame = CGRect(x: xPos, y: yPos, width: cWidth, height: newCHeight)
//            textFieldView.frame = CGRect(x: tFrameX, y: tFrameY, width: tFrameWidth, height: newTHeight)
//            lineSeparatorView.frame = CGRect(x: lineX, y: lineY, width: lineWidth, height: lineHeight)
//            messagesTableView.frame = CGRect(x: tableX, y: tableY, width: tableWidth, height: tableHeight)
//            maskImageView.frame = CGRect(x: maskX, y: maskY, width: maskWidth, height: maskHeight)
//            sliderButtonImageView.frame = CGRect(x: xFrame, y: yFrame, width: wFrame, height: hFrame)
//
//        }, completion: nil)
        
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        self.viewParameters?.keyBoardHeight = keyboardSize.cgRectValue.height
        //print("final keyboardHeight", keyboardSize.cgRectValue.height)
        return keyboardSize.cgRectValue.height
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }
    
   
    // DECLARED ABOVE
    //var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
//    func getCurrentUserInfo() {
//        self.ref.child("users/\(self.selfUID!)/userDisplayName").observeSingleEvent(of: .value) { (snapshot: DataSnapshot) in
//            self.selfDisplayName = (snapshot.value! as? String)!
//            //print("my display name is", self.selfDisplayName)
//        }
//
//        self.ref.child("users/\(self.selfUID!)/full_URL").observeSingleEvent(of: .value) { (snapshot: DataSnapshot) in
//            self.selfDefaultPhotoURL = (snapshot.value! as? String)!
//            //print("my display name is", self.selfDefaultPhotoURL)
//        }
//    }
    
   
    //problem: slider key is animating frame change when outgoing is approved. We only want a change in the fill and not the frame. Therefore, go to when other accepts in the swapControlFlow under selfInit case true & make sure it's calling the correct animator function
    
 
    //MARK: SWAP CONTROL FLOW .........................................................................................................................................................................
    
    func swapControlFlow() { //called when action button is pressed. This is NOT THE UI CONTROL FLOW FOR THE BUTTON STATE
        
        //A. if either user has previously initiated a swap request
        if self.swapRequestDoesExist == true {
            //print("CSVC A.")
            
            //if self initiated request
            if selfInitiatedSwapRequest == true {
                self.selfSwitchSwapResponseCases()
                
                //if other initiated request
            } else if selfInitiatedSwapRequest == false {
                //print("ControlCheck.")
                self.othSwitchResponseCases()
            }
            
        }
        
        //B. if no one has previously initiated a swap request
        if self.swapRequestDoesExist == false {
            
            //If my profile is hidden
            if self.selfVisus == "false" {
                
                //&& if other user's profile is hidden
                if self.otherVisus! == "false" {
                    //THEN self has to instantiate a new swapRequest & has to pass selfPPrivatePhoto
                    self.createNewSwapRequest(withStatus: "false")
                    self.photoURLBoomerang()
                }
                    
                    //&& if other user's profile is visible
                else if self.otherVisus! == "true" {
                    self.createNewSwapRequest(withStatus: "true")
                    self.oneWayShow()
                }
                
                //If my profile is visible
            } else if self.selfVisus == "true"  {
                
                //&& if other user's profile is hidden
                if self.otherVisus! == "false" {
                    //THEN self can ask the hidden user to reveal
                    //print("C11. ")
                    self.oneWayAsk()
                    
                    //&& if other user's profile is visible
                } else if self.otherVisus! == "true" {
                    //THEN both user profiles are visible and slider does not appear in view or is disabled
                    
                }
            }
            
        }
        
    }
    
    func updateUnlockedUsersArray() {
        self.ref.child("users/\(self.selfUID!)/unlockedUsers/\(self.otherUID!)").setValue("true")
    }
    
    func updateSeenReceipt() {
        if self.selfInitiatedSwapRequest == true {
            self.ref.child("globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapInitiatorDidOpen").setValue("true")
            self.ref.child("globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapInitiatorDidOpen").setValue("true")
        } else {
            self.ref.child("globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapRecipientDidOpen").setValue("true")
            self.ref.child("globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapRecipientDidOpen").setValue("true")
        }
    }
    
    //note: the purpose of the below function is to block the UI from showing going to segue, when instead key should be grayed out & disabled (or even removed & replaced with a successful swap symbol) after user unlocks other's profile. This can also be achieved by observing for unlocked user's array & profile will update accordingly.
    
    func updateUserDidUnlockProfile() {
        if self.importedChatUser.keys.contains("swapRequestID") {
            
            let swapRequestID = importedChatUser["swapRequestID"]!
            let selfUID = self.selfUID!
            let otherUID = self.otherUID!
            
            if self.selfUID! == importedChatUser["swapInitiatorID"]! {
                //print("T1")
                if importedChatUser["swapInitiatorDidOpen"]! == "false" {
                    //print("T1.1")
                    ref.child("globalSwapRequests/\(selfUID)/\(swapRequestID)/swapInitiatorDidOpen").setValue("true")
                    ref.child("globalSwapRequests/\(otherUID)/\(swapRequestID)/swapInitiatorDidOpen").setValue("true")
                }
                
            } else if self.selfUID! == importedChatUser["swapRecipientID"]! {
                //print("T2")
                if importedChatUser["swapRecipientDidOpen"]! == "false" {
                    //print("T2.2")
                    ref.child("globalSwapRequests/\(selfUID)/\(swapRequestID)/swapRecipientDidOpen").setValue("true")
                    ref.child("globalSwapRequests/\(otherUID)/\(swapRequestID)/swapRecipientDidOpen").setValue("true")
                }
            }
        }
    }
    
    //MARK: SWAP RESPONSE CASES FLOW ..............................................................................................................................................
    
    func selfSwitchSwapResponseCases() {
        //print("NOW Switching swap response states where initiator is self")
        switch self.swapRequestStatus {
            
        case "true":
            
            //&& 1|0: when self is hidden and other user is visible
            if self.selfVisus == "false" && self.otherVisus! == "true" {
                //self must reveal profile when button is pressed
//                self.oneWayShow()
                
                //&& 0|1: when self is visible and other user is hidden
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //result of previous oneWayAsk() is successful
                //can now reveal user's profile
                //                self.revelaro()
                //                self.voila()
                
                //&& 1|1: when both users are hidden
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap complete
                //can now reveal user's profile
                //                self.revelaro()
                //                self.voila()
                
            } else {
                //
            }
            
        //&& swap response is nil
        case "nil":
            //print("user denied your request") //button disabled
            break
        //&& swap response is default = "false"
        default:
            //&& 1|0
            if self.selfVisus == "false" && self.otherVisus! == "true" {
                //empty: would be one-way reveal
                
                //&& 0|1
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //result of your previous oneWayAsk() is pending
                
                //&& 1|1
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap request pending
            } else {
                //
            }
        }
    }
    
    func othSwitchResponseCases() { //REMEMBER: To call these functions when the didSet variables are updated
        //print("NOW Switching swap response states where initiator is other")
        switch self.swapRequestStatus {
            //when self has initiated swap
            
        //Oth&& TRUE
        case "true":
            //print("CSVC othSwitchResponseCases, true")
            //&& 1|0
            if self.selfVisus == "false" && self.otherVisus! == "true" {
                //you previously revealed your profile to this user
                
                //&& 0|1
            } else if self.selfVisus == "true" && self.otherVisus! == "false" {
                //other voluntarily revealed their profile
                //button activates...
                //                self.revelaro() //fires only once if selfDidOpen == false
                //                self.voila()
                
                //&& 1|1
            } else if self.selfVisus == "false" && self.otherVisus! == "false" {
                //swap complete
                //                self.revelaro() //fires only once if selfDidOpen == false
                //                self.voila()
                
            } else {
                
            }
            
        //Oth&& FALSE
        case "false":
            //print("CSVC othSwitchResponseCases, false")
            //&& 1|0
            if self.selfVisus! == "false" && self.otherVisus! == "true" {
                //approve the user's request for you to reveal your profile
                //print("1|0")
                self.oneWayReveal()
                
                //&& 0|1
            } else if self.selfVisus! == "true" && self.otherVisus! == "false" {
                //print("0|1")
                
                //Impossibe | OR you denied the user permission to reveal their profile
                
                //&& 1|1
            } else if self.selfVisus! == "false" && self.otherVisus! == "false" {
                //swap request pending
                //print("1|1")
                self.faciemRevelaro()
                
            } else {
                
            }
        //Oth&& NIL
        default:
            //print("CSVC othSwitchResponseCases, nil")
            break
        }
    }
    
    //MARK: RESPONSE FUNCTIONS......................................................................................................................................................
    
    //Box is initially nil. Adam sends request, so new UI update happens. One where
    func oneWayAsk() {
        //one way ask is equivalent to visible user asking invisible user to reveal. Not to be confused withh oneWayRevael() method which APPROVES an incoming reveal request.

        self.sliderButtonImageView?.isUserInteractionEnabled = false
        self.sliderMaskImageView?.isUserInteractionEnabled = false
        self.createNewSwapRequest(withStatus: "false") //timeMachine - changed by adding withStatus
        
        //
        self.updateSelfSwapRequests(withSource: "oneWayOutgoing")
        //
        
        //configure chat message with secret message key
        let selfUID = self.selfUID!
        let swapData = ["chatMessage" : "outgoingRevelaro_\(selfUID)"]
        self.sendMessage(data: swapData) //NOTE: sendMessage does NOT also send message notification. push is handled by textFieldShouldReturn via updateLastMessage()
        
        //FROM OTHER PERSPECTIVE: Rercord Incoming 1way request (outgoing from current user)
        self.sendRequestRSVP(withSource: "oneWayOutgoing")
    
        //NOTE: These operations should ideally all happen simultaneously - perform batch update of methods
        let finalBadgeIntString = String(finalBadgeInt)

        var notificationData = [String : String]()
        notificationData["forDisplayName"] = self.selfDisplayName!
        notificationData["senderID"] = self.selfUID!
        notificationData["messageBody"] = ""
        notificationData["chatThreadID"] = ""
        notificationData["badgeCount"] = finalBadgeIntString
        notificationData["notificationType"] = "incomingSwap1Way"
        
        if self.otherPushID != "" {
            
            let childUpdates = [
                "messageNotifications/\(otherPushID)": notificationData,
            ]
            
            self.ref.updateChildValues(childUpdates)
            
        }

    }
    
    func oneWayReveal() { //reactive disclosure - oneway
        //print("oneWayReveal")
        let otherUID = self.otherUID!
        let absExistingSwapRequestID = self.absExistingSwapRequestID!
        
        self.scarletMessenger(swapStatus: "true", shouldSetDidSeeRequest: true)
        self.ref.child("users/\(otherUID)/outgoingSwapRequests/\(absExistingSwapRequestID)/swapRequestStatus").setValue("true")
        
        let selfUID = self.selfUID!
        
        let swapData = ["chatMessage" : "outgoingRevelaro_D_\(selfUID)"]
        self.sendMessage(data: swapData)
    }
    
    func oneWayShow() { //voluntary disclosure - oneway
        //print("oneWayShow")
        self.scarletMessenger(swapStatus: "true", shouldSetDidSeeRequest: false)
        self.updateDestinationUserUnlockedUserArray()
        
        if self.otherUserIsReadingMessage == true {
            let selfUID = self.selfUID!
            
            let swapData = ["chatMessage" : "outgoingOneWayShow_\(selfUID)"]
            self.sendMessage(data: swapData)
        }
    }
    
//    "swap_\(selfUID) == outgoingSwapRequest"
//    "swap_\(otherUID) == incomingSwapRequest"
//
//    "swap_D_\(selfUID) == incomingSwapApproved"
//    "swap_D_\(otherUID) == outgoingSwapApproved"
    
    func photoURLBoomerang() { //initiator disclosure - twoway
        //print("photoURLBoomerang")
    
        self.scarletMessenger(swapStatus: "false", shouldSetDidSeeRequest: false)
        
        let selfUID = self.selfUID!
        let swapData = ["chatMessage" : "swap_\(selfUID)"]
        self.sendMessage(data: swapData)
        
        //FROM OTHER PERSPECTIVE: Rercord Incoming 1way request (outgoing from current user)
        self.sendRequestRSVP(withSource: "twoWayOutgoing")
    }
    
    func faciemRevelaro() { //receiver disclosure - twoway
        //print("faciemRevelaro")
        self.scarletMessenger(swapStatus: "true", shouldSetDidSeeRequest: true)
        
        var swapRequestID: String?
    
        let selfUID = self.selfUID!
        let swapData = ["chatMessage" : "swap_D_\(selfUID)"]
        self.sendMessage(data: swapData)
        
        if let oneWayRequestID = importedChatUser["oneWayRequestID"] {
            swapRequestID = oneWayRequestID
        } else if let oneWayRequestID2 = self.absExistingSwapRequestID {
            swapRequestID = oneWayRequestID2
        }
        
        if let swapID = swapRequestID {
            self.ref.child("/users/\(self.selfUID!)/pendingSwapRequests/\(swapID)/swapStatus").setValue("true")
            self.ref.child("/users/\(self.otherUID!)/pendingSwapRequests/\(swapID)/swapStatus").setValue("true")
        }
    }
    
    func updateDestinationUserUnlockedUserArray() {
        self.ref.child("users/\(self.otherUID!)/unlockedUsers/\(self.selfUID!)").setValue("true")
    }
    
    func createNewSwapRequest(withStatus: String) {
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and other ID is \(otherUID)")
        
        let newSwapRequestID = ref.child("globalSwapRequests").childByAutoId().key
        
        var swapRequestData = ["swapInitiatorID" : selfUID]
        swapRequestData["swapInitiatorFirstName"] = self.selfDisplayName!
//        swapRequestData["swapInitiatorDefaultURL"] = self.selfDefaultPhotoURL!
        swapRequestData["swapInitiatorDefaultURL"] = self.myProfileInfoDict["gridThumb_URL"]!
        
        swapRequestData["swapInitiatorPhotoURL"] = ""
        swapRequestData["swapInitiatorDidOpen"] = "false"
        swapRequestData["swapInitiatorVisus"] = self.selfVisus!
        
        swapRequestData["swapRecipientID"] = otherUID
        swapRequestData["swapRecipientFirstName"] = self.otherDisplayName!
//        swapRequestData["swapRecipientDefaultURL"] = self.otherDefaultPhotoURL!
        swapRequestData["swapRecipientDefaultURL"] = self.otherDefaultPhotoURL!

        swapRequestData["swapRecipientPhotoURL"] = ""
        swapRequestData["swapRecipientDidOpen"] = "false"
        swapRequestData["swapRecipientVisus"] = self.otherVisus!
        
        swapRequestData["swapRequestStatus"] = withStatus
        swapRequestData["swapIsActive"] = "true"
        
        //recipientDidSeeRequest is always set to false UNLESS (1) user is present in chatList,
//        if self.otherUserIsReadingMessage == true {
//            swapRequestData["recipientDidSeeRequest"] = "true"
//        } else {
//            swapRequestData["recipientDidSeeRequest"] = "false"
//        }
        
        swapRequestData["recipientDidSeeRequest"] = "false"
        swapRequestData["initiatorDidSeeRequest"] = "false"

//        //chats metadata
//        swapRequestData["chatThreadID"] = self.absExistingMessageThreadID
//
//        //get last message String //reactivate salvo
//        let lastMessageSnapshot = self.messagesArray.last!
//        let lastMessageBody = lastMessageSnapshot.value as! [String : String]
//        let lastMessageString = lastMessageBody["chatMessage"]!
        
        //append message data
        if let lastMessageSnapshot = self.messagesArray.last {
            let lastMessageBody = lastMessageSnapshot.value as! [String : String]
            let lastMessageString = lastMessageBody["chatMessage"]!
            swapRequestData["lastMessage"] = lastMessageString
        } else {
            swapRequestData["lastMessage"] = ""
        }
        
        //onewayAsk property
        //        if self.selfVisus! == "true" && self.otherVisus! == "false" {
        //            let chatThreadID = self.absExistingMessageThreadID
        //            //print("onewayAskPropertySetter absExistingMessageThreadID", self.absExistingMessageThreadID)
        //            swapRequestData["didAskShow"] = "true"
        //            self.ref.child("users/\(self.selfUID!)/myActiveChatThreads/\(chatThreadID)/didAskShow").setValue("true")
        //            self.ref.child("users/\(self.otherUID!)/myActiveChatThreads/\(chatThreadID)/didAskShow").setValue("true")
        //        }
        
        /////
        let chatThreadID = self.absExistingMessageThreadID
        swapRequestData["chatThreadID"] = chatThreadID

        //print("onewayAskPropertySetter absExistingMessageThreadID", self.absExistingMessageThreadID)
        swapRequestData["didAskShow"] = "true"
        
        self.ref.child("users/\(self.selfUID!)/myActiveChatThreads/\(chatThreadID)/didAskShow").setValue("true")
        self.ref.child("users/\(self.otherUID!)/myActiveChatThreads/\(chatThreadID)/didAskShow").setValue("true")
        
        //////
        //configure didSwap property observer in chatRequest Body - property used to filter for chat requests that transformed into swap requests in CVC
        if self.importedChatUser.keys.contains("chatRequestID") {
            //print("self.importedChatUser.keys.containschatRequestID")
            let chatRequestID = importedChatUser["chatRequestID"]!
            
            //configure didSwap property
            self.ref.child("globalChatRequests/\(self.selfUID!)/\(chatRequestID)/didSwap").setValue("true")
            self.ref.child("globalChatRequests/\(self.otherUID!)/\(chatRequestID)/didSwap").setValue("true")
            
            //configure fromChatRequest property
            swapRequestData["fromChatRequest"] = "yes"
            
        } else if self.chatRequestDoesExist == true {
            //print("self.chatRequestDoesExist == true")
            let chatRequestID = self.absExistingChatRequestID!
            self.ref.child("globalChatRequests/\(self.selfUID!)/\(chatRequestID)/didSwap").setValue("true")
            self.ref.child("globalChatRequests/\(self.otherUID!)/\(chatRequestID)/didSwap").setValue("true")
            
            //configure fromChatRequest property
            swapRequestData["fromChatRequest"] = "yes"
        } else {
            swapRequestData["fromChatRequest"] = "no"
        }
        
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        swapRequestData["swapRequestTimeStamp"] = timeStamp
        
        let childUpdates = [
            "/globalSwapRequests/\(selfUID)/\(newSwapRequestID)": swapRequestData,
            "/globalSwapRequests/\(otherUID)/\(newSwapRequestID)": swapRequestData,
        ]
        
        //create array of users who have requested swaps
        ref.updateChildValues(childUpdates)
        self.absExistingSwapRequestID = newSwapRequestID
    }
    
    func scarletMessenger(swapStatus: String?, shouldSetDidSeeRequest: Bool) {
        
        guard let imageData = UIImageJPEGRepresentation(self.visibleDisplayPhoto!, 0.4) else { return } //not returning photo
       
        //print("scarletMessenger imageData successfully loaded PASS")
        
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let newTimeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))

        storageRef!.child(imageStoragePath).putData(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                //print("Error uploading: \(error)")
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        
                        let visibleURL = (metadata?.downloadURLs?[0].absoluteString)!
                        
                        //if initiator != self, write visible image @swapRecipient x 2, else, write visible image @swapInitiatorx2
                        var imageStorageNode = ""
                        if self.selfInitiatedSwapRequest == true {
                            imageStorageNode = "swapInitiatorPhotoURL"
                        } else {
                            imageStorageNode = "swapRecipientPhotoURL"
                        }
                        
                        //image
                        let urlPathSelf = "globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/\(imageStorageNode)"
                        let urlPathOther = "globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/\(imageStorageNode)"
                        
                        //status
                        guard let swapStatus = swapStatus else { return }
                        let statusSelf = "globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapRequestStatus"
                        let statusOther = "globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapRequestStatus"
                        
                       //time
                        let timeSelf = "globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapRequestTimeStamp"
                        let timeOther = "globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapRequestTimeStamp"

                        let childUpdates = [
                            urlPathSelf: visibleURL,
                            urlPathOther: visibleURL,
                            statusSelf: swapStatus,
                            statusOther: swapStatus,
                        ]
                        
                        let timeSampUpdates = [
                            timeSelf: newTimeStamp,
                            timeOther: newTimeStamp,
                        ]
                        
                        self.ref.updateChildValues(childUpdates)
                        self.ref.updateChildValues(timeSampUpdates)
//                        if shouldSetDidSeeRequest == true {
//                            self.ref.updateChildValues(didSeeUpdates)
//                        }
                        
//                        //write URL x2
//                        self.ref.child("globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/\(imageStorageNode)").setValue(visibleURL)
//                        self.ref.child("globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/\(imageStorageNode)").setValue(visibleURL)
//
//                        //write status x2
//                        guard let swapStatus = swapStatus else { return }
//                        self.ref.child("globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapRequestStatus").setValue(swapStatus)
//                        self.ref.child("globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapRequestStatus").setValue(swapStatus)
                        
//                        //write new timeStamp x2
//                        self.ref.child("globalSwapRequests/\(self.selfUID!)/\(self.absExistingSwapRequestID!)/swapRequestTimeStamp").setValue(newTimeStamp)
//                        self.ref.child("globalSwapRequests/\(self.otherUID!)/\(self.absExistingSwapRequestID!)/swapRequestTimeStamp").setValue(newTimeStamp)
                        
                        //would also write clear name
                        
                    }
                }
            }
        }
    }

    var clearPhotoDeletedAlert: UIAlertController?
    
    func addAlert_clearPhotoDeletedFromStorage() {
        let alertTitle = "Upload New Photo"
//        let alertMessage = "Unable to upload your clear photo. Please add a new photo in Profile Editor to continue sharing with others."
        var alertMessage = "Unable to share your clear photo. Please add a new photo in Profile Editor to send with your requests."
        if let othVisus = self.otherVisus {
            if othVisus == "true" {
                alertMessage = "Unable to share your clear photo. Please add a new photo in Profile Editor."
            } else {
                alertMessage = "Unable to share your clear photo. Please add a new photo in Profile Editor to send with your requests."
            }
        }

        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okACtion = UIAlertAction(title: "OK", style: .default) { (action) in
            // Respond to user selection of the action.
            //exit to home without saving profile changes
            self.clearPhotoDeletedAlert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okACtion)
        
        self.clearPhotoDeletedAlert = alert
    }
    
//    func revelaro() {
//        guard let newPhotoURL = URL(string: self.revelaroURL!) else { return }
//        //print("R1. setting thumbnail image")
//
//        //create array of unlocked users in DB
//        self.ref.child("users/\(self.selfUID!)/unlockedUsers/\(self.otherUID!)").setValue("true")
//        self.updateSeenReceipt()
//    }

    var askToShowAlert: UIAlertController?
    
    func addAskToShowAlert() {
        
        let alertTitle = "Request Unlock"
        let alertMessage = "This will send " + "\(self.otherDisplayName!)" + " a request to unlock his full profile. Would you like to continue?"
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okACtion = UIAlertAction(title: "Request", style: .default) { (action) in
            self.askToShowAlert!.dismiss(animated: true, completion: nil)
            self.swapUI_1WayReveal_N_Helper()
            UserDefaults.standard.set("true", forKey: "askToShowAlert_Seen")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.askToShowAlert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okACtion)
        alert.addAction(cancelAction)
        
        self.askToShowAlert = alert
    }
    
    var twoWaySwapAlert: UIAlertController?
    
    func addTwoWaySwapAlert() {
        
        let alertTitle = "Request Unlock"
//        let alertMessage = "This will send " + "\(self.otherDisplayName!)" + " a request to unlock his full profile. Would you like to continue?"
        
//        let alertMessage = "Ask " + "\(self.otherDisplayName!)" + " to unlock? " + "\n" + "\(self.otherDisplayName!)" + " won't be able to see your full profile unless he also agrees to unlock his profile."
//        let alertMessage = "This will send " + "\(self.otherDisplayName!)" + " a request to unlock his full profile." + "\n" + "\(self.otherDisplayName!)" + " won't be able to see your full profile unless he also agrees to unlock his profile. Would you like to continue?"

        let alertMessage = "This will initiate an unlock exchange." + "\n" + "\(self.otherDisplayName!)" + " won't be able to see your full profile unless he also agrees to unlock his profile. Would you like to continue?"

        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okACtion = UIAlertAction(title: "Request", style: .default) { (action) in
            self.twoWaySwapAlert!.dismiss(animated: true, completion: nil)
            self.tapToSwap_2Way_N_Helper()
            UserDefaults.standard.set("true", forKey: "swapAlert_Seen")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.twoWaySwapAlert!.dismiss(animated: true, completion: nil)
            
        }
        
        alert.addAction(okACtion)
        alert.addAction(cancelAction)
        
        self.twoWaySwapAlert = alert
    }
    
    var oneWayShowAlert: UIAlertController?

    func addOneWayShowAlert() {
        
        let alertTitle = "Unlock Profile"
        //        let alertMessage = "This will send " + "\(self.otherDisplayName!)" + " a request to unlock his full profile. Would you like to continue?"
        
        let alertMessage = "This will make your profile fully visible to this user. Would you like to continue?"
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okACtion = UIAlertAction(title: "Unlock", style: .default) { (action) in
            self.oneWayShowAlert!.dismiss(animated: true, completion: nil)
            self.oneWayShow_Helper()
            UserDefaults.standard.set("true", forKey: "oneWayShowAlert_Seen")
            UserDefaults.standard.set("true", forKey: "approveIncomingOneWayAlert_Seen")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.oneWayShowAlert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okACtion)
        alert.addAction(cancelAction)
        
        self.oneWayShowAlert = alert
    }
    
    var acceptIncomingOneWayAlert: UIAlertController!
    
    func add1WayInAlert() {
        let alertTitle = "Unlock Profile"
        //        let alertMessage = "This will send " + "\(self.otherDisplayName!)" + " a request to unlock his full profile. Would you like to continue?"
        
        let alertMessage = "This will make your profile fully visible to this user. Would you like to continue?"
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okACtion = UIAlertAction(title: "Unlock", style: .default) { (action) in
            self.acceptIncomingOneWayAlert!.dismiss(animated: true, completion: nil)
            self.acceptIncomingRevealRequest_Helper()
            UserDefaults.standard.set("true", forKey: "approveIncomingOneWayAlert_Seen")
            UserDefaults.standard.set("true", forKey: "oneWayShowAlert_Seen")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            self.acceptIncomingOneWayAlert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okACtion)
        alert.addAction(cancelAction)
        
        self.acceptIncomingOneWayAlert = alert
    }
    
//    if let swapAlertSeen = UserDefaults.standard.string(forKey: "approveIncomingOneWayAlert_Seen") {
//        if swapAlertSeen == "true" {
//            self.acceptIncomingRevealRequest_Helper()
//        } else {
//            self.acceptIncomingRevealRequest_Helper()
//        }
//    } else {
//    self.present(self.acceptIncomingOneWayAlert!, animated: true)
//    }
//}
//
//func acceptIncomingRevealRequest_Helper() {
    
    //MARK: LIVE OBSERVERS METHODS  .................................................................................................................................................
    
    //!!!!!!!!! IMPORTANT: NOTE THAT WE DID NOT TEST THE NEW OBSERVESWAPCHANGES() METHOD BELOW AFTER REPLACING OLD ONE (WHICH WAS CALLING .VALUE INSTEAD OF .CHILDCHANGED). IF IT DOESN"T WORK THEN RESTORE THE OLD METHOD.
    
    func updateSelfSwapRequests(withSource: String) {
        //Note: called for outgoing 1Way reveal
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        guard let absExistingSwapRequestID = self.absExistingSwapRequestID else { return }
        
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        }
        
        guard chatThreadID != "" else { return }
         
        var requestBody = [String:String]()
        requestBody["swapID"] = absExistingSwapRequestID
        requestBody["swapInitiatorID"] = selfUID
        requestBody["swapRecipientID"] = otherUID
        requestBody["swapRequestStatus"] = "false"
        requestBody["chatThreadID"] = chatThreadID
        
        let childUpdates = [
            "/users/\(selfUID)/outgoingSwapRequests/\(absExistingSwapRequestID)": requestBody,
            ]
        self.ref.updateChildValues(childUpdates)
    }
    
    func sendRequestRSVP(withSource: String) {
        //print("sendRequestRSVP withSource,", withSource)
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        guard let absExistingSwapRequestID = self.absExistingSwapRequestID else { return }

        var requestBody = [String:String]()
        requestBody["swapID"] = absExistingSwapRequestID
        requestBody["swapInitiatorID"] = selfUID
        requestBody["swapRecipientID"] = otherUID
        requestBody["swapStatus"] = "false"
        if withSource == "oneWayOutgoing" {
            requestBody["objectType"] = "oneWay"
        } else if withSource == "twoWayOutgoing" {
            requestBody["objectType"] = "twoWay"
        }
        
        var chatThreadID = ""
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else if self.temporaryMessageThreadID != "" {
            chatThreadID = self.temporaryMessageThreadID
        }
        requestBody["chatID"] = chatThreadID

        //Note: When self receives && approves these requests, need to clear the pendingRequestBody. i.e. write Status == true when (1) approves incoming 1wayreveal (2) approves incoming 2wayswap
        let childUpdates = [
            "/users/\(otherUID)/pendingSwapRequests/\(absExistingSwapRequestID)": requestBody,
            "/users/\(selfUID)/pendingSwapRequests/\(absExistingSwapRequestID)": requestBody,
            ]
        
        self.ref.updateChildValues(childUpdates)
    }
    
    func observeUnlockedUsers() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/unlockedUsers").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            let unlockedUID = snapshot.key
            self.unlockedUsers.append(unlockedUID)
        })
    }
    
    func observeIncomingLiveSwapRequests() {
        //print("CSVC O1. now observing incoming live requests")
        
        self.ref.child("globalSwapRequests/\(selfUID!)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            let newSwapRequestID = snapshot.key
            
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }
            
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }
            
            guard let swapIsActive = newSwapRequestBody["swapIsActive"] else { return }
            
            guard swapIsActive != "nil" else { return }
            
            //we record instances of swapRequests where self is initiator&&other is recipient or where other is initiator and recpient is self
            if swapInitiatorID == self.otherUID! {
                
                self.swapRequestDoesExist = true
                self.selfInitiatedSwapRequest = false
                self.absExistingSwapRequestID = newSwapRequestID
                self.swapRequestStatus = swapRequestStatus
                
                self.swapUIControlFlow(fromLiveObserver: true)
                
            } else if swapInitiatorID == self.selfUID! && swapRecipientID == self.otherUID! {
                self.swapRequestDoesExist = true
                self.selfInitiatedSwapRequest = true
                self.absExistingSwapRequestID = newSwapRequestID
                self.swapRequestStatus = swapRequestStatus
                
            } else {
                //
            }
            
            self.observeSwapRequestStatusChanges()
            
            //SETrecipientDidSeeRequest_incoming
            if swapInitiatorID != self.selfUID! && swapRequestStatus == "false"  {
                if !self.ref_SelfModelController.didClearSwapAlertForUserID.contains(self.otherUID!) {
                    self.ref_SelfModelController.didClearSwapAlertForUserID.append(self.otherUID!)
                    self.ref.child("globalSwapRequests/\(self.selfUID!)/\(newSwapRequestID)/recipientDidSeeRequest").setValue("true")
                    self.ref_SelfModelController.didSeeSwapRequestForUID["\(self.otherUID!)"] = "true"
                }
            }
        })
    }
    
    func updateViewWillAppear_ClearSwapAlert() {
        //SETrecipientDidSeeRequest_incoming
        
        guard let swapID = self.absExistingSwapRequestID else { return }
        guard let selfInitRequest = self.selfInitiatedSwapRequest else { return }
        let swapRequestStatus = self.swapRequestStatus
        //outgoingRequests
        if selfInitRequest == true && swapRequestStatus == "false"  {
            if !self.ref_SelfModelController.didClearSwapAlertForUserID.contains(self.otherUID!) {
                self.ref_SelfModelController.didClearSwapAlertForUserID.append(self.otherUID!)
                self.ref.child("globalSwapRequests/\(self.selfUID!)/\(swapID)/recipientDidSeeRequest").setValue("true")
                self.ref_SelfModelController.didSeeSwapRequestForUID["\(self.otherUID!)"] = "true"
            }
        }
    }
    
    //BUG: Reproduce: Self init 2Way swap request -> oth approve -> navigate to CSVC from CLVC -> viewDidLoad() calls observeChanges() -> observeChanges() detects "true" as status of request && Calls swapUICOntrol -> Calls Case"true" which calls _fade2BlueUI. THIS IS NOT NEEDED BECAUSE THE VIEW SHOULD LOAD FROM DRAWVIEW. WHY IS OBSERVER FIRING? THIS IS BECAUSE CHANGE OBSERVER IS CALLING.VALUE AND NOT.CHILDCHANGED. NEED TO FIX THIS METHOD.
    
    //    func observeSwapRequestStatusChanges() {
    //        //LEFT OFF: this function will not work when viewDidLoad fires without existing chatThreadID. ToDo: Fire this function when self.absExistingChatThreadID didSet(). Create new liveChatID var to not interfere with existing chatID
    //
    //        //print("CSVC: LIVE SWAP STATUS OBSERVER")
    //
    //        guard let swapRequestID = self.absExistingSwapRequestID else { return }
    //        //make sure the guard statement terminates the function call if swapRequestID == nil
    //
    //        //print("GUARD. self.absExistingSwapRequestID is,", self.absExistingSwapRequestID!)
    //        //print("self.absExistingSwapRequestID!", self.absExistingSwapRequestID!)
    //
    //        //only observing for currently active swapRequest with current user, and not for any swap request with other users
    //        _refHandle = self.ref.child("globalSwapRequests/\(selfUID!)/\(swapRequestID)").observe(.value , with: { (snapshot: DataSnapshot) in
    //
    //            //print("CSVC observeSwapRequestStatusChanges snapshot is,", snapshot)
    //
    //            guard let modifiedSwapRequestBody = snapshot.value as? [String:String] else { return }
    //            //print("XZ2. modifiedSwapRequestBody is,", modifiedSwapRequestBody)
    //
    //            guard let modifiedSwapRequestStatus = modifiedSwapRequestBody["swapRequestStatus"] else { return }
    //            //print("XZ3. modifiedSwapRequestStatus is", modifiedSwapRequestStatus)
    //
    //            guard let swapInitiatorPhotoURL = modifiedSwapRequestBody["swapInitiatorPhotoURL"] else { return }
    //            //print("XZ4. newswapRecipientPhotoURL is", swapInitiatorPhotoURL )
    //
    //            guard let swapRecipientPhotoURL = modifiedSwapRequestBody["swapRecipientPhotoURL"] else { return }
    //            //print("XZ4. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
    //
    //            self.swapRequestStatus = modifiedSwapRequestStatus
    //
    //            if modifiedSwapRequestStatus == "true" {
    //                //get the users new URL
    //                //print("CSVC: LIVE SWAP STATUS OBSERVER modifiedSwapRequestStatus is,", modifiedSwapRequestStatus)
    //
    //                self.swapUIControlFlow(fromLiveObserver: true)
    //
    //                if self.selfInitiatedSwapRequest == true {
    //                    self.revelaroURL = swapRecipientPhotoURL
    //                    if self.selfVisus! == "false" && self.otherVisus! == "false" {
    //                        self.loadImageToMemory(withClearURL: swapRecipientPhotoURL)
    //                    }
    //
    //                } else if self.selfInitiatedSwapRequest == false {
    //                    self.revelaroURL = swapInitiatorPhotoURL
    //                    self.loadImageToMemory(withClearURL: swapInitiatorPhotoURL)
    //                    if self.selfVisus! == "false" && self.otherVisus! == "false" {
    //                        self.loadImageToMemory(withClearURL: swapInitiatorPhotoURL)
    //                    }
    //
    //                }
    //            }
    //        })
    //
    //    }
    
    //try this: under any circumstance, the liveObserverInit method will record the instance of the new request being added. For the .childChangedmethod, observe incomingChanges for selfUID && guardLet snapshot.key (swapRequestID) == self.swapRequestID (noted from initObserver). Then proceed with the modifications. I.E. Retrieve the image loader methods, and update requestStatus accordingly. This should solve your problem by only triggering the observerFunction && hence the swapControlFlowUI method only when status changes to True (WAIT!) Can we just call the animatorFunciton when .case==True for 2Way swaps? WHy not?
    
    func observeSwapRequestStatusChanges() {
        //print("CSVC: LIVE SWAP STATUS OBSERVER")
        
        //GUARD THAT we're only observing for swaprequest for current users only on screen
        guard let swapRequestID = self.absExistingSwapRequestID else { return }
        //make sure the guard statement terminates the function call if swapRequestID == nil
        
        //print("GUARD. self.absExistingSwapRequestID is,", self.absExistingSwapRequestID!)
        //print("self.absExistingSwapRequestID!", self.absExistingSwapRequestID!)
        
        //only observing for currently active swapRequest with current user, and not for any swap request with other users
        _refHandle = self.ref.child("globalSwapRequests/\(selfUID!)").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            
            //print("CSVC observeSwapRequestStatusChanges snapshot is,", snapshot)
            
            let swapRequestIDKey = snapshot.key
            //print("XZ swapRequestID is,", swapRequestID)
            
            guard let modifiedSwapRequestBody = snapshot.value as? [String:String] else { return }
            //print("XZ2. modifiedSwapRequestBody is,", modifiedSwapRequestBody)
            
            guard let swapInitiatorID = modifiedSwapRequestBody["swapInitiatorID"] else { return }
            //print("L4. newswapInitiatorID is", swapInitiatorID )
            guard let swapRecipientID = modifiedSwapRequestBody["swapRecipientID"] else { return }
            //print("L5. newswapRecipientID is", swapRecipientID )
            
            guard let modifiedSwapRequestStatus = modifiedSwapRequestBody["swapRequestStatus"] else { return }
            //print("XZ3. modifiedSwapRequestStatus is", modifiedSwapRequestStatus)
            
            guard let swapInitiatorPhotoURL = modifiedSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            //print("XZ4. newswapRecipientPhotoURL is", swapInitiatorPhotoURL )
            guard let swapRecipientPhotoURL = modifiedSwapRequestBody["swapRecipientPhotoURL"] else { return }
            //print("XZ4. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
            
            //Visus Property
            guard let swapInitiatorVisus = modifiedSwapRequestBody["swapInitiatorVisus"] else { return }
            //print("XZ5. swapInitiatorVisus is", swapInitiatorVisus)
            
            guard let swapRecipientVisus = modifiedSwapRequestBody["swapRecipientVisus"] else { return }
            //print("XZ6. swapRecipientVisus is", swapRecipientVisus)
            
            //didSee
            guard let initiatorDidSeeRequest = modifiedSwapRequestBody["initiatorDidSeeRequest"] else { return }

            guard let swapIsActive = modifiedSwapRequestBody["swapIsActive"] else { return }
            
            guard swapIsActive != "nil" else { return }
            
            self.swapRequestStatus = modifiedSwapRequestStatus
            
            guard swapRequestIDKey == swapRequestID else { return }
            //print("XZ PASSED")
            
            if modifiedSwapRequestStatus == "true" {
                //get the users new URL
                //print("CSVC: LIVE SWAP STATUS OBSERVER modifiedSwapRequestStatus is,", modifiedSwapRequestStatus)
                
                //2Way swap requests
                if self.selfVisus! == "false" && self.otherVisus! == "false" {
                    
                    //2Way Outgoing
                    if swapInitiatorID == self.selfUID! {
                        self.revelaroURL = swapRecipientPhotoURL
                        //print("CSVC OBSERVER WILL loadImageFromDatabaseToMemory_thenSave 1")
                        //
                        
                        //immediately when an outgoing 2Way swap is detected, text should change to unlocking profile & button should fade to blue & ring should rotate UNTIL photo is successfullly loaded -
                        let sliderButtonImageView = self.sliderButtonImageView!
                        
                        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
                        }) { (true) in
                            if self.didSetUnlockingProfileText == false {
                                
                                //print("2 wilLSet Unlocking profile from observeSwapRequestStatusChanges")
                                self.stateLabel!.text = "Unlocking Profile..."
                                self.didSetUnlockingProfileText = true
                            }
                        }
                        
                        if swapRecipientPhotoURL != "" ||  swapRecipientPhotoURL != "Clear" {
                            //animateLive2WayIncomingRequest LIVE CALLED FROM WITHIN OBSERVER TO ASCERTAIN THAT IMAGE HAS LOADED BEFORE BUTTON SWITCHES STATE
                            self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: swapRequestIDKey, photoChildNode: "swapRecipientPhotoURL", from2WaySwapObserver: true)
                        }
                        
                        //CLEARSwapRequest_Outgoing 2Way outgoing
                        if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "false" {
                            self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(swapRecipientID)
                            self.ref.child("globalSwapRequests/\(self.selfUID!)/\(swapRequestID)/initiatorDidSeeRequest").setValue("true")
                            self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapRecipientID)"] = "true"
                        }
                    }
                    
                    if swapInitiatorID == self.otherUID! {
                        self.revelaroURL = swapInitiatorPhotoURL
                        
                        //print("CSVC OBSERVER WILL loadImageFromDatabaseToMemory_thenSave 2")
                        //always set node of OTHER not of SELF to nil after image loads (i.e. is other swapRecipient or initiator & set accordingly)
                        
                        if swapInitiatorPhotoURL != "" ||  swapInitiatorPhotoURL != "Clear" {
                            self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapRequestIDKey, photoChildNode: "swapInitiatorPhotoURL", from2WaySwapObserver: true)
                        }
                    }
                    
                }
            
                self.stateLabel!.layer.removeAllAnimations()
                
                //ATTENTION:
                
                // swapUIControlFlow(fromLiveObserver: true) method call moved to loadImageFromDatabaseToMemory_thenSave

                //self.swapUIControlFlow(fromLiveObserver: true)
            }
            
            //ONEWAY SWAP REQUESTS
            if self.selfVisus! == "true" && self.otherVisus! == "false" && modifiedSwapRequestStatus == "true" && swapRecipientID == self.selfUID! {
                //print("CSVC CHANGESliveObserveIncoming oneway show AFTER url is successfully written")
                
                //load image to memory
                self.revelaroURL = swapInitiatorPhotoURL
                
                //print("CSVC OBSERVER WILL loadImageFromDatabaseToMemory_thenSave 3")
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapRequestIDKey, photoChildNode: "swapInitiatorPhotoURL", from2WaySwapObserver: false)
                
                if self.selfVisus! == "false" && self.otherVisus! == "false" {
                    //image is loaded to memory. Method call also triggers saving image to disk.
                    //print("CSVC OBSERVER WILL loadImageFromDatabaseToMemory_thenSave 4")
                    self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapRequestIDKey, photoChildNode: "swapInitiatorPhotoURL", from2WaySwapObserver: false)
                }
                
                //ATTN: method call NOT moved to image loader, since it is assumed that status updates to true ONLY after image is successfully loaded
                //call animator that shows that other unlocked profile
                //print("animateShowProfile_FillCircle source observer")
                self.swapUI_1WayShow_Incoming_Live(withSource: "observeChangesSwitchFx")
                
                //CLEARSwapRequest_Outgoing 1Way outgoing
                if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "false" {
                    self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(swapRecipientID)
                    self.ref.child("globalSwapRequests/\(self.selfUID!)/\(swapRequestID)/initiatorDidSeeRequest").setValue("true")
                    self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapRecipientID)"] = "true"
                }
            }
            
            if self.selfVisus! == "true" && self.otherVisus! == "false" && modifiedSwapRequestStatus == "true" && swapRecipientID == self.otherUID! {
                //print("CSVC OneWay Request Outgoing CHANGESliveObserveIncoming oneway show AFTER url is successfully written 5")
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: swapRequestIDKey, photoChildNode: "swapRecipientPhotoURL", from2WaySwapObserver: false)
     
                self.outgoingRequestApproved = true
                self.configureOutgoingRevealApproved_D_UI()
                //ATTENTION:
                //call animator that shows that other unlocked profile
                //METHOD CALL MOVED TO IMAGE LOADER TO MAKE SURE THAT IMAGE IS LOADED BEFORE UI UPDATES
                
//                self.swapUI_1WayShow_Incoming_Live()
            }
        })
    }
    
    var outgoingRequestApproved: Bool?
    
    var cORA_D = false
    func configureOutgoingRevealApproved_D_UI() {
        //print("CSVC configureOutgoingRevealApproved_D_UI")
        
        guard cORA_D == false else { return }
        
        //1. remove previous animations
        self.pulsatingLayer?.removeAllAnimations()
//        self.pulsatingLayer?.removeFromSuperlayer()
        self.pulsatingLayer?.isHidden = true
        self.ringLayer?.isHidden = true
        
        //2. add rotating layer animations
        self.addRotatingLayer(shouldGrow: false, LRC: "C")
        
        //3. when image loads successfuly in loadImageFromDB then need to stop all animations and show to Fill
        self.cORA_D = true
    }
    
    //MARK: State Updaters
    //note the below method needs to also be accessible from UPVC as user can "open chat request" from there too
    func updateSelfDidOpenChatRequest() {
        //true or false as! String
        //print("updateSelfDidOpenChatRequest")
        if self.importedChatUser.keys.contains("chatRequestID") {
            //print("updateSelfDidOpenChatRequest A")
            
            let chatRequestId = importedChatUser["chatRequestID"]!
            let selfUID = self.selfUID!
            let otherUID = self.otherUID!
            
            //2 cases: sender is self, or sender is recipient & update accordingly
            if self.selfUID! == importedChatUser["chatRequestSenderUID"]! {
                //print("T1")
                if importedChatUser["chatRequestSenderDidOpen"]! == "false" {
                    //print("T1.1")
                    ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/chatRequestSenderDidOpen").setValue("true")
                    ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/chatRequestSenderDidOpen").setValue("true")
                }
                
            } else if self.selfUID! == importedChatUser["chatRequestRecipientUID"]! {
                //print("T2")
                if importedChatUser["chatRequestRecipientDidOpen"]! == "false" {
                    //print("T2.2")
                    ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/chatRequestRecipientDidOpen").setValue("true")
                    ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/chatRequestRecipientDidOpen").setValue("true")
                }
            }
        }
    }
    
    //note: didOpen values should also be initialized when user approves request from within CSVC since would have opened if accepted inside
    func updateSelfDidOpenCompletedSwapRequest() {
        
        if self.importedChatUser.keys.contains("swapRequestID") {
            
            let swapRequestID = importedChatUser["swapRequestID"]!
            let selfUID = self.selfUID!
            let otherUID = self.otherUID!
            
            if self.selfUID! == importedChatUser["swapInitiatorID"]! {
                //print("T1")
                if importedChatUser["swapInitiatorDidOpen"]! == "false" {
                    //print("T1.1")
                    ref.child("globalSwapRequests/\(selfUID)/\(swapRequestID)/swapInitiatorDidOpen").setValue("true")
                    ref.child("globalSwapRequests/\(otherUID)/\(swapRequestID)/swapInitiatorDidOpen").setValue("true")
                }
                
            } else if self.selfUID! == importedChatUser["swapRecipientID"]! {
                //print("T2")
                if importedChatUser["swapRecipientDidOpen"]! == "false" {
                    //print("T2.2")
                    ref.child("globalSwapRequests/\(selfUID)/\(swapRequestID)/swapRecipientDidOpen").setValue("true")
                    ref.child("globalSwapRequests/\(otherUID)/\(swapRequestID)/swapRecipientDidOpen").setValue("true")
                }
            }
        }
        
//        if self.importedChatUser.keys.
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var loadedImageToMemory_Other: UIImage?

    //Problem: when incoming swap detected and immediately approved, there may not be enough time for the image to load to memory. i.e.
    //Problem: User needs to be able to unlock other's photo when accessing CSVC from !withinCSVC
    
    var animateLive2WayIncomingRequestDidSet = false
    
    func loadImageFromDatabaseToMemory_thenSave(withClearURL: String, withRequestID: String, photoChildNode: String, from2WaySwapObserver: Bool) {
        //print("CSVC loadImageToMemory()")
        //print("CSVC loadImageFromDatabaseToMemory_thenSave withClearURL", withClearURL)
        
        let newPhotoURL = URL(string: withClearURL)
        
        self.imageLoaderView!.sd_setImage(with: newPhotoURL) { (loadedImage, error, _, savedURL) in
            
            if error == nil {
                self.saveUnlockedImageToDisk(imageObject: loadedImage!)
                self.loadedImageToMemory_Other = loadedImage
                //print("revelaro successfully loaded image")
                
                let selfUID = self.selfUID!
                let otherUID = self.otherUID!
                
                //print("loadImageFromDatabaseToMemory_thenSave will clear photoURL")
//                self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
//                self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                
                self.swapUIControlFlow(fromLiveObserver: true)
                
                if self.outgoingRequestApproved == true {
//                    self.swapUI_1WayShow_Incoming_Live(withSource: "loadImageFromDatabaseToMemory_thenSave")
                        self.swapUI_Outgoing1WayRevealApproved_D_Live(withSource: "loadImageFromDatabaseToMemory_thenSave")
                }
                
                //in case of 2Way swap from live observer
                if from2WaySwapObserver == true && self.animateLive2WayIncomingRequestDidSet == false {
//                    self.animateLive2WayIncomingRequest()
                    //print("will remove rotanimation")
                    self.rotatingLayer?.removeAllAnimations()
                    self.rotatingLayer?.removeFromSuperlayer()
                    self.animateRevealProfile()
                    self.sliderButtonImageView?.isUserInteractionEnabled = true
                    self.animateLive2WayIncomingRequestDidSet = true
                }
                
                //print("willSet tapToOpen from loadImageToMemory")
                self.stateLabel?.text = "Tap to Open"
                
            } else {
                
            }
        }
    }
    
    
    //set swapInitiatorPhotoURL || swapRecipientPhotoURL == "Empty"

    func deleteClearPhotoFromDB(withClearPhotoURL: String) {
//        // Create a reference to the file to delete
//        //        let storageURL = self.myProfileInfoDict["userPhotoStorageRef"]!
//        //print("storageURL is,", storageURL)
//
//        let photoRef = storageRef.child(storageURL)
//
//        // Delete the file
//        photoRef.delete { error in
//            if let error = error {
//                // Uh-oh, an error occurred!
//            } else {
//                // File deleted successfully
//            }
//        }
    }
    
    func saveUnlockedImageToDisk(imageObject: UIImage) {
        //print("PV1. writing to vault")
        let fileName = "\(self.otherUID!).jpeg"
        if let data = UIImageJPEGRepresentation(imageObject, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("PV2. photo written to vault")
        }
    }
    
 //SAVE IMAGE WITH ENCRYPTION COMPLETE ENABLED
    
//    func saveUnlockedImageToDisk(imageObject: UIImage) {
//        //print("PV1. writing to vault")
//        let fileName = "\(self.otherUID!).jpeg"
//        if let data = UIImageJPEGRepresentation(imageObject, 0.4) {
//            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
//            try? data.write(to: fileURL, options: ([.atomic, .completeFileProtection]))
//            //print("PV2. COMPLETE FILE PROTECTION photo written to vault")
//        }
//    }
    
//    func loadOtherClearPhotoFromDisk(otherUID: String) -> UIImage? {
//        //print("PV1. accesing photo vault")
//        let fileName = "\(otherUID).jpeg"
//        let fileURL = documentsUrl.appendingPathComponent(fileName)
//        do {
//            let imageData = try Data(contentsOf: fileURL)
//            //print("PV2. Successful try UIIMage")
//            return UIImage(data: imageData)
//        } catch {
//            //print("PV2E. Error loading image : \(error)")
//        }
//        return nil
//    }
    
    func loadClearPhotoFromDisk_Self() -> UIImage? {
        //print("getClearPhoto accesing photo vault")
        let fileName = "\(selfUID!).jpeg"
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            let uiImage = UIImage(data: imageData)
            self.visibleDisplayPhoto = uiImage
        } catch {
            //print("Error loading image : \(error)")
        }
        return nil
    }
    
    //Required: if 2Way swapstatus is true AND otherUID is not unlocked THEN need to loadImageToMemoryFromDB
    //ELSE if otherUID IS Unlocked THEN no action required since tapToUnlock would be inactive
    
    func loadClearPhotoFromDisk_Other() -> UIImage? {
        //this function should only trigger if user is unlocked
        
        //print("loadClearPhotoFromDisk_Other accesing photo vault")
        
        let fileName = "\(self.otherUID!).jpeg"
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            //print("loadClearPhotoFromDisk_Other successful try UIIMage")
            let uiImage_Other = UIImage(data: imageData)
            self.loadedImageToMemory_Other = uiImage_Other
            return uiImage_Other
        } catch {
            //print("PV2E. Error loading image : \(error)")
        }
        return nil
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    //MARK: SYNC MESSAGES ............................................................................................................................................................
    
    var activeMessageThreadsObserver: DatabaseHandle!
    
    func observeActiveMessageThreads() {
        //print("CSVC calling observeActiveMessageThreads")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid

        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        activeMessageThreadsObserver = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childChanged, with: { (snapshot: DataSnapshot) in
            
            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder

            guard let chatThreadBody = snapshot.value as? [String:String] else { return }

            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }

            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }

            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }

            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }

            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }

            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }

            guard let didAskShow = chatThreadBody["didAskShow"] else { return }

            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
        
            guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
            guard chatIsActive != "nil" else { return }
            
            guard threadIDActive == "true" else { return}
        
            if chatInitiatorUID == selfUID && chatRecipientUID == self.otherUID! {
                //self initiated chatThread //will never happen within UPVC

            } else if chatInitiatorUID == self.otherUID! && chatRecipientUID == self.selfUID! {
                //other initiated chatThread
                self.absExistingMessageThreadID = newChatThreadID
                self.messageThreadIDDoesExist = true
                
                //if new absExistingMessageThreadID is updated, call status updater method away from temp message ID
                
                //ATTENTION: METHOD CALL ACTIVATED EVEN AFTER VC LEAVING VIEW. RESOLVED.
//                self.setUserCSVCPresenceTrue(withMethodCallSource: "observeActiveMessageThreads") //DEACTIVATE
            }
            
            //call this function only once when self.messagesArray == 0
            self.configureDatabaseMessages()
        })
    }
    
    var messageIDArray = [String]()
    
    var tempDeletedMessageThreadID: String?
    
    func configureDatabaseMessages() {
        let ref = Database.database().reference()
        
        let selfUID = self.selfUID!
        
        var chatThreadIDKey = self.absExistingMessageThreadID
        
        if let tempDelID = self.tempDeletedMessageThreadID {
            //print("PREVIOUSLY DELETED THREAD ID DOES EXIST", tempDelID)
            chatThreadIDKey = self.tempDeletedMessageThreadID!
        }
        
        //print("configureDatabase FINAL chatThreadIDKey", chatThreadIDKey)
        
        let messagesTable = self.messagesTableView!
        
        //print("keyToUse is", chatThreadIDKey)
        _refHandle = ref.child("globalChatThreads/\(chatThreadIDKey)").observe(.childAdded , with: { (snapshot: DataSnapshot) in
            //print("observed incoming message object", snapshot.value)
            let messageID = snapshot.key
            //print("messageID is,", messageID)
            
            //try to see why the array is producing duplicates, and solve in another way. Are you recalling the same observer multiple times? Check this bitch
        
            if !self.messageIDArray.contains(messageID) {
          
                let messageBody = snapshot.value as! [String:String]
                if let selfDidDeleteMessage = messageBody["\(selfUID)_didDelete"] {
                    if selfDidDeleteMessage == "false" {
                        self.messageIDArray.append(messageID)
                        self.messagesArray.append(snapshot)
                    } else {
                        //do not append deleted message
                    }
                } else {
                    self.messageIDArray.append(messageID)
                    self.messagesArray.append(snapshot)
                }
                //
                //print("CALLING ACTIVATESLIDER WITHIN CONFIGURE DATABSE AKA> WHEN A NEW MESSAGE IS OBSERVED AND APPENDED TO ARRAY")
                self.activateSlider()
            } else {
                
            }
            
            //self.messagesTable.insertRows(at: [IndexPath(row: self.messagesArray.count - 1, section: 0)], with: .automatic)
            messagesTable.reloadData()
            self.scrollToBottomMessage()
            
            //DONT FORGET TO ADD A DEINITIALIZER FOR THIS LISTENER
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messagesArray.count
    }
    
    func sizeOfString(string: String, constrainedToWidth width: Double) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir Next", size: 14.0)! ],
            context: nil).size
    }
    
    var textGray = UIColor(displayP3Red: 185/255, green: 185/255, blue: 185/255, alpha: 1)
    
    /*
    func cellTextViewFrameProperties() {
        //
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        
        let insetRatio = 19/414 as CGFloat
        let insetUnit = insetRatio * screenWidth
        
        let chatSpaceWidth = screenWidth - (2 * insetUnit)
        let messageSpaceRatio = 329 / 376 as CGFloat
        
        let cWidth = ((self.chatSpaceView?.frame.width)! - (2 * insetUnit)) * messageSpaceRatio
        var chatCellTextViewFrame = CGRect(x: 0, y: 0, width: cWidth, height: roundedCGsize)
        let chatCellTextView = UITextView(frame: chatCellTextViewFrame)
        chatCellTextView.font = UIFont(name: "Avenir Next", size: 14)
        chatCellTextView.backgroundColor = UIColor.clear
    }
     */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let CellIdentifier = "chatCell"
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        
//        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
//        if let cell = cell {
//            // populate cell
//        }
//        else {
//            cell = MyCell()
//            // populate cell
//        }
//
//        return cell
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath)
        
        let messageSnapshot: DataSnapshot! = messagesArray[indexPath.row]
        let message = messageSnapshot.value as! [String:String]
        let text = message["chatMessage"]! as String
        let messageSenderUID = message["messageSenderUID"]!
        
        let i = (self.viewParameters?.insetUnit)!
        let messageSpaceRatio = 329 / 376 as CGFloat
        let cWidth = Double(((self.chatSpaceView?.frame.width)! - (2 * i)) * messageSpaceRatio)
        
        let stringCGSize = self.sizeOfString(string: text, constrainedToWidth: cWidth)
        let roundedCGsize = ceil(stringCGSize.height)
        
        cell?.backgroundColor = UIColor.blue
        
        if cell !== nil { //if cell exists
            //print("Cell is nil")
            
            cell = UITableViewCell(style: .default, reuseIdentifier: CellIdentifier)
            cell?.backgroundColor = UIColor.white
            
            ///adding textView
            let i = (self.viewParameters?.insetUnit)!
            let messageSpaceRatio = 329 / 376 as CGFloat
            let cWidth = ((self.chatSpaceView?.frame.width)! - (2 * i)) * messageSpaceRatio
            var chatCellTextViewFrame = CGRect(x: 0, y: 0, width: cWidth, height: roundedCGsize)
            let chatCellTextView = UITextView(frame: chatCellTextViewFrame)
            chatCellTextView.font = UIFont(name: "Avenir Next", size: 14)
            chatCellTextView.backgroundColor = UIColor.clear
            
//            let tableWidth = chatSpaceView!.bounds.width - (1.2 * (viewParameters?.insetUnit)!)
            //lose 0.8 of the chatspace width to get to the original space
            
            if messageSenderUID == self.selfUID! {
//                    let messageBody = "You: " + "\n" + text
                    let messageBody = "You: " + text
                    let name = "You:"
                    
                    let range1 = (messageBody as NSString).range(of: name)
                    let range2 = (messageBody as NSString).range(of: text)
                    
                    let attributedString = NSMutableAttributedString(string: messageBody)
                    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range1)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir Medium", size: 14.0)!, range: range1)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir Medium", size: 14.0)!, range: range2)
                    chatCellTextView.attributedText = attributedString
                
            } else {
                    let othName = "\(self.otherDisplayName!): "
                    let messageBody = othName + text
                    
                    let blueColor = UIColor(red: 0, green: 196/255, blue: 1, alpha: 1)
                    let range1 = (messageBody as NSString).range(of: othName)
                    let range2 = (messageBody as NSString).range(of: text)
                    
                    let attributedString = NSMutableAttributedString(string: messageBody)
                    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: blueColor, range: range1)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir Medium", size: 14.0)!, range: range1)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir Next", size: 14.0)!, range: range2)
                    chatCellTextView.attributedText = attributedString
            }
            
            let selfUID = self.selfUID!
            let otherUID = self.otherUID!
            
            if self.selfWasDismissed == true {
                //print("tableView will grayOut text")
                chatCellTextView.textColor = textGray
            }
            
            if text == "outgoingRevelaro_\(selfUID)" || text == "outgoingRevelaro_\(otherUID)" || text == "outgoingRevelaro_D_\(otherUID)" || text == "outgoingRevelaro_D_\(selfUID)" || text == "swap_\(selfUID)" || text == "swap_\(otherUID)" || text == "swap_D_\(otherUID)" || text == "swap_D_\(selfUID)" || text == "outgoingOneWayShow_\(otherUID)" || text == "outgoingOneWayShow_\(selfUID)" || text == "swap_Nil_\(otherUID)" || text == "swap_Nil_\(selfUID)"   {
                
                //print("outgoingRevealRequest by self detected")
                //let requestCenterX = (tableWidth - (0.8 * (viewParameters?.insetUnit)!)) * 0.5
                //or make the request width as wide as the tablewidth - 0.8 inset & set x = 0 & center text
                let requestFrameWidth = self.messagesTableView!.bounds.width - (0.8 * (viewParameters?.insetUnit)!)
//                let requestFrameWidth = self.messagesTableView!.bounds.width

                let requestFrameSize = self.sizeOfCopy(string: "Hello There", constrainedToWidth: Double(requestFrameWidth), fontName: "AvenirNext-Medium", fontSize: 10.0)
                let requestFrameCGHeight = ceil(requestFrameSize.height)
//                chatCellTextViewFrame = CGRect(x: 0, y: 0, width: requestFrameWidth, height: roundedCGsize)
                chatCellTextViewFrame = CGRect(x: 0, y: 0, width: requestFrameWidth, height: requestFrameCGHeight)
                chatCellTextView.frame = chatCellTextViewFrame
                chatCellTextView.font = UIFont(name: "AvenirNext-Medium", size: 10)!
                let othName = "\(self.otherDisplayName!)"
                let upperCaseName = othName.uppercased()
//                chatCellTextView.text = "\n" + "YOU ASKED \(upperCaseName) TO SHOW HIS PROFILE!"
                
                var finalDismissHeader = "Swap Complete"
                
                //1way reveals
                if text == "outgoingRevelaro_\(selfUID)" {
                    //outgoing reveal request
                    chatCellTextView.text = "YOU ASKED \(upperCaseName) TO SHOW HIS PROFILE"
                } else if text == "outgoingRevelaro_\(otherUID)" {
                    //incoming reveal request
                    chatCellTextView.text = "\(upperCaseName) ASKED TO SEE YOUR PROFILE"
                    finalDismissHeader = "Request Complete"
                } else if text == "outgoingRevelaro_D_\(otherUID)" {
                    //outgoingRevealApproved
                    chatCellTextView.text = "\(upperCaseName) REVEALED HIS PROFILE"
                } else if text == "outgoingRevelaro_D_\(selfUID)" {
                     chatCellTextView.text = "YOU REVEALED YOUR PROFILE"
                    finalDismissHeader = "Request Complete"
                }
                
//                "swap_\(selfUID) == outgoingSwapRequest"
//                "swap_\(otherUID) == incomingSwapRequest"
//
//                "swap_D_\(selfUID) == incomingSwapApproved"
//                "swap_D_\(otherUID) == outgoingSwapApproved"
                
                //2way swaps
                if text == "swap_\(selfUID)" {
                    //outgoing swap request
                    chatCellTextView.text = "YOU ASKED \(upperCaseName) TO SWAP PROFILES"
                } else if text == "swap_\(otherUID)" {
                    //incoming swap request
                    chatCellTextView.text = "\(upperCaseName) ASKED TO SWAP PROFILES"
                } else if text == "swap_D_\(otherUID)" {
                    //outgoing swap approved
                    chatCellTextView.text = "\(upperCaseName) APPROVED SWAP"
                } else if text == "swap_D_\(selfUID)" {
                    //incoming swap approved
                    chatCellTextView.text = "YOU APPROVED SWAP"
                }
                
                //oneway show
                if text == "outgoingOneWayShow_\(otherUID)" {
                    chatCellTextView.text = "\(upperCaseName) SHOWED HIS PROFILE"
                } else if text == "outgoingOneWayShow_\(selfUID)" {
                    chatCellTextView.text = "YOU SHOWED YOUR PROFILE"
                    finalDismissHeader = "Photo Received"
                }
                
                chatCellTextView.textAlignment = .center
                chatCellTextView.textColor = textGray
                chatCellTextView.backgroundColor = UIColor.white
                cell?.backgroundColor = UIColor.white
                
                //incoming dismissal
                if text == "swap_Nil_\(otherUID)" {
                    
                    let string = "\(finalDismissHeader)" + "\n" + "We'll let you know if \(othName) wants to chat any further"
                    
                    //1. adjust frame to fit size of text
                    let requestFrameWidth = self.messagesTableView!.bounds.width - (0.8 * (viewParameters?.insetUnit)!)
                    let requestFrameSize = self.sizeOfCopy(string: string, constrainedToWidth: Double(requestFrameWidth), fontName: "AvenirNext-Medium", fontSize: 12.0)
                    let requestFrameCGHeight = ceil(requestFrameSize.height)
                    chatCellTextViewFrame = CGRect(x: 0, y: 0, width: requestFrameWidth, height: requestFrameCGHeight)
                    chatCellTextView.frame = chatCellTextViewFrame
                    
                    //2. configure text
                    let tBold = "\(finalDismissHeader)"
                    let regText = "We'll let you know if \(othName) wants to chat any further"
                    
                    let range1 = (string as NSString).range(of: tBold)
                    let range2 = (string as NSString).range(of: regText)
                    
                    let attributedString = NSMutableAttributedString(string: string)
                    
                    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range1)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-Medium", size: 12.0)!, range: range1)
                    
                    attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range2)
                    attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: 12.0)!, range: range2)
                    
                    chatCellTextView.attributedText = attributedString
                    chatCellTextView.textAlignment = .center
                    chatCellTextView.backgroundColor = UIColor.white
                }
                
                //outgoing dismissal
                if text == "swap_Nil_\(selfUID)" {
                    chatCellTextView.text = ""
                }
            }
  
//            cell?.contentView.addSubview(chatCellLabel)
//            chatCellTextView.font = UIFont(name: "Avenir Next", size: 14)
            chatCellTextView.isEditable = false
            chatCellTextView.isScrollEnabled = false
            chatCellTextView.textContainer.lineFragmentPadding = 0
            chatCellTextView.textContainerInset = .zero
            chatCellTextView.isSelectable = false
            
            cell?.contentView.addSubview(chatCellTextView)
//            cell?.backgroundColor = UIColor.blue

//            chatCellTextView.leadingAnchor.constraint(equalTo: (cell?.contentView.leadingAnchor)! , constant: 0).isActive = true
//            chatCellTextView.trailingAnchor.constraint(equalTo: (cell?.contentView.trailingAnchor)! , constant: 0).isActive = true
//            chatCellTextView.topAnchor.constraint(equalTo: (cell?.contentView.topAnchor)!, constant: 0).isActive = true
//            chatCellTextView.bottomAnchor.constraint(equalTo: (cell?.contentView.bottomAnchor)!, constant: 0).isActive = true

        } else {
            
            //cell is not empty and will reuse a cell
//            if let chatCell = cell {
//                let chatCellLabel = chatCell.contentView.viewWithTag(7)! as! UILabel
//
//                let text = message["chatMessage"]!
//                let messageSenderUID = message["messageSenderUID"]!
//
//                if messageSenderUID == self.selfUID! {
//                    chatCellLabel.text = "You: " + text
//                } else {
//                    chatCellLabel.text = "\(self.otherDisplayName!): " + text
//                }
//                chatCellLabel.font = UIFont(name: "Avenir Next Regular", size: 14.0)
//                chatCellLabel.autoresizingMask = [.flexibleRightMargin, .flexibleHeight]
//                cell?.contentView.addSubview(chatCellLabel)
//            }
        }
        
        if let cell = cell {
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let messageSnapshot: DataSnapshot! = messagesArray[indexPath.row]
        let message = messageSnapshot.value as! [String:String]
        let text = message["chatMessage"]! as String
        
        let i = (self.viewParameters?.insetUnit)!
        let messageSpaceRatio = 329 / 376 as CGFloat
        
        let cWidth = Double(((self.chatSpaceView?.frame.width)! - (2 * i)) * messageSpaceRatio)
        //        let i = (self.viewParameters?.insetUnit)!
        //        let messageSpaceRatio = 329 / 376 as CGFloat
        //        let textFrameWidth = messageSpaceRatio - i

        let stringCGSize = self.sizeOfString(string: text, constrainedToWidth: cWidth)
        let roundedCGsize = ceil(stringCGSize.height)
        let bottomPadding = 7 as CGFloat
        
        //configure cellheight for 1way outgoing request
        //if
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
//        "outgoingRevelaro_\(selfUID)" = outgoingRevealRequest
//        "outgoingRevelaro_\(otherUID)" = incomingRevealRequest
//        "outgoingRevelaro_D_\(selfUID)" = outgoingRevealApproved
//        "outgoingRevelaro_D_\(otherUID)" = incomingRevealApproved
        
//        if text == "outgoingOneWayShow_\(otherUID)" {
//            chatCellTextView.text = "\(upperCaseName) SHOWED HIS PROFILE"
//        } else if text == "outgoingOneWayShow_\(selfUID)" {
//            chatCellTextView.text = "YOU SHOWED YOUR PROFILE"
//        }
        
        if text == "outgoingRevelaro_\(selfUID)" || text == "outgoingRevelaro_\(otherUID)" || text == "outgoingRevelaro_D_\(otherUID)" || text == "outgoingRevelaro_D_\(selfUID)" || text == "swap_\(selfUID)" || text == "swap_\(otherUID)" || text == "swap_D_\(otherUID)" || text == "swap_D_\(selfUID)" || text == "outgoingOneWayShow_\(otherUID)" || text == "outgoingOneWayShow_\(selfUID)" {
            
            let requestFrameSize = self.sizeOfCopy(string: "Hello There", constrainedToWidth: Double(cWidth), fontName: "AvenirNext-Medium", fontSize: 10.0)
            let requestFrameCGHeight = ceil(requestFrameSize.height)
            let requestCellHeight = requestFrameCGHeight + bottomPadding
            return requestCellHeight
        }
        
        if text == "swap_Nil_\(otherUID)" {
            let string = "Hello" + "\n" + "There"
            let requestFrameSize = self.sizeOfCopy(string: string, constrainedToWidth: Double(cWidth), fontName: "AvenirNext-Medium", fontSize: 12.0)
            let requestFrameCGHeight = ceil(requestFrameSize.height)
            let requestCellHeight = 1.5 * (requestFrameCGHeight + bottomPadding)
            
            return requestCellHeight
        }
        
        let finalCellHeight = roundedCGsize + bottomPadding
        return finalCellHeight
    }
    
    var didUpdateMessage = false
    
    func videoAnimation() {
        
        //1
        var data = ["chatMessage" : "Hi!" as String]

        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = selfUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = otherUID
        mdata["\(selfUID)_didDelete"] = "false"
        
        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
            ]
        //print("will update child values")
        
        if self.didUpdateMessage == false {
            self.didUpdateMessage = true
            ref.updateChildValues(childUpdates)
        }
        
        ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)").setValue("true") //replaced
        ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)").setValue("true") //replaced
        
        //2
        let when = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.sendMessageOther1()
        }
    }
    
    var didUpdateMessage2 = false

    
    func sendMessageOther1() {
        var data = ["chatMessage" : "Hello!" as String]
        self.didUpdateMessage = false

        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = otherUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = selfUID
        mdata["\(selfUID)_didDelete"] = "false"
        
        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
            ]
        if self.didUpdateMessage2 == false {
            self.didUpdateMessage2 = true
            ref.updateChildValues(childUpdates)
        }
        
        let when = DispatchTime.now() + 1.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.sendMessageSelf2()
        }
    }
    
    var didUpdateMessage3 = false

    
    func sendMessageSelf2() {
        
        //1
        var data = ["chatMessage" : "Wanna trade profiles?" as String]
        self.didUpdateMessage = false

        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = selfUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = otherUID
        mdata["\(selfUID)_didDelete"] = "false"
        
        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
            ]
        if self.didUpdateMessage3 == false {
            self.didUpdateMessage3 = true
            ref.updateChildValues(childUpdates)
        }
 
        //2
        let when = DispatchTime.now() + 1.0
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.sendMessageOther2()
        }
        
    }
    
    var didUpdateMessage4 = false
    
    func sendMessageOther2() {
        
        var data = ["chatMessage" : "Sure! Let's do it!" as String]
        self.didUpdateMessage = false

        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = otherUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = selfUID
        mdata["\(selfUID)_didDelete"] = "false"
        
        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
            ]
        
        if self.didUpdateMessage4 == false {
            self.didUpdateMessage4 = true
            ref.updateChildValues(childUpdates)
        }
        
        let when = DispatchTime.now() + 1.2
        DispatchQueue.main.asyncAfter(deadline: when) {

            self.initiateSwapSelfDemo()
            
        }
    }
    
    var didUpdateMessage5 = false
    
    func initiateSwapSelfDemo() {

        if self.didUpdateMessage5 == false {
            self.didUpdateMessage5 = true
            self.animateOutgoing2WaySwapRequest_Slide()
            self.animatePendingReveal_Outgoing_Hint()
            self.swapControlFlow()
            self.animate2WayRequest_Outgoing_FadeInFadeOut_P()
            self.keyImageView!.isUserInteractionEnabled = false
        }

    }
    
    func sendMessage(data: [String : String]) {
    
        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        let data = data
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = selfUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = otherUID
        mdata["\(selfUID)_didDelete"] = "false"

        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
        ]
        
        ref.updateChildValues(childUpdates)
        
        let idKeyUpdates = [
            "users/\(selfUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)" : "true",
            "users/\(otherUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)" : "true",
        ]
        
        ref.updateChildValues(idKeyUpdates)
    }
    
    func setChatRequestObjectChatThreadID() {
        //print("setChatRequestObjectChatThreadID")
        let chatThreadIDKey = self.absExistingMessageThreadID
        
        var chatRequestID = ""
        if self.importedChatUser.keys.contains("chatRequestID") {
            chatRequestID = self.importedChatUser["chatRequestID"]!
        } else if self.chatRequestDoesExist == true {
            chatRequestID = self.absExistingChatRequestID!
        }
        
        let selfUID = self.selfUID!
        //        let otherUID = importedChatUser["requestToUID"]!
        let otherUID = self.otherUID!
        
        let ref = Database.database().reference()
        
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestID)/existingChatThreadID").setValue(chatThreadIDKey) //replaced
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestID)/chatThreadDoesExist").setValue("true")
        
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestID)/existingChatThreadID").setValue(chatThreadIDKey) //replaced
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestID)/chatThreadDoesExist").setValue("true")
    }
    
    /*
     Badge count logic:
     1. Get current badge count
     2. Increment badge count by 1
     3. Update badge count in DB
     
     -- user resets badge count to 0 whenever exits app or enters background
 */
    
    var currentBadgeInt = 0 {
        didSet {
            print("didSet currentBadgeInt", currentBadgeInt)
        }
    }
    
    var finalBadgeInt = 0 {
        didSet {
            print("didSet finalBadgeInt", finalBadgeInt)
        }
    }

    //Note: Will you run into concurrency problems with this? Setting counter property on badge counter. May need to use transaction to ensure that transaction is acid.
    func observeUserBadgeCount() {
        
        guard self.otherPushID != "" else { return }
        
        self.ref = Database.database().reference()
                
        self.ref.child("messageNotifications/\(otherPushID)/").observe(.value) { (snapshot) in
            //print("CSVC observeSelectedUserPresence snapshot is,", snapshot)
            
            guard let snapshotValue = snapshot.value as? [String : String] else { return }
            //print("CSVC observeSelectedUserPresence snapshotValue is,", snapshotValue)
            guard let badgeString = snapshotValue["badgeCount"] else { return }
            //print("CSVC userPresence is,", userPresence)

            let badgeInt = Int(badgeString)
            
            guard badgeInt != nil else { return }

            let newBadgeInt = badgeInt! + 1
            self.finalBadgeInt = newBadgeInt
        }
    }
    
    func updateLastMessageMetaData(data: [String : String]) {
        
        print("imp1", self.importedChatUser)
        print("imp2", self.selectedUser)

        guard self.absExistingMessageThreadID != "" else { return }
        let chatThreadIDKey = self.absExistingMessageThreadID
        
        let data = data
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        //        if self.otherUserIsReadingMessage == true {
        //            mdata["recipientDidReadLastMessage"] = "true"
        //        } else if self.otherUserIsReadingMessage == false {
        //            mdata["recipientDidReadLastMessage"] = "false"
        //        }
        
        mdata["messageSenderUID"] = selfUID
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = otherUID
        mdata["recipientDidReadLastMessage"] = "false"
        
        //
        let finalBadgeIntString = String(finalBadgeInt)

        var notificationData = [String : String]()
        notificationData["forDisplayName"] = self.selfDisplayName!
        notificationData["senderID"] = self.selfUID!
        notificationData["messageBody"] = data["chatMessage"]!
        notificationData["chatThreadID"] = chatThreadIDKey
        notificationData["badgeCount"] = finalBadgeIntString
        notificationData["notificationType"] = "message"
        
        //check if you will need to have all last messages fanned out in user pushID node per chatThreadID, vs storing only last message for most recent push notification instance
        var childUpdates = [
            "/globalChatThreadMetaData/\(chatThreadIDKey)": mdata,
        ]
        
        //
        if self.otherPushID != "" {
            //
            print("otherPushID", otherPushID)

            childUpdates = [
                "/globalChatThreadMetaData/\(chatThreadIDKey)": mdata,
                "messageNotifications/\(otherPushID)": notificationData,
            ]
            
        } else {
            //
            print("NOPUSHID")
        }
        
        ref.updateChildValues(childUpdates)
        
//        //
//        "/globalChatThreadMetaData/messageSenderUID" : selfUID!,
//        "/globalChatThreadMetaData/messageTimeStamp" : timeStamp,
//        "/globalChatThreadMetaData/messageRecipientUID" : otherUID!,
//        "/globalChatThreadMetaData/recipientDidReadLastMessage" : "false",
//
//        //
//        "/messageNotifications/senderDisplayName" : myProfileInfoDict["selfDisplayName"]!,
//        "/messageNotifications/senderID" : selfUID!,
//        "/messageNotifications/messageBody" : data["chatMessage"]!,
//        "/messageNotifications/chatThreadID" : chatThreadIDKey,
    }
    
    func updateChatThreadMetaData() {
        //print("updateChatThreadMetaData()")
        
        guard self.absExistingMessageThreadID != "" else { return }
        guard self.messagesArray.count >= 1 else { return }
        
        let chatThreadIDKey = self.absExistingMessageThreadID
        
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        let lastMessageSnapshot = self.messagesArray.last!
        let lastMessageBody = lastMessageSnapshot.value as! [String:String]
        //print("Last Message Body is", lastMessageBody)
        
        //add more info?
        //lastMessageBody["defaultURL"] = self.selfDefaultURL!
        
        let childUpdates = [
            "/globalChatThreadMetaData/\(chatThreadIDKey)": lastMessageBody,
            ]
        
        ref.updateChildValues(childUpdates)
    }
    
    func updateChatRequestLastMessage(data: [String:String]) {
        //print("updateChatRequestLastMessage()")
        
        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        guard let chatRequestID = self.importedChatUser["chatRequestID"] else { return }
        //print("GUARD FOR CHAT OBJECT TYPE PASSED")
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
    
        let messageString = data["chatMessage"]
        
        //print("updateChatRequestLastMessage last message is,", messageString)
        
        //update chatThreadID
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestID)/existingChatThreadID").setValue(chatThreadIDKey) //replaced
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestID)/existingChatThreadID").setValue(chatThreadIDKey) //replaced
        
        //update lastMessage
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestID)/lastMessageString").setValue(messageString) //replaced
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestID)/lastMessageString").setValue(messageString) //replaced
    }
    
    func update1WaySwapOutgoingLastMessage(data: [String:String]) {
        //print("update1WaySwapOutgoingLastMessage()")
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        guard self.selfVisus! == "false" && self.otherVisus! == "true" else { return }
        //print("GUARD FOR 1WaySWAP OBJECT TYPE PASSED")

        let messageString = data["chatMessage"]
        
        //print("updateChatRequestLastMessage last message is,", messageString)
        if let absExistingSwapRequestID = self.absExistingSwapRequestID {
            //update lastMessage
            ref.child("globalSwapRequests/\(selfUID)/\(absExistingSwapRequestID)/lastMessage").setValue(messageString) //replaced
            ref.child("globalSwapRequests/\(otherUID)/\(absExistingSwapRequestID)/lastMessage").setValue(messageString) //replaced
        }
    }
    
    func updateGeneralChatLastMessage() {
        //print("updateChatRequestLastMessage()")
        
        let chatThreadIDKey = self.absExistingMessageThreadID
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        guard self.messagesArray.count >= 1 else { return }
        //print("GUARD FOR MESSAGE COUNT PASSED")
        
        let lastMessageSnapshot = self.messagesArray.last!
        let lastMessageBody = lastMessageSnapshot.value as! [String:String]
        //print("Last Message Body is", lastMessageBody)
        
        let messageString = lastMessageBody["chatMessage"]!
        
        ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadIDKey)/lastMessage").setValue(messageString)
        ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadIDKey)/lastMessage").setValue(messageString)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //print("ChatScreenVC viewWillDisappear")
        //print("self.updateChatThreadMetaData()")
        
        self.setUserCSVCPresenceFalse(withMethodCallSource: "CSVC viewWillDisappear")
//        self.
        
        //Deactivated last message updates in viewWillDisappear()
//        if self.absExistingMessageThreadID != "" {
//            self.updateChatThreadMetaData()
//            //            self.updateChatRequestLastMessage()
//        }
//        guard let otherPrivacy = self.otherPrivacy else { return }
//        if otherPrivacy == "false" {
//            self.updateGeneralChatLastMessage()
//        }
    }
    
    var chatRequestDoesExist: Bool?
    var absExistingChatRequestID: String?
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !textField.text!.isEmpty {
            let data = ["chatMessage" : textField.text! as String]
            
            if self.selfWasDismissed == true {
                //
                //print("textFieldShouldReturn selfWasDismissed is T willNot send text ")
            } else {
                //print("textFieldShouldReturn selfWasDismissed is FALSE WILL send text ")
                sendMessage(data: data)
            }
            
            self.textFieldView!.text = ""
            
            //set message thread id in chatrequestObject
            
            //!!!!!! WE WILL NOT BE ABLE TO DETECT chatRequestID KEY WHEN USER IS ACCESSED FROM PROFILE INSTEAD OF FROM CHAT SINCE PASSED OBJECT IS NOT CHATLISTOBJECT BUT USERPROFILEOBJECT. SO HOW TO RESOLVE THIS? COULD PASS AROUND CHATLISTOBJECT EVEN THROUGH USERPROFILESEGUE AND CHECK IF IMPORTED USER BELONGS TO THIS ARRAY, THEN UPDATE USERINFO FROM THIS ARRAY
            if self.importedChatUser.keys.contains("chatRequestID") {
                //print("textFieldShouldReturn setChatRequestObjectChatThreadID ")
                self.setChatRequestObjectChatThreadID()
                self.updateChatRequestLastMessage(data: data)
            } else if self.chatRequestDoesExist == true {
                self.setChatRequestObjectChatThreadID()
                self.updateChatRequestLastMessage(data: data)
            }
            
            //update the ChatThreadMetaData, make sure that the messageBody IS overwritten at same location
            self.updateLastMessageMetaData(data: data)
            
            //1WaySwap_Outgoing: Update lastMessageinSwapBody IFF self.messagesArray.count = 0
            if self.messagesArray.isEmpty {
                self.update1WaySwapOutgoingLastMessage(data: data)
            }

            //print("TEXTFIELD DID RETURN PROCESSED")
            
            //print("textFieldShouldReturn self.selfWasDismissed", self.self.selfWasDismissed)
            
            self.sendRowToTopChatListForUID(withSourceCall: "textFieldShouldReturn")
        }
            return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let characterCountLimit = 500
        let startingLength = textField.text?.count ?? 0
        let lengthToAdd = string.count
        let lengthToReplace = range.length
        let stringLength = startingLength + lengthToAdd - lengthToReplace
        
//        if self.selfWasDismissed == true {
//            return false
//        } else {
//            return stringLength <= characterCountLimit
//        }
        
        return stringLength <= characterCountLimit

        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    
    func activateSlider() {
        
        if self.messagesArray.count == 1 && self.selfVisus! == "false" && self.otherVisus! == "false" && self.swapRequestDoesExist! == false {
            
            let sView = self.sliderButtonImageView!
            let kView = self.keyImageView!
            let wView = self.wImageViewLHS_i!
            
            UIView.transition(with: sView, duration: 0.3, options: .transitionCrossDissolve, animations: {
               sView.image = UIImage(named: "sliderOutlineOrange")
            }, completion: nil)
            
            UIView.transition(with: kView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                kView.image = UIImage(named: "sliderKeyOrangeFill")
            }, completion: nil)
            
            UIView.transition(with: wView, duration: 0.3, options: .transitionCrossDissolve, animations: {
                wView.image = UIImage(named: "sliderWigglerLHSOrangeFill")
            }) { (true) in
                self.animateSlideToSwap_Hint()
            }
            self.keyImageView!.isUserInteractionEnabled = true
        }
    }
    
    func scrollToBottomMessage() {
        let messagesTable = self.messagesTableView!
        
        if messagesArray.count == 0 { return }
        let bottomMessageIndex = IndexPath(row: messagesTable.numberOfRows(inSection: 0) - 1, section: 0)
        messagesTable.scrollToRow(at: bottomMessageIndex, at: .bottom, animated: true)
    }
    
    @IBAction func prepareForUnwindUserProfileToChat (segue: UIStoryboardSegue) {
        
        
    }
    
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        //print("IBAction prepareForUnwind")
        
        self.shimmerView?.isHidden = true
    
        let thumbImageView = self.thumbImageView!
//        let clearImage = self.loadOtherClearPhotoFromDisk(otherUID: self.otherUID!)
        let clearThumbImage = self.loadedImageToMemory_Other!
        let maskView = self.sliderMaskImageView!
        let sliderButtonView = self.sliderButtonImageView!
        
        //remove slider views
        maskView.removeFromSuperview()
        sliderButtonView.removeFromSuperview()
        
        if let kView = self.keyImageView {
            kView.removeFromSuperview()
        }
        if let wView = self.wImageViewRHS_i {
            wView.removeFromSuperview()
        }
        
        if let p = self.pulsatingLayer {
            p.isHidden = true
        }
        
        if let r = self.ringLayer {
            r.isHidden = true
        }

        //remove transparency view
        //print("prepareForUnwind chatScreenWillAnimateTransparencyToZERO")
        if let tView = self.transparencyView {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
                tView.alpha = 0
            }) { (true) in
                //print("chatScreenWillRmvView")
                tView.removeFromSuperview()
            }
        }

        //unblur thumbnailView
//        UIView.transition(with: thumbImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
//            thumbImageView.image = clearThumbImage
//        }, completion: nil)

        UIView.animate(withDuration: 1, delay: 2, options: .transitionCrossDissolve, animations: {
            if self.selfWasDismissed == true || self.incomingDismissedUIDs.contains(self.otherUID!) {
                //
            } else {
                thumbImageView.image = clearThumbImage
            }
        }, completion: nil)
        
        //remove slider & adjust view frames
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear , animations: {
            //below code is rotating the views
//            maskView.transform = CGAffineTransform.identity.scaledBy(x: -1, y: -1)
//            sliderButtonView.transform = CGAffineTransform.identity.scaledBy(x: -1, y: -1)
//            kView.transform = CGAffineTransform.identity.scaledBy(x: -1, y: -1)
//            wView.transform = CGAffineTransform.identity.scaledBy(x: -1, y: -1)

        }) { (true) in
//            maskView.removeFromSuperview()
//            sliderButtonView.removeFromSuperview()
//            kView.removeFromSuperview()
//            wView.removeFromSuperview()
        }
        
        //animate frames to bottom
        //lineSeparatorView new frames
        let chatSpaceView = self.chatSpaceView!
        let chatSpaceHeight = chatSpaceView.bounds.height
        let chatSpaceWidth = chatSpaceView.bounds.width
        
        let messageFieldBorderInset_Lo = self.messageFieldBorderInset_Lo!
        let chatSpaceHeightWithKeyboard = chatSpaceHeight - (self.viewParameters?.keyBoardHeight)!
        let finalInset = (messageFieldBorderInset_Lo * chatSpaceHeightWithKeyboard) as CGFloat

        //CODE FOR LINE VIEW
        
        let newBottomInsetConstant_Lo = 51 as CGFloat
        
        let l_x = 0 as CGFloat
        let l_y = chatSpaceHeight - newBottomInsetConstant_Lo
        let lineFrame = CGRect(x: l_x, y: l_y, width: chatSpaceWidth, height: 0.5)
       
        //text view frame
        let t_x = self.textFieldView!.frame.minX
        let t_y = chatSpaceHeight - newBottomInsetConstant_Lo
        let tWidth = self.textFieldView!.bounds.width
        let tHeight = self.textFieldView!.bounds.height
        let textViewFrame = CGRect(x: t_x, y: t_y, width: tWidth, height: tHeight)

        //
//        WARNING: DO NOT GET THESE VALUES FROM SELF. THEY WILL ONLY BE AVAILABLE IF KEYBOARDDIDSHOW, !!NOT THE CASE IF USER SEGUES FROM    CLVC!!, SO SET AS CONSTANTS
        let duration = 0.25
        let delay = 0.25

        //compare frames & adjust when chatScreen touches bottom
        let lineSeparatorFrame = self.lineSeparatorView!.frame.maxY
        //print("Y lineSeparatorFrame", lineSeparatorFrame)
        let screenThreshold = (UIScreen.main.bounds.height) * (2 / 3)
        //print("Y screenThreshold", screenThreshold)

        //new table properties
        let tableX = (viewParameters?.insetUnit)!
        let tableY = self.thumbImageView!.bounds.height + (1.5 * (viewParameters?.insetUnit)!)
        let tableWidth = chatSpaceView.bounds.width - (1.2 * (viewParameters?.insetUnit)!)
        let tableHeight = chatSpaceView.bounds.height - tableY - newBottomInsetConstant_Lo
        let tableFrame = CGRect(x: tableX, y: tableY, width: tableWidth, height: tableHeight)
        
        if lineSeparatorFrame > screenThreshold {
            //print("line is below threshold")
//            self.textFieldView!.backgroundColor = UIColor.red
            UIView.animate(withDuration: duration, delay: delay, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
                self.lineSeparatorView!.frame = lineFrame
                self.textFieldView!.frame = textViewFrame
                self.messagesTableView!.frame = tableFrame
            }, completion: nil)
        } else {
            //keyboard is present on screen
            //need to increase tableView height
            //print("WILLRESETTABLEVIEWPROPETIES")
//            self.messagesTableView!.frame = tableFrame
        }
    }
    
    var statusBarShouldHide = false

    override var prefersStatusBarHidden: Bool {
        return statusBarShouldHide
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    @objc func backButtonSegue(_ sender: UITapGestureRecognizer) {
        //print("backButton observed")
        
        self.textFieldView!.resignFirstResponder()
        
        if self.presentingSegueIdentifier == "profileVCtochatScreenVC" {
            statusBarShouldHide = true
            UIView.animate(withDuration: 0.20) {
                self.setNeedsStatusBarAppearanceUpdate()
            }
            
            //print("backButtonSegue selfWasDismissed", self.selfWasDismissed)
            if self.selfWasDismissed != true {
                //print("backButtonSegue A")
                self.textFieldView?.resignFirstResponder()
                self.performSegue(withIdentifier: "backToProfileVC", sender: self)
            } else {
                //print("backButtonSegue B")
                //print("backButtonSegue BpresentingViewController", self.presentingViewController)
                self.textFieldView?.resignFirstResponder()
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }
            
        } else {
            //print("backButtonSegue C")
            self.textFieldView?.resignFirstResponder()
            self.performSegue(withIdentifier: "backtoChatListView", sender: self)
        }
    }
    
    func backButtonSegue_Helper() {
        self.textFieldView!.resignFirstResponder()
        
        if self.presentingSegueIdentifier == "profileVCtochatScreenVC" {
            statusBarShouldHide = true
            UIView.animate(withDuration: 0.20) {
                self.setNeedsStatusBarAppearanceUpdate()
            }
            
            //print("backButtonSegue selfWasDismissed", self.selfWasDismissed)
            if self.selfWasDismissed != true {
                //print("backButtonSegue A")
                self.textFieldView?.resignFirstResponder()
                self.performSegue(withIdentifier: "backToProfileVC", sender: self)
            } else {
                //print("backButtonSegue B")
                //print("backButtonSegue BpresentingViewController", self.presentingViewController)
                self.textFieldView?.resignFirstResponder()
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            }
            
        } else {
            //print("backButtonSegue C")
            self.textFieldView?.resignFirstResponder()
            self.performSegue(withIdentifier: "backtoChatListView", sender: self)
        }
    }
    
    
    var freezeFrame: UIImageView?
    var arrestoImago: UIImage?
    
    //Arresto Momento is called right before Transparency View is added. Check addTransparencyView()
    
    func arrestoMomento() {
        //print("CSVC arrestoMomento")
 
        let arrestoImago = self.takeScreenshot()
        self.arrestoImago = arrestoImago
        
//        let freezeFrame = UIImageView(frame: UIScreen.main.bounds)
//        freezeFrame.image = arrestoImago!
//
//        self.view.addSubview(freezeFrame)
//        self.freezeFrame = freezeFrame
    }
    
    func takeScreenshot() -> UIImage? {
        //print("CSVC willtakeScreenshot")
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        let context = UIGraphicsGetCurrentContext()
        
        layer.render(in:context!)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    func sendRowToTopChatListForUID(withSourceCall: String) {
        //you need to set this for all actions
        //print("sendRowToTopChatListForUID withSourceCall", withSourceCall)
        var userInfoDict = [String : String]()
        userInfoDict["objectUID"] = self.otherUID!
        NotificationCenter.default.post(name: Notification.Name(rawValue: "sendChatListRowToTopForUID"), object: self, userInfo: userInfoDict)
    }
    
    var ref_SelfModelController = SelfModelController()
    
    var didPresentFromChatList = false
    
    
    

    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.sourceVC = ""
        
        if segue.identifier == "toVoilaVC" {
            let voilaVC = segue.destination as! VoilaViewController
        
            //set image in destinationVC
            //othImage should always be loaded WHEN tapToUnlock is active. TaptoUnlock is active only in two scenarios: user just completed swap live, user completed swap live but did not unlock. The marker is whether or not "unlockedArray" contains other.UID. The array is updated when user tapsToUnlock
            
            var imageToPass: UIImage?
            
            voilaVC.presentingSegueIdentifier = self.presentingSegueIdentifier
            voilaVC.selfVisus = self.selfVisus!
            voilaVC.otherVisus = self.otherVisus!
            voilaVC.selfWasDismissed = self.selfWasDismissed
            
            if self.unlockedUsers.contains(self.otherUID!) {
                imageToPass = self.loadedImageToMemory_Other!
            } else {
                imageToPass = self.loadClearPhotoFromDisk_Other()
            }
            
            if let loadedImage = self.loadedImageToMemory_Other {
                imageToPass = loadedImage
            }
            
            voilaVC.userDisplayName = self.otherDisplayName!
            voilaVC.voilaImage = imageToPass!
            
            //pass values
            voilaVC.selfUID = self.selfUID!
            voilaVC.otherUID = self.otherUID!
            
            if self.absExistingMessageThreadID != "" {
                voilaVC.absExistingMessageThreadID = self.absExistingMessageThreadID
            } else if self.temporaryMessageThreadID != "" {
                voilaVC.absExistingMessageThreadID = self.temporaryMessageThreadID
            }
            
            voilaVC.absExistingSwapRequestID = self.absExistingSwapRequestID!
            
            //create screenshot
            self.arrestoMomento()
            voilaVC.arrestoImago = self.arrestoImago!
            
        } else if segue.identifier == "backtoChatListView" {
            //print("CSVC segue backtoChatListView ")
            
            let controller = segue.destination as! ChatListViewController
//            controller.modifyRowForUID = self.otherUID!
            

            //update dictionary values in targetVC
            
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
            self.removeDidReadObserverLastMessage()
 
        } else if segue.identifier == "backToProfileVC" {
            
            //print("CSVC unwinding backToProfileVC")
            
            let controller = segue.destination as! UserProfileViewController

            self.textFieldView!.resignFirstResponder()
            controller.didUnwindFromChatScreen = true

            NotificationCenter.default.post(name: Notification.Name(rawValue: "willUnwindFromCSVC"), object: self)

            //print("CSVC didBlockUser before sending IS,", self.didBlockUser)
            if self.didBlockUser == true {
                //print("CSVC sending didBlockUser,", self.didBlockUser)
                controller.didBlockUser = true
            }
    
            //print("CSVC segue backToProfileVC ")
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
            self.removeDidReadObserverLastMessage()
            
        } else if segue.identifier == "showProfileFromThumbnail" {
            //REQUIRED NOW
            
            //1. need to pass self-refmodel
            let controller = segue.destination as! UserProfileViewController
            controller.ref_SelfModelController = self.ref_SelfModelController

            //set info for drawing chat button
            if self.messagesArray.isEmpty {
                controller.shouldDrawFullChatButton = true
            } else {
                controller.shouldDrawFullChatButton = false
            }
            
            //2. need to present sourceVC identifier
            controller.upvcShouldUnwindToChatScreen = true
//            controller.importedStatusBarHeight = self.importedStatusBarHeight!

            //NEed to pass user info object to populate user profile
            //1. pass info through CVC -> UPVC -> CSVC -> UPVC
            if self.didPresentFromChatList == true {
                controller.selectedUser = self.userInfoDict_fromDB
                controller.shouldUnwindToChatList_whenBlock = true
            } else {
                controller.selectedUser = self.selectedUser
            }
            
            
            
            //2. pass info through CVC ->ChatList-CSVC-UPVC
            
            
            //REQUIRED FOR UPVC
       //OK     //1. segue back to CSVC with back button NOT to CVC
            //2. be able to block user from this nav flow now
        //OK     //3. remove the chat button when presenting UPVC this way - keep only back button (actually keep chat button but let it segue back to CSVC && PERFORM NO CHAT LOGIC)
            
    
            //what happens if self gets blocked or dismissed while checking the user profile? maybe handle later
        }
    }
    
    func removeDidReadObserverLastMessage() {
        var chatThreadID = ""
        
        if self.absExistingMessageThreadID != "" {
            chatThreadID = self.absExistingMessageThreadID
        } else {
            chatThreadID = self.temporaryMessageThreadID
        }
        
        self.ref.child("globalChatThreadMetaData/\(chatThreadID)").removeObserver(withHandle: didReadLastMessage_Handle)
        
        let user = Auth.auth().currentUser!
        
        //remove active chat thread observers
        self.ref.child("users/\(user.uid)/myActiveChatThreads").removeAllObservers()
        self.ref.child("users/\(user.uid)/myActiveChatThreads").removeObserver(withHandle: activeMessageThreadsObserver)
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
        //print("CSVC WILL UNWIND TO SOURCE VIEWCONTROLLER")
    }
    
    deinit {
        //print("WELD_A CSVC DEINIT CALLED")
    }
  
}

extension UITextView {
    func addCharacterSpacing(withValue: CGFloat) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedStringKey.kern, value: withValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}

/*
 //CODE FOR BEZIER PATH
 //        // create path
 //        let path = UIBezierPath()
 //
 //        //line coordinates
 //        let x1 = 0 as CGFloat
 //        let y1 = chatSpaceHeight - messageFieldBorderInset1!
 //
 //        let x2 = x1 + chatSpaceWidth
 //        let y2 = chatSpaceHeight - messageFieldBorderInset1!
 //
 //        let lineStart = CGPoint(x: x1, y: y1)
 //        let lineEnd = CGPoint(x: x2, y: y2)
 //
 //        path.move(to: lineStart)
 //        path.addLine(to: lineEnd)
 //
 //        // Create a `CAShapeLayer` that uses that `UIBezierPath`:
 //        let shapeLayer = CAShapeLayer()
 //        shapeLayer.path = path.cgPath
 //        shapeLayer.strokeColor = UIColor.red.cgColor
 ////        shapeLayer.fillColor = UIColor.clear.cgColor
 //        shapeLayer.lineWidth = 1
 //
 //        // Add that `CAShapeLayer` to your view's layer:
 //        self.linePath = shapeLayer
 //        chatSpaceView.layer .addSublayer(shapeLayer)
 //
 //        //
 }
 
 func addingAnchors() {
 //        nameLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor , constant: 100).isActive = true
 //        self.view.translatesAutoresizingMaskIntoConstraints = false
 //        chatSpaceView.translatesAutoresizingMaskIntoConstraints = false
 //        chatSpaceView.addConstraint(NSLayoutConstraint(item: sLabel, attribute: .firstBaseline , relatedBy: .equal, toItem: nameLabel, attribute: .firstBaseline, multiplier: 1, constant: 0))
 
 //        sLabel.addConstraint(NSLayoutConstraint(item: sLabel, attribute: .firstBaseline , relatedBy: .equal, toItem: nameLabel, attribute: .firstBaseline, multiplier: 1, constant: 11))
 }
 //
 
 */
