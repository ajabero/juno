//
//  ReadyViewController.swift
//  Juno
//
//  Created by Asaad on 2/17/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import CoreLocation

class ReadyViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var importedUserLocation: CLLocation?
    let hyperBlue = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)

    var ref_AuthTracker = AuthTracker()
    
    
    //VIP NOTES: ATTENTION: ATTN: Current Build Submitted to Apple does not delete user account when they reach view.
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addXimoLogo()
//        self.addHelloLabel()
//        self.addForwardArrow()
        self.addStartExploringLabel()
        
        UserDefaults.standard.set("false", forKey: "makePrivateAlert_Seen")
        UserDefaults.standard.set("false", forKey: "makePublicAlert_Seen")
        
        NotificationCenter.default.removeObserver(self.ref_AuthTracker.aRef, name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationController = segue.destination as! CollectionViewController
        destinationController.userLocationFromSetup = self.importedUserLocation
    }
    
    var logoView: UIImageView!
    
    func addXimoLogo() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let widthRatio = 86 / 414 as CGFloat
        let htwRatio = 19.75 / 86 as CGFloat
        
        let width = widthRatio * sWidth
        let height = width * htwRatio
     
        let logoView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        logoView.contentMode = .scaleAspectFill
        logoView.image = UIImage(named: "logoVector")!
        
        let cX = sWidth * 0.5
//        let cY = (4 * superInsetHeight) - gridUnitHeight + sBarH - (0.5 * gridUnitHeight)
        let cY = sHeight * 0.5
        
        logoView.layer.position = CGPoint(x: cX, y: cY)
        
        self.view.addSubview(logoView)
        self.logoView = logoView
        
        let cLine = UIView(frame: CGRect(x: 0, y: sHeight * 0.5, width: sWidth, height: 0.5))
        cLine.backgroundColor = UIColor.blue
//        self.view.addSubview(cLine)
    }
    
    var userName = "There"
    var helloLabel: UITextView!
    
    func addHelloLabel() {
        
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let helloString = "Hello!, " + "\(userName)"
        
        let font = "Avenir-Heavy"
        let fontSize = 18.0 as CGFloat
        
        let tWidth = 112 as CGFloat
        let textFieldCGSize = self.sizeOfCopy(string: helloString, constrainedToWidth: Double(tWidth), fontName: font, fontSize: fontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let tY = sHeight * 0.5
        
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let helloLabel = UITextView(frame: tFrame)
        
        helloLabel.font = UIFont(name: font, size: fontSize)
        self.view.addSubview(helloLabel)
        self.helloLabel = helloLabel
        
        self.helloLabel.adjustsFontForContentSizeCategory = true
        self.helloLabel!.text = helloString
        self.helloLabel!.textAlignment = .center
        self.helloLabel!.isEditable = false
        self.helloLabel!.isScrollEnabled = false
        self.helloLabel!.isSelectable = false
        self.helloLabel!.textContainer.lineFragmentPadding = 0
        self.helloLabel!.textContainerInset = .zero
        self.helloLabel!.textColor = UIColor.black
        self.helloLabel!.backgroundColor = UIColor.clear
    }
    
    var forwardArrowView: UIImageView?
    
    func addForwardArrow() {
        //
        let fW = (47 / 414) * UIScreen.main.bounds.width
        let fH = fW as CGFloat
        let fX = UIScreen.main.bounds.width - ((15 / 414) * UIScreen.main.bounds.width) - fW
        let fY = 0 as CGFloat
        
        let fF = CGRect(x: fX, y: fY, width: fW, height: fH)
        let fView = UIImageView(frame: fF)
        fView.image = UIImage(named: "readyArrow")!
        fView.contentMode = .scaleAspectFit
        
        self.view.addSubview(fView)
        self.forwardArrowView = fView
        
        let cX = self.forwardArrowView!.frame.midX
        let cY = self.logoView.frame.midY
        let centerPoint = CGPoint(x: cX, y: cY)
        
        self.forwardArrowView!.layer.position = centerPoint
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ReadyViewController.forwardButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.forwardArrowView!.addGestureRecognizer(tapGesture)
        self.forwardArrowView!.isUserInteractionEnabled = true
    }
    
    @objc func forwardButton(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "startGrid", sender: self)
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    var exploreLabel: UITextView?
    
    func addStartExploringLabel() {
        
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let helloString = "Start Exploring"
        
        let font = "Avenir-Medium"
        let fontSize = 18.0 as CGFloat
        
        let tWidth = 152 as CGFloat
        let textFieldCGSize = self.sizeOfCopy(string: helloString, constrainedToWidth: Double(tWidth), fontName: font, fontSize: fontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let tY = sHeight - (0.5 * superInsetHeight) - (0.5 * tH)
        
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let exploreLabel = UITextView(frame: tFrame)
        
        exploreLabel.font = UIFont(name: font, size: fontSize)
        self.view.addSubview(exploreLabel)
        self.exploreLabel = exploreLabel
        
        self.exploreLabel!.adjustsFontForContentSizeCategory = true
        self.exploreLabel!.text = helloString
        self.exploreLabel!.textAlignment = .center
        self.exploreLabel!.isEditable = false
        self.exploreLabel!.isScrollEnabled = false
        self.exploreLabel!.isSelectable = false
        self.exploreLabel!.textContainer.lineFragmentPadding = 0
        self.exploreLabel!.textContainerInset = .zero
        self.exploreLabel!.textColor = hyperBlue
        self.exploreLabel!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ReadyViewController.forwardButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.exploreLabel!.addGestureRecognizer(tapGesture)
        self.exploreLabel!.isUserInteractionEnabled = true
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
}
