//
//  SettingsViewController.swift
//  Juno
//
//  Created by Asaad on 5/24/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SettingsViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    var myProfileInfoDict = [String:String]()
    var profileChangesUIDsInProximity = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        self.view.backgroundColor = self.tint
        
        // Do any additional setup after loading the view.
        self.addNavSpaceLayer()
        self.addScreenTitle()
        self.addNavBarButtons()
//        self.addScrollView()
        
        self.addSettingsInfo()
        
        self.addShowMeAgeCell()
        self.addShowMeAgeCellValue()
        self.populateMinAgeArray()
        self.populateMaxAgeArray()
        self.addAgePicker()
        
        //add age
        self.addShowToAgeCell()
        self.addRadiusSlider()

        self.addDeleteCell()
        self.addAlert()
        self.addActivityIndicator()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Colors
    var navBarColor = UIColor(displayP3Red: 246/255, green: 246/255, blue: 248/255, alpha: 1)
    var hyperBlue = UIColor(displayP3Red: 0, green: 108/255, blue: 255/255, alpha: 1)
    
    var navSpaceLayer: UIView?
    var navEdge: UIView?
    
    var mainScreenNameLabel: UITextView?
    var titleLabel: UILabel?
    
    var scrollView: UIScrollView!
//    let tint = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
    let tint = UIColor(displayP3Red: 240/255, green: 239/255, blue: 245/255, alpha: 1)

    func addScrollView() {
//        let finalScrollViewHeight = UIScreen.main.bounds.height - self.navSpaceLayer!.frame.height
        let finalScrollViewHeight = UIScreen.main.bounds.height

        let scrollFrame = CGRect(x: 0, y: self.navSpaceLayer!.frame.maxY, width: UIScreen.main.bounds.width, height: finalScrollViewHeight)
        
        //initialize scrollView
        let scrollView = UIScrollView(frame: scrollFrame)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: finalScrollViewHeight)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.backgroundColor = self.tint
        
        //add scrollView as Subview
        self.view.addSubview(scrollView)
        self.scrollView = scrollView
        self.scrollView!.delegate = self
    }
    
    func addNavSpaceLayer() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let navX = 0 as CGFloat
        let navY = 0 as CGFloat
        let navW = sWidth
        let navH = navSpaceHeight
        
        let navF = CGRect(x: navX, y: navY, width: navW, height: navH)
        let navView = UIView(frame: navF)
        navView.backgroundColor = self.navBarColor
        self.navSpaceLayer = navView
        
        self.view!.addSubview(navView)
        
        //& Line View
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.navSpaceLayer!.frame.maxY
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.view!.addSubview(lineView)
        self.navEdge = lineView
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    var cancelButton: UIButton?
    var cancelGray = UIColor(displayP3Red: 190/255, green: 190/255, blue: 190/255, alpha: 1)

    func addScreenTitle() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 3 * gridUnitWidth
        let lH = 35 as CGFloat
        let lX = (sWidth - lW) * 0.5
        let titleInsetFromBottom = ((self.navSpaceLayer!.bounds.height - statusBarHeight) * 0.5) - (lH * 0.5)
        let lY = statusBarHeight + titleInsetFromBottom
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let titleLabel = UILabel(frame: tF)
        
        //init
        self.navSpaceLayer!.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.titleLabel!.text = "Settings"
        self.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 20)!
        self.titleLabel!.textColor = UIColor.black
        self.titleLabel!.textAlignment = .center
    }
    
    var layerTintColor = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
    var doneButton: UIButton?
    
    func addNavBarButtons() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
        
        let dW = 47 as CGFloat
        let dH = 35 as CGFloat
        let dX = UIScreen.main.bounds.width - dW - marginInset
        let titleInsetFromBottom = ((self.navSpaceLayer!.bounds.height - statusBarHeight) * 0.5) - (dH * 0.5)
        let dY = statusBarHeight + titleInsetFromBottom
        
        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
        let doneButton = UIButton(frame: dF)
        
        self.navSpaceLayer!.addSubview(doneButton)
        self.doneButton = doneButton
        
        self.doneButton!.setTitle("Done", for: .normal)
        self.doneButton!.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 18.0)!
        self.doneButton!.setTitleColor(self.hyperBlue, for: .normal)
        self.doneButton!.addTarget(self, action: #selector(SettingsViewController.done(_:)), for: .touchUpInside)
        self.doneButton!.isUserInteractionEnabled = true
    }
    
    var userRadius_Initial: Float?
    var userRadius_Final: Float?
    
    @objc func done(_ sender: UIButton) {
        //print("done")
        //print("D! ", self.minAge_initial, self.minAge_final)
        //print("D!!", self.maxAge_initial, self.maxAge_final)
        
        if userRadius_Initial != userRadius_Final {
            self.ref_SelfModelController.cvcShouldReloadView = true
        } else {
            //
        }
        
        if self.minAge_initial != self.minAge_final {
            self.ref_SelfModelController.cvcShouldReloadView = true
            
        } else {
            
        }
        
        if self.maxAge_initial != self.maxAge_final {
            self.ref_SelfModelController.cvcShouldReloadView = true
            
        } else {
            
        }
        
        self.performSegue(withIdentifier: "unwindSettingsToHome", sender: self)
        self.updateAgeFilter()
    }
    
    func updateAgeFilter() {
        
        //print("updateAgeFilter")
        let minAgeSelected = self.minAgeSelected
        let maxAgeSelected = self.maxAgeSelected
        
        var showMeMinAge = ""
        var showMeMaxAge = ""
        
        if minAgeSelected == "Minimum" && maxAgeSelected == "Maximum" {
            //print("Case1: NO AGE FILTER SELECTED")
            showMeMinAge = ""
            showMeMaxAge = ""
        }
        
        if minAgeSelected == "60+" && maxAgeSelected == "Maximum" {
            //print("Case2: All above 60")
            showMeMinAge = "60"
            showMeMaxAge = ""
        }
        
        if minAgeSelected == "Minimum" && maxAgeSelected == "60+" {
            //print("Case3: NO AGE FILTER SELECTED")
            showMeMinAge = ""
            showMeMaxAge = ""
        }
        
        if minAgeSelected != "Minimum" && maxAgeSelected != "Maximum" && minAgeSelected != "60+" {
            //print("Case4: RANGE AGE FILTER SELECTED")

            if maxAgeSelected == "60+" {
                showMeMinAge = minAgeSelected
                showMeMaxAge = ""
            } else {
                showMeMinAge = minAgeSelected
                showMeMaxAge = maxAgeSelected
            }
        }
        
        if maxAgeSelected == "Maximum" && minAgeSelected != "Minimum" {
            //print("Case5: RANGE AGE FILTER SELECTED")
            if minAgeSelected == "60+" {
                showMeMinAge = "60"
                showMeMaxAge = ""
            } else {
                showMeMinAge = minAgeSelected
                showMeMaxAge = ""
            }
        }
        
        if minAgeSelected == "Minimum" && maxAgeSelected != "Maximum" && maxAgeSelected != "60+" && minAgeSelected != "60+" {
            showMeMinAge = ""
            showMeMaxAge = maxAgeSelected
        }
        
        //print("FINAL AGE FILTER IS: minAge,", showMeMinAge, "maxAge", showMeMaxAge)
        
        let selfUID = Auth.auth().currentUser!.uid
        
        self.ref.child("users/\(selfUID)/showMeMinAge").setValue(showMeMinAge)
        self.ref.child("users/\(selfUID)/showMeMaxAge").setValue(showMeMaxAge)
        
        UserDefaults.standard.set(showMeMinAge, forKey: "\(selfUID)_showMeMinAge")
        UserDefaults.standard.set(showMeMaxAge, forKey: "\(selfUID)_showMeMaxAge")
        if let showMeAgeString = self.finalAgeRangeString {
            //print("SetAgeFilterwillset self.finalAgeRangeString", self.finalAgeRangeString!)
            UserDefaults.standard.set(showMeAgeString, forKey: "\(selfUID)_showMeAgeString")
        }
    }
    
    var showMeAgeLabel: UILabel?
    
    var showMeAgeCellView: UIView!
    var showMeAgeCellTitle: UITextView!
    var showMeAgeCellValue: UITextView!
    
    func addShowMeAgeCell() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let cellHeight = (53 / 414) * sWidth
        let cellWidth = sWidth
        let cellX = 0 as CGFloat
        let cellY = self.navEdge!.frame.maxY + (2 * gridUnitHeight)
        
        let cellFrame = CGRect(x: cellX, y: cellY, width: cellWidth, height: cellHeight)
        let showMeAgeCellView = UIView(frame: cellFrame)
        showMeAgeCellView.backgroundColor = UIColor.white
        self.showMeAgeCellView = showMeAgeCellView
        self.view.addSubview(showMeAgeCellView)
        
        //
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        let fontName = "AvenirNext-Regular"
        
        //add title
        // Font Size
        let tFontSize = 16 as CGFloat
        let nW = 6 * gridUnitWidth
        
        //get height
        var nH = 0 as CGFloat
        let titleString = "Show Me Ages"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: fontName, fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
        
        let nX = marginInset
        let nY = (cellHeight * 0.5) - (0.5 * tH)
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let showMeAgeCellTitle = UITextView(frame: f)
        
        //
        showMeAgeCellTitle.text = titleString
        self.showMeAgeCellView.addSubview(showMeAgeCellTitle)
        self.showMeAgeCellTitle = showMeAgeCellTitle
        
        self.showMeAgeCellTitle!.font = UIFont(name: fontName, size: tFontSize)!
        self.showMeAgeCellTitle!.textAlignment = .left
        self.showMeAgeCellTitle!.isEditable = false
        self.showMeAgeCellTitle!.isScrollEnabled = false
        self.showMeAgeCellTitle!.isSelectable = false
        self.showMeAgeCellTitle!.textContainer.lineFragmentPadding = 0
        self.showMeAgeCellTitle!.textContainerInset = .zero
        self.showMeAgeCellTitle!.textColor = UIColor.black
        self.showMeAgeCellTitle!.backgroundColor = UIColor.white
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.filterShowMeAge(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.showMeAgeCellView!.addGestureRecognizer(tapGesture)
        self.showMeAgeCellView!.isUserInteractionEnabled = true

    }
    
    var sliderSpaceView: UIView?
    var radiusSlider: UISlider?
    var radiusValueSelected: Float?
    var distanceTitle: UITextView?
    var distanceValueLabel: UITextView?
    
    func addRadiusSlider() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        let fontName = "AvenirNext-Regular"
        
        //
        let spaceW = sWidth
        let spaceH = 4 * gridUnitHeight
        let spaceX = 0 as CGFloat
//        let spaceY = sHeight * 0.5
//        let spaceY = self.showMeAgeCellView!.frame.maxY
        let spaceY = self.showMeAgeCellView!.frame.maxY + (1.5 * gridUnitHeight)

        //
        let sF = CGRect(x: spaceX, y: spaceY, width: spaceW, height: spaceH)
        let spaceView = UIView(frame: sF)
        spaceView.backgroundColor = UIColor.white
        
        self.view.addSubview(spaceView)
        self.sliderSpaceView = spaceView
        
        //
        let slider = UISlider()
        var sliderFrame = slider.frame
        
        let sliderWidth = sWidth - (2 * marginInset)
        let sliderHeight = sliderFrame.height
        let sliderX = marginInset
//        let sliderY = (spaceH - sliderHeight) * 0.5
        let sliderY = ((2/3) * spaceH) - (0.5 * sliderHeight)

        sliderFrame = CGRect(x: sliderX, y: sliderY, width: sliderWidth, height: sliderHeight)
        slider.frame = sliderFrame
    
        slider.minimumValue = 10
        slider.maximumValue = 200
        
        //
        slider.addTarget(self, action: #selector(SettingsViewController.radiusDidChange(_:)), for: .valueChanged)
        slider.isContinuous = true
        
        self.sliderSpaceView!.addSubview(slider)
        self.radiusSlider = slider
        
        //ADD SLIDER TITLE
        //add title
        // Font Size
        let tFontSize = 16 as CGFloat
        let nW = 6 * gridUnitWidth
        
        //get height
        var nH = 0 as CGFloat
        let titleString = "Distance"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: fontName, fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
            nH = tH
        
        let nX = marginInset
        let nY = (0.25 * spaceH) - (0.5 * nH)
    
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let distanceTitle = UITextView(frame: f)
        
        //
        distanceTitle.text = titleString
        self.sliderSpaceView!.addSubview(distanceTitle)
        self.distanceTitle = distanceTitle
        
        self.distanceTitle!.font = UIFont(name: fontName, size: tFontSize)!
        self.distanceTitle!.textAlignment = .left
        self.distanceTitle!.isEditable = false
        self.distanceTitle!.isScrollEnabled = false
        self.distanceTitle!.isSelectable = false
        self.distanceTitle!.textContainer.lineFragmentPadding = 0
        self.distanceTitle!.textContainerInset = .zero
        self.distanceTitle!.textColor = UIColor.black
        self.distanceTitle!.backgroundColor = UIColor.white
        
        //add distance value
        let x = sWidth - marginInset - nW
        let y = nY
        
        let f2 = CGRect(x: x, y: y, width: nW, height: nH)
        let distanceValueLabel = UITextView(frame: f2)
        
        //
        distanceValueLabel.text = "200+ mi."
        self.sliderSpaceView!.addSubview(distanceValueLabel)
        self.distanceValueLabel = distanceValueLabel
        
        self.distanceValueLabel!.font = UIFont(name: fontName, size: tFontSize)!
        self.distanceValueLabel!.textAlignment = .right
        self.distanceValueLabel!.isEditable = false
        self.distanceValueLabel!.isScrollEnabled = false
        self.distanceValueLabel!.isSelectable = false
        self.distanceValueLabel!.textContainer.lineFragmentPadding = 0
        self.distanceValueLabel!.textContainerInset = .zero
        self.distanceValueLabel!.textColor = self.cellLabelColor
        self.distanceValueLabel!.backgroundColor = UIColor.clear
        
        //& Line View
        //add Line Border
//        let lineW = (385 / 414) * sWidth
//        let lineH = 0.2 as CGFloat
//        let lineX = sWidth - lineW
//        let lineY = self.showMeAgeCellView!.frame.maxY
//        
//        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
//        let lineView = UIView(frame: lineFrame)
//
//        let lineColor = UIColor(displayP3Red: 180/250, green: 180/250, blue: 180/250, alpha: 180/250)
//        lineView.backgroundColor = lineColor
//
//        self.view.addSubview(lineView)
    }
    
    @objc func radiusDidChange(_ sender: UISlider) {
        //print("radiusDidChange")
        
        let selfUID = Auth.auth().currentUser!.uid
        
        let newVal = ceil(self.radiusSlider!.value)
//        UserDefaults.standard.set(newVal, forKey: "\(selfUID)_userRadius")
        UserDefaults.standard.set(newVal, forKey: "\(selfUID)_userRadius")
        
        //Note: Setting to max "200" value shows user "200+" -> will effectively trigger radius >200
        
        //print("newradval,", newVal)
        self.radiusValueSelected = newVal
        self.userRadius_Final = newVal
        var ceilString = String(newVal)
        if ceilString.contains(".0") {
            ceilString.removeLast()
            ceilString.removeLast()

            if ceilString != "200" {
                self.distanceValueLabel!.text = ceilString + " mi."

            } else {
                //print("~~~")
                self.distanceValueLabel!.text = "200+ mi."
            }
        }
      
    }
    
    func addShowMeAgeCellValue() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        let fontName = "AvenirNext-Regular"
        
        //add title
        // Font Size
        let tFontSize = 16 as CGFloat
        let nW = 3 * gridUnitWidth
        var nH = 0 as CGFloat
        let titleString = "Hey"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: fontName, fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
//        let nX = 6 * gridUnitWidth
        let nX = sWidth - marginInset - nW

        let cellHeight = self.showMeAgeCellView!.frame.height
        let nY = (cellHeight * 0.5) - (0.5 * tH)
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let showMeAgeCellValue = UITextView(frame: f)
        
        //
        showMeAgeCellValue.text = "Any"
        self.showMeAgeCellView.addSubview(showMeAgeCellValue)
        self.showMeAgeCellValue = showMeAgeCellValue
        
        self.showMeAgeCellValue!.font = UIFont(name: fontName, size: tFontSize)!
        self.showMeAgeCellValue!.textAlignment = .right
        self.showMeAgeCellValue!.isEditable = false
        self.showMeAgeCellValue!.isScrollEnabled = false
        self.showMeAgeCellValue!.isSelectable = false
        self.showMeAgeCellValue!.textContainer.lineFragmentPadding = 0
        self.showMeAgeCellValue!.textContainerInset = .zero
        self.showMeAgeCellValue!.textColor = self.cellLabelColor
        self.showMeAgeCellValue!.backgroundColor = UIColor.clear
    }
    
    @objc func filterShowMeAge(_ sender: UITapGestureRecognizer) {
        //print("filterShowMeAge")
        self.animateAgePicker(present: true)
    }
    
    func addShowToAgeCell() {
        
    }
    
    var statusBarHeight: CGFloat?
    

    override func viewWillAppear(_ animated: Bool) {
        
        self.ref = Database.database().reference()
        self.setAgePickerCurrentPreferences()
        self.statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        let selfUID = Auth.auth().currentUser!.uid
        
        let userRadius = UserDefaults.standard.float(forKey: "\(selfUID)_userRadius")
        
        if userRadius != 0.0 {
            //print("USERDEFAULTS RADIUS,", userRadius)
            self.radiusSlider!.setValue(userRadius, animated: false)
            if userRadius != 200.0 {
                var ceilString = String(ceil(userRadius))
                ceilString.removeLast()
                ceilString.removeLast()
                self.radiusSlider?.value = userRadius
                self.distanceValueLabel?.text = ceilString + " mi."
                self.userRadius_Initial = userRadius
                self.userRadius_Final = userRadius
            } else {
                self.radiusSlider?.value = 200.0
                self.distanceValueLabel?.text = "200+ mi."
                
                //
                self.userRadius_Initial = 200.0
                self.userRadius_Final = 200.0
            }
            
        } else {
            self.radiusSlider?.value = 200.0
            self.distanceValueLabel?.text = "200+ mi."
            
            //
            self.userRadius_Initial = 200.0
            self.userRadius_Final = 200.0
        }

        //set autoAgeUI
        if let autoAgeEnabled = UserDefaults.standard.object(forKey: "\(selfUID)_didSelectShowMeAutoAge") as? Bool {
            if autoAgeEnabled == true {
                //reverse logic
                self.autoAgeIsEnabled = false
                self.switchAutoButtonUIStateonClick()
                
            } else {
                //reverse logic
                self.autoAgeIsEnabled = true
                self.switchAutoButtonUIStateonClick()
            }
        } else {
            //reverse logic
            self.autoAgeIsEnabled = false
        }
        
        self.getAutoAgeBoundStrings()
        
    }
    
    func addAutoRectMaskLayer() {
        let aRect = self.autoRectFrame!.frame
        
        let frame = CGRect(x: 0, y: 0, width: aRect.width, height: aRect.height)
        let cornerRadiusWidth = (1 / 8) * (aRect.width)
        let cornerRadiusHeight = (1 / 8) * (aRect.width)

        let path = UIBezierPath(roundedRect: frame, byRoundingCorners: [.topLeft, .topRight, .bottomLeft, .bottomRight], cornerRadii: CGSize(width: cornerRadiusWidth, height: cornerRadiusHeight))
        let maskLayer = CAShapeLayer()
        
        maskLayer.frame = frame
        maskLayer.path = path.cgPath
        self.autoRectFrame?.layer.mask = maskLayer
    }
    
    //MARK: Add Age Picker
    func populateMinAgeArray() {
        self.minAgeArray.append("Minimum")
        for age in 18...60 {
            var ageString = String(age)
            if age == 60 {
                ageString = "60+"
            }
            self.minAgeArray.append(ageString)
            //print("self.minAgeArray is now,", self.minAgeArray)
        }
    }
    
    func populateMaxAgeArray() {
        self.maxAgeArray.append("Maximum")
        for age in 18...60 {
            var ageString = String(age)
            if age == 60 {
                ageString = "60+"
            }
            self.maxAgeArray.append(ageString)
            //print("self.maxAgeArray is now,", self.maxAgeArray)
        }
    }
    
    //
    var minAge_initial = "Minimum"
    var minAge_final = "Minimum"

    var maxAge_initial = "Maximum"
    var maxAge_final = "Maximum"
    
    func setAgePickerCurrentPreferences() {
        let selfUID = Auth.auth().currentUser!.uid
        
        if let userDefaultMinAgeInt = UserDefaults.standard.value(forKey: "\(selfUID)_minAgeIntDefault") as? Int {
            self.select_AgeIntMin = userDefaultMinAgeInt
        }
        
        if let userDefaultMaxAgeInt = UserDefaults.standard.value(forKey: "\(selfUID)_maxAgeIntDefault") as? Int {
            self.select_AgeIntMax = userDefaultMaxAgeInt
        }
        
        if let minAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMinAge") {
            //print("SVC setAgeFilterTitles _showMeMinAge is,", minAge)
            if minAge != "" {
                if minAge != "60" {
                    let rowIndex = self.minAgeArray.index(of: minAge)
                    self.agePicker!.selectRow(rowIndex!, inComponent: 0, animated: true)
                    self.minAgeSelected = minAge
                } else {
                    let rowIndex = self.minAgeArray.index(of: "60+")
                    self.agePicker!.selectRow(rowIndex!, inComponent: 0, animated: true)
                    self.minAgeSelected = minAge
                }
                
                //
                self.minAge_initial = minAge
                self.minAge_final = minAge
            } else {
                //
                //print(" SVC setAgeFilterTitles willSET empty")
                let rowIndex = self.minAgeArray.index(of: "Minimum")
                self.agePicker!.selectRow(rowIndex!, inComponent: 0, animated: true)
            }
            
        
            
        } else {
            
        }
        
        if let maxAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMaxAge") {
            //print("SVC setAgeFilterTitles _showMeMaxAge is,", maxAge)
            if maxAge != "" {
                let rowIndex = self.maxAgeArray.index(of: maxAge)
                self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                self.maxAgeSelected = maxAge
                
                //
                self.maxAge_initial = maxAge
                self.maxAge_final = maxAge
            } else {
                let rowIndex = self.maxAgeArray.index(of: "Maximum")
                self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                self.maxAgeSelected = "Maximum"
            }
            
        } else {
            //print(" SVC setAgeFilterTitles willSET empty")
            let rowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
        }
        
        if let ageFilterLabel = UserDefaults.standard.string(forKey: "\(selfUID)_showMeAgeString") {
            //print("SVC detected ageFilterlabel", ageFilterLabel)
            self.showMeAgeCellValue!.text = ageFilterLabel
        }
        
//        if let isAutoAge = UserDefaults.standard.string(forKey: "\(selfUID)_didSelectShowMeAutoAge") {
//            if isAutoAge == "true" {
//                self.didSelectAutoAge = true
//                self.switchAutoAgeButtonOnTap()
//            } else {
//                self.didSelectAutoAge = false
//                self.switchAutoAgeButtonOnTap()
//            }
//        }
    }
    
    var minAgeArray = [String]()
    var maxAgeArray = [String]()
    var agePicker: UIPickerView?
    var barView: UIView?
    var purpleShade = UIColor(displayP3Red: 128/255, green: 0, blue: 1, alpha: 1)
//    var barGray = UIColor(displayP3Red: 228/255, green: 227/255, blue: 229/255, alpha: 1)
//    let cellLabelColor = UIColor(displayP3Red: 70/255, green: 70/255, blue: 70/255, alpha: 1)
    let cellLabelColor = UIColor(displayP3Red: 125/255, green: 125/255, blue: 125/255, alpha: 1)
    var barGray = UIColor(displayP3Red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
    let lightTitleColor = UIColor(displayP3Red: 190/255, green: 190/255, blue: 190/255, alpha: 1)
    
    var autoShowMeAgeButton: UIButton?
    var autoRectFrame: UIView?
    
    func addAgePicker() {
        //        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        let agePicker = UIPickerView()
        agePicker.delegate = self
        agePicker.dataSource = self
        agePicker.backgroundColor = UIColor.white
        //        agePicker.addTarget(self, action: #selector(MyInfoViewController.heightChanged(_:)), for: .valueChanged)
        
        let pH = agePicker.frame.height
        //        let pY = UIScreen.main.bounds.height - pH
        let barH = 2 * gridUnitHeight
        let pY = sHeight + barH
        
        let frame = CGRect(x: 0, y: pY, width: UIScreen.main.bounds.width, height: pH)
        agePicker.frame = frame
        
        //ADD TOOLBAR VIEW
        let barW = UIScreen.main.bounds.width
        //        let barH = 2 * gridUnitHeight
        let barX = 0 as CGFloat
        //        let barY = pY - barH
        let barY = sHeight
        
        let barF = CGRect(x: barX, y: barY, width: barW, height: barH)
        let barView = UIView(frame: barF)
        barView.backgroundColor = self.barGray
        
        self.view.addSubview(barView)
        self.barView = barView
        
        //ADD TOOLBAR DONE BUTTON
        //
        let bW = 45 as CGFloat
//        let bH = 20 as CGFloat
        let bH = barH

        let bX = UIScreen.main.bounds.maxX - (marginInset * 0.5) - bW
//        let bY = (barH - bH) * 0.5
        let bY = 0 as CGFloat

        //
        let fB = CGRect(x: bX, y: bY, width: bW, height: bH)
        let doneB = UIButton(frame: fB)
        
        doneB.setTitle("Done", for: .normal)
        doneB.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 16)!
        doneB.setTitleColor(cellLabelColor, for: .normal)
        doneB.setTitleColor(UIColor.lightGray, for: .selected)
        doneB.addTarget(self, action: #selector(SettingsViewController.doneShowMeAgeButton(_:)), for: .touchUpInside)
        doneB.isUserInteractionEnabled = true
        
        self.barView?.addSubview(doneB)
        self.view.addSubview(agePicker)
        self.agePicker = agePicker
        
        //
        let autoInset = (19 / 414) * sWidth
        let autoShowMeAgeButtonF = CGRect(x: autoInset, y: bY, width: bW, height: bH)
        let autoShowMeAgeButton = UIButton(frame: autoShowMeAgeButtonF)
        
        autoShowMeAgeButton.setTitle("Auto", for: .normal)
//        autoShowMeAgeButton.titleLabel!.font = UIFont(name: "Avenir-Light", size: 16)!
        autoShowMeAgeButton.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 16)!
        autoShowMeAgeButton.setTitleColor(lightTitleColor, for: .normal)
//        autoShowMeAgeButton.setTitleColor(UIColor.darkGray, for: .selected)
//        autoShowMeAgeButton.setTitleColor(UIColor.white, for: .selected)
        autoShowMeAgeButton.addTarget(self, action: #selector(SettingsViewController.autoShowMeAge(_:)), for: .touchUpInside)
        autoShowMeAgeButton.isUserInteractionEnabled = true

        self.barView?.addSubview(autoShowMeAgeButton)
        self.autoShowMeAgeButton = autoShowMeAgeButton
    
        //add auto rect
        let autoRectFrame = CGRect(x: 0, y: 0, width: 51, height: 26)
        let autoRect = UIView(frame: autoRectFrame)
//        autoRect.layer.cornerRadius = 4
//        autoRect.layer.masksToBounds = true
        autoRect.backgroundColor = UIColor.black
        self.autoRectFrame = autoRect
        
        let fX = self.autoShowMeAgeButton!.center.x
        let fY = self.autoShowMeAgeButton!.center.y
        
        let cPos = CGPoint(x: fX - 0.2, y: fY + 0.5)
        autoRect.layer.position = cPos
        autoRect.alpha = 0
        
        self.barView?.addSubview(autoShowMeAgeButton)
        self.barView?.addSubview(autoRect)
        
        //LAST
        self.barView!.bringSubview(toFront: self.autoShowMeAgeButton!)
        self.view.addSubview(agePicker)
        self.agePicker = agePicker
    }
        
    override func viewDidLayoutSubviews() {
//        if #available(iOS 11.0, *) {
//            autoRectFrame!.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
//        } else {
//            // Fallback on earlier versions
//        }
        autoRectFrame!.layer.masksToBounds = true
//        autoRectFrame!.clipsToBounds = true
        
        self.addAutoRectMaskLayer()
    }
    
//    @objc func heightChanged(_ sender: UIPickerView) {
//        //print("heightChanged")
//        //        self.heightTextField?.text =
//    }

//    var didSelectAutoAge = false
//
//    func switchAutoAgeButtonOnTap() {
//        let selfUID = Auth.auth().currentUser!.uid
//        if self.didSelectAutoAge == false {
//            self.didSelectAutoAge = true
//            self.autoRectFrame?.alpha = 0
//            self.autoShowMeAgeButton?.setTitleColor(lightTitleColor, for: .normal)
//            UserDefaults.standard.set("true", forKey: "\(selfUID)_didSelectShowMeAutoAge")
//        } else {
//            self.didSelectAutoAge = false
//            self.autoRectFrame?.alpha = 1
//            self.autoShowMeAgeButton?.setTitleColor(UIColor.white, for: .normal)
//            UserDefaults.standard.set("false", forKey: "\(selfUID)_didSelectShowMeAutoAge")
//        }
//    }

    let hapticFeedback = UISelectionFeedbackGenerator()
    
    func autoAgeClick() {
        self.hapticFeedback.selectionChanged()
    }
    
    var autoAgeIsEnabled = true
    
    func switchAutoButtonUIStateonClick() {
        let selfUID = Auth.auth().currentUser!.uid

        //Cool animations:
        //- //try to fade light as if light is fading away when click
        // - measure duration of press on button & set duration of fade = duration of press - will animate according to pressure
        
//        guard let defaultMinAge = self.autoAgeIntMin else { return }
//        guard let defaultMaxAge = self.autoAgeIntMax else { return }
//        guard let selectMinAge = self.select_AgeIntMin else { return }
//        guard let selectMaxAge = self.select_AgeIntMax else { return }
        
        if let defaultMinAge = self.autoAgeIntMin {
            if let defaultMaxAge = self.autoAgeIntMax {
                if let selectMinAge = self.select_AgeIntMin {
                    if let selectMaxAge = self.select_AgeIntMax {
                        if defaultMinAge == selectMinAge && defaultMaxAge == selectMaxAge {
                            return
                        }
                    }
                }
            }
        }
 
        if self.autoAgeIsEnabled == true {
            UserDefaults.standard.set(false, forKey: "\(selfUID)_didSelectShowMeAutoAge")

            UIView.animate(withDuration: 0.08, delay: 0, options: .transitionCrossDissolve, animations: {
                self.autoRectFrame?.alpha = 0
            }) { (true) in

            }
            
            self.autoShowMeAgeButton?.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 16)!
            self.autoShowMeAgeButton?.setTitleColor(self.lightTitleColor, for: .normal)

            self.autoAgeIsEnabled = false
        } else if self.autoAgeIsEnabled == false {
            
            UserDefaults.standard.set(true, forKey: "\(selfUID)_didSelectShowMeAutoAge")

            UIView.animate(withDuration: 0.08, delay: 0, options: .transitionCrossDissolve, animations: {
                self.autoRectFrame?.alpha = 1
            }) { (true) in
                
            }
            
            self.autoShowMeAgeButton?.titleLabel!.font = UIFont(name: "Avenir-Light", size: 16)!
            self.autoShowMeAgeButton?.setTitleColor(UIColor.white, for: .normal)

            self.autoAgeIsEnabled = true
        }
    }
    
    var currentAutoAge_Min = ""
    var currentAutoAge_Max = ""

    @objc func autoShowMeAge(_ sender: UIButton) {
        //print("autoShowMeAge", autoShowMeAge)
        
        self.autoAgeClick()
        self.switchAutoButtonUIStateonClick()
        
        let myAge = self.myProfileInfoDict["userAge"]!

        let intMyAge = (myAge as NSString).integerValue

        var autoMinAgeInt = 18
        var autoMaxAgeInt = 100
        
        let minAgeSelectable = 18
        let maxAgeSelectable = 100
        
        let plusUpperRange = 4
        let minusLowerRange = 4
        
        let maxAgeBound = [56, 57, 58, 59, 60]
        
        let lowerAgeDifference = intMyAge - minAgeSelectable
        
        if !maxAgeBound.contains(intMyAge) {
            if lowerAgeDifference < 4 {
                autoMinAgeInt = intMyAge - lowerAgeDifference
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
                
            } else {
                autoMinAgeInt = intMyAge - minusLowerRange
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
            }

            let stringMinAge = String(autoMinAgeInt)
            let minRowIndex = self.minAgeArray.index(of: stringMinAge)
            self.agePicker!.selectRow(minRowIndex!, inComponent: 0, animated: true)
            self.minAgeSelected = stringMinAge
            
            let stringMaxAge = String(autoMaxAgeInt)
            let maxRowIndex = self.maxAgeArray.index(of: stringMaxAge)
            self.agePicker!.selectRow(maxRowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = stringMaxAge
            
            //print("autoMaxAgeInt is,", autoMaxAgeInt)

            
            self.setAgeFilterTitle()

        } else {
            //print("user is 56 or older")
    
            autoMinAgeInt = intMyAge - minusLowerRange
            //print("autoMinAgeInt is,", autoMinAgeInt)
            autoMaxAgeInt = intMyAge + plusUpperRange
            //print("autoMaxAgeInt is,", autoMinAgeInt)

            let stringMinAge = String(autoMinAgeInt)
            let minRowIndex = self.minAgeArray.index(of: stringMinAge)
            self.agePicker!.selectRow(minRowIndex!, inComponent: 0, animated: true)
            self.minAgeSelected = stringMinAge
        
            //
            let maxRowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(maxRowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            self.setAgeFilterTitle()
        }
        
        //
        self.currentAutoAge_Min = self.minAgeSelected
        self.currentAutoAge_Max = self.maxAgeSelected

        //
        self.minAge_final = self.minAgeSelected
        self.maxAge_initial = self.maxAgeSelected
        
        //
        self.select_AgeIntMin = autoMinAgeInt
        self.select_AgeIntMax = autoMaxAgeInt
        
        //
        let selfUID = Auth.auth().currentUser?.uid
        if let selfUID = selfUID {
            UserDefaults.standard.setValue(autoMinAgeInt, forKeyPath: "\(selfUID)_minAgeIntDefault")
            UserDefaults.standard.setValue(autoMaxAgeInt, forKeyPath: "\(selfUID)_maxAgeIntDefault")
        }
    }
    
    var autoAgeIntMin: Int?
    var autoAgeIntMax: Int?
    
    func getAutoAgeBoundStrings() {
        guard let myAge = self.myProfileInfoDict["userAge"] else { return }
        let intMyAge = (myAge as NSString).integerValue
        
        var autoMinAgeInt = 18
        var autoMaxAgeInt = 100
        
        var autoMinAgeString = ""
        var autoMaxAgeString = ""
        
        let minAgeSelectable = 18
        let maxAgeSelectable = 100
        
        let plusUpperRange = 4
        let minusLowerRange = 4
        
        let maxAgeBound = [56, 57, 58, 59, 60]
        
        let lowerAgeDifference = intMyAge - minAgeSelectable
        
        if !maxAgeBound.contains(intMyAge) {
            if lowerAgeDifference < 4 {
                autoMinAgeInt = intMyAge - lowerAgeDifference
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
                
            } else {
                autoMinAgeInt = intMyAge - minusLowerRange
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
            }
            
            let stringMinAge = String(autoMinAgeInt)
            let stringMaxAge = String(autoMaxAgeInt)
            
            autoMinAgeString = stringMinAge
            autoMaxAgeString = stringMaxAge

            var ageBoundArray = [String]()
            ageBoundArray.append(autoMinAgeString)
            ageBoundArray.append(autoMaxAgeString)

//            return ageBoundArray
            
        } else {
            //print("user is 56 or older")
            
            autoMinAgeInt = intMyAge - minusLowerRange
            //print("autoMinAgeInt is,", autoMinAgeInt)
            autoMaxAgeInt = intMyAge + plusUpperRange
            
            let stringMinAge = String(autoMinAgeInt)
            
            //
            let maxRowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(maxRowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            self.setAgeFilterTitle()
            
            autoMinAgeString = stringMinAge
            autoMaxAgeString = "Maximum"
            
            var ageBoundArray = [String]()
            ageBoundArray.append(autoMinAgeString)
            ageBoundArray.append(autoMaxAgeString)
            
//            return ageBoundArray
        }
        
        self.autoAgeIntMin = autoMinAgeInt
        self.autoAgeIntMax = autoMaxAgeInt
        
//        //since once when view is initialized
//        self.select_AgeIntMin = autoMinAgeInt
//        self.select_AgeIntMax = autoMaxAgeInt
    }
    
    func setAgeFilterTitle() {
        
        //Default
        if minAgeSelected == "Minimum" && maxAgeSelected == "Maximum" {
            self.showMeAgeCellValue!.text = "All"
            self.finalAgeRangeString = "All"
        }
        
        //Lower Threshold a
        if minAgeSelected != "Minimum" && maxAgeSelected == "Maximum" {
            self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
            self.finalAgeRangeString = self.minAgeSelected + " and Above"
        }
        
        //Lower Threshold b
        if maxAgeSelected == "60+" && minAgeSelected != "Minimum" {
            self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
            self.finalAgeRangeString = self.minAgeSelected + " and Above"
        }
        
        //Upper Threshold
        if minAgeSelected == "Minimum" && maxAgeSelected != "Maximum" {
            self.showMeAgeCellValue!.text = self.maxAgeSelected + " and Below"
            self.finalAgeRangeString = self.maxAgeSelected + " and Below"
        }
        
        //Range
        if minAgeSelected != "Minimum" && maxAgeSelected != "Maximum" {
            if maxAgeSelected != "60+" {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " - " + self.maxAgeSelected
                self.finalAgeRangeString = self.minAgeSelected + " - " + self.maxAgeSelected
            } else {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
                self.finalAgeRangeString = self.minAgeSelected + " and Above"
            }
        }
        
        //Minimum 60
        if minAgeSelected == "60+" {
            //fix max age to "None"
            let rowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            self.showMeAgeCellValue!.text = "60+"
            self.finalAgeRangeString = "60+"
        }
        
        //
        if maxAgeSelected == "60+" && minAgeSelected == "Minimum"  {
            //set max age to "All"
            self.maxAgeSelected = "Maximum"
            self.showMeAgeCellValue!.text = "All"
            self.finalAgeRangeString = "All"
        }
        
        //
        let excludedMinSet = [56, 57, 58, 59, 60]
        let excludedMinSet_String = ["56", "57", "58", "59", "60+"]
        
        if excludedMinSet_String.contains(minAgeSelected) {
            let rowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            
            if minAgeSelected != "60+" {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
                self.finalAgeRangeString = self.minAgeSelected + " and Above"
                
            } else {
                self.showMeAgeCellValue!.text = "60+"
                self.finalAgeRangeString = "60+"
            }
        }
        
    }
    
    @objc func doneShowMeAgeButton(_ sender: UIButton) {
        //print("doneHeightButton tap detected")
        self.animateAgePicker(present: false)
        self.updateAgeFilter()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        //number of wheels
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //print("component is,", component)
        if component == 0 {
            return self.minAgeArray.count
        } else {
            return self.maxAgeArray.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        //print("titleForRow")
        
        if component == 0 {
            let minAge = self.minAgeArray[row]
            //print("minAge is", minAge )
            return minAge
        } else {
            let maxAge = self.maxAgeArray[row]
            //print("maxAge title is", maxAge )
            return maxAge
        }
    }
    
    var finalAgeRangeString: String?
    var minAgeSelected = "Minimum"
    var maxAgeSelected = "Maximum" 
//
    
    
    
//    func shortStringUI() {
//        let marginInsetRatio = 19 / 414 as CGFloat
//        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
//        self.showMeAgeCellValue.transform = CGAffineTransform(translationX: -marginInset, y: 0)
//        self.showMeAgeCellValue.textAlignment = .right
//    }
//
//    func longStringUI() {
//        let marginInsetRatio = 19 / 414 as CGFloat
//        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
//        self.showMeAgeCellValue.transform = CGAffineTransform(translationX: marginInset, y: 0)
//        self.showMeAgeCellValue.textAlignment = .center
//    }
    
//    var pickerDidDeactivateAutoAge = false
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        
        //print("@@")
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        //print("pickerView didSelectrow,")
        
//        if self.autoAgeIsEnabled == true {
//            self.switchAutoButtonUIStateonClick()
//        }
        
        
        let excludedMinSet = [56, 57, 58, 59, 60]
        let excludedMinSet_String = ["56", "57", "58", "59", "60+"]
        
        let selfUID = Auth.auth().currentUser?.uid

        if component == 0 {
            let minAgeSelected = self.minAgeArray[row]
            self.minAgeSelected = minAgeSelected
            //print("minAgeSelected is", minAgeSelected)
            
            //When min age is adjusted, make sure max age is always higher than min. Always push age up by increment if min<max
            
            var maxAgeInt = (self.maxAgeSelected as NSString).integerValue
            var minAgeInt = (self.minAgeSelected as NSString).integerValue
            
            if self.minAgeSelected == "Minimum" {
                minAgeInt = 18
            }
            
            if minAgeSelected != "Minimum" {
                
                if minAgeInt >= maxAgeInt && !excludedMinSet.contains(minAgeInt) && maxAgeSelected != "Maximum" {
                    //set maxAgeInt to always be  at least 4 above
                    maxAgeInt = minAgeInt + 4
                    let stringAge = String(maxAgeInt)
                    let rowIndex = self.maxAgeArray.index(of: stringAge)
                    self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                    self.maxAgeSelected = stringAge
                }
                
                let range = maxAgeInt - minAgeInt
                if range <= 4 && range >= 0 && !excludedMinSet.contains(minAgeInt) && maxAgeSelected != "Maximum" {
                    //print("currently selected maxAge,", maxAgeSelected)
                    
                    //need to increment maxAge by difference to complete range
                    let incrementNeeded = 4 - range
                    maxAgeInt = maxAgeInt + incrementNeeded
                    let stringAge = String(maxAgeInt)
                    let rowIndex = self.maxAgeArray.index(of: stringAge)
                    self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                    self.maxAgeSelected = stringAge
                }
                
            }
            
            //
            self.select_AgeIntMin = minAgeInt
            self.select_AgeIntMax = maxAgeInt
    
            //
            if let selfUID = selfUID {
                UserDefaults.standard.setValue(minAgeInt, forKeyPath: "\(selfUID)_minAgeIntDefault")
                UserDefaults.standard.setValue(maxAgeInt, forKeyPath: "\(selfUID)_maxAgeIntDefault")
            }
           

        } else {
            let maxAgeSelected = self.maxAgeArray[row]
            self.maxAgeSelected = maxAgeSelected
            //print("maxAgeSelected is", maxAgeSelected)

            //make sure maximum is always higher than minimum
//            if self.minAgeSelected != "Minimum" && maxAgeSelected != "Maximum"  {
            
            if maxAgeSelected != "Maximum"  {

                var maxAgeInt = (self.maxAgeSelected as NSString).integerValue
                var minAgeInt = (self.minAgeSelected as NSString).integerValue
                
                if self.minAgeSelected == "Minimum" {
                    minAgeInt = 18
                } else if self.minAgeSelected == "60+" {
                    minAgeInt = 60
                }
                
                let ageRange = maxAgeInt - minAgeInt
                //print("ageRange is", ageRange)
                
                if ageRange <= 4 && !excludedMinSet.contains(minAgeInt)   {
                    //print("ageRange controlflow", ageRange)
                    
                    if ageRange <= 0 {
                        maxAgeInt = minAgeInt + 4
                        let stringAge = String(maxAgeInt)
                        let rowIndex = self.maxAgeArray.index(of: stringAge)
                        self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                        self.maxAgeSelected = stringAge
                        
                    } else if ageRange == 1 {
                        maxAgeInt = maxAgeInt + 3
                        let stringAge = String(maxAgeInt)
                        let rowIndex = self.maxAgeArray.index(of: stringAge)
                        self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                        self.maxAgeSelected = stringAge

                    } else if ageRange == 2 {
                        maxAgeInt = maxAgeInt + 2
                        let stringAge = String(maxAgeInt)
                        let rowIndex = self.maxAgeArray.index(of: stringAge)
                        self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                        self.maxAgeSelected = stringAge

                    } else if ageRange == 3 {
                        maxAgeInt = maxAgeInt + 1
                        let stringAge = String(maxAgeInt)
                        let rowIndex = self.maxAgeArray.index(of: stringAge)
                        self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
                        self.maxAgeSelected = stringAge

                    } else if ageRange == 4 {
                        //
                    }
                }
                
                self.select_AgeIntMin = minAgeInt
                self.select_AgeIntMax = maxAgeInt
                
                //
                
                if let selfUID = selfUID {
                    UserDefaults.standard.setValue(minAgeInt, forKeyPath: "\(selfUID)_minAgeIntDefault")
                    UserDefaults.standard.setValue(maxAgeInt, forKeyPath: "\(selfUID)_maxAgeIntDefault")
                }
            }
            if self.maxAgeSelected == "Maximum" {
                select_AgeIntMax = 100
                if let selfUID = selfUID {
                    UserDefaults.standard.setValue(100, forKeyPath: "\(selfUID)_maxAgeIntDefault")
                }
            }
        }
        
        //Default
        if minAgeSelected == "Minimum" && maxAgeSelected == "Maximum" {
            self.showMeAgeCellValue!.text = "All"
            self.finalAgeRangeString = "All"
        }
        
        //Lower Threshold a
        if minAgeSelected != "Minimum" && maxAgeSelected == "Maximum" {
            self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
            self.finalAgeRangeString = self.minAgeSelected + " and Above"
        }
        
        //Lower Threshold b
        if maxAgeSelected == "60+" && minAgeSelected != "Minimum" {
            self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
            self.finalAgeRangeString = self.minAgeSelected + " and Above"
        }
        
        //Upper Threshold
        if minAgeSelected == "Minimum" && maxAgeSelected != "Maximum" {
            self.showMeAgeCellValue!.text = self.maxAgeSelected + " and Below"
            self.finalAgeRangeString = self.maxAgeSelected + " and Below"
        }
        
        //Range
        if minAgeSelected != "Minimum" && maxAgeSelected != "Maximum" {
            if maxAgeSelected != "60+" {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " - " + self.maxAgeSelected
                self.finalAgeRangeString = self.minAgeSelected + " - " + self.maxAgeSelected
            } else {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
                self.finalAgeRangeString = self.minAgeSelected + " and Above"
            }
        }
        
        //Minimum 60
        if minAgeSelected == "60+" {
            //fix max age to "None"
            let rowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            self.showMeAgeCellValue!.text = "60+"
            self.finalAgeRangeString = "60+"
        }
        
        //
        if maxAgeSelected == "60+" && minAgeSelected == "Minimum"  {
            //set max age to "All"
            self.maxAgeSelected = "Maximum"
            self.showMeAgeCellValue!.text = "All"
            self.finalAgeRangeString = "All"
        }
        
        //
        if excludedMinSet_String.contains(minAgeSelected) {
            let rowIndex = self.maxAgeArray.index(of: "Maximum")
            self.agePicker!.selectRow(rowIndex!, inComponent: 1, animated: true)
            self.maxAgeSelected = "Maximum"
            
            if minAgeSelected != "60+" {
                self.showMeAgeCellValue!.text = self.minAgeSelected + " and Above"
                self.finalAgeRangeString = self.minAgeSelected + " and Above"
                
            } else {
                self.showMeAgeCellValue!.text = "60+"
                self.finalAgeRangeString = "60+"
            }
        }
        
        //
        self.minAge_final = self.minAgeSelected
        self.maxAge_final = self.maxAgeSelected
        
        //
        if self.autoAgeIsEnabled == true {
            self.switchAutoButtonUIStateonClick()
        }

    }
    
    var select_AgeIntMin: Int?
    var select_AgeIntMax: Int?
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var pickerLabel: UILabel? = (view as? UILabel)
        
        if component == 0 {
            
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "AvenirNext-Regular", size: 22)
                pickerLabel?.textAlignment = .center
            }
            
            pickerLabel?.text = self.minAgeArray[row]
            pickerLabel?.textColor = UIColor.black
            
            
        } else if component == 1 {
            if pickerLabel == nil {
                pickerLabel = UILabel()
                pickerLabel?.font = UIFont(name: "AvenirNext-Regular", size: 22)
                pickerLabel?.textAlignment = .center
            }
            pickerLabel?.text = self.maxAgeArray[row]
            pickerLabel?.textColor = UIColor.black
        }
    
        return pickerLabel!
    }
    
    func animateAgePicker(present: Bool) {
        
        let agePicker = self.agePicker!
        let barView = self.barView!
        
        //declare pickerFrame
        let pW = UIScreen.main.bounds.width
        let pH = agePicker.frame.height
        let pX = 0 as CGFloat
        var pY = UIScreen.main.bounds.height - pH
        
        if present == false {
            pY = UIScreen.main.bounds.height + barView.frame.height
        } else {
            //            self.scrollView!.scrollRectToVisible(<#T##rect: CGRect##CGRect#>, animated: )
        }
        
        let pickerFrame = CGRect(x: pX, y: pY, width: pW, height: pH)
        //        let pickerFrameBottom = CGRect(x: pX, y: pY, width: pW, height: pH)
        
        //declare barViewFrame
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let barW = UIScreen.main.bounds.width
        let barH = 2 * gridUnitHeight
        let barX = 0 as CGFloat
        var barY = pY - barH
        
        if present == false {
            barY = UIScreen.main.bounds.height
        }
        
        let barF = CGRect(x: barX, y: barY, width: barW, height: barH)
        
        let duration = 0.25
        //Animate frames
        self.agePicker!.isHidden = false
        self.barView!.isHidden = false
        
        UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            //            self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
            //print("in animator")
            agePicker.frame = pickerFrame
            barView.frame = barF
        }, completion: nil)
    }
    
    var deleteLabel: UITextView?
    var deleteCell: UIView!
    
    func addDeleteCell() {
        
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        //
        
        let cellHeight = (53 / 414) * sWidth
        let cellWidth = sWidth
        let cellX = 0 as CGFloat
//        let cellY = self.showMeAgeCellView.frame.maxY + (1.5 * gridUnitHeight)
        let cellY = self.sliderSpaceView!.frame.maxY + (1.5 * gridUnitHeight)

        let cellFrame = CGRect(x: cellX, y: cellY, width: cellWidth, height: cellHeight)
        let cell = UIView(frame: cellFrame)
        cell.backgroundColor = UIColor.white
        self.deleteCell = cell
        self.view.addSubview(cell)
        
        //
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        let fontName = "AvenirNext-Regular"
        
        //add title
        // Font Size
        let tFontSize = 16 as CGFloat
        var nW = 150 as CGFloat
        var nH = 0 as CGFloat
        let titleString = "Delete Account"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: fontName, fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
        let nX = marginInset
        let nY = (cellHeight * 0.5) - (0.5 * tH)
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let deleteLabel = UITextView(frame: f)
        
        //
        deleteLabel.text = titleString
        self.deleteCell.addSubview(deleteLabel)
        self.deleteLabel = deleteLabel

        self.deleteLabel!.font = UIFont(name: fontName, size: tFontSize)!
        self.deleteLabel!.textAlignment = .left
        self.deleteLabel!.isEditable = false
        self.deleteLabel!.isScrollEnabled = false
        self.deleteLabel!.isSelectable = false
        self.deleteLabel!.textContainer.lineFragmentPadding = 0
        self.deleteLabel!.textContainerInset = .zero
        self.deleteLabel!.textColor = UIColor.black
        self.deleteLabel!.backgroundColor = UIColor.clear
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.deleteAccount(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.deleteCell!.addGestureRecognizer(tapGesture)
        self.deleteCell!.isUserInteractionEnabled = true
        
        //& Line View
        //add Line Border
        let lineW = sWidth
        let lineH = 0.2 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.deleteCell!.frame.maxY
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        let lineColor = UIColor(displayP3Red: 225/250, green: 225/250, blue: 225/250, alpha: 225/250)
        lineView.backgroundColor = lineColor
        
        self.view!.addSubview(lineView)
    }
    
    var ref: DatabaseReference!
    
    @objc func deleteAccount(_ sender: UITapGestureRecognizer) {
        self.present(self.alert, animated: true, completion: nil)
    }
    
    //ATTENTION: update deleteAuth to delete all swapRequestObjects & all chatThreadIDs
    
    //Note: Delete account will delete user object before deleting account? -> User will be force logged out before account deletion can happen. 
    func deleteAuth() {
        
        let selfUID = Auth.auth().currentUser!.uid

        self.activityIndicatorView?.startAnimating()
        
        //print("deleteAccount")
        
        //OK to remove value when Auth == self
//        self.ref.child("users/\(selfUID)").removeValue()
//        self.ref.child("userLocationGF/\(selfUID)").removeValue()
//        self.ref.child("globalSwapRequests/\(selfUID)").removeValue()

//        self.ref.child("deletedUsers/\(selfUID)").setValue("true")
//        self.ref.child("globalUserPresence/\(selfUID)/isOnline").setValue("false")
        
        let nullUpdates = [
            "users/\(selfUID)" : NSNull(),
            "userLocationGF/\(selfUID)" : NSNull(),
            "globalSwapRequests/\(selfUID)" : NSNull()
        ]
        
        self.ref.updateChildValues(nullUpdates)
        
        let metaUpdates = [
            "deletedUsers/\(selfUID)" : "true",
            "globalUserPresence/\(selfUID)/isOnline" : "false",
        ]
        
        self.ref.updateChildValues(metaUpdates)

        let user = Auth.auth().currentUser
        user?.delete { error in
            if let error = error {
                // An error happened.
                //print("error is", error)
                self.performSegue(withIdentifier: "toReAuth", sender: self)
                self.activityIndicatorView?.stopAnimating()
            } else {
                // Account deleted.
                self.performSegue(withIdentifier: "toWelcomeScreen", sender: self)
            }
        }
    }
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 16 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
//        let aX = (sWidth - aW) * 0.5
//        let aY = (sHeight - aH) * 0.5
        let aX = self.showMeAgeCellValue!.frame.maxX - aH
        let aY = (self.deleteCell!.frame.height - aH) * 0.5

        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        self.deleteCell.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
//        self.activityIndicatorView?.startAnimating()
    }
    
    var alert: UIAlertController!
    
    func addAlert() {
        
        //        let alertMessage = "Making your profile private will delete any previous profile unlock requests you made to others."
        //        let alertTitle = "Private Visibility"
        
        let alertTitle = "Delete Account"
        let alertMessage = "You are about to permanently delete your account. Are you sure you want to continue?"
//        let alertMessage = "You are about to permanently delete your account. Are you sure you want to continue?" + "\n" + "You will need to use a different email or phone number than the one you signed up with to join again in the future."
//        let alertMessage = "You are about to permanently delete your account. Are you sure you want to continue?" + "\n" + "You will need to use a new email or phone number to sign up again in the future."
//        let alertMessage = "You are about to permanently delete your account." + "\n" + "You will need to use a new email or phone number to sign up again in the future." + "\n" + "Are you sure you want to continue?"

        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let cancelChanges = UIAlertAction(title: "Delete", style: .destructive) { (action) in
            self.deleteAuth()
        }
        
//        let delTitle = "Go Back"
        let delTitle = "Cancel"

        let keepChanges = UIAlertAction(title: delTitle, style: .default) { (action) in
            // Respond to user selection of the action.
            self.alert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelChanges)
        alert.addAction(keepChanges)
//        alert.view.tintColor = UIColor.black

        self.alert = alert
    }
    
    var settingsInfo: UITextView?
    
    func addSettingsInfo() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        let tWidthRatio = 365 / 414 as CGFloat
        
        var tWidth = 330 as CGFloat
        let tFontSize = 14.0 as CGFloat
        
        let veriCopy = "ximo v1.1" + "\n " + "Copyright \u{00A9} 2018 Animata Inc."
        let textFieldCGSize = self.sizeOfCopy(string: veriCopy, constrainedToWidth: Double(tWidth), fontName: "Avenir-Light", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        
//        let tY = self.deleteCell!.frame.maxY + (0.5 * gridUnitHeight)
        let tY = (sHeight - tH) - (0.5 * gridUnitHeight)

        let tReg1 = "ximo v1.0"
        let tReg2 = "Copyright \u{00A9} 2018 Animata Inc."
        let r1 = (veriCopy as NSString).range(of: tReg1)
        let r2 = (veriCopy as NSString).range(of: tReg2)
        
        let attributedString = NSMutableAttributedString(string: veriCopy)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: 12.0)!, range: r1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: 12.0)!, range: r2)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let settingsInfo = UITextView(frame: tFrame)
        
        settingsInfo.font = UIFont(name: "Avenir-Light", size: tFontSize)
        
        self.view.addSubview(settingsInfo)
        
        self.settingsInfo = settingsInfo
//        self.settingsInfo!.text = veriCopy
        self.settingsInfo!.attributedText = attributedString
        self.settingsInfo!.textAlignment = .center
        self.settingsInfo!.isEditable = false
        self.settingsInfo!.isScrollEnabled = false
        self.settingsInfo!.isSelectable = false
        self.settingsInfo!.textContainer.lineFragmentPadding = 0
        self.settingsInfo!.textContainerInset = .zero
        self.settingsInfo!.textColor = UIColor.lightGray
        self.settingsInfo!.backgroundColor = UIColor.clear
    }
    
    var ref_SelfModelController = SelfModelController()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindSettingsToHome" {
            let controller = segue.destination as! HomeScreenViewController
//            controller.ref_SelfModelController = self.ref_SelfModelController

        } else if segue.identifier == "toReAuth" {
            let controller = segue.destination as! SignInViewController
            controller.sourceVC = "SettingsVC"
            controller.statusBarHeight = self.statusBarHeight!
        }
    }
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
}
