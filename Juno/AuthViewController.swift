//
//  ViewController.swift
//  Juno
//
//  Created by Asaad on 1/27/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//
 
import UIKit
import FirebaseAuth
import UserNotifications
import Firebase

class AuthViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UNUserNotificationCenterDelegate {
    
    //MARK: Global Properties
    var keyBoardHeight: CGFloat?
    var userEmailString = ""
    var validatedUserEmailString = ""
   
    var ref_AuthTracker = AuthTracker()

    var ref: DatabaseReference!
    
    //MARK: Global Outlets
    @IBOutlet weak var mainStack: UIStackView!
    @IBOutlet weak var mobileStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var mobileLineView: UIImageView!
    
//    @IBOutlet weak var emailTextField: UITextField!
//    @IBOutlet weak var emailLineView: UIImageView!
    var emailTextField: UITextField!
    var emailLineView: UIImageView!

//    @IBOutlet weak var buttonStackView: UIStackView!
//    @IBOutlet weak var sbContinueButton: UIButton!

    var continueButton: UIButton!
    
//    @IBOutlet weak var facebookSignUp: UIButton!
    
    //MARK: View Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        self.mainStack.isHidden = true
        
        self.addTitleLabel()
        self.addEmailTitleHeader()
        self.addEmailTextField()
        self.addPhoneNumberTextField()
        self.addLineView()
        self.addContinueButton()
        self.addToggleAuthButton()
        self.addActivityIndicator()
        
        //
        self.addConfirmationCodeHeader()
        self.addConfirmationCodeTextField()
        self.addConfirmationLineView()

//        self.addBackButton()
//        sbContinueButton.setImage(UIImage(named: "GrayButton"), for: UIControlState.normal)
//        sbContinueButton.isEnabled = false
        
        mobileTextField.delegate = self
        emailTextField.delegate = self
      
//        self.facebookSignUp.isHidden = true
//        self.positionStackView()
    
        self.addPrivacyAndTermsOfService()
        self.addEmailErrorLabel()
        self.addPhoneNumberErrorLabel()
        self.addConfirmationCodeErrorLabel()
        
        self.phoneNumberErrorLabel?.isHidden = true
        self.emailErrorLabel!.isHidden = true
        self.confirmationCodeErrorLabel?.isHidden = true
        
        self.addNotificationRequiredAlert()
        self.addNotificationRequired_toSettings()
        self.addBackgroundRefreshAlert()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AuthViewController.willEnterForeground), name: .UIApplicationWillEnterForeground , object: nil)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let pushID = appDelegate.pushID
        print("aVC pushID", pushID)
        
        self.ref_AuthTracker.pushID = pushID
    }
    
    @objc func willEnterForeground(_ notification: Notification) {
        
        //print("app willEnterForeground state")
        DispatchQueue.main.async {
            self.retrieveBackgroundRefresh_Status()
        }
        
    }
    
    var authIsValid = true
    var authErrorString: String?

    var bottomPadding: CGFloat?
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
        
        self.mobileStackView.isHidden = true
//        self.buttonStackView.isHidden = true
        
        if self.authIsValid == true {
            self.emailErrorLabel?.isHidden = true
//            self.TOSTextView.isHidden = true
            self.TOSTextView.alpha = 0

        } else {
            self.emailErrorLabel?.isHidden = false
            self.emailErrorLabel?.text = self.authErrorString!
            self.TOSTextView.alpha = 1
            self.TOSTextView.isHidden = false
        }
        
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            self.bottomPadding = bottomPadding
            //print("AVC iPhoneX bottomPadding,", bottomPadding)
        }
        
        if self.unwindFromWebView == true {
            self.TOSTextView.alpha = 1
            self.TOSTextView.isHidden = false
        }
        
        self.emailTextField.textContentType = UITextContentType("")
        self.emailTextField.keyboardType = .emailAddress
      
        DispatchQueue.main.async {
            self.retrieveBackgroundRefresh_Status()
        }
    }
    
    var tosLabel: UILabel!
    var tosButton: UIButton!
    var unwindFromWebView = false
    
    var TOSTextView: UITextView!
    let hyperBlue = UIColor(displayP3Red: 0, green: 108/255, blue: 255/255, alpha: 1)
    
    func addPrivacyAndTermsOfService() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 12.0 as CGFloat
        let tWidth = self.emailLineView!.frame.width * 1.2
  
        let veriCopy = "By continuing, you acknowledge that you have read our" + "\n" + "Privacy Policy and agreed to our Terms of Service"
        if sWidth < 350 {
            tFontSize = 10.0 as CGFloat
        }
        
        let textFieldCGSize = self.sizeOfCopy(string: veriCopy, constrainedToWidth: Double(tWidth), fontName: "AvenirNext-Regular", fontSize: 12.0)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let heightInset = 7 as CGFloat
        let tY = self.emailLineView!.frame.maxY + heightInset
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let tView = UITextView(frame: tFrame)
        tView.delegate = self
        tView.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)
        
        //configure text
        let tReg = "By continuing, you acknowledge that you have read our"
        let tBlue1 = "Privacy Policy"
        let tReg2 = "and agreed to our"
        let tBlue2 = "Terms of Service"
        
        let range1 = (veriCopy as NSString).range(of: tReg)
        let range2 = (veriCopy as NSString).range(of: tBlue1)
        let range3 = (veriCopy as NSString).range(of: tReg2)
        let range4 = (veriCopy as NSString).range(of: tBlue2)
        
        let fontName = "AvenirNext-Regular"
        
        let attributedString = NSMutableAttributedString(string: veriCopy)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: fontName, size: tFontSize)!, range: range1)
        
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: self.hyperBlue, range: range2)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: fontName, size: tFontSize)!, range: range2)
        
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range3)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: fontName, size: tFontSize)!, range: range3)
        
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: self.hyperBlue, range: range4)
        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: fontName, size: tFontSize)!, range: range4)
       
        tView.attributedText = attributedString
        
        //
        self.view.addSubview(tView)
        
        self.TOSTextView = tView
        self.TOSTextView!.textAlignment = .center
        self.TOSTextView!.isEditable = false
        self.TOSTextView!.isScrollEnabled = false
        self.TOSTextView!.isSelectable = false
        self.TOSTextView!.textContainer.lineFragmentPadding = 0
        self.TOSTextView!.textContainerInset = .zero
//        self.TOSTextView!.textColor = UIColor.white
        self.TOSTextView!.backgroundColor = UIColor.clear

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.textViewWasTapped(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.TOSTextView!.addGestureRecognizer(tapGesture)
        self.TOSTextView!.isUserInteractionEnabled = true
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    var privacyPolicy = true
    
    @objc func textViewWasTapped(_ tapGesture: UITapGestureRecognizer) {
        //print("AuthVC textViewWasTapped")
        let point = tapGesture.location(in: self.TOSTextView!)
        if let wordTapped = self.wordAtPoint(point) {
            //print("AuthVC textView wordTapped is,", wordTapped)
            
            if wordTapped == "Privacy" || wordTapped == "Policy" {
                self.privacyPolicy = true
                self.performSegue(withIdentifier: "toWebView", sender: self)
            }
            
            if wordTapped == "Terms" || wordTapped == "of" || wordTapped == "Service" {
                self.privacyPolicy = false
                self.performSegue(withIdentifier: "toWebView", sender: self)
            }
        }
    }
    
    func wordAtPoint(_ point: CGPoint) -> String? {
        if let textPosition = self.TOSTextView!.closestPosition(to: point) {
            if let range = self.TOSTextView!.tokenizer.rangeEnclosingPosition(textPosition, with: .word, inDirection: 1) {
                return self.TOSTextView!.text(in: range)
            }
        }
        return nil
    }
    
    @objc func toWebView(_ sender: UIButton) {
    
        self.performSegue(withIdentifier: "toWebView", sender: self)

    }
    
    //MARK: Button View Formatting
    
    let activeGreenColor = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    var inactiveGrayColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    var superInsetHeight: CGFloat?
    
    var titleLabel: UILabel!
    var emailTitleHeader: UILabel!
    
    func addTitleLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 27 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let titleLabel = UILabel(frame: tF)
        
        //init
        self.view.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.titleLabel!.text = "Create a New Account"
        self.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 24)!
        self.titleLabel!.textColor = UIColor.black
        self.titleLabel!.textAlignment = .center
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.trySwitch(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        self.titleLabel.addGestureRecognizer(tapGesture)
//        self.titleLabel.isUserInteractionEnabled = true
    }

    
    @objc func trySwitch(_ sender: UITapGestureRecognizer) {
        if self.nowInConfirmationScene == false {
            self.slideConfirmationViews(toLeft: true)
        } else {
            self.slideConfirmationViews(toLeft: false)
        }
    }
    
    
    func addEmailTitleHeader() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
        let lY = superInsetHeight + (3 * gridUnitHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let emailTitleHeader = UILabel(frame: tF)
        
        emailTitleHeader.text = "Sign Up Using Email"
//        emailTitleHeader.text = "Email Address"

        emailTitleHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        emailTitleHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(emailTitleHeader)
        self.emailTitleHeader = emailTitleHeader
    }
    
    

    func addEmailTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        let lY = (2 * superInsetHeight) + gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let emailTextField = UITextField(frame: tF)
        
        emailTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        emailTextField.minimumFontSize = 17.0
        emailTextField.adjustsFontSizeToFitWidth = true
        
        emailTextField.autocapitalizationType = .none
        emailTextField.autocorrectionType = .no
        emailTextField.keyboardType = .emailAddress
        emailTextField.clearButtonMode = .whileEditing
        
        emailTextField.delegate = self
        
        self.view.addSubview(emailTextField)
        self.emailTextField = emailTextField
    }

    var phoneNumberTextField: UITextField!
    
    func addPhoneNumberTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        let lY = (2 * superInsetHeight) + gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let phoneNumberTextField = UITextField(frame: tF)
        
        phoneNumberTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        phoneNumberTextField.minimumFontSize = 17.0
        phoneNumberTextField.adjustsFontSizeToFitWidth = true
        
        phoneNumberTextField.autocapitalizationType = .none
        phoneNumberTextField.autocorrectionType = .no
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.clearButtonMode = .whileEditing
        phoneNumberTextField.placeholder = "10-digit Phone Number"
        phoneNumberTextField.delegate = self
        
        self.view.addSubview(phoneNumberTextField)
        self.phoneNumberTextField = phoneNumberTextField
        self.phoneNumberTextField.isHidden = true
    }
    
    //MARK: Confirmation Code Views
    
    var confirmationCodeHeader: UILabel!
    
    func addConfirmationCodeHeader() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth + sWidth
        let lY = superInsetHeight + (3 * gridUnitHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let confirmationCodeHeader = UILabel(frame: tF)
        
        confirmationCodeHeader.text = "Enter the Code We Sent to Your Number"
        //        emailTitleHeader.text = "Email Address"
        
        confirmationCodeHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        confirmationCodeHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(confirmationCodeHeader)
        self.confirmationCodeHeader = confirmationCodeHeader
        self.confirmationCodeHeader.isHidden = true
    }
    
    var confirmationCodeTextField: UITextField!
    
    func addConfirmationCodeTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth + sWidth
        let lY = (2 * superInsetHeight) + gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let confirmationCodeTextField = UITextField(frame: tF)
        
        confirmationCodeTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        confirmationCodeTextField.minimumFontSize = 17.0
        confirmationCodeTextField.adjustsFontSizeToFitWidth = true
        
        confirmationCodeTextField.autocapitalizationType = .none
        confirmationCodeTextField.autocorrectionType = .no
        confirmationCodeTextField.keyboardType = .numberPad
        confirmationCodeTextField.clearButtonMode = .whileEditing
        confirmationCodeTextField.placeholder = "6-digit Confirmation Code"
        confirmationCodeTextField.delegate = self
        confirmationCodeTextField.backgroundColor = UIColor.clear
        
        self.view.addSubview(confirmationCodeTextField)
        self.confirmationCodeTextField = confirmationCodeTextField
        self.confirmationCodeTextField.isHidden = true
    }
    
    var confirmationCodeLineView: UIImageView!
    
    func addConfirmationLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = 0.50 as CGFloat
        let lX = gridUnitWidth + sWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.confirmationCodeTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let confirmationCodeLineView = UIImageView(frame: f)
        confirmationCodeLineView.backgroundColor = UIColor.white
        confirmationCodeLineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(confirmationCodeLineView)
        self.confirmationCodeLineView = confirmationCodeLineView
        self.confirmationCodeLineView.isHidden = true
    }
    
    var confirmationCodeErrorLabel: UITextView?
    
    func addConfirmationCodeErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Code. Please Try Again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = ((sWidth - w) * 0.5) + sWidth
        let insetPadding = 5 as CGFloat
        //        let y = self.emailLineView!.frame.maxY + insetPadding
        let y = self.emailLineView.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let confirmationCodeErrorLabel = UITextView(frame: f)
        
        //
        confirmationCodeErrorLabel.text = errorString
        self.view.addSubview(confirmationCodeErrorLabel)
        self.confirmationCodeErrorLabel = confirmationCodeErrorLabel
        
        self.confirmationCodeErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.confirmationCodeErrorLabel!.textAlignment = .center
        self.confirmationCodeErrorLabel!.isEditable = false
        self.confirmationCodeErrorLabel!.isScrollEnabled = false
        self.confirmationCodeErrorLabel!.isSelectable = false
        self.confirmationCodeErrorLabel!.textContainer.lineFragmentPadding = 0
        self.confirmationCodeErrorLabel!.textContainerInset = .zero
        self.confirmationCodeErrorLabel!.textColor = UIColor.red
        self.confirmationCodeErrorLabel!.backgroundColor = UIColor.clear
        self.confirmationCodeErrorLabel!.isHidden = true
    }
  
    func addLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = 0.50 as CGFloat
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.emailTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineView = UIImageView(frame: f)
        lineView.backgroundColor = UIColor.white
        lineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(lineView)
        self.emailLineView = lineView
    }
    
    func addContinueButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = 3 * gridUnitWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
        //high-screen position
        //        let bY = sHeight - superInsetHeight - bH
        //lo-screen position
        let bY = sHeight - superInsetHeight
        self.superInsetHeight = superInsetHeight

        //
        let f = CGRect(x: bX, y: bY, width: bW, height:bH)
        let continueButton = UIButton(frame: f)
        
        continueButton.addTarget(self, action: #selector(AuthViewController.passInfo(_:)), for: .touchUpInside)
        continueButton.backgroundColor = self.inactiveGrayColor
        continueButton.setBackgroundImage(UIImage(named: "darkGreenBackGround")!, for: .highlighted)

        continueButton.isUserInteractionEnabled = true
        self.view.addSubview(continueButton)
        self.continueButton = continueButton

        self.continueButton!.layer.cornerRadius = 8
        self.continueButton!.layer.masksToBounds = true
        
        self.continueButton!.setTitle("Continue", for: .normal)
        self.continueButton!.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 16.0)!
        self.continueButton!.setTitleColor(UIColor.white, for: .normal)
    }
    
    var toggleAuth: UITextView?
    var bottomButtonPadding: CGFloat!
    var tapFrame: UIView!
    
    func addToggleAuthButton() {
        
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let tFontSize = 12.0 as CGFloat
        let buttonCopy = "Sign Up Using Phone Instead"
        let tWidth = sWidth - (2 * gridUnitWidth)
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontName: "Avenir-Medium", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let marginSpace = 2 as CGFloat
        let tY = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let toggleAuth = UITextView(frame: tFrame)
        toggleAuth.text = buttonCopy
        toggleAuth.font = UIFont(name: "Avenir-Medium", size: tFontSize)
        
        let tapFrameWidth = 4 * gridUnitWidth
        let tapFrameX = (sWidth - tapFrameWidth) * 0.5
        let tapFrame = UIView(frame: CGRect(x: tapFrameX, y: tY, width: tapFrameWidth, height: tH * 2))
        tapFrame.backgroundColor = UIColor.clear
        //
        self.view.addSubview(toggleAuth)
        self.toggleAuth = toggleAuth
        
        self.view.addSubview(tapFrame)
        self.tapFrame = tapFrame
        
        self.toggleAuth!.textAlignment = .center
        self.toggleAuth!.isEditable = false
        self.toggleAuth!.isScrollEnabled = false
        self.toggleAuth!.isSelectable = false
        self.toggleAuth!.textContainer.lineFragmentPadding = 0
        self.toggleAuth!.textContainerInset = .zero
        self.toggleAuth!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.toggleAuth!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AuthViewController.switchAuth_UI(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.tapFrame.addGestureRecognizer(tapGesture)
        self.tapFrame.isUserInteractionEnabled = true
//        self.toggleAuth!.addGestureRecognizer(tapGesture)
//        self.toggleAuth!.isUserInteractionEnabled = true
        
        self.bottomButtonPadding = UIScreen.main.bounds.height - self.toggleAuth!.frame.maxY
    }
    
    var authIsEmail = true
    var phoneNumberString = ""
    
    @objc func switchAuth_UI(_ sender: UITapGestureRecognizer) {
        
        if self.toggleAuthShouldGoBackToPhone == true {
            
            //moved to end of animation block
//            self.veriCodeDidReturnErrorCode = false
//            self.confirmationCodeTextField.text = ""
//            self.confirmationCodeTextField.resignFirstResponder()
            
            self.slideConfirmationViews(toLeft: false)
            
            return
        }
        
        if self.authIsEmail == true {
            
            //
            
            //Start at email
            self.emailErrorLabel!.isHidden = true
            self.continueButton.backgroundColor = self.inactiveGrayColor
            self.continueButton.isUserInteractionEnabled = false
            self.phoneNumberTextField.text = ""
            self.emailTextField.text = ""
            self.toggleAuth!.text = "Sign Up Using Email Instead"
            self.emailTitleHeader.text = "Sign Up Using Phone (U.S. Only)"
//            self.emailTitleHeader.text = "Phone Number (U.S. Only)"

            self.authIsEmail = false
            
            //
            self.emailTextField.isHidden = true
            self.phoneNumberTextField.isHidden = false
            
            if !emailTextField.isFirstResponder && !phoneNumberTextField.isFirstResponder {
                //don't make 1st resp when both fields are inactive
            }
            
            if emailTextField.isFirstResponder {
                self.phoneNumberTextField.becomeFirstResponder()
            }
        } else {
            //Now at phone
            self.continueButton.backgroundColor = self.inactiveGrayColor
            self.continueButton.isUserInteractionEnabled = false
            self.emailTextField.text = ""
            self.phoneNumberTextField.text = ""
            self.toggleAuth!.text = "Sign Up Using Phone Instead"
            self.emailTitleHeader.text = "Sign Up Using Email"
//            emailTitleHeader.text = "Email Address"
            self.authIsEmail = true
            
            //
            self.phoneNumberTextField.isHidden = true
            self.emailTextField.isHidden = false
            
            
            if !emailTextField.isFirstResponder && !phoneNumberTextField.isFirstResponder {
                //don't make 1st resp when both fields are inactive
            }
            
            if phoneNumberTextField.isFirstResponder {
                self.emailTextField.becomeFirstResponder()
            }
        }
    }
    
    var emailErrorLabel: UITextView?
    
    func addEmailErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Email Address. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
//        let y = self.emailLineView!.frame.maxY + insetPadding
        let y = self.TOSTextView!.frame.maxY + insetPadding

        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let nameLabel = UITextView(frame: f)
        
        //
        nameLabel.text = errorString
        self.view.addSubview(nameLabel)
        self.emailErrorLabel = nameLabel
        
        self.emailErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.emailErrorLabel!.textAlignment = .center
        self.emailErrorLabel!.isEditable = false
        self.emailErrorLabel!.isScrollEnabled = false
        self.emailErrorLabel!.isSelectable = false
        self.emailErrorLabel!.textContainer.lineFragmentPadding = 0
        self.emailErrorLabel!.textContainerInset = .zero
        self.emailErrorLabel!.textColor = UIColor.red
        self.emailErrorLabel!.backgroundColor = UIColor.clear
    }
    
    var phoneNumberErrorLabel: UITextView?

    func addPhoneNumberErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Error. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        //        let y = self.emailLineView!.frame.maxY + insetPadding
        let y = self.TOSTextView.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let phoneNumberErrorLabel = UITextView(frame: f)
        
        //
        phoneNumberErrorLabel.text = errorString
        self.view.addSubview(phoneNumberErrorLabel)
        self.phoneNumberErrorLabel = phoneNumberErrorLabel
        
        self.phoneNumberErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.phoneNumberErrorLabel!.textAlignment = .center
        self.phoneNumberErrorLabel!.isEditable = false
        self.phoneNumberErrorLabel!.isScrollEnabled = false
        self.phoneNumberErrorLabel!.isSelectable = false
        self.phoneNumberErrorLabel!.textContainer.lineFragmentPadding = 0
        self.phoneNumberErrorLabel!.textContainerInset = .zero
        self.phoneNumberErrorLabel!.textColor = UIColor.red
        self.phoneNumberErrorLabel!.backgroundColor = UIColor.clear
    }
    
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "AvenirNext-DemiBold", size: fontSize)! ],
            context: nil).size
    }
    
    var backButton: UIButton?
    
    var statusBarHeight: CGFloat?
    
    func addBackButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let topInsetRatio = 22 / 736 as CGFloat
        let sBarH = self.statusBarHeight!
        
        var topInset = (topInsetRatio * sHeight) + sBarH
        //print("topInset", topInset, "sBarH", sBarH)
        
        if sHeight > 750 {
            topInset = (topInsetRatio * 736) + sBarH
        }
        
        //
        let fW = 50 as CGFloat
        let fH = fW as CGFloat
        let fX = 0 as CGFloat
        let fY = 0 as CGFloat
        
        let fF = CGRect(x: fX, y: fY, width: fW, height: fH)
        let fView = UIButton(frame: fF)
        
        fView.setImage(UIImage(named: "accountBackButton")!, for: .normal)
        fView.backgroundColor = UIColor.clear
        fView.contentMode = .center
        
        fView.addTarget(self, action: #selector(AuthViewController.goBack(_:)), for: .touchUpInside)
        fView.isUserInteractionEnabled = true
        
        self.view.addSubview(fView)
        self.backButton = fView
        
        var cX = gridUnitWidth * 0.5
        var cY = gridUnitHeight * 1.5
        if sHeight > 750 {
            cX = self.emailLineView!.frame.minX
            cY = self.titleLabel!.center.y
        }
        let centerPoint = CGPoint(x: cX, y: cY)
        self.backButton!.layer.position = centerPoint
    }
    
    @objc func goBack(_ sender: UIButton) {
        self.performSegue(withIdentifier: "backToWelcome", sender: self)
    }
    
    
    @objc func passInfo(_ sender: UIButton) {
        //print("passInfo detected")
        
        //3
        if self.nowInConfirmationScene == true {
            //sign in user
            self.signInUser_Phone_Step2()
            
            return
        }
        
        //1
        if self.authIsEmail == true {
            //Email Auth
            let userPasswordString = emailTextField.text!
            let outcome = self.validateEmail(userPasswordString)

            if outcome == true {
                self.validatedUserEmailString = userPasswordString
                self.emailTextField.resignFirstResponder()
                self.mobileTextField.resignFirstResponder()
                performSegue(withIdentifier: "SecurityViewController", sender: self)
                
            } else if self.emailTextField.text! != "" {
                self.emailErrorLabel!.isHidden = false
                self.validatedUserEmailString = ""
                //print("please enter valid email")
            }
            
        //2
        } else {
            //Phone Auth
            if let numberString = self.phoneNumberTextField.text {
                if numberString.count >= 10 {
                    if self.phoneNumberTextField.isFirstResponder {
                        self.phoneNumberTextField.resignFirstResponder()
                    }
                    DispatchQueue.main.async {
                        self.authPhone_Step1()
                    }
                }
            }
        }
    }

    var veriCodeDidReturnErrorCode = false
    
//    Asynchronously signs in to Firebase with the given 3rd-party credentials (e.g. a Facebook login Access Token, a Google ID Token/Access Token pair, etc.) and returns additional identity provider data.
//    Remark
//
//    Possible error codes: FIRAuthErrorCodeInvalidCredential - Indicates the supplied credential is invalid. This could happen if it has expired or it is malformed. FIRAuthErrorCodeOperationNotAllowed - Indicates that accounts with the identity provider represented by the credential are not enabled. Enable them in the Auth section of the Firebase console. FIRAuthErrorCodeAccountExistsWithDifferentCredential - Indicates the email asserted by the credential (e.g. the email in a Facebook access token) is already in use by an existing account, that cannot be authenticated with this sign-in method. Call fetchProvidersForEmail for this user’s email and then prompt them to sign in with any of the sign-in providers returned. This error will only be thrown if the "One account per email address" setting is enabled in the Firebase console, under Auth settings. FIRAuthErrorCodeUserDisabled - Indicates the user's account is disabled. FIRAuthErrorCodeWrongPassword - Indicates the user attempted sign in with an incorrect password, if credential is of the type EmailPasswordAuthCredential. FIRAuthErrorCodeInvalidEmail - Indicates the email address is malformed. FIRAuthErrorCodeMissingVerificationID - Indicates that the phone auth credential was created with an empty verification ID. FIRAuthErrorCodeMissingVerificationCode - Indicates that the phone auth credential was created with an empty verification code. FIRAuthErrorCodeInvalidVerificationCode - Indicates that the phone auth credential was created with an invalid verification Code. FIRAuthErrorCodeInvalidVerificationID - Indicates that the phone auth credential was created with an invalid verification ID. FIRAuthErrorCodeSessionExpired - Indicates that the SMS code has expired.
//
    func signInUser_Phone_Step2() {
        //Sign In After veriCode is received
        
        guard let verificationCode = self.confirmationCodeTextField.text else { return }
        //print("verificationCode", verificationCode)
        guard verificationCode.count >= 6 else { return }
        guard let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") else { return }
        
        self.confirmationCodeTextField.resignFirstResponder()
        confirmationCodeLineView.image = UIImage(named: "GrayLine")!
        self.confirmationCodeTextField.isUserInteractionEnabled = false
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                //print("phoneAuthError")
                
                //
                self.confirmationCodeErrorLabel?.isHidden = false
                self.confirmationCodeErrorLabel?.text = "Invalid Code. Please Try Again."
                self.activityIndicatorView.stopAnimating()
                self.continueButton.isUserInteractionEnabled = false
                self.continueButton.backgroundColor = self.inactiveGrayColor
                
                self.confirmationCodeTextField.becomeFirstResponder()
                self.confirmationCodeLineView.image = UIImage(named: "BlueLine")!
                
                self.confirmationCodeTextField.isUserInteractionEnabled = true
                self.veriCodeDidReturnErrorCode = true
                
                self.activityIndicatorView.stopAnimating()
                
                //reenable views on error
                self.tapFrame.isUserInteractionEnabled = true
                self.phoneNumberTextField.isUserInteractionEnabled = true
                self.veriCodeDidReturnErrorCode = true
                
                if let errorCode = AuthErrorCode(rawValue: error._code) {
                    switch errorCode {
                    case .captchaCheckFailed :
                        print("captchaCheckFailed")
                        
                    case .invalidPhoneNumber :
                        print("invalidPhoneNumber")
                        
                    case .missingPhoneNumber :
                        print("missingPhoneNumber")
                        
                    case .quotaExceeded :
                        print("quotaExceeded")
                        self.confirmationCodeErrorLabel?.text = "We're experiencing unusually high traffic with this sign up method at the moment. Please try again later, or sign up with email instead."

                    case .userDisabled  :
                        self.confirmationCodeErrorLabel?.text = "Account disabled. Please contact support."

                    case .internalError  :
                        print("internalError")
                        self.confirmationCodeErrorLabel?.text = "We've run into a problem."

                    case .invalidAppCredential  :
                        print("invalidAppCredential")
                        
                    case .invalidUserToken  :
                        print("invalidUserToken")
                        
                    case .sessionExpired :
                        print("sessionExpired")
                        self.confirmationCodeErrorLabel?.text = "Code Expired. Please Request a New Code."

                    default:
                        print("default")
                        self.confirmationCodeErrorLabel?.text = "Error. Try Again."
                    }
                }

                return
            } else {
                //print("phoneAuthSuccessful")
                self.veriCodeDidReturnErrorCode = false
//                self.activityIndicatorView.stopAnimating()
//                self.ref_AuthTracker.emailAuthSource = false
//                self.ref_AuthTracker.phoneAuthSource = true
//                self.ref_AuthTracker.performBatchUpdates_Security(withEmail: "nil", withTermsBool: "true")
                
                if let user = Auth.auth().currentUser {
                    self.checkForExistingUserInstance(withUID: user.uid)
                }
//                self.performSegue(withIdentifier: "authToTrust", sender: self)
            }
        }
    }
    
    
    
    

    var phoneNumberString_10 = ""
    
    var phoneNumberDidReturnError = false
    
    func authPhone_Step1() {
        //The I
        guard let phoneNumberString = self.phoneNumberTextField!.text else { return }
        guard phoneNumberString.count >= 10 else { return }
        self.phoneNumberString_10 = phoneNumberString
        let finalPhoneString = "+1" + phoneNumberString
        
        //Request Notifications
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .notDetermined {
                
                //Present User with Notification Request 1st time
                self.present(self.notificationsPrompt!, animated: true)
                
                return
                
            } else if settings.authorizationStatus == .denied {
                //When User Has Previously Denied Request, prompt to Settings
                self.present(self.notificationsPrompt_toSettings!, animated: true)
                
                return
                
            } else if settings.authorizationStatus == .authorized {
                //Authorized -> Continue
                
                if self.backgroundRefreshIsAvailable == true {
                    DispatchQueue.main.async {
                        self.authPhone_Step1_Helper()
                    }
                } else {
                    self.present(self.backgroundRefresh_Alert!, animated: true)
                }
                
//                if settings .alertSetting == .disabled {
//
//                    self.present(self.notificationsPrompt_toSettings!, animated: true)
//
//                    return
//                }
//
//                if settings .alertSetting == .enabled {
//                    self.authPhone_Step1_Helper()
//                }
                
            }
        }
    }
    
    var backgroundRefreshIsAvailable = false 
    
    func retrieveBackgroundRefresh_Status() -> Bool {
        switch UIApplication.shared.backgroundRefreshStatus {
        case .available:
            //print("Refresh available")
            self.backgroundRefreshIsAvailable = true
            return true
        case .denied:
            //print("Refresh denied")
            self.backgroundRefreshIsAvailable = false
            return false

        case .restricted:
            //print("Refresh restricted")
            self.backgroundRefreshIsAvailable = false
            return true
        }
    }
    
    func authPhone_Step1_Helper() {
        
        guard let phoneNumberString = self.phoneNumberTextField!.text else { return }
        guard phoneNumberString.count >= 10 else { return }
        self.phoneNumberString_10 = phoneNumberString
        let finalPhoneString = "+1" + phoneNumberString
        
        if self.retrieveBackgroundRefresh_Status() == true {
            //resume
            //print("retrieveBackgroundRefresh_Status is Active")
        } else {
            //print("retrieveBackgroundRefresh_Status NOT Active")
            
            
            //
            return
        }
        
        self.activityIndicatorView.startAnimating()
        
        //disable ancillary views
        self.continueButton.isUserInteractionEnabled = false
        self.tapFrame.isUserInteractionEnabled = false
        
        PhoneAuthProvider.provider().verifyPhoneNumber(finalPhoneString) { (verificationID, error) in
            
            if let error = error {
                
                //
                self.phoneNumberErrorLabel?.isHidden = false
                self.phoneNumberErrorLabel?.text = "Error. Try Again, or Use Email Instead."
                self.activityIndicatorView.stopAnimating()
                self.tapFrame.isUserInteractionEnabled = true
                
                //reenable views on error
                self.tapFrame.isUserInteractionEnabled = true
                self.phoneNumberTextField.isUserInteractionEnabled = true
                self.phoneNumberDidReturnError = true
                
                if let errorCode = AuthErrorCode(rawValue: error._code) {
                    switch errorCode {
                    case .captchaCheckFailed :
                        
                        print("captchaCheckFailed")
                        
                    case .invalidPhoneNumber :
                        print("invalidPhoneNumber")
                        self.phoneNumberErrorLabel?.text = "Invalid Number. Please Try Again."
                        
                    case .missingPhoneNumber :
                        print("missingPhoneNumber")
                        
                    case .quotaExceeded :
                        print("quotaExceeded")
                        
                    case .userDisabled  :
                        print("userDisabled")
                        self.phoneNumberErrorLabel?.text = "Disabled account. Please contact support."
                        
                    case .credentialAlreadyInUse  :
                        print("credentialAlreadyInUse")
                        self.phoneNumberErrorLabel?.isHidden = false
                        self.phoneNumberErrorLabel?.text = "Number Already in Use. Try Again."
                        
                    case .internalError  :
                        print("internalError")
                        self.phoneNumberErrorLabel?.text = "We've run into a problem. Try again later."
                        
                    case .invalidAppCredential  :
                        print("invalidAppCredential")
                        
                    case .invalidUserToken  :
                        print("invalidUserToken")
                        
                    case .expiredActionCode  :
                        print("expiredActionCode")
                        
                    case .tooManyRequests  :
                        print("tooManyRequests")
                        self.phoneNumberErrorLabel?.text = "Trials exceeded. Try again later."
                        
                    default:
                        //print("default")
                        self.phoneNumberErrorLabel?.isHidden = false
                        self.phoneNumberErrorLabel?.text = "Error. Try Again, or Use Email Instead."
                    }
                    
                    self.activityIndicatorView.stopAnimating()
                    
                    
                } else {
                    
                    
                }
                return
            } else {
                //print("phoneAUthSuccess")
                self.phoneNumberDidReturnError = false
                self.phoneNumberErrorLabel?.isHidden = true
                
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                self.activityIndicatorView.stopAnimating()
                self.slideConfirmationViews(toLeft: true)
                //                self.switchToConfirmationUI()
                //                self.performSegue(withIdentifier: "toConfirmationCode", sender: self)
            }
        }
    }
    
    var nowInConfirmationScene = false
    var toggleAuthShouldGoBackToPhone = false
    
    func slideConfirmationViews(toLeft: Bool) {
    
        if toLeft == true {
            
            //SLIDE VERICODE VIEW TO VISIBLE

            self.nowInConfirmationScene = true
            
            self.titleLabel.text = "Confirm Your Code"

            let sWidth = -(UIScreen.main.bounds.width)
            
            //auth views
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
//                screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
                
                self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
                
                self.emailTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.phoneNumberTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
                
                self.emailLineView.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.TOSTextView.transform = CGAffineTransform(translationX: sWidth, y: 0)
                
                self.emailErrorLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
            }) { (true) in
                
                if self.authIsEmail == false {
                    
//                    self.phoneNumberTextField.text = ""
                    
                }
                
            }
            
            //confirmation views
            
            self.confirmationCodeHeader.isHidden = false
            self.confirmationCodeTextField.isHidden = false
            self.confirmationCodeLineView.isHidden = false
            self.confirmationCodeHeader.text = "Enter the Code We Sent to " + phoneNumberString_10
            self.confirmationCodeTextField.becomeFirstResponder()
            self.confirmationCodeLineView.image = UIImage(named: "BlueLine")!
   
            //
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                //                screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeLineView.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeErrorLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
            }, completion: nil)
            
            //button
            self.toggleAuthShouldGoBackToPhone = true
            self.toggleAuth?.isHidden = false
            self.toggleAuth?.text = "Go Back"
            self.tapFrame.isUserInteractionEnabled = true
        
        } else if toLeft == false {
            
            self.titleLabel.text = "Create a New Account"

            //
            self.emailTitleHeader.text = "Sign Up Using Phone (U.S. Only)"
            self.toggleAuthShouldGoBackToPhone = false
            self.toggleAuth?.text = "Sign Up Using Email Instead"
            self.nowInConfirmationScene = false
            self.tapFrame.isUserInteractionEnabled = true
            self.toggleAuth?.isHidden = false
            
            //SLIDE AUTH VIEW TO VISIBLE
            
            let sWidth = (UIScreen.main.bounds.width)

            //auth views
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                //                screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
                
                self.emailTitleHeader.transform = CGAffineTransform(translationX: 0, y: 0)
                
                self.emailTextField.transform = CGAffineTransform(translationX: 0, y: 0)
                self.phoneNumberTextField.transform = CGAffineTransform(translationX: 0, y: 0)
                
                self.emailLineView.transform = CGAffineTransform(translationX: 0, y: 0)
                self.TOSTextView.transform = CGAffineTransform(translationX: 0, y: 0)
                
                self.emailErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
                self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            }) { (true) in
                //print("finalFramePos", self.emailTextField.frame.minX)
            }
            
            //confirmation views
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                //                screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeLineView.transform = CGAffineTransform(translationX: sWidth, y: 0)
                self.confirmationCodeErrorLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
            }) { (true) in
                self.confirmationCodeHeader.isHidden = true
                self.confirmationCodeTextField.isHidden = true
                self.confirmationCodeLineView.isHidden = true
                self.confirmationCodeTextField.resignFirstResponder()
                self.confirmationCodeErrorLabel?.isHidden = true
                self.confirmationCodeLineView.image = UIImage(named: "GrayLine")!
                
                self.veriCodeDidReturnErrorCode = false
                self.confirmationCodeTextField.text = ""
            }
            
        }

    }
        
        
    
    
    
    //    func slideConfirmationView() {
    //
    //        let sWidth = -(UIScreen.main.bounds.width)
    //
    //        let screenTitle = self.screenTitleLabel!
    //        let choiceCopy = self.choiceCopyTextView!
    //        let photoPrivacyButton = self.securityTextView!
    //        let toggler = self.profileTogglerView!
    //        let label = self.visibilityLabel!
    //
    //        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
    //            //photo to left
    //            screenTitle.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            choiceCopy.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            photoPrivacyButton.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            toggler.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            label.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //
    //            if let blurControl = self.bView1 {
    //                blurControl.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            }
    //
    //            //name to left
    //            self.privacyScreenTitle!.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            self.nameChoiceTextView!.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            self.nameLabel!.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            self.showHideButton!.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //            self.changeCopy!.transform = CGAffineTransform(translationX: sWidth, y: 0)
    //
    //        }, completion: nil)
    //    }
    
    func switchToConfirmationUI() {
        
//        self.emailTextField.isHidden = true
//        self.phoneNumberTextField.isHidden = true
//
//        self.addConfirmationCodeTextField()
//        self.confirmationCodeTextField.becomeFirstResponder()
//
//        self.titleLabel.text = "Confirm Your Code"
//        self.TOSTextView.removeFromSuperview()
//        self.emailTitleHeader.text = "Enter the confirmation code we sent to " + phoneNumberString_10
//
//        self.toggleAuth!.isHidden = true
//        self.tapFrame.isHidden = true
//
//        self.continueButton!.setTitle("Confirm", for: .normal)
//        self.continueButton.backgroundColor = self.inactiveGrayColor
//        self.continueButton.isUserInteractionEnabled = false
//
//        self.nowInConfirmationScene = true
    }

    var activityIndicatorView: UIActivityIndicatorView!
    
    func addActivityIndicator() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 7/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = ((gridUnitWidth - aW) * 0.5) - 2
        let aY = (gridUnitWidth - aW) * 0.5
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        //        activityIndicatorView.color = UIColor.white
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        let centerX = sWidth * 0.5
        let centerY = (5 * superInsetHeight) + gridUnitHeight
        let centerPosition = CGPoint(x: centerX, y: centerY)
        
        //        self.continueButton2!.addSubview(activityIndicatorView)
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
        
        self.activityIndicatorView!.layer.position = centerPosition
        
        //        activityIndicatorView.startAnimating()
    }
    
//    
//    func positionStackView() {
//        //adjust height
//        let bottomInsetRatio = 67/736 as CGFloat
//        let screenHeight = screen.height
//        let stackViewHeight = buttonStackView.frame.height
//        let inset = screenHeight * bottomInsetRatio
//        self.buttonStackView.frame.origin.y = screenHeight - inset - stackViewHeight + 10.0
//        
//        //adjust center
//        let screenWidth = screen.width
//        let stackViewWidth = buttonStackView.frame.width
//        self.buttonStackView.frame.origin.x = (screenWidth/2) - (stackViewWidth/2)
//    }
    
    //MARK: Button View Repositioning
    
        //MARK: Button View Repositioning/Properties
    
        let screen = UIScreen.main.bounds
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }

    @objc func keyboardWillShow(_ notification:Notification) {
//        let screenHeight = screen.size.height
//        let stackViewHeight = self.buttonStackView.frame.height
//        let keyboardHeight = getKeyboardHeight(notification)
//        let risingStackViewPosition = screenHeight - keyboardHeight - stackViewHeight - spaceToKeyboard
//        self.buttonStackView.frame.origin.y = risingStackViewPosition
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        let spaceToKeyboard = 1.5 * gridUnitHeight

        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton.frame.height
        let keyboardHeight = getKeyboardHeight(notification)
        let risingStackViewPosition = screenHeight - keyboardHeight - buttonHeight - spaceToKeyboard
        self.continueButton.frame.origin.y = risingStackViewPosition

//        self.toggleAuth?.frame.origin.y = screenHeight - keyboardHeight - bottomButtonPadding
        self.toggleAuth?.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        self.tapFrame.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
//        let screenHeight = screen.size.height
//        let stackViewHeight = self.buttonStackView.frame.height
//        let restingStackViewPosition = screenHeight - stackViewHeight - spaceToKeyboard
//        self.buttonStackView.frame.origin.y = restingStackViewPosition
        
        if self.authIsEmail == true {
            if self.emailTextField.text == "" {
                self.continueButton.backgroundColor = inactiveGrayColor
                self.emailLineView!.image = UIImage(named: "GrayLine")!
                self.emailErrorLabel!.isHidden = true
            }
        }
        
        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton.frame.height
        let restingStackViewPosition = screenHeight - superInsetHeight!
        self.continueButton.frame.origin.y = restingStackViewPosition
        
        self.toggleAuth?.frame.origin.y = screenHeight - bottomButtonPadding - self.toggleAuth!.frame.height
        self.tapFrame?.frame.origin.y = screenHeight - bottomButtonPadding - self.toggleAuth!.frame.height

//        self.toggleAuth?.frame.origin.y = restingStackViewPosition
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        let keyBoardHeight = keyboardSize.cgRectValue.height
        self.keyBoardHeight = keyBoardHeight
        return keyBoardHeight
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        buttonStackView.superview!.layoutIfNeeded()
//        buttonStackView.superview!.setNeedsLayout()
        // Now modify bottomView's frame here
    }
    
    //
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        //should also add condition for keyboardwillhide here?
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }

    //MARK: Email Formatter
    func validateEmail(_ emailString: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        let trueFalseResult = emailTest.evaluate(with: emailString)
        return trueFalseResult
    }
    
    //MARK: Get & Pass Info
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toWebView" {
            self.unwindFromWebView = true
            let destinationController = segue.destination as! TOAViewController
            if self.privacyPolicy == true {
                destinationController.withPrivacyPolicy = true
            } else {
                destinationController.withPrivacyPolicy = false
            }
            
        } else if segue.identifier == "backToWelcome" {
            let destinationController = segue.destination as! WelcomeViewController
            
        } else if segue.identifier == "SecurityViewController"  {
            let destinationController = segue.destination as! SecurityViewController
            //print("valid user email passed is \(self.validatedUserEmailString)")
            destinationController.verifiedEmail = self.validatedUserEmailString
            destinationController.bottomPadding = self.bottomPadding!
            destinationController.ref_AuthTracker = self.ref_AuthTracker
            
        } else if segue.identifier == "authToTrust" {
            
            let destinationController = segue.destination as! TrustViewController
            destinationController.ref_AuthTracker = self.ref_AuthTracker
            
        }
        
    }
    
    //MARK: Other UI Adjustments

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.emailTextField {
            if string == "" {
                self.emailErrorLabel!.isHidden = true
            }
            
            if string == " " {
                return false
            } else {
                //FBCondition
                let characterCountLimit = 80
                let startingLength = textField.text?.count ?? 0
                let lengthToAdd = string.count
                let lengthToReplace = range.length
                let stringLength = startingLength + lengthToAdd - lengthToReplace
                if stringLength >= 1 {
                    //                sbContinueButton.setImage(UIImage(named: "GreenButton"), for: UIControlState.normal)
                    //                sbContinueButton.isEnabled = true
                    self.continueButton.backgroundColor = activeGreenColor
                    self.emailErrorLabel!.isHidden = true
                    self.continueButton.isUserInteractionEnabled = true

                } else {
                    //                sbContinueButton.setImage(UIImage(named: "GrayButton"), for: UIControlState.normal)
                    //                sbContinueButton.isEnabled = false
                    self.continueButton.backgroundColor = inactiveGrayColor
                    self.continueButton.isUserInteractionEnabled = false
                }
                return stringLength <= characterCountLimit
            }
        }
        
        if textField == self.phoneNumberTextField {
            
            var numberArr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ""]
            
            if string == " " {
                //print("1phoneString,", self.phoneNumberTextField.text) //returning corr length
                self.phoneNumberErrorLabel?.isHidden = true
                return false
            } else if !numberArr.contains(string)  {
                return false
            } else {
                //FBCondition
                self.phoneNumberErrorLabel?.isHidden = true
                let characterCountLimit = 10
                let startingLength = textField.text?.count ?? 0
                let lengthToAdd = string.count
                let lengthToReplace = range.length
                let stringLength = startingLength + lengthToAdd - lengthToReplace
                if stringLength >= 10 {
                    self.continueButton.backgroundColor = activeGreenColor
                    self.emailErrorLabel!.isHidden = true
                    self.continueButton.isUserInteractionEnabled = true
                    //print("2phoneString,", self.phoneNumberTextField.text)

                } else {
                    //                sbContinueButton.setImage(UIImage(named: "GrayButton"), for: UIControlState.normal)
                    //                sbContinueButton.isEnabled = false
                    self.continueButton.backgroundColor = inactiveGrayColor
                    self.continueButton.isUserInteractionEnabled = false

                }
                return stringLength <= characterCountLimit
            }
            self.phoneNumberErrorLabel?.isHidden = true
        }
        
        if textField == self.confirmationCodeTextField {
            if string == " " {
                self.confirmationCodeErrorLabel?.isHidden = true
                return false
            } else {
                //FBCondition
    
                let characterCountLimit = 6
                let startingLength = textField.text?.count ?? 0
                let lengthToAdd = string.count
                let lengthToReplace = range.length
                let stringLength = startingLength + lengthToAdd - lengthToReplace
                if stringLength >= 6 {
                    self.continueButton.backgroundColor = activeGreenColor
                    self.continueButton.isUserInteractionEnabled = true
                } else {
                    self.continueButton.backgroundColor = inactiveGrayColor
                    self.continueButton.isUserInteractionEnabled = false
                }
                self.confirmationCodeErrorLabel?.isHidden = true
                return stringLength <= characterCountLimit
            }
        }
     
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            self.emailErrorLabel?.isHidden = true
        } else if textField == self.phoneNumberTextField {
            self.phoneNumberErrorLabel?.isHidden = true
        } else if textField == self.confirmationCodeTextField {
            self.confirmationCodeErrorLabel?.isHidden = true
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        /* if textField == mobileTextField || textField == emailTextField {
         sbContinueButton.setImage(UIImage(named: "GreenButton"), for: UIControlState.normal)
         } */
        
        UIView.animate(withDuration: 0.15, delay: 0, options: .transitionCrossDissolve, animations: {
            self.TOSTextView.alpha = 1
        }) { (true) in
            self.TOSTextView.isHidden = false
        }
        
        
        if textField == mobileTextField {
            mobileLineView.image = UIImage(named: "BlueLine")
            emailLineView.image = UIImage(named: "GrayLine")
            emailTextField.text = ""
        }
        
        if textField == emailTextField {
            mobileLineView.image = UIImage(named: "GrayLine")
            emailLineView.image = UIImage(named: "BlueLine")
            mobileTextField.text = ""
        }
        
        if textField == confirmationCodeLineView {
            confirmationCodeLineView.image = UIImage(named: "BlueLine")!
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.emailTextField.isFirstResponder == true {
            self.emailTextField.resignFirstResponder()
            return true
        } else {
            return false
        }
    }
    
//    confirmationCodeLineView.image = UIImage(named: "BlueLine")!
//    confirmationCodeLineView.image = UIImage(named: "GrayLine")!
    
    @IBAction func TapAreaOutside(_ sender: Any) {
        
        if self.nowInConfirmationScene == true {
            
            if self.confirmationCodeTextField.isFirstResponder {
                self.confirmationCodeTextField.resignFirstResponder()
                confirmationCodeLineView.image = UIImage(named: "GrayLine")!
                if let code = self.confirmationCodeTextField.text {
                    if code.count >= 6 {
                        if self.veriCodeDidReturnErrorCode == false {
                            self.continueButton.isUserInteractionEnabled = true
                            self.continueButton.backgroundColor = self.activeGreenColor
                        } else {
                            self.confirmationCodeErrorLabel?.isHidden = true
                            self.confirmationCodeTextField.text = ""
                            self.veriCodeDidReturnErrorCode = false
                            self.continueButton.isUserInteractionEnabled = false
                            self.continueButton.backgroundColor = self.inactiveGrayColor
                        }
                    }
                }
            } else {
                if self.veriCodeDidReturnErrorCode == true {
                    self.confirmationCodeTextField.becomeFirstResponder()
                    self.confirmationCodeLineView.image = UIImage(named: "BlueLine")!
                    self.confirmationCodeTextField?.text = ""
                    self.confirmationCodeErrorLabel?.isHidden = true
                    self.continueButton.isUserInteractionEnabled = false
                    self.continueButton.backgroundColor = self.inactiveGrayColor
                    self.veriCodeDidReturnErrorCode = false
                } else {
                   self.confirmationCodeTextField.becomeFirstResponder()
                    self.confirmationCodeLineView.image = UIImage(named: "BlueLine")!

                }
            }
            
            return
        }
        
        if authIsEmail == true {
            if mobileTextField.isFirstResponder || emailTextField.isFirstResponder {
                mobileTextField.resignFirstResponder()
                emailTextField.resignFirstResponder()

            } else {
                emailTextField.becomeFirstResponder()
            }
            
        } else if authIsEmail == false {
            
            //Using PhoneAuth
            
            if self.phoneNumberTextField.isFirstResponder {
                self.phoneNumberTextField.resignFirstResponder()
                if let numberString = self.phoneNumberTextField!.text {
                    if numberString.count >= 10 {
                        self.continueButton.backgroundColor = self.activeGreenColor
                        self.continueButton.isUserInteractionEnabled = true
                    } else {
                        self.continueButton.isUserInteractionEnabled = false
                        self.continueButton.backgroundColor = self.inactiveGrayColor
                    }
                }
            } else {
                if self.phoneNumberDidReturnError == false {
                    //no error
                    if let numberString = self.phoneNumberTextField!.text {
                        if numberString.count >= 10 {
                            self.continueButton.backgroundColor = self.activeGreenColor
                            self.continueButton.isUserInteractionEnabled = true
                        } else {
                            self.continueButton.isUserInteractionEnabled = false
                            self.continueButton.backgroundColor = self.inactiveGrayColor
                        }
                    }
                    self.continueButton.isUserInteractionEnabled = true
                    self.continueButton.backgroundColor = self.activeGreenColor
                } else {
                    //yes error
                    self.phoneNumberErrorLabel?.isHidden = true
                    self.phoneNumberTextField.text = ""
                    self.phoneNumberDidReturnError = false
                    self.continueButton.isUserInteractionEnabled = false
                    self.continueButton.backgroundColor = self.inactiveGrayColor
                }
                
                self.phoneNumberTextField.becomeFirstResponder()
                if let numberString = self.phoneNumberTextField!.text {
                    if numberString.count >= 10 {
                        self.continueButton.backgroundColor = self.activeGreenColor
                        self.continueButton.isUserInteractionEnabled = true
                    } else {
                        self.continueButton.isUserInteractionEnabled = false
                        self.continueButton.backgroundColor = self.inactiveGrayColor
                    }
                }
            }
        }
    }
    
    var notificationsPrompt: UIAlertController?
    
    func addNotificationRequiredAlert() {
        let alertTitle = "One-Time SMS Notification"
//        let alertMessage = "Ximo needs permission to send notifications to your device so we can send you an SMS confirmation code." + "\n" + "You will only receive notifications from Ximo when you need a verification code to sign in again in the future."
        let alertMessage = "Please enable notifications so we can send you an SMS confirmation code." + "\n" + "\n" + "You will only receive notifications from us again when you need to sign in with a verification code in future."

        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let continueAction = UIAlertAction(title: "Continue", style: .default) { (action) in

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            //Apple: The first time your app makes this authorization request, the system prompts the user to grant or deny the request and records the user’s response. Subsequent authorization requests don't prompt the user.
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (authResult, error) in
                if error != nil {
                    //print("notification error", error)
                    
                } else {
                    if authResult == true {
                        //print("permission granted", authResult)
                        if self.backgroundRefreshIsAvailable == true {
                            DispatchQueue.main.async {
                                self.authPhone_Step1_Helper()
                            }
                        } else {
                            self.present(self.backgroundRefresh_Alert!, animated: true)
                        }
                    } else {
                        //Denied
                        //print("permission denied", authResult)
                        DispatchQueue.main.async {
                            self.continueButton.isUserInteractionEnabled = true
                            self.continueButton.backgroundColor = self.activeGreenColor
                        }
                    }
                }
            })
            
            self.notificationsPrompt!.dismiss(animated: true, completion: nil)
        }
        
        let goBackAction = UIAlertAction(title: "Go Back", style: .default) { (action) in
            
            self.continueButton.isUserInteractionEnabled = true
            self.continueButton.backgroundColor = self.activeGreenColor
            self.notificationsPrompt!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(goBackAction)
        alert.addAction(continueAction)
        alert.preferredAction = continueAction
        self.notificationsPrompt = alert
    }
    
    var notificationsPrompt_toSettings: UIAlertController?
    
    func addNotificationRequired_toSettings() {
        let alertTitle = "One-Time SMS Notification"

        let alertMessage = "Please enable Notification Banner Alerts so you can receive an SMS confirmation code." + "\n" + "\n" + "You will only receive notifications from us again when you need a verification code in future."
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            self.notificationsPrompt_toSettings!.dismiss(animated: true, completion: nil)
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (result) in })
        }
        
        let goBackAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.notificationsPrompt_toSettings!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(goBackAction)
        alert.addAction(settingsAction)
        alert.preferredAction = settingsAction
        self.notificationsPrompt_toSettings = alert
    }
    
    var backgroundRefresh_Alert: UIAlertController?
    
    func addBackgroundRefreshAlert() {
        
        let alertTitle = "Enable Background Refresh"
        
        let alertMessage = "To receive an SMS confirmation code to your phone number, please enable Background Refresh on your device."

//        let alertMessage = "To sign up using a phone number, please enable Background Refresh on your device." + "\n" + "Go to Settings -> General -> Background App Refresh."
//        let alertMessage = "For phone-number sign-up to work properly on your device, please enable Background Refresh." + "\n" + "To enable Background App Refresh for Ximo: Go to Settings -> General -> Background App Refresh."
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            self.backgroundRefresh_Alert!.dismiss(animated: true, completion: nil)
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (result) in })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.backgroundRefresh_Alert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        alert.addAction(settingsAction)
        alert.preferredAction = settingsAction
        self.backgroundRefresh_Alert = alert
    }
    
    

    //Sign-in User
    
    
    
    //FB Setup
    
    /*
     //You should update your view controller to check for an existing token at load. This eliminates an unnecessary app switch to Facebook if someone already granted permissions to your app:
     func viewDidLoad() {
        super.viewDidLoad()
        if FBSDKAccessToken.current() {
            // User is logged in, do work such as go to next view controller.
        }
    }
     
     // Add this to the header of your file, e.g. in ViewController.m
     // after #import "ViewController.h"
     
     import FBSDKCoreKit
     import FBSDKLoginKit
     
     // Add this to the body
     class ViewController {
     func viewDidLoad() {
     super.viewDidLoad()
     let loginButton = FBSDKLoginButton()
     // Optional: Place the button in the center of your view.
     loginButton.center = view.center
     view.addSubview(loginButton as? UIView ?? UIView())
     }
     }
     
     //ADD TO VIEWDIDLOAD:
    */
    
    //    deinit {
    //        Auth.auth().removeStateDidChangeListener(_authHandle)
    //    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func prepareForUnwindTOA (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    //MARK: Check User Auth
    var uidCheckDidResolve = false
    var gfKeyCheckDidResolve = false
    
    var userGFKeyDoesExist = false
    var userUIDKeyDoesExist = false
    
    var finalUserAuthState = false
    
    func checkForExistingUserInstance(withUID: String) {
        
        self.ref = Database.database().reference()
        
        self.ref.child("users/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("user does exist in DB")
                self.userUIDKeyDoesExist = true
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("user doesNotExist in DB")
                self.userUIDKeyDoesExist = false
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            }
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("error checking snapshot")
            self.userUIDKeyDoesExist = false
            self.uidCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
        
        self.ref.child("userLocationGF/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("GF user does exist in DB")
                self.userGFKeyDoesExist = true
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("GF user doesNotExist in DB")
                self.userGFKeyDoesExist = false
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()
            }
            
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("GF error checking snapshot")
            self.userGFKeyDoesExist = false
            self.gfKeyCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
    }
    
    func returnFinalUserAuthState() {
        
        if self.uidCheckDidResolve == true && self.gfKeyCheckDidResolve == true {
            
            self.activityIndicatorView.stopAnimating()

            //Case 1.
            if self.userUIDKeyDoesExist == false && self.userGFKeyDoesExist == false {
                //No record of user. Proceed with sign-up flow.
                self.ref_AuthTracker.emailAuthSource = false
                self.ref_AuthTracker.phoneAuthSource = true
                self.ref_AuthTracker.performBatchUpdates_Security(withEmail: "nil", withTermsBool: "true")
                
                self.performSegue(withIdentifier: "authToTrust", sender: self)
            }
            
            //Case 2.
            if self.userUIDKeyDoesExist == true && self.userGFKeyDoesExist == false {
                //User Did Attempt to sign up with phone previously, but did not complete account creation - Proceed with signup flow
                self.ref_AuthTracker.emailAuthSource = false
                self.ref_AuthTracker.phoneAuthSource = true
                self.ref_AuthTracker.performBatchUpdates_Security(withEmail: "nil", withTermsBool: "true")
                
                self.performSegue(withIdentifier: "authToTrust", sender: self)
            }
            
            //Case 3.
            if self.userUIDKeyDoesExist == true && self.userGFKeyDoesExist == true {
                self.veriCodeDidReturnErrorCode = true
                self.confirmationCodeErrorLabel?.isHidden = false
                self.confirmationCodeErrorLabel?.text = "User record already exists. Please sign in."
                
                self.toggleAuth?.isUserInteractionEnabled = true
                self.confirmationCodeTextField.isUserInteractionEnabled = true
            }
  
        }
    }
    
    
}


