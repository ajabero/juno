//
//  CollectionViewController.swift
//  Juno
//
//  Created by Asaad on 2/10/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseStorage
import GeoFire
import QuartzCore
import GameplayKit

private let reuseIdentifier = "Cell"

class CollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate, UICollectionViewDelegateFlowLayout {
 
    //
    let uuid = UUID()
    var userDictArray = [[String : String]]()
    var userDictArray_UID = [String]()

    //MARK: Class References
    var ref_SelfModelController = SelfModelController()
    
    //MARK: DB setup
    var ref: DatabaseReference!
    var storageRef: StorageReference!
    fileprivate var _swapRefHandle: DatabaseHandle!
    fileprivate var _swapChangesRefHandle: DatabaseHandle!
    fileprivate var _chatRequestsRefHandle: DatabaseHandle!
    fileprivate var _chatRequestChangesRefHandle: DatabaseHandle!

    //MARK: User Profile Info
    var myProfileInfoDict = [String : String]()
    var selfUID: String?
    var selfVisus: String?
    
    var sourceController = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        //print("CVC VIEWDIDLOAD self.userDictArray", self.userDictArray.count)
        self.gridDidLoad = self.ref_SelfModelController.gridDidLoad
        //print("cvc gridDidLoad,", self.gridDidLoad)
        
        self.ref = Database.database().reference()
        storageRef = Storage.storage().reference()
        self.selfUID = Auth.auth().currentUser!.uid

        //NEW
        self.getAllUsersOnline()

        //add supplementary views
        self.addActivityIndicator()
        self.addImageLoader()
        self.addLocationAlert()
        
        //configure CollectionViewLayout
        self.configureFlowLayout()
        self.overrideLayoutConstraints()

        self.collectionView.alwaysBounceVertical = true
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        //
        self.addDiscoveryGridIcon()
        self.addChatListNavButton()
        self.addHomeScreenNavButton()
        
        //
        self.gridDidLoad = self.ref_SelfModelController.gridDidLoad
        if self.gridDidLoad == false {
            self.activityIndicatorView?.startAnimating()
        }
        
        self.ref_SelfModelController.observeOrderQuery()
        self.ref_SelfModelController.checkForExistingUserInstance(withUID: Auth.auth().currentUser!.uid)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.observeFinalAuthState), name: NSNotification.Name(rawValue: "observeFinalAuthState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.forceLogout_IncompleteSelfInfo), name: NSNotification.Name(rawValue: "forceLogout_IncompleteSelfInfo"), object: nil)

//        NotificationCenter.default.post(name: Notification.Name(rawValue: "observeFinalAuthState"), object: self, userInfo: userInfoDict)

        //print("CVC uid is,", Auth.auth().currentUser!.uid)
    }
    
    @objc func observeFinalAuthState(_ notification: Notification) {
        guard notification.name.rawValue == "observeFinalAuthState" else { return }
  
        if let didAdmitUser = notification.userInfo?["didAdmitUser"] as? String {
            if didAdmitUser == "true" {
                self.viewDidLoad_Helper()
            } else {
                self.forceLogout()
            }
        }
    }
    
    @objc func forceLogout_IncompleteSelfInfo(_ notification: Notification) {
        guard notification.name.rawValue == "forceLogout_IncompleteSelfInfo" else { return }
        self.forceLogout()
    }
    
    var isOnlineDict = [String : String]()
    var userIsOnlineDict : [String: [String : String]]?

    var allUsersOnline = [String]() { didSet {
//        print("allUsersOnline count", allUsersOnline.count)
    }}
    
    //Make sure this loads before users in proximity loads
    //Don't create dependency between this and sortUsers
    //Make sure this is only done once - that it doesnt keep reloading the cvc
    
    func getAllUsersOnline() {
        
        guard let user = Auth.auth().currentUser else { return }
        
        self.ref.child("globalUserPresence").queryOrdered(byChild: "isOnline").queryEqual(toValue: "true").observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? [String: [String : String]] else { return }
//            print("useronline snapshot, ", value)
            for userId in value.keys {
                if userId != user.uid {
                    self.allUsersOnline.append(userId)
                }
            }
        }
    }
    
    
//    {
//    ZaGkPNeoc1ScCjvK01opJoR0WFe2 =     {
//    isOnline = true;
//    };
//    ZwxReKuGXAPqvq0hrYRdQtUPMPu1 =     {
//    isOnline = true;
//    };
//    kE2QDCJlMqfBBvcmqc4vRGU6IRg2 =     {
//    isOnline = true;
//    };
//    zmAfTZZoB5e1fn8RVxhDiifxDdN2 =     {
//    isOnline = true;
//    };
//    }
    
    func viewDidLoad_Helper() {
        //initialize observers
        //Note: blocks are observed before popArrDict beause: (1) If obs prepares a blocked key BEFORE popFiresParallelKey, popWillNotAppend; (2) If obs prepares a blocked key AFTER popFiresParKey, obsWillRemoveItemFromDict
        

        
        self.observeUnlockedUsers() //POSITION_INCODEQUEUE_AFTER
        self.observeOutgoingBlockRequests() //reactivate -> Moved to popArrDict
        self.observeIncomingBlockRequests() //reactivate -> //
        self.observeIncomingDismissedRequests() //reactivate -> //
        //Note: Abscence of obsOutDism() since they are treated they same as blockedUsers
        
        
        //Location
        self.configureLocation()
        
        if self.ref_SelfModelController.selfProfileInfoDidLoad == false {
            self.loadSelfProfileInfo()
            self.ref_SelfModelController.firebaseConnect()
        } else {
            self.selfVisus = self.ref_SelfModelController.myProfileInfoDict["selfVisus"]!
        }
        
        //ATTN: May not need to call these methods if you're reloadingCVCfromMemory. Test & see
        self.observeActiveMessageThreads()
        self.observeActiveChatRequests() //reactivate
        
        //Note: observeActiveSwapRequests() CALL moved to
        self.observeActiveSwapRequests(withSourceCall: "viewDidLoad") //-> uidKeysInLastMessagesDict_Array didSet
        
//        self.observeUnlockedUsers() //POSITION_INCODEQUEUE_BEFORE
        
        //new: observing presence
        //Note: This one observes self's presence vs. observeGlobalUsersPresence() observes all OTH's presence
        self.setObserveSelfPresenceInDB()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.clearSwapAlert_FromChatList), name: NSNotification.Name(rawValue: "clearSwapAlertForCVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.setSwapAlert_FromChatList), name: NSNotification.Name(rawValue: "setSwapAlertForCVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.setChatAlert_FromChatList), name: NSNotification.Name(rawValue: "setChatAlertForCVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.clearChatAlert_FromChatList), name: NSNotification.Name(rawValue: "clearChatAlertForCVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.manualClearChatAlertForCVC), name: NSNotification.Name(rawValue: "manualClearChatAlertForCVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.convertIncomingSwapToChat(_:)), name: NSNotification.Name(rawValue: "convertIncomingSwapToChat"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.castOutgoingRevealtoGenChat_onPubToPriv(_:)), name: NSNotification.Name(rawValue: "castOutgoingRevealtoGenChat_onPubToPriv"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.castOutgoingSwaptoGenChat_onPrivToPub(_:)), name: NSNotification.Name(rawValue: "castOutgoingSwaptoGenChat_onPrivToPub"), object: nil)
    }
    
    @objc func manualClearChatAlertForCVC(_ notification: Notification) {
        guard notification.name.rawValue == "manualClearChatAlertForCVC" else { return }
        self.ref_SelfModelController.chatAlertIsActive = false
        self.ref_SelfModelController.swapAlertIsActive = false
//        self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
        self.blueDotView?.isHidden = true
    }
    
    @objc func setChatAlert_FromChatList(_ notification: Notification) {
        guard notification.name.rawValue == "setChatAlertForCVC" else { return }
        self.setChatNotificationAlert()
    }
    
    @objc func clearChatAlert_FromChatList(_ notification: Notification) {
        guard notification.name.rawValue == "clearChatAlertForCVC" else { return }
        self.clearChatNotificationAlert()
    }
    
    @objc func setSwapAlert_FromChatList(_ notification: Notification) {
        guard notification.name.rawValue == "setSwapAlertForCVC" else { return }
        self.setSwapNotificationAlert()
    }
    
    @objc func clearSwapAlert_FromChatList(_ notification: Notification) {
        guard notification.name.rawValue == "clearSwapAlertForCVC" else { return }
        self.clearSwapNotificationAlert()
    }
    
    var shouldClearNewChatAlert = false
    
    override func viewWillAppear(_ animated: Bool) {
        //print("ViewWillAppear called")
        
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        self.statusBarHeight = UIApplication.shared.statusBarFrame.height

        if self.ref_SelfModelController.cvcShouldReloadView == true {
            self.resetAllValues()
            self.gridDidLoad = false
            //print("self.userDictArray_UID", self.userDictArray_UID)
            self.collectionView.reloadSections([0])
            self.addActivityIndicator()
            self.activityIndicatorView?.startAnimating()
            self.configureLocation()
            
            return
        } else {
            
        }
        
        if self.shouldClearNewChatAlert == true {
            self.clearChatNotificationAlert()
        }
        
        self.gridDidLoad = self.ref_SelfModelController.gridDidLoad
        //print("vwa gridDidLoad", self.gridDidLoad)
        
        //MARK: BIG CHANGE HAPPENED - MOVING ALL METHOD OBSERVERS.CHANGED FROM viewDidLoad TO viewDidAppear() - so that these methods will be called and update information properly whenever unwind segue is performed
        self.observeActiveMessageThreadChanges()
        self.observeChatRequestStatusChanges() //reactiveate
        self.observeSwapRequestStatusChanges()
        
        //print("CVC inherit self.chatListObjects",self.chatListObjects.count)
        
        //print("presentingConCVC,", self.presentingViewController!)
        
        //ATTN: Make sure this isn't called more than once if user enters background, locks phone, presents alert, ...
        if self.sourceController == "ChatListViewController" {
            
            self.observeActiveSwapRequests(withSourceCall: "VWA")
            self.manualClearChatListView()
        
        }
    }
    
    func manualClearChatListView() {
        self.ref_SelfModelController.chatAlertIsActive = false
        self.ref_SelfModelController.swapAlertIsActive = false
//        self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal) //new UI Logic: clear Grid whenever segue back.
        self.blueDotView?.isHidden = true
    }
    
    func resetAllValues() {
        self.userDictArray.removeAll()
        self.uidinProximityRawArray.removeAll()
        self.sortedUIDinProximityArray.removeAll()

        self.uniqueUIDsInProximity_Array.removeAll()
        self.userDictArray_UID.removeAll()
        
        self.CLLocationForUID.removeAll()
        self.distanceToByUID.removeAll()
        
        self.minimumUserCountThreshold = 0
        
        self.tierQueryRadius = Double(200)
        self.numberOfItems_Tier0 = 0

        self.didSetCount_Tier0 = false
        self.didQueryMaxRadiusPossible = false
        self.didObserveKeysReadyForRefresh = false
        
        self.keysEnteredCount = 0
        self.popArrCounter = 0
    }

//    override func viewDidAppear(_ animated: Bool) {
//        //print("viewDidAppear self.userDictArray", self.userDictArray.count)
//    }
//
//    override func viewDidLayoutSubviews() {
//        //print("viewDidLayoutSubviews self.userDictArray", self.userDictArray.count)
//    }

    //MARK: Drawing Functions
    var discoverGridNavButton: UIButton?
    var chatListNavButton: UIButton?
    var homeScreenNavButton: UIButton?
    
    @IBOutlet weak var gridTopConstraint: NSLayoutConstraint!

    func configureFlowLayout() {
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 9
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 9, 0, 9)
        flowLayout.itemSize = CGSize(width: 126, height: 182)
    }
    
    func overrideLayoutConstraints() {
        let sHeight = UIScreen.main.bounds.height
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        
        let absoluteNavSpaceRatio = 56 / 734 as CGFloat
        var absoluteNavSpace = absoluteNavSpaceRatio * sHeight
        
        if sHeight >= 734 as CGFloat {
            //constrain the navSpace to a fixed amount for iPhoneX
            absoluteNavSpace = 56 as CGFloat
        } else {
            //
        }
        self.gridTopConstraint.constant = statusBarHeight + absoluteNavSpace
    }
    
    func addDiscoveryGridIcon() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        //print("statusBarHeight is", statusBarHeight)
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        let rRatio = 37 / 414 as CGFloat
        //        let navSpaceRatio = 76 / 736 as CGFloat
        //        let navSpaceHeight = sHeight * navSpaceRatio
        //
        //        //
        //        let hW = rRatio * sWidth
        //        let hH = hW
        //        let hX = (sWidth - hW) * 0.5
        //        let hY = sBarH + ((navSpaceHeight - sBarH - hH) * 0.5)
        
        //header frame properties
//        let lW = 3 * gridUnitWidth
//        let lH = 35 as CGFloat
//        let lX = (sWidth - lW) * 0.5
//        let titleInsetFromBottom = ((self.navSpaceLayer!.bounds.height - statusBarHeight) * 0.5) - (lH * 0.5)
//        let lY = statusBarHeight + titleInsetFromBottom
        
        //
        let dW = rRatio * sWidth
        let dH = dW
        let dX = (sWidth - dW) * 0.5
//        let dY = statusBarHeight + ((navSpaceHeight - statusBarHeight - dH) * 0.5)
        let insetFromStatusBar = 5 as CGFloat
        let dY = statusBarHeight + insetFromStatusBar
        //init label
        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
        let discoverGridNavButton = UIButton(frame: dF)
        
        discoverGridNavButton.setImage(#imageLiteral(resourceName: "discoveryGridIcon"), for: .normal)
        discoverGridNavButton.addTarget(self, action: #selector(CollectionViewController.gridMainIcon(_:)), for: .touchUpInside)
        discoverGridNavButton.isUserInteractionEnabled = true
        discoverGridNavButton.adjustsImageWhenHighlighted = false

        //init
        self.view.addSubview(discoverGridNavButton)
        self.discoverGridNavButton = discoverGridNavButton
    }
    
    func addChatListNavButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let cW = rRatio * sWidth
        let cH = cW
        let cX = sWidth - marginInset - cW
        var cY = 0 as CGFloat
        let centerAxis = self.discoverGridNavButton!.frame.minY + (self.discoverGridNavButton!.bounds.height * 0.5)
        cY = centerAxis - (cH * 0.5)
        
        //
        let f = CGRect(x: cX, y: cY, width: cW, height: cH)
        let chatListNavButton = UIButton(frame: f)
        chatListNavButton.clipsToBounds = false
//        chatListNavButton.backgroundColor = UIColor.red
        
        if let chatListNavButton = self.chatListNavButton {
            //reuse existing button
        } else {
            chatListNavButton.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
            chatListNavButton.addTarget(self, action: #selector(CollectionViewController.toChatListButton(_:)), for: .touchUpInside)
            
            self.view.addSubview(chatListNavButton)
            self.chatListNavButton = chatListNavButton
            
            //add blue dot
            self.addBlueDotView()
        }
    }
    
    var blueDotView: CAShapeLayer?
    
    func animateBlurDot() {

        let dotView = self.blueDotView!

        dotView.isHidden = false

        let grow = CABasicAnimation(keyPath: "transform.scale")
        grow.fromValue = 0.0
        grow.toValue = 1.0
        grow.duration = 0.25
        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)

        dotView.add(grow, forKey: "grow")

//        let when = DispatchTime.now() + 0.5
//        DispatchQueue.main.asyncAfter(deadline: when) {
//
//        }
    }
    
    func addBlueDotView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 7/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: cR, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        let cLayer = CAShapeLayer()
        let blueDotColor = UIColor(displayP3Red: 12/255, green: 69/255, blue: 124/255, alpha: 1)
        cLayer.path = circularPath.cgPath
        cLayer.fillColor = blueDotColor.cgColor
        cLayer.opacity = 1
//        cLayer.lineWidth = 1.5
//        cLayer.strokeColor = UIColor.white.cgColor
        
//        let dotX = self.chatListNavButton!.frame.maxX
//        let dotY = self.chatListNavButton!.frame.minY
        
        let dotX = self.chatListNavButton!.bounds.width - (0.5 * cR)
        let dotY = 0 as CGFloat + (0.5 * cR)
        
        let cPoint = CGPoint(x: dotX, y: dotY)
        cLayer.position = cPoint
        self.chatListNavButton!.layer.addSublayer(cLayer)
        self.blueDotView = cLayer
        self.blueDotView!.isHidden = true
    }
    
//    func animateGrowRing2() {
//        let ringLayer = self.checkRingLayer!
//        let checkMark = self.checkMarkView!
//
//        ringLayer.isHidden = false
//        checkMark.isHidden = false
//
//        let grow = CABasicAnimation(keyPath: "transform.scale")
//        grow.fromValue = 0.0
//        grow.toValue = 1.0
//        grow.duration = 0.3
//        grow.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
//
//        ringLayer.add(grow, forKey: "grow")
//        checkMark.layer.add(grow, forKey: "grow2")
//
//        let when = DispatchTime.now() + 0.5
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            self.performSegue(withIdentifier: "trustVCtoInfoVC", sender: self)
//        }
//    }
//
    func addHomeScreenNavButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = marginInset
        var hY = 0 as CGFloat
        let centerAxis = self.discoverGridNavButton!.frame.minY + (self.discoverGridNavButton!.bounds.height * 0.5)
        hY = centerAxis - (hH * 0.5)
        
        let pitchBlue = UIColor(displayP3Red: 0, green: 51/255, blue: 83/255, alpha: 1)
        
        //
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let homeScreenNavButton = UIButton(frame: f)
        homeScreenNavButton.setImage(#imageLiteral(resourceName: "meGridIcon"), for: .normal)
        homeScreenNavButton.addTarget(self, action: #selector(CollectionViewController.toHomeButton(_:)), for: .touchUpInside)
        homeScreenNavButton.showsTouchWhenHighlighted = false
        
        self.view.addSubview(homeScreenNavButton)
        self.homeScreenNavButton = homeScreenNavButton
    }

    //MARK: Button Action Methods
    @objc func gridMainIcon(_ sender: UIButton) {
        
//        shuffleArray()
        
        //scroll to top
        let origin = CGPoint(x: 0, y: 0)
        guard self.collectionView!.contentOffset != origin else { return }
        self.collectionView.scrollRectToVisible(UIScreen.main.bounds, animated: true)
        
        print("selfUID", Auth.auth().currentUser!.uid)
        
    }
    
    @objc func toChatListButton(_ sender: UIButton) {
        //print("toChatListButton tap detected")
        //do a final sortChronoDict_byTimeStamp() to be sure that all objects are sorted before traversing VCs
        self.willSegueToChatList = true
        self.sortChronoDict_byTimeStamp()
        
        self.sortChatList_ByLastMessagesArrayOrder()
        
//        self.arrestoMomento()
        self.performSegue(withIdentifier: "gridVCtochatListVC", sender: self)
    }

    @objc func toHomeButton(_ sender: UIButton) {
        //print("toHomeScreen tap detected")
        
        self.performSegue(withIdentifier: "gridVCtoHomeVC", sender: self)
    }
    
    var photoURLStringArray = [String]()
    
    
    
    
    
    
    
    //MARK: Location Configuration......................................................................................................
    
    
    
    
    
    
    
    
    //Location
    let locationManager = CLLocationManager()
    
    var userLocationFromSetup: CLLocation?
    var userLocationFromDB: CLLocation?
    var center: CLLocation?
    var userLongitude = ""
    var userLatitude = ""
    var userTimeStamp = ""

    struct usersinProximityStruct {
        var otherUserID = "a1x"
        var toOtherUserDistance = 0123.4567890
    }
    
    var mirrorLocation: CLLocation!
    
    var mirrorLon = ""
    var mirrorLat = ""
    
    //GeoLocation
    var numberOfUidsInProximity = [String]()
    var uidinProximityRawArray = [usersinProximityStruct]()
    var sortedUIDinProximityArray = [String]()
    
    func configureLocation() {
        locationManager.delegate = self
        
        //Control-flow: if user is accessing Grid for first time from Account Setup, use importedUserLocation as initial value for user location, else, retrieve last location from DB
        
        self.checkLocationPermissions()
        
        if userLocationFromSetup != nil {
            
            if self.gridDidLoad == false {
                self.observeKeysEntered(withSource: "configureLocation")
//                self.observeKeysReady()
            }
       
            
        } else {
            self.getUserLastLocation_FromDB()
            if CLLocationManager.locationServicesEnabled() {
                //print("true, location services are enabled")
                self.setLocationParameters()
                
                //
                locationManager.startUpdatingLocation() //~5
                //
                
                //locationManager.startMonitoringSignificantLocationChanges()
            } else {
                //print("false, location services not enabled")
                self.present(self.locationAlert!, animated: true, completion: nil)
            }
        }
    }
    
    func checkLocationPermissions() {
        switch CLLocationManager.authorizationStatus() {
            
        case .notDetermined:
            // Request when-in-use authorization initially
            locationManager.requestWhenInUseAuthorization()
            //print("CVC location notDetermined")
            
        case .restricted, .denied:
            // Disable location features
            self.present(self.locationAlert!, animated: true, completion: nil)
            //print("CVC location denied")
            
        case .authorizedWhenInUse, .authorizedAlways:
            //
            break
        }
    }
    
    //Note: Why are you getting last location from DB instead of retrieving current location from device? It may be faster to just retrieve location from device. Test & Check.
    func getUserLastLocation_FromDB() {
        guard let user = Auth.auth().currentUser else { return }
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        geoFire.getLocationForKey("\(user.uid)") { (location, error) in
            if (error != nil) {
                //print("An error occurred getting the location for \"firebase-hq\": \(String(describing: error?.localizedDescription))")
            } else if (location != nil) {
                //print("Location for user is [\(String(describing: location?.coordinate.latitude)), \(String(describing: location?.coordinate.longitude))]")
                self.userLocationFromDB = CLLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                //print("Geofire retrieved userLocationFromDB is,", self.userLocationFromDB!)
                
                //
                if self.gridDidLoad == false {
                    self.observeKeysEntered(withSource: "getUserLastLocation")
//                    self.observeKeysReady()
                }
            }
        }
    }
    
    func setLocationParameters() {
        //print("1.setting location parameters")
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //~3
        //The minimum distance (measured in meters) a device must move horizontally before an update event is generated. //~4
        locationManager.distanceFilter = 500
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //print("Will update location coordinates")
        if let lastLocation = locations.last {
            //print("2. getting location coordinates")
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let time = lastLocation.timestamp
            
            self.userLongitude = String(describing: long)
            self.userLatitude = String(describing: lat)
            self.userTimeStamp = String(describing: time)
            
            self.userLocationFromDB = CLLocation(latitude: lat, longitude: long)
            //print("user location has been set", self.userLocationFromDB!)
//            self.updateUserLocationRecord(userNewLat: String(describing: lat), userNewLon: String(describing: long), userNewTimeStamp: String(describing: time))
            
            self.locusObfuscare(withLocation: lastLocation)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //print("locationManager error.", error)
    }
    
    func locusObfuscare(withLocation: CLLocation) {
        //generate randomized distance between 100 & 300 meters
        var randDistance = Random.number8bit()
        //print("testing randDistance", randDistance)
        while randDistance < 200  {
            //try again
            randDistance = Random.number8bit()
        }
        
        let randDistance_Double = Double(randDistance)
        //print("FINAL RAND DISTANCE IS,", randDistance_Double)
        
        //ATTENTION: WILL ONLY RETURN value between 0 - 256. Fix later to return between 0 - 360
        var randBearing = Random.number8bit()
        //print("testing randBearing", randBearing)
//        while randBearing < 360 {
//            randBearing = Random.number8bit()
//        }
        let randBearing_Double = Double(randBearing)
        //print("FINAL RAND BEARING IS,", randBearing_Double)
        
        let Location2D = withLocation.coordinate
        self.locationWithBearing(bearing: randBearing_Double, distanceMeters: randDistance_Double, origin: Location2D)
    }
    
    func locationWithBearing(bearing:Double, distanceMeters:Double, origin:CLLocationCoordinate2D) {
        //print("in locationWithBearing")
        let distRadians = distanceMeters / (6372797.6) // earth radius in meters
        
        let realLat = origin.latitude * Double.pi / 180
        let realLon = origin.longitude * Double.pi / 180
        
        let mirrorLat = asin(sin(realLat) * cos(distRadians) + cos(realLat) * sin(distRadians) * cos(bearing))
        let mirrorLon = realLon + atan2(sin(bearing) * sin(distRadians) * cos(realLat), cos(distRadians) - sin(realLat) * sin(mirrorLat))
        
        let finalLat = mirrorLat * 180 / Double.pi
        let finalLon = mirrorLon * 180 / Double.pi
        
        let finalCLLocation = CLLocation(latitude: finalLat, longitude: finalLon)
        
        self.mirrorLon = String(describing: finalCLLocation.coordinate.longitude)
        self.mirrorLat = String(describing: finalCLLocation.coordinate.latitude)
        
        //print("finalCLLocation is,", finalCLLocation)
        self.mirrorLocation = finalCLLocation
        
        self.updateUserLocationRecord()
    }
    
    func updateUserLocationRecord() {
        //print("3. Now updating user location record")
        if let user = Auth.auth().currentUser {
            //update DB location coordinates
            //print("updating user location in DB")
            //set real location
            //            self.ref.child("users/\(user.uid)/userLongitude").setValue(userNewLat)
            //            self.ref.child("users/\(user.uid)/userLatitude").setValue(userNewLon)
            
//            self.ref.child("users/\(user.uid)/userLongitude").setValue(self.mirrorLon)
//            self.ref.child("users/\(user.uid)/userLatitude").setValue(self.mirrorLat)
//            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(self.userTimeStamp)
//
//            let locationUpdates = [
//                "users/\(user.uid)/userLongitude" : self.mirrorLon,
//                "users/\(user.uid)/userLatitude" : self.mirrorLat,
//            ]
            
            //update geofire location
            let geoFireRef = Database.database().reference().child("userLocationGF")
            let geoFire = GeoFire(firebaseRef: geoFireRef)
            
            //set location of user in GeofireFolder in DB
            //            geoFire.setLocation(self.userLocationFromDB!, forKey: "\(user.uid)")
            geoFire.setLocation(self.mirrorLocation, forKey: "\(user.uid)")
            //print("Geofire location updated successfully")
        }
    }

    
    
    
    
    
    
    //MARK: UserDictArray Loader Methods......................................................................................................................
    
    
    
    
    
    
    
    var gridDidLoad = false

    //create array that links UID to distanceTo
    var distanceToByUID = [String: String]()

    var tierQueryRadius = Double(200) //starts at 200 and multiplies by *2
    var userQueryRadius: Double?
    let maxQueryRadius = Double(4000)
    
    var maxUsersToShow = 200
    var minimumUserCountThreshold = 0

    var maxKeysThresholdReached = false
    
    var CLLocationForUID = [String: CLLocation]()

    var newKeyRowIndex: Int?
    
    var uniqueUIDsInProximity_Array = [String]()
    
    var didObserveKeysReadyForRefresh = false
    var keysEnteredCount = 0
    //Note: if ur calling observeKeysEntered so many times through tier, check if its recording multiple instance of new keys entered
    
//    var testLat = 36.08145091495198
//    var testLon = -115.0326007586253

    var testLat = 37.3229978
    var testLon = -122.03218229999999
  
    //texas
//    var testLat = 31.9685988
//    var testLon = -99.90181310000003
    
    
    func observeKeysEntered(withSource: String) {
        //Note 1: Method called immediately after location is set form either configureLocation() or getLastLocationfromDB()
        //Note 2: Method call will only fire at source if gridDidLoad == false
        
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        guard let selfUID = Auth.auth().currentUser?.uid else { return }
        
        var finalQueryRadius: Double!
        
        //Set queryCenter
        if userLocationFromSetup != nil {
            //print("user center A")
            self.center = userLocationFromSetup
        } else {
            //print("user center B")
            self.center = userLocationFromDB
        }
        
        //Note:
        if let userRadius = UserDefaults.standard.object(forKey: "\(selfUID)_userRadius") as? Float {
            
            //CASE 1. Query based on user preference
            if userRadius < 200 {
                //1.a. Limited Radius Pref.
                self.shouldQueryUsingTiers = false
                self.userQueryRadius = Double(userRadius)
                finalQueryRadius = Double(userRadius)
                //print("finalUserRadius,", finalQueryRadius)
                self.observeKeysReady(withUserRadius: Double(userRadius))

            } else {
                //1.b. Unlimited Radius Pref.
                self.shouldQueryUsingTiers = true
                finalQueryRadius = self.tierQueryRadius
                
                self.observeKeysReady_Tiered(withTierRadius: self.tierQueryRadius)
            }
            
        } else {
            
            //CASE 2. Use System Default
            // !  Default Radius == Double(4000)
            self.shouldQueryUsingTiers = true
            finalQueryRadius = self.tierQueryRadius
            
            self.observeKeysReady_Tiered(withTierRadius: self.tierQueryRadius)
        }

        //ATTN: is there a need to index on "g" in geofire node?
        //print("finalUserRadiusX,", finalQueryRadius)
        //print("self.center!X", self.center!)
        
        
//        let testLocation = CLLocation(latitude: self.testLat, longitude: self.testLon)
//        let circleQuery = geoFire.query(at: testLocation, withRadius: 3000) //radius in km

        let circleQuery = geoFire.query(at: self.center!, withRadius: finalQueryRadius) //radius in km
        
        //Observe keys within radius
        circleQuery.observe(.keyEntered, with: { (key : String!, location : CLLocation!) in
            
            if self.ref_SelfModelController.cvcShouldReloadView == true && self.didObserveKeysReadyForRefresh == false {
                //print("queryRadius23,", self.tierQueryRadius)
                self.observeKeysReady_Tiered(withTierRadius: self.tierQueryRadius)
                self.didObserveKeysReadyForRefresh = true
            }
            
            let userIDKey = key!
            
            //calculate self's distance to retrieved user distance
            let locationAcross = self.center?.distance(from: location)
         
            //instantiate structure containing ID of user within proximity & add UID and distanceTo values to this structure
            var userInstance = usersinProximityStruct()
            
            userInstance.otherUserID = userIDKey
            if userIDKey != selfUID {
                userInstance.toOtherUserDistance = locationAcross!
            } else {
                let myDistance: CLLocationDistance = 0
                userInstance.toOtherUserDistance = myDistance
            }
            
            //build array of all users within proximity, where each array entry is a struct containing the UID and distanceTo vlaues of each instance
            
            if self.outgoingBlockedUIDs.contains(userIDKey) || self.incomingBlockedUIDs.contains(userIDKey)  {
                //print("UID IS BLOCKED, do not append")
                
            } else {
                //print("UID IS not blocked, do append")
                if !self.uniqueUIDsInProximity_Array.contains(userIDKey) {
                    
                    self.keysEnteredCount = self.keysEnteredCount + 1
                    //print("keysEnteredCount", self.keysEnteredCount)
                    
                    self.uniqueUIDsInProximity_Array.append(userIDKey)
                    self.uidinProximityRawArray.append(userInstance)
                
                    //
                    let distanceToInMeters = locationAcross!
                    let distanceToInKM = distanceToInMeters / 1000
                    let distanceToInMi = distanceToInKM / 1.609344
                    
                    let formatter = MKDistanceFormatter()
                    formatter.units = .imperial
                    formatter.unitStyle = .abbreviated
                    
                    let formattedDistanceString = formatter.string(fromDistance: locationAcross!)
                    //print("formattedDistanceString is,", formattedDistanceString)
                    
                    self.CLLocationForUID[userIDKey] = location
                    self.distanceToByUID[userIDKey] = formattedDistanceString
                    //print("distanceToByUID array is NOW,", self.distanceToByUID.count)
                }
            }
            
            //Note: For newly entered keys only
            if self.gridDidLoad == true {
                //print("observedNewKeyEntered")
                
                let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
                
                let i = sortedStructArray.index(where: { (userStruct) -> Bool in
                    userStruct.otherUserID == userIDKey
                })
                
                if let newKeyRowIndex = i {
                    if self.outgoingBlockedUIDs.contains(userIDKey) || self.incomingBlockedUIDs.contains(userIDKey)  {
                        // do not append if blocked out or blocked in
                    } else {
//                        self.populateArrayofDictionaries_NewKeysEnteredUpdater(forUID: userIDKey)
//                        self.newKeyRowIndex = newKeyRowIndex
                    }
                }
            }
        })
    }
    
//    KE1
//    observeKeysReadyUserRadiusX, 172.0
//    observeKeysReadyself.center!X <+42.36408687,-71.10042275>
    
    //        finalUserRadiusX, some(172.0)
    //        self.center!X <+42.36408687,-71.10042275>

    //Observe keys radius - User-set default
    func observeKeysReady(withUserRadius: Double) {
        //Note 1: Method called immediately after location is set form either configureLocation() or getLastLocationfromDB()
        //Note 2: Method ALWAYS called  Simultaneously with observeKeysEntered()
        //Note 2: Method call will only fire at source if gridDidLoad == false
        
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        //print("observeKeysReadyUserRadiusX,", withUserRadius)
        //print("observeKeysReadyself.center!X", self.center!)
        
        let circleQuery = geoFire.query(at: center!, withRadius: withUserRadius) //radius in km
        
        circleQuery.observeReady({
            
            //print("observeReadyNoTier keysEnteredCount", self.keysEnteredCount)
            
            let numberOfUsersInProximity = self.uidinProximityRawArray.count
            
            //
            if  numberOfUsersInProximity < self.maxUsersToShow {
                //print("observeReadyFunction willset minimumthreshold to", self.uidinProximityRawArray.count)
                self.minimumUserCountThreshold = self.uidinProximityRawArray.count
                
            } else {
                //number of Items threshold at which collectionView will start loading
                //print("observeReadyFunction willset minimumthreshold to maxUsersToShowDefault", self.maxUsersToShow)
                self.minimumUserCountThreshold = self.maxUsersToShow
            }
            
            //Sort users in Proximity AFTER all keys are fired
            self.sortUsersinProximity()
            //
            
            //Start populating user objects AFTER all keys are sorted
            self.populateArrayofDictionaries(withSource: "observeKeysReady")
            //
            
            //Should call presence ONLY AFTER ARRAY IS FULLY LOADED -> Method call moved to ENDPopArrDict
            //            self.observeGlobalUsersPresence()
        })
    }
    
    //Note: Bool set in observeKeysEntered: userDidSetRadiusPreference returns (false) else (true)
    var shouldQueryUsingTiers = true

    //Note: What happens if number of users in DB == 0? (Make sure you at least have one written user key before you launch)
    var numberOfItems_Tier0 = 0
    var didSetCount_Tier0 = false
    var didQueryMaxRadiusPossible = false
 
    func observeKeysReady_Tiered(withTierRadius: Double) {
     
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
//        let testLocation = CLLocation(latitude: self.testLat, longitude: self.testLon)
//        let circleQuery = geoFire.query(at: testLocation, withRadius: 3000) //
        
        let circleQuery = geoFire.query(at: center!, withRadius: withTierRadius) //radius in km

        circleQuery.observeReady({
            
            //print("observeReadyTiered keysEnteredCount", self.keysEnteredCount)
            
            //Note: Tier0 is constant (number of items from first query) -> onlySetOnce
            if self.didSetCount_Tier0 == false {
                self.numberOfItems_Tier0 = self.uidinProximityRawArray.count
            }
            
            let usersInProximityCount = self.uidinProximityRawArray.count //most-up-to-date key count
            
            if usersInProximityCount < self.maxUsersToShow {
                //Case 1. There AREN'T enough user-keys in proximity
                
                if self.didSetCount_Tier0 == false {
                    //Tier 1: Query wider radius: @R 800 = 400 * 2 ONLY ONCE
                    self.tierQueryRadius = withTierRadius * 4
                    //print("A querying Radius,", self.tierQueryRadius)

                    //call keysEntered with wider radius
                    self.observeKeysEntered(withSource: "observeKeysReady_Tiered")

                    //call observeReady with wider radius
                    //self.observeKeysReady_Tiered(withTierRadius: (withTierRadius * 4)) //now called through observeKeysEntered()

                    self.didSetCount_Tier0 = true

                    return
                } else if self.didSetCount_Tier0 == true {

                    //print("usersInProximityCount vs.", usersInProximityCount,  "numberOfItems_Tier0,", self.numberOfItems_Tier0)

                    if usersInProximityCount <= self.numberOfItems_Tier0 {
                        //Max number of users in DB has been queried. No more keys exist.

                        self.minimumUserCountThreshold = usersInProximityCount
                        self.sortUsersinProximity()
                        self.populateArrayofDictionaries(withSource: "observeKeysReady_Tiered")

                        return

                    } else {
                        //More keys do exist -> keep querying
                        
                        //Note: trip fx when radius == 3200 since next step (3600 * 4) will exceed maxRadius == 4000
                        if self.tierQueryRadius != 3200 && self.tierQueryRadius < 4000 {
                            self.tierQueryRadius = withTierRadius * 4
//                            print("B querying Radius,", self.tierQueryRadius)

                            self.observeKeysEntered(withSource: "observeKeysReady_Tiered")
                            
                            return
                            
                        } else {
                            //LAST Query at maxQueryRadius if all Tiers Fail to return enough keys
                            guard self.didQueryMaxRadiusPossible == false else {
                                //final Set
                                self.minimumUserCountThreshold = usersInProximityCount
                                self.sortUsersinProximity()
                                self.populateArrayofDictionaries(withSource: "observeKeysReady_Tiered")
                                
                                return
                            }
                            
                            self.tierQueryRadius = self.maxQueryRadius
                            self.observeKeysEntered(withSource: "observeKeysReady_Tiered")
                            self.didQueryMaxRadiusPossible = true
                            
                            return
                        }
                    }
                    
                }
                
            } else if usersInProximityCount >= self.maxUsersToShow  {
                //Case 2. There ARE enough user-keys in minimum proximity == 200 mi Radius

                //set threshold
                
                //BEFORE CHANGE. WHy was minimumThreshold set to usersInProximityCount
                
//                self.minimumUserCountThreshold = usersInProximityCount
                self.minimumUserCountThreshold = self.maxUsersToShow

                //sort items
                self.sortUsersinProximity()
                //popArr
                self.populateArrayofDictionaries(withSource: "observeKeysReady")
            }
        })
    }

    //Note: keysReady might be returning keys by order of proximity to begin with. You may not need to sort users at all.
    
    func sortUsersinProximity() {
        
        let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
        
        let maxKeysObservedCount = sortedStructArray.count
        
        //Note: When not enough users in proximity, cap user array to maxKeysObservedCount
        if maxKeysObservedCount < maxUsersToShow {
            for i in 0..<sortedStructArray.count {
                self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
            }
            
        //when excess users in proximity, cap user array to max
        } else if maxKeysObservedCount >= maxUsersToShow {
            for i in 0..<maxUsersToShow {
                self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
            }
        }
        
        //
        if self.ref_SelfModelController.shouldQuery == true {
            self.addNewOnlineKeys()
        }
    }
    
    var didAppend1 = false
    var didAppend2 = false
    var didAppend3 = false
    var didAppend4 = false
    var didAppend5 = false
    
    var didAppend6 = false
    var didAppend7 = false
    var didAppend8 = false
    var didAppend9 = false
    var didAppend10 = false
    
    var didAppend11 = false
    var didAppend12 = false
    var didAppend13 = false
    var didAppend14 = false
    var didAppend15 = false
    
    var didAppend16 = false
    var didAppend17 = false
    var didAppend18 = false
    var didAppend19 = false
    var didAppend20 = false
    
    var didAppend21 = false
    var didAppend22 = false
    var didAppend23 = false
    var didAppend24 = false
    var didAppend25 = false
    
    var didAppend26 = false
    var didAppend27 = false
    var didAppend28 = false
    var didAppend29 = false
    var didAppend30 = false
    
    var finalUsersOnlineArray = [String]()
    
    func addNewOnlineKeys() {
        //add new online keys
        guard !self.allUsersOnline.isEmpty else { return }
//        print("self.allUsersOnline.count", self.allUsersOnline.count)
//        print("self.sortedUIDinProximityArray.count", self.sortedUIDinProximityArray.count)
        //already filtered for selfUID
        
        //if users online is more than 50, you only want to grab 50 and not more
        if self.allUsersOnline.count <= 50 {
            self.addNewOnlineKeys_Helper(withArray: self.allUsersOnline)
            
        } else if self.allUsersOnline.count > 50 {
            
            //get only first 20 users and scatter randomly
            for index in 0..<50 {
                let getID = self.allUsersOnline[index]
                self.finalUsersOnlineArray.append(getID)
            }
            
            self.addNewOnlineKeys_Helper(withArray: self.finalUsersOnlineArray)
        }
        
    }
    
    func addNewOnlineKeys_Helper(withArray: [String]) {
        
        for onlineId in withArray {
            
            if !self.sortedUIDinProximityArray.contains(onlineId) {
                
                //1
                if self.didAppend1 == false {
                    if self.sortedUIDinProximityArray.count >= 5 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 5)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend1 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //2
                if self.didAppend2 == false {
                    if self.sortedUIDinProximityArray.count >= 6 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 6)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend2 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //3
                if self.didAppend3 == false {
                    if self.sortedUIDinProximityArray.count >= 7 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 7)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend3 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //4
                if self.didAppend4 == false {
                    if self.sortedUIDinProximityArray.count >= 10 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 10)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend4 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //5
                if self.didAppend5 == false {
                    if self.sortedUIDinProximityArray.count >= 11 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 11)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend5 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //6
                if self.didAppend6 == false {
                    if self.sortedUIDinProximityArray.count >= 10 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 10)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend6 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //7
                if self.didAppend7 == false {
                    if self.sortedUIDinProximityArray.count >= 11 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 11)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend7 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //8
                if self.didAppend8 == false {
                    if self.sortedUIDinProximityArray.count >= 12 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 12)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend8 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //9
                if self.didAppend9 == false {
                    if self.sortedUIDinProximityArray.count >= 17 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 17)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend9 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //10
                if self.didAppend10 == false {
                    if self.sortedUIDinProximityArray.count >= 18 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 18)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend10 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //11
                if self.didAppend11 == false {
                    if self.sortedUIDinProximityArray.count >= 22 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 22)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend11 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //12
                if self.didAppend12 == false {
                    if self.sortedUIDinProximityArray.count >= 23 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 23)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend12 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //13
                if self.didAppend13 == false {
                    if self.sortedUIDinProximityArray.count >= 24 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 24)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend13 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //14
                if self.didAppend14 == false {
                    if self.sortedUIDinProximityArray.count >= 26 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 26)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend14 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //15
                if self.didAppend15 == false {
                    if self.sortedUIDinProximityArray.count >= 29 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 29)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend15 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //16
                if self.didAppend16 == false {
                    if self.sortedUIDinProximityArray.count >= 30 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 30)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend16 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //17
                if self.didAppend17 == false {
                    if self.sortedUIDinProximityArray.count >= 35 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 35)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend17 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //18
                if self.didAppend18 == false {
                    if self.sortedUIDinProximityArray.count >= 37 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 37)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend18 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //19
                if self.didAppend19 == false {
                    if self.sortedUIDinProximityArray.count >= 38 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 38)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend19 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //20
                if self.didAppend20 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend20 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //21
                if self.didAppend21 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend21 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //22
                if self.didAppend22 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend22 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //23
                if self.didAppend23 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend23 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //24
                if self.didAppend24 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend24 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //25
                if self.didAppend25 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend25 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //30
                if self.didAppend26 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend26 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //31
                if self.didAppend27 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend27 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //28
                if self.didAppend28 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend28 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //29
                if self.didAppend29 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend29 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
                //30
                if self.didAppend30 == false {
                    if self.sortedUIDinProximityArray.count >= 41 {
                        self.sortedUIDinProximityArray.insert(onlineId, at: 41)
                        self.distanceToByUID[onlineId] = " "
                        self.didAppend30 = true
                        
                        continue
                    } else {
                        return
                    }
                } else {
                    //
                }
                
            }
        }
    }
    
    
    /*
    func sortUsersinProximity() {
        let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
        //initiate first here: for i in 0..<100 in sortedArray
        for i in 0..<sortedStructArray.count {
            self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
        }
        self.buildPhotoURLStringArray()
        ////print("Test 1: \(self.urlStringArray)")
        //self.populateArrayofDictionaries()
    }
    */

    func loadSelfProfileInfo() {
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)").observe(.value) { (snapshot) in
            guard let value = snapshot.value as? [String: Any] else { return }
            self.ref_SelfModelController.getSelfProfileInfo_ParserMethod(snapshot: snapshot)
            self.selfVisus = self.ref_SelfModelController.myProfileInfoDict["selfVisus"]
        }
    }
    
    var completeInfo = true
    var popArrCounter = 0
    
    //Remember: for in loop - a return function will exit entire loop. must call continue if you want to stop current iteration and go back.
    
    //Note: Method called in observeKeysReady() AFTER all keys in proximity are LOADED && SORTED && CAPPED
    //Note: populateArrayofDictionaries() behaves both as a LOADER method and SYNCER method (.value)
    
    //Johnny pushID: "eBJnZF_Zuic:APA91bE-nAo9xSZhSGkBNyidbEPXJoemXFJJnMy1AzEeA5bRDMGHQy_yf3OM0NZXO93LFShBzJUM80mJeVg3jl4Waaph7faSDrU5vvymzlzaqhx0Q2bEQ9nUvHmFJvCaBDF5vUpzc60g"
    
    func populateArrayofDictionaries(withSource: String) {
        
        self.ref_SelfModelController.setSelfAgeFilterValues()
        
        for UID in self.sortedUIDinProximityArray {
            
            ref.child("users").child(UID).observe(.value) { (snapshot) in
                
                self.popArrCounter = self.popArrCounter + 1
                
                var value: [String: Any]!
                
                if let snapshotVal = snapshot.value as? [String: Any] {
                     value = snapshotVal
                } else {
                    //print("ESCAPE AT 0")
                    
                    self.completeInfo = false
                    self.decrementMinimumThreshold()
                    
                    if let selfUID = Auth.auth().currentUser?.uid {
                        if snapshot.key == selfUID {
                            self.cleanDB(withUID: snapshot.key)
                            return
                        } else {
                            
                        }
                    }
                    
                    //ATTN: Doesn't a guard statement exit a function entirely?
                    return
                }
                
                let userUID = snapshot.key
//                if userUID == "2IDDJrdq98RFMSFBQvOi0cUVqyn2" {
//                    print("check")
//                }
                //Note: Method should LOAD or SYNC
                if !self.userDictArray_UID.contains(userUID) {
                    //New instance -> LOAD
                    
                } else {
                    //Existing instance -> SYNC
                    self.updateUserProfileChanges(snapshot: snapshot)
                    
                    return
                }
                
                //Init userObject
                var userGridThumbInfoDict = [String: String]()

                //uid
                userGridThumbInfoDict["UID"] = userUID
                
                //userName
                if let userDisplayName = value["userDisplayName"] as? String {
                    if userDisplayName != "DisplayName" {
                        userGridThumbInfoDict["userDisplayName"] = userDisplayName
                        self.completeInfo = true
                    } else {
//                        self.cleanDB(withUID: userUID)
                        self.completeInfo = false
                    }
                } else {
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //userAge
                var userAge = ""
                if let uAge = value["userAge"] as? String {
                    userAge = uAge
                    userGridThumbInfoDict["userAge"] = userAge
                } else {
                    //print("ESCAPE AT 4")
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //userMinAge Filter
                var userMinAge = ""
                if let uMinAge = value["showMeMinAge"] as? String {
                    userMinAge = uMinAge
                    userGridThumbInfoDict["userMinAge"] = userMinAge
                } else {
                    //print("ESCAPE AT 5")
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //userMaxAge Filter
                var userMaxAge = ""
                if let uMaxAge = value["showMeMaxAge"] as? String {
                    userMaxAge = uMaxAge
                    userGridThumbInfoDict["userMaxAge"] = userMaxAge
                } else {
                    //print("ESCAPE AT 6")
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //userFullURL
                if let userThumbnailURLString = value["full_URL"] as? String {
                    userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                    //print("populateArrayofDictionaries userThumbnailURLString", userThumbnailURLString)
                } else {
                    //print("ESCAPE AT 1")
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //userGridURL
                if let gridThumbURL = value["gridThumb_URL"] as? String {
                    //print("CHECK gridThumbURL")
                    userGridThumbInfoDict["gridThumb_URL"] = gridThumbURL
                } else {
                    //print("FAIL gridThumbURL")
                }
                
                //userVisus
                if let visus = value["visus"] as? String {
                    userGridThumbInfoDict["visus"] = visus
                } else {
                    //print("ESCAPE AT 3")
//                    self.cleanDB(withUID: userUID)
                    self.completeInfo = false
                }
                
                //AboutMe
                if let aboutMe = value["AboutMe"] as? String {
                    userGridThumbInfoDict["aboutMe"] = aboutMe
                } else {
                    userGridThumbInfoDict["aboutMe"] = ""
                }
                
                //School
                if let school = value["School"] as? String {
                    userGridThumbInfoDict["school"] = school
                } else {
                    userGridThumbInfoDict["school"] = ""
                }
                
                //Work
                if let work = value["Work"] as? String {
                    userGridThumbInfoDict["work"] = work
                } else {
                    userGridThumbInfoDict["work"] = ""
                }
                
                //Height
                if let height = value["Height"] as? String {
                    userGridThumbInfoDict["height"] = height
                } else {
                    userGridThumbInfoDict["height"] = ""
                }
                
                //HereFor
                if let lookingFor = value["LookingFor"] as? String {
                    userGridThumbInfoDict["lookingFor"] = lookingFor
                } else {
                    userGridThumbInfoDict["lookingFor"] = ""
                }
                
                //Privacy
                if let userPrivacyPreference = value["privacy"] as? String {
                    userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
                } else {
                    userGridThumbInfoDict["userPrivacyPreference"] = ""
                }
                
                //BlurDegree
                if let blurState = value["blurState"] as? String {
                    userGridThumbInfoDict["blurState"] = blurState
                } else {
                    userGridThumbInfoDict["blurState"] = ""
                }
                
                //Presence
                if let isOnline = value["isOnline"] as? String {
                    userGridThumbInfoDict["isOnline"] = isOnline
                } else {
                    userGridThumbInfoDict["isOnline"] = ""
                }
                
                //additional photo URLs
                if let photoURL2 = value["full_URL_2"] as? String {
                    userGridThumbInfoDict["full_URL_2"] = photoURL2
                }
                
                if let photoURL3 = value["full_URL_3"] as? String {
                    userGridThumbInfoDict["full_URL_3"] = photoURL3
                }
                
                if let photoURL4 = value["full_URL_4"] as? String {
                    userGridThumbInfoDict["full_URL_4"] = photoURL4
                }
                
                //DistanceTo
                let distanceToUser = self.distanceToByUID[userUID]
                userGridThumbInfoDict["distanceTo"] = distanceToUser
                
                if let pushID = value["pushID"] as? String {
                    //                    print("cvc pushID", pushID)
                    userGridThumbInfoDict["pushID"] = pushID
                } else {
                    //                    print("cvc NO pushID")
                }
                
                //1. Show me Profiles with Age Between
                
                //self props
                let showMeMinAge = self.ref_SelfModelController.showMeMinAgeInt!
                let showMeMaxAge = self.ref_SelfModelController.showMeMaxAgeInt!
                let myAgeInt = self.ref_SelfModelController.myAge
                
                //oth props
                let userAgeInt = (userAge as NSString).integerValue
                var userMinAgeFilter = 0
                var userMaxAgeFilter = 100
                
                if userMinAge != "" {
                    userMinAgeFilter = (userMinAge as NSString).integerValue
                }
                
                if userMaxAge != "" {
                    userMaxAgeFilter = (userMaxAge as NSString).integerValue
                }

                if self.completeInfo == true {
                    //print("completeInfo CHECK")
                    if userAgeInt < showMeMinAge || userAgeInt > showMeMaxAge || myAgeInt > userMaxAgeFilter || myAgeInt < userMinAgeFilter {
                        self.decrementMinimumThreshold()
                        
                    } else {
                        self.userDictArray_UID.append(userUID)
                        self.userDictArray.append(userGridThumbInfoDict)
                    }
                    
                } else if self.completeInfo == false {
                    //print("will decrement threshold")
                    self.decrementMinimumThreshold()
                }

                //2. filter out users who's age range I am outside of myself
//                print("ACCIO self.userDictArray.count", self.userDictArray.count)
//                print("ACCIO self.minimumUserCountThreshold", self.minimumUserCountThreshold)
                
//                if self.userDictArray.count + 1 >= self.minimumUserCountThreshold {
                if self.userDictArray.count + 0 >= self.minimumUserCountThreshold {

                    //Meta
                    self.ref_SelfModelController.gridDidLoad = true
                    self.ref_SelfModelController.cvcShouldReloadView = false
//                    self.performFinalBlocksCleanup()
                    self.activityIndicatorView?.stopAnimating()
                    self.collectionView!.reloadSections([0])
                    //ATTN: Make sure you remove observer in callback for initialized users online NOW
                    self.observeGlobalUsersPresence()
                }
            }
        }
    }
    
//    var preDictArray = [[String:String]]()

//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.becomeFirstResponder() // To get shake gesture
//    }
    
    // We are willing to become first responder to get shake motion
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    // Enable detection of shake motion
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            print("Why are you shaking me?")
            self.shuffleArray()
        }
    }
    
    func shuffleArray() {

        //get the shuffled array
        let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: self.userDictArray) as? [[String:String]]
        collectionView.forLastBaselineLayout.layer.speed = 0.4

        collectionView.performBatchUpdates({

            //update original data source
//            self.userDictArray = shuffled!

            //
            self.userDictArray.swapAt(3, 1)
            self.userDictArray.swapAt(7, 5)
            self.userDictArray.swapAt(0, 9)
            self.userDictArray.swapAt(4, 8)
            self.userDictArray.swapAt(6, 11)
//
//            //
////            self.collectionView.exchangeSubview(at: 3, withSubviewAt: 1)
////            self.collectionView.exchangeSubview(at: 7, withSubviewAt: 5)
////            self.collectionView.exchangeSubview(at: 0, withSubviewAt: 9)
////            self.collectionView.exchangeSubview(at: 4, withSubviewAt: 8)
////            self.collectionView.exchangeSubview(at: 6, withSubviewAt: 11)
//
//            //test some cells
            self.collectionView.moveItem(at: IndexPath(item: 1, section: 0), to: IndexPath(item: 3, section: 0))
            self.collectionView.moveItem(at: IndexPath(item: 5, section: 0), to: IndexPath(item: 7, section: 0))
            self.collectionView.moveItem(at: IndexPath(item: 9, section: 0), to: IndexPath(item: 0, section: 0))
            self.collectionView.moveItem(at: IndexPath(item: 8, section: 0), to: IndexPath(item: 4, section: 0))
            self.collectionView.moveItem(at: IndexPath(item: 11, section: 0), to: IndexPath(item: 6, section: 0))

        }) { (true) in

            self.collectionView.reloadData()

        }
    }
    
//    func shuffleArray() {
//
//        //speed as factor of original (i.e. speed = 2 -> 2xSpeedOriginal
//        collectionView.forLastBaselineLayout.layer.speed = 0.3
//
//        let shuffled = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: self.userDictArray) as? [[String:String]]
//
//        //for each item, move item from old index to new index
//        for thumb in self.userDictArray {
//
//            let userID = thumb["UID"]!
//
//            //get from index
//            let i = self.userDictArray.index(where: { (cell) -> Bool in
//                cell["UID"] == userID
//            })
//
//            guard let fromIndex = i else { continue }
//            let fromIndexPath = IndexPath(item: fromIndex, section: 0)
//
//            //get to index
//            let i2 = shuffled!.index(where: { (cell) -> Bool in
//                cell["UID"] == userID
//            })
//
//            guard let toIndex = i2 else { continue }
//            let toIndexPath = IndexPath(item: toIndex, section: 0)
//
//            //move item with animation
//            self.collectionView.moveItem(at: fromIndexPath, to: toIndexPath)
////            self.collectionView.addMotionEffect(<#T##effect: UIMotionEffect##UIMotionEffect#>)
//        }
//
//        //set this to set cells when reloaded
////        self.userDictArray = shuffled!
//    }

    
    
    
    
//    func shuffleAnimat() {
//
//
//        collectionView.performBatchUpdates({
//            <#code#>
//        }) { (true) in
//            //
//        }
//
//    }
    
    
    
    //Note: should call only once in lifecycle
    /*
    @objc func selfProfileInfoDidLoad(_ notification: Notification) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(CollectionViewController.selfProfileInfoDidLoad(_:)), name: NSNotification.Name(rawValue: "selfProfileInfoDidLoad"), object: nil)

        
        guard notification.name.rawValue == "selfProfileInfoDidLoad" else { return }
        guard ref_SelfModelController.gridDidLoad == false else { return }
        
        self.activityIndicatorView?.stopAnimating()
        self.collectionView!.reloadSections([0])
        self.ref_SelfModelController.gridDidLoad = true
        
        self.observeGlobalUsersPresence()
    }
    */
    
    func cleanDB(withUID: String) {
        //print("populateArrayofDictionaries willcleanDB withUID,", withUID)
        //we don't need to delete the instance from
        
        self.ref.child("userLocationGF/\(withUID)").removeValue()
        self.ref.child("users/\(withUID)").removeValue()
    }
    
    func decrementMinimumThreshold() {
        self.minimumUserCountThreshold = self.minimumUserCountThreshold - 1
    }
    
    
    
    
    
    
    
    //MARK: UserDictArray Syncing Methods................................................................................................
    
    
    
    
    
    
    
    
    func populateArrayofDictionaries_NewKeysEnteredUpdater(forUID: String) {

        ref.child("users").child(forUID).observe(.value) { (snapshot) in

            guard let userGridThumbInfoDict = self.userDictArray_ParserHelper(snapshot: snapshot) else { return }
            guard let indexRow = self.newKeyRowIndex else { return }
            guard !self.userDictArray_UID.contains(forUID) else { return }
            
            //ATTN: Indx out of range error
            let indexPath = IndexPath(item: indexRow, section: 0)
            
            self.userDictArray_UID.append(forUID)
            self.userDictArray.insert(userGridThumbInfoDict, at: indexRow)
            self.collectionView!.insertItems(at: [indexPath])
            self.collectionView!.reloadItems(at: [indexPath])
        }
    }
    
    func userDictArray_ParserHelper(snapshot: DataSnapshot) -> [String : String]? {

        var userGridThumbInfoDict = [String: String]()
        
        //ATTN: NEed to set case where user changed profile photo, as in regular popArraDict, otherwise won't observe profile changes for new key entered
        guard let value = snapshot.value as? [String: Any] else {
            //escape method if geoFire key is not mirrored in UID
//            self.cleanDB(withUID: snapshot.key)
            self.decrementMinimumThreshold()
            
            return nil
        }
        
        let userUID = snapshot.key
        
        //uid
        userGridThumbInfoDict["UID"] = userUID
        
        //userName
        if let userDisplayName = value["userDisplayName"] as? String {
            if userDisplayName != "DisplayName" {
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                self.completeInfo = true
            } else {
//                self.cleanDB(withUID: userUID)
                self.completeInfo = false
            }
        } else {
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userAge
        var userAge = ""
        if let uAge = value["userAge"] as? String {
            userAge = uAge
            userGridThumbInfoDict["userAge"] = userAge
        } else {
            //print("ESCAPE AT 4")
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userMinAge Filter
        var userMinAge = ""
        if let uMinAge = value["showMeMinAge"] as? String {
            userMinAge = uMinAge
            userGridThumbInfoDict["userMinAge"] = userMinAge
        } else {
            //print("ESCAPE AT 5")
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userMaxAge Filter
        var userMaxAge = ""
        if let uMaxAge = value["showMeMaxAge"] as? String {
            userMaxAge = uMaxAge
            userGridThumbInfoDict["userMaxAge"] = userMaxAge
        } else {
            //print("ESCAPE AT 6")
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userFullURL
        if let userThumbnailURLString = value["full_URL"] as? String {
            userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
            //print("populateArrayofDictionaries userThumbnailURLString", userThumbnailURLString)
        } else {
            //print("ESCAPE AT 1")
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userGridURL
        if let gridThumbURL = value["gridThumb_URL"] as? String {
            //print("CHECK gridThumbURL")
            userGridThumbInfoDict["gridThumb_URL"] = gridThumbURL
        } else {
            //print("FAIL gridThumbURL")
        }
        
        //userVisus
        if let visus = value["visus"] as? String {
            userGridThumbInfoDict["visus"] = visus
        } else {
            //print("ESCAPE AT 3")
//            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //AboutMe
        if let aboutMe = value["AboutMe"] as? String {
            userGridThumbInfoDict["aboutMe"] = aboutMe
        } else {
            userGridThumbInfoDict["aboutMe"] = ""
        }
        
        //School
        if let school = value["School"] as? String {
            userGridThumbInfoDict["school"] = school
        } else {
            userGridThumbInfoDict["school"] = ""
        }
        
        //Work
        if let work = value["Work"] as? String {
            userGridThumbInfoDict["work"] = work
        } else {
            userGridThumbInfoDict["work"] = ""
        }
        
        //Height
        if let height = value["Height"] as? String {
            userGridThumbInfoDict["height"] = height
        } else {
            userGridThumbInfoDict["height"] = ""
        }
        
        //HereFor
        if let lookingFor = value["LookingFor"] as? String {
            userGridThumbInfoDict["lookingFor"] = lookingFor
        } else {
            userGridThumbInfoDict["lookingFor"] = ""
        }
        
        //Privacy
        if let userPrivacyPreference = value["privacy"] as? String {
            userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
        } else {
            userGridThumbInfoDict["userPrivacyPreference"] = ""
        }
        
        //BlurDegree
        if let blurState = value["blurState"] as? String {
            userGridThumbInfoDict["blurState"] = blurState
        } else {
            userGridThumbInfoDict["blurState"] = ""
        }
        
        //Presence
        if let isOnline = value["isOnline"] as? String {
            userGridThumbInfoDict["isOnline"] = isOnline
        } else {
            userGridThumbInfoDict["isOnline"] = ""
        }
        
        //additional photo URLs
        if let photoURL2 = value["full_URL_2"] as? String {
            userGridThumbInfoDict["full_URL_2"] = photoURL2
        }
        
        if let photoURL3 = value["full_URL_3"] as? String {
            userGridThumbInfoDict["full_URL_3"] = photoURL3
        }
        
        if let photoURL4 = value["full_URL_4"] as? String {
            userGridThumbInfoDict["full_URL_4"] = photoURL4
        }
  
        //DistanceTo
        let distanceToUser = self.distanceToByUID[userUID]
        userGridThumbInfoDict["distanceTo"] = distanceToUser
        
        let selfUID = Auth.auth().currentUser!.uid
        //1. Show me Profiles with Age Between
        
        //self props
        let showMeMinAge = self.ref_SelfModelController.showMeMinAgeInt!
        let showMeMaxAge = self.ref_SelfModelController.showMeMaxAgeInt!
        let myAgeInt = self.ref_SelfModelController.myAge
        
        //oth props
        let userAgeInt = (userAge as NSString).integerValue
        var userMinAgeFilter = 0
        var userMaxAgeFilter = 100
        
        if userMinAge != "" {
            userMinAgeFilter = (userMinAge as NSString).integerValue
        }
        
        if userMaxAge != "" {
            userMaxAgeFilter = (userMaxAge as NSString).integerValue
        }

        if self.completeInfo == true {
            return userGridThumbInfoDict
            
        } else if self.completeInfo == false {
            return nil
        }

        return userGridThumbInfoDict
    }
    
    
    //ATTN: FINISH THE BELOW METHOD TO UPDATE LIVE CHANGES IN USER: VISIBILITY PREFERNECES
    //ATTN: FOR SAFETY ALWAYS SET SELF PHOTO IN GRID FROM FULL_URL NOT FROM THUMB_URL
    
    var profileChangesUIDsInProximity = [String]()
    
    //note: value observer firing whenever user also changes location (i,e, whenever user traverses viewcontrollers)
    //ATTN: MAKE SURE THE .VALUE OBSERVER IS ONLY FIRING AS MANY TIMES AS NEEDED AND NOT 10,000 TIMES PER USER SESSION
    //WHAT HAPPENS if you attach the observer to the child node ie - userID/userDIsplayPhotoURL - can you get the parent ID in that case?
    
    var profileChangesCounter = 0
    
    func updateUserProfileChanges(snapshot: DataSnapshot) {
        
        self.profileChangesCounter = self.profileChangesCounter + 1
        //print("userprofilechanges counter,", self.profileChangesCounter)
        
        guard let value = snapshot.value as? [String: Any] else { return }
        
        let userID = snapshot.key
        
        //get user entry
        //                userGridThumbInfoDict["UID"] = userUID
        
        //        //print("self.userDictArray", self.userDictArray)
        let i = self.userDictArray.index(where: { (dictionaryOfInterest) -> Bool in
            dictionaryOfInterest["UID"] == userID
        })
        
        guard let index = i else { return }
        
        //additional info configuration
        if let aboutMe = value["AboutMe"] as? String {
             (self.userDictArray[index])["aboutMe"] = aboutMe
        }
        
        if let lookingFor = value["LookingFor"] as? String {
            (self.userDictArray[index])["lookingFor"] = lookingFor
        }
        
        if let height = value["Height"] as? String {
            (self.userDictArray[index])["height"] = height
        }
        
        if let school = value["School"] as? String {
            (self.userDictArray[index])["school"] = school
        }
        
        if let work = value["Work"] as? String {
            (self.userDictArray[index])["work"] = work
        }

        //get new vals
        guard let newThumbURL = value["gridThumb_URL"] as? String else { return }
        guard let newFullURL = value["full_URL"] as? String else { return }
        guard let newVisus = value["visus"] as? String else { return }
        
        //get old vals
        guard let oldThumbURL = (self.userDictArray[index])["gridThumb_URL"] else { return }
        guard let oldFullURL = (self.userDictArray[index])["userThumbnailURLString"] else { return }
        guard let oldVisus = (self.userDictArray[index])["visus"] else { return }
    
        var newURL2 = ""
        var newURL3 = ""
        var newURL4 = ""
        
        //new plus photos
        if let url2 = value["full_URL_2"] as? String {
            newURL2 = url2
        }
        
        if let url3 = value["full_URL_3"] as? String {
            newURL3 = url3
        }
        
        if let url4 = value["full_URL_4"] as? String {
            newURL4 = url4
        }
        
        var oldURL2 = ""
        var oldURL3 = ""
        var oldURL4 = ""

        //get old extra photos
        if let url2_ = (self.userDictArray[index])["full_URL_2"] {
            oldURL2 = url2_
        }
        
        if let url3_ = (self.userDictArray[index])["full_URL_3"] {
            oldURL3 = url3_
        }
        
        if let url4_ = (self.userDictArray[index])["full_URL_4"] {
            oldURL4 = url4_
        }

        //print("GUARD FULL PASS observeProfileChanges")
        
        let indexPath = IndexPath(row: index, section: 0)
        
        //change existing user thumb url
        if newThumbURL != oldThumbURL && newFullURL != oldFullURL {
            //print("!CHECK gridThumbURL observer")
            
            self.updateUserPhoto(newThumbURL: newThumbURL, userID: userID, forIndexPath: indexPath, newVisus: newVisus, newFullURL: newFullURL, url2: newURL2, url3: newURL3, url4: newURL4)
        } else {
            //print("!FAIL newThumbURL")
        }
        
        //change extra URL
        if newURL2 != oldURL2 || newURL3 != oldURL3 || newURL4 != oldURL4 {
            self.updateUserPhoto(newThumbURL: newThumbURL, userID: userID, forIndexPath: indexPath, newVisus: newVisus, newFullURL: newFullURL, url2: newURL2, url3: newURL3, url4: newURL4)
        }
        
        //change existing user visus property
        if let visus = value["visus"] as? String {
            //print("oldVis,", (self.userDictArray[index])["visus"]!)
            (self.userDictArray[index])["visus"] = visus
            //print("newVis,", (self.userDictArray[index])["visus"]!)
        } else {

        }
    }
    
    //CAST FROM 1WAY TO 2WAY
//    newListObject["objectType"] = "swapRequestOtherNew"
//    newListObject["swapRequestID"] = (self.chatListObjects[oneWayIndex])["oneWayRequestID"]
//    newListObject["requestStatus"] = "false"
//
//    //Profile Info
//    newListObject["swapWithUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
//    newListObject["swapWithDisplayName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
//    newListObject["swapWithDefaultPhotoURL"] = newFullURL
//    newListObject["swapWithVisus"] = newVisus
//
//    //Chat Info
//    newListObject["chatThreadID"] = (self.chatListObjects[oneWayIndex])["chatThreadID"]
//    newListObject["lastMessage"] = ""
//
//    //UI Marker
//    newListObject["selfDidOpen"] = "false"
//
//    newListObject["swapRecipientID"] = Auth.auth().currentUser!.uid
//    newListObject["swapInitiatorID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
//    newListObject["swapInitiatorFirstName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
//    newListObject["swapInitiatorDefaultURL"] = newFullURL
//
//    newListObject["swapRecipientFirstName"] = "You"
//    newListObject["swapInitiatorDidOpen"] = "false"
//    newListObject["swapRecipientDidOpen"] = "false"
//    newListObject["objectUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
    
    //RUN TEST: ONE WAY UNLOCK SHOULD NEVER UPDATE CHANGED PHOTO ON RECIPIENT END- I>E> RECIPIENT SHOULD ALWAYS SHOW OLD PHOTO ON
    func updateUserPhoto(newThumbURL: String, userID: String, forIndexPath: IndexPath, newVisus: String, newFullURL: String, url2: String, url3: String, url4: String) {
        //save new image for user to cache then reload item data within completion block
        
        if !self.allUniqueUIDsinChatList.contains(userID) {
            //print("updateUserPhoto for CaseI")
            
            if !self.unlockedUsers.contains(userID) {
                //User not unlocked
                //print("updateUserPhoto for CaseI.A")
                //print("CaseI.A ", self.userDictArray)
                
                let row = forIndexPath.row
                //print("CaseI.A self.userDictArray[row]", self.userDictArray[row])
                
                //update dictionary values in self
                (self.userDictArray[row])["gridThumb_URL"] = newThumbURL
                (self.userDictArray[row])["userThumbnailURLString"] = newFullURL
                (self.userDictArray[row])["full_URL_2"] = url2
                (self.userDictArray[row])["full_URL_3"] = url3
                (self.userDictArray[row])["full_URL_4"] = url4
                
//                //print("newval0", ((self.userDictArray[row])["gridThumb_URL"]))
//                //print("newval1", ((self.userDictArray[row])["userThumbnailURLString"]))
//                //print("newval2", ((self.userDictArray[row])["full_URL_2"]))
//                //print("newval3", ((self.userDictArray[row])["full_URL_3"]))
//                //print("newval4", ((self.userDictArray[row])["full_URL_4"]))
                
                var userInfoDict = [String : String]()
                userInfoDict["forUID"] = userID
                
                userInfoDict["gridURL"] = newThumbURL
                userInfoDict["fullURL"] = newFullURL
                userInfoDict["visus"] = newVisus
                
                userInfoDict["url2"] = url2
                userInfoDict["url3"] = url3
                userInfoDict["url4"] = url4
                
                //update dictionary values in targetVC
                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateOthProfileChanges"), object: self, userInfo: userInfoDict)
                
                //
                if let cellToChange = self.collectionView.cellForItem(at: forIndexPath) as? CollectionViewCell {
                    //print("changeCellCHECK")
                    //print("changeCellCHECK dict", self.userDictArray)
                
                    cellToChange.profilePhotoThumbnailView.sd_setImage(with: URL(string: newThumbURL), completed: nil)
                    
                    //                    self.collectionView!.reloadItems(at: [IndexPath(row: 2, section: 0)])
                    
                }
                
                
                //                self.saveNewImageToCache(newThumbURL: newThumbURL, userID: userID, forIndexPath: forIndexPath)
                
            } else {
                //print("updateUserPhoto for CaseI.B")
                //user was unlocked, do not update UI
            }
            
        } else {
            //FIRST, update the new values: full_URL, gridThumURL, andVisus properties in the chatList object, and check if the ViewControllerLogic adequately performs logic for presenting/dismissing swapControls and ChatListUI
            //get the object from chatListObjects
            //print("updateUserPhoto for CaseII.A")
            //print("II. ChatLISTOBJECTS DOES CONTAIN USER OBJECT for key,", userID)
            var objectIndex: Int?
            
            //1. Is current object generalChatThread?
            let genChatIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["chatWithUID"] == userID
            })
            
//            var chatListObjectPhotoURLKeys = ["chatRequestWithDefaultPhotoURL", "chatWithPhotoURL", "revealToDefaultPhotoURL", "swapWithDefaultPhotoURL", "revealToDefaultPhotoURL" ]
            
            //IMP NOTE: You DO NOT need to update extra meta info (fullUrl2..4) for chatListObjects - since you won't access user profile from chatList -> objects only need reference to mainURL/gridURL
            if let chatObjectIndex = genChatIndex {
                //print("updateUserPhoto for CaseII.A1")
                
                var chatListObject = self.chatListObjects[chatObjectIndex]
                //c.update appropriate keys
                chatListObject["chatWithVisus"] = newVisus
                chatListObject["chatWithPhotoURL"] = newFullURL
                
                //print("!!A old photo URL", chatListObject["chatWithPhotoURL"]!)
                
                //print("!!A new photo URL", chatListObject["chatWithPhotoURL"]!)
                //CHECK PASS - URLS ARE DIFFERENT AND WRITTEN APPROPRIATELY, SO WHAT"S GOING ON?
                
                var userInfoDict = [String : String]()
                userInfoDict["changeUserID"] = userID
                userInfoDict["newPhotoURL"] = newFullURL
                userInfoDict["newUserVisus"] = newVisus
                userInfoDict["forObjectType"] = "generalChatThread"
                
                if !self.unlockedUsers.contains(userID) {
                    
                    //if user was not unlocked
                    self.saveNewImageToCache(newThumbURL: newThumbURL, userID: userID, forIndexPath: forIndexPath)
                    
                    (self.userDictArray[forIndexPath.row])["gridThumb_URL"] = newThumbURL
                    (self.userDictArray[forIndexPath.row])["userThumbnailURLString"] = newFullURL
                    
                } else {
                    //user was unlocked, do not update UI
                    //image will always set from Vault even when updated
                }
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "othProfileChangeObserved"), object: self, userInfo: userInfoDict)
                
            } else {
                
            }
            
            //2. Is current object 1way?
            let oneWaySwapIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["revealToUID"] == userID
            })
            
//            /////////////////////////////////
//            newListObject["oneWayRequestID"] = newSwapRequestID
//            newListObject["requestStatus"] = swapRequestStatus
//
//            //Profile Info
//            newListObject["revealToUID"] = swapInitiatorID
//            newListObject["revealToDisplayName"] = swapInitiatorFirstName
//            newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
//            newListObject["revealToVisus"] = swapInitiatorVisus
//
//            //Chat Info
//            newListObject["chatThreadID"] = chatThreadID
//            newListObject["lastMessage"] = lastMessage
//
//            //UI Marker
//            //newListObject["selfDidOpen"] = swapInitiatorDidOpen
//            newListObject["otherDidSee"] = swapInitiatorDidOpen //optionally used in UI
//            newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
//            newListObject["requestInitiatorFirstName"] = "You"
//            newListObject["objectUID"] = swapInitiatorID
            
            if let oneWayIndex = oneWaySwapIndex {
                //print("updateUserPhoto for CaseII.A2")
                let row = forIndexPath.row

                guard self.ref_SelfModelController.myProfileInfoDict["selfVisus"]! == "false" else { return }
                guard newVisus == "false" else { return } //need to make sure change only takes place once
//                guard newVisus != (self.userDictArray[row])["visus"] else { return }
                
                //redundant conditions below, since will only ever unlock if false
//                guard let requestStatus = (self.chatListObjects[oneWayIndex])["swapRequestStatus"] else { return }
//                guard requestStatus == "false" else { return }
 
                //user dict entry changes
                (self.userDictArray[row])["gridThumb_URL"] = newThumbURL
                (self.userDictArray[row])["userThumbnailURLString"] = newFullURL
                (self.userDictArray[row])["full_URL_2"] = url2
                (self.userDictArray[row])["full_URL_3"] = url3
                (self.userDictArray[row])["full_URL_4"] = url4
                (self.userDictArray[row])["visus"] = newVisus

                //chatlistobjectchanges
                (self.chatListObjects[oneWayIndex])["revealToVisus"] = newVisus
                (self.chatListObjects[oneWayIndex])["revealToDefaultPhotoURL"] = newFullURL

                //CAST AS NEW GenChat OBJECT
                var generalChatObject = [String : String]()
                
                generalChatObject["objectType"] = "generalChatThread"
                
                //user info
                generalChatObject["chatWithUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
                generalChatObject["chatWithDisplayName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
                generalChatObject["chatWithPhotoURL"] = newFullURL
                //print("newFullURL", newFullURL)
                //print("newThumbURL", newThumbURL)

                generalChatObject["chatWithVisus"] = newVisus
                
                //chatMetaData
                generalChatObject["chatThreadID"] = (self.chatListObjects[oneWayIndex])["chatThreadID"]
                generalChatObject["lastMessage"] = ""
                generalChatObject["objectUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
                
                //remove old item
                self.chatListObjects.remove(at: oneWayIndex)
         
                //insert new item
                if let lastMessage = self.lastMessagesbyUID["\(userID)"] {
                    if lastMessage != "" {
                        self.chatListObjects.insert(generalChatObject, at: oneWayIndex)
                    } else {
//                        if self.uidtoChatThreadIDIndexDictionary.keys.contains(userID) {
//                            self.uidtoChatThreadIDIndexDictionary.removeValue(forKey: userID)
//                        }
                    }
                } else {
                    
                }
                
                //notification meta info
                var userInfoDict = [String : String]()
                userInfoDict["changeUserID"] = userID
                userInfoDict["newPhotoURL"] = newFullURL
                userInfoDict["newUserVisus"] = newVisus
                userInfoDict["forObjectType"] = "oneWaySwapObject_ANY"
                
                self.collectionView.reloadItems(at: [forIndexPath])

                generalChatObject["forObjectType"] = "1WaySwapToChat"
                //Notify ChatListVC
                NotificationCenter.default.post(name: Notification.Name(rawValue: "othProfileChangeObserved"), object: self, userInfo: generalChatObject)
                
                var cvcUserInfoDict = [String : String]()
                cvcUserInfoDict["forUserID"] = userID
                //Notify CVC
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "convertIncomingSwapToChat"), object: self, userInfo: cvcUserInfoDict)

            }
            
            //3. Is current object 2way?
            let twoWaySwapIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapWithUID"] == userID
            })
            
            if let twoWayIndex = twoWaySwapIndex {
                //print("updateUserPhoto for CaseII.A3")
                guard newVisus == "false" else { return }
                
                var chatListObject = self.chatListObjects[twoWayIndex]
                
                chatListObject["swapWithVisus"] = newVisus
                chatListObject["swapWithDefaultPhotoURL"] = newFullURL
                
                var userInfoDict = [String : String]()
                userInfoDict["changeUserID"] = userID
                userInfoDict["newPhotoURL"] = newFullURL
                userInfoDict["newUserVisus"] = newVisus
                userInfoDict["forObjectType"] = "twoWaySwapObject_ANY"
                
                //CAST 2Way swap as one way
                
            }
        }
        
        //ALWAYS UPDATE WHEN SELF
        //        (newThumbURL: String, userID: String, forIndexPath: IndexPath, newVisus: String, newFullURL: String, url2: String, url3: String, url4: String)
        
        if userID == Auth.auth().currentUser!.uid {
            //print("updateUserPhoto for CaseIII")
            let row = forIndexPath.row
            
            //update dictionary values in self
            (self.userDictArray[row])["gridThumb_URL"] = newThumbURL
            (self.userDictArray[row])["userThumbnailURLString"] = newFullURL
            (self.userDictArray[row])["full_URL_2"] = url2
            (self.userDictArray[row])["full_URL_3"] = url3
            (self.userDictArray[row])["full_URL_4"] = url4
            
            //print("gridThumbSelf", ((self.userDictArray[row])["gridThumb_URL"]))
            //print("newval2", ((self.userDictArray[row])["full_URL_2"]))
            //print("newval3", ((self.userDictArray[row])["full_URL_3"]))
            //print("newval4", ((self.userDictArray[row])["full_URL_4"]))
            
            var userInfoDict = [String : String]()
            userInfoDict["forUID"] = userID
            
            userInfoDict["gridURL"] = newThumbURL
            userInfoDict["fullURL"] = newFullURL
            userInfoDict["visus"] = newVisus
            
            userInfoDict["url2"] = url2
            userInfoDict["url3"] = url3
            userInfoDict["url4"] = url4
            
            //update dictionary values in targetVC
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateOthProfileChanges"), object: self, userInfo: userInfoDict)
            
            //
            //            self.saveNewImageToCache(newThumbURL: newThumbURL, userID: userID, forIndexPath: forIndexPath)
            self.collectionView.reloadItems(at: [forIndexPath])
        }
    }
    
    @objc func castOutgoingRevealtoGenChat_onPubToPriv(_ notification: Notification) {
        
        guard notification.name.rawValue == "castOutgoingRevealtoGenChat_onPubToPriv" else { return }
        
        guard let forUID = notification.userInfo?["forUserID"] as? String else { return }
        
        let oneWaySwapIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
            dictionaryOfInterest["revealToUID"] == forUID
        })
        
        guard let oneWayIndex = oneWaySwapIndex else { return }
      
        let oldChatID = (self.chatListObjects[oneWayIndex])["chatThreadID"]
        
        //CAST AS NEW GenChat OBJECT
        var generalChatObject = [String : String]()

        generalChatObject["objectType"] = "generalChatThread"

        //user info
        generalChatObject["chatWithUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
        generalChatObject["chatWithDisplayName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
        generalChatObject["chatWithPhotoURL"] = (self.chatListObjects[oneWayIndex])["revealToDefaultPhotoURL"]
 
        generalChatObject["chatWithVisus"] = (self.chatListObjects[oneWayIndex])["revealToVisus"]

        //chatMetaData
        generalChatObject["chatThreadID"] = (self.chatListObjects[oneWayIndex])["chatThreadID"]
        generalChatObject["lastMessage"] = ""
        generalChatObject["objectUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]

        //remove old item
        self.chatListObjects.remove(at: oneWayIndex)
        if let index = self.allUniqueUIDsinChatList.index(of: forUID) {
            self.allUniqueUIDsinChatList.remove(at: index)
        }
        //insert new item
//        if oldChatID != "" {
        //            self.chatListObjects.insert(generalChatObject, at: oneWayIndex)

//        }
        
        //insert new item
        if let lastMessage = self.lastMessagesbyUID["\(forUID)"] {
            if lastMessage != "" {
                self.chatListObjects.insert(generalChatObject, at: oneWayIndex)
                self.allUniqueUIDsinChatList.append(forUID)
            } else {
//                if self.uidtoChatThreadIDIndexDictionary.keys.contains(forUID) {
//                    self.uidtoChatThreadIDIndexDictionary.removeValue(forKey: forUID)
//                }
            }
        } else {
//            if self.uidtoChatThreadIDIndexDictionary.keys.contains(forUID) {
//                self.uidtoChatThreadIDIndexDictionary.removeValue(forKey: forUID)
//            }
        }
    }
    
     @objc func castOutgoingSwaptoGenChat_onPrivToPub(_ notification: Notification) {
        
        //Note: Should activate for every pending outgoing 2Way swap that's cleared on change visibility
        guard notification.name.rawValue == "castOutgoingSwaptoGenChat_onPrivToPub" else { return }
        
         guard let forUID = notification.userInfo?["forUserID"] as? String else { return }
        
        let twoWaySwapIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
            dictionaryOfInterest["swapWithUID"] == forUID
        })
        
         guard let twoWayIndex = twoWaySwapIndex else { return }
        
        //
        var generalChatObject = [String : String]()
        
        generalChatObject["objectType"] = "generalChatThread"

        //user info
        generalChatObject["chatWithUID"] = (self.chatListObjects[twoWayIndex])["swapWithUID"]
        generalChatObject["chatWithDisplayName"] = (self.chatListObjects[twoWayIndex])["swapWithDisplayName"]
        generalChatObject["chatWithPhotoURL"] = (self.chatListObjects[twoWayIndex])["swapWithDefaultPhotoURL"]
        generalChatObject["chatWithVisus"] = (self.chatListObjects[twoWayIndex])["swapWithVisus"]
        
        //chatMetaData
        generalChatObject["chatThreadID"] = (self.chatListObjects[twoWayIndex])["chatThreadID"]
        generalChatObject["lastMessage"] = ""
        generalChatObject["objectUID"] = (self.chatListObjects[twoWayIndex])["swapWithUID"]
        
        //
        self.chatListObjects.remove(at: twoWayIndex)
        self.chatListObjects.insert(generalChatObject, at: twoWayIndex)
        
        //clear old chatID key

        
    }
    
    
    //CAST FROM 1WAY TO 2WAY
    //    newListObject["objectType"] = "swapRequestOtherNew"
    //    newListObject["swapRequestID"] = (self.chatListObjects[oneWayIndex])["oneWayRequestID"]
    //    newListObject["requestStatus"] = "false"
    //
    //    //Profile Info
    //    newListObject["swapWithUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
    //    newListObject["swapWithDisplayName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
    //    newListObject["swapWithDefaultPhotoURL"] = newFullURL
    //    newListObject["swapWithVisus"] = newVisus
    //
    //    //Chat Info
    //    newListObject["chatThreadID"] = (self.chatListObjects[oneWayIndex])["chatThreadID"]
    //    newListObject["lastMessage"] = ""
    //
    //    //UI Marker
    //    newListObject["selfDidOpen"] = "false"
    //
    //    newListObject["swapRecipientID"] = Auth.auth().currentUser!.uid
    //    newListObject["swapInitiatorID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]
    //    newListObject["swapInitiatorFirstName"] = (self.chatListObjects[oneWayIndex])["revealToDisplayName"]
    //    newListObject["swapInitiatorDefaultURL"] = newFullURL
    //
    //    newListObject["swapRecipientFirstName"] = "You"
    //    newListObject["swapInitiatorDidOpen"] = "false"
    //    newListObject["swapRecipientDidOpen"] = "false"
    //    newListObject["objectUID"] = (self.chatListObjects[oneWayIndex])["revealToUID"]

    func saveNewImageToCache(newThumbURL: String, userID: String, forIndexPath: IndexPath) {
        //print("CVC WILLsaveNewImageToCache")
        
        self.collectionView!.numberOfItems(inSection: 0)
        self.collectionView.reloadItems(at: [forIndexPath])
        
        //idk why this only updates when user scrolls across the cell
        if let cellToChange = self.collectionView.cellForItem(at: [forIndexPath.row]) as? CollectionViewCell {
            //print("changeCellCHECK")
            cellToChange.profilePhotoThumbnailView.sd_setImage(with: URL(string: newThumbURL), completed: nil)
        }
        
        
        //clear old image
        //        SDImageCache.shared().removeImage(forKey: userID, fromDisk: false) {
        //            SDWebImageDownloader.shared().downloadImage(with: URL(string: newThumbURL), options: .highPriority, progress: nil) { image, _, _, _ in
        //                if let image = image {
        //                    SDImageCache.shared().store(image, forKey: userID, completion: {
        //                        //print("!stored Cache image forKey", userID)
        //                        //MUST not reload cell if swap request exists & user did not unlock
        //
        //                        //print("forIndexPath X!,", forIndexPath)
        //                        //print("self.userDictArray X!,", self.userDictArray)
        //                        //print("self.userDictArray.count X!,", self.userDictArray.count)
        //
        //                        self.collectionView!.numberOfItems(inSection: 0)
        //                        self.collectionView.reloadItems(at: [forIndexPath])
        //                    })
        //                }
        //            }
        //        }
    }
    

    //    var ref_ModelController = ModelController()
    //    var ref_UserModelController = UserModelController()
    
    //AFTER MODEL CONTROLLER:
    /*
    func populateArrayofDictionaries() {
        
        guard gridDidLoad == false else { return }
        
        ref_UserModelController.ref = self.ref
        ref_UserModelController.myProfileInfoDict = self.myProfileInfoDict
        ref_UserModelController.minimumUserCountThreshold = self.minimumUserCountThreshold
        
        for UID in self.sortedUIDinProximityArray {
            //print("populateArrayofDictionaries for UID", UID)
            
            ref.child("users").child(UID).observe(.value) { (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else {
                    //escape method if geoFire key is not mirrored in UID
                    //also you might want to write a validation rule that enforces that geoFire key is written with mirror in uID atomically
                    //print("error for snapshot,", snapshot.key)
                    //print("ESCAPE AT 0")
                    self.cleanDB(withUID: snapshot.key)
                    if self.ref_UserModelController.minimumUserCountThreshold > 0 {
                           self.ref_UserModelController.minimumUserCountThreshold = self.ref_UserModelController.minimumUserCountThreshold - 1
                    }
                    return
                }
                
                let userID = snapshot.key
                let othLocation = self.CLLocationForUID[userID]!
                self.ref_UserModelController.userDictArray_ParserMethod(snapshot: snapshot, center: self.center!, othLocation: othLocation)
                
                let newUserDictArrayCount = self.ref_UserModelController.userDictArray.count
                let newMinimumUserCountThreshold = self.ref_UserModelController.minimumUserCountThreshold
                
                if newUserDictArrayCount + 1 >= newMinimumUserCountThreshold {
                    //print("willLoad collection view NOW with self.userDictArray.count,", self.userDictArray.count, "self.minimumUserCountThreshold", self.minimumUserCountThreshold)

                    self.userDictArray = self.ref_UserModelController.userDictArray
                
                    self.activityIndicatorView?.stopAnimating()
                    self.collectionView!.reloadSections([0])
                    self.gridDidLoad = true
//                    self.ref_ModelController.initGridProperties(withBool: true)
                    //                    self.userDictArrayFullyLoaded = true
                    self.observeOutgoingBlockRequests() //reactivate
                    self.observeIncomingBlockRequests() //reactivate
                    self.observeIncomingDismissedRequests() //reactivate
                }
            }
        }
    }
 */
 
    func storeCachedImage(urlString: String, forUID: String) {
        SDWebImageDownloader.shared().downloadImage(with: URL(string: urlString), options: .highPriority, progress: nil) { image, _, _, _ in
            if let image = image {
                let dimension = 126 as CGFloat
                //                let forImageData = self.resizeImage(dimension, with: 0.9, forImage: image)!
                //                self.setDisplayProfilePhotoURL(imageData: forImageData, forUID: forUID)
                //print("storeCachedImage will resize photo")
                
                SDImageCache.shared().store(image, forKey: forUID, completion: {
                    //print("stored Cache image forKey", forUID)
                })
                //then try resizing image
            }
        }
    }
    
    
    /*
    func populateArrayofDictionaries() {
        //check that our sortedUIDinProximityArray contains all UID values sorted by distance
        //print("populateArrayofDictionaries minimumUserCountThreshold is,", minimumUserCountThreshold)
        
        self.setSelfAgeFilterValues()
        
        for UID in self.sortedUIDinProximityArray {
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else { return }
                
                let userUID = snapshot.key
                //print("willloadUID,", userUID)
                
                guard let userDisplayName = value["userDisplayName"] as? String else { return }
                guard let userThumbnailURLString = value["full_URL"] as? String else { return }
                guard let userPhotoStorageRef = value["userDisplayPhoto"] as? String else { return }
                guard let userAge = value["userAge"] as? String else { return }
                guard let userMinAge = value["showMeMinAge"] as? String else { return }
                guard let userMaxAge = value["showMeMaxAge"] as? String else { return }
                
                guard let school = value["School"] as? String else { return }
                guard let work = value["Work"] as? String else { return }
                guard let aboutMe = value["AboutMe"] as? String else { return }
                guard let height = value["Height"] as? String else { return }
                guard let lookingFor = value["LookingFor"] as? String else { return }
                
                guard let userPrivacyPreference = value["privacy"] as? String else { return }
                guard let visus = value["visus"] as? String else { return }
                guard let blurState = value["blurState"] as? String else { return }
                
                guard let isOnline = value["isOnline"] as? String else { return }
                
                let distanceToUser = self.distanceToByUID[userUID]
                //print("WILLAPPENDDISTANCETOUSEROBJECT,", distanceToUser)
                
                var userGridThumbInfoDict = [String: String]()
                
                //Create key-value pairs for each user
                userGridThumbInfoDict["UID"] = userUID
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                userGridThumbInfoDict["userAge"] = userAge
                userGridThumbInfoDict["userMinAge"] = userMinAge
                userGridThumbInfoDict["userMaxAge"] = userMaxAge
                
                userGridThumbInfoDict["distanceTo"] = distanceToUser
                
                userGridThumbInfoDict["school"] = school
                userGridThumbInfoDict["work"] = work
                userGridThumbInfoDict["aboutMe"] = aboutMe
                userGridThumbInfoDict["height"] = height
                userGridThumbInfoDict["lookingFor"] = lookingFor
                userGridThumbInfoDict["blurState"] = blurState
                
                userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
                userGridThumbInfoDict["visus"] = visus
                userGridThumbInfoDict["isOnline"] = isOnline
                
                //                guard let noNym = value["noNym"] as? String else { return }
                //                guard let onuma = value["onuma"] as? String else { return }
                //                userGridThumbInfoDict["noNym"] = noNym
                //                userGridThumbInfoDict["onuma"] = onuma
                
                let selfUID = Auth.auth().currentUser!.uid
                //1. Show me Profiles with Age Between
                
                //self props
                let showMeMinAge = self.showMeMinAgeInt!
                let showMeMaxAge = self.showMeMaxAgeInt!
                let myAgeInt = self.myAge
                
                //oth props
                let userAgeInt = (userAge as NSString).integerValue
                var userMinAgeFilter = 0
                var userMaxAgeFilter = 100
                
                if userMinAge != "" {
                    userMinAgeFilter = (userMinAge as NSString).integerValue
                }
                
                if userMaxAge != "" {
                    userMaxAgeFilter = (userMaxAge as NSString).integerValue
                }
                
                //print("incoming user age int is,", userAgeInt)
                
                if userAgeInt < showMeMinAge || userAgeInt > showMeMaxAge || myAgeInt > userMaxAgeFilter || myAgeInt < userMinAgeFilter {
                    //remove user from required GFarray
                    self.minimumUserCountThreshold = self.minimumUserCountThreshold - 1
                    //print("userAgeInt,", userAgeInt, "not in bounds will NOT APPEND USER")
                } else {
                    //print("noMinAge detected, will default to appending user object")
                    self.userDictArray.append(userGridThumbInfoDict)
                }
                
                //code that will prevent other user from seeing my profile if other user falls outside my specified age range. I.e.I don't want other user to see my profile if they don't fall in my age range
                //2. filter out users who's age range I am outside of myself
   
                //
                
                //should not append user object if object does not contain all values
                //                self.userDictArray.append(userGridThumbInfoDict)
                //print("User Array Dictionary now contains: \(self.userDictArray)")
                //print("will reload collectionView")
                
                //CV is reloading as many times as there are items in array. we want this function to execute only once after all array values are populated. (once 100 items are populated?)
                //                guard self.userDictArrayFullyLoaded == false else { return }
                //                self.collectionView!.reloadSections([0])
                
                //print("ACCIO self.userDictArray.count", self.userDictArray.count)
                //print("ACCIO self.self.minimumUserCountThreshold", self.minimumUserCountThreshold)
                
                //                if self.userDictArrayFullyLoaded == false {
                    //print("willLoad collection view NOW with self.userDictArray.count,", self.userDictArray.count, "self.minimumUserCountThreshold", self.minimumUserCountThreshold)
                    self.activityIndicatorView?.stopAnimating()
                    self.collectionView!.reloadSections([0])
                    self.userDictArrayFullyLoaded = true
                    self.observeUserProfileChanges()
                    self.observeOutgoingBlockRequests()
                    self.observeIncomingBlockRequests()
                    self.observeIncomingDismissedRequests()
                
                //                } else {
                //
                //                }
                //
            })
        }
    }
    */
    
    // MARK: Collection View Protocol Methods
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    /*func numberOfSections(in collectionView: UICollectionView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.userDictArray.count
    }
    
    let softSpaceGray = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    
    var urlString = ""
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        //Clear views on Dequeue
        cell.profilePhotoThumbnailView!.image = nil
        cell.userDisplayNameLabel.text = ""

        cell.swapSymbView.isHidden = true
        cell.swapSymbView2.isHidden = true
        
        let imageView = cell.profilePhotoThumbnailView as UIImageView
        
        //Parse User Info
        let user = self.userDictArray[(indexPath as NSIndexPath).row]
        
        let userUID = user["UID"]!
        let userVisus = user["visus"]!

        let isOnline = user["isOnline"]!
        let userDisplayName = user["userDisplayName"]!
        let userAge = user["userAge"]!
        
        //1. Set name & age
        let fullString = "\(userDisplayName)," + " " + userAge
        
        let nameRange = (fullString as NSString).range(of: "\(userDisplayName),")
        let ageRange = (fullString as NSString).range(of: userAge)
        
        let boldFont = UIFont(name: "Avenir-Medium", size: 16.0)!
        let thinFont = UIFont(name: "Avenir-Light", size: 16.0)!
        
        let attributedString = NSMutableAttributedString(string: fullString)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: nameRange)
        attributedString.addAttribute(NSAttributedStringKey.font, value: thinFont, range: ageRange)
        
        cell.userDisplayNameLabel.backgroundColor = UIColor.clear
        cell.userDisplayNameLabel.attributedText = attributedString
        cell.userDisplayNameLabel.layer.shadowOpacity = 0.4
        cell.userDisplayNameLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.userDisplayNameLabel.layer.shadowRadius = 0.8
        
        //2. Set presence
        if isOnline == "true" {
            cell.presenceSymbol.image = UIImage(named: "newFullOnline")

//            cell.presenceSymbol.image = UIImage(named: "onlineThumbSymbol")
            
        } else {
            cell.presenceSymbol.image = UIImage(named: "newEmptyOffline")

//            cell.presenceSymbol.image = UIImage(named: "offlineOutline")!
        }
        

        
//        imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!), completed: nil)
        
        //3. Set image
        var finalThumbURL: String?
        if let thumbURL = user["gridThumb_URL"] {
            finalThumbURL = thumbURL
            self.urlString = thumbURL
        } else {
            if let fullURL = user["userThumbnailURLString"] {
                finalThumbURL = fullURL
                self.urlString = fullURL
            }
        }
        
        if !self.unlockedUsers.contains(user["UID"]!) {
            if finalThumbURL == self.urlString {
                //print("1 willSet userPhoto for URL,", finalThumbURL!)
                imageView.sd_setImage(with: URL(string: finalThumbURL!), completed: nil)
            }
        } else {
            let imageFromVault = self.photoVault(otherUID: user["UID"]!)
            if imageFromVault != nil {
                imageView.image = imageFromVault
                self.updateCellSwapSymbol(cell: cell, withUserVisus: userVisus)
            } else {
                //Will evaluate this node when:
                //(1) Photo fails to load from vault (user deleted app, and all references were erased)
                //(2) unlockedUsers() contains userID AS outgoingUnlock                
                imageView.sd_setImage(with: URL(string: finalThumbURL!), completed: nil)
            }
        }

        /*
        if !self.unlockedUsers.contains(user["UID"]!) {
            //USER NOT UNLOCKED, SET IMAGE FROM CLOUD
            if let image = SDImageCache.shared().imageFromCache(forKey: userUID) {
                //print("will load image from cachec forUID,", userUID)
                if finalThumbURL == self.urlString {
                    imageView.image = image
                }
            } else {
                //print("will load image TO cachec forUID,", userUID)
                SDWebImageDownloader.shared().downloadImage(with: URL(string: finalThumbURL!), options: .highPriority, progress: nil) { image, _, _, _ in
                    if let image = image {
                        SDImageCache.shared().store(image, forKey: userUID, completion: {
                            if finalThumbURL == self.urlString {
                                imageView.image = image
                            }
//                            imageView.image = image
                            //print("stored Cache image forKey", userUID)
                        })
                    }
                }
            }
        } else {
            //USER IS UNLOCKED, SET IMAGE FROM VAULT
            let imageToSet = self.photoVault(otherUID: user["UID"]!)
            imageView.image = imageToSet
            //print("settingImageFromVault")
            
//            if user["visus"]! == "false" {
//                let imageToSet = self.photoVault(otherUID: user["UID"]!)
//                imageView.image = imageToSet
//                //print("settingImageFromVault")
//            } else {
//                imageView.sd_setImage(with: URL(string: finalThumbURL!), completed: nil)

//                if let image = SDImageCache.shared().imageFromCache(forKey: userUID) {
//                    //print("will load image from cachec forUID,", userUID)
//                    imageView.image = image
//                } else {
//                    NOTE: TO UPDATE USER IMAGE IF USER CHANGES PROFILE PHOTO AFTER UNLOCK - SWITCH OFF THE ELSE BLOCK ABOVE & JUST KEEP THE ONE HERE
//                    SDWebImageDownloader.shared().downloadImage(with: URL(string: finalThumbURL!), options: .highPriority, progress: nil) { image, _, _, _ in
//                        if let image = image {
//                            SDImageCache.shared().store(image, forKey: userUID, completion: {
//                                //print("stored Cache image forKey", userUID)
//                                imageView.image = image
//                                self.writeNewPhotoToVault(withImage: image, forUID: userUID)
//                            })
//                        }
//                    }
//                }
//            }
            //~ 4. Set Swap Status Symbol
            self.updateCellSwapSymbol(cell: cell, withUserVisus: userVisus)
        }
        */
        
        //print("collectionViewContains,", self.collectionView!.numberOfItems(inSection: 0))
        
        return cell
    }
//
    func writeNewPhotoToVault(withImage: UIImage, forUID: String) {
        let fileName = "\(forUID).jpeg"
        if let data = UIImageJPEGRepresentation(withImage, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("CVC. newPhoto written to vault")
        }
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        //Set cell corner radius
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.masksToBounds = true
        cell.backgroundColor = self.softSpaceGray
        
        //let imageView = cell.profilePhotoThumbnailView!
        let imageView = cell.profilePhotoThumbnailView as UIImageView
        
        //Dictionary code
        let user = self.userDictArray[(indexPath as NSIndexPath).row]
        let isOnline = user["isOnline"]!
        let userUID = user["UID"]!
        let userVisus = user["visus"]!
        //
        let userDisplayName = user["userDisplayName"]!
        let userAge = user["userAge"]!
        
        self.configureCellLayout(cell: cell, displayName: userDisplayName, isOnline: isOnline, userID: userUID, userAge: userAge)

         if self.unlockedUsers.contains(user["UID"]!) {
            self.updateCellSwapSymbol(cell: cell, withUserVisus: userVisus)
            //print("HEY! GVC Detected Mark in Grid")
            
            if user["visus"]! == "false" {
                let imageToSet = self.photoVault(otherUID: user["UID"]!)
                imageView.image = imageToSet
                //print("settingImageFromVault")
            } else {
                //1
//                imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
//                //print("settingImageFromCloud")
                
                //2
                if let thumbURL = user["gridThumb_URL"] {
                    imageView.sd_setImage(with: URL(string: thumbURL))
                } else {
                    imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
                }
//
//                //print("settingImageFromCloud")
                
                //3
//                if let image = SDImageCache.shared().imageFromCache(forKey: userUID) {
//                    //print("CVCell getting image from Cache 1")
//                    imageView.image = image
//                }
            }
           
        } else {
            //instead of setting the thumbnail this way, try setting from memorycache instead.
            //when's the best time to cache images to memory? maybe right after the url array fuly loads?
            //BEFORE SDCACHE & BEFORE smallThumb
//            imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
//            //print("settingImageFromCloud")
            
            //AFTER SMALLTHUMB
            //print("loading image url")
            var finalURL = ""
            if let thumbURL = user["gridThumb_URL"] {
                imageView.sd_setImage(with: URL(string: thumbURL))
                finalURL = thumbURL
            } else {
                imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
                finalURL = user["userThumbnailURLString"]!
            }
//            //print("settingImageFromCloud")
            
            //AFTER SD CACHE
//            if let image = SDImageCache.shared().imageFromCache(forKey: userUID) {
//                //print("CVCell getting image from Cache 2")
//                imageView.image = image
//            }
            
            //4try caching the image for the cells in view only
            if let image = SDImageCache.shared().imageFromCache(forKey: userUID) {
                //print("will load image from cachec forUID,", userUID)
                imageView.image = image
            } else {
                //print("will load image TO cachec forUID,", userUID)
                SDWebImageDownloader.shared().downloadImage(with: URL(string: finalURL), options: .highPriority, progress: nil) { image, _, _, _ in
                    if let image = image {
                        SDImageCache.shared().store(image, forKey: userUID, completion: {
                            //print("stored Cache image forKey", userUID)
                        })
                        //then try resizing image
                    }
                }
            }
        }

        //imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
        
        let fullString = "\(userDisplayName)," + " " + userAge
        
        let nameRange = (fullString as NSString).range(of: "\(userDisplayName),")
        let ageRange = (fullString as NSString).range(of: userAge)
        
        let boldFont = UIFont(name: "Avenir-Medium", size: 16.0)!
        let thinFont = UIFont(name: "Avenir-Light", size: 16.0)!
    
        let attributedString = NSMutableAttributedString(string: fullString)
        
        attributedString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: nameRange)
        attributedString.addAttribute(NSAttributedStringKey.font, value: thinFont, range: ageRange)
        
        cell.userDisplayNameLabel.backgroundColor = UIColor.clear
//        cell.userDisplayNameLabel.text = user["userDisplayName"]!
        cell.userDisplayNameLabel.attributedText = attributedString

        cell.userDisplayNameLabel.layer.shadowOpacity = 0.4
        cell.userDisplayNameLabel.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.userDisplayNameLabel.layer.shadowRadius = 0.8
        
//        SDImageCache.shared().store
        
        return cell
    }
    */
    
    //Cell layout is
    /*
     1. adding presenceSymbl
     2. adding 2WasySwapPlaceholder
     3.
 */
    
    func configureCellLayout(cell: CollectionViewCell, displayName: String, isOnline: String, userID: String, userAge: String) {
        //print("configureCellLayout")
        //set screen properties
        let screen = UIScreen.main.bounds
        let screenHeight = screen.height
        let screenWidth = screen.width
        
        //set inset properties
        let insetRatio = 9/414 as CGFloat
        let insetSpacing = insetRatio * screenWidth
        
        //set cell properties
        let cellDimension = 182/126 as CGFloat
        let cellWidth = (screenWidth - (4 * insetSpacing)) / 3.00
        let cellHeight = cellWidth * cellDimension
        
        //set Grid properties
        let unitsInGrid = 18 as CGFloat
        let unitSide = cellWidth / unitsInGrid
        
        //set Frames properties
        let sideSize = (11 / 414) * screenWidth
        let boundingRectangle = CGRect(x: 0, y: 0, width: sideSize, height: sideSize)
        let statusImageView = UIImageView(frame: boundingRectangle)
        
        //1. Add presenceSymb
        let offset = unitSide
        let xPos = cell.bounds.maxX - statusImageView.bounds.width - offset
        let yPos = cell.bounds.maxY - statusImageView.bounds.height - offset

        statusImageView.frame.origin.x = xPos
        statusImageView.frame.origin.y = yPos
        
        statusImageView.layer.shadowOpacity = 0.3
        statusImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        statusImageView.layer.shadowRadius = 0.8
        
        if isOnline == "true" {
            statusImageView.image = UIImage(named: "newFullOnline")
//            statusImageView.image = UIImage(named: "onlineThumbSymbol")
        } else {
            statusImageView.image = UIImage(named: "newEmptyOffline")
//            statusImageView.image = UIImage(named: "offlineThumbSymbol")

            
        }
        
        cell.contentView.addSubview(statusImageView)

        //
//        let swap2WayComplete =

//        if self.unlockedUsers.contains(userID) {
//            cell.swapCompletePlaceHolder.isHidden = false
//            cell.swapCompletePlaceHolder.image = UIImage(named: "swapCompleteThumbSymb")
//        } else {
//            cell.swapCompletePlaceHolder.isHidden = true
//            cell.swapCompletePlaceHolder.image = UIImage(named: "swapCompleteThumbSymb")
//
//        }
//
        //2. Add 2 Way swap indicator
        let swRatio = 22 / 414 as CGFloat
        let shRatio = 13.6 / 22 as CGFloat
        
        let swapSymbWidth = swRatio * screenWidth
        let swapSymbHeight = swapSymbWidth * shRatio
        let swapSymbX = cell.bounds.maxX - unitSide - swapSymbWidth
        let swapSymbY = unitSide
        
        let swapSymbFrame = CGRect(x: swapSymbX, y: swapSymbY, width: swapSymbWidth, height: swapSymbHeight)
        let swapSymbView = UIImageView(frame: swapSymbFrame)
        swapSymbView.image = UIImage(named: "2WaySwapGridSymbol_N")!
        
        swapSymbView.layer.shadowOpacity = 0.4
        swapSymbView.layer.shadowOffset = CGSize(width: 0, height: 0)
        swapSymbView.layer.shadowRadius = 0.8
        
        cell.contentView.addSubview(swapSymbView)
        //.isHidden property will change to false when 2Way swaps are detected
        cell.swapSymbView = swapSymbView
        cell.swapSymbView.isHidden = true
        
        //3. Add 1 Way swap Indicator
        let swRatio2 = 13.7 / 414 as CGFloat
        
        let swapSymbWidth2 = swRatio2 * screenWidth
        let swapSymbHeight2 = swapSymbWidth2
        let swapSymbX2 = cell.bounds.maxX - unitSide - swapSymbWidth2
        let swapSymbY2 = unitSide
        
        let swapSymbFrame2 = CGRect(x: swapSymbX2, y: swapSymbY2, width: swapSymbWidth2, height: swapSymbHeight2)
        let swapSymbView2 = UIImageView(frame: swapSymbFrame2)
        swapSymbView2.image = UIImage(named: "1WaySwapGridSymb_Outgoing_N")!
        
        swapSymbView2.layer.shadowOpacity = 0.4
        swapSymbView2.layer.shadowOffset = CGSize(width: 0, height: 0)
        swapSymbView2.layer.shadowRadius = 0.8
        
        cell.contentView.addSubview(swapSymbView2)
        
        //.isHidden property will change to false when 2Way swaps are detected
        cell.swapSymbView2 = swapSymbView2
        cell.swapSymbView2.isHidden = true
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screen = UIScreen.main.bounds
        
        //adjust height
        let screenHeight = screen.height
        
        //adjust center
        let screenWidth = screen.width
        
        let insetRatio = 9/414 as CGFloat
        let insetSpacing = insetRatio * screenWidth
        
        //cell
        let cellDimension = 182/126 as CGFloat
        let cellWidth = (screenWidth - (4 * insetSpacing)) / 3.00
        let cellHeight = cellWidth * cellDimension
        
        //print("cell height is,", cellHeight, "cell cellWidth is,", cellWidth)
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let screen = UIScreen.main.bounds
        //adjust height
        let screenHeight = screen.height
        
        //adjust center
        let screenWidth = screen.width
        
        let insetRatio = 9/414 as CGFloat
        let insetSpacing = insetRatio * screenWidth
        
        let top = 0.00 as CGFloat
        let bottom = 0.00 as CGFloat
        
        let left = insetSpacing
        let right = insetSpacing
        
        let edgeInsets = UIEdgeInsetsMake(top, left, bottom, right)
        
        return edgeInsets
    }
    
    
    
    
    
    
    
    //MARK: Chat Request LOADER Methods
    
    
    

    
    
    //Chat Request Variables
    var chatRequestPermissionsDictionary = [String:String]()
    var senderUIDtoChatRequestIDExtrapDict = [String:String]()
    var selfInitiatedChatThreadIDsDictionary = [String : String]()
    
    //Chat Control Flow Variables
    var existingChatThreadID: String?
    var preexistingChatRequest: Bool? ; var preexistingChatRequestKey: String? ; var requestInitiatedBySelf: Bool?
    var selectedUserResponseState = "" //True (Accepted), False (Pending), Nil (Rejected/Ignored)
    
    
    func observeActiveChatRequests() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _chatRequestsRefHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
             
            guard let newChatRequest = snapshot.value as? [String:String] else { return }
            
            //Key
            let newChatRequestID = snapshot.key
            //print("CVC newChatRequestID", newChatRequestID)
            //UID
            guard let chatRequestSenderUID = newChatRequest["chatRequestSenderUID"] else { return }
            guard let chatRequestRecipientUID = newChatRequest["chatRequestRecipientUID"] else { return }
            
            //First Name
            guard let chatRequestSenderFirstName = newChatRequest["chatRequestSenderFirstName"] else { return }
            guard let chatRequestRecipientFirstName = newChatRequest["chatRequestRecipientFirstName"] else { return }
            
            //DidOpen
            guard let chatRequestSenderDidOpen = newChatRequest["chatRequestSenderDidOpen"] else { return }
            guard let chatRequestRecipientDidOpen = newChatRequest["chatRequestRecipientDidOpen"] else { return }
            
            //PhotoURL
            guard let chatRequestSenderDefaultPhotoURL = newChatRequest["chatRequestSenderDefaultPhotoURL"] else { return }
            guard let chatRequestRecipientDefaultPhotoURL = newChatRequest["chatRequestRecipientDefaultPhotoURL"] else { return }
            
            //Visus
            guard let chatRequestSenderVisus = newChatRequest["chatRequestSenderVisus"] else { return }
            guard let chatRequestRecipientVisus = newChatRequest["chatRequestRecipientVisus"] else { return }
            
       
            //ChatMetaData
//            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return }
//            guard let lastMessageString = newChatRequest["lastMessageString"] else { return }
            //RECONSTITUE
            guard let chatThreadDoesExist = newChatRequest["chatThreadDoesExist"] else { return } //bool
            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return } //string
            guard let lastMessageString = newChatRequest["lastMessageString"] else { return } //string
        
            //
//            if chatThreadDoesExist == "true" {
//                self.observeLastMessages(withThreadID: "\(existingChatThreadID)")
//            }
            //
            
            //RequestStatus
            guard let chatRequestStatus = newChatRequest["chatRequestStatus"] else { return }
            
            //Swapped?
            guard let didSwap = newChatRequest["didSwap"] else { return }

            //for ProfileVC
            if chatRequestSenderUID == selfUID {
                
                self.selfInitiatedChatRequestsIDsDict[chatRequestRecipientUID] = newChatRequestID
                self.chatRequestPermissionsDictionary[chatRequestRecipientUID] = chatRequestStatus
                
            } else {
                //add incoming request to chatPermissionsDictionary with default value as false
                self.chatRequestPermissionsDictionary[chatRequestSenderUID] = chatRequestStatus
                
                //create array that links each chatUserRequester to the IDRequest they initiated
                self.senderUIDtoChatRequestIDExtrapDict[chatRequestSenderUID] = newChatRequestID
            }
            
            //ChatRequests for ChatlistVC
            //CR1. Incoming Chat Requests
            if chatRequestRecipientUID == selfUID && chatRequestStatus == "false" {
                //print("Incoming Chat Request")
    
                var newListObject = [String:String]()
                
                //request info
                newListObject["objectType"] = "chatRequestOtherNew"
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestSenderUID
                newListObject["requestToDisplayName"] = chatRequestSenderFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestSenderVisus
        
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen
                
                //are these duplicates of the above lines? TEST.
                
                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestSenderDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                
                newListObject["chatRequestRecipientFirstName"] = chatRequestRecipientFirstName
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["objectUID"] = chatRequestSenderUID

                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    //do not append objects where users have swapped. These objects are accounted for as SwapObjectTypes. Only append chatRequestsIf for incoming oneWayShow
          
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                    
                    self.setChatNotificationAlert()
                }
                
                //append UID where swapper != selfUID
                if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                }
            }
            
            //CR2. Outgoing Chat Requests
            if chatRequestSenderUID == selfUID && chatRequestStatus == "false" {
                //print("Outgoing Chat Request")

                var newListObject = [String:String]()

                //request info
                newListObject["objectType"] = "chatRequestSelfNew"
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus

                //profile info
                newListObject["requestToUID"] = chatRequestRecipientUID
                newListObject["requestToDisplayName"] = chatRequestRecipientFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestRecipientVisus

                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen

                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = "You"
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen //tracking swapInitiator since sender is self
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["chatRequestRecipientDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                newListObject["objectUID"] = chatRequestRecipientUID

                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(chatRequestRecipientUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestRecipientUID)
                }
            }
            
            //CR3. Incoming Chat Requests Approved
            if chatRequestRecipientUID == selfUID && chatRequestStatus == "true" {
                
                var newListObject = [String:String]()
                
                if chatThreadDoesExist == "false" {
                    newListObject["objectType"] = "otherInitChatApprovedNew"
                    //print("Detected 2way otherInitChatApprovedNew")
                } else if chatThreadDoesExist != "false" {
                    newListObject["objectType"] = "otherInitChatApprovedPre"
                    //print("Detected 2way otherInitChatApprovedPre")
                }
        
                //request info
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestSenderUID
                newListObject["requestToDisplayName"] = chatRequestSenderFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestSenderVisus

                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen

                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestSenderDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["objectUID"] = chatRequestSenderUID

                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    
                    if didSwap == "true" && chatRequestRecipientVisus == "true" && chatRequestSenderVisus == "false" {
                        self.chatListObjectIDs.append(newChatRequestID)
                        self.chatListObjects.append(newListObject)
                        
                        if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                            self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                        }
                    }
                    
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                }
                
            }
            
            //CR4. Outgoing Chat Requests Approved
            if chatRequestSenderUID == selfUID && chatRequestStatus == "true" {
                //print("Detected CHAT case where someone approved my previously initiated request")
                
                var newListObject = [String:String]()
                
                if chatThreadDoesExist == "false" {
                    //print("CVC setting selfInitChatApprovedNew")
                    newListObject["objectType"] = "selfInitChatApprovedNew"
                } else if chatThreadDoesExist == "true" {
                    //print("CVC setting selfInitChatApprovedPre")
                    newListObject["objectType"] = "selfInitChatApprovedPre"
                }

                //request info
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestRecipientUID
                newListObject["requestToVisus"] = chatRequestRecipientVisus
                newListObject["requestToDisplayName"] = chatRequestRecipientFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString

                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen

                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                //tracking swapInitiator since sender is self //careful withthese, need to reset all to default getter values
                
                newListObject["chatRequestRecipientFirstName"] = chatRequestRecipientFirstName
                newListObject["chatRequestRecipientDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                
                newListObject["objectUID"] = chatRequestRecipientUID

                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    if didSwap == "true" && chatRequestRecipientVisus == "true" && chatRequestSenderVisus == "false" {
                        self.chatListObjectIDs.append(newChatRequestID)
                        self.chatListObjects.append(newListObject)
                        
                        if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                            self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                        }
                    }
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                if !self.allUniqueUIDsinChatList.contains(chatRequestRecipientUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestRecipientUID)
                }
            }
            
            //print("END observeActiveChatRequests() chatListObjects now contains", self.chatListObjects)
            
            }, withCancel: { error in
                self.ref.child("globalChatRequests/\(user.uid)").removeAllObservers()
                //print("WILL PRINT CANCEL ERROR")
                //print("C+ANCELERRORIS,", error.localizedDescription)
                self.forceLogout()
        })
    }
    
   func forceLogout() {
        //print("forceLogout  detected")
        
        if let user = Auth.auth().currentUser {
            let selfUID = user.uid
//            self.ref.child("users/\(selfUID)").removeValue()
            
            //ATTENTION: Deleted users should NEVER be able to write to GFLocation in DB, otherwise will screw up program
//            self.ref.child("userLocationGF/\(selfUID)").removeValue()
            
            do {
                try Auth.auth().signOut()
                //print("signout successful")
                self.performSegue(withIdentifier: "cvcToHome", sender: self)
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
                self.activityIndicatorView?.stopAnimating()
            }
        }
    }
    
    func observeChatRequestStatusChanges() {
        //print("func observeSwapRequestStatusChanges")
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _chatRequestChangesRefHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            
            guard let newChatRequest = snapshot.value as? [String:String] else { return }
            
            //Key
            let existingChatRequestID = snapshot.key
            //print("CVC existingChatRequestID", existingChatRequestID)

            //UID
            guard let chatRequestSenderUID = newChatRequest["chatRequestSenderUID"] else { return }
            //print("Change1")
            guard let chatRequestRecipientUID = newChatRequest["chatRequestRecipientUID"] else { return }
            //print("Change2")

            //First Name
            guard let chatRequestSenderFirstName = newChatRequest["chatRequestSenderFirstName"] else { return }
            //print("Change3")

            guard let chatRequestRecipientFirstName = newChatRequest["chatRequestRecipientFirstName"] else { return }
            //print("Change4")

            //DidOpen
            guard let chatRequestSenderDidOpen = newChatRequest["chatRequestSenderDidOpen"] else { return }
            //print("Change5")

            guard let chatRequestRecipientDidOpen = newChatRequest["chatRequestRecipientDidOpen"] else { return }
            //print("Change6")

            //PhotoURL
            guard let chatRequestSenderDefaultPhotoURL = newChatRequest["chatRequestSenderDefaultPhotoURL"] else { return }
            //print("Change7")

            guard let chatRequestRecipientDefaultPhotoURL = newChatRequest["chatRequestRecipientDefaultPhotoURL"] else { return }
            //print("Change8")

            //status
            guard let modifiedChatRequestStatus = newChatRequest["chatRequestStatus"] else { return }
            //print("Change9")

            //Chat MetaData
            guard let chatThreadDoesExist = newChatRequest["chatThreadDoesExist"] else { return } //bool
            //print("Change10")

            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return } //string
            //print("Change11")

            guard let lastMessageString = newChatRequest["lastMessageString"] else { return } //string
            //print("Change12")
            
            guard let didSwap = newChatRequest["didSwap"] else { return }
            //print("Change12 Changes, didSwap,", didSwap)
            
            if self.chatListObjectIDs.contains(existingChatRequestID) {
                //print("chatListObjectIDs contains existingChatRequestID")
                
                let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                    dictionaryOfInterest["chatRequestID"] == existingChatRequestID
                })
                
                //...Update chatListObjects ChatRequestInfo
                //When I approve someone's previous request
                if chatRequestRecipientUID == self.selfUID! &&  modifiedChatRequestStatus == "true"  {
                    //print("ACCIO 2 chatListObjects", self.chatListObjects)
                    
                    if chatRequestRecipientDidOpen == "false" {
                        //print("CVC otherInitChatApprovedNew")
                        (self.chatListObjects[i!])["objectType"] = "otherInitChatApprovedNew"
                      
                    } else if chatRequestRecipientDidOpen == "true" {
                        //print("CVC otherInitChatApprovedPre")
                        (self.chatListObjects[i!])["objectType"] = "otherInitChatApprovedPre"
                    }
                    
                    (self.chatListObjects[i!])["chatThreadDoesExist"] = chatThreadDoesExist
                    (self.chatListObjects[i!])["existingChatThreadID"] = existingChatThreadID
                    (self.chatListObjects[i!])["lastMessageString"] = lastMessageString
                    
                    //print("CVC updating chatRequestStatus")
                    (self.chatListObjects[i!])["chatRequestStatus"] = modifiedChatRequestStatus
                    
                    //update chatRequestStatus - for UPVC
                    //for ProfileVC
                    if chatRequestSenderUID == selfUID {
                        self.chatRequestPermissionsDictionary["\(chatRequestRecipientUID)"] = modifiedChatRequestStatus
                    } else {
                        self.chatRequestPermissionsDictionary["\(chatRequestSenderUID)"] = modifiedChatRequestStatus
                    }
                    
                    //lastMessagesObserver
                    self.observeLastMessages(withThreadID: existingChatThreadID)
                }
                
                //Self initiated swap requests that have been approved by other
                if chatRequestSenderUID == selfUID  && modifiedChatRequestStatus == "true" {
                    //that have been opened
                    if chatRequestSenderDidOpen == "false" {
                        //print("GRIDVC CHATREQUESTDIDCHANGE WITH FALSE")
                        (self.chatListObjects[i!])["objectType"] = "selfInitChatApprovedNew"
                        
                    } else if chatRequestSenderDidOpen == "true" {
                        //print("GRIDVC CHATREQUESTDIDCHANGE WITH TRUE")
                        (self.chatListObjects[i!])["objectType"] = "selfInitChatApprovedPre"
                    }
                    
                    (self.chatListObjects[i!])["requestStatus"] = modifiedChatRequestStatus
                    (self.chatListObjects[i!])["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                    (self.chatListObjects[i!])["selfDidOpen"] = chatRequestSenderDidOpen
                    
                    (self.chatListObjects[i!])["chatThreadDoesExist"] = chatThreadDoesExist
                    (self.chatListObjects[i!])["existingChatThreadID"] = existingChatThreadID
                    (self.chatListObjects[i!])["lastMessageString"] = lastMessageString
                    (self.chatListObjects[i!])["chatRequestStatus"] = modifiedChatRequestStatus

                    //update chatRequestStatus - for UPVC
                    //for ProfileVC
                    if chatRequestSenderUID == selfUID {
                        self.chatRequestPermissionsDictionary["\(chatRequestRecipientUID)"] = modifiedChatRequestStatus
                    } else {
                        self.chatRequestPermissionsDictionary["\(chatRequestSenderUID)"] = modifiedChatRequestStatus
                    }

                    self.observeLastMessages(withThreadID: existingChatThreadID)
                }
                //print("END observeActiveChatRequestChanges() chatListObjects now contains", self.chatListObjects)
            }
            
            ///////////////////
            //update user info since chatListObject acts as userProfileInfoDict for GridVC->ChatListVC->ChatScreenVC
            if self.chatListObjectIDs.contains(existingChatRequestID) {
                let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                    dictionaryOfInterest["chatRequestID"] == existingChatRequestID
                })
                if chatRequestSenderUID == selfUID {
                    //update with new recipient values
                    (self.chatListObjects[i!])["swapWithDefaultURL"] = chatRequestRecipientDefaultPhotoURL
                    (self.chatListObjects[i!])["swapWithDisplayName"] = chatRequestRecipientFirstName
                    
                } else {
                    //when other is initiator, update with new initiator values
                    (self.chatListObjects[i!])["chatRequestWithDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                    (self.chatListObjects[i!])["requestToDisplayName"] = chatRequestSenderFirstName
                }
            }
            //print("END observeActiveChatRequestChanges() chatListObjects now contains", self.chatListObjects)
        })
    }
    
    ///////////////////////////////////////////////CHAT THREAD METADATA//////////////////////////////////////////////////////////////////////
    //C. MARK: Chat Thread MetaData
    
    var allChatThreadIDsDictionary = [String : [String: [String: AnyObject]]]()
    
    //where Key is UID & Val is ChatThreadID
    var uidtoChatThreadIDIndexDictionary = [String: String]()
    var selfInitiatedChatRequestsIDsDict = [String:String]()

    /*
     chatThreadData["absExistingMessageThreadID"] = "false"
     chatThreadData["chatInitiatorDisplayName"] = chatInitiatorDisplayName
     chatThreadData["chatInitiatorPhotoURL"] = chatInitiatorPhotoURL
     chatThreadData["chatRecipientDisplayName"] = chatRecipientDisplayName
     chatThreadData["chatRecipientPhotoURL"] = chatRecipientPhotoURL
     */
    
    var lastMessagesbyUID = [String: String]()
    var didReadLastMessageForUID = [String : String]()
    var lastMessagesPaths = [DatabaseReference]()
    
    //
    var chronoDict = [String : Double]() { didSet {
        self.sortChronoDict_byTimeStamp() } }
    
    var sortedChronoDict = [(key: String, value: Double)]()
    
    //Ideally call the below function after all messageThreadID's have been set
    
    func sortChronoDict_byTimeStamp() {
        let sortedChronoDict = self.chronoDict.sorted(by: { $0.value > $1.value } )
        self.sortedChronoDict = sortedChronoDict
    }
    
    //Ideally call this after all chatListObjects have been set && after sortLastMessages_byTimeStamp has completed processing
    func sortChatList_ByLastMessagesArrayOrder() {
        
        self.sortChronoDict_byTimeStamp()
        
        //will need to append objectMeta in chatList to chronoDict and pass back & forth chronoDict
        var sortedUIDKeys = [String]()
        
        for dict in sortedChronoDict {
            sortedUIDKeys.append(dict.key)
        }

        for listObject in chatListObjects {
            if let objectUID = listObject["objectUID"] {
                if !sortedUIDKeys.contains(objectUID) {
                    //should sortedKeys not contain objectUID, append objectUID to end of list
                    sortedUIDKeys.append(objectUID)
                }
            } else {
                //if chatList does not contain objectUID, then data will be corrupted. Exit method call & return the original chatList
                return
            }
        }
        
        let sortedChatList = chatListObjects.sorted{ (sortedUIDKeys.index(of: $0["objectUID"]!))! < (sortedUIDKeys.index(of: $1["objectUID"]!))! }
        
        if self.chatListObjects.count == sortedChatList.count {
            //print("WILLUPDATECHATLISTSORTED")
            self.chatListObjects = sortedChatList
        }
    }
    
    var uidKeysInLastMessagesDict_Array = [String]() { didSet {
        
        
//        self.observeActiveSwapRequests(withSourceCall: "didSetSelfMessageThreadCount1")


//        rest if backrun
        
        /*
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.observeActiveSwapRequests(withSourceCall: "didSetSelfMessageThreadCount1")
        }
        
        if let selfMessageThreadCount = self.selfMessageThreadCount {

            //print("uidKeysInLastMessagesDict_Array.count,", uidKeysInLastMessagesDict_Array.count, "vs.selfMessageThreadCount", selfMessageThreadCount)

            if uidKeysInLastMessagesDict_Array.count >= selfMessageThreadCount {
                //Note: observeActiveSwapRequests() WILLFIRE when lastMessagesArray contains all UIDs chatting with self. This is so to make sure that LastMessagesArray contains all UID's before observeActiveSwapRequests() fires, since observeActiveSwapRequests() filters chatListObjects based on UID's in lastMessagesArray.
                //print("WILLCALLSWAPS uidKeysInLastMessagesDict_Array")
                self.observeActiveSwapRequests(withSourceCall: "didSetSelfMessageThreadCount2")
            }
        }
        */
    }}
    
    var grid_LastMessages_Listeners = [DatabaseReference]()

    var observingLastMessageForChatThreads_Array = [String]()
    
    /*
     1. User receives a new message
 How will server observe new messages per device.
     If user receives a new message, their chatThread will be updated & chatThread metadata will be updated as well.
     
     If we want to fire the observer on .onWrite event, then observer needs to be attached to node where last message is written, and last message contains a registration token.
     
     Therefore, better to also add last messages to new node by device registration token, and then push that payload to user device
     vs.
     
     Structures:
     1. Parent Node == DeviceTokenID -> messageBody: "" + messageTimeStamp: "" + messageSenderName: "" + messageSenderID: ""
     2. Parent Node == threadID -> DeviceToken: "" + message + ...
     

     */
    
    
    func observeLastMessages(withThreadID: String) {
        
        let path = self.ref.child("globalChatThreadMetaData/\(withThreadID)")
        self.lastMessagesPaths.append(path)
        
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
    
        if !self.observingLastMessageForChatThreads_Array.contains(withThreadID) {
            //continue
            
        } else {
            //trip function
            return
        }
        
        self.observingLastMessageForChatThreads_Array.append(withThreadID)
        
        self.ref.child("globalChatThreadMetaData/\(withThreadID)").observe(.value , with: { (snapshot: DataSnapshot) in
            
            let refPath = self.ref.child("globalChatThreadMetaData/\(withThreadID)")
            self.grid_LastMessages_Listeners.append(refPath)
            
            guard let messageThreadBody = snapshot.value as? [String:String] else { return }
        
            guard let lastMessage = messageThreadBody["chatMessage"] else { return }
            guard let messageRecipientUID = messageThreadBody["messageRecipientUID"] else { return }
            guard let messageSenderUID = messageThreadBody["messageSenderUID"] else { return }
            guard let recipientDidReadLastMessage = messageThreadBody["recipientDidReadLastMessage"] else { return }
            guard let messageTimeStamp = messageThreadBody["messageTimeStamp"] else { return }

            //Note: As of Jul 21, 2018, LASTMESSAGES Array is supplemented by incomingOneWaySwapRequests. Latter object type appends dummy [UID] : lastmessage == "" to ensure that lastMessages contains allUids that are also contained in chatListObjects.
            
            if messageSenderUID == selfUID {
                //last message
                self.lastMessagesbyUID["\(messageRecipientUID)"] = lastMessage
                self.ref_SelfModelController.lastMessagesbyUID["\(messageRecipientUID)"] = lastMessage
                //new for didRead last messages
                self.ref_SelfModelController.didReadLastMessageForUID["\(messageRecipientUID)"] = "true"
                
//                //for timestamp
                self.updateChronoDict(forObjectUID: messageRecipientUID, withObjectTimeStamp: messageTimeStamp)
                
            } else if messageSenderUID != selfUID {
                //last message
                self.lastMessagesbyUID["\(messageSenderUID)"] = lastMessage
                self.ref_SelfModelController.lastMessagesbyUID["\(messageSenderUID)"] = lastMessage

                //new for didRead last messages
                self.ref_SelfModelController.didReadLastMessageForUID["\(messageSenderUID)"] = recipientDidReadLastMessage
                
                //for timestamp
                self.updateChronoDict(forObjectUID: messageSenderUID, withObjectTimeStamp: messageTimeStamp)
            }

            //
//            if !self.didReadLastMessageForUID.values.contains("false") {
//                //clear chatAlert when all values are true
//                self.clearChatNotificationAlert()
//            } else {
//                self.setChatNotificationAlert()
//            }
            
//            if messageRecipientUID == self.selfUID! {
//                if recipientDidReadLastMessage == "false" {
//                    self.setChatNotificationAlert()
//
//                } else if recipientDidReadLastMessage == "true" {
//                    self.clearChatNotificationAlert()
//                }
//            }
        })
    }
    
    func updateChronoDict(forObjectUID: String, withObjectTimeStamp: String) {
        //print("updateChronoDict")
        
        let newTimeStamp = Double(withObjectTimeStamp)!
        
        //for timestamp
        if self.chronoDict.keys.contains(forObjectUID) {
            //chronoDict may already contain instance appended from swapObject, if so, compare stamps & append larger value
            let existingTimeStamp = self.chronoDict["\(forObjectUID)"]!
        
            if newTimeStamp > existingTimeStamp {
                self.chronoDict[forObjectUID] = Double(newTimeStamp)
            } else {
                //
            }
            
            //print("chronoStamp doesExist,", existingTimeStamp)
            
        } else {
            //print("chronoStamp doesNotExist")
            self.chronoDict[forObjectUID] = newTimeStamp
        }
        
        if !self.uidKeysInLastMessagesDict_Array.contains(forObjectUID) {
            self.uidKeysInLastMessagesDict_Array.append(forObjectUID)
        }
    }
    
    var listenerPaths = [DatabaseReference]()
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //TODO:
    /*
     1. separate observer handlers from parsers and builder methods
     2. execute childAdded & childChanged. Let childChanged observe for when chatThreadIDActive turns to true, so that these items can be appended LIVE
 */
    
    //MARK: Object Types & Corr Keys
    var swapObjectTypes_2Way = ["swapRequestOtherNew", "swapRequestSelfNew", "otherInitSwapApprovedNew", "otherInitSwapApprovedPre", "selfInitSwapApprovedNew", "selfInitSwapApprovedPre"]
    var corr_Keys_2Way = ["swapWithUID", "swapWithDefaultPhotoURL", "swapWithVisus"]
    
    var swapObjectTypes_1Way = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre", "outgoingOneWayShow", "incomingOneWayShow"]
    var corr_Keys_1Way = ["revealToUID", "revealToDefaultPhotoURL", "revealToVisus"]
    
    var genChatObjectTypes = ["generalChatThread"]
    var corr_Keys_Gen = ["chatWithUID", "chatWithPhotoURL", "chatWithVisus"]
    
    var _genChatHandle: DatabaseHandle!
    var _genChatHandleChanges: DatabaseHandle!
    
    var selfMessageThreadCount: Int?
    
    func observeActiveMessageThreads() {
        //print("viewDidLoad observeActiveMessageThreads")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //set count as threshold
//        self.ref.child("users/\(user.uid)/myActiveChatThreads").observeSingleEvent(of: .value) { (snapshot: DataSnapshot) in
//            //ATTN: This will include chatThreads that aren't active
//            //print("observeActiveMessageThreads count", snapshot.childrenCount)
//            //test at 0, error
//            let intCount = Int(snapshot.childrenCount)
//            if intCount != 0 {
//                self.selfMessageThreadCount = intCount
//            } else {
//                self.observeActiveSwapRequests(withSourceCall: "observeActiveMessageThreads")
//            }
//        }
    
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        self._genChatHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded , with: { (snapshot: DataSnapshot) in
            //print("CVC: observeActiveMessageThreads snapshot is,", snapshot)

            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
            
            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
            //        //print("observeActiveMessageThreads is,", chatThreadBody)
            
            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
            //print("CHAIN1. threadIDActive", threadIDActive)
            
            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            //print("CHAIN2. chatInitiatorUID", chatInitiatorUID)
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
            //print("CHAIN3. chatRecipientUID", chatRecipientUID)
            
            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            //print("CHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
            //print("CHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
            
            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
            //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
            
            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
            //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
            
            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
            //print("CHAIN10. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let didAskShow = chatThreadBody["didAskShow"] else { return }
            //print("CHAIN11. chatThreadFromRequest", chatThreadFromRequest)
            
            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
            //print("CHAIN12. lastMessage", lastMessage)
            
            guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
            
            guard chatIsActive != "nil" else { return }
            
            var generalChatObject = [String : String]()
            generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
            
            //            struct GeneralChatObject {
            //                let chatWithDisplayName: String
            //                let chatWithDisplayNameMaybe: String?
            //            }
            //
            //            let generalChatObject = GeneralChatObject(chatWithDisplayName: chatRecipientDisplayName)
            
            //X. build dictionary of chatThreads
            if threadIDActive == "true" {
                
                guardPoint:
                    if chatThreadFromRequest == "false" {
                    
                    guard didAskShow != "true" else {
                        //print("GUARD: object is not a one-way swap")
                        break guardPoint
                    }
                    
                    var generalChatObject = [String : String]()
                    
                    if chatInitiatorUID == selfUID  { //when chatInitiator is self, we need he chatlistinfo to be of other
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        //user info
                        generalChatObject["chatWithUID"] = chatRecipientUID
                        generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatRecipientPhotoURL
                        generalChatObject["chatWithVisus"] = chatRecipientVisus
                        
                        //chatMetaData
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["lastMessage"] = lastMessage
                        generalChatObject["objectUID"] = chatRecipientUID
                        
                        if !self.allUniqueUIDsinChatList.contains(chatRecipientUID) {
                            self.allUniqueUIDsinChatList.append(chatRecipientUID)
                            self.chatListObjects.append(generalChatObject)
                            
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Added"), object: self, userInfo: generalChatObject)
                        }
                        
                    } else if chatInitiatorUID != selfUID {
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        generalChatObject["chatWithUID"] = chatInitiatorUID
                        generalChatObject["chatWithDisplayName"] = chatInitiatorDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatInitiatorPhotoURL
                        generalChatObject["chatWithVisus"] = chatInitiatorVisus
                        
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["lastMessage"] = lastMessage
                        generalChatObject["objectUID"] = chatInitiatorUID
                        
                        if !self.allUniqueUIDsinChatList.contains(chatInitiatorUID) {
                            self.allUniqueUIDsinChatList.append(chatInitiatorUID)
                            self.chatListObjects.append(generalChatObject)
                            
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Added"), object: self, userInfo: generalChatObject)
                        }
                    }
                }
            }
            
            //print("WillNowGuard threadIDActive")
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            guard threadIDActive == "true" else { return }
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            let globalRequestsChatThreadID = newChatThreadID
            //print("CHAIN12. globalRequestsChatThreadID", globalRequestsChatThreadID)
            
            //print("CHAIN13. sending to last message observer", globalRequestsChatThreadID)
            self.observeLastMessages(withThreadID: globalRequestsChatThreadID)
            
            //
            self.buildChatThreadID(withInfo: snapshot, globalRequestsChatThreadID: globalRequestsChatThreadID)
            
            //print("END GENERALCHATTHREADOBSERVER,", self.chatListObjects.count)


        })
        
        let refPath = self.ref.child("users/\(user.uid)/myActiveChatThreads")
        self.listenerPaths.append(refPath)
    }
    
    
//    newListObject["oneWayRequestID"] = newSwapRequestID
//    newListObject["requestStatus"] = swapRequestStatus
//
//    //Profile Info
//    newListObject["revealToUID"] = swapRecipientID
//    newListObject["revealToDisplayName"] = swapRecipientFirstName
//    newListObject["revealToDefaultPhotoURL"] = swapRecipientDefaultURL
//    newListObject["revealToVisus"] = swapRecipientVisus
//
//    //Chat Info
//    newListObject["chatThreadID"] = chatThreadID
//    newListObject["lastMessage"] = lastMessage
//
//    newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
//    newListObject["requestInitiatorFirstName"] = "You"
//    //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
//
//    //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
//    newListObject["objectUID"] = swapRecipientID
    
    @objc func convertIncomingSwapToChat(_ notification: Notification) {

        guard notification.name.rawValue == "convertIncomingSwapToChat" else { return }

        guard let forUID = notification.userInfo?["forUserID"] as? String else { return }
        
        //Required chatObject values
        var chatWithUID = ""
        var chatWithDisplayName = ""
        var chatWithPhotoURL = ""
        var chatWithVisus = ""
        var chatThreadID = ""
        var lastMessage = ""
        var objectUID = ""
        
        //print("!!!chatListObjects", chatListObjects)
        //retrieve old swap object
        let chatListIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
           userInfoObject["objectUID"] == forUID
        })
        
        if let chatListIndex = chatListIndex {
            
            let swapObject = self.chatListObjects[chatListIndex]
            
            guard let uid = swapObject["revealToUID"] else { return }
            chatWithUID = uid
            guard let name = swapObject["revealToDisplayName"] else { return }
            chatWithDisplayName = name
            guard let url = swapObject["revealToDefaultPhotoURL"] else { return }
            chatWithPhotoURL = url
            guard let visus = swapObject["revealToVisus"] else { return }
            chatWithVisus = visus
            guard let chatID = swapObject["chatThreadID"] else { return }
            chatThreadID = chatID
            guard let lm = swapObject["lastMessage"] else { return }
            lastMessage = lm
            guard let objectID = swapObject["objectUID"] else { return }
            objectUID = objectID
        } else {
            return
        }
        
        //instantiate new object
        //user info
        var generalChatObject = [String : String]()

        generalChatObject["chatWithUID"] = chatWithUID
        generalChatObject["chatWithDisplayName"] = chatWithDisplayName
        generalChatObject["chatWithPhotoURL"] = chatWithPhotoURL
        generalChatObject["chatWithVisus"] = chatWithVisus

        //chatMetaData
        generalChatObject["chatThreadID"] = chatThreadID
        generalChatObject["lastMessage"] = lastMessage
        generalChatObject["objectUID"] = objectUID
        
        //remove old instance
        self.chatListObjects.remove(at: chatListIndex!)
        if let index = self.allUniqueUIDsinChatList.index(of: forUID) {
            self.allUniqueUIDsinChatList.remove(at: index)
        }
        
        //append new instance
        if let lastMessage = self.lastMessagesbyUID["\(forUID)"] {
            if lastMessage != "" {
                self.chatListObjects.append(generalChatObject)
                self.allUniqueUIDsinChatList.append(forUID)
            } else {
//                if self.uidtoChatThreadIDIndexDictionary.keys.contains(forUID) {
//                    self.uidtoChatThreadIDIndexDictionary.removeValue(forKey: forUID)
//                }
            }
        }
    }

    func observeActiveMessageThreadChanges() {
        //print("viewDidLoad observeActiveMessageThreadChanges")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        self._genChatHandleChanges = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            //print("GRID VC observeActiveMessageThreadChanges snapshot is,", snapshot)
//            self.observeActiveMessageThreadsHelper(selfUID: selfUID, snapshot: snapshot)
            
            self._genChatHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded , with: { (snapshot: DataSnapshot) in
                //print("CVC: observeActiveMessageThreads snapshot is,", snapshot)
                
                let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
                
                guard let chatThreadBody = snapshot.value as? [String:String] else { return }
                //        //print("observeActiveMessageThreads is,", chatThreadBody)
                
                guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
                //print("CHAIN1. threadIDActive", threadIDActive)
                
                //UID
                guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
                //print("CHAIN2. chatInitiatorUID", chatInitiatorUID)
                guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
                //print("CHAIN3. chatRecipientUID", chatRecipientUID)
                
                //DisplayName
                guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
                //print("CHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
                guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
                //print("CHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
                
                //PhotoURL
                guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
                //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
                guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
                //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
                
                //Visus
                guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
                //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
                guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
                //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
                
                //Property observes whether or not objectType is generalChat or chatRequest
                guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
                //print("CHAIN10. chatThreadFromRequest", chatThreadFromRequest)
                
                guard let didAskShow = chatThreadBody["didAskShow"] else { return }
                //print("CHAIN11. chatThreadFromRequest", chatThreadFromRequest)
                
                //Message MetaData
                guard let lastMessage = chatThreadBody["lastMessage"] else { return }
                //print("CHAIN12. lastMessage", lastMessage)
                
                guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
                guard chatIsActive != "nil" else { return }
                
                var generalChatObject = [String : String]()
                generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                
                //X. build dictionary of chatThreads
                if threadIDActive == "true" {
                    
                    guardPoint:
                        if chatThreadFromRequest == "false" {
                        
                        guard didAskShow != "true" else {
                            //print("GUARD: object is not a one-way swap")
                            break guardPoint
                        }
                        
                        var generalChatObject = [String : String]()
                        
                        if chatInitiatorUID == selfUID  { //when chatInitiator is self, we need he chatlistinfo to be of other
                            generalChatObject["objectType"] = "generalChatThread"
                            
                            //user info
                            generalChatObject["chatWithUID"] = chatRecipientUID
                            generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                            generalChatObject["chatWithPhotoURL"] = chatRecipientPhotoURL
                            generalChatObject["chatWithVisus"] = chatRecipientVisus
                            
                            //chatMetaData
                            generalChatObject["chatThreadID"] = newChatThreadID
                            generalChatObject["lastMessage"] = lastMessage
                            generalChatObject["objectUID"] = chatRecipientUID
                            
                            if !self.allUniqueUIDsinChatList.contains(chatRecipientUID) {
                                self.allUniqueUIDsinChatList.append(chatRecipientUID)
                                self.chatListObjects.append(generalChatObject)
                                
//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Changed"), object: self, userInfo: generalChatObject)
                            }
                            
                        } else if chatInitiatorUID != selfUID {
                            generalChatObject["objectType"] = "generalChatThread"
                            
                            generalChatObject["chatWithUID"] = chatInitiatorUID
                            generalChatObject["chatWithDisplayName"] = chatInitiatorDisplayName
                            generalChatObject["chatWithPhotoURL"] = chatInitiatorPhotoURL
                            generalChatObject["chatWithVisus"] = chatInitiatorVisus
                            
                            generalChatObject["chatThreadID"] = newChatThreadID
                            generalChatObject["lastMessage"] = lastMessage
                            generalChatObject["objectUID"] = chatInitiatorUID
                            
                            if !self.allUniqueUIDsinChatList.contains(chatInitiatorUID) {
                                self.allUniqueUIDsinChatList.append(chatInitiatorUID)
                                self.chatListObjects.append(generalChatObject)
                                
//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Changed"), object: self, userInfo: generalChatObject)
                            }
                        }
                    }
                }
                
                //print("WillNowGuard threadIDActive")
                ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                guard threadIDActive == "true" else { return }
                ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                
                let globalRequestsChatThreadID = newChatThreadID
                //print("CHAIN12. globalRequestsChatThreadID", globalRequestsChatThreadID)
                
                //print("CHAIN13. sending to last message observer", globalRequestsChatThreadID)
                self.observeLastMessages(withThreadID: globalRequestsChatThreadID)
                
                //
                self.buildChatThreadID(withInfo: snapshot, globalRequestsChatThreadID: globalRequestsChatThreadID)
                
                //print("END GENERALCHATTHREADOBSERVER,", self.chatListObjects.count)
                
                
            })
            
            let refPath = self.ref.child("users/\(user.uid)/myActiveChatThreads")
            self.listenerPaths.append(refPath)
            
        })
    }
    
    var activeMessageThreads = [String : String]()
    
    /*
    func observeActiveMessageThreadsHelper(selfUID: String, snapshot: DataSnapshot) {
        self._genChatHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded , with: { (snapshot: DataSnapshot) in
            //print("CVC: observeActiveMessageThreads snapshot is,", snapshot)
            
            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
            
            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
            //        //print("observeActiveMessageThreads is,", chatThreadBody)
            
            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
            //print("CHAIN1. threadIDActive", threadIDActive)
            
            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            //print("CHAIN2. chatInitiatorUID", chatInitiatorUID)
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
            //print("CHAIN3. chatRecipientUID", chatRecipientUID)
            
            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            //print("CHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
            //print("CHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
            
            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
            //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
            
            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
            //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
            
            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
            //print("CHAIN10. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let didAskShow = chatThreadBody["didAskShow"] else { return }
            //print("CHAIN11. chatThreadFromRequest", chatThreadFromRequest)
            
            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
            //print("CHAIN12. lastMessage", lastMessage)
            
            var generalChatObject = [String : String]()
            generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
            
            //            struct GeneralChatObject {
            //                let chatWithDisplayName: String
            //                let chatWithDisplayNameMaybe: String?
            //            }
            //
            //            let generalChatObject = GeneralChatObject(chatWithDisplayName: chatRecipientDisplayName)
            
            //X. build dictionary of chatThreads
            if threadIDActive == "true" {
                
                guardPoint:
                    if chatThreadFromRequest == "false" {
                    
                    guard didAskShow != "true" else {
                        //print("GUARD: object is not a one-way swap")
                        break guardPoint
                    }
                    
                    var generalChatObject = [String : String]()
                    
                    if chatInitiatorUID == selfUID  { //when chatInitiator is self, we need he chatlistinfo to be of other
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        //user info
                        generalChatObject["chatWithUID"] = chatRecipientUID
                        generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatRecipientPhotoURL
                        generalChatObject["chatWithVisus"] = chatRecipientVisus
                        
                        //chatMetaData
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["lastMessage"] = lastMessage
                        generalChatObject["objectUID"] = chatRecipientUID
                        
                        if !self.allUniqueUIDsinChatList.contains(chatRecipientUID) {
                            self.allUniqueUIDsinChatList.append(chatRecipientUID)
                            self.chatListObjects.append(generalChatObject)
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Added"), object: self, userInfo: generalChatObject)
                        }
                        
                    } else if chatInitiatorUID != selfUID {
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        generalChatObject["chatWithUID"] = chatInitiatorUID
                        generalChatObject["chatWithDisplayName"] = chatInitiatorDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatInitiatorPhotoURL
                        generalChatObject["chatWithVisus"] = chatInitiatorVisus
                        
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["lastMessage"] = lastMessage
                        generalChatObject["objectUID"] = chatInitiatorUID
                        
                        if !self.allUniqueUIDsinChatList.contains(chatInitiatorUID) {
                            self.allUniqueUIDsinChatList.append(chatInitiatorUID)
                            self.chatListObjects.append(generalChatObject)
                            
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "chatList_Added"), object: self, userInfo: generalChatObject)
                        }
                    }
                }
            }
            
            //print("WillNowGuard threadIDActive")
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            guard threadIDActive == "true" else { return }
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            let globalRequestsChatThreadID = newChatThreadID
            //print("CHAIN12. globalRequestsChatThreadID", globalRequestsChatThreadID)
            
            //print("CHAIN13. sending to last message observer", globalRequestsChatThreadID)
            self.observeLastMessages(withThreadID: globalRequestsChatThreadID)
            
            //
            self.buildChatThreadID(withInfo: snapshot, globalRequestsChatThreadID: globalRequestsChatThreadID)
            
            //print("END GENERALCHATTHREADOBSERVER,", self.chatListObjects.count)
            
            
        })
        
        let refPath = self.ref.child("users/\(user.uid)/myActiveChatThreads")
        self.listenerPaths.append(refPath)

    }
    */
    
    func buildChatThreadID(withInfo: DataSnapshot, globalRequestsChatThreadID: String) {
        //print("buildChatThreadID")
        //buildingChatThreadID index
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
            
        //seems like we can replace observer below with simple guard for true. Try later
        self.ref.child("globalChatThreads/\(globalRequestsChatThreadID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
//            //print("I1. snapshot is,", snapshot)
            
            //let newMessageID = snapshot.key
            
            guard let messageBodyDictionary = snapshot.value as? NSDictionary else { return }
//            //print("I2. snapshot dictionary is", messageBodyDictionary)
            
            guard let messageSenderUID = messageBodyDictionary["messageSenderUID"] as? String else { return }
            //print("I3. sender UID is,", messageSenderUID)
            
            guard let messageRecipientUID = messageBodyDictionary["messageRecipientUID"] as? String else { return }
            //print("I4. recipient UID is,", messageRecipientUID)
            
            if messageSenderUID == selfUID {
                //print("I5. this chatThreadID was initiated by self")
                
                self.selfInitiatedChatThreadIDsDictionary[messageRecipientUID] = globalRequestsChatThreadID
                //print("I6.new selfInitiatedChatThreadsDictionary is,", self.selfInitiatedChatThreadIDsDictionary.count)
                
            } else {
                //
            }
            
            //this dictionary contains senderID:chatThreadID key-value pairs, including self
            if !self.uidtoChatThreadIDIndexDictionary.keys.contains(messageSenderUID) { //note: "!" condition reversal
                //print("I7. dictionary does not already contain UID")
                
                self.uidtoChatThreadIDIndexDictionary[messageSenderUID] = globalRequestsChatThreadID
                //print("I8. dictionary is now", self.uidtoChatThreadIDIndexDictionary.count)
                
            } else {
                //print("I9. dictionary contains UID")
            }
            
        })
    }
    
    ///////////////////////////////////////////////didSelect Item PASS //////////////////////////////////////////////////////////////////////
    //D. MARK: Collection View Selector Method
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:IndexPath) {
        
        let userProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        
        self.ref_SelfModelController.finalsBarH = self.statusBarHeight
         self.ref_SelfModelController.distanceToByUID = self.distanceToByUID
        
        //1. Pass Profile Info
        //self info
//        userProfileViewController.myProfileInfoDict = self.myProfileInfoDict
        userProfileViewController.myProfileInfoDict = self.myProfileInfoDict
        userProfileViewController.ref_SelfModelController = self.ref_SelfModelController
        
        //print("GridVC WILLPASS statusBarHeight,", self.statusBarHeight!)
        if let sBarH = self.statusBarHeight {
            userProfileViewController.importedStatusBarHeight = sBarH
        }
        
        //other info
        //print("CVC userDictArray HERE is,", userDictArray)
        let selectedUser = userDictArray[(indexPath as NSIndexPath).row]
        //print("CVC selectedUser HERE is,", selectedUser)
        userProfileViewController.selectedUser = selectedUser
        let selectedUserUID = selectedUser["UID"]!

        //2. Pass Chat Request Info [chatRequestDoesExist?,absExistingChatRequestID, selfInitiatedChatRequest?, chatRequestStatus ]
        //chatRequestDoesExist?, absExistingChatRequestID
        if self.selfInitiatedChatRequestsIDsDict.keys.contains(selectedUserUID)  {
            userProfileViewController.chatRequestDoesExist = true
            userProfileViewController.absExistingChatRequestID = self.selfInitiatedChatRequestsIDsDict[selectedUserUID]!
            //print("sdkjfhskdhf")
        } else if self.chatRequestPermissionsDictionary.keys.contains(selectedUserUID) {
            userProfileViewController.chatRequestDoesExist = true
            userProfileViewController.absExistingChatRequestID = self.senderUIDtoChatRequestIDExtrapDict[selectedUserUID]!
            //print("sdfjkhsdfjsfd")
        } else {
            userProfileViewController.chatRequestDoesExist = false
            userProfileViewController.absExistingChatRequestID = nil
        }
        
        //selfInitiatedChatRequest?
        if selfInitiatedChatRequestsIDsDict.keys.contains(selectedUserUID) {
            userProfileViewController.selfInitiatedChatRequest = true
        } else {
            userProfileViewController.selfInitiatedChatRequest = false
        }
        
        //chatRequestStatus
        if let userResponseState = self.chatRequestPermissionsDictionary[selectedUserUID] {
            //print("CVC PASSING ChatRequestStatus is ", userResponseState)
            userProfileViewController.chatRequestStatus = userResponseState
        } else {
            userProfileViewController.chatRequestStatus = ""
        }
        
        //3.Pass new chatThreadData: [messageThreadIDDoesExist, absExistingMessageThreadID]
        if self.uidtoChatThreadIDIndexDictionary.keys.contains(selectedUserUID) {
            //Incoming Chat Thread
        
            guard let existingChatThreadID = self.uidtoChatThreadIDIndexDictionary[selectedUserUID] else { return }
            userProfileViewController.messageThreadIDDoesExist = true
            userProfileViewController.absExistingMessageThreadID = existingChatThreadID
            
        } else if self.selfInitiatedChatThreadIDsDictionary.keys.contains(selectedUserUID) {
            //Outgoing Chat Thread
            
            guard let existingChatThreadID = self.selfInitiatedChatThreadIDsDictionary[selectedUserUID] else { return }
            userProfileViewController.messageThreadIDDoesExist = true
            userProfileViewController.absExistingMessageThreadID = existingChatThreadID
            
        } else {
            //NO Thread
            userProfileViewController.messageThreadIDDoesExist = false
        }
    
//        var chatListObjects = [[String : String]]()
//        var chatListObjectIDs = [String]() //Array of unique list object ID's (swapID, chatRequestID, or ThreadID)
//        var allUniqueUIDsinChatList = [String]()
        userProfileViewController.chatListObjects = self.chatListObjects
        userProfileViewController.chatListObjectIDs = self.chatListObjectIDs
        userProfileViewController.allUniqueUIDsinChatList = self.allUniqueUIDsinChatList
        userProfileViewController.unlockedUsers = self.unlockedUsers
        userProfileViewController.incomingDismissedUIDs = self.incomingDismissedUIDs
        
        self.present(userProfileViewController, animated: true, completion: nil)
    }

    //MARK: SECTION 3: GVC -> CLVC -> CSVC Configuration
    
    //Chat List Data Struct
    var chatListObjects = [[String : String]]()
    
    var chatListObjectIDs = [String]()
    var allUniqueUIDsinChatList = [String]()
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MARK: Chat Request Objects Configuration
    //CHAT REQUEST DATA retrieved in active chat Requests observer

    
    var chatRequestObjectTypes = ["chatRequestOtherNew", "chatRequestSelfNew", "otherInitChatApprovedNew", "otherInitChatApprovedPre", "selfInitChatApprovedNew", "selfInitChatApprovedPre"]
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MARK: Swap Request Objects Configuration
    
    //Swap Request Variables
    var selfInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID == selfUID, Maps ["otherUID" : "requestID"]
    var otherInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID != selfUID, Maps ["initiatorUID" : "requestID"]
    
    var swapRequestStatusDictionary = [String:String]()
    var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
    var selfInitiatedSwapRequest: Bool?
    var swapRequestStatus: String?
    
    var swapObjectTypes = ["swapRequestOtherNew", "swapRequestSelfNew", "otherInitSwapApprovedNew", "otherInitSwapApprovedPre", "selfInitSwapApprovedNew", "selfInitSwapApprovedPre"]
    
    //contains UIDs of unlocked users
    var unlockedUsers = [String]()
    
    
    /*var swapObjectKeys = [swapInitiatorID, swapInitiatorFirstName, swapInitiatorDefaultURL, swapInitiatorPhotoURL, swapInitiatorDidOpen, swapInitiatorVisus, swapRecipientID, swapRecipientFirstName, swapRecipientDefaultURL, swapRecipientPhotoURL, swapRecipientDidOpen, swapRecipientVisus, swapRequestStatus] */

    //Swap observer function: (1) If a user is selected, observer gets the SwapIDrequest (if any) associated with select users to pass information to UPVC and CSVC as needed.
    
    //ATTENTION: OBSERVE UNLOCKED USERS WILL CALL PHOTOVAULT FUNCTION WHEN UID IS UNLOCKED> HOWEVER, IT APPEARS AS THOUGH IN SOME CASES, PHOOVAULT FAILS TO SUCCESSFULLY LOAD IMAGE IN TIME. THIS MAY BE BECAUSE OBSERVER IS OBSERVING UNLOCKED TRUE BEfORE IMAGE IS ACTUALLY SET IN DATABASE. AS A POSSIBLE RESOLUTION, CONSIDER ONLY SETTING UNLOCK TO TRUE ONCE IN DATA UPLOAD SUCCESS OBSERVER.
    
    //ATTN: Need to batchUpdate These Values &&& PAY ATTENTION TO ANY UPDATER METHODS THAT WILL ALSO OVERWRITE ENTIRE USER NODE
    func setObserveSelfPresenceInDB() {
        //store the presence information in the user's folder
        let selfUID = Auth.auth().currentUser!.uid
        let databaseReference = Database.database().reference(withPath: "users/\(selfUID)/isOnline")
        let lastOnlineReference = Database.database().reference(withPath: "users/\(selfUID)/lastOnline")
        
        //also store the presence information under a universal presence node
        let universalPresenceNode = Database.database().reference(withPath: "globalUserPresence/\(selfUID)/isOnline")
        
        let connectionReference = Database.database().reference(withPath: ".info/connected")
        connectionReference.observe(.value, with: { snapshot in
            //print("PRESENCE snapshot is", snapshot)
            guard let connected = snapshot.value as? Bool, connected else { return }
            
            /*
            // The onDisconnect() call is before the call to set() itself. This is to avoid a race condition
            // where you set the user's presence to true and the client disconnects before the
            // onDisconnect() operation takes effect, leaving a ghost user.
            databaseReference.onDisconnectSetValue("false")
            universalPresenceNode.onDisconnectSetValue("false")
            
            // this value could contain info about the device or a timestamp instead of just true
            databaseReference.setValue("true")
            universalPresenceNode.setValue("true")
            
            // when I disconnect, update the last time I was seen online
            lastOnlineReference.onDisconnectSetValue(ServerValue.timestamp())
            */
            
            //After:
            // The onDisconnect() call is before the call to set() itself. This is to avoid a race condition
            // where you set the user's presence to true and the client disconnects before the
            // onDisconnect() operation takes effect, leaving a ghost user.
            let childUpdatesDisconnect = [
                "users/\(selfUID)/isOnline": "false",
                "globalUserPresence/\(selfUID)/isOnline": "false",
                ]
            
            self.ref.onDisconnectUpdateChildValues(childUpdatesDisconnect)
            
            // this value could contain info about the device or a timestamp instead of just true
            let childUpdatesConnect = [
                "users/\(selfUID)/isOnline": "true",
                "globalUserPresence/\(selfUID)/isOnline": "true",
            ]
            
            self.ref.updateChildValues(childUpdatesConnect)
            
            // when I disconnect, update the last time I was seen online
            
            let childUpdatesLastSeen = [
                "users/\(selfUID)/lastOnline": ServerValue.timestamp(),
            ]
            
            self.ref.onDisconnectUpdateChildValues(childUpdatesLastSeen)
        })
    }
    
    func observeUnlockedUsers() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/unlockedUsers").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            let unlockedUID = snapshot.key
            self.unlockedUsers.append(unlockedUID)
            
            let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
                userInfoDict["UID"] == unlockedUID
            })
        
            if let userIndex = userIndex {
                let indexPath = IndexPath(row: userIndex, section: 0)
                
                let userEntryToChange = self.userDictArray[userIndex]
                let observedUserVisus = userEntryToChange["visus"]!
                
                let cellToChange = self.collectionView.cellForItem(at: indexPath) as? CollectionViewCell

                //2Way swaps
                if self.selfVisus! == "false" && observedUserVisus == "false" {
                    cellToChange?.swapSymbView.isHidden = false
                }

                //1Way Outgoing
                if self.selfVisus! == "false" && observedUserVisus == "true" {
                    cellToChange?.swapSymbView2.isHidden = false
                }
                
                //1Way Incoming
                if self.selfVisus! == "true" && observedUserVisus == "false" {
                    cellToChange?.swapSymbView2.isHidden = false
                    cellToChange?.swapSymbView2.image = UIImage(named: "1WaySwapGridSymb_Incoming_N")
                }

                self.collectionView?.reloadItems(at: [indexPath])
            }
        })
    }
    
    var outgoingBlockedUIDs = [String]()
    var incomingBlockedUIDs = [String]()

    func observeOutgoingBlockRequests() {

        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/blockedUsers").observe(.childAdded, with: { (snapshot: DataSnapshot) in
      
            let blockedUID = snapshot.key
            self.outgoingBlockedUIDs.append(blockedUID)
        
            //Case 1. objectUID exists in uidInProximity
            let proximityIdIndex = self.uidinProximityRawArray.index(where: { (userStruct) -> Bool in
                userStruct.otherUserID == blockedUID
            })
            
            if let proxID = proximityIdIndex {
                self.uidinProximityRawArray.remove(at: proxID)
            }
            
            //Case 2. objectUID exists in sortedArray
            if self.sortedUIDinProximityArray.contains(blockedUID) {
                if let index = self.sortedUIDinProximityArray.index(of: blockedUID) {
                    self.sortedUIDinProximityArray.remove(at: index)
                }
            }
            
            //Case 3. objectUID exists in userDictArray
            let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
                userInfoDict["UID"] == blockedUID
            })
            
            if let userIndex = userIndex {
                let indexPath = IndexPath(row: userIndex, section: 0)
                self.userDictArray.remove(at: userIndex)
                
                self.collectionView?.deleteItems(at: [indexPath])

//                if self.collectionView.cellForItem(at: [userIndex]) != nil {
//                    self.collectionView?.deleteItems(at: [indexPath])
//                }
                
//                self.collectionView?.deleteItems(at: [indexPath])
//                DispatchQueue.main.async {
//                    self.collectionView?.deleteItems(at: [indexPath])
//                }
//                self.collectionView?.deleteItems(at: [indexPath])
            }
            
            let listIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
                userInfoObject["chatWithUID"] == blockedUID || userInfoObject["revealToUID"] == blockedUID || userInfoObject["swapWithUID"] == blockedUID || userInfoObject["objectUID"] == blockedUID
            })
            
            if let listIndex = listIndex {
                self.chatListObjects.remove(at: listIndex)
            }
            
        
            let userInfoDict = ["outgoingBlockUID" : blockedUID]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "outgoingBlockObserved"), object: self, userInfo: userInfoDict)
        
        })
    }
    
    func observeIncomingBlockRequests() {

        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/blockedBy").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            let blockedUID = snapshot.key
            self.incomingBlockedUIDs.append(blockedUID)
            
            //print("del userDictArray", self.userDictArray.count)
            //Delete user from CollectionViewUI & From userDictArray
            let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
                userInfoDict["UID"] == blockedUID
            })
        
            if let userIndex = userIndex {
                let indexPath = IndexPath(row: userIndex, section: 0)
                self.userDictArray.remove(at: userIndex)
                self.collectionView?.deleteItems(at: [indexPath])
//
//                if self.collectionView.cellForItem(at: [userIndex]) != nil {
//                    self.collectionView?.deleteItems(at: [indexPath])
//                }
//                self.collectionView?.deleteItems(at: [indexPath])
//                self.collectionView?.deleteItems(at: [indexPath])
            }
            
            //Delete user fromChatListObjects
            let chatListIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
                userInfoObject["chatWithUID"] == blockedUID || userInfoObject["revealToUID"] == blockedUID || userInfoObject["swapWithUID"] == blockedUID
            })
            
            if let chatListIndex = chatListIndex {
                self.chatListObjects.remove(at: chatListIndex)
            }
            
            let userInfoDict = ["incomingBlockUID" : blockedUID]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "incomingBlockObserved"), object: self, userInfo: userInfoDict)
        })
    }
    
    //pass this between vcs and read in viewWilLAppear in VoilaVC to update UI accordingly -> To Set Button Text in VoilaVC
    var incomingDismissedUIDs = [String]()
    
    //ATTN: method below causing applicatin to crash @:
//    self.collectionView?.deleteItems(at: [indexPath])

    func observeIncomingDismissedRequests() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/dismissedBy").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            let dismissedByUID = snapshot.key
            self.incomingDismissedUIDs.append(dismissedByUID)
            
            //Delete user from CollectionViewUI & From userDictArray
            let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
                userInfoDict["UID"] == dismissedByUID
            })
            
            if let userIndex = userIndex {
                let indexPath = IndexPath(row: userIndex, section: 0)
                self.userDictArray.remove(at: userIndex)
                self.collectionView?.deleteItems(at: [indexPath])

//                if self.collectionView.cellForItem(at: [userIndex]) != nil {
//                    self.collectionView?.deleteItems(at: [indexPath])
//                }
//                self.collectionView?.deleteItems(at: [indexPath])
            }
            
            let userInfoDict = ["incomingDismissedByUID" : dismissedByUID]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "incomingDismissedByUID"), object: self, userInfo: userInfoDict)
        })
    }
    
    func observeGlobalUsersPresence() {
        //ARRAY OF USERS IN PROXIMITY SHOULD ALREADY BE FULLY LOADED ONCE THIS FUNCTION IS CALLED
        let usersInProximityArray = self.sortedUIDinProximityArray
        
        for userInProximityUID in usersInProximityArray {
            
            self.ref.child("globalUserPresence/\(userInProximityUID)").observe(.childChanged, with: { (snapshot) in
                
                guard let snapshotValue = snapshot.value as? String else { return }
                
                let userUID = userInProximityUID
                
                let userIndex = self.userDictArray.index(where: { (userInfoDict) -> Bool in
                    userInfoDict["UID"] == userUID
                })
                
                //ATTN: Crash on index not locating
                if let userIndex = userIndex {
                    let userEntryToChange = self.userDictArray[userIndex]
                    
                    (self.userDictArray[userIndex])["isOnline"] = snapshotValue
                    let indexPath = IndexPath(row: userIndex, section: 0)
                    
                    self.collectionView!.reloadItems(at: [indexPath])
                }
            })
        }
    }
    
    func updateCellSwapSymbol(cell: CollectionViewCell, withUserVisus: String) {
        //2Way swaps
        if self.myProfileInfoDict.isEmpty == false {
            self.selfVisus = self.myProfileInfoDict["selfVisus"]!
        }
        
        //
        if self.selfVisus! == "false" && withUserVisus == "false" {
            cell.swapSymbView.isHidden = false
        }
        
        //1Way Outgoing
        if self.selfVisus! == "false" && withUserVisus == "true" {
            cell.swapSymbView2.isHidden = false
        }
    
        //1Way Incoming
        if self.selfVisus! == "true" && withUserVisus == "false" {
            cell.swapSymbView2.isHidden = false
            cell.swapSymbView2.image = UIImage(named: "1WaySwapGridSymb_Incoming_N")
        }
    }

    //    photoChildNode: "swapInitiatorPhotoURL" || "swapRecipientPhotoURL"
    
    func liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: String, swapRecipientID: String, swapInitiatorPhotoURL: String, swapRecipientPhotoURL: String, swapID: String, swapInitiatorVisus: String, swapRecipientVisus: String) {
 
        guard swapInitiatorVisus == "false" && swapRecipientVisus == "false" else { return }
        
        if swapInitiatorID == self.selfUID! {
            
            if swapRecipientPhotoURL != "" && swapRecipientPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: swapID, photoChildNode: "swapRecipientPhotoURL", fromUserID: swapRecipientID, requestType: "")
            }
            
        } else if swapInitiatorID != self.selfUID! {
            //swap initiator is other
            if swapInitiatorPhotoURL != "" && swapInitiatorPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "")
            }
        }
    }

    var didSeeSwapRequestForUID = [String: String]()
    
    //PROBLEM: Observer is not deleting chatThread objects initiated by self, when self initiates swapRequest
    func observeActiveSwapRequests(withSourceCall: String) {
        //print("CVC observeActiveSwapRequests withSourceCall", withSourceCall)
        
        self.ref_SelfModelController.applicationDidCallSwapObserver = true
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _swapRefHandle = self.ref.child("globalSwapRequests/\(selfUID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            let newSwapRequestID = snapshot.key
    
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            
            //UID
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }
            
            //First Name
            guard let swapInitiatorFirstName = newSwapRequestBody["swapInitiatorFirstName"] else { return }
            guard let swapRecipientFirstName = newSwapRequestBody["swapRecipientFirstName"] else { return }
            
            //Default Image URL
            guard let swapInitiatorDefaultURL = newSwapRequestBody["swapInitiatorDefaultURL"] else { return }
            guard let swapRecipientDefaultURL = newSwapRequestBody["swapRecipientDefaultURL"] else { return }
            
            //New Photo URL
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            //print("GUARD CVC swapInitiatorPhotoURL", swapInitiatorPhotoURL)
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            
            //Did Open?
            guard let swapInitiatorDidOpen = newSwapRequestBody["swapInitiatorDidOpen"] else { return }
            guard let swapRecipientDidOpen = newSwapRequestBody["swapRecipientDidOpen"] else { return }
            
            //Visus Property
            guard let swapInitiatorVisus = newSwapRequestBody["swapInitiatorVisus"] else { return }
            guard let swapRecipientVisus = newSwapRequestBody["swapRecipientVisus"] else { return }
            
            //Request Status
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }
            guard let recipientDidSeeRequest = newSwapRequestBody["recipientDidSeeRequest"] else { return }

            //chatInfo
            guard let chatThreadID = newSwapRequestBody["chatThreadID"] else { return }
            guard let lastMessage = newSwapRequestBody["lastMessage"] else { return }
            
            //ChatRequestMarker
            guard let fromChatRequest = newSwapRequestBody["fromChatRequest"] else { return }
            guard let swapRequestTimeStamp = newSwapRequestBody["swapRequestTimeStamp"] else { return }
            
            //didSee
            guard let initiatorDidSeeRequest = newSwapRequestBody["initiatorDidSeeRequest"] else { return }
            guard let swapIsActive = newSwapRequestBody["swapIsActive"] else { return }
            
            guard swapIsActive != "nil" else { return }
            
//            let swapRequestTimeStamp = newSwapRequestBody["swapRequestTimeStamp"]!
//            let swapASDouble = Double(swapRequestTimeStamp)
//
//            //print("swapRequestTimeStamp is,", swapASDouble!)
//            let readableTime = self.getReadableDate(timeStamp: swapASDouble!)
//            //print("readableTime is,", readableTime)
            
            //observe last message from swap requests
            self.observeLastMessages(withThreadID: "\(chatThreadID)")
            //
            
            //live image loader method
            self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: newSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
            
            //CSVC Swap Configuration
            if swapInitiatorID == selfUID {
                self.selfInitiatedSwapRequestIndexDict[swapRecipientID] = newSwapRequestID
                self.swapRequestStatusDictionary[swapRecipientID] = "false"
            } else {
                self.otherInitiatedSwapRequestIndexDict[swapInitiatorID] = newSwapRequestID
                self.swapRequestStatusDictionary[swapInitiatorID] = "false"
            }
            
            //CLVC Swap Configuration
            //S1. Incoming Swaps
            if swapRecipientID == selfUID && swapRequestStatus == "false" && swapInitiatorVisus == "false" && swapRecipientVisus == "false" {
                //print("1 SWPOBSRV Incoming Swap")
                
                var newListObject = [String:String]()
                
                //Request Info
                newListObject["objectType"] = "swapRequestOtherNew"
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["swapWithUID"] = swapInitiatorID
                newListObject["swapWithDisplayName"] = swapInitiatorFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["swapWithVisus"] = swapInitiatorVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                newListObject["selfDidOpen"] = swapRecipientDidOpen

                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL
                
                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                newListObject["objectUID"] = swapInitiatorID

                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                } else {
                    let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        //remove the general chat duplicate
                        dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                    })
                    if let i = i {
                        //print("general chatListObjects before removal,", self.chatListObjects)
                        self.chatListObjects.remove(at: i)
                        //print("chatListObjects after removal,", self.chatListObjects)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                //SwapAlerts_incoming S1.
                if recipientDidSeeRequest == "false" {
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID.contains(swapInitiatorID) {
                        self.ref_SelfModelController.didSetSwapAlertForUserID.append(swapInitiatorID)
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                    }
                } else if recipientDidSeeRequest == "true" && !self.unlockedUsers.contains(swapInitiatorID) {
                    //set ONLY ONCE when app loads to remind users ONCE of any PENDING requests
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID.contains(swapInitiatorID) {
                        self.ref_SelfModelController.didSetSwapAlertForUserID.append(swapInitiatorID)
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                    }
                }
            }
            
            //S2. Outgoing Swaps
            if swapInitiatorID == selfUID && swapRequestStatus == "false" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                //print("2 SWPOBSRV Outgoing Swap")

                var newListObject = [String:String]()
        
                //Request Info
                newListObject["objectType"] = "swapRequestSelfNew"
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["swapWithUID"] = swapRecipientID
                newListObject["swapWithDisplayName"] = swapRecipientFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["swapWithVisus"] = swapRecipientVisus
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                newListObject["selfDidOpen"] = swapInitiatorDidOpen

                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = "You"
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen //tracking swapInitiator since sender is self

                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["objectUID"] = swapRecipientID

                //remove old entry for general chatthread
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                } else {
                    let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["chatWithUID"] == swapRecipientID
                    })
                    
                    if let i = i {
                        //print("chatListObjects before removal,", self.chatListObjects)
                        self.chatListObjects.remove(at: i)
                        //print("chatListObjects after removal,", self.chatListObjects)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
//                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
//                    self.allUniqueUIDsinChatList.append(swapRecipientID)
//                }
            }
            
            //S3. Incoming Swaps Accepted
            if swapRecipientID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapInitiatorVisus == "false" && swapRecipientVisus == "false" {
                //print("3 SWPOBSRV Incoming Swap Appr")

                var newListObject = [String:String]()
                
                if swapRecipientDidOpen == "false" {
                    newListObject["objectType"] = "otherInitSwapApprovedNew"
                } else if swapRecipientDidOpen == "true" {
                    
                    
                    newListObject["objectType"] = "otherInitSwapApprovedPre"
                }
                
                //Request Info
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["swapWithUID"] = swapInitiatorID
                newListObject["swapWithVisus"] = swapInitiatorVisus
                newListObject["swapWithDisplayName"] = swapInitiatorFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapInitiatorDefaultURL
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                newListObject["selfDidOpen"] = swapRecipientDidOpen

                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL
                
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["objectUID"] = swapInitiatorID

                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                }
            }
            
            //S4. Outgoing Swaps Accepted
            if swapInitiatorID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                //print("4 SWPOBSRV Outgoing Swap Appr")

                var newListObject = [String:String]()
                
                if swapInitiatorDidOpen == "false" {
                    newListObject["objectType"] = "selfInitSwapApprovedNew"
                    
                } else if swapInitiatorDidOpen == "true" {
                    //print("XPASS1")
                    newListObject["objectType"] = "selfInitSwapApprovedPre"
            
                    //print("XPASS userDictArray", self.userDictArray.count)
                    //update swap icon
                }
                
                //Request Info
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["swapWithUID"] = swapRecipientID
                newListObject["swapWithVisus"] = swapRecipientVisus
                newListObject["swapWithDisplayName"] = swapRecipientFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapRecipientDefaultURL
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                newListObject["selfDidOpen"] = swapInitiatorDidOpen
                
                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self //careful with these, need to reset all to default getter values
                
                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                newListObject["objectUID"] = swapRecipientID

                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                }
                
                //SETdidSeeSwapRequestForUID_Outgoing FOR outgoing2WayRequests that have been accepted but not yet unlocked, only set notification once in grid. Note: initiator would have seen request previously, but we're just notifying them that there's an approved request that they haven't unlocked yet.
                //ATTN: how do you block out swaps that were just accepted (leave it - willSet once)
                if !self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "true" {
                    if !self.unlockedUsers.contains(swapRecipientID) {
                        //print("unlockedUsers", self.unlockedUsers)
                        self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.append(swapRecipientID)
//                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                    }
                }
            }
            
            var oneWaySwapObjectTypes = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre"]

            //MARK: 1Way Swaps
            //MARK: S5. incomingReveal Request
            if swapRecipientID == selfUID && swapInitiatorPhotoURL == "" && swapInitiatorVisus == "true" {
                //print("5 SWPOBSRV incomingReveal")
                
                var newListObject = [String:String]()
                
                //Request Info
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Request Info
                if swapRequestStatus == "false" {
                    newListObject["objectType"] = "incomingReveal"
                } else if swapRequestStatus == "true" {
                    newListObject["objectType"] = "incomingRevealAppr"
                }
                
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["revealToUID"] = swapInitiatorID
                newListObject["revealToDisplayName"] = swapInitiatorFirstName
                newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["revealToVisus"] = swapInitiatorVisus
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                //newListObject["selfDidOpen"] = swapInitiatorDidOpen
                newListObject["otherDidSee"] = swapInitiatorDidOpen //optionally used in UI
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"
                newListObject["objectUID"] = swapInitiatorID

                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                //
                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                } else {
                    //remove old tableView record, in this case, remove old chatRequest record because it's being replaced with a new swapRecrod for the same user. Was this chatRequest initiated by self or other in the first place?
                    let iCR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        //chatRequestUID key //!!!!! MAKE SURE THIS WORKS IN ALL DIRECTIONS OF WHO SENDS CHAT REQUEST/SWAPREQUEST SINCE THESE ARE INDEPENDENT i.e. ChatRequestInitiator can be Different form ChatRequestInitiator
                        dictionaryOfInterest["requestToUID"] == swapInitiatorID
                    })
                    
                    if let iCR = iCR {
                        //print("chatListObjects before removal,", self.chatListObjects)
                        self.chatListObjects.remove(at: iCR)
                        //print("chatListObjects after removal,", self.chatListObjects)
                    }
                    
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        //locate entry in chatListObjects where
//                        generalChatObject["objectType"] = "generalChatThread"

                        dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                    })
                    
                    if let iGR = iGR {
                        //print(" GEN chatListObjects before removal,", self.chatListObjects)
                        self.chatListObjects.remove(at: iGR)
                        //print(" GEN chatListObjects after removal,", self.chatListObjects)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                    
                    //New: Jul 21, 2018
                    //last message
//                    self.chronoDict[swapInitiatorID] = Double(swapRequestTimeStamp)
                    self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
                }
                
                //SwapAlerts_incoming S5. incomingReveal Request
                //swapStatus == false && recipientDidSeeRequest == false
                if recipientDidSeeRequest == "false" {
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID.contains(swapInitiatorID) {
                        self.ref_SelfModelController.didSetSwapAlertForUserID.append(swapInitiatorID)
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = recipientDidSeeRequest
                    }
                } else if recipientDidSeeRequest == "true" && !self.unlockedUsers.contains(swapInitiatorID) {
                    //set ONLY ONCE when app loads to remind users ONCE of any PENDING requests
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID.contains(swapInitiatorID) {
                        self.ref_SelfModelController.didSetSwapAlertForUserID.append(swapInitiatorID)
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                    }
                }
                
                //print("5 SWPOBSRV incomingReveal chatList objects is nowm, ", self.chatListObjects)
                //also need to remove old general chat Record in case: incoming chat thread -> turned to incoming 1way swap
            }
            
            //MARK: S6. outgoingReveal
            
            if swapInitiatorID == selfUID && swapInitiatorVisus == "true" && swapRecipientVisus == "false" {
                //print("6 SWPOBSRV outgoingRevealRequest")
                //print()
                var newListObject = [String:String]()
            
                //Request Info
                if swapRequestStatus == "false" {
                    newListObject["objectType"] = "outgoingReveal"
                    
                } else if swapRequestStatus == "true" {
                    //UI Marker
                    newListObject["selfDidOpen"] = swapInitiatorDidOpen
            
                    if swapInitiatorDidOpen == "false" {
                        //print("6.1 SWPOBSRV ")
            
                        newListObject["objectType"] = "outgoingRevealAppr"
                        
                        if swapRecipientPhotoURL != "" && swapRecipientPhotoURL != "Clear" {
                            //print("ZIPPO")
                            self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: newSwapRequestID, photoChildNode: "swapRecipientPhotoURL", fromUserID: swapRecipientID, requestType: "")
                            
                            //Note; unlockedUsersObserver will observe that user was unlocked, and will call CollectionView to reload userWhoHasShownProfile, and will load image from memory
                        }
                        
                    } else if swapInitiatorDidOpen == "true" {
                        //print("6.2 SWPOBSRV ")

                        newListObject["objectType"] = "outgoingRevealApprPre"
                    }
                    
                    //SETdidSeeSwapRequestForUID_Outgoing FOR outgoing1WayRequests that have been accepted but not yet unlocked, only set notification once in grid. Note initiator may have seen request previously, but we're just notifying them that there's an approved request that they haven't unlocked yet.
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.contains(swapRecipientID) {
                        if !self.unlockedUsers.contains(swapRecipientID) {
                            self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.append(swapRecipientID)
                            self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                        }
                    }
                    
                }
     
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["revealToUID"] = swapRecipientID
                newListObject["revealToDisplayName"] = swapRecipientFirstName
                newListObject["revealToDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["revealToVisus"] = swapRecipientVisus
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                
                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["objectUID"] = swapRecipientID
                
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                    if !self.chatListObjectIDs.contains(newSwapRequestID) {
                        self.chatListObjectIDs.append(newSwapRequestID)
                        self.chatListObjects.append(newListObject)
                    }
                }
                
                //self.printMyUserInfo()
            }
            
            //MARK: S7. oneWayShow
            if swapInitiatorID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && swapRequestStatus == "true" {
                //print("CVC 7 SWPOBSRV outgoing oneWayShow")
                
                var newListObject = [String:String]()

                newListObject["objectType"] = "outgoingOneWayShow"

                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["revealToUID"] = swapRecipientID
                newListObject["revealToDisplayName"] = swapRecipientFirstName
                newListObject["revealToDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["revealToVisus"] = swapRecipientVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage

                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self

                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["objectUID"] = swapRecipientID
                
                if self.lastMessagesbyUID.keys.contains(swapRecipientID) {
                
                    if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                        
                        self.allUniqueUIDsinChatList.append(swapRecipientID)
                        
                        
                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                            self.chatListObjectIDs.append(newSwapRequestID)
                            //                    self.chatListObjects.append(newListObject)
                            self.chatListObjects.insert(newListObject, at: 0)
                            //print("CVCX DIDAPPENDONEWAYSHOW")
                        }
                    }
                } else {
                    //print("CVCX DIDNOTAPPENDONEWAYSHOW")
                }
                
                //                if self.lastMessagesbyUID.keys.contains(swapRecipientID) {
                //                    //print("CVC PASS 1")
                //
                //                    if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                //                        //print("CVC PASS 2")
                //
                //                        self.allUniqueUIDsinChatList.append(swapRecipientID)
                //
                //                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                //                            //print("CVC PASS 3")
                //
                //                            self.chatListObjectIDs.append(newSwapRequestID)
                //                            self.chatListObjects.append(newListObject)
                //                            //print("CVCVCVC DIDAPPENDONEWAYSHOW")
                //                        }
                //                    }
                //                } else {
                //
                //                    //print("CVCVCVC DIDNOTAPPENDONEWAYSHOW")
                //                }
                
            }
            
            //MARK: S8. Incoming Voluntary OneWayShow
            if swapRecipientID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && swapRequestStatus == "true" {
                //print("8 SWPOBSRV incomingOneWayShow")
                
                var newListObject = [String:String]()
                
                newListObject["objectType"] = "incomingOneWayShow"
                
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["revealToUID"] = swapInitiatorID
                newListObject["revealToDisplayName"] = swapInitiatorFirstName
                newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["revealToVisus"] = swapInitiatorVisus
                newListObject["fromChatRequest"] = fromChatRequest

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = swapInitiatorFirstName
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["objectUID"] = swapInitiatorID

                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
//                need to add condition: if UID is not associated with last message then DO NOT Append
                //print("CVC SWPOBSRV lastMessagesArray", self.lastMessagesbyUID)
                //print("CVC SWPOBSRV swapInitiatorID", swapInitiatorID)
                
                if lastMessage != "" {
                    if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                        self.allUniqueUIDsinChatList.append(swapInitiatorID)
                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                            self.chatListObjectIDs.append(newSwapRequestID)
                            self.chatListObjects.append(newListObject)
                            //print("DIDAPPENDONEWAYSHOW")
                        }
                        
                    } else {
                        let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                            dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                        })
                        if let iGR = iGR {
                            //print(" GEN chatListObjects before removal,", self.chatListObjects)
                            self.chatListObjects.remove(at: iGR)
                            //print(" GEN chatListObjects after removal,", self.chatListObjects)
                        }
                    }
                    
                } else {
                    //print("GVC will NOT add oneWayShow to Array unless oneWayShowdidcome from request")
                }
                
                //self.printMyUserInfo()
                //load oneWayShow Image to memory from DB then purge
                //print("8 CVC ONE WAY SHOW swapInitiatorPhotoURL,", swapInitiatorPhotoURL)
                if swapInitiatorPhotoURL != "" {
                    //print("XN PASS 1")
                    self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: newSwapRequestID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "incomingOneWayShow")
                    
                    //Note; unlockedUsersObserver will observe that user was unlocked, and will call CollectionView to reload userWhoHasShownProfile, and will load image from memory
                }
            }
            
            if swapInitiatorID == self.selfUID! {
                //print("Z!! 1")
//                self.chronoDict[swapRecipientID] = Double(swapRequestTimeStamp)
                self.updateChronoDict(forObjectUID: swapRecipientID, withObjectTimeStamp: swapRequestTimeStamp)

            } else if swapInitiatorID != self.selfUID! {
                //print("Z!! 2")
//                self.chronoDict[swapInitiatorID] = Double(swapRequestTimeStamp)
                self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
            }
        })
    }
    
    //ATTN: need to make sure that this method is performed for each incoming oneway show before unlocked users observes unlocked user an dloads image from disk.
    
    var imageLoaderView: UIImageView!
    
    func addImageLoader() {
        let imageX = 0 as CGFloat
        let imageY = 0 as CGFloat
        let imageWidth = 5 as CGFloat
        let imageHeight = 5 as CGFloat
        
        let imageFrame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        let imageLoaderView = UIImageView(frame: imageFrame)
        
        view.addSubview(imageLoaderView)
        self.imageLoaderView = imageLoaderView
        self.imageLoaderView.isHidden = true
    }
    
    var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    func photoVault(otherUID: String) -> UIImage? {
        //print("PV1. accesing photo vault")
        let fileName = "\(otherUID).jpeg"
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            //print("PV2. Successful try UIIMage")
            return UIImage(data: imageData)
        } catch {
            //print("PV2E. Error loading image : \(error)")
        }
        return nil
    }
    
    func loadImageFromDatabaseToMemory_thenSave(withClearURL: String, withRequestID: String, photoChildNode: String, fromUserID: String, requestType: String) {
        //print("CVC ONEWAYSHOW loadImageToMemory()")
        let newPhotoURL = URL(string: withClearURL)
        self.imageLoaderView!.sd_setImage(with: newPhotoURL) { (loadedImage, error, _, savedURL) in
            
            if error == nil {
                self.saveUnlockedImageToDisk(imageObject: loadedImage!, fromUserID: fromUserID)
                //print("revelaro successfully loaded image")
                
                let selfUID = self.selfUID!
                let otherUID = fromUserID
                
                //print("CVC ONEWAYSHOW loadImageFromDatabaseToMemory_thenSave will clear photoURL")
                if requestType == "incomingOneWayShow" {
                    //print("CVC incomingOneWayShow CLEAR")

                    //clear only on one-way & let the voilaVC clear all other cases
                    self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                    self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                }
            } else {
                //print("CVC phot=o loader error,", error)
            }
            
        }
    }
    
    func saveUnlockedImageToDisk(imageObject: UIImage, fromUserID: String) {
        //print("CVC  OneWayShow writing to vault")
        let fileName = "\(fromUserID).jpeg"
        if let data = UIImageJPEGRepresentation(imageObject, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("CVC OneWayShow photo written to vault with filename,", fileName)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func getReadableDate(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        
        if Calendar.current.isDateInTomorrow(date) {
            return "Tomorrow"
        } else if Calendar.current.isDateInYesterday(date) {
            return "Yesterday"
        } else if dateFallsInCurrentWeek(date: date) {
            if Calendar.current.isDateInToday(date) {
                dateFormatter.dateFormat = "h:mm a"
                return dateFormatter.string(from: date)
            } else {
                dateFormatter.dateFormat = "EEEE"
                return dateFormatter.string(from: date)
            }
        } else {
            dateFormatter.dateFormat = "MMM d, yyyy"
            return dateFormatter.string(from: date)
        }
    }
    
    func dateFallsInCurrentWeek(date: Date) -> Bool {
        let currentWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: Date())
        let datesWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: date)
        return (currentWeek == datesWeek)
    }
    
    func observeSwapRequestStatusChanges() {
        //print("func observeSwapRequestStatusChanges")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        _swapChangesRefHandle = self.ref.child("globalSwapRequests/\(user.uid)").observe(.childChanged , with: { info in
            self.updateSwapStatusChangesSnapshot(selfUID: selfUID, snapshot: info)
            
        })
    }
    
    func updateSwapStatusChangesSnapshot(selfUID: String, snapshot: DataSnapshot) {
        
        let existingSwapRequestID = snapshot.key
        //print("M1. existingSwapRequestID is,", existingSwapRequestID)
        
        guard let modifiedSwapRequestBody = snapshot.value as? [String:String] else { return }
        //print("M2. modifiedSwapRequestBody is,", modifiedSwapRequestBody)
        
        //UID
        guard let swapInitiatorID = modifiedSwapRequestBody["swapInitiatorID"] else { return }
        //print("M3. newswapInitiatorID is", swapInitiatorID )
        guard let swapRecipientID = modifiedSwapRequestBody["swapRecipientID"] else { return }
        //print("M4. newswapRecipientID is", swapRecipientID )
        
        //FirstName
        guard let swapInitiatorFirstName = modifiedSwapRequestBody["swapInitiatorFirstName"] else { return }
        //print("S3. newswapInitiatorID is", swapInitiatorFirstName )
        guard let swapRecipientFirstName = modifiedSwapRequestBody["swapRecipientFirstName"] else { return }
        //print("S11. swapRecipientFirstName is", swapRecipientFirstName)
        
        //Default Image URL
        guard let swapInitiatorDefaultURL = modifiedSwapRequestBody["swapInitiatorDefaultURL"] else { return }
        //print("S9. swapRequestStatus is", swapInitiatorDefaultURL)
        guard let swapRecipientDefaultURL = modifiedSwapRequestBody["swapRecipientDefaultURL"] else { return }
        //print("S10. ACCIO swapRequestStatus is", swapRecipientDefaultURL)
        
        //PhotoURL
        guard let swapInitiatorPhotoURL = modifiedSwapRequestBody["swapInitiatorPhotoURL"] else { return }
        //print("M5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL )
        guard let swapRecipientPhotoURL = modifiedSwapRequestBody["swapRecipientPhotoURL"] else { return }
        //print("M6. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
        
        //didOpen
        guard let swapInitiatorDidOpen = modifiedSwapRequestBody["swapInitiatorDidOpen"] else { return }
        //print("S7. swapRequestStatus is", swapInitiatorDidOpen)
        guard let swapRecipientDidOpen = modifiedSwapRequestBody["swapRecipientDidOpen"] else { return }
        //print("S7. swapRequestStatus is", swapRecipientDidOpen)
        
        //SwapStatus
        guard let modifiedSwapRequestStatus = modifiedSwapRequestBody["swapRequestStatus"] else { return }
        //print("M7. modifiedSwapRequestStatus is", modifiedSwapRequestStatus)
        
        //Visus Property
        guard let swapInitiatorVisus = modifiedSwapRequestBody["swapInitiatorVisus"] else { return }
        //print("S7. swapInitiatorVisus is", swapInitiatorVisus)
        guard let swapRecipientVisus = modifiedSwapRequestBody["swapRecipientVisus"] else { return }
        //print("S8. swapRecipientVisus is", swapRecipientVisus)
        
        //ChatData
        guard let chatThreadID = modifiedSwapRequestBody["chatThreadID"] else { return }
        guard let lastMessage = modifiedSwapRequestBody["lastMessage"] else { return }
        
        //ChatRequestMarker
        guard let fromChatRequest = modifiedSwapRequestBody["fromChatRequest"] else { return }
        guard let swapRequestTimeStamp = modifiedSwapRequestBody["swapRequestTimeStamp"] else { return }

        //didSee
        guard let initiatorDidSeeRequest = modifiedSwapRequestBody["initiatorDidSeeRequest"] else { return }
        guard let swapIsActive = modifiedSwapRequestBody["swapIsActive"] else { return }
        
        guard swapIsActive != "nil" else { return }
                
        //live image loader
        self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: existingSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
        
        //Swap Status Modifier
        if swapInitiatorID == selfUID {
            //print("M8. Self-initiated swap request status now has status,", modifiedSwapRequestStatus)
            self.swapRequestStatusDictionary[swapRecipientID] = modifiedSwapRequestStatus
        } else {
            //print("M9. Other-initiated swap request status now has status,", modifiedSwapRequestStatus)
            self.swapRequestStatusDictionary[swapInitiatorID] = modifiedSwapRequestStatus
        }
        
        //..........Update chatListObjects for 2Wayswaps
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            //print("chatListObjectIDs contains existingSwapRequestID")
            
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapRequestID"] == existingSwapRequestID
            })
            
            //When I approve someone's previous request
            if swapRecipientID == selfUID &&  modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                
                //print("ACCIO 2 chatListObjects", self.chatListObjects)
                
                if swapRecipientDidOpen == "false" {
                    (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedNew"
                    
                } else if swapRecipientDidOpen == "true" {
                    (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedPre"
                    (self.chatListObjects[i!])["requestStatus"] = "true"
                }
                
                (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                (self.chatListObjects[i!])["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                //
                //update chatData
                (self.chatListObjects[i!])["fromChatRequest"] = fromChatRequest
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                //            (self.chatListObjects[i!])["lastMessage"] = lastMessage
            }
            
            //2Way Self initiated swap requests that have been approved by other
            if swapInitiatorID == selfUID  && modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                
                //that have been opened
                if swapInitiatorDidOpen == "false" {
                    (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedNew"
                    
                } else if swapInitiatorDidOpen == "true" {
                    (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedPre"
                }
                
                (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                (self.chatListObjects[i!])["swapInitiatorDidOpen"] = swapInitiatorDidOpen
                
                //update chatData
                (self.chatListObjects[i!])["fromChatRequest"] = fromChatRequest
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                //            (self.chatListObjects[i!])["lastMessage"] = lastMessage
                
                //SETdidSeeSwapRequestForUID_Outgoing
                if !self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.contains(swapRecipientID) {
                    if initiatorDidSeeRequest == "false" && !self.unlockedUsers.contains(swapRecipientID) {
                        self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.append(swapRecipientID)
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                    }
                } else {
                    
                }
                
                
            }
            
            //1Way Incoming Reveal - case when user sends message after initiating reveal
            if swapRecipientID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && swapRequestStatus == "true" {
                //print("8 SWPOBSRV incomingOneWayShow")
                
                var newListObject = [String:String]()
                
                newListObject["objectType"] = "incomingOneWayShow"
                
                newListObject["oneWayRequestID"] = existingSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["revealToUID"] = swapInitiatorID
                newListObject["revealToDisplayName"] = swapInitiatorFirstName
                newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["revealToVisus"] = swapInitiatorVisus
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = swapInitiatorFirstName
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["objectUID"] = swapInitiatorID

                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                //                need to add condition: if UID is not associated with last message then DO NOT Append
                //print("CVC SWPOBSRV lastMessagesArray", self.lastMessagesbyUID)
                //print("CVC SWPOBSRV swapInitiatorID", swapInitiatorID)
                
                if lastMessage != "" {
                    //print("GVC will add oneWayShow to Array")
                    if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                        self.allUniqueUIDsinChatList.append(swapRecipientID)
                        if !self.chatListObjectIDs.contains(existingSwapRequestID) {
                            self.chatListObjectIDs.append(existingSwapRequestID)
                            self.chatListObjects.append(newListObject)
                        }
                    }
                } else {
                    //print("GVC will NOT add oneWayShow to Array")
                }
            }
        }
        
        //........
        //MARK: 1Way Swaps
        //MARK: S5. incomingReveal Request
        
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            //print("chatListObjectIDs contains existingSwapRequestID")
            //print("self.chatListObjectIDs", self.chatListObjectIDs)
            //print("self.chatListObjects", self.chatListObjects)

            
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["oneWayRequestID"] == existingSwapRequestID
            })
            
            //when incoming reveals, need to see if self approved the request
            
            if swapRecipientID == selfUID && swapInitiatorVisus == "true" {
                //print("Modifying incomingReveal")
                
                if modifiedSwapRequestStatus == "true" {
                    (self.chatListObjects[i!])["objectType"] = "incomingRevealAppr"
                    (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                }
                
                //print("self.chatListObjects[i!]", self.chatListObjects[i!])
                (self.chatListObjects[i!])["selfDidOpen"] = swapRecipientDidOpen
                (self.chatListObjects[i!])["otherDidSee"] = swapInitiatorDidOpen
                
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                (self.chatListObjects[i!])["lastMessage"] = lastMessage
            }
            
            //S5. 1Way Outgoing when outgoing reveals,
            if swapInitiatorID == selfUID && swapInitiatorVisus == "true" && swapRecipientVisus == "false" {
                
                if modifiedSwapRequestStatus == "true" {
                    //print("modifying chat initiated by self and approved by other ")

                    if swapInitiatorDidOpen == "false" {
                        (self.chatListObjects[i!])["objectType"] = "outgoingRevealAppr"
                        (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                        
                        (self.chatListObjects[i!])["selfDidOpen"] = swapInitiatorDidOpen
                        (self.chatListObjects[i!])["otherDidSee"] = swapRecipientDidOpen
                        
                    } else if swapInitiatorDidOpen == "true" {
                        (self.chatListObjects[i!])["objectType"] = "outgoingRevealApprPre"
                        (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                        
                        (self.chatListObjects[i!])["selfDidOpen"] = swapInitiatorDidOpen
                        (self.chatListObjects[i!])["otherDidSee"] = swapRecipientDidOpen
                    }
                    //
                    (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                    (self.chatListObjects[i!])["lastMessage"] = lastMessage

                    //SETdidSeeSwapRequestForUID_Outgoing
                    //1. swapStatus == true
                    if !self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "false" {
                        if !self.unlockedUsers.contains(swapRecipientID) {
                            self.ref_SelfModelController.didSetSwapAlertForUserID_Outgoing.append(swapRecipientID)
                            self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = "false"
                        }
                    } else {
                        
                    }
                }
            }
        }
        
        //1WayShow
        if swapRecipientID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && modifiedSwapRequestStatus == "true" {
            //print("8.2 SWPOBSRV incomingOneWayShow")
            
            var newListObject = [String:String]()
            
            newListObject["objectType"] = "incomingOneWayShow"
            
            newListObject["oneWayRequestID"] = existingSwapRequestID
            newListObject["requestStatus"] = modifiedSwapRequestStatus
            
            //Profile Info
            newListObject["revealToUID"] = swapInitiatorID
            newListObject["revealToDisplayName"] = swapInitiatorFirstName
            newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
            newListObject["revealToVisus"] = swapInitiatorVisus
            
            //Chat Info
            newListObject["chatThreadID"] = chatThreadID
            newListObject["lastMessage"] = lastMessage
            
            newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
            newListObject["requestInitiatorFirstName"] = swapInitiatorFirstName
            //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
            newListObject["objectUID"] = swapInitiatorID

            //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
            //                need to add condition: if UID is not associated with last message then DO NOT Append
            //print("CVC SWPOBSRV lastMessagesArray", self.lastMessagesbyUID)
            //print("CVC SWPOBSRV swapInitiatorID", swapInitiatorID)
            if lastMessage != "" {
                //print("GVC will add oneWayShow to Array")
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                    
                    if !self.chatListObjectIDs.contains(existingSwapRequestID) {
                        self.chatListObjectIDs.append(existingSwapRequestID)
                        self.chatListObjects.append(newListObject)
//                        self.reloadTableViewData()
                    }
                }
                
            } else {
                //print("GVC will NOT add oneWayShow to Array")
            }
            
            if swapInitiatorPhotoURL != "Clear" {
                //print("swapInitiatorPhotoURL CHANGES PASS 1")
                //print("XZ new swapInitiatorPhotoURL,", swapInitiatorPhotoURL)
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: existingSwapRequestID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "incomingOneWayShow")
                //Note; unlockedUsersObserver will observe that user was unlocked, and will call CollectionView to reload userWhoHasShownProfile, and will load image from memory
            }
        }
        
        //......update user info since chatListObject acts as userProfileInfoDict for GridVC->ChatListVC->ChatScreenVC
        guard swapRecipientVisus == "false" && swapInitiatorVisus == "false" else { return }
        
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapRequestID"] == existingSwapRequestID
            })
            
            if swapInitiatorID == selfUID {
                //update with new recipient values
                (self.chatListObjects[i!])["swapWithVisus"] = swapRecipientVisus
                (self.chatListObjects[i!])["swapWithDefaultURL"] = swapRecipientDefaultURL
                (self.chatListObjects[i!])["swapWithDisplayName"] = swapRecipientFirstName
                
            } else {
                //when other is initiator, update with new initiator values
                (self.chatListObjects[i!])["swapWithVisus"] = swapInitiatorVisus
                (self.chatListObjects[i!])["swapWithDefaultURL"] = swapInitiatorDefaultURL
                (self.chatListObjects[i!])["swapWithDisplayName"] = swapInitiatorFirstName
            }
        }
        
        //update timestamps for existing swapRequests (i,e, if other user accepts swap, we should record this as a dummy "new last message", so that the chatList reorders appropriately
        //print("V! B")

        if swapInitiatorID == self.selfUID! {
            //print("V! 1")
//            self.chronoDict[swapRecipientID] = Double(swapRequestTimeStamp)
            self.updateChronoDict(forObjectUID: swapRecipientID, withObjectTimeStamp: swapRequestTimeStamp)

        } else if swapInitiatorID != self.selfUID! {
            //print("V! 2")
//            self.chronoDict[swapInitiatorID] = Double(swapRequestTimeStamp)
            self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
        }
        
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MARK: TODO: General Chat Objects Configuration
    
/* *************************    *************************   *************************   *************************   *********************** */

//    var importedStatusBarHeight
    var statusBarHeight: CGFloat?
    var finalScrollPos: CGPoint?
    //ATTN: if scrollStillScrollingWhileSliding -> set up observer for when scrollFinishesDecelerating to update final value of scrollView
    
    //        SDImageCache.shared().clearMemory()
    //        SDImageCache.shared().clearDisk()
    
//    var ref_SourceController = CollectionViewController()
    
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func prepareForUnwindForHome (segue: UIStoryboardSegue) {
        
    }
    
    @IBAction func prepareForUnwindForChats (segue: UIStoryboardSegue) {
        
    }
    
    //    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
    //
    ////        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
    //
    //        let segue = PushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
    //
    //        segue.perform()
    //    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.sourceController = ""
        self.shouldClearNewChatAlert = false
        
        self.ref_SelfModelController.finalsBarH = self.statusBarHeight
        
        //1. Segue to Home-Screen Controller
        if segue.identifier == "gridVCtoHomeVC" {
            let controller = segue.destination as! HomeScreenViewController
            //            controller.myProfileInfoDict = self.myProfileInfoDict
            controller.sourceController = "GridVC"
            controller.unlockedUsers = self.unlockedUsers
            
//            controller.userDictArray = self.userDictArray
//            controller.userDictArray_UID = self.userDictArray_UID
//            controller.ref_SourceController = self.ref_SourceController
            
            //print("CVC viewDidDisappear contentOffset, ATSEGUE,", self.collectionView!.contentOffset)
            //            controller.ref_ModelController = self.ref_ModelController
            controller.ref_SelfModelController = self.ref_SelfModelController
        }
        
        //2. Segue to Chat List Controller
        if segue.identifier == "gridVCtochatListVC" {
            let controller = segue.destination as! ChatListViewController

            //print("CVC pass self.chatListObjects", self.chatListObjects.count)
            controller.chatListObjects = self.chatListObjects
            controller.chatListObjectIDs = self.chatListObjectIDs
            controller.allUniqueUIDsinChatList = self.allUniqueUIDsinChatList
            
//            controller.myProfileInfoDict = self.myProfileInfoDict
            //print("GRIDVC PASSING ARRAY,", self.lastMessagesbyUID)
            controller.unlockedUsers = self.unlockedUsers
            controller.lastMessagesbyUID = self.lastMessagesbyUID
//            controller.didReadLastMessageForUID = self.didReadLastMessageForUID
            controller.incomingDismissedUIDs = self.incomingDismissedUIDs

            controller.userDictArray = self.userDictArray
            controller.userDictArray_UID = self.userDictArray_UID
            controller.ref_SelfModelController = self.ref_SelfModelController
            controller.chronoDict = self.chronoDict
            
            //REMOVE ALL OBSERVERS MOVED FROM VIEWWILLDISAPPEAR TO PREPAREFORSENDER METHOD ONLY WEN SEGUE>TO CLVC. THIS WILL ALLOW GVC TO CONTINUE READING CHANGES TO DATA STRUCTS WHILE USER IS IN PROFILE VIEW AND WHEN UNWINDING BACK TO GRID
//            self.removeAllDBObservers()
        }
        
        //3. Segue to Profile Controller
        if let destinationViewController = segue.destination as? UserProfileViewController {
            destinationViewController.importedStatusBarHeight = self.statusBarHeight
            destinationViewController.finalScrollPos = self.collectionView!.contentOffset
        }
    }
        
    func swapRequestControlFlow(selectedUserUID: String) {
        
        //Var_Mirror
        //var selfInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID == selfUID, Maps ["otherUID" : "requestID"]
        //var otherInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID != selfUID, Maps ["initiatorUID" : "requestID"]
        
        //var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
        //var selfInitiatedSwapRequest: Bool?
        //var swapRequestStatus: String?
        //var swapRequestStatusDictionary = [String:String]()
        
        //Checking if previous swapRequestID exists and who is initiator
        if self.otherInitiatedSwapRequestIndexDict.keys.contains(selectedUserUID) {
            //request does exist, update requestID accordingly
            self.swapRequestDoesExist = true //(1)
            self.absExistingSwapRequestID = self.otherInitiatedSwapRequestIndexDict[selectedUserUID] //(2)
            
            //request initiated by other
            self.selfInitiatedSwapRequest = false //(1)
            
        } else if self.selfInitiatedSwapRequestIndexDict.keys.contains(selectedUserUID) {
            //request does exist, update requestID accordingly
            self.swapRequestDoesExist = true
            self.absExistingSwapRequestID = self.selfInitiatedSwapRequestIndexDict[selectedUserUID]
            
            //request initiated by self
            self.selfInitiatedSwapRequest = true
        } else {
            self.swapRequestDoesExist = false
        }
        
        //Checking swap status
        if swapRequestDoesExist == true {
            //can observe request state
            //get swapRequestStatus
            guard let swapRequestStatus = self.swapRequestStatusDictionary[selectedUserUID] else { return }
            self.swapRequestStatus = swapRequestStatus
            //print("P1. selected user request status is,", self.swapRequestStatus!)
        } else {
            //no need to observe request state
        }
    }
    
    //
    var freezeFrame: UIImageView?
    var arrestoImago: UIImage?
    
    func arrestoMomento() {
        //print("CLVC arrestoMomento")
        
        let arrestoImago = self.takeScreenshot()
        self.arrestoImago = arrestoImago
        
        let freezeFrame = UIImageView(frame: UIScreen.main.bounds)
//        freezeFrame.image = arrestoImago!
        
        self.view.addSubview(freezeFrame)
        self.freezeFrame = freezeFrame
    }
    
    func takeScreenshot() -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        let context = UIGraphicsGetCurrentContext()
        
        layer.render(in:context!)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
        //print("CVC now in viewWillDisappear")
        
        self.activityIndicatorView?.removeFromSuperview()
        
        if self.willSegueToChatList == true {
            self.blueDotView?.isHidden = true
        }
        
        if self.ref_SelfModelController.applicationDidCallSwapObserver == false {
            //Note: This should only call when all other callSources fail. I.e. this should NEVER happen
//            self.observeActiveSwapRequests(withSourceCall: "lastResort")
        }
        
//        if let freezeFrame = self.freezeFrame {
//            //print("CVC WILLRMV FF")
//            freezeFrame.removeFromSuperview()
//        }

        self.willSegueToChatList = false
        //MARK: NOTE MAJOR CHANGE TO STRUCTURE OF PROGRAM - DO NOT DEACTIVATE OBSERVERS WHEN FLOW IS GVC -> UPVC
//        self.removeAllDBObservers()
        //moved this functino from removeAllObservers
    }
    
    func removeAllDBObservers() {
        
        self.ref.child("globalChatThreadMetaData").removeAllObservers()
//        self.ref.child("users/\(self.selfUID!)/myActiveChatThreads").removeAllObservers()
//        self.ref.child("users/\(self.selfUID!)/myActiveChatThreads").removeObserver(withHandle: _genChatHandleChanges)

//        listenerPaths only contains path for ActiveMessageThreads
//        for i in self.listenerPaths {
//            //print("listenerPaths i is,", i)
//            //print("CVC: Removing listeners at,", i)
//            i.removeAllObservers()
//        }

        //swap
        self.ref.child("globalSwapRequests/\(self.selfUID!)").removeAllObservers()
        
//        for i in self.grid_LastMessages_Listeners {
//            //print("listenerPaths i is,", i)
//            //print("CVC: Removing listeners at,", i)
//            i.removeAllObservers()
//        }
//
//        self.ref.child("globalChatThreadMetaData").removeAllObservers()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        
    }
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 16 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = (sWidth - aW) * 0.5
        let aY = (sHeight - aH) * 0.5
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
    }

    //MARK: Configure UI Notifications
    
    let notification = UINotificationFeedbackGenerator()
    var locationAlert: UIAlertController?

    func addLocationAlert() {
        let alertTitle = "Location Services"
        let alertMessage = "Location Services are disabled for Ximo. Please enable location services in Settings to continue using Ximo."
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let keepChanges = UIAlertAction(title: "OK", style: .default) { (action) in
            self.locationAlert?.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(keepChanges)
        self.locationAlert = alert
    }
    
    func setChatNotificationAlert() {
        if self.willSegueToChatList == true {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.animateBlurDot()
//                self.chatListNavButton!.setImage(UIImage(named: "chatListNotification_NewSwap"), for: .normal)
            }
        } else {
            self.animateBlurDot()
//            self.chatListNavButton!.setImage(UIImage(named: "chatListNotification_NewSwap"), for: .normal)
        }
    }
    
    func setSwapNotificationAlert() {
        if self.willSegueToChatList == true {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.animateBlurDot()
//                self.chatListNavButton!.setImage(UIImage(named: "chatListNotification_NewSwap"), for: .normal)
            }
        } else {
            self.animateBlurDot()
//            self.chatListNavButton!.setImage(UIImage(named: "chatListNotification_NewSwap"), for: .normal)
        }
    }
    
    func clearChatNotificationAlert() {
        if self.willSegueToChatList == true {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
//                self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
                self.blueDotView?.isHidden = true
            }
        } else {
//                self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
                self.blueDotView?.isHidden = true
            
        }
    }

    func clearSwapNotificationAlert() {
        if self.willSegueToChatList == true {
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
//                self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
                self.blueDotView?.isHidden = true

            }
        } else {
//            self.chatListNavButton?.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
            self.blueDotView?.isHidden = true

        }
    }
    
    var willSegueToChatList = false
    
    //MARK: Randomizer Struct
    //Courtesy, Anthony Levings
    
    struct Random {
        static func randomBytes(numberOfBytes:Int) -> [UInt8] {
            var randomBytes = [UInt8](repeating: 0, count: numberOfBytes) // array to hold randoms bytes
            SecRandomCopyBytes(kSecRandomDefault, numberOfBytes, &randomBytes)
            return randomBytes
        }
        
        static func numberToRandom<T:ExpressibleByIntegerLiteral>(num:T) -> T {
            
            var num = num
            let randomBytes = Random.randomBytes(numberOfBytes: MemoryLayout<T>.size)
            // Turn bytes into data and pass data bytes into int
            NSData(bytes: randomBytes, length: MemoryLayout<T>.size).getBytes(&num, length: MemoryLayout<T>.size)
            return num
        }
        
        static func number64bit() -> UInt64 {
            return Random.numberToRandom(num: UInt64())  // variable for random unsigned 64
        }
        
        static func number32bit() -> UInt32 {
            return Random.numberToRandom(num: UInt32())  // variable for random unsigned 32
        }
        
        static func number16bit() -> UInt16 {
            return Random.numberToRandom(num: UInt16())  // variable for random unsigned 16
        }
        
        static func number8bit() -> UInt8 {
            return Random.numberToRandom(num: UInt8())  // variable for random unsigned 8
        }
        
        static func int() -> Int {
            
            return Random.numberToRandom(num: Int())  // variable for random Int
        }
        static func double() -> Double {
            
            return Random.numberToRandom(num: Double())  // variable for random Double
        }
        static func float() -> Float {
            
            return Random.numberToRandom(num: Float())  // variable for random Double
        }
    }
    
    func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    func randomize() {
        let r1 = Random.number64bit()
        //print("r1 is,", r1)
        
        let r2 =  Random.number32bit()
        //print("r2 is,", r2)
        
        let r3 = Random.number16bit()
        //print("r3 is,", r3)
        
        let r4 =  Random.number8bit()
        //print("r4 is,", r4)
        
        let r5 =  Random.int()
        //print("r5 is,", r5)
        
        let r6 =  Random.double()
        //print("r6 is,", r6)
        
        let r7 = Random.float()
        //print("r6 is,", r6)
    }
}

/*
 //MARK: Init self Info using External Data Source
 var ref_ModelController = ModelController()
 
 func getSelfProfileInfo_2() {
 //print("getSelfProfileInfo")
 let user = Auth.auth().currentUser!
 let selfUID = user.uid
 
 self.ref.child("users/\(selfUID)").observeSingleEvent(of: .value , with: { info in
 self.ref_ModelController.populateSelfProfileInfo(snapshot: info)
 })
 }
 
 func updateSelfProfileInfo_2() {
 //print("getSelfProfileInfo")
 let user = Auth.auth().currentUser!
 let selfUID = user.uid
 
 self.ref.child("users/\(selfUID)").observe(.childChanged, with: { info in
 //print("info is,", info)
 guard let snapshot = info.value as? [String: String] else { return }
 //
 //print("newname is,", snapshot["userDisplayName"]!)
 self.ref_ModelController.updateSelfProfileInfo(snapshot: info)
 self.printMyUserInfo()
 })
 }
 
 func printMyUserInfo() {
 //print("printMyUserInfo")
 //print("MOST RECENT DISPLAY NAME,", ref_ModelController.myUserInfo.selfDisplayName!)
 //print("MOST RECENT DISPLAY NAME,", ref_ModelController.myUserInfo.selfVisus!)
 }
 
 ////////////////////////////////
 
 */

