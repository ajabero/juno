//
//  UserProfileViewController.swift
//  Juno
//
//  Created by Asaad on 2/14/18.
//  Copyright © 2018 Animata Inc. All rights reserved
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth




class UserProfileViewController: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
//    var interactor:Interactor? = nil
    
    //Database Init
    var ref: DatabaseReference!
    fileprivate var _refHandle: DatabaseHandle!
    
    //FOR CHATLIST PREPARATION
    var chatListObjects = [[String : String]]()
    var chatListObjectIDs = [String]() //Array of unique list object ID's (swapID, chatRequestID, or ThreadID)
    var allUniqueUIDsinChatList = [String]()
    
    //CHAT UI CONTROL FLOW VARIABLES
    //... messageThreadData - GET from messageThreadDataObject
    var messageThreadIDDoesExist: Bool?
    var absExistingMessageThreadID = ""
    
    //...chatRequestData - GET form chatRequestDataObject
    var chatRequestDoesExist: Bool?; var selfInitiatedChatRequest: Bool?; var absExistingChatRequestID: String?;
    var chatRequestStatus = ""
    
    //MARK: Swap Control Flow Variables
    var swapRequestDoesExist = false; var absExistingSwapRequestID: String?
    var selfInitiatedSwapRequest: Bool?
    var swapRequestStatus = ""
    var swapRequestStatusDictionary = [String:String]()

    //myUserInfo //Self Info should always be retrieved from selfObject instantiated in GridVC
    var myProfileInfoDict = [String : String]()
    var unlockedUsers = ["", ""]
    var incomingDismissedUIDs = [String]()
    
    var selfUID: String?
    var selfDisplayName: String?
    var selfVisus: String? //always inherited from GridVC at source
    var selfDefaultPhotoURL: String?
    var selfPrivacy: String?
    
    //Other User Info
    var selectedUser = [String: String]()
    var userBio = "My name is Quavo."
    var lookingFor = "Chats, dates, friends, relationship"
    var userHeight = "6 ft"
    
    var otherUID: String? //otherUID is passed directly from CLVC or parsed from passed selectedUser["UID"] from ProfileVC //
    var otherDisplayName: String?
    var otherVisus: String? //always inherited from GridVC at source
    var otherDefaultPhotoURL: String?
    var otherPrivacy: String?
    
    var isOnline = true
    var didBlockUser = false
    
    var profileChangesUIDsInProximity = [String]()

    //MARK: Load View
    var importedStatusBarHeight: CGFloat?

    override func loadView() {
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let myInputView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        myInputView.backgroundColor = UIColor.white
        self.view = myInputView
        
        //set UILayout properties
        self.initTextInsets()
        
        //init user info
        self.initializeSelfProfileInfo()
        self.initializeOtherProfileInfo()
        
        //set final scrollView height
//        self.addImageScrollViewX()
        let finalScrollViewHeight = self.getFinalScrollViewHeight()
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = screenHeight * navSpaceRatio
        
        //initialize scrollView
//        let scrollFrame = CGRect(x: 0, y: navSpaceHeight, width: screenWidth, height: screenHeight - navSpaceHeight)
        let scrollView = UIScrollView(frame: screen)
//        let scrollView = UIScrollView(frame: scrollFrame)
        scrollView.contentSize = CGSize(width: screenWidth, height: finalScrollViewHeight)
        scrollView.showsVerticalScrollIndicator = false
        
        //add scrollView as Subview
        self.view.addSubview(scrollView)
        self.scrollView = scrollView
        self.scrollView!.delegate = self
        
        //add subviews
//        self.addProfileImageView()
//        self.addNavSpaceLayer()
//        self.addHomeIconButton()
        self.addImageScrollView()
        self.addImageFrames()
        self.setFinalPhotos()
        
        self.addBackButton()
//        self.addReportIcon()
        self.addGradientLayer()
        self.addBannerMask()
        self.addProfileBanner()
        self.addForegroundMask()
        
        self.addStatusIndicator()
        self.addNameLabel()
        self.addEducationIcon()
//        self.addSchoolLabel()
//        self.addDistanceLabel()
        
        self.addAboutMeHeader()
        self.addAboutMeTextView()
        
        self.addLookingForHeader()
        self.addLookingForLabel()
        
        self.addHeightHeader()
        self.addHeightLabel()
        self.addMoreActions()
        self.addActionSheet()
    }
    
    var imageScrollView: UIScrollView!
    var userNumberOfImages = 6 as CGFloat
//    newImageView.translatesAutoresizingMaskIntoConstraints = false
    
    /// Readjust the scroll view's content size in case the layout has changed.
    fileprivate func adjustScrollView() {
//        imageScrollView.contentSize =
//            CGSize(width: imageScrollView.frame.width * CGFloat(numPages),
//                   height: imageScrollView.frame.height - topLayoutGuide.length)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        // Switch the indicator when more than 50% of the previous/next page is visible.
//        let pageWidth = scrollView.frame.width
//        let page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
//        pageControl.currentPage = Int(page)
//
//        loadCurrentPages(page: pageControl.currentPage)
        
        
        if scrollView == self.imageScrollView {
            
            if self.imageScrollView.contentOffset.x < UIScreen.main.bounds.width {
                self.bannerViewIsManual = false
            }
            
        }
        
    }
    ///////
    
    let beachBeige = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
    
    var navBarColor = UIColor(displayP3Red: 246/255, green: 246/255, blue: 248/255, alpha: 1)
    var hyperBlue = UIColor(displayP3Red: 0, green: 108/255, blue: 255/255, alpha: 1)
    var lineSeparatorHeight = 0.35 as CGFloat
    let violetGrayTint = UIColor(displayP3Red: 240/255, green: 239/255, blue: 245/255, alpha: 1)

    var navSpaceLayer: UIView?
    var navEdge: UIView?
    
    func addNavSpaceLayer() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let navX = 0 as CGFloat
        let navY = 0 as CGFloat
        let navW = sWidth
        let navH = navSpaceHeight
        
        let navF = CGRect(x: navX, y: navY, width: navW, height: navH)
        let navView = UIView(frame: navF)
        navView.backgroundColor = self.navBarColor
        self.navSpaceLayer = navView
        
        self.view!.addSubview(navView)
        
        //& Line View
        //add Line Border
        let lineW = sWidth
        let lineH = 0.35 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = navSpaceHeight
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.view!.addSubview(lineView)
        self.navEdge = lineView
    }
    
    var scrollConstraints: [NSLayoutConstraint]!
    
    func addImageScrollView() {
        //        //initialize scrollView
        //        let scrollView = UIScrollView(frame: screen)
        //        scrollView.contentSize = CGSize(width: screenWidth, height: finalScrollViewHeight)
        //        scrollView.showsVerticalScrollIndicator = false
        //
        //        //add scrollView as Subview
        //        self.view.addSubview(scrollView)
        //        self.scrollView = scrollView
        //        self.scrollView!.delegate = self
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = 0 as CGFloat
        let imageView_Y = 0 as CGFloat

        //initialize view
        let scrollFrame = CGRect(x: imageView_X, y: 0, width: imageViewWidth, height: imageViewHeight)
        let imageScrollView = UIScrollView(frame: scrollFrame)
        imageScrollView.contentSize = CGSize(width: 4 * imageViewWidth, height: imageViewHeight)
        imageScrollView.backgroundColor = self.beachBeige
        
        self.scrollView!.addSubview(imageScrollView)
        self.imageScrollView = imageScrollView
        self.imageScrollView!.delegate = self
        imageScrollView.isPagingEnabled = true
        imageScrollView.showsHorizontalScrollIndicator = false
        
        let topConstraint = self.imageScrollView!.topAnchor.constraint(equalTo: self.scrollView!.topAnchor, constant: 0)
        let leftConstraint = self.imageScrollView!.leadingAnchor.constraint(equalTo: self.scrollView!.leadingAnchor, constant: 0)
//        let rightConstraint = self.profileImageView!.trailingAnchor.constraint(equalTo: self.imageScrollView!.trailingAnchor, constant: 0)
        
        scrollConstraints = [topConstraint, leftConstraint]
        
        NSLayoutConstraint.activate(scrollConstraints)
    }
    
    
    
    var scrollViewX: UIScrollView!
    
    func addImageScrollViewX() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = 10 as CGFloat
        let imageView_X = 0 as CGFloat
        let imageView_Y = 0 as CGFloat
        
        //initialize view
        let scrollFrame = CGRect(x: imageView_X, y: 0, width: imageViewWidth, height: imageViewHeight)
        let imageScrollView = UIScrollView(frame: scrollFrame)
        imageScrollView.contentSize = CGSize(width: 4 * imageViewWidth, height: imageViewHeight)
        imageScrollView.backgroundColor = self.beachBeige
        
        self.scrollView!.addSubview(imageScrollView)
        self.scrollViewX = imageScrollView
        self.scrollViewX!.delegate = self
        scrollViewX.isPagingEnabled = true
        scrollViewX.showsHorizontalScrollIndicator = false
    }
    
    var homeIconButton: UIButton?
    
    func addHomeIconButton() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        //        let rRatio = 37 / 414 as CGFloat
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = (sWidth - hW) * 0.5
        let insetFromStatusBar = 5 as CGFloat
        let hY = sBarH + insetFromStatusBar
        //        let hY = sBarH + ((navSpaceHeight - sBarH - hH) * 0.5)
        
        //
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let homeIconButton = UIButton(frame: f)
//        homeIconButton.setImage(UIImage(named: "homeIcon_Main"), for: .normal)
        homeIconButton.setImage(UIImage(named: "profileIconOutline"), for: .normal)
//        homeIconButton.addTarget(self, action: #selector(UserProfileViewController.homeIconButton(_:)), for: .touchUpInside)
//        homeIconButton.isUserInteractionEnabled = false
        self.view.addSubview(homeIconButton)
        self.homeIconButton = homeIconButton
    }
    
    @objc func homeIconButton(_ sender: UIButton) {
        //print("homeIconButton tap detected")
    }
    
    var reportButton: UIImageView!
    
    func addReportIcon() {
        let x = 0 as CGFloat
        let y = 0 as CGFloat
        let w = 38 as CGFloat
        let h = 34 as CGFloat
        
        let insetRatio = 19 / 414 as CGFloat
        let inset = insetRatio * UIScreen.main.bounds.width
        
        //init frame & view
        let bFrame = CGRect(x: x, y: y, width: w, height: h)
        let reportButton = UIImageView(frame: bFrame)

        //init view properties
//        reportButton.image = UIImage(named: "profileReportIcon")!
//        reportButton.image = UIImage(named: "profileReportIcon_LightGray")!
        reportButton.image = UIImage(named: "profileReportIcon_Black")!

//        reportButton.layer.shadowOpacity = 0.4
//        reportButton.layer.shadowOffset = CGSize(width: 0, height: 0)
//        reportButton.layer.shadowRadius = 1.5
        
        //add to subview
        self.scrollView!.addSubview(reportButton)
        self.view!.addSubview(reportButton)
        self.reportButton = reportButton
        
        let centerX = self.view.frame.maxX - (0.5 * w) - inset
        let centerY = self.backButtonView!.center.y
        
        self.reportButton.layer.position = CGPoint(x: centerX, y: centerY)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.moreActions(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.reportButton!.addGestureRecognizer(tapGesture)
        self.reportButton!.isUserInteractionEnabled = true
    }
    
    func addImageFrames() {
        
        self.getFinalNumberofUserPhotos()
        self.addScrollReelImage1()
        
        let height = self.profileImageView!.frame.height
        let width = self.profileImageView!.frame.width
        
        if self.finalPhotoArrayContains == 1 {
            self.imageScrollView.isScrollEnabled = false
            self.imageScrollView.contentSize = CGSize(width: 1 * width, height: height)
        } else if self.finalPhotoArrayContains == 2 {
            self.addScrollReelImage2()
            self.imageScrollView.contentSize = CGSize(width: 2 * width, height: height)
        } else if self.finalPhotoArrayContains == 3 {
            self.addScrollReelImage2()
            self.addScrollReelImage3()
            self.imageScrollView.contentSize = CGSize(width: 3 * width, height: height)
        } else if self.finalPhotoArrayContains == 4 {
            self.addScrollReelImage2()
            self.addScrollReelImage3()
            self.addScrollReelImage4()
            self.imageScrollView.contentSize = CGSize(width: 4 * width, height: height)
        }
    }
    
    //max is 3 - since finalPhotoArrayContains refers to total ADDITIONAL images (up to 3 additional images besides main image per user) THEREFORE max == 3 && != 4
    
    var finalPhotoArrayContains = 1
    
    var photoURL1 = ""
    var photoURL2 = ""
    var photoURL3 = ""
    
    func getFinalNumberofUserPhotos() {
        
        var url1 = ""
        var url2 = ""
        var url3 = ""

//        if let myPhotoURL1 = self.myProfileInfoDict["full_URL_2"] {
//            url1 = myPhotoURL1
//        }
//
//        if let myPhotoURL2 = self.myProfileInfoDict["full_URL_3"] {
//            url2 = myPhotoURL2
//        }
//
//        if let myPhotoURL3 = self.myProfileInfoDict["full_URL_4"] {
//            url3 = myPhotoURL3
//       }
    
        if let othPhotoUrl1 = self.selectedUser["full_URL_2"] {
            url1 = othPhotoUrl1
        }

        if let othPhotoUrl2 = self.selectedUser["full_URL_3"] {
            url2 = othPhotoUrl2
        }

        if let othPhotoUrl3 = self.selectedUser["full_URL_4"] {
            url3 = othPhotoUrl3
        }

  
        //cascade
        if url1 != "" {
            //1 is good
            photoURL1 = url1
            finalPhotoArrayContains = 2
            //
            if url2 != "" {
                //2 is good
                photoURL2 = url2
                finalPhotoArrayContains = 3
                //
                if url3 != "" {
                    //3 is good
                    photoURL3 = url3
                    finalPhotoArrayContains = 4
                } else {
                    //3 is no good
                    finalPhotoArrayContains = 3
                }

            } else {
                //2 is no good
                if url3 != "" {
                    //3 is good
                    photoURL2 = url3
                    finalPhotoArrayContains = 3
                } else {
                    //3 is no good
                    finalPhotoArrayContains = 2
                }
            }

        } else if url2 != "" {
        //1 not good but 2 is good
            photoURL1 = url2
            finalPhotoArrayContains = 2

            //
            if url3 != "" {
                photoURL2 = url2
                finalPhotoArrayContains = 3
            }

        } else if url3 != "" {
            photoURL1 = url3
            finalPhotoArrayContains = 2
            
        } else {
            finalPhotoArrayContains = 1
        }
    }
    
    func setFinalPhotos() {
        
        //the main user photo is set in viewWillAppear
        
        if finalPhotoArrayContains == 4 {
            self.imageView2?.sd_setImage(with: URL(string: photoURL1))
            self.imageView3?.sd_setImage(with: URL(string: photoURL2))
            self.imageView4?.sd_setImage(with: URL(string: photoURL3))

        } else if finalPhotoArrayContains == 3 {
            self.imageView2?.sd_setImage(with: URL(string: photoURL1))
            self.imageView3?.sd_setImage(with: URL(string: photoURL2))

        } else if finalPhotoArrayContains == 2 {
            self.imageView2?.sd_setImage(with: URL(string: photoURL1))
        }
    }
    
    var numberOfUserImages = 1
    var userImagesArray = [UIImage]()

    var image1Constraints: [NSLayoutConstraint]!
    
    func addScrollReelImage1() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = 0 as CGFloat
        let imageView_Y = 0 as CGFloat
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = UIColor.lightGray
        
//        if self.unlockedUsers.contains(selectedUser["UID"]!) {
//            if self.otherVisus! == "false" {
//                let clearImage = self.photoVault(otherUID: selectedUser["UID"]!)
//                self.profileImageView!.image = clearImage
//            } else {
//                self.profileImageView!.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
//            }
//        } else {
//            self.profileImageView!.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
//        }
        
//        profileImageView.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!)) //REVERT

        self.profileImageView = profileImageView
        self.imageScrollView!.addSubview(profileImageView)
        
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.profileImageView!.addGestureRecognizer(tapGesture)
        self.profileImageView!.isUserInteractionEnabled = true
        
        //
        let topConstraint = self.profileImageView!.topAnchor.constraint(equalTo: self.imageScrollView!.topAnchor, constant: 0)
        let leftConstraint = self.profileImageView!.leadingAnchor.constraint(equalTo: self.imageScrollView!.leadingAnchor, constant: 0)
//        let rightConstraint = self.profileImageView!.trailingAnchor.constraint(equalTo: self.scrollView!.trailingAnchor, constant: 0)
        
        image1Constraints = [topConstraint, leftConstraint]

        NSLayoutConstraint.activate(image1Constraints)

    }
    
    @objc func reel1(_ sender: UITapGestureRecognizer) {
        //print("reel1")
    }
    
    @objc func reel2(_ sender: UITapGestureRecognizer) {
        //print("reel2")
    }
    
    var imageView2: UIImageView?
    var imageView3: UIImageView?
    var imageView4: UIImageView?

    func addScrollReelImage2() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = imageViewWidth
        let imageView_Y = 0 as CGFloat
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = UIColor.lightGray
        profileImageView.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))

        
     
        
        
        self.imageView2 = profileImageView
        self.imageScrollView!.addSubview(profileImageView)
        
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.imageView2!.addGestureRecognizer(tapGesture)
        self.imageView2!.isUserInteractionEnabled = true
    }
    
  
    func addScrollReelImage3() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = 2 * imageViewWidth
        let imageView_Y = 0 as CGFloat
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = UIColor.lightGray
        profileImageView.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
        
        self.imageView3 = profileImageView
        self.imageScrollView!.addSubview(profileImageView)
        
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.imageView3!.addGestureRecognizer(tapGesture)
        self.imageView3!.isUserInteractionEnabled = true
    }
    
    func addScrollReelImage4() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = 3 * imageViewWidth
        let imageView_Y = 0 as CGFloat
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = UIColor.lightGray
        profileImageView.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
        
        self.imageView4 = profileImageView
        self.imageScrollView!.addSubview(profileImageView)
        
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.imageView4!.addGestureRecognizer(tapGesture)
        self.imageView4!.isUserInteractionEnabled = true
    }
    
    func observeSelectedUserPresence() {
        let otherUID = self.otherUID!
        //print("otherUID is,", otherUID)
        
        self.ref.child("globalUserPresence/\(otherUID)").observe(.childChanged) { (snapshot) in
            guard let snapshotValue = snapshot.value as? String else { return }
            //print("snapshotValue is,", snapshotValue)
            
            let userPresence = snapshotValue
            
            if userPresence == "true" {
                self.setUserStatusIndicator(withOnlineStatus: true)
            } else {
                self.setUserStatusIndicator(withOnlineStatus: false)
            }
        }
    }

    func setUserStatusIndicator(withOnlineStatus: Bool) {
        let status = self.statusIndicator!
        
        if withOnlineStatus == true {
            UIView.transition(with: status, duration: 0.25, options: .transitionCrossDissolve, animations: {
//                self.statusIndicator!.image = UIImage(named: "statusOnline")
                self.statusIndicator!.image = UIImage(named: "userOnline_Round")

            }, completion: nil)
            
//            self.statusIndicator!.image = UIImage(named: "statusOnline")
        } else if withOnlineStatus == false {
            UIView.transition(with: status, duration: 0.25, options: .transitionCrossDissolve, animations: {
//                self.statusIndicator!.image = UIImage(named: "statusOffline")
//                self.statusIndicator!.image = UIImage(named: "userOffline_Round")
                self.statusIndicator!.image = UIImage(named: "offlineSymb_WhiteFill")

            }, completion: nil)
            
//            self.statusIndicator!.image = UIImage(named: "statusOffline")
        }
    }
    
//    func addBackButton() {
//        //yposition needs to be statusbarheight relative
//        let sBarH = UIApplication.shared.statusBarFrame.size.height
//        let insetFromStatusBar = 5 as CGFloat
//
//        //
//        let wButton = 40 as CGFloat
//        let hButton = wButton
//        let xB = 0 as CGFloat
////        let yB = 0 as CGFloat
//        let yB = sBarH
//        //
//        let bFrame = CGRect(x: xB, y: yB, width: wButton, height: hButton)
//        let bImageView = UIImageView(frame: bFrame)
//
//        //
//        bImageView.image = UIImage(named: "profileBackButton")
//        self.scrollView!.addSubview(bImageView)
//        self.backButtonView = bImageView
//
//        //add slider key tapRecognizer
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.unwindToGrid(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        backButtonView!.addGestureRecognizer(tapGesture)
//        backButtonView!.isUserInteractionEnabled = true
//    }

    func addBackButton() {
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        let insetFromStatusBar = 5 as CGFloat
        
        let insetRatio = 19/414 as CGFloat
        let insetUnit = insetRatio * UIScreen.main.bounds.width
        
        //init properties
//        let bW = 30 as CGFloat
//        let bH = 30 as CGFloat
        let bW = 52 as CGFloat
        let bH = 56 as CGFloat
//        let bX = insetUnit
        let bX = 0 as CGFloat

        //        let bY = ((self.viewParameters?.topInset)! - bH) * 0.5
//        let bY = sBarH + insetFromStatusBar
        
        var bY = insetFromStatusBar + (0.5 * sBarH)
        if UIScreen.main.bounds.height > 750 as CGFloat {
            bY = sBarH + insetFromStatusBar
        }
        
        //init frame & view
        let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
        let backButtonImageView = UIImageView(frame: bFrame)
        
        //init view properties
//        backButtonImageView.image = UIImage(named: "profileBackButton")
        backButtonImageView.image = UIImage(named: "profileBackButtonLarge")!
//        backButtonImageView.image = UIImage(named: "profileBackButton_LightGray")!
//        backButtonImageView.image = UIImage(named: "profileBackButton_Black")!

        backButtonImageView.layer.shadowOpacity = 0.4
        backButtonImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backButtonImageView.layer.shadowRadius = 1.5
        
        //add to subview
//        self.scrollView!.addSubview(backShadowView)
//        self.scrollView!.addSubview(backButtonImageView)
        self.view.addSubview(backButtonImageView)

        self.backButtonView = backButtonImageView
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.unwindToGrid_BackArrow(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.backButtonView!.addGestureRecognizer(tapGesture)
        self.backButtonView!.isUserInteractionEnabled = true
        
        self.rotateBackArrow_AnimateUp()
    }
    
    var upvcShouldUnwindToChatScreen = false { didSet {
        print("didSet upvcShouldUnwindToChatScreen", upvcShouldUnwindToChatScreen)
        }}
    
    @objc func unwindToGrid_BackArrow(_ sender: UITapGestureRecognizer) {
        //print("detected back to grid")
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve, animations: {
            self.backButtonView?.alpha = 1
        }, completion: nil)
        
        self.backButtonView?.isUserInteractionEnabled = false
        
        //Note: method handles SEGUE LOGIC AFTER BUTTON ANIMATION FINISHES
        self.rotateBackArrow_Animate()

//        self.performSegue(withIdentifier: "unwindToGrid", sender: self)
        
    }
    
    func getFinalScrollViewHeight() -> CGFloat {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        
        //
        let marginSpaceError = 8 as CGFloat
        let sectionWidth =  screenWidth - 2 * (self.textInsets?.marginInset)! + marginSpaceError
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let bannerHeight = 100 as CGFloat
        let superInset = (self.textInsets?.superInset)!
        let bioTextViewHeight = 2 * (ceil(self.sizeOfString(string: self.userBio, constrainedToWidth: sectionWidth).height))
        let b_c_height = ((90 * 2) / 736) * screenHeight as CGFloat
        let headerAHeight = 23 as CGFloat
        let footerSpaceTotal = (self.textInsets?.footerInset)! * 3
        
        let scrollViewHeight = imageViewHeight + bannerHeight + superInset + bioTextViewHeight + b_c_height + headerAHeight + footerSpaceTotal - tIn
        
        return scrollViewHeight
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //disable bounce only at the top of the screen
        self.scrollView!.bounces = scrollView.contentOffset.y > 100
    }
    
    struct TextInsets {
        let superInsetRatio = 36 / 736 as CGFloat
        var superInset: CGFloat?
        
        let marginInsetRatio = 19 / 414 as CGFloat
        var marginInset: CGFloat?
        
        let footerInsetRatio = 30 / 736 as CGFloat
        var footerInset: CGFloat?
        
        let headerInsetRatio = 24 / 736 as CGFloat
        var headerInset: CGFloat?
        
        let bodyInset = -2 as CGFloat
    }
    
    var textInsets: TextInsets?
    
    func initTextInsets() {
        let superInsetRatio = 36 / 736 as CGFloat
        let marginInsetRatio = 19 / 414 as CGFloat
        let footerInsetRatio = 30 / 736 as CGFloat
        let headerInsetRatio = 24 / 736 as CGFloat
        
        let superInset = superInsetRatio * UIScreen.main.bounds.height
        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
        let footerInset = footerInsetRatio * UIScreen.main.bounds.height
        let headerInset = headerInsetRatio * UIScreen.main.bounds.height
        
        self.textInsets = TextInsets(superInset: superInset, marginInset: marginInset, footerInset: footerInset, headerInset: headerInset)
    }
    
    //MARK: UIView User Profile Objects
    var scrollView: UIScrollView?
    var profileImageView: UIImageView?
    var bannerImageView: UIImageView?
    
    var displayNameLabel: UILabel?
    var schoolLabel: UILabel?
    var statusIndicator: UIImageView?
    var distanceLabel: UILabel?
    
    var aboutMeHeaderLabel: UILabel?
    var aboutMeTextField: UITextView?
    var lineSeparatorView_a: UIView?
    
    var lookingForHeaderLabel: UILabel?
    var lookingForLabel: UILabel?
    var lineSeparatorView_b: UIView?
    
    var heightHeader: UILabel?
    var heightLabel: UILabel?
    var lineSeparatorView_c: UIView?
    
    var chatButton_Open: UIImageView?
    var sliderButtonImageView: UIImageView?
    var keyImageView: UIImageView?
    var wImageViewLHS_i: UIImageView?
    var wImageViewRHS_i: UIImageView?
    
    var largeDot: UIImageView?
    var backButtonView: UIImageView?
    
    //MARK: Add Views
    
    func addStatusIndicator() {
        //header frame properties
        let topInset = 11 as CGFloat
        
        let sWidth = 9 as CGFloat
        let sHeight = 33 as CGFloat
        let s_X = (self.textInsets?.marginInset)!
        let s_Y = topInset
        
        //init label
        let sLabelFrame = CGRect(x: s_X, y: s_Y, width: sWidth, height: sHeight)
        let statusImageView = UIImageView(frame: sLabelFrame)
        
        //
        self.bannerImageView!.addSubview(statusImageView)
        self.statusIndicator = statusImageView
        
        //
//        if self.isOnline == true {
//            self.statusIndicator!.isHidden = false
//            self.statusIndicator!.image = UIImage(named: "profileOnlineSymb")
//        } else {
//            self.statusIndicator!.isHidden = true
//        }
        
    }
    
    func addNameLabel() {
        let topInset = 11 as CGFloat
        
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let nameWidth = 5 * gridUnitWidth
        let nameHeight = 33 as CGFloat
        var name_X = (self.textInsets?.marginInset)!
        let nameInsetFromStatus = 4.0 as CGFloat
        
        if self.isOnline == true {
            name_X = (self.textInsets?.marginInset)! + self.statusIndicator!.bounds.width + nameInsetFromStatus
        } else {
            //
        }
        
        let name_Y = topInset
        
    
        
        //init label
        let nameLabelFrame = CGRect(x: name_X, y: name_Y, width: nameWidth, height: nameHeight)
        let displayNameLabel = UILabel(frame: nameLabelFrame)
    
        let displayName = self.selectedUser["userDisplayName"]!
        let userAge = self.selectedUser["userAge"]!
        
        let fontForName = UIFont(name: "AvenirNext-DemiBold", size: 24)
        let fontForAge = UIFont(name: "Avenir-Light", size: 24)
        
        let titleString = "\(displayName), \(userAge)"
       
        let nameToBold = "\(displayName),"
        let ageToNotBold = userAge
        
        let nameRange = (titleString as NSString).range(of: nameToBold)
        let ageRange = (titleString as NSString).range(of: ageToNotBold)

        let attributedString = NSMutableAttributedString(string: titleString)
        attributedString.addAttribute(NSAttributedStringKey.font, value: fontForName!, range: nameRange)
        attributedString.addAttribute(NSAttributedStringKey.font, value: fontForAge!, range: ageRange)

        displayNameLabel.attributedText = attributedString
        displayNameLabel.textColor = UIColor.white

        //
        self.bannerImageView!.addSubview(displayNameLabel)
        self.displayNameLabel = displayNameLabel
        self.displayNameLabel!.backgroundColor = UIColor.clear
    }
    
    var row1IconView: UIImageView?
    var row2IconView: UIImageView?
    var row1Label: UITextView?
    var row2Label: UITextView?
    
    func addEducationIcon() {
        //Row1 Properties
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //let row1 inset from top
        let row1InsetFromTop = 47 as CGFloat
        let row2InsetFromTop = 67 as CGFloat

        //add row 1
        let iW = 15 as CGFloat
        let iH = iW
        let iX = 0 as CGFloat
        let iY = row1InsetFromTop
        
        let iFrame = CGRect(x: iX, y: iY, width: iW, height: iH)
        let iView = UIImageView(frame: iFrame)
        iView.contentMode = .center
        iView.backgroundColor = UIColor.clear
     
        if let schoolTitle = self.selectedUser["school"] {
            //print("schoolTitle passed even if nil")
            if schoolTitle != "" {
                iView.image = UIImage(named: "educationIcon")!
                
                self.bannerImageView!.addSubview(iView)
                self.row1IconView = iView
                
                let centerX = (self.textInsets?.marginInset)! + (self.statusIndicator!.bounds.width * 0.5)
                let iLayerCenter = CGPoint(x: centerX, y: (iY + (0.5 * iH)))
                self.row1IconView!.layer.position = iLayerCenter
            
                //need to add second row
                let i2Y = row2InsetFromTop
                let i2Frame = CGRect(x: iX, y: i2Y, width: iW, height: iH)
                let i2View = UIImageView(frame: i2Frame)
                i2View.contentMode = .center

                i2View.image = UIImage(named: "distance_Icon")!
                
                self.bannerImageView!.addSubview(i2View)
                self.row2IconView = i2View
                
                let i2LayerCenter = CGPoint(x: centerX, y: (i2Y + (0.5 * iH)))
                self.row2IconView!.layer.position = i2LayerCenter

                self.addLabel1(andLabel2: true)
            //if schoolTitle.isEmpty = true
            } else {
                //row1 is distance label
                iView.image = UIImage(named: "distance_Icon")!

                self.bannerImageView!.addSubview(iView)
                self.row1IconView = iView
                
                let centerX = (self.textInsets?.marginInset)! + (self.statusIndicator!.bounds.width * 0.5)
                let iLayerCenter = CGPoint(x: centerX, y: (iY + (0.5 * iH)))
                self.row1IconView!.layer.position = iLayerCenter
                
                self.addLabel1(andLabel2: false)
            }
        }
    }
    
    func addLabel1(andLabel2: Bool) {
        //add row 1 label as! TextView
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        let iW = 15 as CGFloat

        //let row1 inset from top
        let row1InsetFromTop = 47 as CGFloat
        let row2InsetFromTop = 67 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
    
        // Font Size
        let labelFontSize = 14.0 as CGFloat
        let labelWidth = 4 * gridUnitWidth
        
        var labelHeight = 0 as CGFloat
        let testTitleString = "Hello"
        let textFieldCGSize = self.sizeOfCopy(string: testTitleString, constrainedToWidth: Double(labelWidth), fontName: "Avenir-Light", fontSize: labelFontSize)
//        let tH = ceil(textFieldCGSize.height)
        let tH = textFieldCGSize.height

        labelHeight = tH
        
        let insetFromIcon = 6 as CGFloat
        let labelX = (self.textInsets?.marginInset)! + iW + insetFromIcon
        let labelY = self.row1IconView!.frame.minY
        
        //
        let labelFrame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        let row1Label = UITextView(frame: labelFrame)

        if let schoolTitle = self.selectedUser["school"] {
            //print("schoolTitle passed even if nil")
            if schoolTitle != "" {
                row1Label.text = schoolTitle
            } else {
                row1Label.text = self.selectedUser["distanceTo"]!
            }
        }
    
    
        self.bannerImageView!.addSubview(row1Label)
        self.row1Label = row1Label

        //affix position to icon
        let centerX = labelX + (labelWidth * 0.5)
        let centerY = self.row1IconView!.layer.position.y
        self.row1Label!.layer.position = CGPoint(x: centerX, y: centerY)
        
        //no Icon Code
        let noViewX = self.statusIndicator!.frame.minX
        self.row1Label!.frame = CGRect(x: noViewX, y: self.row1Label!.frame.minY, width: labelWidth, height: labelHeight)
        self.row1IconView?.isHidden = true
        
        //
        self.row1Label!.font = UIFont(name: "Avenir-Light", size: 14.0)!
        self.row1Label!.textAlignment = .left
        self.row1Label!.isEditable = false
        self.row1Label!.isScrollEnabled = false
        self.row1Label!.isSelectable = false
        self.row1Label!.textContainer.lineFragmentPadding = 0
        self.row1Label!.textContainerInset = .zero
        self.row1Label!.textColor = UIColor.white
        self.row1Label!.backgroundColor = UIColor.clear
        
        if andLabel2 == true {
            //
            let label2Frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
            let row2Label = UITextView(frame: label2Frame)
            
            row2Label.text = self.selectedUser["distanceTo"]!
            
            self.bannerImageView!.addSubview(row2Label)
            self.row2Label = row2Label
            
            //affix position to icon
            let centerX = labelX + (labelWidth * 0.5)
            let center2Y = self.row2IconView!.layer.position.y
            self.row2Label!.layer.position = CGPoint(x: centerX, y: center2Y)
            
            //
            //no Icon Code
            self.row2Label!.frame = CGRect(x: noViewX, y: self.row2Label!.frame.minY, width: labelWidth, height: labelHeight)
            self.row2IconView?.isHidden = true
            
            //
            self.row2Label!.font = UIFont(name: "Avenir-Light", size: 14.0)!
            self.row2Label!.textAlignment = .left
            self.row2Label!.isEditable = false
            self.row2Label!.isScrollEnabled = false
            self.row2Label!.isSelectable = false
            self.row2Label!.textContainer.lineFragmentPadding = 0
            self.row2Label!.textContainerInset = .zero
            self.row2Label!.textColor = UIColor.white
            self.row2Label!.backgroundColor = UIColor.clear
            
        } else {
            
        }
    }
    
    func addLabel2() {
        //add row 1 label as! TextView
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        let iW = 15 as CGFloat
        
        //let row1 inset from top
        let row1InsetFromTop = 47 as CGFloat
        let row2InsetFromTop = 65 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        // Font Size
        let labelFontSize = 14.0 as CGFloat
        let labelWidth = 4 * gridUnitWidth
        
        var labelHeight = 0 as CGFloat
        let testTitleString = "Hello"
        let textFieldCGSize = self.sizeOfCopy(string: testTitleString, constrainedToWidth: Double(labelWidth), fontName: "Avenir-Light", fontSize: labelFontSize)
        //        let tH = ceil(textFieldCGSize.height)
        let tH = textFieldCGSize.height
        
        labelHeight = tH
        
        let insetFromIcon = 6 as CGFloat
        let labelX = (self.textInsets?.marginInset)! + iW + insetFromIcon
        let labelY = self.row2IconView!.frame.minY
        
        //
        let label2Frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        let row2Label = UITextView(frame: label2Frame)
        
        row2Label.text = self.selectedUser["distanceTo"]!
        
        self.bannerImageView!.addSubview(row2Label)
        self.row2Label = row2Label
        
        //affix position to icon
        let centerX = labelX + (labelWidth * 0.5)
        let center2Y = self.row2IconView!.layer.position.y
        self.row2Label!.layer.position = CGPoint(x: centerX, y: center2Y)
        
        //
        self.row2Label!.font = UIFont(name: "Avenir-Light", size: 14.0)!
        self.row2Label!.textAlignment = .left
        self.row2Label!.isEditable = false
        self.row2Label!.isScrollEnabled = false
        self.row2Label!.isSelectable = false
        self.row2Label!.textContainer.lineFragmentPadding = 0
        self.row2Label!.textContainerInset = .zero
        self.row2Label!.textColor = UIColor.white
        self.row2Label!.backgroundColor = UIColor.clear
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    
    func addProfileImageView() {
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let imageAspectRatio = 500 / 414 as CGFloat
//        let imageAspectRatio = 529 / 414 as CGFloat

        let imageViewWidth = screenWidth
        let imageViewHeight = imageViewWidth * imageAspectRatio
        let imageView_X = 0 as CGFloat
//        let imageView_Y = 0 as CGFloat
        let imageView_Y = self.navSpaceLayer!.frame.maxY

        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView = UIImageView(frame: imageViewFrame)
        
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.backgroundColor = UIColor.lightGray
        profileImageView.image = UIImage(named: "Dale")
        
        //
        self.profileImageView = profileImageView
//        self.scrollView!.addSubview(profileImageView)
        self.imageScrollView!.addSubview(profileImageView)
        
//            labelConstraints = [titleLabel.bottomAnchor.constraint(equalTo: viewAllCommentsLabel.topAnchor,
//                                                                   constant: betweenConstant),
        
        //add gradient bar
//        let gradientFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: 25 as CGFloat)
//        let gradientBar = UIImageView(frame: gradientFrame)
//        gradientBar.image = UIImage(named: "gradientBar_Plus")!
//        self.scrollView!.addSubview(gradientBar)

        //add Pan Gesture
//        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(UserProfileViewController.slideScreenDown(_:)))
//        panGesture.delegate = self
//        panGesture.minimumNumberOfTouches = 1
//        self.profileImageView!.addGestureRecognizer(panGesture)
//        self.profileImageView!.isUserInteractionEnabled = true
    }
    
    var bannerGradient: CAGradientLayer?
    
    func addProfileBanner() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let bannerViewWidth = screenWidth
        let bannerViewHeight = 100 as CGFloat
        let bannerView_X = 0 as CGFloat
//        let bannerView_Y = self.profileImageView!.bounds.height - bannerViewHeight
        let bannerView_Y = self.imageScrollView!.frame.maxY - bannerViewHeight

        //initialize view
        let bannerViewFrame = CGRect(x: bannerView_X, y: bannerView_Y, width: bannerViewWidth, height: bannerViewHeight)
        let bannerImageView = UIImageView(frame: bannerViewFrame)
        
//        bannerImageView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        bannerImageView.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        //bannerImageView.alpha = 0.8
        
        //add as Subview
        self.scrollView!.addSubview(bannerImageView)
        self.bannerImageView = bannerImageView
        bannerImageView.clipsToBounds = true
        bannerImageView.layer.masksToBounds = true
        
        //
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.bannerImageView!.addGestureRecognizer(tapGesture)
        self.bannerImageView!.isUserInteractionEnabled = true
    }
    
    var bannerMask: UIView?
    
    func addBannerMask() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let bannerViewWidth = screenWidth
        let bannerViewHeight = 100 as CGFloat
        let bannerView_X = 0 as CGFloat
        //        let bannerView_Y = self.profileImageView!.bounds.height - bannerViewHeight
        let bannerView_Y = self.imageScrollView!.frame.maxY - bannerViewHeight
        
        //initialize view
        let bannerViewFrame = CGRect(x: bannerView_X, y: bannerView_Y, width: bannerViewWidth, height: bannerViewHeight)
        let bannerView = UIImageView(frame: bannerViewFrame)
        
        //        bannerImageView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        bannerView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        //bannerImageView.alpha = 0.8
        
        //add as Subview
        self.scrollView!.addSubview(bannerView)
        self.bannerMask = bannerView
        
        //
        //add tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.hideShowBannerView(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.bannerMask!.addGestureRecognizer(tapGesture)
        self.bannerMask!.isUserInteractionEnabled = true
    }
    
    var foregroundMask: UIView!
    
    func addForegroundMask() {
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //set view properties
        let bannerViewWidth = screenWidth
        let bannerViewHeight = 100 as CGFloat
        let bannerView_X = 0 as CGFloat
        let bannerView_Y = self.imageScrollView!.frame.maxY
        
        //initialize view
        let fFrame = CGRect(x: bannerView_X, y: bannerView_Y, width: bannerViewWidth, height: bannerViewHeight)
        let fMask = UIView(frame: fFrame)
        
        fMask.backgroundColor = UIColor.white
        self.foregroundMask = fMask
        
        self.scrollView!.addSubview(fMask)
    }
    
    @objc func hideShowBannerView(_ sender: UITapGestureRecognizer) {
        //print("hideShowBannerView")
//        self.imageScrollView!.scrollRectToVisible(self.imageView2!.frame, animated: true)
//        self.scrollViewX.scrollRectToVisible(self.imageView2!.frame, animated: true)
//        self.animateTapToShowHideBanner()
        
        guard self.imageScrollView!.contentOffset.x >= (0.5 * UIScreen.main.bounds.width) else { return }

        if self.bannerViewDidSet == false {
            self.bannerViewDidSet = true
        } else {
            self.bannerViewDidSet = false
        }
    
        self.animateTapToShowHideBanner()
    }
    
    func animateFadeBannerView(withContentOffset: CGFloat) {
        
        guard bannerViewIsManual == false else { return }
        guard withContentOffset >= 0 else { return }

        let fadeFactor = (withContentOffset / (UIScreen.main.bounds.width * 0.5))
        let fadeBy = 1 - (fadeFactor * 1.5)
        
        //print("fadeFactor is,", fadeFactor)
        let newColors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0.8 * fadeBy).cgColor]
        
        self.bannerGradient?.colors = newColors
    }
    
    //logic:
    //1. banner visible by default on View1
    //2. banner can be called by tapping on imageView - when called: bannerViewIsSticky = true.
    //Once user calls bannerView, it's always visible until it's called back
    
    /*
    func animateTapToShowHideBanner() {
        let chatButton = self.chatButton_Open!
        let nameLabel = self.displayNameLabel!
        let row1Label = self.row1Label!
        let presenceLabel = self.statusIndicator!
        
        let maxColors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0.8).cgColor]
        let yDisplacement = (1 / 6) * self.bannerImageView!.frame.height

//        nameLabel.transform = CGAffineTransform(translationX: 0, y: -yDisplacement)

//        nameLabel.transform = CGAffineTransform.identity.translatedBy(x: 0, y: -yDisplacement)
       
        UIView.animate(withDuration: 0.20, delay: 0, options: .transitionCrossDissolve, animations: {
            //grad
            self.bannerGradient?.colors = maxColors

            //labels
            nameLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            presenceLabel.transform = CGAffineTransform(translationX: 0, y: 0)
            row1Label.transform = CGAffineTransform(translationX: 0, y: 0)
            row1Label.alpha = 1
            
            //chat
            chatButton.alpha = 1
            chatButton.transform = CGAffineTransform(translationX: 0, y: 0)
            chatButton.transform = chatButton.transform.scaledBy(x: 1, y: 1)

        }, completion: nil)
    }
 */
    
    var bannerViewDidSet = false
    var bannerViewIsManual = false
    
    func animateTapToShowHideBanner() {
        
        guard self.finalPhotoArrayContains >= 2 else { return }
        guard self.imageScrollView!.contentOffset.x >= (0.5 * UIScreen.main.bounds.width) else { return }
            
        self.bannerViewIsManual = true
        
        let chatButton = self.chatButton_Open!
        let nameLabel = self.displayNameLabel!
        let row1Label = self.row1Label!
        let presenceLabel = self.statusIndicator!
        let backButton = self.backButtonView!
        let duration = 0.25 as Double

        if bannerViewDidSet == false {
            let maxColors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0.8).cgColor]
            
            UIView.animate(withDuration: duration, delay: 0, options: .transitionCrossDissolve, animations: {
                //grad
//                self.bannerGradient?.colors = maxColors
                self.bannerMask!.transform = CGAffineTransform(translationX: 0, y: 0)
//                self.bannerMask!.alpha = 1

                //labels
                nameLabel.transform = CGAffineTransform(translationX: 0, y: 0)
                presenceLabel.transform = CGAffineTransform(translationX: 0, y: 0)
                row1Label.transform = CGAffineTransform(translationX: 0, y: 0)
                self.row2Label?.transform = CGAffineTransform(translationX: 0, y: 0)

                //chat
                backButton.alpha = 1
                chatButton.transform = CGAffineTransform(translationX: 0, y: 0)
                chatButton.transform = chatButton.transform.scaledBy(x: 1, y: 1)
                
            }, completion: nil)
            
            UIView.animate(withDuration: duration / 1.5, delay: 0, options: .transitionCrossDissolve, animations: {
                chatButton.alpha = 1
                self.bannerMask!.alpha = 1
                row1Label.alpha = 1
                self.row2Label?.alpha = 1

            }, completion: nil)
            
        } else {
            let maxColors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0).cgColor]
            
            UIView.animate(withDuration: duration, delay: 0, options:  [.transitionCrossDissolve, .curveEaseInOut], animations: {
                //grad
//                self.bannerGradient?.colors = maxColors
                self.bannerMask!.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
//                self.bannerMask!.alpha = 0

                //labels
                nameLabel.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
                presenceLabel.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
                row1Label.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
                self.row2Label?.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
//                row1Label.alpha = 0
//                self.row2Label?.alpha = 0

                //chat
                backButton.alpha = 0.5
                chatButton.transform = CGAffineTransform(translationX: 0, y: ((1 / 3) * self.bannerImageView!.frame.height))
                chatButton.transform = chatButton.transform.scaledBy(x: 1/3, y: 1/3)
                
            }, completion: nil)
            
            UIView.animate(withDuration: duration / 1.5, delay: 0, options: [.transitionCrossDissolve, .curveEaseInOut], animations: {
                chatButton.alpha = 0
                self.bannerMask!.alpha = 0
                row1Label.alpha = 0
                self.row2Label?.alpha = 0
                
            }, completion: nil)
        }
    }
    
//    bannerViewDidSet true -> down
//    bannerViewDidSet false -> up
    
    func animateBannerSubviewTransformations(withContentOffset: CGFloat) {

        //Note: enable guard statement if you want user to toggle banner on main view
//        guard bannerViewIsManual == false else { return }
        
        if bannerViewIsManual == false {
        
            //
        } else if bannerViewIsManual == true {
            
            //when we;re on second frame, we want frame to not animate if position is up
            if self.imageScrollView.contentOffset.x < UIScreen.main.bounds.width {
                if self.bannerViewDidSet == false {
                    return
                }
            } else {
                return
            }
            
//            if self.imageScrollView.contentOffset.x < UIScreen.main.bounds.width {
//                if self.bannerViewDidSet == false {
//                    return
//                }
//            } else {
//                return
//            }
        }
        
        self.bannerViewDidSet = true
        guard withContentOffset >= 0 else { return }
        
        let nameLabel = self.displayNameLabel!
        let row1Label = self.row1Label!
        let presenceLabel = self.statusIndicator!
        let chatButton = self.chatButton_Open!
        let backButton = self.backButtonView!
        
        let yDisplacement = (1 / 6) * self.bannerImageView!.frame.height
        
        let displaceMentFactor = (withContentOffset / (UIScreen.main.bounds.width * 0.5))
        
        let translationY = yDisplacement * displaceMentFactor
        //print("translationY is,", translationY)
        
        guard translationY <= (1 / 3) * self.bannerImageView!.frame.height else { return }
        
        //banner position animations
//        let newX = UIScreen.main.bounds.width * 0.5
//        let newY = 450 + translationY
//        //print("gradLayer yFrame,", newY)
//        let newCGpoint = CGPoint(x: newX, y: 450 + translationY)
//        self.bannerGradient!.position = newCGpoint
        
        nameLabel.transform = CGAffineTransform.identity.translatedBy(x: 0, y: translationY)
        row1Label.transform = CGAffineTransform.identity.translatedBy(x: 0, y: translationY)
        row2Label?.transform = CGAffineTransform.identity.translatedBy(x: 0, y: translationY)
        presenceLabel.transform =  CGAffineTransform.identity.translatedBy(x: 0, y: translationY)
        self.bannerMask!.transform = CGAffineTransform.identity.translatedBy(x: 0, y: translationY)
        self.bannerMask!.alpha = 1 * (1 - (displaceMentFactor * 1.5))

//        nameLabel.transform = CGAffineTransform(translationX: 0, y: translationY)
//        row1Label.transform = CGAffineTransform(translationX: 0, y: translationY)
//        presenceLabel.transform = CGAffineTransform(translationX: 0, y: translationY)

        row1Label.alpha = 1 * (1 - (displaceMentFactor * 1.5))
        row2Label?.alpha = 1 * (1 - (displaceMentFactor * 1.5))

//        nameLabel.alpha = 1 * (1 - (displaceMentFactor * 1.5))
//        presenceLabel.alpha = 1 * (1 - (displaceMentFactor * 1.5))
        
        //chatButtonAnimations
        chatButton.transform = CGAffineTransform(translationX: 0, y: translationY)
        chatButton.alpha = 1 * (1 - (displaceMentFactor * 1.5))
        chatButton.transform = chatButton.transform.scaledBy(x: 1 * (1 - (displaceMentFactor * 0.25)), y: 1 * (1 - (displaceMentFactor * 0.25)))
        let maxXScale = 1 * (1 - (displaceMentFactor * 0.25))
        //print("maxXScale", maxXScale)
        
        //backButtonAnimations
        var backBlurFactor = (1 - (displaceMentFactor * 0.25))
        if backBlurFactor >= 0.3 {
            backBlurFactor = (1 - (displaceMentFactor * 0.25))
            //print("backBlurFactor is,", backBlurFactor)
            backButton.alpha = 1 * backBlurFactor
        }
    
//        let newY = self.bannerGradient!.bounds.minY - translationY
//        self.bannerGradient!.bounds = CGRect(x: 0, y: newY, width: UIScreen.main.bounds.width, height: 100)
        
//        backButton.transform = chatButton.transform.scaledBy(x: 1 * (1 - (displaceMentFactor)), y: 1 * (1 - (displaceMentFactor)))
        
        //opacity should reduce to 0 when button reaches half its size
//        let old_cX = self.bannerGradient!.frame.midX
//        let old_cY = self.bannerGradient!.frame.midY
//        //print("oldCY is,", old_cY)
//        let new_cY = old_cY + translationY
//
//        let newCenterPoint = CGPoint(x: old_cX, y: new_cY)
//        self.bannerGradient!.position = newCenterPoint
        
//        let shrink = CABasicAnimation(keyPath: "transform.scale")
//        shrink.toValue = 1 * (1 - (displaceMentFactor * 3))
//        chatButton.layer.add(shrink, forKey: "shrink")
//        let transformBy = 1 * (1 - (displaceMentFactor * 2))
//        UIView.animate(withDuration: 0.0, delay: 0.0, options: .curveLinear , animations: {
//            chatButton.transform.scaledBy(x: transformBy, y: transformBy)
//        }, completion: nil)
//        gradientLayer.position = CGPoint(x: 0, y: 0)
    }
    
    
    
    func addGradientLayer() {
        let startPoint: CGFloat?
        let endPoint: CGFloat?
//        let colors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0.8).cgColor]
        let colors = [UIColor.black.withAlphaComponent(0.8).cgColor, UIColor.black.withAlphaComponent(0.0).cgColor]

        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        let gradW = screenWidth
        let gradH = 100 as CGFloat
        let gradX = 0 as CGFloat
//        let gradY = self.profileImageView!.bounds.height - gradH
        let gradY = self.imageScrollView!.frame.maxY - gradH

        let gradient = CAGradientLayer()
        
        let gradFrame = CGRect(x: gradX, y: gradY, width: gradW, height: gradH)
        gradient.frame = gradFrame
    
        gradient.startPoint = CGPoint(x: 0.5, y: 1)
        gradient.endPoint = CGPoint(x: 0.5, y: 0)
        gradient.colors = colors
        
        self.scrollView?.layer.addSublayer(gradient)
        self.bannerGradient = gradient
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.imageScrollView {
//            //print("scrollView content offset X is,", scrollView.contentOffset.x)
//            self.animateFadeBannerView(withContentOffset: <#CGFloat#>)
        }
    }
    
    func addOffsetObserver() {
        //print("UPVC addOffsetObserver")
        
//        self.scrollView!.addObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: UIScrollView.contentOffset)), options: .new, context: nil)
        
        self.imageScrollView.addObserver(self, forKeyPath: NSStringFromSelector(#selector(getter: UIScrollView.contentOffset)), options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        /*
        Define contentOffset
        Content Offset: The point at which the origin of the content view is offset from the origin of the scroll view.
        The default value is CGPointZero.
        */
        
        //GOT IT: THE CONTENT OFFSET IS THE POINT AT WHICH THE ORIGIN OF THE CONTENT VIEW
        //CONTENT OFFSET REPRESENTS HOW MUCH THE CONTENT VIEW IS OFFSET FROM THE ORIGIN OF THE SCROLL VIEW.
        //IE IF SCROLLVIEW ORIGIN IS (0,0) (origin of Screen) && you displace a horizontally scrolling scrollview's content left THE origin of contentview shifts left wrt oriign of scrolView && hence consittues a positive contentOffset
        
//        //print("keyPath", keyPath)
//        //print("object", object)
//        //print("change", change)

        if keyPath == "contentOffset" {
            //print("keypath is,", keyPath)
            
            let newOffsetX = self.imageScrollView.contentOffset.x
            //print("observeValue newOffsetX", newOffsetX)
//            self.animateFadeBannerView(withContentOffset: newOffsetX)
            self.animateBannerSubviewTransformations(withContentOffset: newOffsetX)

//            let newOffsetY = self.scrollView!.contentOffset.y

//            self.scaleUIImageView(forYOffset: newOffsetY)

            
//            if self.imageScrollView.contentOffset.y == 0 {
//                //print("offset for imageScrollView")
//
//                let newOffsetX = self.imageScrollView.contentOffset.x
//                //print("observeValue newOffsetX", newOffsetX)
//                self.animateFadeBannerView(withContentOffset: newOffsetX)
//                self.animateBannerSuviewTransformations(withContentOffset: newOffsetX)
//            }
//
//            if self.scrollView!.contentOffset.x == 0 {
//                //print("offset for scrollView")
//            }
        }
    }
//
//    //screen properties
//    let screen = UIScreen.main.bounds
//    let screenWidth = screen.width
//    let screenHeight = screen.height
//
//    //set view properties
//    let imageAspectRatio = 500 / 414 as CGFloat
//    //        let imageAspectRatio = 529 / 414 as CGFloat
//
//    let imageViewWidth = screenWidth
//    let imageViewHeight = imageViewWidth * imageAspectRatio
    
    var oldFactor = 0 as CGFloat

    
    func scaleUIImageView(forYOffset: CGFloat) {
        
//        guard forYOffset <= 0 else { return }
        
//        let scaleFactor = -(forYOffset / UIScreen.main.bounds.height)
        
        //print("forYOffset is,", forYOffset)

        let scaleFactor = -(forYOffset / self.scrollView!.frame.height)

        let absFactor = (fabs(forYOffset) /  self.scrollView!.frame.height)
        
//        let displaceMentFactor = (withContentOffset / (UIScreen.main.bounds.width * 0.5))

        
        if absFactor > oldFactor {
            oldFactor = absFactor
//            let scaleBy = 1.0 + absFactor
            let scaleBy = 1 * (1 + (absFactor * 0.25))
            self.imageScrollView.transform = self.imageScrollView.transform.scaledBy(x: scaleBy, y: scaleBy)
        } else {
//            let scaleBy = 1.0 - absFactor
            let scaleBy = 1 * (1 - (absFactor * 0.25))
            self.imageScrollView.transform = self.imageScrollView.transform.scaledBy(x: scaleBy, y: scaleBy)
        }
        
 
//        chatButton.transform = chatButton.transform.scaledBy(x: 1 * (1 - (displaceMentFactor * 0.25)), y: 1 * (1 - (displaceMentFactor * 0.25)))

        
        var peakScale = 0 as CGFloat
        
        if scaleFactor > peakScale {
            peakScale = scaleFactor
            //print("scaleBy is increasing")
            //print("peakScale while increasing is,", peakScale)
        } else {
            //print("peakScale while decreasing is,", peakScale)
            //print("scaleBy is decreasing,", scaleFactor)
        }
        
//        if scaleFactor
//        self.imageScrollView.transform = self.imageScrollView.transform.scaledBy(x: 1, y: 1)
        
//        self.imageScrollView.transform = self.imageScrollView.transform.scaledBy(x: scaleBy, y: scaleBy)

        
        let increaseHeightBy = -forYOffset
        

        

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView!) {
        // This will be called every time the user scrolls the scroll view with their finger
        // so each time this is called, contentOffset should be different.
        //print("scrollViewDidScroll")
        //print(self.imageScrollView.contentOffset.x)
        
        //Additional workaround here.
    }

    let tIn = 100 as CGFloat
    let lineColor = UIColor(displayP3Red: 225/250, green: 225/250, blue: 225/250, alpha: 225/250)
    let labelGray = UIColor(displayP3Red: 210/255, green: 210/255, blue: 210/255, alpha: 1)
    let finalHeaderAlpha = 0.4 as CGFloat
    
    func addAboutMeHeader() {
        //header frame properties
        let headerWidth = 100 as CGFloat
        let headerHeight = 23 as CGFloat
        let header_X = (self.textInsets?.marginInset)!
//        let header_Y = self.profileImageView!.bounds.height + self.bannerImageView!.bounds.height + (self.textInsets?.superInset)! - tIn
        let header_Y = self.imageScrollView!.bounds.height + self.bannerImageView!.bounds.height + (self.textInsets?.superInset)! - tIn

        //init label
        let sectionHeaderFrame = CGRect(x: header_X, y: header_Y, width: headerWidth, height: headerHeight)
        let sectionHeaderLabel = UILabel(frame: sectionHeaderFrame)
        
        let fontName = "AvenirNext-DemiBold"
//        let fontName = "AvenirNext-UltraLight"
//        let fontName = "Avenir-Light"
        sectionHeaderLabel.text = "ABOUT ME"
        sectionHeaderLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 17)
        sectionHeaderLabel.font = UIFont(name: fontName, size: 12)
        sectionHeaderLabel.addCharacterSpacing(withValue: 1.0)
        sectionHeaderLabel.textColor = UIColor.black
        sectionHeaderLabel.alpha = finalHeaderAlpha
        
        //
        self.scrollView!.addSubview(sectionHeaderLabel)
        self.aboutMeHeaderLabel = sectionHeaderLabel
    }

    

    
    func addAboutMeTextView() {
        let aboutMeString = self.selectedUser["aboutMe"]!
        
        //
        let marginSpaceError = 8 as CGFloat
        let sectionWidth =  self.profileImageView!.bounds.width - 2 * (self.textInsets?.marginInset)! + marginSpaceError
        let textFieldCGSize = self.sizeOfString(string: aboutMeString, constrainedToWidth: sectionWidth)
        let sectionHeight = ceil(textFieldCGSize.height)
        let section_X = (self.textInsets?.marginInset)! - marginSpaceError * 0.5
        let section_Y = self.profileImageView!.bounds.height + self.bannerImageView!.bounds.height + (self.textInsets?.superInset)! + self.aboutMeHeaderLabel!.bounds.height + (self.textInsets?.bodyInset)! - tIn
        
        //
        let aboutMeTextFieldFrame = CGRect(x: section_X, y: section_Y, width: sectionWidth, height: sectionHeight)
        let aboutMeTextField = UITextView(frame: aboutMeTextFieldFrame)
        
        //
        aboutMeTextField.text = aboutMeString
        aboutMeTextField.font = UIFont(name: "Avenir-Light", size: 14)
        aboutMeTextField.isEditable = false
        aboutMeTextField.isScrollEnabled = false
        aboutMeTextField.sizeToFit()
        
        //
        self.scrollView!.addSubview(aboutMeTextField)
        self.aboutMeTextField = aboutMeTextField
        
        //add LineSeparator
        
        //line separator properties
        let lineWidth = UIScreen.main.bounds.width
        let lineHeight = 0.5 as CGFloat
        let line_X = 0 as CGFloat
        let line_Y = self.profileImageView!.bounds.height + self.bannerImageView!.bounds.height + (self.textInsets?.superInset)! + self.aboutMeHeaderLabel!.bounds.height + (self.textInsets?.bodyInset)! + aboutMeTextField.bounds.height + (self.textInsets?.footerInset)! - tIn
        
        //
        let lineFrame = CGRect(x: line_X, y: line_Y, width: lineWidth, height: lineHeight)
        let lineSeparatorView_a = UIView(frame: lineFrame)
        
        lineSeparatorView_a.backgroundColor = lineColor
        
        //
        self.scrollView!.addSubview(lineSeparatorView_a)
        self.lineSeparatorView_a = lineSeparatorView_a
    }
    
    func addLookingForHeader() {
        //header frame properties
        let headerWidth = 200 as CGFloat
        let headerHeight = 23 as CGFloat
        let header_X = (self.textInsets?.marginInset)!
        let header_Y = self.lineSeparatorView_a!.frame.maxY + self.lineSeparatorView_a!.bounds.height + (self.textInsets?.headerInset)!
        
        //init label
        let sectionHeaderFrame = CGRect(x: header_X, y: header_Y, width: headerWidth, height: headerHeight)
        let sectionHeaderLabel = UILabel(frame: sectionHeaderFrame)
        
        let fontName = "AvenirNext-DemiBold"
        //        let fontName = "AvenirNext-UltraLight"
//        let fontName = "Avenir-Light"
        sectionHeaderLabel.text = "HERE FOR"
        sectionHeaderLabel.font = UIFont(name: fontName, size: 17)
        sectionHeaderLabel.textColor = UIColor.black
//        sectionHeaderLabel.textColor = UIColor.black

        sectionHeaderLabel.font = UIFont(name: fontName, size: 12)
        sectionHeaderLabel.addCharacterSpacing(withValue: 1.0)
        sectionHeaderLabel.alpha = finalHeaderAlpha
        
        //
        self.scrollView!.addSubview(sectionHeaderLabel)
        self.lookingForHeaderLabel = sectionHeaderLabel
    }
    
    func addLookingForLabel() {
        let lkWidth = UIScreen.main.bounds.width - (self.textInsets?.marginInset)!
        let lkHeight = 23 as CGFloat
        let lk_X = (self.textInsets?.marginInset)!
        let lk_Y = self.lookingForHeaderLabel!.frame.maxY + (self.textInsets?.bodyInset)! + 6
        
        let lookingForLabelFrame = CGRect(x: lk_X, y: lk_Y, width: lkWidth, height: lkHeight)
        let lookingForLabel = UILabel(frame: lookingForLabelFrame)
        
        //
        lookingForLabel.text = self.lookingFor
        lookingForLabel.font = UIFont(name: "Avenir-Light", size: 14)
        lookingForLabel.textColor = UIColor.black
        
        //
        self.scrollView!.addSubview(lookingForLabel)
        self.lookingForLabel = lookingForLabel
        
        //line separator properties
        let lineWidth = UIScreen.main.bounds.width
        let lineHeight = 0.5 as CGFloat
        let line_X = 0 as CGFloat
        let line_Y = self.lookingForLabel!.frame.maxY + (self.textInsets?.footerInset)!
        
        //
        let lineFrame = CGRect(x: line_X, y: line_Y, width: lineWidth, height: lineHeight)
        let lineSeparatorView = UIView(frame: lineFrame)
        
        lineSeparatorView.backgroundColor = lineColor
        
        //
        self.scrollView!.addSubview(lineSeparatorView)
        self.lineSeparatorView_b = lineSeparatorView
    }
    
    func addHeightHeader() {
        //header frame properties
        let headerWidth = 100 as CGFloat
        let headerHeight = 23 as CGFloat
        let header_X = (self.textInsets?.marginInset)!
        let header_Y = self.lineSeparatorView_b!.frame.maxY + self.lineSeparatorView_a!.bounds.height + (self.textInsets?.headerInset)!
        
        //init label
        let sectionHeaderFrame = CGRect(x: header_X, y: header_Y, width: headerWidth, height: headerHeight)
        let sectionHeaderLabel = UILabel(frame: sectionHeaderFrame)
        
        let fontName = "AvenirNext-DemiBold"
        //        let fontName = "AvenirNext-UltraLight"
//        let fontName = "Avenir-Light"
        sectionHeaderLabel.text = "HEIGHT"
        sectionHeaderLabel.font = UIFont(name: fontName, size: 17)
        sectionHeaderLabel.textColor = UIColor.black

        sectionHeaderLabel.font = UIFont(name: fontName, size: 12)
        sectionHeaderLabel.addCharacterSpacing(withValue: 1.0)
        sectionHeaderLabel.alpha = finalHeaderAlpha

        //
        self.scrollView!.addSubview(sectionHeaderLabel)
        self.heightHeader = sectionHeaderLabel
    }
    
    func addHeightLabel() {
        let lkWidth = 250 as CGFloat
        let lkHeight = 23 as CGFloat
        let lk_X = (self.textInsets?.marginInset)!
        let lk_Y = self.heightHeader!.frame.maxY + (self.textInsets?.bodyInset)! + 6
        
        let lookingForLabelFrame = CGRect(x: lk_X, y: lk_Y, width: lkWidth, height: lkHeight)
        let heightLabel = UILabel(frame: lookingForLabelFrame)
        
        //
        heightLabel.text = self.userHeight
        heightLabel.font = UIFont(name: "Avenir-Light", size: 14)
        heightLabel.textColor = UIColor.black
        //
        self.scrollView!.addSubview(heightLabel)
        self.heightLabel = heightLabel
        
        //line separator properties
        let lineWidth = UIScreen.main.bounds.width
        let lineHeight = 0.5 as CGFloat
        let line_X = 0 as CGFloat
        let line_Y = self.heightLabel!.frame.maxY + (self.textInsets?.footerInset)!
        
        //
        let lineFrame = CGRect(x: line_X, y: line_Y, width: lineWidth, height: lineHeight)
        let lineSeparatorView = UIView(frame: lineFrame)
        
        lineSeparatorView.backgroundColor = lineColor
        
        //
        self.scrollView!.addSubview(lineSeparatorView)
        self.lineSeparatorView_c = lineSeparatorView
        
        self.adjustScrollViewHeight()
    }
    
    var moreButton: UIImageView?
    var moreButtonArea: UIView?
    
    func addMoreActions() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        let sWidth = UIScreen.main.bounds.width
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = (137 / 414) * sWidth as CGFloat
        let bH = 25 as CGFloat
        let bX = (sWidth - bW) * 0.5
        let bY = self.lineSeparatorView_c!.frame.maxY + (gridUnitHeight)
        
        //
        let moreButton = UIImageView(frame: CGRect(x: bX, y: bY, width: bW, height: bH))
        moreButton.contentMode = .scaleAspectFit
        moreButton.image = UIImage(named: "moreActionsButton")!
    
        //
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.moreActions(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        moreButton.addGestureRecognizer(tapGesture)
        moreButton.isUserInteractionEnabled = true
        
        //add selectorBroad
        let heightFromMoreActions = 2 * gridUnitHeight + bH
        let moreActionsWideArea = UIView(frame: CGRect(x: 0, y: self.lineSeparatorView_c!.frame.maxY, width: sWidth, height: heightFromMoreActions))
        moreActionsWideArea.backgroundColor = UIColor.white
        
        //
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.moreActions(_:)))
        tapGesture2.delegate = self
        tapGesture2.numberOfTapsRequired = 1
        tapGesture2.numberOfTouchesRequired = 1
        moreActionsWideArea.addGestureRecognizer(tapGesture2)

        //
        self.moreButton = moreButton
        self.moreButtonArea = moreActionsWideArea
        
        self.scrollView!.addSubview(moreActionsWideArea)
        self.scrollView!.addSubview(moreButton)
    }
    
    @objc func moreActions(_ sender: UIButton) {
        //print("moreActions detected")
        self.present(self.reportAlert, animated: true, completion: nil)
    }
    
    func adjustScrollViewHeight() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let sWidth = UIScreen.main.bounds.width
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        
        var footerInset = (2 * textInsets!.footerInset!)
        
        if UIScreen.main.bounds.height > 750 {
           footerInset = (5 * textInsets!.footerInset!)
        }
        
        let heightFromMoreActions = 2 * gridUnitHeight
        
        self.scrollView?.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.heightLabel!.frame.maxY + heightFromMoreActions + footerInset)
    }
    
    var reportAlert: UIAlertController!

    func addActionSheet() {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //
        let reportAction = UIAlertAction(title: "Report Profile", style: .default) { (action) in
            self.reportUser(forReason: "Profile")
        }
        alert.addAction(reportAction)
        
        //
//        let reportAction2 = UIAlertAction(title: "Report Photo", style: .default) { (action) in
//            self.reportUser(forReason: "Photo")
//        }
//        alert.addAction(reportAction2)
        
        //
        let blockAction = UIAlertAction(title: "Block", style: .default) { (action) in
            self.blockUser()
        }
        
        alert.addAction(blockAction)
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            self.reportAlert.dismiss(animated: true, completion: nil)
        }
        
        alert.view.tintColor = UIColor.black
        alert.addAction(cancelAction)
        self.reportAlert = alert
    }
    
    func reportUser(forReason: String) {
        //print("reportUser press")
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        let reportIncidentID = self.ref.child("reportedUsers").childByAutoId().key
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        if forReason == "Speech" {
            //print("reportReasonSpeech")
            var reportData = [String : String]()
            reportData["issuerID"] = selfUID
            reportData["targetID"] = otherUID
            reportData["forReason"] = "Speech"
            reportData["timeStamp"] = timeStamp
            
            let childUpdates = [
                "/reportedUsers/\(otherUID)/\(reportIncidentID)": reportData,
                "/users/\(selfUID)/myFiledReports/\(reportIncidentID)" : reportData,
                "/users/\(otherUID)/incidentReports/\(reportIncidentID)" : reportData,
                ]
            
            //create array of users who have requested swaps
            ref.updateChildValues(childUpdates)
        }
        
        if forReason == "Photo" {
            //print("reportReasonPhoto")
            var reportData = [String : String]()
            reportData["issuerID"] = selfUID
            reportData["targetID"] = otherUID
            reportData["forReason"] = "Photo"
            reportData["timeStamp"] = timeStamp
            
            let childUpdates = [
                "/reportedUsers/\(otherUID)/\(reportIncidentID)": reportData,
                "/users/\(selfUID)/myFiledReports/\(reportIncidentID)" : reportData,
                "/users/\(otherUID)/incidentReports/\(reportIncidentID)" : reportData,
                ]
            
            //create array of users who have requested swaps
            ref.updateChildValues(childUpdates)
        }
        
        if forReason == "Profile" {
            //print("reportReasonProfile")
            var reportData = [String : String]()
            reportData["issuerID"] = selfUID
            reportData["targetID"] = otherUID
            reportData["forReason"] = "Profile"
            reportData["timeStamp"] = timeStamp
            
            let childUpdates = [
                "/reportedUsers/\(otherUID)/\(reportIncidentID)": reportData,
                "/users/\(selfUID)/myFiledReports/\(reportIncidentID)" : reportData,
                "/users/\(otherUID)/incidentReports/\(reportIncidentID)" : reportData,
                ]
            
            //create array of users who have requested swaps
            ref.updateChildValues(childUpdates)
        }
    }
    
    func blockUser() {
        //print("blockUser press")
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
//        self.ref.child("users/\(selfUID)/blockedUsers/\(otherUID)").setValue("true")
//        self.ref.child("users/\(otherUID)/blockedBy/\(selfUID)").setValue("true")
        
        let metaUpdates = [
            "users/\(selfUID)/blockedUsers/\(otherUID)" : "true",
            "users/\(otherUID)/blockedBy/\(selfUID)" : "true",
            ]
        
        self.ref.updateChildValues(metaUpdates)
        
        //need to remove chat id
        if self.absExistingMessageThreadID != "" {
            let chatThreadID = self.absExistingMessageThreadID
//            self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
//            self.ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
            let blockChatUpdates = [
                "users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/chatIsActive" : "nil",
                "users/\(otherUID)/myActiveChatThreads/\(chatThreadID)/chatIsActive" : "nil",
            ]
            
            self.ref.updateChildValues(blockChatUpdates)
            
        }
        
        if let swapID = self.absExistingSwapRequestID {
            
//            self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//            self.ref.child("globalSwapRequests/\(otherUID)/\(swapID)").removeValue()
            
            let blockSwapUpdates = [
                "globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive" : "nil",
                "globalSwapRequests/\(otherUID)/\(swapID)/swapIsActive" : "nil",
            ]
            
            self.ref.updateChildValues(blockSwapUpdates)

        }
        
        //        UserDefaults.standard.set("true", forKey: "selfDidBlockOrDismissUser")
        
        self.didBlockUser = true
        self.goBackFromBlock()
    }
    
    var shouldUnwindToChatList_whenBlock = false
    
    func goBackFromBlock() {
        
        //when presenting identifier is grid
        if self.shouldUnwindToChatList_whenBlock == false {
            self.performSegue(withIdentifier: "unwindToGrid", sender: self)
        } else {
            //else should unwind back to chatlist
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
     
    }
    
    func rotateBackArrow_Animate() {
        //print("rotateBackArrow_Animation")

        UIView.animate(withDuration: 0.08) {
            self.backButtonView!.transform = self.backButtonView!.transform.rotated(by: -CGFloat(M_PI_2))
        }
        
        let when = DispatchTime.now() + 0.04
      
        
        if upvcShouldUnwindToChatScreen == true {
            DispatchQueue.main.asyncAfter(deadline: when) {
                //RESET PROPERTY
                self.shouldDrawFullChatButton = false
                self.performSegue(withIdentifier: "unwindProfileToChat", sender: self)
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.performSegue(withIdentifier: "unwindToGrid", sender: self)
            }
        }
        
    }
//    
    
    func rotateBackArrow_AnimateUp() {
        //start out with button pointing upward
        self.backButtonView!.transform = self.backButtonView!.transform.rotated(by: -CGFloat(M_PI_2))
        
        //while screen finishes presenting, rotate arrow leftward
        let when = DispatchTime.now() + 0.06
        DispatchQueue.main.asyncAfter(deadline: when) {
            UIView.animate(withDuration: 0.27) {
                self.backButtonView!.transform = self.backButtonView!.transform.rotated(by: CGFloat(M_PI_2))
            }
        }
//        0.08 & 0.30. good but sliw
        //btetter 0.06 & 0.23
        //better 0.06 & 0.25
    }
    
    func sizeOfString(string: String, constrainedToWidth width: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 16)! ],
            context: nil).size
    }
    
    //MARK: ViewDidLoad

    var shouldDrawFullChatButton = false

//    var deleteUserID = [ "28SgzwgsolYzT0Xcz5X0yEwyqxE2", "2OMkVQBlZgdA4FAYFj6WGbpnmuF3",
//    "2klIDCjiMfN3d9LAYTcGTIHui1K2",
//    "3mjOC7a9GBSNeBBlXvuxIiiRsuy2",
//    "3u4EZmBF4IXCbgkaUlxRkQfbhS72",
//    "3uenfqlCmUXJO6niwm7S7Rz2nu12",
//    "404pw5QpGMPcJ7GqCFSVcDcZzgg1",
//    "43QSnMzruVOBIvWjI5GtNrsOHDy2",
//    "466UcCJLlaa9o9mlqAM0A331nD42",
//    "4qrSWLN9XaQxccI2cpF9ctchH1A2",
//    "4vp7y9BsJ7ZCankBXX6X62AEr0A3",
//    "4wR47rTMWWPC9QRrhaDofYmxF6A3",
//    "56d4FzxFFvMmgEDRWpoAWZ9XvwC2",
//    "6J8jqilyTnd6E3i1CtA36jPlLRF2",
//    "6MzCvBAEV1WSSdfDjJrvud33Wku1",
//    "6dUytwuCdAggIHIyTDfYkdla0R33",
//    "6sdmliwGTKfkMEVJAs5Vjlh5fT23",
//    "7Qp0mq2CITOogH9cUYNU9YaR2mk2",
//    "8af27nuDf7VMQONW0g3si54zNNq2",
//    "AmZlqMwrKTW9qbg6EZ9CKSpID8P2",
//    "B3VwfAd4s0dFsA0YtrBjdZyvIOo1",
//    "BUIPkFEztUX3qkZuGMFlpFoqgY63",
//    "CGxKozb6F2bmk1S7MqFMiCmuUfl2",
//    "ChWM9yocUnWKGIO6hwtN8Aff6nQ2",
//    "CvjKoZHu4edUxQbLZy8LBpZx7s83",
//    "CxzQ1XHC5IYZRw2WjrHCs5AaVv12"
//    ]
    
    //    "1MCjastwi9O9IYorZuCwMMkMWbZ2", "DeaBlRk9hSZlBdE6lHI9Jgfvusc2", "Ds9KkR8TW3dsGwI07cGo2pRluYU2",
    //    "EWOsy1YzE8QUYNgglykW5CnuAkv2", "EkGdaLEGT9M2AaXIwaszeVVpCll2",
    //    "EoZsvylfkgeb2epk83CmRvCYGRb2",
    //    "Eq1apCJppfMPnjOXHmIH7cx0usD3",
    //    "Fa11Ivs29Iawm7dyonvXYp0oHFu2",
    //    "HMoWePebhqcjBu03ehxDjqtHgdQ2",
    //    "HSl66r8QhgWb6BCgD4b9OJCEvTq2",
    //    "HdIdRb4SWxUblj2xcm4XgNS5goH2", "Ie6veYQc3UNqrgOrfLxh0rRBs9f2", "J4J8qBkqA8QOrrikx3hu7Y1Fbpp1", "JmUrPtxNYDbDDEPpx3kHLneM76k2", "Jxo4io1hz7YDxhekheLpwsHi4bG2", "Jzo70Dv9Ebb9BsB0RYdu8TuFFxn1", "KKMuqaZ27GYLaqUYEE7ffWcjnFq1", "KmT27zIeTvQ83n8HnSzYjEoZMnk2",
    //    "KxExIbVpIGS4pNv4q45lljHoNsJ2",
    //"LdS3HuMP9afkStACVTD6mpAtNPx1", "MCb99B3h5eWS6WFay7QvbXNclqA3", "MW4i3FhkytOZTvqk9ZitKLmtgFz1", "N7MFKeleUKXt8pelwBmMHTDBhy92", "OMOn92FwuhUr1Py7htkKu3xEmNH3", "On8uzlcy1WTvACW2UgyPppyAMC23", "OtYW34AK7VMzNtljhkt2CyeIHEe2", "P6f9G7FZZVYxKtTaXGWqFPjEfJf2", "PZeJTt2gudXrCJgLInoFYdpz0kj2", "PkGF8hWZUfc0xq4eYUQaTJkrjM73", "PlLp6kk6TRX26RgsOeeDmvDQghe2", "RTXlWaxmkjRs9zHc1zu6xtxmRDO2", "ReNXDilnatOxxXGMgkvefVcWFhM2", "SGFfU7QZjfR1avq1xQHyuvOfqEl1", "T53rzkNJzIfJY8cggxUnT18tVQf2", "Tp2U0Qn8g7X7bocIQLk28Xl4acp2"
    
//    var newDel = [
//        "VmZAAJJGIxhsrbamRXb4sh68UcR2", "WXus1DIA4zOxNFCqLpUc8ye1Gwh2", "WdfW0fxagoTaG4XpGBd2bZ8hyBt2", "XB4fHu9AWrZ146daHGA9RyemVVs1", "XPNqAzVMUFXFJAlutRj1SmR9ft33", "XvwCywSQvGYNyIjeTiSw1pyFLYJ2", "YCpS4uGU56fAJ1rVS2kGvekxzhF3", "YfJNL4itIYeRuKPc3g5xkSYVzB82", "ZuZshWGMz1f2y9S1ov4xYag7sEi2", "anoWDVBnMrMXHbFvi1H23ak75W72", "b8bo0f7Cl5gUlBSfb1Gq6XmAh1H3", "bOsKClnEh4MBAjiOfC93K7QCp783", "cKV7y70MjSUfLON5NPblK8Eu9Rg1", "cQ5BS2cXo8ZLCvK7eP5BUYadsxt2", "cl24QAN9sFZH2aaewggMUZgU7E62", "d1Kn0qg4TFey26bHigONuledofl1", "d7xturkXstgRsXOgJQM96So8Woq2", "dHqlsPKVOrWURIXJrMkET5Ie24y2", "fEUw1Dgpl3SHE0JooI5MNC6B2Qt2", "fYGEvkxY1IgkL7ncnTVur3uxNqi2", "gte7N2Ary1eBFwPuqkLPNLBetNu1", "h36hajMtyfQiattO14eSdyOXHpU2", "hXzny2bEpMePXXqpjTISXUEyjHK2", "hgiwuyet81WLWXubl16t5GP44HQ2", "ibyItITDnLWy2emnv59UzQ5awj82", "ifsoCH10bTY7IouUWNBK1RIoXDk1", "ikig20nm8nScofQhranV0Rxltdz2", "ixJczyCvPMcz9c59ov4EL7IRROM2", "jZJX8nUCiSOh3UVPVLsvqDMLeTA2", "kE2QDCJlMqfBBvcmqc4vRGU6IRg2", "kdlaJLuN9DOvbKdhyVmLqXf8S7R2", "kg15uRJSpqO58LGF8k6u5GtmfUS2", "kjosfOXAgBdPRot3YSniA60YLSg1", "l1Sm1XyN9xZK5VfoPR5lzxjigZ22", "m4Y7zAyYoBfxoVA8otqx6uK35932", "mhsy1Q9HiQfcO4Aj6T12NMnqTNa2", "mmbftCadVLVvnKt1We4oOXpfOg12", "nWYViEWiDyNywS6jK9wCU4W0sXW2", "nzNUIIeQPzMkx0Jx137SLzz4sQv1", "oRTrHUDoyhPbroVF9W8xqKrIkgy2", "ob0WMyf295XiVLCqFRytiOaoH4T2", "ovbXbEjk09bjFijjOUjp0ayPt4s2", "pEze5ItAn0NZzMcexd89bNJNxr32", "pWf9VfxnjYS1czNemxNPH4So0TK2", "pecZRYos4sW337VlQNpeGnBtgON2", "qR3MKKAwbUcILY6qBgjswfiW0yO2", "qgc04RQgoiUNFyLoLQ0cRQkEGdI3", "qwpjorxmCEMfadhJNYKDwGR94Qm2", "rAOw3RD0p2dItVvzZi2NIvP8Box1", "rLdyAwxiOYVxaka0Npv4VP4ul5O2", "rblBF6qEDTe0UCg2aXkMKfoLYwX2", "rsqjDEWaLVWaa2HeetdUjUFKLnM2", "sVBn83tKjETOd8wh6krghpfxt6z2", "tUBeRm5xxNZKDvKhhiCVnN2AeX13", "td1f9va2jLflcd35ciSLqC9MnnP2", "tfkadboLliblMEGfXzkzIDmLsV32", "tykoa5dMSOSPNy5w1JT6yKDrM633", "u9LPYv1jdAMIFKsLKbZF5qyY38v2", "ukxxovYKcJfwXqi1PStt7vwTG132", "ulJdBeTaWXepanpeanmcfe35iM22", "vu4GQI00VMS4Bkgb6NBd1A3ybiJ2", "wDWEdDUhkMV0E0UPaCe2rIfb5L52", "xaOPUbqqGMcuw84WoP06KLrce6X2", "xeTdGFmeAkUEChtmPs6nXr88XYF3", "xweMM0VpzxOuH2SQ2eqlaclnpxh2", "yCdoQuvGScfm9J6YxklrhLEqIAF2", "zKUT8I1bYpeMx50AGCGU1VNfig12", "zb9ujXM0ujcz4eCCBjJkbBySe7E2", "zqOOPVjbTuWEDhlLCxrS1FGVSgS2"
//    ]

//    var disable = [
//    "XsJ8a2n5IaVy52lWRCd5iKmibrD3", "ZaHzvbPuYEXdJ0dFf4v6pixqJOG3"
//    ]
//
//    func removeUsers() {
//        for user in self.disable {
//            self.cleanDB(withUID: user)
//        }
//    }
//
//    func cleanDB(withUID: String) {
//        //print("populateArrayofDictionaries willcleanDB withUID,", withUID)
//        //we don't need to delete the instance from
//
//        self.ref.child("userLocationGF/\(withUID)").removeValue()
//        self.ref.child("users/\(withUID)").removeValue()
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        //init ui
        //        self.chatRequestUIControlFlow()
        
        
//        print("selectUID", self.otherUID!)
        
        //init database
        self.ref = Database.database().reference()
//        self.removeUsers()

        //init observers
        //        if self.messageThreadIDDoesExist != true { self.observeActiveMessageThreads() }
        if self.chatRequestDoesExist != true { self.observeSelectedUserIncomingChatRequest() }
        self.observeSelectedUserChatRequestChanges()
        
        self.chatRequestUIControlFlow(fromLiveObserver: false)
        
        //confirm values
        //print("chatRequestDoesExist,", self.chatRequestDoesExist )
        //print("chatRequestStatus,", self.chatRequestStatus )
        
        self.setUserInfo()
        
        if #available(iOS 11.0, *) {
            self.scrollView!.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.view.insetsLayoutMarginsFromSafeArea = false
        } else {
            // Fallback on earlier versions
        }
        
        self.setProfileSectionTitleColor()
        self.observeSelectedUserPresence()
        self.addImageLoader()
        
        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.unwindAfterSelfDidBlock), name: NSNotification.Name(rawValue: "selfDidBlock_SourceIsProfileVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.animateBackButtonFade), name: NSNotification.Name(rawValue: "willUnwindFromCSVC"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.clearUIOnDismissal), name: NSNotification.Name(rawValue: "clearProfileOnDismissal"), object: nil)
        
        self.view.isOpaque = false
        self.view.backgroundColor = UIColor.white
        
        if self.upvcShouldUnwindToChatScreen == true {
            //presenting controller is chatScreen
            if self.shouldDrawFullChatButton == true {
                self.drawChatOpen_N()
            } else {
                self.drawContinueChat_Open()
            }
        }

    }
    
    @objc func clearUIOnDismissal(_ notification: Notification) {
        
        if notification.name.rawValue == "clearProfileOnDismissal" {
            
            self.view.isOpaque = false
            self.view.backgroundColor = UIColor.clear
            
            self.imageScrollView!.alpha = 0
            self.imageView2?.alpha = 0
            self.imageView3?.alpha = 0
            self.imageView4?.alpha = 0
            self.bannerMask?.alpha = 0
            self.chatButton_Open?.alpha = 0
            self.backButtonView?.alpha = 0
        }
    }
    
    @objc func unwindAfterSelfDidBlock(_ notification: Notification) {
        //print("WELD-1 UPVC WILL EXECUTE UNWIND AFTER BLOCK")
        if notification.name.rawValue == "selfDidBlock_SourceIsProfileVC" {
            self.dismiss(animated: false, completion: nil)
        }
    }

    var didUnwindFromChatScreen = false
    
    //MARK: ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {

        //print("WELD_B UPVC viewWillAppear called")
        
        super.viewWillAppear(animated)
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict

        if self.unlockedUsers.contains(selectedUser["UID"]!) {
            if self.otherVisus! == "false" {
                let clearImage = self.photoVault(otherUID: selectedUser["UID"]!)
                self.profileImageView!.image = clearImage
            } else {
                self.profileImageView!.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
            }
        } else {
            self.profileImageView!.sd_setImage(with: URL(string: selectedUser["userThumbnailURLString"]!))
        }
        
        self.observeUnlockedUsers()

        
        if self.selectedUser["isOnline"]! == "true" {
            self.setUserStatusIndicator(withOnlineStatus: true)
        } else {
            self.setUserStatusIndicator(withOnlineStatus: false)
        }
        
        //print("UPVC ViewWillAppear myProfileInfoDict,", self.myProfileInfoDict)
        //        UIApplication.shared.statusBarFrame.size.height
        UIApplication.shared.statusBarStyle = .lightContent
        
        if self.selectedUser["UID"]! == self.selfUID! {
            //print("USER SELECTED IS SELF")
            self.chatButton_Open?.isHidden = true
            self.sliderButtonImageView?.isHidden = true
            self.moreButton?.removeFromSuperview()
            self.moreButtonArea?.removeFromSuperview()
            self.lineSeparatorView_c?.removeFromSuperview()
        }
        
        if self.didBlockUser == true {
            //profile needs to unwind simultaneously when CSVC unwinds
            //to coordinate set notificaiton upon dismissal
            self.dismiss(animated: false, completion: nil)
        }
        
        self.observeDeletedMessageThreads()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(UserProfileViewController.animateBackButtonFade), name: NSNotification.Name(rawValue: "willUnwindFromCSVC"), object: nil)
        
        self.addOffsetObserver()
    }
    
    @objc func animateBackButtonFade(_ notification: Notification) {
        //print("CLVC observedNotification animateBackButtonFade")
        
        guard notification.name.rawValue == "willUnwindFromCSVC" else { return }
        if self.didUnwindFromChatScreen == true {
            //print("UPVC WILL ANIMATE BACK BUTTON")
            self.backButtonView?.alpha = 0
            UIView.animate(withDuration: 0.20, delay: 0.1, options: .curveLinear, animations: {
                //print("UIViewWillAnimateAlphaTransparency to 0.8")
                self.backButtonView?.alpha = 1
            }, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.observeSelectedUserIncomingSwapRequest()
        self.observeSelectedUserIncomingSwapRequestChanges()
        if self.messageThreadIDDoesExist != true {
            self.observeActiveMessageThreads()
        }
    }
    
    //MARK: UI CONFIGURATION METHODS - CHAT REQUESTS
    
    func drawChatOpen_N() {
        //parameters
        let insetRatio = 19 / 414 as CGFloat
        let inset = insetRatio * UIScreen.main.bounds.width
        
        let cWidth = 63 as CGFloat
        let cHeight = cWidth
        let c_X = self.bannerImageView!.bounds.width - inset - cWidth
        let c_Y = (self.bannerImageView!.bounds.height - cHeight) * 0.5
        
        //init chat button frame
        let chatButtonFrame = CGRect(x: c_X, y: c_Y, width: cWidth, height: cHeight)
        let chatButtonView = UIImageView(frame: chatButtonFrame)
    
        chatButtonView.image = UIImage(named: "chatButtonOpen_N")
        self.chatButton_Open = chatButtonView
        self.bannerImageView!.addSubview(chatButtonView)
        self.chatButton_Open!.alpha = 1
        
        //
        //add slider key tapRecognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.goToChat(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        chatButton_Open!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        chatButton_Open!.isUserInteractionEnabled = true
    }
    
    func drawContinueChat_Open() {
        //parameters
        let insetRatio = 19 / 414 as CGFloat
        let inset = insetRatio * UIScreen.main.bounds.width
        
        let cWidth = 60 as CGFloat
        let cHeight = cWidth
        let c_X = self.bannerImageView!.bounds.width - inset - cWidth
        let c_Y = (self.bannerImageView!.bounds.height - cHeight) * 0.5
        
        //init chat button frame
        let chatButtonFrame = CGRect(x: c_X, y: c_Y, width: cWidth, height: cHeight)
        let chatButtonView = UIImageView(frame: chatButtonFrame)
        chatButtonView.isOpaque = true
        
        chatButtonView.image = UIImage(named: "chatButtonOpen_D")
        self.chatButton_Open = chatButtonView
        self.bannerImageView!.addSubview(chatButtonView)
        
        //add slider key tapRecognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.goToChat(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        chatButton_Open!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        chatButton_Open!.isUserInteractionEnabled = true
    }
    
    func chatRequest_N() {
//        self.addSliderBoxView(withSliderState: "chatRequestSliderEmpty")
        self.addSliderBoxView(withSliderState: "sliderOutlineOrange")
        self.addSliderKey(withKeyState: "sliderKeyOrangeFill")
    }
    
    func chatRequestSelf_P() {
        self.addSliderBoxView(withSliderState: "sliderButtonBoxOrangeFill")
        self.addSliderKeyfromRequestSelf_P()
    }
    
    func chatRequestOther_P() {
        self.addSliderBoxView(withSliderState: "sliderButtonBoxOrangeFill")
        self.addSliderKeyfromRequestOther_P()
    }
    
    func chatRequestOther_D() {
        self.addSliderBoxView(withSliderState: "sliderButtonBoxBlueFill")
        self.addSliderKeyfromRequestComplete()
    }
    
    func chatRequestSelf_D() {
        self.addSliderBoxView(withSliderState: "sliderButtonBoxBlueFill")
        self.addSliderKeyfromRequestComplete()
    }
    
    //NOTE: NEED TO FIX DUPLICATE SLIDER BOX
    func addSliderBoxView(withSliderState: String) {
        let bannerImageView = self.bannerImageView!

        //parameters
        let insetRatio = 19 / 414 as CGFloat
        let inset = insetRatio * UIScreen.main.bounds.width
        
        //slider image view
        let wFrame = 102 as CGFloat
        let hFrame = 52 as CGFloat
        let xFrame = bannerImageView.bounds.width - inset - wFrame
        let yFrame = (bannerImageView.bounds.height - hFrame) * 0.5
        
//        if let sliderButtonBox = self.sliderButtonImageView {
//            sliderButtonBox.image = UIImage(named: withSliderState)
//        } else {
            self.sliderButtonImageView?.removeFromSuperview()
            let sliderButtonBox = CGRect(x: xFrame, y: yFrame, width: wFrame, height: hFrame)
            let sliderImageHolder = UIImageView(frame: sliderButtonBox)
            self.sliderButtonImageView = sliderImageHolder
            sliderButtonImageView!.isOpaque = true
            
            sliderButtonImageView!.image = UIImage(named: withSliderState)
            self.bannerImageView!.addSubview(sliderImageHolder) ////
            sliderButtonImageView!.contentMode = .center
//        }
    }
    
    func addSliderKey(withKeyState: String) {
        let bannerView = self.bannerImageView!
        let sliderButtonImageView = self.sliderButtonImageView!

        //init |K
        let sWidth = 102 as CGFloat
        let sHeight = 52 as CGFloat
        let sI = 7 as CGFloat

        let kInset = 8 as CGFloat
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat

        //...add slider key frame
        let kX = kInset
        let kY = kInset

        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        let kImageView = UIImageView(frame: kFrame)
        kImageView.image = UIImage(named: withKeyState)

        //
        self.sliderButtonImageView?.addSubview(kImageView)
        self.keyImageView = kImageView

        //add slider key tapRecognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.slideToRequestChat(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        keyImageView!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        sliderButtonImageView.isUserInteractionEnabled = true
        keyImageView!.isUserInteractionEnabled = true

        //***** IMPORTANT NOTICE:
        /*
         1. Observed Error: kImageView was not receiving touches
         2. Error Solution: setting the .isUserInteractionEnabled property of the parent view of the kImageView resolved the issue
         */
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh

        //...
        let wWidth = mRi + mRo
        let wHeight = mRh

        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wX_i = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + kInset + wYError

        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSOrangeFill")

        //define the starting position for the right wiggler
        let wXRHS_i = kInset + kWidth + mRo + cornerRadiusInset

        let wFrameRHS_i = CGRect(x: wXRHS_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_i = UIImageView(frame: wFrameRHS_i)
        wImageViewRHS_i.image = UIImage(named: "sliderWiggler_f")

        sliderButtonImageView.addSubview(wImageViewLHS_i)
        self.wImageViewLHS_i = wImageViewLHS_i

        sliderButtonImageView.addSubview(wImageViewRHS_i)
        self.wImageViewRHS_i = wImageViewRHS_i

        wImageViewRHS_i.alpha = 0 as CGFloat
    }
    
    func addSliderKeyfromRequestSelf_P() {
        //required: key frame right + wiggler right final and no animation
        
        //init |K
        let kI = 8 as CGFloat
        
        //1. KEY FRAME PROPERTIES
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        var kX = kI + kWidth + kI
        var kY = kI
   
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        let kImageView = UIImageView(frame: kFrame)
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
    
        //
        self.sliderButtonImageView!.addSubview(kImageView)
        self.keyImageView = kImageView
        
        //add slider key tapRecognizer
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.pendingOutgoing(_:)))
//        tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.slideToAcceptIncoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        keyImageView!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        sliderButtonImageView!.isUserInteractionEnabled = true
        keyImageView!.isUserInteractionEnabled = true
        
        //ADD WIGGLER
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        
        //2. Add RHS wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        let wYError = 0.3604 as CGFloat
        let wXRHS_f = kI + kWidth + 1
        let wY = kHeight - mRh + kI + wYError
        
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_f = UIImageView(frame: wImageViewFrameRHS_f)
        wImageViewRHS_f.image = UIImage(named: "sliderWiggler_f")
        
        self.sliderButtonImageView!.addSubview(wImageViewRHS_f)
        self.wImageViewRHS_i = wImageViewRHS_f
    }
    
    func addSliderKeyfromRequestOther_P() {
        //need to add key on left & LHS wiggler on left & wigglerRHS on right with 0 visibility and set primary button box fill to orange and keys & wigglers to white. Basically, mimic initial slider with complimentary colors
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let kInset = 8 as CGFloat

        //...add slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        let kX = kInset
        let kY = kInset
        
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        let kImageView = UIImageView(frame: kFrame)
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
        //
        self.sliderButtonImageView?.addSubview(kImageView)
        self.keyImageView = kImageView
        
        //add slider key tapRecognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.slideToAcceptIncoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        keyImageView!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        sliderButtonImageView.isUserInteractionEnabled = true
        keyImageView!.isUserInteractionEnabled = true
        
        //add slider key wiggler
        //...wiggler |K
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wX_i = kInset + kWidth - mRi + wXError
        let wY = kHeight - mRh + kInset + wYError
        
        let wFrame_i = CGRect(x: wX_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewLHS_i = UIImageView(frame: wFrame_i)
        wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        
        //define the starting position for the right wiggler
        let wXRHS_i = kInset + kWidth + mRo + cornerRadiusInset
        
        let wFrameRHS_i = CGRect(x: wXRHS_i, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_i = UIImageView(frame: wFrameRHS_i)
        wImageViewRHS_i.image = UIImage(named: "sliderWiggler_f")
        
        sliderButtonImageView.addSubview(wImageViewLHS_i)
        self.wImageViewLHS_i = wImageViewLHS_i
        
        sliderButtonImageView.addSubview(wImageViewRHS_i)
        self.wImageViewRHS_i = wImageViewRHS_i
        
        wImageViewRHS_i.alpha = 0 as CGFloat
    }
    
    func addSliderKeyfromRequestComplete() {
        //required: key frame right + wiggler right final and no animation
        
        //init |K
        let kI = 8 as CGFloat
        
        //1. KEY FRAME PROPERTIES
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        var kX = kI + kWidth + kI
        var kY = kI
        
        let kFrame = CGRect(x: kX, y: kY, width: kWidth, height: kHeight)
        let kImageView = UIImageView(frame: kFrame)
        kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        
        //
        self.sliderButtonImageView!.addSubview(kImageView)
        self.keyImageView = kImageView
        
        //add slider key tapRecognizer
        var tapGesture = UITapGestureRecognizer()
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.goToChat(_:)))
        //        tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserProfileViewController.slideToAcceptIncoming(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        keyImageView!.addGestureRecognizer(tapGesture)
        bannerImageView!.isUserInteractionEnabled = true
        sliderButtonImageView!.isUserInteractionEnabled = true
        keyImageView!.isUserInteractionEnabled = true
        
        //ADD WIGGLER
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //2. Add RHS wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        let wXRHS_f = kI + kWidth + 1
        let wY = kHeight - mRh + kI + wYError
        
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        let wImageViewRHS_f = UIImageView(frame: wImageViewFrameRHS_f)
        wImageViewRHS_f.image = UIImage(named: "sliderWiggler_f")
        
        self.sliderButtonImageView!.addSubview(wImageViewRHS_f)
        self.wImageViewRHS_i = wImageViewRHS_f
    }
    
    @objc func slideToRequestChat(_ sender: UITapGestureRecognizer) {
        //print("swap slider tap observed")
        self.animateSlideKey()
        self.createNewChatRequest()
        
        self.keyImageView!.isUserInteractionEnabled = false
    }
    
    @objc func pendingOutgoing(_ sender: UITapGestureRecognizer) {
//        self.animateSlideKey()

    }
    
    @objc func slideToAcceptIncoming(_ sender: UITapGestureRecognizer) {
        self.acceptChatRequest()
        self.animateAcceptChat()
    }
    
    
    
    //Note: chat open
    @objc func goToChat(_ sender: UITapGestureRecognizer) {
        //print("GOING TO CHAT NEED CODE TO GO TO CHAT")
        
        
        if self.upvcShouldUnwindToChatScreen == true {
            self.performSegue(withIdentifier: "unwindProfileToChat", sender: self)
        } else {
            self.chatRequestActionControlFlow()
        }
    }
    
    
    func addingUIBezierPathCircleLayerSample() {
        //set frame properties
        //        chatButtonView.layer.cornerRadius = cWidth / 2
        //        chatButtonView.layer.masksToBounds = true
        //        chatButtonView.layer.borderWidth = 0.5
        //        chatButtonView.layer.borderColor = UIColor.orange.cgColor
        
        //        let circlePath = UIBezierPath(arcCenter: CGPoint(x: 30, y: 30) , radius: 30, startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        //        let shapeLayer = CAShapeLayer()
        //        shapeLayer.path = circlePath.cgPath
        //        shapeLayer.strokeColor = UIColor.orange.cgColor
        //        shapeLayer.fillColor = UIColor.clear.cgColor
        //        chatButtonView.layer.addSublayer(shapeLayer)
    }
    
    func animateSlideKey() {
        //1. Animate Key
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 102 as CGFloat
        let sHeight = 52 as CGFloat
        let sI = 7 as CGFloat
        let kI = 8 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kI
        let kXR = kI + kWidth + kI
        let kY = kI
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations

        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kI + kWidth - mRi + wXError
        let wY = kHeight - mRh + kI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kI + kWidth + 1
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        //
        //add circles animation (add one circle for now)
        let cWidth = 6 as CGFloat
        let cHeight = 6 as CGFloat
        let c_X = kI + (kWidth * 0.5) - (cWidth * 0.5)
        let c_Y = c_X
        
        let cFrame = CGRect(x: c_X, y: c_Y, width: cWidth, height: cHeight)
        let circleImageView = UIImageView(frame: cFrame)
        circleImageView.image = UIImage(named: "smallDot")
        circleImageView.alpha = 0
//        sliderButtonImageView.addSubview(circleImageView)
        
        //SUBVIEW IMAGE CHANGES
        //change outline to solid box orange
        UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
        }, completion: nil)

        //change key to white
        UIView.transition(with: kImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
            kImageView.image = UIImage(named: "sliderKeyWhiteFill")
        }, completion: nil)
        //

        //change wLHS to white
        UIView.transition(with: wImageViewLHS_i, duration: 0.2, options: .transitionCrossDissolve, animations: {
            wImageViewLHS_i.image = UIImage(named: "sliderWigglerLHSWhiteFill")
        }, completion: nil)

        var t = 0.2 - 0.05

        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0 + t , options: .curveLinear, animations: {
            wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }, completion: nil)

        //FRAME POSITION TRANSLATIONS
        //KL -> KR
        UIView.animate(withDuration: 0.2, delay: 0 + t, options: .curveLinear , animations: {
            kImageView.frame = kFrameR
        }, completion: nil)

        //W_LHS = 0
        UIView.animate(withDuration: 0, delay: 0.08 + t, options: .curveLinear, animations: {
            wImageViewLHS_i.alpha = 0
        }, completion: nil)

        //W_RHS = 1
        UIView.animate(withDuration: 0, delay: 0.12 + t, options: .curveLinear, animations: {
            wImageViewRHS_i.alpha = 1
        }, completion: nil)
//

        //fade color after frame changes
        //when slider reaches right, bring out tail
        
//        ///CASE: WORKING BUT NOT VERY GOOD
//        //FRAME POSITION TRANSLATIONS
//        //KL -> KR
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
//            kImageView.frame = kFrameR
//
//            UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
//                wImageViewRHS_i.frame = wImageViewFrameRHS_f
//            }, completion: nil)
//
//        }) { (true) in
//            UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
//                sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
//            }, completion: nil)
//
//            //change key to white
//            UIView.transition(with: kImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
//                kImageView.image = UIImage(named: "sliderKeyWhiteFill")
//            }, completion: nil)
//
//            UIView.animate(withDuration: 0.01, delay: 0.16, options: .curveLinear, animations: {
//                wImageViewRHS_i.alpha = 1
//            }, completion: nil)
//        }
//
//        //W_LHS = 0
//        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
//            wImageViewLHS_i.alpha = 0
//        }, completion: nil)
//
        //CASE 3: fade after change frame
        
//        //KL -> KR
//        let scaleFactor = CGFloat((1 + sqrt(5.0)) * 0.5)
//
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
//            kImageView.frame = kFrameR
//        }) { (true) in
//            UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
//                kImageView.image = UIImage(named: "sliderKeyWhiteFill")
//                sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
//            }, completion: nil)
//
//            UIView.animate(withDuration: 0.21, delay: 0, options: .curveLinear, animations: {
//                wImageViewRHS_i.alpha = 1
//            }) { (true) in
//                self.addPendingLabel()
////                self.fadeSliderKey()
//            }
        
            //Circles
//            UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear , animations: {
//                circleImageView.alpha = 1
//                circleImageView.transform = circleImageView.transform.scaledBy(x: scaleFactor, y: scaleFactor)
//            }, completion: nil)
            
//        }
//
//        //W_RHS_i -> W_RHS_f
//        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
//            wImageViewRHS_i.frame = wImageViewFrameRHS_f
//        }, completion: nil)
//
//        //W_LHS_@ = 0
//        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
//            wImageViewLHS_i.alpha = 0
//        }, completion: nil)
        
        //the below function (animate vs. transition) is NOT transitioning the view, i.e. it does not animate the change in fade gradient over time
//        UIView.animate(withDuration: 0.2, delay: 0.1, options: .transitionCrossDissolve, animations: {
//            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxOrangeFill")
//        }, completion: nil)

    }
    
    var pendingLabel: UILabel?
    
    func addPendingLabel() {
        let sliderView = self.sliderButtonImageView!
    
        let lkWidth = sliderView.bounds.width
        let lkHeight = 23 as CGFloat
        let lk_X = self.bannerImageView!.bounds.width - (self.textInsets?.marginInset)! - lkWidth
        let topNveSpace = (self.bannerImageView!.bounds.height - sliderView.bounds.height) * 0.5
        let lk_Y = 75 as CGFloat
        
        let lookingForLabelFrame = CGRect(x: lk_X, y: lk_Y, width: lkWidth, height: lkHeight)
        let pendingLabel = UILabel(frame: lookingForLabelFrame)
        
        //
        pendingLabel.text = "Request Pending"
        pendingLabel.font = UIFont(name: "Avenir-Light", size: 10)
        pendingLabel.textColor = UIColor.white
        pendingLabel.textAlignment = .center
        //
        
        self.bannerImageView!.addSubview(pendingLabel)
        self.pendingLabel = pendingLabel
        
        let pLabel = self.pendingLabel!
        UIView.animate(withDuration: 0.5, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
            pLabel.alpha = 0
            
        }) { (true) in
            
        }
        
    }
    
    func fadeSliderKey() {
        let sliderKey = self.keyImageView!
        let wiggler = self.wImageViewRHS_i!
        let sView = sliderButtonImageView!
        
        UIView.animate(withDuration: 0.4, delay: 0, options: [.repeat, .autoreverse, .curveEaseInOut], animations: {
//            sliderKey.alpha = 0.3
//            wiggler.alpha = 0.3
//            sView.alpha = 0
        }) { (true) in
            
        }
        
    }
    
    func animateFadeToBlue() {
        //print("animateFadeToBlue")
        let kImageView = self.keyImageView!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //change orange to blue
        UIView.transition(with: sliderButtonImageView, duration: 0.3, options: .transitionCrossDissolve, animations: {
            sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
        }, completion: nil)
        
//        //remove old tapGestureRecognizer
//        kImageView.isUserInteractionEnabled = false
//
//        //add new tap gesture that allows the user to accept the request: add slide to swap
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ChatScreenViewController.tapToUnlock_Outgoing(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        sliderButtonImageView.addGestureRecognizer(tapGesture)
//        sliderButtonImageView.isUserInteractionEnabled = true
    }
    
    func animateAcceptChat() {
        //1. Animate Key
        let kImageView = self.keyImageView!
        let wImageViewLHS_i = self.wImageViewLHS_i!
        let wImageViewRHS_i = self.wImageViewRHS_i!
        let sliderButtonImageView = self.sliderButtonImageView!
        
        //init |K
        let sWidth = 102 as CGFloat
        let sHeight = 52 as CGFloat
        let sI = 7 as CGFloat
        let kI = 8 as CGFloat
        
        //...adjust new  slider key frame
        let kWidth = 39.5 as CGFloat
        let kHeight = 36 as CGFloat
        
        let kXL = kI
        let kXR = kI + kWidth + kI
        let kY = kI
        
        let kFrameR = CGRect(x: kXR, y: kY, width: kWidth, height: kHeight)
        let kFrameL = CGRect(x: kXL, y: kY, width: kWidth, height: kHeight)
        
        //Wiggler animations
        
        //...wiggler |K ANIMATIONS
        let mRo = 7 as CGFloat
        let mRi = 12 as CGFloat
        let mRh = 10.3604 as CGFloat
        let cornerRadiusInset = mRh
        
        //...1. LHS Wiggler
        let wWidth = mRi + mRo
        let wHeight = mRh
        
        let wXError = -0.1000 as CGFloat
        let wYError = 0.3604 as CGFloat
        
        let wX = kI + kWidth - mRi + wXError
        let wY = kHeight - mRh + kI + wYError
        
        let wFrame_i = CGRect(x: wX, y: wY, width: wWidth, height: wHeight)
        
        //...2. RHS Wiggler
        let wXRHS_f = kI + kWidth + 1
        let wImageViewFrameRHS_f = CGRect(x: wXRHS_f, y: wY, width: wWidth, height: wHeight)
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear, animations: {
            kImageView.frame = kFrameR
        }) { (true) in
            UIView.transition(with: sliderButtonImageView, duration: 0.2, options: .transitionCrossDissolve, animations: {
                kImageView.image = UIImage(named: "sliderKeyWhiteFill")
                sliderButtonImageView.image = UIImage(named: "sliderButtonBoxBlueFill")
            }, completion: nil)
            
            UIView.animate(withDuration: 0.21, delay: 0, options: .curveLinear, animations: {
                wImageViewRHS_i.alpha = 1
            }, completion: nil)
        }
        
        //W_RHS_i -> W_RHS_f
        UIView.animate(withDuration: 0.1, delay: 0, options: .curveLinear, animations: {
            wImageViewRHS_i.frame = wImageViewFrameRHS_f
        }, completion: nil)
        
        //W_LHS_@ = 0
        UIView.animate(withDuration: 0, delay: 0.08, options: .curveLinear, animations: {
            wImageViewLHS_i.alpha = 0
        }, completion: nil)
        
    }
    
    var chatButtonNames = ["startTalking", "keepTalking", "requestChat", "selfChatRequestPending", "selfChatRequestApproved", "othChatRequestPending", "othChatRequestApproved"]
    
    func chatRequestUIControlFlow(fromLiveObserver: Bool?) {
        //print("chatRequestUIControlFlow")
        
        //A
        inA:
        if self.otherPrivacy! == "false" {
            //print("inA")
            
            //new condition (Apr 5, 2018)
            guard self.chatRequestDoesExist == false else { break inA }
            //
            
            if self.messageThreadIDDoesExist == false {
                //A1. Start Talking when neither user is private
                
                self.drawChatOpen_N()
                
            } else if self.messageThreadIDDoesExist == true {
                //A2. Start or keep talking with person who already has talked to you
                
                self.drawContinueChat_Open()
            }
        }
        
        //B
        if self.otherPrivacy! == "true" {
            //print("inB")
            if self.chatRequestDoesExist == false {
                //B1. You need to request user to chat
                //stay on screen
                
                if self.messageThreadIDDoesExist == true {
                    //                    self.sliderButton.setImage(UIImage(named: "keepTalking"), for: .normal)
                    //                    self.buttonStateLabel.text = "Continue Talking"
                    self.drawContinueChat_Open()
                } else if self.messageThreadIDDoesExist == false {
                    //                    self.sliderButton.setImage(UIImage(named: "requestChat"), for: .normal)
                    //                    self.buttonStateLabel.text = "Request Chat"
                    self.chatRequest_N()
                }
                
            } else if self.chatRequestDoesExist == true {
                if self.selfInitiatedChatRequest == true {
                    //Need to switch cases of status
                    if self.chatRequestStatus == "false" {
                        //B2. Self-initiated chat request is pending
                        //do nothing, disable button
                        //                        self.sliderButton.setImage(UIImage(named: "selfChatRequestPending"), for: .normal)
                        //                        self.buttonStateLabel.text = "Request Pending"
                        self.chatRequestSelf_P()
                        
                    } else if self.chatRequestStatus == "true" {
                        //B3. Self-init chatReq approved by other
                        if self.messageThreadIDDoesExist == true {
                            //If selected user has already sent a message after approving our request, use this ID key
                            //print("User approved && sent a message,", self.absExistingMessageThreadID)
                            //                            self.sliderButton.setImage(UIImage(named: "selfChatRequestApproved"), for: .normal)
                            //                            self.buttonStateLabel.text = "\(self.otherDisplayName!) Approved"
                            self.chatRequestSelf_D()
                            
                        } else if self.messageThreadIDDoesExist == false {
                            //if user approved and has not sent a message, and we want to begin a convo then create new messageThreadKey
                            
                            //segue to CSVC, passing new messageThreadID
                            //                            self.sliderButton.setImage(UIImage(named: "selfChatRequestApproved"), for: .normal)
                            //                            self.buttonStateLabel.text = "\(self.otherDisplayName!) Approved"
                            self.chatRequestSelf_D()
                        }
                    }
                }
            }
        }
        
        //C
        if self.selfPrivacy! == "true" {
            //print("inC")
            //print("inC chatRequestDoesExist", self.chatRequestDoesExist)
            //print("inC selfInitiatedChatRequest", self.selfInitiatedChatRequest)
            //print("inC absExistingChatRequestID", self.absExistingChatRequestID)
            //print("inC status", self.chatRequestStatus)
            
            if self.chatRequestDoesExist! == true {
                if self.selfInitiatedChatRequest! == false {
                    if self.chatRequestStatus == "false" {
                        //C2. User requested to chat with you. Slide to approve
                        //print("inC2")
                        //                        self.sliderButton.setImage(UIImage(named: "othChatRequestPending"), for: .normal)
                        //                        self.buttonStateLabel.text = "Slide to Accept"
                        self.chatRequestOther_P()
                        
                    } else if self.chatRequestStatus == "true" {
                        //C3. You previously approved this user's request. Tap to chat
                        //                        self.sliderButton.setImage(UIImage(named: "othChatRequestApproved"), for: .normal)
                        
                        if self.messageThreadIDDoesExist == false {
                            
                            //                            self.buttonStateLabel.text = "You Approved"
                            self.chatRequestOther_D()
                        } else if self.messageThreadIDDoesExist == true {
                            
                            //                            self.buttonStateLabel.text = "You Approved"
                            self.chatRequestOther_D()
                        }
                    }
                    
                } else if self.selfInitiatedChatRequest! == true {
                    if self.chatRequestStatus == "false" {
                        //You requested to chat with user & status is pending
                        //                        self.sliderButton.setImage(UIImage(named: "selfChatRequestPending"), for: .normal)
                        //                        self.buttonStateLabel.text = "Your Request IS Pending"
                        self.chatRequestSelf_P()

                    } else if self.chatRequestStatus == "true" {
                        //                        self.sliderButton.setImage(UIImage(named: "selfChatRequestApproved"), for: .normal)
                        //                        self.buttonStateLabel.text = "Other Approved You"
                        self.chatRequestSelf_D()
                    }
                }
            }
        }
    }
    
  
    func setProfileSectionTitleColor() {
        let titleColor = UIColor.black
        
        self.aboutMeHeaderLabel!.textColor = titleColor
        self.lookingForHeaderLabel!.textColor = titleColor
        self.heightLabel!.textColor = titleColor
    }
    
    func setUserInfo() {

        //set ancillary values
        if let aboutMe = self.selectedUser["aboutMe"] {
            self.aboutMeTextField!.text = aboutMe
        } else {
            self.aboutMeTextField!.text = ""
        }
        
        if let lookingFor = self.selectedUser["lookingFor"] {
            self.lookingForLabel!.text = lookingFor
        } else {
            self.lookingForLabel!.text = ""
        }
        
        if let height = self.selectedUser["height"] {
            self.heightLabel!.text = height
        } else {
            self.heightLabel!.text = ""
        }
    }
    
    

    
    
    //need to observe for selected user incoming chat thread
    func observeActiveMessageThreads() {
        //print("XviewDidLoad observeActiveMessageThreads")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            
            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
            
            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
            //print("observeActiveMessageThreads is,", chatThreadBody)
            
            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
            //print("XCHAIN1. threadIDActive", threadIDActive)
            
            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            //print("XCHAIN2. chatInitiatorUID", chatInitiatorUID)
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
            //print("XCHAIN3. chatRecipientUID", chatRecipientUID)
            
            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            //print("XCHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
            //print("XCHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
            
            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
            //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
            
            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
            //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
            
            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
            //print("XCHAIN10. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let didAskShow = chatThreadBody["didAskShow"] else { return }
            //print("XCHAIN11. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
            guard chatIsActive != "nil" else { return }
            
            guard threadIDActive == "true" else { return}
            
            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
            //print("XCHAIN12. lastMessage", lastMessage)
            
            if threadIDActive == "true" {
                //TWO CASES: SelfInit message thread | OtherInit message thread
                //...1
                if chatInitiatorUID == self.selfUID! && chatRecipientUID == self.otherUID! {
                    self.messageThreadIDDoesExist = true
                    self.absExistingMessageThreadID = newChatThreadID
                
                    if self.otherPrivacy! == "false" {
                        self.chatButton_Open!.image = UIImage(named: "chatButtonOpen_D")
                    }
                    //...2
                } else if chatInitiatorUID == self.otherUID! && chatRecipientUID == self.selfUID! {
                    self.messageThreadIDDoesExist = true
                    self.absExistingMessageThreadID = newChatThreadID
                    
                    //need to change buttonView to full chat when chat is intercepted
                    if self.otherPrivacy! == "false" {
                        self.chatButton_Open?.image = UIImage(named: "chatButtonOpen_D")
                    }
                }
            } else {
            
            }
        })
        
    }
    
    var tempDeletedMessageThreadID: String?
    var tempDeletedMessageThreadIDDoesExist = false
    
//    func observeDeletedMessageThreads() {
//        let selfUID = self.selfUID!
//
//        //print("observeDeletedMessageThreads CALLED")
//        self.ref.child("users/\(selfUID)/myActiveChatThreads").observe(.childChanged, with: { (snapshot: DataSnapshot) in
//
//            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
//
//            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
//            //print("observeDeletedMessageThreads is,", chatThreadBody)
//
//            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
//            //print("observeDeletedMessageThreads. threadIDActive", threadIDActive)
//
//            if let selfDidDeleteThreadID = chatThreadBody["\(selfUID)_didDeleteThread"] {
//                //print("selfDidDeleteThreadID PASS UPVC", selfDidDeleteThreadID)
//                if threadIDActive == "false" && selfDidDeleteThreadID == "true" {
//                    //CASE ADDED WHEN SELF DELETES MESSAGE THREAD ID
//                    //print("UPVC WILLSET tempDeletedMessageThreadID", newChatThreadID)
//                    self.tempDeletedMessageThreadID = newChatThreadID
//                    self.messageThreadIDDoesExist = true
//                    self.tempDeletedMessageThreadIDDoesExist = true
//                }
//            }
//        })
//    }
    
    var deletedChatsbyThread = [String]()
    var deletedChatsbyUID = [String]()
    
    func observeDeletedMessageThreads() {
        //print("observeDeletedMessageThreads")
        let selfUID = self.selfUID!
        
        self.ref.child("users/\(selfUID)/deletedChats").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("observeDeletedMessageThreads key", snapshot.key)
            //print("observeDeletedMessageThreads value", snapshot.value)
            
            guard let chatThreadID = snapshot.key as? String else { return }
            guard let userID = snapshot.value as? String else { return }
            //print("DELETED chatThreadID", chatThreadID, "userID,", userID)
            
            if userID == self.otherUID! {
                self.tempDeletedMessageThreadID = chatThreadID
                self.messageThreadIDDoesExist = true
                self.tempDeletedMessageThreadIDDoesExist = true
            }
            
            if !self.deletedChatsbyThread.contains(chatThreadID) {
                self.deletedChatsbyThread.append(chatThreadID)
                self.deletedChatsbyUID.append(userID)
            }
            
        })
    }
    
//    func observeActiveMessageThreads() {
//        guard let user = Auth.auth().currentUser else { return }
//        let selfUID = user.uid
//
//        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
//        _refHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded, with: { (snapshot: DataSnapshot) in
//
//            let newChatThreadIDMarker = snapshot.key
//
//            //this allChatThreadIDMarkersArray all chatThreadID Markers stored in the user's local DB folder
//
//            let globalChatThreadID = newChatThreadIDMarker
//
//            self.ref.child("globalChatThreads/\(globalChatThreadID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
//                //print("I1. snapshot is,", snapshot)
//
//                //let newMessageID = snapshot.key
//
//                guard let messageBodyDictionary = snapshot.value as? NSDictionary else { return }
//                //print("I2. snapshot dictionary is", messageBodyDictionary)
//
//                guard let messageSenderUID = messageBodyDictionary["messageSenderUID"] as? String else { return }
//                //print("I3. sender UID is,", messageSenderUID)
//
//                guard let messageRecipientUID = messageBodyDictionary["messageRecipientUID"] as? String else { return }
//                //print("I4. recipient UID is,", messageRecipientUID)
//
//                if messageSenderUID == selfUID && messageRecipientUID == self.otherUID! {
//                    //self initiated chatThread //will never happen within UPVC
//
//                } else if messageSenderUID == self.otherUID! && messageRecipientUID == selfUID {
//                    //other initiated chatThread
//                    //print("oth init messageID")
//                    self.messageThreadIDDoesExist = true
//                    self.absExistingMessageThreadID = globalChatThreadID
//                }
//
//                self.chatRequestUIControlFlow()
//
//            })
//        })
//    }

    func observeSelectedUserIncomingChatRequest() {
        //print("observing incoming chat Requests within UPVC")
        
        self.ref.child("globalChatRequests/\(self.selfUID!)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("Z1. snapshot value is,", snapshot.value)
            
            let newChatRequestID = snapshot.key
            //print("Z2. newChatRequestID is,", newChatRequestID)
            
            guard let newChatRequestBody = snapshot.value as? [String:String] else { return }
            //print("Z3. newSwapRequestBody is,", newChatRequestBody)
            
            //UID
            guard let chatRequestSenderUID = newChatRequestBody["chatRequestSenderUID"] else { return }
            //print("Z4. ", chatRequestSenderUID )
            
            guard let chatRequestRecipientUID = newChatRequestBody["chatRequestRecipientUID"] else { return }
            //print("Z5. chatRequestSenderUID is", chatRequestSenderUID )
            
            //
            guard let chatRequestStatus = newChatRequestBody["chatRequestStatus"] else { return }
            //print("Z6. chatRequestSenderUID is", chatRequestSenderUID )
            
            //print("CONDITION1, chatRequestSenderUID", chatRequestSenderUID)
            //print("CONDITION1, otherUID", self.otherUID!)

            //print("CONDITION1", self.selfUID!)
            //print("CONDITION1", chatRequestRecipientUID)
            
            if chatRequestSenderUID == self.otherUID! {
                self.chatRequestDoesExist = true
                self.absExistingChatRequestID = newChatRequestID
                self.selfInitiatedChatRequest = false
                self.chatRequestStatus = chatRequestStatus
                //print("setting new request values1")
                
                //only fire UI control flow when other requests to not override existing animation
                self.chatRequestUIControlFlow(fromLiveObserver: true)

            } else if chatRequestSenderUID == self.selfUID! && chatRequestRecipientUID == self.otherUID! {
                self.chatRequestDoesExist = true
                self.absExistingChatRequestID = newChatRequestID
                self.selfInitiatedChatRequest = true
                self.chatRequestStatus = chatRequestStatus
                //print("setting new request values2")
            }
            
            //problem: when presses button, animation fires, but also updates database which reads request and updates UI accordingly
            //but we only want UI to update when other sends request
            
//            self.chatRequestUIControlFlow()
            self.observeSelectedUserChatRequestChanges()

        })
        //print("Firing UIChanges from within observer")
    }
    
    func observeSelectedUserChatRequestChanges() {
        //print("observeSelectedUserChatRequestChanges")
        
        guard let existingChatRequestID = self.absExistingChatRequestID else { return }
        
        self.ref.child("globalChatRequests/\(self.selfUID!)").observe(.childChanged, with: { (snapshot: DataSnapshot) in
            //print("X3. snapshot value is,", snapshot.value)
            
            //print("in body of observeSelectedUserChatRequestChanges ")
            
            let newChatRequestID = snapshot.key
            //print("S1. newChatRequestID is,", newChatRequestID)
            
            guard let newChatRequestBody = snapshot.value as? [String:String] else { return }
            //print("S2. newSwapRequestBody is,", newChatRequestBody)
            
            //UID
            guard let chatRequestSenderUID = newChatRequestBody["chatRequestSenderUID"] else { return }
            //print("S3. chatRequestSenderUID is", chatRequestSenderUID )
            
            guard let chatRecipientSender = newChatRequestBody["chatRequestRecipientUID"] else { return }
            //print("S3. chatRequestSenderUID is", chatRequestSenderUID )
            
            //
            guard let chatRequestStatus = newChatRequestBody["chatRequestStatus"] else { return }
            //print("S3. chatRequestSenderUID is", chatRequestSenderUID )
            
            if chatRequestSenderUID == self.otherUID! {
                self.chatRequestDoesExist = true
                self.absExistingChatRequestID = newChatRequestID
                self.selfInitiatedChatRequest = false
                self.chatRequestStatus = chatRequestStatus
                //print("observeSelectedUserChatRequestChanges2")

            } else if chatRequestSenderUID == self.selfUID! {
                self.chatRequestDoesExist = true
                self.absExistingChatRequestID = newChatRequestID
                self.selfInitiatedChatRequest = true
                self.chatRequestStatus = chatRequestStatus
                //print("observeSelectedUserChatRequestChanges3")
            }
            
            self.chatRequestUIControlFlow(fromLiveObserver: true)
        })
    }
    
    func confirmNewChatVars() {
        //print("INHERIT GridVC to PVC chatVars")
        //print("messageThreadIDDoesExist", self.messageThreadIDDoesExist)
        //print("absExistingMessageThreadID", self.absExistingMessageThreadID)
        //print("chatRequestDoesExist", self.chatRequestDoesExist)
        //print("selfInitiatedChatRequest", self.selfInitiatedChatRequest)
        //print("absExistingChatRequestID", self.absExistingChatRequestID)
        //print("chatRequestStatus", self.chatRequestStatus)
    }
    
    //struct MyUserInfo {
    //    var selfUID: String?
    //    var selfDisplayName: String?
    //    var selfVisus: String? //always inherited from GridVC at source
    //    var selfDefaultPhotoURL: String?
    //    var selfPrivacy: String?
    //}
    
    //var myUserInfo: MyUserInfo?
    
    func initializeSelfProfileInfo() {
        //initliazed from same source regardless of presenting
        self.selfUID = Auth.auth().currentUser!.uid
        self.selfDisplayName = myProfileInfoDict["selfDisplayName"]!
        self.selfVisus = myProfileInfoDict["selfVisus"]!
        self.selfDefaultPhotoURL = myProfileInfoDict["selfDefaultPhotoURL"]!
        self.selfPrivacy = myProfileInfoDict["privacy"]!
        
//        self.myUserInfo = MyUserInfo(selfUID: Auth.auth().currentUser?.uid,
//                                     selfDisplayName: myProfileInfoDict["selfDisplayName"],
//                                     selfVisus: myProfileInfoDict["selfVisus"],
//                                     selfDefaultPhotoURL: myProfileInfoDict["selfDefaultPhotoURL"],
//                                     selfPrivacy: myProfileInfoDict["privacy"])
    }
    
    func initializeOtherProfileInfo() {
        self.otherUID = selectedUser["UID"]!
        self.otherDisplayName = selectedUser["userDisplayName"]!
        self.otherVisus = selectedUser["visus"]!
        self.otherDefaultPhotoURL = selectedUser["userThumbnailURLString"]!
        self.otherPrivacy = selectedUser["userPrivacyPreference"]!
    }

    //CHAT ControlFlow:
    
    //All possible UI Outcomes:
    /*
     
     1. Immediately start chatting with user WHEN userprivacy.isEnabled = false. Two cases:
            Check for preexistingChatID:
            a. If True WHEN existingChatThreadIDKey != nil : then use preexistingChatID
            b. If False existingChatThreadIDKey = nil : Instantiate new chatThreadID
            UI presentation: Chat Slider Button (slide right to chat - or just tap button to chat)
     
     2. Userprivacy.isEnabled is true.
     
         A. If no request has been made WHEN preexistingChatRequest == false.
            UI presentation: Empty Slider Button (slide left to request chat)
     
         B. If a request has already been made WHEN preexistingChatRequest == true.
            b1. by self WHEN requestInitiatedBySelf == true :
                Case: && selectedUserResponseState == false (pending)
                UI presentation: request still pending (button disabled)
     
                Case: && selectedUserResponseState == true
                UI presentation: request approved (button enabled - slide right to chat)
     
                Case: && selectedUserResponseState == nil
                UI presentation: request denied (button disabled - wait 48h? or...
     
            b2. by other WHEN requestInitiatedBySelf == false :
                UI Presentation: accept/ignore request (button enabled - slide right to accept or press button to ignore/block/reject?)
                */
    
//    @IBAction func chatButton(_ sender: Any) {
//        //check for pre-existing chatThreadID, whether created by self or by other. If one exists, use that to present information in the ChatScreenVC, if not, then create a newChatThreadID() & pass that to ChatScreenVC
//        //self.chatButtonControlFlow()
//        self.chatRequestActionControlFlow()
//    }
    
//    @IBOutlet weak var buttonStateLabel: UILabel!
    
    func chatRequestActionControlFlow() {
        //A
        if self.otherPrivacy! == "false" && self.selfPrivacy! == "false" {
            //self privacy does not alter states so condition is ignored
            
            if self.messageThreadIDDoesExist == false {
                //A1. Start Talking when neither user is private
                //initiate new chatThreadID
                
                self.createNewMessageThreadID() //this method updates elf.absExistingMessageThreadID with new messageThreadID
                
                //segue to CSVC, passing new messageThreadID
                //print("SEGUE-A1")
                self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)

                
            } else if self.messageThreadIDDoesExist == true {
                //A2. Start or keep talking with person who already has talked to you
                
                //print("self.absExistingMessageThreadID!", self.absExistingMessageThreadID)
                //print("SEGUE-A2")
                self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                
                //segue to CSVC, using existing messageThreadID
            }
            
        }
        
        //B
        if self.otherPrivacy! == "true" {
            if self.chatRequestDoesExist == false {
                //B1. You need to request user to chat
                if self.messageThreadIDDoesExist == true {
                    //user previously chatted, you don't have to request them any more
                    //print(self.absExistingMessageThreadID)
                    //print("SEGUE-B1")
                    self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                } else if self.messageThreadIDDoesExist == false {
                    //you need to request user to chat
                    self.createNewChatRequest()
                    //stay on screen
                    
                    self.chatRequestUIControlFlow(fromLiveObserver: false)
                }
    
            } else if self.chatRequestDoesExist == true {
                if self.selfInitiatedChatRequest == true {
                    //Need to switch cases of status
                    if self.chatRequestStatus == "false" {
                        //B2. Self-initiated chat request is pending
                        //do nothing, disable button
                        
                    } else if self.chatRequestStatus == "true" {
                        //B3. Self-init chatReq approved by other
                        if self.messageThreadIDDoesExist == true {
                            //If selected user has already sent a message after approving our request, use this ID key, or if we already initiated chat previously
                            //print("SEGUE-B2")

                            //print("User approved && sent a message,", self.absExistingMessageThreadID)
                            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)

                        } else if self.messageThreadIDDoesExist == false {
                            //if user approved and has not sent a message, and we want to begin a convo then create new messageThreadKey
                            self.createNewMessageThreadID()
                            
                            //segue to CSVC, passing new messageThreadID
                            //print("SEGUE-B3")
                            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                        }
                    }
                }
            }
        }
        
        //C
        if self.selfPrivacy! == "true" {
            if self.chatRequestDoesExist! == true {
                if self.selfInitiatedChatRequest! == false {
                    if self.chatRequestStatus == "false" {
                        //C2. User requested to chat with you. Slide to approve
                        self.acceptChatRequest()
                        //reconfigureUI
                        self.chatRequestUIControlFlow(fromLiveObserver: false)

                    } else if self.chatRequestStatus == "true" {
                        //C3. You previously approved this user's request. Tap to chat
                        if self.messageThreadIDDoesExist == false {
                            self.createNewMessageThreadID()
                            //print("SEGUE-C31")

                            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                        } else if self.messageThreadIDDoesExist == true {
                            //print("self absExistingMessageThreadID", self.absExistingMessageThreadID)
                            
                            //print("SEGUE-C32=")

                            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                        }
                    }
                } else if self.selfInitiatedChatRequest! == true {
                    //
                }
                
            } else if self.chatRequestDoesExist! == false {
                if self.otherPrivacy! == "true" {
                    //case covered previoiusly
                    //check that Joe's button changes
                    //make sure this works
                } else if self.otherPrivacy! == "false" {
                    if self.messageThreadIDDoesExist == false {
                        self.createNewMessageThreadID()
                        //print("SEGUE-C33")
                        self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                    } else if self.messageThreadIDDoesExist == true {
                        //print(self.absExistingMessageThreadID)
                        self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                    }
                }
            }
        }
    }
    
    func presentChatScreenVC() {
        let controller = self.storyboard!.instantiateViewController(withIdentifier: "ChatScreenViewController") as! ChatScreenViewController
        
        controller.importedChatUser = self.selectedUser
        
        //print("SEGUE PASS profileVCtochatScreenVC passing absExistingMessageThreadID", self.absExistingMessageThreadID)
        
        controller.absExistingMessageThreadID = self.absExistingMessageThreadID
        controller.messageThreadIDDoesExist = self.messageThreadIDDoesExist
        
        /*guard let existingChatThreadIDKey = self.existingChatThreadIDKey else { return }
         //print("UProfileVC passing chatThready ID key to ChatScreenVC")
         controller.existingChatThreadIDKey = existingChatThreadIDKey*/
        /*
         var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
         var selfInitiatedSwapRequest: Bool?
         var swapRequestStatus = ""
         */
        if self.swapRequestDoesExist == true {
            //print("ACCIO UPVC: swapRequestDoesExist is,", self.swapRequestDoesExist, self.absExistingSwapRequestID!)
            controller.swapRequestDoesExist = true
            controller.selfInitiatedSwapRequest = self.selfInitiatedSwapRequest
            controller.absExistingSwapRequestID = self.absExistingSwapRequestID!
            controller.swapRequestStatus = self.swapRequestStatus
            
        } else  if self.swapRequestDoesExist == false  {
            controller.swapRequestDoesExist = false
        }
        
        //controller.swapRequestStatus = self.swapRequestStatus
        controller.swapRequestStatusDictionary = self.swapRequestStatusDictionary
        controller.selectedUser = self.selectedUser
        controller.otherUID = self.selectedUser["UID"]!
        //print("SEGUE profileVCtochatScreenVC self.selfVisus! PASS", self.selfVisus!)
        controller.selfVisus = self.selfVisus!
        controller.presentingSegueIdentifier = "profileVCtochatScreenVC"
//        controller.myProfileInfoDict = self.myProfileInfoDict
                //PASS CHAT REQEUEST DATA * NEW
        
        if self.chatRequestDoesExist != false {
            //print("PASSING chatRequestDoesExist to CSVC", self.chatRequestDoesExist)
            controller.chatRequestDoesExist = chatRequestDoesExist
            controller.absExistingChatRequestID = self.absExistingChatRequestID!
        }
        
        //
        controller.chatListObjects = self.chatListObjects
        controller.chatListObjectIDs = self.chatListObjectIDs
        controller.allUniqueUIDsinChatList = self.allUniqueUIDsinChatList
        
        self.present(controller, animated: false, completion: nil)
    }
    

    
    
    /*
    func chatButtonControlFlow() {
        
        if otherPrivacy == "false" { //1
            if preexistingChatRequest == true { //automatically always means that other requested chat
                
                //print("0a, This user has previously requested to chat with you. Please accept or ignore.")
                self.acceptChatRequest()
                self.switchResponseCases2()
                
            } else if preExistingMessageThreadID != nil { //1a
                    //use preexisting chat ID
                    //print("1a, preexisting chatID exists")
                    
                    guard let existingChatThreadID = self.preExistingMessageThreadID else { return }
                    //print("existingchatId is,", existingChatThreadID)
                    self.messageThreadID = existingChatThreadID
                    
                    //segue to chatscreen
                    self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                    
                } else { //1b if existingChatThreadIDKey = nil
                    //instantiate newChatThreadID
                    //print("1b, no preexisting chatThreadyIDkey exists")
                    self.createNewMessageThreadID()
                    //print("new chatThreadIDkey is,", self.messageThreadID)
                    
                    self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
                    
                    self.buttonStateLabel.text = "Start Talking"
            }
            
            //ATTN!: NEED TO ADD CASE WHERE SELECTEDUSERPRIVACYPREF == FALSE && PREEXISTING CHATREQUESTID EXISTS ~ Solved as 0a.
        }
        
        if otherPrivacy == "true" { //2
            if preexistingChatRequest == false { //2A
                //present empty slider to prompt user to request other user to chat
                //print("2A, User has requested privacy. No preexisting request exists from this user.")
                
                self.createNewChatRequest()
                self.buttonStateLabel.text = "Request Pending"
                
            }
        }
        
        if otherPrivacy == "true" {  //2
            if preexistingChatRequest == true { //2B
                //print("2B")
                if requestInitiatedBySelf == true { //2b1
                    self.switchResponseCases1()
                } else { //2b2 //requestInitiatedBySelf == false
                    //present button to accept or reject other user's request
                    //print("2b2")
                    
                    self.acceptChatRequest()
                }
            }
        }
        
    }
    
    func switchResponseCases1() {
        //print("switching response states")
        switch self.selectedUserResponseState {
        case "true":
            //print("user approved your request") //button slide to chat with label user approved your request
            self.determineChatStateTest()
        case "nil":
            //print("user denied your request") //button disabled
        default:
            //print("your request is still pending") //button pending with label request pending
        }
    }
    
    func switchResponseCases2() {
        //print("switching response states")
        switch self.selectedUserResponseState {
        case "true":
            //print("You previously approved this request") //chat button to talk to user
            self.determineChatStateTest()
        case "nil":
            //print("You denied this request") //button disabled
        default:
            //print("This user's request is still pending") //button pending with label request pending
            self.acceptChatRequest()
            self.buttonStateLabel.text = "Request Approved"
        }
    }
    
    func determineChatStateTest() {
        if preExistingMessageThreadID != nil { //1a
            //use preexisting chat ID
            //print("1a, preexisting chatID exists")
            
            guard let existingChatThreadID = self.preExistingMessageThreadID else { return }
            //print("existingchatId is,", existingChatThreadID)
            self.messageThreadID = existingChatThreadID
            
            //segue to chatscreen
            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
            
        } else { //1b if existingChatThreadIDKey = nil
            //instantiate newChatThreadID
            //print("1b, no preexisting chatThreadyIDkey exists")
            self.createNewMessageThreadID()
            //print("new chatThreadIDkey is,", self.messageThreadID)
            
            self.performSegue(withIdentifier: "profileVCtochatScreenVC", sender: self)
            
            self.buttonStateLabel.text = "Start Talking"
        }
    }
    
    func chatLabelControlFlow() {
        
        if otherPrivacy == "false" { //1
            if preexistingChatRequest == true { //automatically always means that other requested chat
                self.switchResponseCasesLabel2()
                
            } else if otherPrivacy == "false" { //1
                if preExistingMessageThreadID != nil { //1a
                    
                    self.buttonStateLabel.text = "Continue Talking"
                    
                } else { //1b if existingChatThreadIDKey = nil
                    
                    self.buttonStateLabel.text = "Start Talking"
                }
            }
        }
        
        if otherPrivacy == "true" { //2
            if preexistingChatRequest == false { //2A
                //present empty slider to prompt user to request other user to chat
                //print("2A")
                
                self.buttonStateLabel.text = "Request Chat"
                
            }
        }
        
        if otherPrivacy == "true" {  //2
            if preexistingChatRequest == true { //2B
                //print("2B")
                if requestInitiatedBySelf == true { //2b1
                    self.switchResponseCasesLabel()
                } else { //2b2 //requestInitiatedBySelf == false
                    //present button to accept or reject other user's request
                    //print("2b2")
                }
            }
        }
        
    }
    
    func switchResponseCasesLabel() {
        //print("switching response states")
        switch self.selectedUserResponseState {
        case "true":
            //print("user approved your request") //button slide to chat with label user approved your request
            self.buttonStateLabel.text = "User approved your request"
        case "nil":
            //print("user denied your request") //button disabled
            self.buttonStateLabel.text = "User denied request"
        default:
            //print("your request is still pending") //button pending with label request pending
            self.buttonStateLabel.text = "Request Pending"
        }
    }
    
    func switchResponseCasesLabel2() {
        //print("switching response states")
        switch self.selectedUserResponseState {
        case "true":
            //print("You previously approved this request") //chat button to talk to user
            self.buttonStateLabel.text = "Previously Approved"
        case "nil":
            //print("Previously Denied") //button disabled
            self.buttonStateLabel.text = "Previously Denied"
        default:
            //print("Still Pending") //button pending with label request pending
            self.buttonStateLabel.text = "Still Pending"
        }
    }
*/
    
    //MARK: DB setup
    
    var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    func photoVault(otherUID: String) -> UIImage? {
        //print("PV1. accesing photo vault")
        let fileName = "\(otherUID).jpeg"
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            //print("PV2. Successful try UIIMage")
            return UIImage(data: imageData)
        } catch {
            //print("PV2E. Error loading image : \(error)")
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     let otherUID = selectedUser["UID"]!

 */
    
    func acceptChatRequest() {
        //print("UPVC: now responding to existing chat request")
        //"accept" the users request when button is pressed by writing true to selfUID in respective DB locations
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = selectedUser["UID"]!
        
        //guard let chatRequestId = self.preexistingRequestKey else { return }
        let chatRequestId = self.absExistingChatRequestID!
        //print("chatRequestId is", chatRequestId)
        
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/\(selfUID)").setValue("true")
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/\(selfUID)").setValue("true")
        
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/chatRequestStatus").setValue("true")
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/chatRequestStatus").setValue("true")
    }
    
    func createNewMessageThreadID() {
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = selectedUser["UID"]!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        self.absExistingMessageThreadID = ref.child("globalChatThreads").childByAutoId().key //might not need this
        let absExistingMessageThreadID = self.absExistingMessageThreadID
 
        let chatInitiatorUID = self.selfUID!
        let chatInitiatorDisplayName = self.selfDisplayName!
//        let chatInitiatorPhotoURL = self.selfDefaultPhotoURL!
        let chatInitiatorPhotoURL = self.myProfileInfoDict["gridThumb_URL"]!
        let chatInitiatorVisus = self.selfVisus!

        let chatRecipientUID = self.otherUID!
        let chatRecipientDisplayName = self.otherDisplayName!
//        let chatRecipientPhotoURL =  !
        
        var chatRecipientPhotoURL = ""
        if let gridURL = selectedUser["gridThumb_URL"] {
            chatRecipientPhotoURL = gridURL
        } else if let othDefaultUrl = self.otherDefaultPhotoURL {
            chatRecipientPhotoURL = othDefaultUrl
        }
      
  
        
        let chatRecipientVisus = self.otherVisus!

        var chatThreadBody = [String:String]()

        chatThreadBody["\(absExistingMessageThreadID)"] = "false"
        
        chatThreadBody["chatIsActive"] = "true"

        chatThreadBody["chatInitiatorUID"] = chatInitiatorUID
        chatThreadBody["chatInitiatorDisplayName"] = chatInitiatorDisplayName
        chatThreadBody["chatInitiatorPhotoURL"] = chatInitiatorPhotoURL
        chatThreadBody["chatInitiatorVisus"] = chatInitiatorVisus
        
        chatThreadBody["chatRecipientUID"] = chatRecipientUID
        chatThreadBody["chatRecipientDisplayName"] = chatRecipientDisplayName
        chatThreadBody["chatRecipientPhotoURL"] = chatRecipientPhotoURL
        chatThreadBody["chatRecipientVisus"] = chatRecipientVisus

        //since creatingAnewthreadID always segues to chatscreen set isPresnet = true
        chatThreadBody["\(selfUID)_isPresent"] = "true"
        chatThreadBody["\(chatRecipientUID)_isPresent"] = "false"
        
        chatThreadBody["lastMessage"] = ""
        
        //for oneWaySwaps filtering
        chatThreadBody["didAskShow"] = "false"
        
        //for thread deletion
        chatThreadBody["\(selfUID)_didDeleteThread"] = "false"
        
        if self.otherPrivacy! == "false" {
            chatThreadBody["chatThreadFromRequest"] = "false"
        } else {
            chatThreadBody["chatThreadFromRequest"] = "true"
        }
        
        let childUpdates = [
            "/users/\(selfUID)/myActiveChatThreads/\(absExistingMessageThreadID)": chatThreadBody,
            "/users/\(otherUID)/myActiveChatThreads/\(absExistingMessageThreadID)": chatThreadBody,
            ]
        
//        ref.updateChildValues(childUpdates)

        if self.tempDeletedMessageThreadIDDoesExist == true {
            //print("detected tempDeletedMessageThreadID WILL NOT create new Thread ID")
            
        } else {
            //print("WILL UPDATECHILDVALUES")
            ref.updateChildValues(childUpdates)
        }
    }
    
    //MARK: Observers
    
    func observeUnlockedUsers() {
        //print("observeUnlockedUsers self.unlockedUsers", unlockedUsers)
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/unlockedUsers").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("observeUnlockedUsers snapshot is,", snapshot.key)
            let unlockedUID = snapshot.key
            //print("observeUnlockedUsers unlockedUID,", unlockedUID)
            self.unlockedUsers.append(unlockedUID)
            if unlockedUID == self.otherUID! && self.otherVisus! == "false" && self.selfVisus == "true" {
                if let imageToSet = self.photoVault(otherUID: unlockedUID) {
                    self.profileImageView!.image = imageToSet
                }
            }
        })
    }
    
    func liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: String, swapRecipientID: String, swapInitiatorPhotoURL: String, swapRecipientPhotoURL: String, swapID: String, swapInitiatorVisus: String, swapRecipientVisus: String) {
        
        guard swapInitiatorVisus == "false" && swapRecipientVisus == "false" else { return }
        
        //print("PASS GUARD 2waySWAP liveSaveUserProfileImageFromDBtoDisk")
        
        if swapInitiatorID == self.selfUID! {
            //oneway reveal
            if swapRecipientPhotoURL != "" && swapRecipientPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: swapID, photoChildNode: "swapRecipientPhotoURL", fromUserID: swapRecipientID, requestType: "")
            }
            
        } else if swapInitiatorID != self.selfUID! {
            //swap initiator is other
            if swapInitiatorPhotoURL != "" && swapInitiatorPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "incomingOneWayShow")
            }
        }
    }
    
    func loadImageFromDatabaseToMemory_thenSave(withClearURL: String, withRequestID: String, photoChildNode: String, fromUserID: String, requestType: String) {
        //print("CVC ONEWAYSHOW loadImageToMemory()")
        let newPhotoURL = URL(string: withClearURL)
        self.imageLoaderView!.sd_setImage(with: newPhotoURL) { (loadedImage, error, _, savedURL) in
            
            if error == nil {
                self.saveUnlockedImageToDisk(imageObject: loadedImage!, fromUserID: fromUserID)
                //print("revelaro successfully loaded image")
                
                let selfUID = self.selfUID!
                let otherUID = fromUserID
                
                //print("CVC ONEWAYSHOW loadImageFromDatabaseToMemory_thenSave will clear photoURL")
                    if requestType == "incomingOneWayShow" {
                        //clear only on one-way & let the voilaVC clear all other cases
                        //print("UPVC incomingOneWayShow CLEAR")

                        self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                        self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                    }
            } else {
                //print("CVC phot=o loader error,", error)
            }
        }
    }
    
    func saveUnlockedImageToDisk(imageObject: UIImage, fromUserID: String) {
        //print("CVC  OneWayShow writing to vault")
        let fileName = "\(fromUserID).jpeg"
        if let data = UIImageJPEGRepresentation(imageObject, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("CVC OneWayShow photo written to vault with filename,", fileName)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    var imageLoaderView: UIImageView!
    
    func addImageLoader() {
        let imageX = 0 as CGFloat
        let imageY = 0 as CGFloat
        let imageWidth = 5 as CGFloat
        let imageHeight = 5 as CGFloat
        
        let imageFrame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        let imageLoaderView = UIImageView(frame: imageFrame)
        
        view.addSubview(imageLoaderView)
        self.imageLoaderView = imageLoaderView
        self.imageLoaderView.isHidden = true
    }
    
    func observeSelectedUserIncomingSwapRequest() {
        //print("observeSelectedUserIncomingSwapRequest")
        
        self.ref.child("globalSwapRequests/\(self.selfUID!)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("X3. snapshot value is,", snapshot.value)
            
            let newSwapRequestID = snapshot.key
            //print("S1. newSwapRequestID is,", newSwapRequestID)
            
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            //print("S2. newSwapRequestBody is,", newSwapRequestBody)
            
            //UID
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            //print("S3. newswapInitiatorID is", swapInitiatorID )
            
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }
            //print("S4. newswapRecipientID is", swapRecipientID )
            
            //First Name
            guard let swapInitiatorFirstName = newSwapRequestBody["swapInitiatorFirstName"] else { return }
            //print("S3. newswapInitiatorID is", swapInitiatorFirstName )
            
            guard let swapRecipientFirstName = newSwapRequestBody["swapRecipientFirstName"] else { return }
            //print("S11. swapRecipientFirstName is", swapRecipientFirstName)
            
            //Default Image URL
            guard let swapInitiatorDefaultURL = newSwapRequestBody["swapInitiatorDefaultURL"] else { return }
            //print("S9. swapRequestStatus is", swapInitiatorDefaultURL)
            
            guard let swapRecipientDefaultURL = newSwapRequestBody["swapRecipientDefaultURL"] else { return }
            //print("S10. ACCIO swapRequestStatus is", swapRecipientDefaultURL)
            
            //New Photo URL
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            //print("S5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL )
            
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            //print("S6. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
            
            //Did Open?
            guard let swapInitiatorDidOpen = newSwapRequestBody["swapInitiatorDidOpen"] else { return }
            //print("S7. swapRequestStatus is", swapInitiatorDidOpen)
            
            guard let swapRecipientDidOpen = newSwapRequestBody["swapRecipientDidOpen"] else { return }
            //print("S8. swapRequestStatus is", swapRecipientDidOpen)
            
            //Visus Property
            
            guard let swapInitiatorVisus = newSwapRequestBody["swapInitiatorVisus"] else { return }
            //print("S7. swapInitiatorVisus is", swapInitiatorVisus)
            
            guard let swapRecipientVisus = newSwapRequestBody["swapRecipientVisus"] else { return }
            //print("S8. swapRecipientVisus is", swapRecipientVisus)
            
            //Request Status
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }
            //print("S7. swapRequestStatus is", swapRequestStatus)
            
            guard let swapIsActive = newSwapRequestBody["swapIsActive"] else { return }
            guard swapIsActive != "nil" else { return }
            
            //set the following variables:
            /*
             //MARK: Swap Control Flow Variables
             var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
             var selfInitiatedSwapRequest: Bool?
             var swapRequestStatus: String?
             var swapRequestStatusDictionary = [String:String]()
             */
            
            self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: newSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
            
            
            //returns incoming swap requests from selected user
            if swapInitiatorID == self.otherUID! {
                self.swapRequestDoesExist = true
                self.absExistingSwapRequestID = newSwapRequestID
                self.selfInitiatedSwapRequest = false
                self.swapRequestStatus = swapRequestStatus
                //print("UPVC Live observer. Observed incoming swap request from curent user,", swapInitiatorFirstName)
                
            //returns outgoing swap requests to selected user
            } else if swapInitiatorID == self.selfUID! && swapRecipientID == self.otherUID! {
                self.swapRequestDoesExist = true
                self.absExistingSwapRequestID = newSwapRequestID
                self.selfInitiatedSwapRequest = true
                self.swapRequestStatus = swapRequestStatus
                //print("UPVC Live observer. Observed incoming swap request from curent user,", swapInitiatorFirstName)
            }
        })
    }
    
    func observeSelectedUserIncomingSwapRequestChanges() {
        
        //adding below guard statement makes sure that observer will run only when there is an existing chatRequest
        
        guard let existingSwapRequestID = self.absExistingSwapRequestID else { return }
        
        _refHandle = self.ref.child("globalSwapRequests/\(self.selfUID!)").observe(.childChanged, with: { (snapshot: DataSnapshot) in
            //print("X3. snapshot value is,", snapshot.value)
            
            let newSwapRequestID = snapshot.key
            //print("S1. newSwapRequestID is,", newSwapRequestID)
            
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            //print("S2. newSwapRequestBody is,", newSwapRequestBody)
            
            //UID
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            //print("S3. newswapInitiatorID is", swapInitiatorID )
            
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }
            //print("S4. newswapRecipientID is", swapRecipientID )
            
            //First Name
            guard let swapInitiatorFirstName = newSwapRequestBody["swapInitiatorFirstName"] else { return }
            //print("S3. newswapInitiatorID is", swapInitiatorFirstName )
            
            guard let swapRecipientFirstName = newSwapRequestBody["swapRecipientFirstName"] else { return }
            //print("S11. swapRecipientFirstName is", swapRecipientFirstName)
            
            //Default Image URL
            guard let swapInitiatorDefaultURL = newSwapRequestBody["swapInitiatorDefaultURL"] else { return }
            //print("S9. swapRequestStatus is", swapInitiatorDefaultURL)
            
            guard let swapRecipientDefaultURL = newSwapRequestBody["swapRecipientDefaultURL"] else { return }
            //print("S10. ACCIO swapRequestStatus is", swapRecipientDefaultURL)
            
            //New Photo URL
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            //print("S5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL)
            
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            //print("S6. newswapRecipientPhotoURL is", swapRecipientPhotoURL)
            
            //Did Open?
            guard let swapInitiatorDidOpen = newSwapRequestBody["swapInitiatorDidOpen"] else { return }
            //print("S7. swapRequestStatus is", swapInitiatorDidOpen)
            
            guard let swapRecipientDidOpen = newSwapRequestBody["swapRecipientDidOpen"] else { return }
            //print("S8. swapRequestStatus is", swapRecipientDidOpen)
            
            //Visus Property
            
            guard let swapInitiatorVisus = newSwapRequestBody["swapInitiatorVisus"] else { return }
            //print("S7. swapInitiatorVisus is", swapInitiatorVisus)
            
            guard let swapRecipientVisus = newSwapRequestBody["swapRecipientVisus"] else { return }
            //print("S8. swapRecipientVisus is", swapRecipientVisus)
            
            //Request Status
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }
            //print("S7. swapRequestStatus is", swapRequestStatus)
            
            guard let swapIsActive = newSwapRequestBody["swapIsActive"] else { return }
            guard swapIsActive != "nil" else { return }
            
            //set the following variables:
            /*
             //MARK: Swap Control Flow Variables
             var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
             var selfInitiatedSwapRequest: Bool?
             var swapRequestStatus: String?
             var swapRequestStatusDictionary = [String:String]()
             */
            
             self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: newSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
            
            if self.swapRequestDoesExist == true {
                if swapInitiatorID == self.otherUID {
                    //print("updating values live within UPVC")
                    self.swapRequestDoesExist = true
                    self.absExistingSwapRequestID = newSwapRequestID
                    self.selfInitiatedSwapRequest = false
                    self.swapRequestStatus = swapRequestStatus
                }
            }
        })
    }
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = SegueFromRight(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    var finalScrollPos: CGPoint?
    
    var ref_SelfModelController = SelfModelController()

    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "profileVCtochatScreenVC" {

            var prefersStatusBarHidden: Bool {
                return false
            }
            
            let controller = segue.destination as! ChatScreenViewController
            controller.importedChatUser = self.selectedUser
            //print("UPVC willpass,importedStatusBarHeight ", importedStatusBarHeight)
            
            if self.upvcShouldUnwindToChatScreen == true {
                //RESET
                self.upvcShouldUnwindToChatScreen = false
            } else {
                //do not send back status bar height if unwind
                controller.importedStatusBarHeight = self.importedStatusBarHeight!
            }

            controller.sourceVC = "UserProfileViewController"
            controller.ref_SelfModelController = self.ref_SelfModelController
            
            //print("SEGUE PASS profileVCtochatScreenVC passing absExistingMessageThreadID", self.absExistingMessageThreadID)
            if let tempChatThreadID = self.tempDeletedMessageThreadID {
                //print("UPVC WILL PAS tempDeletedMessageThreadID", tempChatThreadID)
                controller.tempDeletedMessageThreadID = self.tempDeletedMessageThreadID!
            }
            
            controller.absExistingMessageThreadID = self.absExistingMessageThreadID
            controller.messageThreadIDDoesExist = self.messageThreadIDDoesExist

            /*guard let existingChatThreadIDKey = self.existingChatThreadIDKey else { return }
             //print("UProfileVC passing chatThready ID key to ChatScreenVC")
             controller.existingChatThreadIDKey = existingChatThreadIDKey*/
            /*
            var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
            var selfInitiatedSwapRequest: Bool?
            var swapRequestStatus = ""
            */
            if self.swapRequestDoesExist == true {
                //print("ACCIO UPVC: swapRequestDoesExist is,", self.swapRequestDoesExist, self.absExistingSwapRequestID!)
                controller.swapRequestDoesExist = true
                controller.selfInitiatedSwapRequest = self.selfInitiatedSwapRequest
                controller.absExistingSwapRequestID = self.absExistingSwapRequestID!
                controller.swapRequestStatus = self.swapRequestStatus

            } else  if self.swapRequestDoesExist == false  {
                controller.swapRequestDoesExist = false
            }

            //controller.swapRequestStatus = self.swapRequestStatus
            controller.swapRequestStatusDictionary = self.swapRequestStatusDictionary
            controller.selectedUser = self.selectedUser
            controller.otherUID = self.selectedUser["UID"]!
            //print("SEGUE profileVCtochatScreenVC self.selfVisus! PASS", self.selfVisus!)
            controller.selfVisus = self.selfVisus!
            controller.presentingSegueIdentifier = "profileVCtochatScreenVC"
//            controller.myProfileInfoDict = self.myProfileInfoDict

            //PASS CHAT REQEUEST DATA * NEW
            if self.chatRequestDoesExist != false {
                //print("PASSING chatRequestDoesExist to CSVC", self.chatRequestDoesExist)
                controller.chatRequestDoesExist = chatRequestDoesExist
                controller.absExistingChatRequestID = self.absExistingChatRequestID!
            }

            //
            controller.chatListObjects = self.chatListObjects
            controller.chatListObjectIDs = self.chatListObjectIDs
            controller.allUniqueUIDsinChatList = self.allUniqueUIDsinChatList
            controller.unlockedUsers = self.unlockedUsers
            controller.incomingDismissedUIDs = self.incomingDismissedUIDs

        } else if segue.identifier == "unwindToGrid" {
            let controller = segue.destination as! CollectionViewController
        }
    }
    
    func createNewChatRequest() {
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = selectedUser["UID"]!
        
        let chatRequestKey = self.ref.child("globalChatRequests").childByAutoId().key
        //let data = [otherUID : "false"]
        let data = ["chatRequestSenderUID" : selfUID]
        
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        //mdata["chatRequestSenderUID"] = selfUID
        mdata["messageTimeStamp"] = timeStamp
        //NEW
        mdata["chatRequestRecipientUID"] = otherUID
        mdata["chatRequestStatus"] = "false"
        mdata["chatRequestSenderFirstName"] = self.selfDisplayName!
        mdata["chatRequestRecipientFirstName"] = self.otherDisplayName!
        mdata["chatRequestSenderDidOpen"] = "false"
        mdata["chatRequestRecipientDidOpen"] = "false"
        mdata["chatRequestSenderDefaultPhotoURL"] = self.selfDefaultPhotoURL!
        mdata["chatRequestRecipientDefaultPhotoURL"] = self.otherDefaultPhotoURL!
        mdata["chatRequestSenderVisus"] = self.selfVisus!
        mdata["chatRequestRecipientVisus"] = self.otherVisus!
        
        mdata["chatThreadDoesExist"] = "false"
        mdata["existingChatThreadID"] = ""

        mdata["lastMessageString"] = ""
        mdata["didSwap"] = "false"
        
        
        let childUpdates = [
            //"/globalChatRequests/\(chatRequestKey)": mdata,
            "globalChatRequests/\(selfUID)/\(chatRequestKey)": mdata,
            "globalChatRequests/\(otherUID)/\(chatRequestKey)": mdata,
        ]
        
        ref.updateChildValues(childUpdates)
        //ref.child("globalChatRequests/\(selfUID)/chatRequestKey/\(otherUID)").setValue("false")
        //ref.child("globalChatRequests/\(otherUID)/chatRequestKey/\(otherUID)").setValue("false")
//        self.sliderButton.setImage(UIImage(named: "pendingRequestSlider"), for: .normal)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //extension UserProfileViewController {
    //
    //    @objc func slideScreenDown(_ sender: UIPanGestureRecognizer) {
    //        //print("pan gesture observed")
    //
    //        let percentThreshold:CGFloat = 0.3
    //
    //        // convert y-position to downward pull progress (percentage)
    //        let translation = sender.translation(in: view)
    //        let verticalMovement = translation.y / view.bounds.height
    //        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
    //        let downwardMovementPercent = fminf(downwardMovement, 1.0)
    //        let progress = CGFloat(downwardMovementPercent)
    //        guard let interactor = interactor else { return }
    //
    //        switch sender.state {
    //        case .began:
    //            interactor.hasStarted = true
    //            dismiss(animated: true, completion: nil)
    //        case .changed:
    //            interactor.shouldFinish = progress > percentThreshold
    //            interactor.update(progress)
    //        case .cancelled:
    //            interactor.hasStarted = false
    //            interactor.cancel()
    //        case .ended:
    //            interactor.hasStarted = false
    //            interactor.shouldFinish
    //                ? interactor.finish()
    //                : interactor.cancel()
    //        default:
    //            break
    //        }
    //
    //    }
    //}

    
    /*
     
     func observeActiveChatRequestChanges() {
     guard let user = Auth.auth().currentUser else { return }
     let selfUID = user.uid
     
     _refHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childChanged , with: { (snapshot: DataSnapshot) in
     
     let newChatRequestID = snapshot.key
     
     guard let chatRequestData = snapshot.value as? [String:String] else { return }
     //print("chatRequestData", chatRequestData)
     guard let chatRequestSenderUID = chatRequestData["chatRequestSenderUID"] as? String else { return }
     //print("chatRequestSenderUID", chatRequestSenderUID)
     guard let chatRequestRecipientUID = chatRequestData["chatRequestRecipientUID"] as? String else { return }
     //print("chatRequestRecipientUID", chatRequestRecipientUID)
     guard let chatRequestStatus = chatRequestData["chatRequestStatus"] as? String else { return }
     /*
     //MARK: Chat Control Flow Variables - Make sure all of these are successfully passed from previous VC
     var otherPrivacy: String? //true or false string
     var chatThreadID = "" ; var existingChatThreadID: String?
     var preexistingChatRequest: Bool? ; var preexistingChatRequestKey: String? ; var requestInitiatedBySelf: Bool?
     var selectedUserResponseState = "" //True (Accepted), False (Pending), Nil (Rejected/Ignored)
     */
     
     //When initiator is other
     if chatRequestSenderUID == self.otherUID! {
     self.preexistingChatRequestKey = newChatRequestID
     self.requestInitiatedBySelf = false
     self.selectedUserResponseState = chatRequestStatus
     
     //When initiator is self & recipient is other
     } else if chatRequestRecipientUID == self.otherUID! {
     self.preexistingChatRequestKey = newChatRequestID
     self.requestInitiatedBySelf = true
     self.selectedUserResponseState = chatRequestStatus
     
     }
     
     })
     }
     
 */
        
    /* func chatButtonControlFlow() { // SKELETON
        
        func switchResponseCases() {
            //print("switching response states")
            switch self.selectedUserResponseState {
            case "true": //print("user approved your request") //button slide to chat with label user approved your request
            case "nil": //print("user denied your request") //button disabled
            default: //print("your request is still pending") //button pending with label request pending
            }
        }
        
        if otherPrivacy == "false" { //1
            if existingChatThreadID != nil { //1a
                //use preexisting chat ID
                //print("1a, preexisting chatID exists")
                
            } else { //1b if existingChatThreadIDKey = nil
                //instantiate newChatThreadID
        
                
            }
        }
        
        if otherPrivacy == "true" { //2
            if preexistingChatRequest == false { //2A
                //present empty slider to prompt user to request other user to chat
                //print("2A")
            }
        }
        
        if otherPrivacy == "true" {  //2
            if preexistingChatRequest == true { //2B
                //print("2B")
                if requestInitiatedBySelf == true { //2b1
                    switchResponseCases()
                } else { //2b2 //requestInitiatedBySelf == false
                    //present button to accept or reject other user's request
                    //print("2b2")
                }
            }
        }
        
    } */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //CHECK POINT BEFORE ANY CHANGES ARE MADE TO INTERNAL CODE ***************  Chats Working!!!
}

extension UILabel {
    func addCharacterSpacing(withValue: CGFloat) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedStringKey.kern, value: withValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}
