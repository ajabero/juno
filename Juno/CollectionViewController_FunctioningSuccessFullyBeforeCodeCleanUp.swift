/*



//
//  CollectionViewController.swift
//  Juno
//
//  Created by Asaad on 2/10/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseStorage
import GeoFire

//need to:
/*
 Create a thumbnail for the bluirred image and upload that as well to the user's profile picture. Should pictures be in a folder?
 Then update the code in the table view to check for blurred prpofile condition if T/F and then request the url depending on the user's choice
 Must write T/F condition to user's profile folder
 
 Temporary location solution:
 Get user's last updated location from DB file, and and use that location to populate collection view. In the meantime, retrieve user location updates in the code in background, and update location record when viewWillDisappear. That way, user has to either reload the collection view for data to reload, or user has to go to another screen and come back
 */

private let reuseIdentifier = "Cell"

class CollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate {
    
    //Objective make sure existingChatThreadId is detected
    //Cases: selfinitiated ID, otherinitiatedID
    
    //MARK: Control Flow Variables - Make sure all of these are successfully passed from previous VC
    var selectedUserPrivacyPreference: String?
    var existingChatThreadID: String?
    var preexistingChatRequest: Bool? ; var preexistingChatRequestKey: String? ; var requestInitiatedBySelf: Bool?
    var selectedUserResponseState = "" //True (Accepted), False (Pending), Nil (Rejected/Ignored)
    
    //new
    var chatPermissionsDictionary = [String:String]()
    var uidtoChatRequestIDExtrapDict = [String:String]()
    var chatRequestsApprovedUIDArray = [String]()
    var chatRequestStatusDictionary = [String : String]()
    var selfInitiatedChatThreadsDictionary = [String : String]()
    //MARK: DB setup
    
    var ref: DatabaseReference!
    fileprivate var _refHandle: DatabaseHandle!
    
    //MARK: Global Properties
    
    let locationManager = CLLocationManager()
    
    var importedUserLocation: CLLocation?
    var userLocation: CLLocation?
    var center: CLLocation?
    var userLongitude = ""
    var userLatitude = ""
    var userTimeStamp = ""
    
    //Data Structures
    var uidinProximityRawArray = [usersinProximityStruct]()
    var sortedUIDinProximityArray = [String]()
    var photoURLStringArray = [String]()
    var userDictArray = [[String : String]]()
    
    var allChatThreadIDsDictionary = [String : [String: [String: AnyObject]]]()
    var userChatThreadObjectsArrayofDicts = [[String : String]]()
    var uidtoChatThreadIDKeyIndexDictionary = [String: String]() //where String1 is UID & String2 is ChatThreadID
    var newChatThreadIDsArray = [String]()
    //var existingChatThreadID: String?
    var selfInitiatedChatThreadRequestsDict = [String:String]()
    
    struct usersinProximityStruct {
        var otherUserID = "a1x"
        var toOtherUserDistance = 0123.4567890
    }
    
    //User Preferences
    var isHiddenPreference = true
    
    //MARK: Global Outlets
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    func configureDatabase() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        _refHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
            
            let newChatThreadID = snapshot.key
            //this chatThreadIDsArraycontains all chatThreadIDs
            self.newChatThreadIDsArray.append(newChatThreadID)
            
            self.ref.child("globalChatThreads/\(newChatThreadID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
                print("I1. snapshot is,", snapshot)
                
                guard let snapshotDictionary = snapshot.value as? NSDictionary else { return }
                print("I2. snapshot dictionary is", snapshotDictionary)
                
                guard let parsedMessageSenderUID = snapshotDictionary["messageSenderUID"] as? String else { return }
                print("I3. sender UID is,", parsedMessageSenderUID)
                
                guard let parsedMessageRecipientUID = snapshotDictionary["messageRecipientUID"] as? String else { return }
                print("I4. recipient UID is,", parsedMessageRecipientUID)
                
                if parsedMessageSenderUID == selfUID {
                    print("I5. this chatThreadID was initiated by self")
                    
                    self.selfInitiatedChatThreadsDictionary[parsedMessageRecipientUID] = newChatThreadID
                    print("I6.new selfInitiatedChatThreadsDictionary is,", self.selfInitiatedChatThreadsDictionary)
                    
                } else {
                    //
                }
                
                //this dictionary contains senderID:chatThreadID key-value pairs including self. Therefore, to check if the initiator is self, must search for the value of the existingChatThreadID and check if sender key is self. If true, then self is sender, else other.
                if !self.uidtoChatThreadIDKeyIndexDictionary.keys.contains(parsedMessageSenderUID) {
                    print("I7. dictionary does not already contain UID")
                    self.uidtoChatThreadIDKeyIndexDictionary[parsedMessageSenderUID] = newChatThreadID
                    print("I8. dictionary is now", self.uidtoChatThreadIDKeyIndexDictionary)
                } else {
                    print("I9. dictionary contains UID")
                }
            })
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        self.configureDatabase()
        self.configureLocation()
        
        // Do any additional setup after loading the view.
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 9, 0, 9)
        flowLayout.itemSize = CGSize(width: 126, height: 182)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Register cell classes
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.observeIncomingChatRequests()
        self.observeSentRequestsChanges()
    }
    
    func configureLocation() {
        locationManager.delegate = self
        
        //Control-flow: if user is accessing Grid for first time from Account Setup, use importedUserLocation is initial value for user location, else, retrieve last location from DB
        if importedUserLocation != nil {
            self.observeKeysEntered()
            self.observeKeysReady()
        } else {
            self.getUserLastLocation()
            if CLLocationManager.locationServicesEnabled() {
                print("true, location services are enabled")
                self.setLocationParameters()
            } else {
                print("false, location services not enabled")
            }
        }
    }
    
    func getUserLastLocation() {
        guard let user = Auth.auth().currentUser else { return }
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        geoFire.getLocationForKey("\(user.uid)") { (location, error) in
            if (error != nil) {
                print("An error occurred getting the location for \"firebase-hq\": \(String(describing: error?.localizedDescription))")
            } else if (location != nil) {
                print("Location for user is [\(String(describing: location?.coordinate.latitude)), \(String(describing: location?.coordinate.longitude))]")
                self.userLocation = CLLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                print("Geofire retrieved userlocation is,", self.userLocation!)
                self.observeKeysEntered()
                self.observeKeysReady()
            }
        }
    }
    
    func setLocationParameters() {
        print("1.setting location parameters")
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //~3
        locationManager.distanceFilter = 500 //The minimum distance (measured in meters) a device must move horizontally before an update event is generated. //~4
        locationManager.startUpdatingLocation() //~5
        //locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Will update location coordinates")
        if let lastLocation = locations.last {
            print("2. getting location coordinates")
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let time = lastLocation.timestamp
            
            self.userLongitude = String(describing: long)
            self.userLatitude = String(describing: lat)
            self.userTimeStamp = String(describing: time)
            
            self.userLocation = CLLocation(latitude: lat, longitude: long)
            print("user location has been set", self.userLocation!)
            self.updateUserLocationRecord(userNewLat: String(describing: lat), userNewLon: String(describing: long), userNewTimeStamp: String(describing: time))
        }
    }
    
    func observeIncomingChatRequests() {
        guard let user = Auth.auth().currentUser else { return }
        
        _refHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            guard let requestDictionary = snapshot.value as? [String:String] else { return }
            
            let requestSenderUID = requestDictionary["chatRequestSenderUID"]!
            
            let chatRequesttoUID = requestDictionary["chatRequesttoUID"]!
            
            if requestSenderUID == user.uid {
                self.selfInitiatedChatThreadRequestsDict[chatRequesttoUID] = snapshot.key
                self.chatPermissionsDictionary[chatRequesttoUID] = "false" //resolved? No. Should be requestRecipient? CHeck.
            } else {
                //add incoming request to chatPermissionsDictionary with default value as false
                self.chatPermissionsDictionary[requestSenderUID] = "false"
                
                //create array that links each chatUserRequester to the IDRequest they initiated
                self.uidtoChatRequestIDExtrapDict[requestSenderUID] = snapshot.key
                print("uidtoChatRequestIDExtrapDict now contains", self.uidtoChatRequestIDExtrapDict)
            }
            
            //Omit the case where request sender is self
            //guard requestSenderUID != user.uid else { return }
            
        })
    }
    
    //objective want collection view to observe requests that have already been made to changes in value, but only for requests that are not sent by self
    
    func observeSentRequestsChanges() {
        //note: these observers seem to keep observing even after current VC is exited (meaning observer keeps observing in background). Confirm this observation && decide if you need the observer to keep observing in background, otherwise remove the listener
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            print("snapshot is,", snapshot)
            
            guard let requestDictionary = snapshot.value as? [String:String] else { return }
            
            guard let requestSenderUID = requestDictionary["chatRequestSenderUID"] else { return }
            
            //we're only observing for changes in request status where we initiated requests, therefore we only account for cases where we are the requestSender
            //guard requestSenderUID == selfUID else { return } //WARNING!: THIS STATEMENT WAS COMMENTED OUT BECAUSE WHEN A USER WAS SELECTED, CODE WAS FAILING TO OBSERVE THE CHANGE IN REQUEST STATUS FOR USERS WHERE REQUEST SENDER WAS NOT SELF //ADD CONDITION LATER WHEN NEEDED THAT if requestSenderUID != self to omit case as needed.
            
            guard let chatRequestStatus = requestDictionary["chatRequestStatus"] else { return }
            //print result
            print("new chatRequestStatus is,", chatRequestStatus)
            
            let chatRequesttoUID = requestDictionary["chatRequesttoUID"]!
            
            //update result
            //test
            if requestSenderUID == user.uid {
                self.chatPermissionsDictionary[chatRequesttoUID] = chatRequestStatus //resolved?
            } else {
                self.chatPermissionsDictionary[requestSenderUID] = chatRequestStatus
            }
            
            //
            //self.chatPermissionsDictionary[requestSenderUID] = chatRequestStatus
            
            
            self.chatRequestStatusDictionary[chatRequesttoUID] = chatRequestStatus
            
            /*
             if chatRequestStatus == "true" {
             let uidApprovedChatRequest = requestDictionary["chatRequesttoUID"]!
             self.chatRequestsApprovedUIDArray.append(uidApprovedChatRequest)
             }*/
        })
    }
    
    
    /*
     func configureDatabase() { //~
     // TODO: configure database to sync messages
     let ref = Database.database().reference()
     
     guard let user = Auth.auth().currentUser else { return }
     _refHandle = ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded , with: { (snapshot: DataSnapshot) in
     //this refHandle will observe for all chatThreadIDs, whether created by self or by other.
     print("Snapshot key is,",snapshot.key)=
     let snapshotKeyString = snapshot.key
     self.newChatThreadIDsArray.append(snapshotKeyString)
     //how do we call observer again when chatThreadID is set to true?
     
     //get chatThreadID dictionary for each newChatThreadID belonging to this user //check if observer is firing again when test data changes
     ref.child("globalChatThreads/\(snapshotKeyString)").observeSingleEvent(of: .value, with: { (snapshot) in
     print("snapshot key is", snapshot.key)
     print("snapshot is", snapshot.value)
     let dataModel = snapshot.value as? [String: [String: AnyObject]]
     self.allChatThreadIDsDictionary["\(snapshotKeyString)"] = dataModel
     print("allChatThreadIDsDictionary is now", self.allChatThreadIDsDictionary)
     print("dataModel is", dataModel)
     guard let dataLevel1 = snapshot.value as? [String: AnyObject] else { return } //dictionary has chatMessageID as key, followed by Value. This Value is an array of dictionaries
     for key in dataLevel1.keys {
     print("messageID key is,", key)
     guard let baseDictionary = dataLevel1[key] as? [String: AnyObject] else { return }
     print("base Dictionary is", baseDictionary)
     guard let senderUID = baseDictionary["messageSenderUID"] as? String else { return }
     print("sender UID is", senderUID)
     self.uidtoChatThreadIDKeyIndexDictionary[senderUID] = snapshotKeyString
     print("uidtoChatThreadIDKeyIndexDictionary is now,", self.uidtoChatThreadIDKeyIndexDictionary)
     }
     
     //Test run1: Testing if when other creates a ChatThreadID, that self detects this chat threat ID and appropriately populates array of UID:ChatThreadIDKdy. Note, chatthreadIDKeywill only return true when user has actually sent a message. Result: chatThreadID is being created and observer
     
     //Therefore our dataStructure is [String : [String: [String: AnyObject]]]
     //overall structure is
     //self.userChatThreadObjectsArrayofDicts.append(messageObject)
     })
     
     /*self.participatingChatThreadArray.append(snapshot)
     print("Array is now", self.participatingChatThreadArray)
     //let messageObject = snapshot.value as! [String: String]
     //let senderUID = messageObject["messageSenderUID"]
     //print("someone with uid, \(senderUID!), just sent you a message")
     */
     })
     }
     */
    
    /*func buildChatThreadIDArray() {
     for chatThreadSnapshot in self.participatingChatThreadArray {
     let chatThreadDataObject = chatThreadSnapshot.value as! [String: String]
     for newChatThreadID in chatThreadDataObject.keys {
     self.chatThreadIDArray.append(newChatThreadID)
     }
     }
     print("chatThreadIDArray is,", self.chatThreadIDArray)
     } */
    
    func observeKeysEntered() {
        print("4.now observing keys entered")
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        //guard let center = myLocation else { return }
        if importedUserLocation != nil {
            self.center = importedUserLocation
        } else {
            self.center = userLocation
        }
        
        let circleQuery = geoFire.query(at: self.center!, withRadius: 10) //radius in km
        
        //Observe keys within radius
        circleQuery.observe(.keyEntered, with: { (key:String!, location:CLLocation!) in
            
            //calculate self's distance to retrieved user distance
            let locationAcross = self.center?.distance(from: location)
            
            //instantiate structure containing ID of user within proximity & add UID and distanceTo values to this structure
            var userInstance = usersinProximityStruct()
            userInstance.otherUserID = key
            userInstance.toOtherUserDistance = locationAcross!
            
            //build array of all users within proximity, where each array entry is a struct containing the UID and distanceTo vlaues of each instance
            
            self.uidinProximityRawArray.append(userInstance)
        })
    }
    
    func observeKeysReady() {
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        //let center = userLocation
        
        let circleQuery = geoFire.query(at: center!, withRadius: 10) //radius in km
        
        circleQuery.observeReady({
            self.sortUserinProximity()
            self.populateArrayofDictionaries()
        })
    }
    
    func sortUserinProximity() {
        
        let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
        
        //initiate first here: for i in 0..<100 in sortedArray
        for i in 0..<sortedStructArray.count {
            self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
        }
        
        self.buildPhotoURLStringArray()
        //print("Test 1: \(self.urlStringArray)")
        //self.populateArrayofDictionaries()
    }
    
    func buildPhotoURLStringArray() {
        
        for UID in self.sortedUIDinProximityArray {
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                if let imageStringURL = value?["full_URL"] as? String {
                    self.photoURLStringArray.append(imageStringURL)
                }
            })
        }
    }
    
    func populateArrayofDictionaries() {
        //check that our sortedUIDinProximityArray contains all UID values sorted by distance
        for UID in self.sortedUIDinProximityArray {
            print("getting value for UID", UID)
            //Get the datasnapshot for user
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else { return }
                guard let userDisplayName = value["userDisplayName"] as? String else { return }
                guard let userThumbnailURLString = value["full_URL"] as? String else { return }
                guard let userPrivacyPreference = value["privacy"] as? String else { return }
                //guard let userAge = value["userAge"] as? String else { return }
                
                var userGridThumbInfoDict = [String: String]()
                
                //Create key-value pairs for each user
                userGridThumbInfoDict["UID"] = snapshot.key
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                //dictionary["userAge"] = userAge
                userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
                
                self.userDictArray.append(userGridThumbInfoDict)
                print("User Array Dictionary now contains: \(self.userDictArray)")
                self.collectionView!.reloadSections([0])
            })
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    /*func numberOfSections(in collectionView: UICollectionView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.userDictArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        //Set cell corner radius
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.masksToBounds = true
        
        //let imageView = cell.profilePhotoThumbnailView!
        let imageView = cell.profilePhotoThumbnailView as UIImageView
        
        //~imageView.sd_setImage(with: URL(string: importedURLStringArray[indexPath.row]))
        
        //imageView!.sd_setImage(with: stringURLArray[indexPath.row], completed: nil)
        //imageView!.sd_setImage(with: urlArray[indexPath.row], completed: nil)
        
        
        //Dictionary code
        let user = self.userDictArray[(indexPath as NSIndexPath).row]
        
        imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
        //cell.userDisplayNameLabel.text = user["userDisplayName"]! + ", " + user["userAge"]!
        cell.userDisplayNameLabel.text = user["userDisplayName"]!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:IndexPath) {
        
        // Grab the DetailVC from Storyboard
        let userProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        
        //get selectedUser
        let selectedUser = userDictArray[(indexPath as NSIndexPath).row]
        let selectedUserUID = selectedUser["UID"]!
        
        //1. Set user privacy preference value: "true" or "false"
        self.selectedUserPrivacyPreference = selectedUser["userPrivacyPreference"]!
        print("CVC 1: selected user privacy pref is,", self.selectedUserPrivacyPreference)
        
        //2. Set existing chatThread id value
        if self.uidtoChatThreadIDKeyIndexDictionary.keys.contains(selectedUserUID) {
            print("Therefore chat request with this user was previously initiated by other")
            
            guard let existingChatThreadID = self.uidtoChatThreadIDKeyIndexDictionary[selectedUserUID] else { return }
            self.existingChatThreadID = existingChatThreadID
            print("CVC 2a1: existing chatThreadID initiated by other is,", self.existingChatThreadID)
            
        } else if self.selfInitiatedChatThreadsDictionary.keys.contains(selectedUserUID) {
            print("Therefore, chat request for this user was previously initiated by self")
            
            guard let existingChatThreadID = self.selfInitiatedChatThreadsDictionary[selectedUserUID] else { return }
            self.existingChatThreadID = existingChatThreadID
            print("CVC 2a2: existing chatThreadID initiated by self is,", self.existingChatThreadID)
            
        } else {
            print("CVC 2b: no preexisting chatThreadID detected, need to create new chatThreadID")
        }
        
        //check if there is a prexisting chat request for selected user whether from self or other
        
        //check if passed user has requested to chat with us or if we have requested passed user
        if self.chatPermissionsDictionary.keys.contains(selectedUserUID) {
            self.preexistingChatRequest = true
            print("CVC 3: preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = self.uidtoChatRequestIDExtrapDict[selectedUserUID]
            print("CVC 4: preexistingChatRequestKey exists,", self.preexistingChatRequestKey)
            
        } else if selfInitiatedChatThreadRequestsDict.keys.contains(selectedUserUID)  {
            self.preexistingChatRequest = true
            print("CVC 3: preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = self.selfInitiatedChatThreadRequestsDict[selectedUserUID]
            print("CVC 4: preexistingChatRequestKey exists,", self.preexistingChatRequestKey)
            
        } else {
            self.preexistingChatRequest = false
            print("CVC 3: no preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = nil
            print("CVC 4: no preexistingChatRequestKey exists")
            
            print("CVC 5: no request initiated")
        }
        
        //check to see if request initiated by self or other
        print("checking if request initiator is self or other")
        if selfInitiatedChatThreadRequestsDict.keys.contains(selectedUserUID) {
            print("chat Request initiated by self")
            self.requestInitiatedBySelf = true
            
        } else {
            print("chatr Request initiated by other")
            self.requestInitiatedBySelf = false
            
        }
        
        
        //
        /* if selfInitiatedChatThreadRequestsDict.keys.contains(selectedUserUID) {
         self.preexistingChatRequest = true
         print("CVC 3: preexisting chat request exists,", self.preexistingChatRequest)
         
         self.preexistingChatRequestKey = self.selfInitiatedChatThreadRequestsDict[selectedUserUID]
         print("CVC 4: preexistingChatRequestKey exists,", self.preexistingChatRequestKey)
         
         self.requestInitiatedBySelf = true
         print("CVC 5: request initiated by self")
         } else {
         self.preexistingChatRequest = false
         print("CVC 3: no preexisting chat request exists,", self.preexistingChatRequest)
         
         self.preexistingChatRequestKey = nil
         print("CVC 4: no preexistingChatRequestKey exists")
         
         print("CVC 5: no request initiated")
         } */
        
        //set selected user response state
        if let userResponseState = self.chatPermissionsDictionary[selectedUserUID] {
            print("CVC6: observed response state is,", userResponseState, "now setting self value")
            self.selectedUserResponseState = userResponseState
            print("CVC 6: user response state is,", self.selectedUserResponseState)
        } else {
            self.selectedUserResponseState = "false"
            print("CVC 6: response sate set to default false value")
        }
        
        print("Passing values:", "user is", userDictArray[(indexPath as NSIndexPath).row], "with privacy pref", self.selectedUserPrivacyPreference, "with existing ID?", self.existingChatThreadID, "with existing request?", self.preexistingChatRequest, "with request key?", self.preexistingChatRequestKey, "where initiated byt self? ", self.requestInitiatedBySelf, "where user response state", self.selectedUserResponseState)
        
        userProfileViewController.selectedUser = userDictArray[(indexPath as NSIndexPath).row]
        userProfileViewController.selectedUserPrivacyPreference = self.selectedUserPrivacyPreference
        userProfileViewController.existingChatThreadID = self.existingChatThreadID
        userProfileViewController.preexistingChatRequest = self.preexistingChatRequest
        userProfileViewController.preexistingChatRequestKey = self.preexistingChatRequestKey
        userProfileViewController.requestInitiatedBySelf = self.requestInitiatedBySelf
        userProfileViewController.selectedUserResponseState = self.selectedUserResponseState
        
        // Present the view controller using navigation
        self.present(userProfileViewController, animated: true, completion: nil)
    }
    
    func updateUserLocationRecord(userNewLat: String, userNewLon: String, userNewTimeStamp: String) {
        print("3. Now updating user location record")
        if let user = Auth.auth().currentUser {
            //update DB location coordinates
            print("updating user location in DB")
            self.ref.child("users/\(user.uid)/userLongitude").setValue(userNewLat)
            self.ref.child("users/\(user.uid)/userLatitude").setValue(userNewLon)
            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(userNewTimeStamp)
            
            //update geofire location
            let geoFireRef = Database.database().reference().child("userLocationGF")
            let geoFire = GeoFire(firebaseRef: geoFireRef)
            
            //set location of user in GeofireFolder in DB
            print("updating user location in Geofire folder")
            geoFire.setLocation(self.userLocation!, forKey: "\(user.uid)")
            print("Geofire location updated successfully")
            
            //error: These functions get called everytime location record is updated. We only want to call these functions once after the initial user location is retrieved, and then once after a signficant change in location has occured.
            //self.observeKeysEntered()
            //self.observeKeysReady()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    
}
// MARK: UICollectionViewDelegate

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
 
 }
 }
 */
 
 
 */
