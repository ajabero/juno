//
//  AuthTracker.swift
//  Ximo
//
//  Created by Asaad on 7/30/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import Firebase
import FirebaseAuth

class AuthTracker {

    var ref: DatabaseReference!
    var user: User?
    var authTerminateObserver: Any? = nil
    var aRef: TrustViewController!
    var pushID = ""
    
    var didCompleteUpdates_Security = false
    var didCompleteUpdates_Trust = false
    var didCompleteUpdates_Info = false
    var didCompleteUpdates_Photo = false
    var didCompleteUpdates_Location = false
    
    var phoneAuthSource = false //Set in AuthViewController.signInUser_Phone_Step2() -> After user signs up successfully with PHONE
    var emailAuthSource = false //Set in SecurityViewController.signUpNewUser() -> After user signs up successfully with EMAIL
    
    var didAttemptEmailAuth: String! //UserDefault in SecurityVC once E-Auth is complete
    //UserDefaults.standard.set("true", forKey: "didAttemptEmailAuth")
    //UserDefaults.standard.string(forKey: "didAttemptEmailAuth")
    
    var didSeeNoFacebookPrompt: String! //UserDefault in TrustInfoVC once "I Don't Have a Facebook Account" is observed
    //UserDefaults.standard.set("true", forKey: "didSeeNoFacebookPrompt")
    //ex: guard let didSeeNoFacebookPrompt = UserDefaults.standard.string(forKey: "didSeeNoFacebookPrompt") else { return }

    var didCompleteFacebookVerification: String! //Set Val in TrustVC if user did verify
    
    var didSkipFacebookVerification: String! //Set Val in TrustVC if user did SKIP FBAuth
    var willEnterInfoManually: String! //Set Val in TrustVC if user did Select Enter Manually
    
    /*
    func firebaseConnect() {
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                print("Connected")
            } else {
                print("Not connected")
            }
        })
    }
    */
    
    //MARK: Batch-Updates Initializers
    
    var tryUpTo = 3 // -> two trials max
    
    //1. Init Security

    var selfUID = ""
    var withEmail = ""
    var withTermsBool = ""
    
    var batchUpdatesCounter_Security = 1
    
    func performBatchUpdates_Security(withEmail: String, withTermsBool: String) {
        
        self.withEmail = withEmail
        self.withTermsBool = withTermsBool
        self.selfUID = Auth.auth().currentUser!.uid
        
        self.ref = Database.database().reference()
        
        var childUpdates = [
            "/users/\(selfUID)/userEmail": withEmail,
            "/users/\(selfUID)/didAcceptTerms": withTermsBool,
            "/users/\(selfUID)/privacy": "false",
        ]
        
        //
        if self.pushID != "" {
            childUpdates = [
                "/users/\(selfUID)/userEmail": withEmail,
                "/users/\(selfUID)/didAcceptTerms": withTermsBool,
                "/users/\(selfUID)/privacy": "false",
                "/users/\(selfUID)/pushID": self.pushID,
                "/messageNotifications/\(self.pushID)/recipientID": selfUID,
            ]
        }
        
        /*
          manually test that cloud functions will trigger effectively on .write & then perform plumbing for paths
            */
        
        
        //
        ref.updateChildValues(childUpdates) { (error, forReference_Security) in
            if error != nil {
                //x3 Max Re-trys
                if self.batchUpdatesCounter_Security < self.tryUpTo {
                    self.batchUpdatesCounter_Security = self.batchUpdatesCounter_Security + 1
                    self.didCompleteUpdates_Security = false
                    self.performBatchUpdates_Security(withEmail: self.withEmail, withTermsBool: self.withTermsBool)
                } else {
                    //After x4 tries -> perform final push in HOLD controller
                    self.didCompleteUpdates_Security = false
                }
                
            } else {
    
                self.didCompleteUpdates_Security = true
            
                if self.throttle_Security == true {
                    //
                    self.throttleBatchUpdates_PhotoVC()
                }
            }
        }
        
        
        
    }
    
    //2. Init Trust

    var withName = ""
    var withAgeRange = "18"
    var didAuthFB = ""
    var didUnlinkFB = ""
    var didResolveUnlinkFB = ""
    var batchUpdatesCounter_Trust = 1
    
    func performBatchUpdates_Trust(withName: String, withAgeRange: String, didAuthFB: String, didUnlinkFB: String, didResolveUnlinkFB: String) {
        
        self.ref = Database.database().reference()
        
        self.withName = withName
        self.withAgeRange = withAgeRange
        self.didAuthFB = didAuthFB
        self.didUnlinkFB = didUnlinkFB
        self.didResolveUnlinkFB = didResolveUnlinkFB
        
        let childUpdates = [
            "/users/\(selfUID)/userDisplayName": withName,
            "/users/\(selfUID)/userAgeRange": withAgeRange,
            "/users/\(selfUID)/didAuthFB": didAuthFB,
            "/users/\(selfUID)/didUnlinkFB": didUnlinkFB,
            "/users/\(selfUID)/didResolveUnlinkFB": didResolveUnlinkFB,
            ]
        
        ref.updateChildValues(childUpdates) { (error, forReference_Trust) in
            if error != nil {
                //x3 Max Re-trys
                if self.batchUpdatesCounter_Trust < self.tryUpTo {
                    self.batchUpdatesCounter_Trust = self.batchUpdatesCounter_Trust + 1
                    self.didCompleteUpdates_Trust = false
                    self.performBatchUpdates_Trust(withName: self.withName, withAgeRange: self.withAgeRange, didAuthFB: self.didAuthFB, didUnlinkFB: self.didUnlinkFB, didResolveUnlinkFB: self.didResolveUnlinkFB)
                } else {
                    //After x4 tries -> perform final push in HOLD controller
                    self.didCompleteUpdates_Trust = false
                }
                
            } else {
                
                self.didCompleteUpdates_Trust = true
                
                if self.throttle_Trust == true {
                    //
                    self.throttleBatchUpdates_PhotoVC()
                }
            }
        }
    }
    
    //3. Init Info

    var withAge = ""
    var withDOB = ""
    var withMinAge = ""
    var withMaxAge = ""

    var batchUpdatesCounter_Info = 1
    
    func performBatchUpdates_Info(withAge: String, withDOB: String, withMinAge: String, withMaxAge: String) {
        
        //3. My Info VC - HOLD: [ ]. PASS: [1, 2, 3].
        self.withAge = withAge
        self.withDOB = withDOB
        self.withMinAge = withMinAge
        self.withMaxAge = withMaxAge
        
        //
        let childUpdates = [
            "/users/\(selfUID)/userAge": withAge,
            "/users/\(selfUID)/dateOfBirth": withDOB,
            "/users/\(selfUID)/showMeMinAge": withMinAge,
            "/users/\(selfUID)/showMeMaxAge": withMaxAge,
            ]
        
        ref.updateChildValues(childUpdates) { (error, forReference_Info) in
            if error != nil {
                //x3 Max Re-trys
                if self.batchUpdatesCounter_Info < self.tryUpTo {
                    self.batchUpdatesCounter_Info = self.batchUpdatesCounter_Info + 1
                    self.didCompleteUpdates_Info = false
                    self.performBatchUpdates_Info(withAge: self.withAge, withDOB: self.withDOB, withMinAge: self.withMinAge, withMaxAge: self.withMaxAge)
                } else {
                    //After x4 tries -> perform final push in HOLD controller
                    self.didCompleteUpdates_Info = false
                }
                
            } else {
                
                self.didCompleteUpdates_Info = true
                
                if self.throttle_Info == true {
                    //
                    self.throttleBatchUpdates_PhotoVC()
                }
                
            }
        }

    }
    
    //4. Init Photo
    
    func performBatchUpdates_Photo() {
        //
    }
    
    //4. Throttle Updates
    
    var throttle_Security = false
    var throttle_Trust = false
    var throttle_Info = false
    
    var exitThrottle = false
    
    //Note: ThrottleUpdates will observe all batchUpdates evaluators - if any have failed to complete successfully, throttler will attempt to call failed batchUpdater upto x2. If all throttleCalls fail after x2 trials, throttleExits function && performs segue on incomplete records gracefully.
    
    func throttleBatchUpdates_PhotoVC() {
        
        self.tryUpTo = 3
        
        //Exception: throttles fail, exit throttle
        if throttle_Security == true && throttle_Trust == true && throttle_Info == true && self.didCompleteUpdates_Security == false && self.didCompleteUpdates_Trust == false && self.didCompleteUpdates_Info == false {
            
            //EXIT THROTTLE && POST SEGUE
            self.exitThrottle = true
            
            //Post notification
            NotificationCenter.default.post(name: Notification.Name(rawValue: "authTrackerComplete"), object: self)

            return
        }
        
        //START
        if didCompleteUpdates_Security == true {
            
        } else {
            //reset counter
            self.throttle_Security = true
            self.batchUpdatesCounter_Security = 1
            self.performBatchUpdates_Security(withEmail: self.withEmail, withTermsBool: self.withTermsBool)
        }
        
        //
        if didCompleteUpdates_Trust == true {
            
        } else {
            self.throttle_Trust = true

            self.batchUpdatesCounter_Trust = 1
            self.performBatchUpdates_Trust(withName: self.withName, withAgeRange: self.withAgeRange, didAuthFB: self.didAuthFB, didUnlinkFB: self.didUnlinkFB, didResolveUnlinkFB: self.didResolveUnlinkFB)
        }
        
        //
        if didCompleteUpdates_Info == true {
            
        } else {
            self.throttle_Info = true

            self.batchUpdatesCounter_Info = 1
            self.performBatchUpdates_Info(withAge: self.withAge, withDOB: self.withDOB, withMinAge: self.withMinAge, withMaxAge: self.withMaxAge)
        }
        
        guard didCompleteUpdates_Security == true else { return }
        guard didCompleteUpdates_Trust == true else { return }
        guard didCompleteUpdates_Info == true else { return }
        
        //POST SUCCESS TO PHOTO VC && CAN NOW SEGUE
        NotificationCenter.default.post(name: Notification.Name(rawValue: "authTrackerComplete"), object: self)
    }
    
    //4. Init Location
    func performBatchUpdates_Location() {
        
    }
}
