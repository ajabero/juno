//
//  TOAViewController.swift
//  Ximo
//
//  Created by Asaad on 6/2/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import FirebaseAuth

class TOAViewController: UIViewController, AuthUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addNavSpaceLayer()
        self.addBackButtonView()
        self.makeRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
     
    }
    
    @IBOutlet weak var webView: UIWebView!
    
    var withPrivacyPolicy = true
    
    func makeRequest() {
        if self.withPrivacyPolicy == true {
            let url = URL(string: "https://app.termly.io/document/privacy-policy-for-mobile-app/3934f580-5ed9-4257-9b07-f036affc430b")
            webView.loadRequest(URLRequest(url: url!))
        } else {
            let url = URL(string: "https://app.termly.io/document/terms-of-use-for-website/50cd2456-a2c2-4d0e-8ccb-2551bd40168c")
            webView.loadRequest(URLRequest(url: url!))
        }
    }
    
    var backButton: UIButton!
    
    func addBackButtonView() {
        let sWidth = UIScreen.main.bounds.width
        let sHeight = UIScreen.main.bounds.height
        var sBarH = UIApplication.shared.statusBarFrame.size.height
        
        let leftInsetRatio = 38 / 414 as CGFloat
        let topInsetRatio = 22 / 736 as CGFloat
        sBarH = 20 as CGFloat
        
        var topInset = (topInsetRatio * sHeight) + sBarH
        
        if sHeight > 750 {
            sBarH = 44 as CGFloat
            topInset = (topInsetRatio * 736) + sBarH
        }
        let leftInset = leftInsetRatio * sWidth
        
        let xPos = leftInset
        let yPos = topInset
        let w = 32 as CGFloat
        let h = 32 as CGFloat
        
        let frame = CGRect(x: xPos, y: yPos, width: w, height: h)
        let frameView = UIButton(frame: frame)
        //        frameView.setImage(UIImage(named: "infoBackButton")!, for: .normal)
        frameView.setImage(UIImage(named: "cancelInfo")!, for: .normal)
        
        frameView.contentMode = .center
        frameView.backgroundColor = UIColor.clear
        
        self.view.addSubview(frameView)
        self.backButton = frameView
        
        backButton.addTarget(self, action: #selector(TrustInfoViewController.goBack(_:)), for: .touchUpInside)
        backButton.isUserInteractionEnabled = true
    }
    
    
    var navSpaceLayer: UIView?
    var navEdge: UIView?
    
    func addNavSpaceLayer() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let navX = 0 as CGFloat
        let navY = 0 as CGFloat
        let navW = sWidth
        let navH = navSpaceHeight
        
        let navF = CGRect(x: navX, y: navY, width: navW, height: navH)
        let navView = UIView(frame: navF)
        navView.backgroundColor = UIColor.white
        self.navSpaceLayer = navView
        
        self.view!.addSubview(navView)
        
        //& Line View
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.navSpaceLayer!.frame.maxY
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
//        self.view!.addSubview(lineView)
//        self.navEdge = lineView
    }
   
    @objc func goBack(_ sender: UIButton) {        
        self.performSegue(withIdentifier: "unwindTermsToAuth", sender: self)
    }
}
