//
//  VoilaViewController.swift
//  Juno
//
//  Created by Asaad on 3/14/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase

class VoilaViewController: UIViewController, UIGestureRecognizerDelegate {

    var selfVisus: String?
    var otherVisus: String?
    var presentingSegueIdentifier = ""
    var freezeFrame: UIImageView?
    var arrestoImago: UIImage?
    var selfWasDismissed: Bool?
    
    var ref: DatabaseReference!
    
    var profileChangesUIDsInProximity = [String]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.ref = Database.database().reference()
    
        self.drawVoilaImageView()
        self.addNameBannerLabel()
        self.addAccessDescriptorLabel()
        self.addDismissChatButton()
        self.addKeepTalkingButton()
        
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when) {
            //arresto momento should be called when modalController touches top of view
            self.arrestoMomento()
        }

//        UserDefaults.standard.set("false", forKey: "didPresentNotInterestedAlert")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addDismissAlert()
    }

    var voilaImage: UIImage?
    var userDisplayName: String?
    
    //
    var voilaImageView: UIImageView?
    var nameLabelBanner: UILabel?
    var accessDescriptorLabel: UILabel?
//    var keepTalkingButton: UIImageView?
//    var dismissChatButton: UIImageView?
    var dismissChatButton: UIButton?
    var keepTalkingButton: UIButton?

    func applicationProtectedDataWillBecomeUnavailable(_ application: UIApplication) {
        //print("VoilaVC applicationProtectedDataWillBecomeUnavailable")
    }
    
    func applicationProtectedDataDidBecomeAvailable(_ application: UIApplication) {
        //print("VoilaVC applicationProtectedDataDidBecomeAvailable")
    }
    
    func drawVoilaImageView() {
        //
        let widthRatio = 295 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let vWidth = widthRatio * sWidth
        let vHeight = vWidth
        let vX = ((sWidth - vWidth) * 0.5) as CGFloat
        let topImageInset = (gridUnitY * sHeight) * 5 as CGFloat
        let vY = topImageInset
        
        //
        let vFrame = CGRect(x: vX, y: vY, width: vWidth, height: vHeight)
        let voilaImageView = UIImageView(frame: vFrame)
        voilaImageView.contentMode = .scaleAspectFill
        voilaImageView.layer.cornerRadius = vWidth * 0.5
        voilaImageView.layer.masksToBounds = true
        voilaImageView.layer.borderWidth = 3
        voilaImageView.layer.borderColor = UIColor.white.cgColor
        
        //
        self.view.addSubview(voilaImageView)
        self.voilaImageView = voilaImageView
        
        //
        if let voilaImage = self.voilaImage {
            self.voilaImageView!.image = voilaImage
        }
    }
    
    func arrestoMomento() {
        //print("VoilaVC arrestoMomento")
        
        let frameView = UIImageView(frame: UIScreen.main.bounds)
        frameView.image = self.arrestoImago!
        frameView.alpha = 0
        self.view.addSubview(frameView)
        self.freezeFrame = frameView

        self.view?.sendSubview(toBack: self.freezeFrame!)
        
        self.freezeFrame?.alpha = 1
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.freezeFrame?.removeFromSuperview()
    }
    
    func addNameBannerLabel() {
        
        //
        let widthRatio = 295 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let nLabelX = 0 as CGFloat
        let nLabelY = self.voilaImageView!.frame.maxY + gridUnitHeight
        let nLabelW = sWidth as CGFloat
        let nLabelH = 50 as CGFloat
        
        var fontSize = 40 as CGFloat
        fontSize = self.downSizeFont(fromSize: fontSize)
        
        let nameLabelFrame = CGRect(x: nLabelX, y: nLabelY, width: nLabelW, height: nLabelH)
        let nameLabel = UILabel(frame: nameLabelFrame)
        nameLabel.font = UIFont(name: "Avenir Next", size: 40)
        nameLabel.textColor = UIColor.white
        nameLabel.text = "This is " + self.userDisplayName! + "!"
//        nameLabel.text = "Harry"

        nameLabel.textAlignment = .center
        
        self.view.addSubview(nameLabel)
        self.nameLabelBanner = nameLabel
    }
    
    func addAccessDescriptorLabel() {
        //
        let widthRatio = 295 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let dLabelW = (344 / 414) * sWidth as CGFloat
        let dLabelH = 23 as CGFloat
        let dLabelX = (sWidth - dLabelW) * 0.5
        let dLabelY = self.nameLabelBanner!.frame.maxY
        
        let nameLabelFrame = CGRect(x: dLabelX, y: dLabelY, width: dLabelW, height: dLabelH)
        let descriptorLabel = UILabel(frame: nameLabelFrame)
        descriptorLabel.textColor = UIColor.white
        
        //
        var fontSize = 18 as CGFloat
        fontSize = self.downSizeFont(fromSize: fontSize)
        
        //print("!A finalFontSize", fontSize)
        
        if self.selfVisus! == "false" && self.otherVisus! == "false" {
            
            descriptorLabel.font = UIFont(name: "Avenir Next", size: 16)

            if self.selfWasDismissed != true {
                descriptorLabel.text = "You both have access to each other's full profile"
            } else {
                descriptorLabel.text = "Swap Complete"
            }
            
        } else {
            descriptorLabel.text = "You now have access to his full profile"
            descriptorLabel.font = UIFont(name: "Avenir Next", size: fontSize)
        }
        
        descriptorLabel.adjustsFontSizeToFitWidth = true
        descriptorLabel.textAlignment = .center

        self.view.addSubview(descriptorLabel)
        self.accessDescriptorLabel = descriptorLabel
    }
    
    var orangeFill = UIColor(displayP3Red: 255/255, green: 78/255, blue: 0, alpha: 1)
    
    func downSizeFont(fromSize: CGFloat ) -> CGFloat {
        let sHeight = UIScreen.main.bounds.height
        var sizeFactor = 375 / 414 as CGFloat
        let toSize: CGFloat?
        
        if sHeight < 700 as CGFloat {
            toSize = CGFloat(floor(sizeFactor * fromSize))
            return toSize!
        } else {
            return fromSize
        }
    }
    
    func addDismissChatButton() {
        //
        let widthRatio = 295 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //init properties
        let bW = (155 / 414) * sWidth as CGFloat
        let bHWRatio = 52 / 155 as CGFloat
        let bH = bHWRatio * bW
        let bX = ((sWidth - bW) * 0.5) as CGFloat
        //        let bY = self.keepTalkingButton!.frame.maxY + (gridUnitHeight * 0.75)
//        let bY = self.accessDescriptorLabel!.frame.maxY + (gridUnitHeight * 3)
//        let bY = (6 * superInsetHeight) + gridUnitHeight
        let bY = sHeight - (2 * superInsetHeight) + gridUnitHeight

        //init frame & view
        let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
        
        //INIT AS BUTTON
        let dismissChatButton = UIButton(frame: bFrame)

        dismissChatButton.backgroundColor = self.orangeFill
        dismissChatButton.layer.cornerRadius = bH / 2
        dismissChatButton.layer.masksToBounds = true
        dismissChatButton.titleLabel?.adjustsFontSizeToFitWidth = true
        
        if self.selfWasDismissed != true {
            dismissChatButton.setTitle("Not Interested", for: .normal)
        } else {
            dismissChatButton.setTitle("End Chat", for: .normal)
        }
        
        var fontSize = 18 as CGFloat
        fontSize = self.downSizeFont(fromSize: fontSize)
        
        dismissChatButton.titleLabel!.font = UIFont(name: "Avenir-Light", size: fontSize)!
        dismissChatButton.setTitleColor(UIColor.white, for: .normal)

        if self.selfWasDismissed != true {
            dismissChatButton.addTarget(self, action: #selector(VoilaViewController.dismissChat(_:)), for: .touchUpInside)
        } else {
            dismissChatButton.addTarget(self, action: #selector(VoilaViewController.endChat(_:)), for: .touchUpInside)
        }
        
        dismissChatButton.isUserInteractionEnabled = true
        
        self.view.addSubview(dismissChatButton)
        self.dismissChatButton = dismissChatButton
        
        //INIT AS IMAGE
        //        let dismissChatButton = UIImageView(frame: bFrame)
        //
        //        //init view properties
        ////        dismissChatButton.image = UIImage(named: "dismissUserButton")
        //        dismissChatButton.image = UIImage(named: "voilaGoBack")
        //        dismissChatButton.contentMode = .scaleAspectFit
        //        dismissChatButton.clipsToBounds = false
        //
        //        //add to subview
        //        self.view.addSubview(dismissChatButton)
        //        self.dismissChatButton = dismissChatButton
        
        //        //add gesture recognizer
        //        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(VoilaViewController.dismissChat(_:)))
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(VoilaViewController.goBack(_:)))
        //        tapGesture.delegate = self
        //        tapGesture.numberOfTapsRequired = 1
        //        tapGesture.numberOfTouchesRequired = 1
        //        self.dismissChatButton!.addGestureRecognizer(tapGesture)
        //        self.dismissChatButton!.isUserInteractionEnabled = true
    }
    
    //button Proprs
//    let fontName = "Avenir-Light"
//    var fontSize = 18.0 as CGFloat
//
    func addKeepTalkingButton() {
        //
        let widthRatio = 295 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //init properties
        let bW = (155 / 414) * sWidth as CGFloat
        let bHWRatio = 52 / 155 as CGFloat
        let bH = bHWRatio * bW
        let bX = ((sWidth - bW) * 0.5) as CGFloat
        let bY = self.dismissChatButton!.frame.maxY + (gridUnitHeight * 0.75)

        //init frame & view
        let bFrame = CGRect(x: bX, y: bY, width: bW, height: bH)
//        let keepTalkingButton = UIImageView(frame: bFrame)
//
//        //init view properties
//        keepTalkingButton.image = UIImage(named: "keepTalkingButton")
//        keepTalkingButton.contentMode = .scaleAspectFit
//        keepTalkingButton.clipsToBounds = false
//
//        //add to subview
//        self.view.addSubview(keepTalkingButton)
//        self.keepTalkingButton = keepTalkingButton
//
//        //add gesture recognizer
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(VoilaViewController.keepTalking(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        self.keepTalkingButton!.addGestureRecognizer(tapGesture)
//        self.keepTalkingButton!.isUserInteractionEnabled = true
        
        //INIT AS BUTTON
        let keepTalkingButton = UIButton(frame: bFrame)
        let borderColor = UIColor(displayP3Red: 149/255, green: 152/255, blue: 154/255, alpha: 1)
        
        keepTalkingButton.backgroundColor = UIColor.clear
        keepTalkingButton.layer.cornerRadius = bH / 2
        keepTalkingButton.layer.masksToBounds = true
        keepTalkingButton.layer.borderWidth = 1
        keepTalkingButton.layer.borderColor = borderColor.cgColor

        if self.selfWasDismissed != true {
            keepTalkingButton.setTitle("Keep Talking", for: .normal)
        } else {
            keepTalkingButton.setTitle("Go Back", for: .normal)
        }
        
        var fontSize = 18 as CGFloat
        fontSize = self.downSizeFont(fromSize: fontSize)
        
        keepTalkingButton.titleLabel!.font = UIFont(name: "Avenir-Light", size: fontSize)!
        keepTalkingButton.setTitleColor(UIColor.white, for: .normal)
        
        if self.selfWasDismissed != true {
            keepTalkingButton.addTarget(self, action: #selector(VoilaViewController.keepTalking(_:)), for: .touchUpInside)
        } else {
            keepTalkingButton.addTarget(self, action: #selector(VoilaViewController.goBack(_:)), for: .touchUpInside)
        }
        
        self.view.addSubview(keepTalkingButton)
        self.keepTalkingButton = keepTalkingButton
    }
    
    @objc func keepTalking(_ sender: UITapGestureRecognizer) {
        //print("keepTalking tap detected")
        self.freezeFrame?.removeFromSuperview()
        self.performSegue(withIdentifier: "unwindToChat", sender: self)
    }

    var userWasDismissed: Bool?
    
    @objc func dismissChat(_ sender: UITapGestureRecognizer) {
        //print("dismissChat detected")
        
        if let didPresentNotInterestedAlert = UserDefaults.standard.string(forKey: "didPresentNotInterestedAlert") {
            if didPresentNotInterestedAlert == "true" {
                self.dismissChatConfirmed()
            } else {
                self.present(self.alert, animated: true, completion: nil)
            }
        } else {
            self.present(self.alert, animated: true, completion: nil)
        }
        
        UserDefaults.standard.set("true", forKey: "didPresentNotInterestedAlert")
        //alert calls self.dismissChatConfirmed()
    }
    
    @objc func endChat(_ sender: UITapGestureRecognizer) {
        //print("endChat detected")
        self.dismissChatConfirmed()
    }
    
    func dismissChatConfirmed() {
        //good
        if self.presentingSegueIdentifier == "profileVCtochatScreenVC" {
            self.presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        } else {
            self.performSegue(withIdentifier: "unwindToChat", sender: self)
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            //print("dismissChat 2 presentingViewController is,", presentingViewController)
        }
        
        //voilaVC now handling userBlockage
//        self.blockUser()
        self.sendNotInterestedNotification()
        self.dismissUserDB()
        
        //make sure csvc still handles blocking properly after unwind from source vc
        self.userWasDismissed = true
        
        //
        if let othUID = self.otherUID {
            let userInfoDict = ["outgoingBlockUID" : othUID]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "outgoingBlockObserved"), object: self, userInfo: userInfoDict)
        }
     
        //post notification
        //        NotificationCenter.default.post(name: Notification.Name(rawValue: "selfDidDismissChat"), object: self)
        
        //unwinds to chat and lets csvc handle block unwind
        //        self.performSegue(withIdentifier: "unwindToChat", sender: self)
        
        //let the CSVC handle blocking user through notificaiton center
        //        self.blockUser()
        
    }
    
    func dismissUserDB() {
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
        //I. perform self block as usual
        self.ref.child("users/\(selfUID)/blockedUsers/\(otherUID)").setValue("true")
        
        if let threadID = self.absExistingMessageThreadID {
            if threadID != "" {
                //            self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(threadID)/chatIsActive").setValue("nil")
                //            self.ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
            }
        }

        
        if let swapID = self.absExistingSwapRequestID {
            self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive").setValue("nil")
//            self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//            self.ref.child("globalSwapRequests/\(otherUID)/\(swapID)").removeValue()
        }
        
        //II. add dismissedBy node to other node & read as block value did
        self.ref.child("users/\(otherUID)/dismissedBy/\(selfUID)").setValue("true")
    }
    
    func sendNotInterestedNotification() {
        let selfUID = self.selfUID!
        let swapData = ["chatMessage" : "swap_Nil_\(selfUID)"]
        self.sendMessage(data: swapData)
    }
    
    func sendMessage(data: [String : String]) {
        
        guard let chatThreadIDKey = self.absExistingMessageThreadID else { return }
        //print("key to use is,", chatThreadIDKey)
        
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = self.otherUID!
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
        let key = ref.child("globalChatThreads").childByAutoId().key
        let messageKey = ref.child("globalChatThreads/\(chatThreadIDKey)").childByAutoId().key //replaced chatThreadIDKey with keyToUse
        
        let data = data
        var mdata = data
        let timeStamp = String((Double(Date.timeIntervalSinceReferenceDate * 1000)))
        
        mdata["messageSenderUID"] = selfUID
        //fix the below timestamp later to get something actually readable
        mdata["messageTimeStamp"] = timeStamp
        mdata["messageRecipientUID"] = otherUID
        mdata["\(selfUID)_didDelete"] = "false"
        
        let childUpdates = [
            "/globalChatThreads/\(chatThreadIDKey)/\(messageKey)": mdata,
            ]
        ref.updateChildValues(childUpdates)
        
        ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)").setValue("true") //replaced
        ref.child("users/\(otherUID)/myActiveChatThreads/\(chatThreadIDKey)/\(chatThreadIDKey)").setValue("true") //replaced
    }
    
    @objc func goBack(_ sender: UITapGestureRecognizer) {
        //print("goBack detected")
        self.freezeFrame?.removeFromSuperview()
        self.performSegue(withIdentifier: "unwindToChat", sender: self)
    }
    
    var selfUID: String?
    var otherUID: String?
    
    var absExistingMessageThreadID: String?
    var absExistingSwapRequestID: String?
    
    func blockUser() {
        //print("blockUser press")
        
        let selfUID = self.selfUID!
        let otherUID = self.otherUID!
        
//        self.ref.child("users/\(selfUID)/blockedUsers/\(otherUID)").setValue("true")
//        self.ref.child("users/\(otherUID)/blockedBy/\(selfUID)").setValue("true")
        
        let metaUpdates = [
            "users/\(selfUID)/blockedUsers/\(otherUID)" : "true",
            "users/\(otherUID)/blockedBy/\(selfUID)" : "true",
        ]
        
        self.ref.updateChildValues(metaUpdates)

        
        //need to remove chat id
        if let chatID = self.absExistingMessageThreadID {
//            self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatID)").removeValue()
//            self.ref.child("users/\(otherUID)/myActiveChatThreads/\(chatID)").removeValue()
            
            let blockChatUpdates = [
                "users/\(selfUID)/myActiveChatThreads/\(chatID)/chatIsActive" : "nil",
                "users/\(otherUID)/myActiveChatThreads/\(chatID)/chatIsActive" : "nil",
                ]
            
            self.ref.updateChildValues(blockChatUpdates)
            
        }
        
        //need to remove swap id
        if let swapID = self.absExistingSwapRequestID {
//            self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//            self.ref.child("globalSwapRequests/\(otherUID)/\(swapID)").removeValue()
            
            let blockSwapUpdates = [
                "globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive" : "nil",
                "globalSwapRequests/\(otherUID)/\(swapID)/swapIsActive" : "nil",
                ]
            
            self.ref.updateChildValues(blockSwapUpdates)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //print("VOILAVC IN prepareFORSENDER METHOD")
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "backFromVoila"), object: self)

        let controller = segue.destination as! ChatScreenViewController
        controller.sourceVC = "voilaVC"
        
        if self.userWasDismissed == true {
            controller.userWasDismissed = true
            controller.didBlockUser = true
        }
    }
    
    var logoView: UIImageView!
    
    func animateLogo() {
        self.addWhiteBackground()
        self.addXimoLogo()
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .transitionCrossDissolve, animations: {
            self.whiteBack.alpha = 1
            
            let when = DispatchTime.now() + 0.3
            DispatchQueue.main.asyncAfter(deadline: when) {
                
                //                self.logoView.alpha = 1
                
                let fadeOut = CABasicAnimation(keyPath: "opacity")
                fadeOut.fromValue = 0
                fadeOut.toValue = 1
                fadeOut.duration = 0.25
                
                let expandScale = CABasicAnimation()
                expandScale.keyPath = "transform"
                expandScale.valueFunction = CAValueFunction(name: kCAValueFunctionScale)
                expandScale.fromValue = [0.3, 0.3, 0.3]
                expandScale.toValue = [1, 1, 1]
                expandScale.duration = 0.25
                
                let fadeAndScale = CAAnimationGroup()
                fadeAndScale.animations = [fadeOut, expandScale]
                fadeAndScale.duration = 0.25
                
                self.logoView.layer.add(fadeAndScale, forKey: "logoAnim")
                
                let when = DispatchTime.now() + 0.2
                
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.logoView!.alpha = 1
                }
            }
            
        }) { (true) in
            //print("chatScreenWillRmvView")
        }
    }
    
    var whiteBack: UIView!
    
    func addWhiteBackground() {
        
        let whiteRect = UIView(frame: UIScreen.main.bounds)
        whiteRect.backgroundColor = UIColor.white
        whiteRect.alpha = 0
        
        self.view.addSubview(whiteRect)
        self.whiteBack = whiteRect
    }
    
    func addXimoLogo() {
        
        //print("drawLogoView")
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //proprotions
        let wR = 113 / sWidth
        let wHR = (27 / 113) as CGFloat
        
        //frame props
        let w = wR * sWidth
        let wH = w * wHR
        let wX = (sWidth - w) * 0.5
        let wY = (sHeight * 0.5) - wH
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let logoView = UIImageView(frame: f)
        logoView.image = UIImage(named: "logoVector")!
        logoView.alpha = 0
        
        //print("MAINVIEW WILLADD LOGO SUBVIEW")
        self.view.addSubview(logoView)
        self.logoView = logoView
    }
    
    var alert: UIAlertController!
    //
    
    func addDismissAlert() {
        let alertTitle = "Not Interested?"
        let alertMessage = "They'll no longer be able to contact you or see your profile."
//        let alertMessage = "This person will no longer be able to contact you or see you after Swap is complete."
//        let alertMessage = "This person will no longer be able to contact you or see your photo after they unlock your photo."
//        let alertMessage = "This person will no longer be able to message you, and your profile will become hidden again after Swap is complete."

        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.alert!.dismiss(animated: true, completion: nil)
        }
        
        let affirmAction = UIAlertAction(title: "Not Interested", style: .default) { (action) in
            self.alert!.dismiss(animated: true, completion: nil)
            self.dismissChatConfirmed()
            UserDefaults.standard.set("true", forKey: "didPresentNotInterestedAlert")
        }
        
        alert.addAction(cancelAction)
        alert.addAction(affirmAction)
        
        self.alert = alert
    }
    
    
//    
//    func addVeriCopy() {
//        //
//        let widthRatio = 322 / 414 as CGFloat
//        let gridUnitX = 45.5 / 414 as CGFloat
//        let gridUnitY = 22.5 / 736 as CGFloat
//
//        //
//        let screen = UIScreen.main.bounds
//        let sWidth = screen.width
//        let sHeight = screen.height
//        let sBarH = UIApplication.shared.statusBarFrame.size.height
//
//        //
//        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
//        let gridUnitWidth = gridUnitX * sWidth
//        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
//        let superInsetHeight = 4 * gridUnitHeight
//
//        var tFontSize = 12.0 as CGFloat
//        var tWidth = 330 as CGFloat
//        if sWidth > 350 {
//            tWidth = 330 as CGFloat
//            tFontSize = 12.0 as CGFloat
//        } else {
//            tWidth = 278 as CGFloat
//            tFontSize = 10.0 as CGFloat
//        }
//
//        let veriCopy = "To ensure that users only communicate with trusted profiles," + "\n" + "please use the authentication method below to" + "\n" + "verify your information."
//        let textFieldCGSize = self.sizeOfCopyLarge(string: veriCopy, constrainedToWidth: Double(tWidth))
//        let tH = ceil(textFieldCGSize.height)
//        let tX = (sWidth - tWidth) * 0.5
//        let tY = superInsetHeight +  (1.5 * gridUnitHeight)
//
//        //
//        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
//        let tView = UITextView(frame: tFrame)
//        tView.delegate = self
//        tView.font = UIFont(name: "Avenir-Light", size: tFontSize)
//
//        //configure text
//
//        let tBold = "verify your information."
//        let tReg1 = "To ensure that users only communicate with trusted profiles,"
//        let tReg2 = "please use the authentication method below to"
//        let range = (veriCopy as NSString).range(of: tBold)
//        let r1 = (veriCopy as NSString).range(of: tReg1)
//        let r2 = (veriCopy as NSString).range(of: tReg2)
//
//        let attributedString = NSMutableAttributedString(string: veriCopy)
//
//        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!, range: range)
//        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r1)
//        attributedString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Avenir-Light", size: tFontSize)!, range: r2)
//
//        tView.attributedText = attributedString
//
//        //
//        self.view.addSubview(tView)
//
//        self.veriCopyTextView = tView
//        self.veriCopyTextView!.textAlignment = .center
//        self.veriCopyTextView!.isEditable = false
//        self.veriCopyTextView!.isScrollEnabled = false
//        self.veriCopyTextView!.isSelectable = false
//        self.veriCopyTextView!.textContainer.lineFragmentPadding = 0
//        self.veriCopyTextView!.textContainerInset = .zero
//        self.veriCopyTextView!.textColor = UIColor.white
//        self.veriCopyTextView!.backgroundColor = UIColor.clear
//
//        self.veriCopyTextView = tView
//    }
//
//    func sizeOfCopyLarge(string: String, constrainedToWidth width: Double) -> CGSize {
//        return NSString(string: string).boundingRect(
//            with: CGSize(width: width, height: .greatestFiniteMagnitude),
//            options: .usesLineFragmentOrigin,
//            attributes: [.font: UIFont.init(name: "Avenir-Light", size: 12.0)! ],
//            context: nil).size
//    }
}
