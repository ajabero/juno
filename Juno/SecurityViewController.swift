//
//  SecurityViewController.swift
//  Juno
//
//  Created by Asaad on 2/1/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SecurityViewController: UIViewController, UITextFieldDelegate {

    //MARK: Global Properties
    //Firebase
    var ref: DatabaseReference!
    var user: User?
    
    //
    var ref_AuthTracker = AuthTracker()
    
    var verifiedEmail = ""
    var userPassword = ""
    var userPasswordLength = 0
    var duplicatePassword = ""
    var duplicatePasswordLength = 0
    var verifiedPassword = ""
    
    //MARK: Outlets
    
    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var buttonStackView: UIStackView!

    @IBOutlet weak var newPasswordLineView: UIImageView!
    @IBOutlet weak var confirmPasswordLineView: UIImageView!

    @IBOutlet weak var errorLabelA: UILabel!
    @IBOutlet weak var errorLabelB: UILabel!
    
    var kbHeight: CGFloat?
    var bottomPadding: CGFloat?
    
    //MARK: Button View Formatting
    
    func positionStackView() {
        //adjust height
        let bottomInsetRatio = 67/736 as CGFloat
        let screenHeight = screen.height
        let stackViewHeight = buttonStackView.frame.height
        let inset = screenHeight * bottomInsetRatio
        self.buttonStackView.frame.origin.y = screenHeight - inset - stackViewHeight + 10.0
        
        //adjust center
        let screenWidth = screen.width
        let stackViewWidth = buttonStackView.frame.width
        self.buttonStackView.frame.origin.x = (screenWidth/2) - (stackViewWidth/2)
    }
    
    //MARK: Set Up View
    
    override func viewDidLoad() {
        
        //print("verified user email is \(self.verifiedEmail)")
        
        super.viewDidLoad()
        newPasswordTextfield.delegate = self
        confirmPassword.delegate = self
        continueButton.isEnabled = false
        confirmPassword.isEnabled = false
        
        continueButton.setImage(UIImage(named: "GrayButton")!, for: UIControlState.normal)
//        newPasswordLineView.image = UIImage(named: "BlueLine")!
        newPasswordLineView.image = UIImage(named: "GrayLine")!
        confirmPasswordLineView.image = UIImage(named: "GrayLine")!

        errorLabelA.isHidden = true
        errorLabelB.isHidden = true
        
        self.continueButton?.isHidden = true
        self.addContinueButton()
        self.positionStackView()
        self.addActivityIndicator()
        continueButton2!.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
        
        if #available(iOS 11.0, *) {
            let bottomPadding = view.safeAreaInsets.bottom
            self.bottomPadding = bottomPadding
            //print("SVC iPhoneX bottomPadding,", bottomPadding)
        }

        self.newPasswordTextfield.textContentType = UITextContentType("")
        self.confirmPassword.textContentType = UITextContentType("")
    }
    
    //MARK: Button View Repositioning
    
        //MARK: Button View Repositioning/Properties

    let spaceToKeyboard = 25 as CGFloat
    let screen = UIScreen.main.bounds
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        let screenHeight = screen.size.height
        let stackViewHeight = self.buttonStackView.frame.height
        let keyboardHeight = getKeyboardHeight(notification)
        let risingStackViewPosition = screenHeight - keyboardHeight - stackViewHeight - spaceToKeyboard
//        self.buttonStackView.frame.origin.y = risingStackViewPosition
        
        self.continueButton2!.frame.origin.y = risingStackViewPosition

    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        let screenHeight = screen.size.height
        let stackViewHeight = self.buttonStackView.frame.height
//        let restingStackViewPosition = screenHeight - stackViewHeight - spaceToKeyboard
//        self.buttonStackView.frame.origin.y = restingStackViewPosition
        
        let restingStackViewPosition = screenHeight - superInsetHeight!
        self.continueButton2!.frame.origin.y = restingStackViewPosition
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        buttonStackView.superview!.layoutIfNeeded()
        buttonStackView.superview!.setNeedsLayout()

    }

    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }
    
    //MARK: Configure TextFields
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        } else {
        let characterCountLimit = 80
        let startingLength = textField.text?.count ?? 0
        let lengthToAdd = string.count
        let lengthToReplace = range.length
        self.userPasswordLength = startingLength + lengthToAdd - lengthToReplace
        if self.userPasswordLength < 6 {
            //
            continueButton2!.backgroundColor = self.inactiveGrayColor
            continueButton2!.isEnabled = false
        } else {
            //pwContinueButton.isEnabled = true
            //pwContinueButton.setImage(UIImage(named: "GreenButton"), for: UIControlState.normal)
//            continueButton2!.backgroundColor = self.activeGreenColor
//            continueButton2!.isEnabled = true
            confirmPassword.isEnabled = true
            confirmPasswordLineView.image = UIImage(named: "BlueLine")!
        }
            
        if textField == self.confirmPassword {
            self.duplicatePasswordLength = startingLength + lengthToAdd - lengthToReplace
            
            if self.duplicatePasswordLength < (self.newPasswordTextfield.text?.count)! {
                continueButton2!.backgroundColor = self.inactiveGrayColor
                continueButton2!.isEnabled = false
            } else {
                //print("shouldChangeCharactersIn green")
                continueButton2!.backgroundColor = self.activeGreenColor
                continueButton2!.isEnabled = true
            }
            
           
            
        }
            return userPasswordLength <= characterCountLimit
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.newPasswordTextfield && self.userPasswordLength >= 6 {
            self.confirmPassword.becomeFirstResponder()
        } else if textField == self.confirmPassword {
            self.confirmPassword.resignFirstResponder()
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("textfield did end editing")
        if textField == self.confirmPassword {
            self.duplicatePassword = confirmPassword.text!
            if self.duplicatePassword.count == 0 {
//                continueButton.setImage(UIImage(named: "GrayButton"), for: UIControlState.normal)
                
                continueButton2!.backgroundColor = self.inactiveGrayColor
                continueButton2!.isEnabled = false
            }
        }
        
        if textField == confirmPassword && newPasswordTextfield.isFirstResponder == true {
            confirmPassword.text = ""
        }
        
        if !self.newPasswordTextfield.isFirstResponder && !self.confirmPassword.isFirstResponder {
            self.newPasswordLineView.image = UIImage(named: "GrayLine")!
            self.confirmPasswordLineView.image = UIImage(named: "GrayLine")!
        }
    }
    
    //MARK: Other UI Adjustments
    
    @IBAction func TapAreaOutside(_ sender: Any) {
        
        
        if newPasswordTextfield.isFirstResponder || confirmPassword.isFirstResponder {
            newPasswordTextfield.resignFirstResponder()
            confirmPassword.resignFirstResponder()
        } else {
            newPasswordTextfield.becomeFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == confirmPassword {
            continueButton2!.isEnabled = true
//            continueButton2!.backgroundColor = self.activeGreenColor
//            continueButton.setImage(UIImage(named: "GreenButton"), for: UIControlState.normal)
        }
        
        if textField == newPasswordTextfield {
            newPasswordLineView.image = UIImage(named: "BlueLine")!
            confirmPasswordLineView.image = UIImage(named: "GrayLine")!
            errorLabelA.isHidden = true
            errorLabelB.isHidden = true
        }
        
        if textField == confirmPassword {
            newPasswordLineView.image = UIImage(named: "GrayLine")!
            confirmPasswordLineView.image = UIImage(named: "BlueLine")!
        }
    }
    
    var authErrorString = "Email invalid, or already in use. Please try again."

    func signUpNewUser() {
        //print("will signUpNewUser")
        self.newPasswordTextfield.resignFirstResponder()
        self.confirmPassword.resignFirstResponder()
        
        let verifiedEmail = self.verifiedEmail
        let verifiedPassword = self.verifiedPassword
        //print("email is \(verifiedEmail), password is \(verifiedPassword)")
        
        self.activityIndicatorView!.startAnimating()
        
        Auth.auth().createUser(withEmail: verifiedEmail, password: verifiedPassword) { (user, error) in
            self.ref = Database.database().reference()
            if error != nil {
                
                //Handle auth error
                if let errorCode = AuthErrorCode(rawValue: error!._code) {
                    switch errorCode {
                        
                    case .invalidEmail:
                        //print("ERROR: invalidEmail")
                        self.authErrorString = "Email invalid, or already in use. Please try again."
                        
                    case .emailAlreadyInUse:
                        //print("ERROR: emailAlreadyInUse")
                        self.authErrorString = "Email invalid, or already in use. Please try again."

                    case .networkError:
                        //print("ERROR: networkError")
                        self.authErrorString = "We've run into an error. Please try again."

                    case .internalError:
                        //print("ERROR: internalError")
                        self.authErrorString = "We've run into an error. Please try again."

                    case .weakPassword:
                        //print("ERROR: weakPassword")
                        self.errorLabelA.isHidden = false
                        self.errorLabelB.isHidden = false
                        
                        self.errorLabelA.text = "Password too weak."
                        self.errorLabelB.text = " Please try a different one."
                        
                        //Outcome I: stay in current VC
                        
                        return
                        
                    default:
                        break
                    }
                }

                //Outcome II: Segue back to sign-in
                self.performSegue(withIdentifier: "backToEmail", sender: self)
                
            } else {
                
                if let user = Auth.auth().currentUser {
                    //print("will signUpNewUser SUCCESS")
                    
                    //
                    self.ref_AuthTracker.emailAuthSource = true
                    self.ref_AuthTracker.phoneAuthSource = false
                    self.ref_AuthTracker.performBatchUpdates_Security(withEmail: verifiedEmail, withTermsBool: "true")
                    
                    UserDefaults.standard.set("true", forKey: "didAttemptEmailAuth")
                    self.activityIndicatorView!.stopAnimating()
                    self.performSegue(withIdentifier: "toTrustVC", sender: self)

                    //FBCondition
                    //self.observeFBPermissions()
                }
            }
        }
    }
    
    var yesFBPermissions = false
    
    func observeFBPermissions() {
        
        self.ref.child("fbPermissions").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let value = snapshot.value as? [String: String] {
                if let withAccess = value["withAccess"] {
                    if withAccess == "false" {
                        self.yesFBPermissions = false
                        self.activityIndicatorView!.stopAnimating()
                        self.performSegue(withIdentifier: "toTrustVC", sender: self)
                    } else if withAccess == "true" {
                        self.yesFBPermissions = true
                        self.activityIndicatorView!.stopAnimating()
                        self.performSegue(withIdentifier: "toTrustVC", sender: self)
                    }
                } else {
                    self.activityIndicatorView!.stopAnimating()
                    self.performSegue(withIdentifier: "toTrustVC", sender: self)
                }
            } else {
                self.activityIndicatorView!.stopAnimating()
                self.performSegue(withIdentifier: "toTrustVC", sender: self)
            }
            
        }) { (error) in
            self.activityIndicatorView!.stopAnimating()
            self.performSegue(withIdentifier: "toTrustVC", sender: self)
        }
        
    }
    
    
    
    /*
    func setAllUserParameters() {
        
        if let user = Auth.auth().currentUser {
            
            let selfUID = user.uid
            
            self.ref.child("users").child(user.uid).setValue(["userEmail": verifiedEmail])
            self.ref.child("users/\(user.uid)/privacy").setValue("false")
            
            self.ref.child("users/\(selfUID)/userDisplayName").setValue("DisplayName")
            self.ref.child("users/\(selfUID)/full_URL").setValue("")
            self.ref.child("users/\(selfUID)/userDisplayPhoto").setValue("")
            self.ref.child("users/\(selfUID)/userAge").setValue("")
            
            self.ref.child("users/\(selfUID)/School").setValue("")
            self.ref.child("users/\(selfUID)/Work").setValue("")
            self.ref.child("users/\(selfUID)/AboutMe").setValue("")
            self.ref.child("users/\(selfUID)/Height").setValue("")
            self.ref.child("users/\(selfUID)/LookingFor").setValue("")
            
            self.ref.child("users/\(selfUID)/privacy").setValue("false")
            self.ref.child("users/\(selfUID)/visus").setValue("true")
            self.ref.child("users/\(selfUID)/blurState").setValue("")
            self.ref.child("users/\(selfUID)/isOnline").setValue("")
        }
    }
    */
    
    var activityIndicatorView: UIActivityIndicatorView?
    let activeGreenColor = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    var inactiveGrayColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    var continueButton2: UIButton?
    
    var superInsetHeight: CGFloat!
    
    func addContinueButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        self.superInsetHeight = superInsetHeight
        
        //
        let bW = 3 * gridUnitWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
        let bY = sHeight - superInsetHeight - bH
        
        //
        let f = CGRect(x: bX, y: bY, width: bW, height:bH)
        let continueButton2 = UIButton(frame: f)
        
        continueButton2.addTarget(self, action: #selector(SecurityViewController.doneSecurity(_:)), for: .touchUpInside)
        continueButton2.backgroundColor = self.inactiveGrayColor
        continueButton2.setBackgroundImage(UIImage(named: "darkGreenBackGround")!, for: .highlighted)
        continueButton2.isUserInteractionEnabled = true
        self.view.addSubview(continueButton2)
        self.continueButton2 = continueButton2
        
        self.continueButton2!.layer.cornerRadius = 8
        self.continueButton2!.layer.masksToBounds = true
        
        self.continueButton2!.setTitle("Continue", for: .normal)
        self.continueButton2!.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 16.0)!
        self.continueButton2!.setTitleColor(UIColor.white, for: .normal)
    }
    
    @objc func doneSecurity(_ sender: UIButton) {
        self.userPassword = (self.newPasswordTextfield?.text)!
        self.duplicatePassword = (self.confirmPassword?.text)!
        if self.userPassword == self.duplicatePassword && self.duplicatePassword.count >= 1 {
            self.verifiedPassword = newPasswordTextfield.text!
            self.signUpNewUser()
        } else {
            errorLabelA.isHidden = false
            errorLabelB.isHidden = false
            
            errorLabelA.text = "The passwords you entered don't match."
            errorLabelB.text = "Please try again."
            
            self.verifiedPassword = ""
            //display red button for controlstate.isSelected
            //display label "Passwords don't match. Please try again."
        }
    }
    
    func addActivityIndicator() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 7/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = ((gridUnitWidth - aW) * 0.5) - 2
        let aY = (gridUnitWidth - aW) * 0.5
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
//        activityIndicatorView.color = UIColor.white
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        let centerX = sWidth * 0.5
        let centerY = (5 * superInsetHeight) + gridUnitHeight
        let centerPosition = CGPoint(x: centerX, y: centerY)
        
//        self.continueButton2!.addSubview(activityIndicatorView)
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
        
        self.activityIndicatorView!.layer.position = centerPosition
        
//        activityIndicatorView.startAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let destinationController = segue.destination as! TrustViewController
//        destinationController.kbHeight = self.kbHeight!
        
        if segue.identifier == "backToEmail" {
            let controller = segue.destination as! AuthViewController
            controller.authIsValid = false
            controller.authErrorString = self.authErrorString
            
        } else {
            let controller = segue.destination as! TrustViewController
            controller.yesFBPermissions = self.yesFBPermissions
            controller.bottomPadding = self.bottomPadding!
            controller.ref_AuthTracker = self.ref_AuthTracker
        }
    }
    
    //MARK: Post-setup
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}
