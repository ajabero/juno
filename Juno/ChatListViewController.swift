//
//  ChatListViewController.swift
//  Juno
//
//  Created by Asaad on 3/2/18.
//  Copyright © 2018 Animata Inc. All rights reserved.

import UIKit
import Firebase
import FirebaseAuth
import FirebaseStorage

class ChatListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, chatListCellDelegate {
    
    var userDictArrayFullyLoaded: Bool?
    var lastMessagesbyUID = [String:String]()
    var profileChangesUIDsInProximity = [String]()

    @IBOutlet weak var chatListTableView: UITableView!
    
    //DB Setup
    var ref: DatabaseReference!
    fileprivate var _refHandle: DatabaseHandle!
    
    //swapRequestDataStruct
    var chatListObjects = [[String : String]]() { didSet {
//        print("CLVC didSet chatListObjects", chatListObjects.count)
        }
    }
    
    var chatListObjectIDs = [String]() //Array of unique list object ID's (swapID, chatRequestID, or ThreadID)
    var allUniqueUIDsinChatList = [String]() { didSet {
//        print("CLVC didSet allUniqueUIDsinChatList", allUniqueUIDsinChatList)
        }
    }
    
    var unlockedUsers = ["", ""]
    
    var currentlySelectedObject = [String: String]()
    
    var selfUID: String?
    var selfDisplayName: String?
    var selfVisus: String? //always inherited from GridVC at source
    var selfDefaultPhotoURL: String?
    
    //new
    var otherUID: String? //otherUID is passed directly from CLVC or parsed from passed selectedUser["UID"] from ProfileVC //
    var otherDisplayName: String?
    var otherVisus: String? //always inherited from GridVC at sourcec
    var otherDefaultPhotoURL: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        //print("CLVC self.ref_SelfModelController.myProfileInfoDict,", self.ref_SelfModelController.myProfileInfoDict)
        self.selfUID = Auth.auth().currentUser!.uid
        self.ref = Database.database().reference()
        self.initializeSelfProfileInfo()

        //RESET BADGE NOTIFICATIONS whenever user accesses chatList View
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let pushID = self.myProfileInfoDict["pushID"] {
            ref.child("messageNotifications/\(pushID)/badgeCount").setValue("0") //replaced
        }

        //CheckPoint
        //print("allUniqueUIDsinChatList", allUniqueUIDsinChatList)
        //print("SEGUE gridVCtochatListVC INHERIT chatListObjects", self.chatListObjects.count)
        //print("SEGUE gridVCtochatListVC INHERIT chatListObjectIDs", self.chatListObjectIDs.count)
        //chatListTableView.rowHeight = UITableViewAutomaticDimension
        chatListTableView.estimatedRowHeight = 92
        chatListTableView.separatorStyle = .none
        
        //
//        self.observeActiveMessageThreads()
//        self.observeActiveMessageThreadChanges()
    
        //chat
        self.observeActiveChatRequests()
        self.observeChatRequestStatusChanges()
        
        //
        self.observeUnlockedUsers()
        self.observeIncomingDismissedRequests()
        
        //
        self.nameLabelInsetFromCompletionSymbol = (5 / 414) * UIScreen.main.bounds.width
        self.addImageLoader()
        
        self.configureUINotifications()

        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.sendRowToTop), name: NSNotification.Name(rawValue: "sendChatListRowToTopForUID"), object: nil)
        
//        NotificationCenter.default.post(name: Notification.Name(rawValue: "sendChatListRowToTopForUID"), object: self, userInfo: userInfoDict)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.observeChatList_Added), name: NSNotification.Name(rawValue: "chatList_Added"), object: nil) //deactivate
//
//        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.observeChatList_Changed), name: NSNotification.Name(rawValue: "chatList_Changed"), object: nil) //deactivate
        
        self.observeActiveMessageThreads() //reactivate
        self.observeActiveMessageThreadChanges() //reactivate
        
        self.observeActiveSwapRequests()
        self.observeSwapRequestStatusChanges()
    }
    
    func configureUINotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.observeOutgoingBlockedUser), name: NSNotification.Name(rawValue: "outgoingBlockObserved"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.observeIncomingBlockedUser), name: NSNotification.Name(rawValue: "incomingBlockObserved"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.observeOthProfileChanges), name: NSNotification.Name(rawValue: "othProfileChangeObserved"), object: nil)
    }
    
/* Test:
     //
     1. Switch off chatThread observer & changes
     2. Activate observeChatListChanges in VDL
     3. POST notifications in observer method of CVC
     4. DO NOT REMOVE CHAT OBSERVER ON VIEWDISAPPEAR IN CVC
     
     //prove following
     1. CVC will send object
     2. CLVC will receive object
     3. CLVC will update UI in sorted state
     4. NSNotification will not cause a problem
     
     SUCCESS -> Adopt new design pattern
 */
    
    @objc func observeChatList_Added(_ notification: Notification) {
        //print("CLVC observeChatList_Added")
        
        guard notification.name.rawValue == "chatList_Added" else { return }
        
        //print("CLVC observeChatList_Added2")
        
        //unwrap object
        var chatListObject = [String: String]()
        if let listObject =  notification.userInfo as? [String: String] {
            //print("observeChatList_Added listObject", chatListObject)
            chatListObject = listObject
        }
        
        
        if let objectType = notification.userInfo?["objectType"] as? String {
            
            if objectType == "generalChatThread" {
//                self.chatListObjects.append(listObject)
                self.chatListObjects.insert(chatListObject, at: 0)
                let globalRequestsChatThreadID = chatListObject["chatThreadID"]!
                //will only be called at CVC source if threadIDActive == true
                self.observeLastMessages(withThreadID: globalRequestsChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
            }
            
        }
    }
        
        
    @objc func observeChatList_Changed(_ notification: Notification) {
        //print("CLVC observeChatList_Changed")
        
        guard notification.name.rawValue == "chatList_Changed" else { return }
        
        //print("CLVC observeChatList_Changed2")
        
        //unwrap object
        var chatListObject = [String: String]()
        if let listObject =  notification.userInfo as? [String: String] {
            //print("observeChatList_Changed listObject", chatListObject)
            chatListObject = listObject
        }
        
        
        if let objectType = notification.userInfo?["objectType"] as? String {
            
            if objectType == "generalChatThread" {
                //                self.chatListObjects.append(listObject)
                self.chatListObjects.insert(chatListObject, at: 0)
                let globalRequestsChatThreadID = chatListObject["chatThreadID"]!
                //will only be called at CVC source if threadIDActive == true
                self.observeLastMessages(withThreadID: globalRequestsChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
            }
            
        }
    }
    
    var modifyRowForUID: String?
    
    @objc func sendRowToTop(_ notification: Notification) {
        //print("CLVC sendRowToTop1")

        guard notification.name.rawValue == "sendChatListRowToTopForUID" else { return }
        
        //print("CLVC sendRowToTop2")

        if let forUserID = notification.userInfo?["objectUID"] as? String {
            
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["objectUID"] == forUserID
            })
          
            guard let objectIndex = i else { return }
            
            let userObject = chatListObjects[objectIndex]
            
            //send object to top of tableView
            self.chatListObjects.remove(at: objectIndex)
            self.chatListObjects.insert(userObject, at: 0)
            
            let fromIndexPath = IndexPath(row: objectIndex, section: 0)
            let toIndexPath = IndexPath(row: 0, section: 0)
            self.chatListTableView.moveRow(at: fromIndexPath, to: toIndexPath)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict

//        self.observeActiveMessageThreads() //reactivate
//        self.observeActiveMessageThreadChanges() //reactivate
//
//        self.observeActiveSwapRequests()
//        self.observeSwapRequestStatusChanges()
        
        //Move these to VDL?
        self.addChatListNavButton()
        self.addToGridNavButton()
        self.overrideLayoutConstraints()
//        self.configureUINotifications()
      
        self.discoverGridNavButton?.isUserInteractionEnabled = true
        
        //print("CLVC inherit self.chatListObjects",self.chatListObjects.count)

    }
    
    
    //ATTN: WHY DOESNT THE CHATLIST OBJECT SHOW THE RIGHT DETAIL WHEN SEGUE FROM GRIDVC TO CLVC BEFORE CALLING OBSERVER IN CLVC
    //ATTN: NEED TO UPDATE VALUES CARBON COPY IN OBJECT NODE TYPES 
    
    @objc func observeOthProfileChanges(_ notification: Notification) {
        //print("CLVC observeOthProfileChanges")
        
        guard notification.name.rawValue == "othProfileChangeObserved" else { return }
    
//        userInfoDict["changeUserID"] = userID
//        userInfoDict["newPhotoURL"] = newFullURL
//        userInfoDict["newUserVisus"] = newVisus
        
        var changeUserID = ""
        var newPhotoURL = ""
        var newUserVisus = ""
        var listObjectType = ""
        
        if let changeUID = notification.userInfo?["changeUserID"] as? String {
            changeUserID = changeUID
        }
        
        if let newFullURL = notification.userInfo?["newPhotoURL"] as? String {
            newPhotoURL = newFullURL
        }
        
        if let newVisus = notification.userInfo?["newUserVisus"] as? String {
            newUserVisus = newVisus
        }
        
        //get the entry
        if let objectType = notification.userInfo?["forObjectType"] as? String {
            listObjectType = objectType
        }
        
        //1. CHANGE FOR GEN CHAT THREAD
        if listObjectType == "generalChatThread" {
            
//            let genChatIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
//                dictionaryOfInterest["chatWithUID"] == changeUserID
//            })
//
//            guard let index = genChatIndex else { return }
//            let indexPath = IndexPath(row: index, section: 0)
//            //print("A! CLVC CHANGE OBJECT GUARD PASS")
//
//            (self.chatListObjects[index])["chatWithPhotoURL"] = newPhotoURL
//            (self.chatListObjects[index])["chatWithVisus"] = newUserVisus
//
//            self.chatListTableView.reloadRows(at: [indexPath], with: .none)
        }
        
        if listObjectType == "1WaySwapToChat" {
            
            //switch method off for outgoing reveal because profile autounlocks when user switches
            //but what about when back? i.e. when user switches back once more? //doesn't user still need to r
            
            guard let userID = notification.userInfo?["chatWithUID"] as? String else { return }
            
            let oneWaySwapIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["revealToUID"] == userID
            })
            
            guard let oneWayIndex = oneWaySwapIndex else { return }
            let indexPath = IndexPath(row: oneWayIndex, section: 0)
           
            guard let generalChatObject = notification.userInfo as? [String : String] else { return }
            
            //remove old item
            self.chatListObjects.remove(at: oneWayIndex)
            self.chatListTableView.deleteRows(at: [indexPath], with: .none)
            if let index = self.allUniqueUIDsinChatList.index(of: userID) {
                self.allUniqueUIDsinChatList.remove(at: index)
            }
            //insert new item
//            if generalChatObject["chatThreadID"] != "" {
//                self.chatListObjects.insert(generalChatObject, at: oneWayIndex)
//                self.chatListTableView.insertRows(at: [indexPath], with: .none)
//            }
            
            if let lastMessage = self.lastMessagesbyUID["\(userID)"] {
                if lastMessage != "" {
                    self.chatListObjects.insert(generalChatObject, at: oneWayIndex)
                    self.chatListTableView.insertRows(at: [indexPath], with: .none)
                    self.allUniqueUIDsinChatList.append(userID)
                } else {

                }
            }

        }
        
        //2. CHANGE FOR ANY 1WAY OBJECT TYPE
        if listObjectType == "oneWaySwapObject_ANY" {
            
            //switch method off for outgoing reveal because profile autounlocks when user switches
            //but what about when back? i.e. when user switches back once more? //doesn't user still need to r
            
            let genChatIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["revealToUID"] == changeUserID
            })
            
            guard let index = genChatIndex else { return }
            let indexPath = IndexPath(row: index, section: 0)
            //print("B! CLVC CHANGE OBJECT GUARD PASS")
            
            (self.chatListObjects[index])["revealToDefaultPhotoURL"] = newPhotoURL
            (self.chatListObjects[index])["revealToVisus"] = newUserVisus
            
            self.chatListTableView.reloadRows(at: [indexPath], with: .none)
        }
       
        //3. CHANGE FOR ANY 2WAY OBJECT TYPE
        if listObjectType == "twoWaySwapObject_ANY" {
            
            let genChatIndex = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapWithUID"] == changeUserID
            })
            
            guard let index = genChatIndex else { return }
            let indexPath = IndexPath(row: index, section: 0)
            //print("C! CLVC CHANGE OBJECT GUARD PASS")
            
            (self.chatListObjects[index])["swapWithDefaultPhotoURL"] = newPhotoURL
            (self.chatListObjects[index])["swapWithVisus"] = newUserVisus
            
            self.chatListTableView.reloadRows(at: [indexPath], with: .none)
        }
        
        //    var swapObjectTypes_2Way = ["swapRequestOtherNew", "swapRequestSelfNew", "otherInitSwapApprovedNew", "otherInitSwapApprovedPre", "selfInitSwapApprovedNew", "selfInitSwapApprovedPre"]
        //    var corr_Keys_2Way = ["swapWithUID", "swapWithDefaultPhotoURL", "swapWithVisus"]
        //
        //    var swapObjectTypes_1Way = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre", "outgoingOneWayShow", "incomingOneWayShow"]
        //    var corr_Keys_1Way = ["revealToUID", "revealToDefaultPhotoURL", "revealToVisus"]
        //
        //    var genChatObjectTypes = ["generalChatThread"]
        //    var corr_Keys_Gen = ["chatWithUID", "chatWithPhotoURL", "chatWithVisus"]
    }
    
    @objc func observeOutgoingBlockedUser(_ notification: Notification) {
        //print("CLVC observeOutgoingBlockedUser")
        
        guard notification.name.rawValue == "outgoingBlockObserved" else { return }
        
        if let outgoingBlockUID = notification.userInfo?["outgoingBlockUID"] as? String {
            //print("observedoutgoingBlockUID", outgoingBlockUID)
            
            let userIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
                userInfoObject["chatWithUID"] == outgoingBlockUID || userInfoObject["revealToUID"] == outgoingBlockUID || userInfoObject["swapWithUID"] == outgoingBlockUID || userInfoObject["objectUID"] == outgoingBlockUID
            })

            if let userIndex = userIndex {
                //print("AD3")
                let indexPath = IndexPath(row: userIndex, section: 0)
                self.chatListObjects.remove(at: userIndex)
                self.chatListTableView!.deleteRows(at: [indexPath], with: .none)
            }
        }
    }
    
    @objc func observeIncomingBlockedUser(_ notification: Notification) {
        //print("CLVC observeIncomingBlockedUser")
        
        guard notification.name.rawValue == "incomingBlockObserved" else { return }
        
        if let incomingBlockUID = notification.userInfo?["incomingBlockUID"] as? String {
            //print("observeIncomingBlockedUser incomingBlockUID", incomingBlockUID)
            
            let userIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
                userInfoObject["chatWithUID"] == incomingBlockUID || userInfoObject["revealToUID"] == incomingBlockUID || userInfoObject["swapWithUID"] == incomingBlockUID || userInfoObject["objectUID"] == incomingBlockUID
            })
            
            if let userIndex = userIndex {
                //print("observeIncomingBlockedUser will remove row")
                let indexPath = IndexPath(row: userIndex, section: 0)
                self.chatListObjects.remove(at: userIndex)
                self.chatListTableView!.deleteRows(at: [indexPath], with: .none)
            }
        }
    }
    
    var incomingDismissedUIDs = [String]() { didSet {
//        print("CLVC incomingDismissedUIDs didSet", incomingDismissedUIDs)
        }}

    func observeIncomingDismissedRequests() {
        //print("CLVC observeIncomingDismissedRequests")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)/dismissedBy").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("CLVC observeIncomingDismissedRequests snapshot is,", snapshot.key)
            let dismissedByUID = snapshot.key
            self.incomingDismissedUIDs.append(dismissedByUID)
            
            let userInfoDict = ["incomingDismissedByUID" : dismissedByUID]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "incomingDismissedByUID"), object: self, userInfo: userInfoDict)
        })
    }
    
    @IBOutlet weak var chatListTopLayoutConstraint: NSLayoutConstraint!
    
    func overrideLayoutConstraints() {
        let sHeight = UIScreen.main.bounds.height
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        
        let absoluteNavSpaceRatio = 56 / 734 as CGFloat
        var absoluteNavSpace = absoluteNavSpaceRatio * sHeight
        
        if sHeight >= 734 as CGFloat {
            //constrain the navSpace to a fixed amount for iPhoneX
            absoluteNavSpace = 56 as CGFloat
        } else {
            //
        }

        self.chatListTopLayoutConstraint.constant = statusBarHeight + absoluteNavSpace
    }
    
    var chatListNavButton: UIButton?
    var discoverGridNavButton: UIButton?
    
    func addChatListNavButton() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        //print("statusBarHeight is", statusBarHeight)
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        let rRatio = 37 / 414 as CGFloat

        let dW = rRatio * sWidth
        let dH = dW
        let dX = (sWidth - dW) * 0.5
        //        let dY = statusBarHeight + ((navSpaceHeight - statusBarHeight - dH) * 0.5)
        let insetFromStatusBar = 5 as CGFloat
        let dY = statusBarHeight + insetFromStatusBar
        //init label
        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
        let chatListNavButton = UIButton(frame: dF)
        
        chatListNavButton.setImage(#imageLiteral(resourceName: "chatListChatsIcon"), for: .normal)
//        chatListNavButton.backgroundColor = UIColor.red
        chatListNavButton.addTarget(self, action: #selector(ChatListViewController.chatListMain(_:)), for: .touchUpInside)
        chatListNavButton.isUserInteractionEnabled = true
        chatListNavButton.adjustsImageWhenHighlighted = false
        
        //init
        self.view.addSubview(chatListNavButton)
        self.chatListNavButton = chatListNavButton
    }
    
    func addToGridNavButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = marginInset
        var hY = 0 as CGFloat
        let centerAxis = self.chatListNavButton!.frame.minY + (self.chatListNavButton!.bounds.height * 0.5)
        hY = centerAxis - (hH * 0.5)
        
        //
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let discoverGridNavButton = UIButton(frame: f)
//        discoverGridNavButton.backgroundColor = UIColor.red
        
        discoverGridNavButton.setImage(UIImage(named: "discoveryIconSmall")!, for: .normal)
        discoverGridNavButton.addTarget(self, action: #selector(ChatListViewController.backToGrid(_:)), for: .touchUpInside)
        
        self.view.addSubview(discoverGridNavButton)
        self.discoverGridNavButton = discoverGridNavButton
    }
    
    @objc func chatListMain(_ sender: UIButton) {
        //print("chatListMain tap detected")
//        self.performSegue(withIdentifier: "backToGrid", sender: self)
        let origin = CGPoint(x: 0, y: 0)
        guard self.chatListTableView!.contentOffset != origin else { return }
        self.chatListTableView!.scrollRectToVisible(UIScreen.main.bounds, animated: true)
    }
    
    @objc func backToGrid(_ sender: UIButton) {
        //print("backToGrid tap detected")
//        self.performSegue(withIdentifier: "backToGrid", sender: self)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "manualClearChatAlertForCVC"), object: self)
        self.performSegue(withIdentifier: "chatsToGridUnwinder", sender: self)
    }
    
    
                                                      /* Made In America */
    
                                                /////////////////////////////////
                                                /********* ----------------------
                                                ********** ----------------------
                                                ********** ----------------------
                                                ********** ----------------------
                                                ---------------------------------
                                                ---------------------------------
                                                -------------------------------*/
                                                /////////////////////////////////

    
    var myProfileInfoDict = [String : String]()

    func initializeSelfProfileInfo() {
        //initliazed from same source regardless of presenting
        self.selfUID = Auth.auth().currentUser!.uid
        
        if let myName = myProfileInfoDict["selfDisplayName"] {
            self.selfDisplayName = myName
        }
        
        if let myVisus = myProfileInfoDict["selfVisus"] {
            self.selfVisus = myVisus
        }
        
        if let photoURL = myProfileInfoDict["selfDefaultPhotoURL"] {
            self.selfDefaultPhotoURL = photoURL
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatListObjects.count
    }
    
    /*
     var newListObject = [String:String]()
     newListObject["objectType"] = "swapRequestOtherNew"
     newListObject["swapRequestID"] = newSwapRequestID
     newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
     newListObject["requestStatus"] = swapRequestStatus
     newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
     */
    
//    @IBAction func acceptChatRequestButton(_ sender: Any) {
//        delegate?.acceptChatRequest(self)
//    }
//
//    @IBAction func dismissChatRequestButton(_ sender: Any) {
//        delegate?.rejectChatRequest(self)
//    }
    
//    @IBAction func newChatButton(_ sender: Any) {
//        delegate?.toNewChatButton(self)
//    }
//

//    //Chat Stack & Symbol
//    @IBOutlet weak var chatActiveStack: UIStackView!
//    @IBOutlet weak var completeSwapSymbol: UIImageView!
//    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var lastMessageLabel: UILabel!
//
//    //Request Stack
//    @IBOutlet weak var requestStatusStack: UIStackView!
//    @IBOutlet weak var requestMainLabel: UILabel!
//    @IBOutlet weak var requestTimeStamp: UILabel!
//
//    //Response Action Stack
//    @IBOutlet weak var responseButtonsStack: UIStackView!
//    @IBOutlet weak var chatAcceptButton: UIButton!
//    @IBOutlet weak var chatDismissButton: UIButton!
//    @IBOutlet weak var newChatSymb: UIButton!
    
    var thumbnailSuperView: UIView!
    var thumbnailView: UIImageView!
    var thumbnailSymbol: UIImageView!

    
    func addImageView(toCell: ChatListTableViewCell) {
        //
        //print("toCell", toCell, "bounds are", toCell.frame)
        //print("toCell contentView frame,", toCell.contentView.frame)
        //bounds are (0.0, 0.0, 414.0, 92.0)
        //bounds are (x0.0, y0.0, w320.0, h44.0)
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let hwRatio = 92 / 414 as CGFloat
        let assignedCellHeight = hwRatio * sWidth
        let newFrame = CGRect(x: 0, y: 0, width: sWidth, height: assignedCellHeight)
        toCell.frame = newFrame
//        toCell.backgroundColor = UIColor.red
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = toCell.frame.height
        //print("addImageView chatListTableViewCEllHeight is,", cellHeight)

        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add frameViewOutline
        let fW = cellHeight
        let fH = fW
        let fX = marginInset
        let fY = 0 as CGFloat

        let frame = CGRect(x: fX, y: fY, width: fW, height: fH)
        let frameView = UIView(frame: frame)
//        frameView.backgroundColor = UIColor.yellow
        toCell.contentView.addSubview(frameView)
        toCell.thumbnailSuperView = frameView
        
        //then addImageView
        let imageWidthRatio = 76 / 414 as CGFloat

        let iW = imageWidthRatio * sWidth
        let iH = iW
        let iX = (toCell.thumbnailSuperView.bounds.width - iW) * 0.5
        let iY = iX
        
        let iFrame = CGRect(x: iX, y: iY, width: iW, height: iH)
        let userThumbnailView = UIImageView(frame: iFrame)
        userThumbnailView.layer.cornerRadius = userThumbnailView.frame.height * 0.5
        userThumbnailView.layer.masksToBounds = true
        userThumbnailView.backgroundColor = UIColor.clear
        userThumbnailView.contentMode = .scaleAspectFill
        
        toCell.thumbnailSuperView.addSubview(userThumbnailView)
        toCell.thumbnailView = userThumbnailView
//        toCell.backgroundColor = UIColor.yellow
        
        //then add thumbnailSymbol view
        let symbWidthRatio = 38 / 414 as CGFloat
        let symbHeightRatio = 25 / 414 as CGFloat
        let symbAspectRatio = 25 / 38 as CGFloat
        
        let sW = symbWidthRatio * sWidth
        let sH = sW * symbAspectRatio
        let sX = (toCell.thumbnailSuperView.bounds.maxX) - sW
        let sY = (toCell.thumbnailSuperView.bounds.maxY) - sH
        
        let sF = CGRect(x: sX, y: sY, width: sW, height: sH)
        let thumbnailSymbol = UIImageView(frame: sF)
        thumbnailSymbol.backgroundColor = UIColor.clear
        thumbnailSymbol.contentMode = .center
        
        toCell.thumbnailSuperView.addSubview(thumbnailSymbol)
        toCell.thumbnailSymbol = thumbnailSymbol
        
        //add line separator
        let lW = sWidth - (2 * marginInset) - fW
        let lH = 0.5 as CGFloat
        let lX = toCell.thumbnailSuperView.frame.maxX + marginInset
        let lY = cellHeight
        
        let lineFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineSeparator = UIView(frame: lineFrame)
        let lineColor = UIColor(displayP3Red: 225/250, green: 225/250, blue: 225/250, alpha: 225/250)
        lineSeparator.backgroundColor = lineColor
        
        toCell.addSubview(lineSeparator)
        
        //add unread message view
        let uRatio = 17 / sWidth
        let frameSpace = (fW - iW) * 0.5
        
        //add frame
        let uWidth = uRatio * sWidth
        let uHeight = uWidth
        let uX = toCell.thumbnailSuperView.frame.maxX - uWidth - marginInset - frameSpace
        let uY = toCell.thumbnailSuperView.frame.minY + marginInset
        
        let uFrame = CGRect(x: uX, y: uY, width: uWidth, height: uHeight)
        let uView = UIImageView(frame: uFrame)
        uView.image = UIImage(named: "chatListNewMessageSymbol")
        uView.contentMode = .center
        
        toCell.thumbnailSuperView.addSubview(uView)
        toCell.newMessageSymbol = uView
        toCell.newMessageSymbol.isHidden = true
    }
    
    
    //    //Request Stack
    //    @IBOutlet weak var requestStatusStack: UIStackView!
    //    @IBOutlet weak var requestMainLabel: UILabel!
    //    @IBOutlet weak var requestTimeStamp: UILabel!
    
    func addRequestViews(toCell: ChatListTableViewCell) {
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = toCell.frame.height
        //print("chatListTableViewCEll is,", cellHeight)
        
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add toSwap View
        let dWRatio = 23 / 414 as CGFloat
        let hRatio = 7 / 23 as CGFloat
        let rightInsetRatio = 17 / 414 as CGFloat
        let rightInset = rightInsetRatio * sWidth
        
        let dW = dWRatio * sWidth
        let dH = hRatio * dW
        let dX = sWidth - rightInset - dW
        let dY = (cellHeight - dH) * 0.5
        
        let dFrame = CGRect(x: dX, y: dY, width: dW, height: dH)
        let dotsView = UIImageView(frame: dFrame)
        dotsView.image = UIImage(named: "toSwapDots")
        
        toCell.addSubview(dotsView)
        toCell.dotsView = dotsView
        toCell.dotsView.isHidden = true
        
        //add request label
        var rWidth = sWidth - toCell.thumbnailSuperView!.frame.width - (1 * marginInset) - rightInset - dW
        let rHeight = cellHeight
        var rX = toCell.thumbnailSuperView!.frame.width + (1 * marginInset)
        let rY = 0 as CGFloat
        
        if sWidth < 350 {
            //adjust label width to not touch image view when screen is smaller than 6/7 Class
            rWidth = sWidth - toCell.thumbnailSuperView!.frame.width - (2 * marginInset) - rightInset - dW
            rX = toCell.thumbnailSuperView!.frame.width + (2 * marginInset)
        }
   
        let requestLabel = UIButton()
        requestLabel.frame = CGRect(x: rX, y: rY, width: rWidth, height: rHeight)
        requestLabel.backgroundColor = UIColor.white
//        requestLabel.setTitle("You" + "\n" + "Asked Gabrielle to Show His Profile" , for: .normal)
        requestLabel.titleLabel?.numberOfLines = 2
        requestLabel.titleLabel?.lineBreakMode = .byWordWrapping
        toCell.addSubview(requestLabel)
        requestLabel.titleLabel?.textAlignment = .center
        requestLabel.isUserInteractionEnabled = false
        let textGray = UIColor(displayP3Red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
        requestLabel.setTitleColor(textGray, for: .normal)
        requestLabel.titleLabel?.textColor = textGray
        toCell.requestMainLabel = requestLabel

        //
        let acceptButtonWidth = 63 as CGFloat
        
        //add timestamp
        let timeStampFrame = CGRect(x: rX, y: rY + (rHeight), width: rWidth, height: rHeight)
        let timeStampLabel = UILabel(frame: timeStampFrame)
        timeStampLabel.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        timeStampLabel.textColor = UIColor.lightGray
        timeStampLabel.textAlignment = .center
        timeStampLabel.backgroundColor = UIColor.clear
        
        toCell.addSubview(timeStampLabel)
        toCell.requestTimeStamp = timeStampLabel
    }
    
    //    //Response Action Stack
    //    @IBOutlet weak var responseButtonsStack: UIStackView!
    //    @IBOutlet weak var chatAcceptButton: UIButton!
    //    @IBOutlet weak var chatDismissButton: UIButton!
    //    @IBOutlet weak var newChatSymb: UIButton!
    
    func addActionButtonViews(toCell: ChatListTableViewCell) {
        //add chat accept
        //add chat dismiss
        //add new chat symbol
        //add toSwap symbol
        
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = toCell.frame.height
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add chat accept
        let acceptButtonWidth = 63 as CGFloat
        let aH = 28 as CGFloat
        let aX = sWidth - marginInset - acceptButtonWidth
        let aY = (cellHeight * 0.5) - aH
        
        let aFrame = CGRect(x: aX, y: aY, width: acceptButtonWidth, height: aH)
        let aView = UIButton(frame: aFrame)
        aView.contentMode = .center
        aView.setImage(UIImage(named: "requestAcceptButton")!, for: .normal)
//        aView.backgroundColor = UIColor.green
        
        toCell.addSubview(aView)
        toCell.chatAcceptButton = aView
        
        //add dismiss button
        let dFrame = CGRect(x: aX, y: aY + aH, width: acceptButtonWidth, height: aH)
        let dView = UIButton(frame: dFrame)
        dView.contentMode = .center
        dView.setImage(UIImage(named: "requestDismissButton")!, for: .normal)
//        dView.backgroundColor = UIColor.orange
        
        toCell.addSubview(dView)
        toCell.chatDismissButton = dView
        
        //add toChat view
        let toChatWidth = 36 as CGFloat
        let cH = 22 as CGFloat
        let cX = sWidth - marginInset - toChatWidth
        let cY = toCell.thumbnailView.frame.minY
        
        let cF = CGRect(x: cX, y: cY, width: toChatWidth, height: cH)
        let cView = UIButton(frame: cF)
        cView.contentMode = .center
        cView.setImage(UIImage(named: "newChatSymb")!, for: .normal)
        
        toCell.addSubview(cView)
        toCell.newChatSymb = cView
    }
    
//    @IBOutlet weak var chatActiveStack: UIStackView!
//    @IBOutlet weak var completeSwapSymbol: UIImageView!
//    @IBOutlet weak var userName: UILabel!
//    @IBOutlet weak var lastMessageLabel: UILabel!
    
    func addMessageViews(toCell: ChatListTableViewCell) {
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = toCell.frame.height
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        let messageInsetR = 8 / 414 as CGFloat
        let messageInset = messageInsetR * sWidth

        //1. add userNameLabel
        let nRatio = 200 / 414 as CGFloat
        let nameLabelWidth = nRatio * sWidth
        let nH = 30 as CGFloat
        let nX = toCell.thumbnailSuperView!.frame.maxX + messageInset
        let nY = toCell.thumbnailView.frame.minY
        
        let nFrame = CGRect(x: nX, y: nY, width: nameLabelWidth, height: nH)
        let nLabel = UILabel(frame: nFrame)
        
//        nLabel.backgroundColor = UIColor.yellow
        nLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        nLabel.textColor = UIColor.black
        nLabel.textAlignment = .left

        toCell.contentView.addSubview(nLabel)
        toCell.userName = nLabel
        
        //2. add oneway swap symbol
        let oneWaySymWidth = (18 / 414) * UIScreen.main.bounds.width
        let oH = oneWaySymWidth as CGFloat
        let oX = toCell.thumbnailSuperView!.frame.maxX + messageInset
        let oY = toCell.thumbnailView.frame.minY
        
        let oF = CGRect(x: oX, y: oY, width: oneWaySymWidth, height: oH)
        let oView = UIImageView(frame: oF)
        oView.contentMode = .scaleAspectFit
//        oView.backgroundColor = UIColor.yellow
        
        toCell.addSubview(oView)
        toCell.oneWaySwapCompleteMarker = oView
        toCell.oneWaySwapCompleteMarker.isHidden = true
        
        let centerX = toCell.oneWaySwapCompleteMarker.frame.maxX - (0.5 * oneWaySymWidth)
        let centerY = toCell.userName.frame.minY + (toCell.userName.frame.height * 0.5)
        toCell.oneWaySwapCompleteMarker.layer.position = CGPoint(x: centerX, y: centerY)
        
        //1. add completeSwapSymbol
        let symbWidth = (27 / 414) * UIScreen.main.bounds.width
        let sH = (30 / 27) * symbWidth
        let sX = toCell.thumbnailSuperView!.frame.maxX + messageInset
        let sY = toCell.thumbnailView.frame.minY
        
        let sFrame = CGRect(x: sX, y: sY, width: symbWidth, height: sH)
        let sView = UIImageView(frame: sFrame)
//        sView.backgroundColor = UIColor.yellow
        sView.contentMode = .center
        
        toCell.addSubview(sView)
        toCell.completeSwapSymbol = sView
   
        let center2X = toCell.completeSwapSymbol.frame.maxX - (0.5 * symbWidth)
        let center2Y = toCell.userName.frame.minY + (toCell.userName.frame.height * 0.5)
        toCell.completeSwapSymbol.layer.position = CGPoint(x: center2X, y: center2Y)
        
        //3. addLastMessageLabel
        let lW = nameLabelWidth
        let lH = sH
        let lX = nX
        let lY = (cellHeight - lH) * 0.5
        
        let lFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lLabel = UILabel(frame: lFrame)
        
//        lLabel.backgroundColor = UIColor.clear
        lLabel.font = UIFont(name: "AvenirNext-Regular", size: 14.0)
        lLabel.textColor = UIColor.black
        lLabel.textAlignment = .left
        
        toCell.contentView.addSubview(lLabel)
        toCell.lastMessageLabel = lLabel
    }
    
    var chatThreadObjectTypes = ["lastMessageSeen", "lastMessageUnseen"]
    var chatRequestObjectTypes = ["chatRequestOtherNew", "chatRequestSelfNew", "otherInitChatApprovedNew", "selfInitChatApprovedNew", "otherInitChatApprovedPre", "selfInitChatApprovedPre"]
    
    /*
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ChatListTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "chatListCell") as? ChatListTableViewCell
     
        let chatListObject = self.chatListObjects[(indexPath as NSIndexPath).row]
        let objectType = chatListObject["objectType"]!
   
        //
        cell.selectionStyle = .none
        
        //MARK: ChatRequestTableView
        if objectType == "chatRequestOtherNew" {
            print("chatRequestOtherNew")
            let displayName = chatListObject["chatRequestSenderFirstName"]!
            let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            self.chatRequestOtherNewUI(cell: cell!, nameLabel: displayName, urlString: urlString)
            
            /*
             let lastMessage = self.lastMessageArray["selectedUID"]
             observe
             */
            
        } else if objectType == "chatRequestSelfNew" {
            print("chatRequestSelfNew is")
            print("!!! object is,", chatListObject)
            let displayName = chatListObject["requestToDisplayName"]!
            let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            print("!!! displayName", displayName)
            print("!!! recipientURLString", recipientURLString)
            
            //do not show UI case but just test for now
            self.chatRequestSelfNewUI(cell: cell!, urlString: recipientURLString)
            
        } else if objectType == "otherInitChatApprovedNew" {
            let displayName = chatListObject["chatRequestSenderFirstName"]!
            let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            
            //convert to otherInitChatApprovedPre if selected USer has already instantiated a chatThread right after we newly approve their request
            let selectedUID = chatListObject["requestToUID"]!
            if self.lastMessagesbyUID.keys.contains(selectedUID) {
                let lastMessage = self.lastMessagesbyUID[selectedUID]!
                self.otherInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: urlString, lastMessage: lastMessage)
            } else {
                self.otherInitChatApprovedNewUI(cell: cell!, urlString: urlString)
            }
            
        } else if objectType == "selfInitChatApprovedNew" {
            let displayName = chatListObject["requestToDisplayName"]!
            let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            self.selfInitChatApprovedNewUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString)
            
        } else if objectType == "otherInitChatApprovedPre" {
            let displayName = chatListObject["chatRequestSenderFirstName"]!
            let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            //            let lastMessageString = chatListObject["lastMessageString"]!
            let selectedUID = chatListObject["requestToUID"]!
            if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                //
                self.otherInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: urlString, lastMessage: lastMessageString)
            }
            //            self.otherInitChatApprovedPreUI(cell: cell, nameLabel: displayName, urlString: urlString, lastMessage: lastMessageString)
            
            //problem: Why is CLVC observing a newly approved chatRequest as selfInitChatApprovedPre vs. selfInitChatApprovedNew? Should observe as selfInitChatApprovedNew
            
        } else if objectType == "selfInitChatApprovedPre" {
            let displayName = chatListObject["requestToDisplayName"]!
            let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
            //            let lastMessageString = chatListObject["lastMessageString"]!
            let selectedUID = chatListObject["requestToUID"]!
            
            if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                //
                self.selfInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString, lastMessage: lastMessageString)
            }
        }
        
        if objectType == "generalChatThread" {
            print("cellForRowAt objectType generalChatThread")
            
            let displayName = chatListObject["chatWithDisplayName"]!
            let recipientURLString = chatListObject["chatWithPhotoURL"]!
            
            print("!recipientURLString ,", recipientURLString)
            
            //            let lastMessageString = chatListObject["lastMessage"]!
            let selectedUID = chatListObject["chatWithUID"]!
            print("selectedUID", selectedUID)
            print("self.lastMessagesbyUID is,", lastMessagesbyUID)
            let visus = chatListObject["chatWithVisus"]!
            print("!visus ,", visus)
            
            if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                //
                self.generalChatThreadUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString, lastMessage: lastMessageString, otherUID: selectedUID, userVisus: visus)
            }
        }
        
        //MARK: SwapRequestTableView
        if chatListObject.keys.contains("swapRequestID") {
            let othDisplayName = chatListObject["swapWithDisplayName"]!
            let photoUrlString = chatListObject["swapWithDefaultPhotoURL"]!
            
            //Configure Swap Object Cells
            if objectType == "swapRequestOtherNew" {
                print("swapRequestOtherNewUI")
                //let urlString = chatListObject["swapInitiatorDefaultURL"]!
                let requestID = chatListObject["swapRequestID"]!
                self.swapRequestOtherNewUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, withRequestID: requestID)
                
            } else if objectType == "swapRequestSelfNew" {
                print("swapRequestSelfNew is")
                //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                self.swapRequestSelfNewUI(cell: cell!, urlString: photoUrlString)
                
            } else if objectType == "otherInitSwapApprovedNew" {
                //let urlString = chatListObject["swapInitiatorDefaultURL"]!
                let withUID = chatListObject["swapWithUID"]!
                self.otherInitSwapApprovedNewUI(cell: cell!, urlString: photoUrlString, otherUID: withUID)
                
            } else if objectType == "selfInitSwapApprovedNew" {
                //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                //let displayName = chatListObject["swapRecipientFirstName"]!
                let withUID = chatListObject["swapWithUID"]!
                self.selfInitSwapApprovedNewUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, otherUID: withUID)
                
            } else if objectType == "otherInitSwapApprovedPre" {
                //let urlString = chatListObject["swapInitiatorDefaultURL"]!
                let lastMessageString = chatListObject["lastMessage"]!
                let withUID = chatListObject["swapWithUID"]!
                self.otherInitSwapApprovedPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
                
            } else if objectType == "selfInitSwapApprovedPre" {
                //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                //let displayName = chatListObject["swapRecipientFirstName"]!
                let lastMessageString = chatListObject["lastMessage"]!
                let withUID = chatListObject["swapWithUID"]!
                self.selfInitSwapApprovedPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
            }
        }
        
        //MARK: OneWaySwapRequestTableView
        var oneWaySwapObjectTypes = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre"]
        
        if chatListObject.keys.contains("oneWayRequestID") {
            let objectType = chatListObject["objectType"]!
            
            let othDisplayName = chatListObject["revealToDisplayName"]!
            let photoUrlString = chatListObject["revealToDefaultPhotoURL"]!
            let lastMessage = chatListObject["lastMessage"]!
            
            
            if objectType == "incomingReveal" {
                let requestID = chatListObject["oneWayRequestID"]!
                self.incomingRevealUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, withRequestID: requestID)
                
            } else if objectType == "outgoingReveal" {
                self.outgoingRevealUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString)
                
            } else if objectType == "incomingRevealAppr" {
                let withUID = chatListObject["revealToUID"]!
                var lastMessageString = ""
                if let m = self.lastMessagesbyUID[withUID] {
                    lastMessageString = m
                }
                
                self.incomingRevealApprUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
                
            } else if objectType == "outgoingRevealAppr" {
                let withUID = chatListObject["revealToUID"]!
                self.outgoingRevealApprUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, otherUID: withUID)
                
            } else if objectType == "outgoingRevealApprPre" {
                let withUID = chatListObject["revealToUID"]!
                
                var lastMessageString = ""
                if let m = self.lastMessagesbyUID[withUID] {
                    lastMessageString = m
                }
                
                self.outgoingRevealApprPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, userUID: withUID)
                
                //PROBLEM: DO NOT APPEND OUTGOING ONE WAY SHOW TO CHAT LIST OBJECTS IF NO LAST MESSAGE EXISTS
            } else if objectType == "outgoingOneWayShow" {
                print("CLVC detected outgoingOneWayShow")
                let selectedUID = chatListObject["revealToUID"]!
                print("selectedUID", selectedUID)
                print("self.lastMessagesbyUID is,", lastMessagesbyUID)
                let lastMessageString = self.lastMessagesbyUID[selectedUID]!
                self.outgoingOneWayShowUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: selectedUID)
                
            } else if objectType == "incomingOneWayShow" {
                print("CLVC detected incomingOneWayShow")
                
                let selectedUID = chatListObject["revealToUID"]!
                print("selectedUID", selectedUID)
                print("self.lastMessagesbyUID is,", lastMessagesbyUID)
                if let m = self.lastMessagesbyUID[selectedUID] {
                    self.incomingOneWayShowUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: m, otherUID: selectedUID)
                } else if chatListObject["fromChatRequest"]! == "yes" {
                    
                }
            }
        }
        
        //
        return cell
    }
    */
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell: ChatListTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "chatListCell") as? ChatListTableViewCell
        
        //        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: CellIdentifier)
        //        if let cell = cell {
        //            // populate cell
        //        } else {
        //            cell = MyCell()
        //            // populate cell
        //        }
        //
        //        return cell
        
        let chatListObject = self.chatListObjects[(indexPath as NSIndexPath).row]
        let objectType = chatListObject["objectType"]!
        //        print("RELOAD chatListObject is,", chatListObject)
        
        var chatThreadObjectTypes = ["lastMessageSeen", "lastMessageUnseen"]
        
        var chatRequestObjectTypes = ["chatRequestOtherNew", "chatRequestSelfNew", "otherInitChatApprovedNew", "selfInitChatApprovedNew", "otherInitChatApprovedPre", "selfInitChatApprovedPre"]

        if cell != nil {
            cell = ChatListTableViewCell(style: .default, reuseIdentifier: "chatListCell")
            cell?.selectionStyle = .none
            //            cell?.backgroundColor = UIColor.red
            
            //add subviews
            //Set up layout for each cell
            //            self.addImageView(toCell: cell!)
            //            self.addRequestViews(toCell: cell!)
            //            self.addActionButtonViews(toCell: cell!) //for chatRequestViews
            //            self.addMessageViews(toCell: cell!)
            //Configure Chat Request Object Cells
            
            //write configuration code
            
            //MARK: ChatRequestTableView
            if objectType == "chatRequestOtherNew" {
                //print("chatRequestOtherNew")
                let displayName = chatListObject["chatRequestSenderFirstName"]!
                let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                self.chatRequestOtherNewUI(cell: cell!, nameLabel: displayName, urlString: urlString)
                
                /*
                 let lastMessage = self.lastMessageArray["selectedUID"]
                 observe
                 */
                
            } else if objectType == "chatRequestSelfNew" {
                //print("chatRequestSelfNew is")
                //print("!!! object is,", chatListObject)
                let displayName = chatListObject["requestToDisplayName"]!
                let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                //print("!!! displayName", displayName)
                //print("!!! recipientURLString", recipientURLString)
                
                //do not show UI case but just test for now
                self.chatRequestSelfNewUI(cell: cell!, urlString: recipientURLString)
                
            } else if objectType == "otherInitChatApprovedNew" {
                let displayName = chatListObject["chatRequestSenderFirstName"]!
                let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                
                //convert to otherInitChatApprovedPre if selected USer has already instantiated a chatThread right after we newly approve their request
                let selectedUID = chatListObject["requestToUID"]!
                if self.lastMessagesbyUID.keys.contains(selectedUID) {
                    let lastMessage = self.lastMessagesbyUID[selectedUID]!
                    self.otherInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: urlString, lastMessage: lastMessage)
                } else {
                    self.otherInitChatApprovedNewUI(cell: cell!, urlString: urlString)
                }
                
            } else if objectType == "selfInitChatApprovedNew" {
                let displayName = chatListObject["requestToDisplayName"]!
                let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                self.selfInitChatApprovedNewUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString)
                
            } else if objectType == "otherInitChatApprovedPre" {
                let displayName = chatListObject["chatRequestSenderFirstName"]!
                let urlString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                //            let lastMessageString = chatListObject["lastMessageString"]!
                let selectedUID = chatListObject["requestToUID"]!
                if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                    //
                    self.otherInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: urlString, lastMessage: lastMessageString)
                }
                //            self.otherInitChatApprovedPreUI(cell: cell, nameLabel: displayName, urlString: urlString, lastMessage: lastMessageString)
                
                //problem: Why is CLVC observing a newly approved chatRequest as selfInitChatApprovedPre vs. selfInitChatApprovedNew? Should observe as selfInitChatApprovedNew
                
            } else if objectType == "selfInitChatApprovedPre" {
                let displayName = chatListObject["requestToDisplayName"]!
                let recipientURLString = chatListObject["chatRequestWithDefaultPhotoURL"]!
                //            let lastMessageString = chatListObject["lastMessageString"]!
                let selectedUID = chatListObject["requestToUID"]!
                
                if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                    //
                    self.selfInitChatApprovedPreUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString, lastMessage: lastMessageString)
                }
                
            }
            
            if objectType == "generalChatThread" {
                //print("cellForRowAt objectType generalChatThread")
                
                let displayName = chatListObject["chatWithDisplayName"]!
                let recipientURLString = chatListObject["chatWithPhotoURL"]!
                
                //print("!recipientURLString ,", recipientURLString)
                
                //            let lastMessageString = chatListObject["lastMessage"]!
                let selectedUID = chatListObject["chatWithUID"]!
                //print("selectedUID", selectedUID)
                //print("self.lastMessagesbyUID is,", lastMessagesbyUID)
                let visus = chatListObject["chatWithVisus"]!
                //print("!visus ,", visus)
                
                if let lastMessageString = self.lastMessagesbyUID[selectedUID] {
                    //
                    self.generalChatThreadUI(cell: cell!, nameLabel: displayName, urlString: recipientURLString, lastMessage: lastMessageString, otherUID: selectedUID, userVisus: visus)
                } else {

                }
            }
            
            //MARK: SwapRequestTableView
            if chatListObject.keys.contains("swapRequestID") {
                let othDisplayName = chatListObject["swapWithDisplayName"]!
                let photoUrlString = chatListObject["swapWithDefaultPhotoURL"]!
                let userID = chatListObject["swapWithUID"]!
                let requestID = chatListObject["swapRequestID"]!
                
                //Configure Swap Object Cells
                if objectType == "swapRequestOtherNew" {
                    //print("swapRequestOtherNewUI")
                    //let urlString = chatListObject["swapInitiatorDefaultURL"]!
                    
                    self.swapRequestOtherNewUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, withRequestID: requestID, withUID: userID)
                    
                } else if objectType == "swapRequestSelfNew" {
                    //print("swapRequestSelfNew is")
                    //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                    self.swapRequestSelfNewUI(cell: cell!, urlString: photoUrlString)
                    
                } else if objectType == "otherInitSwapApprovedNew" {
                    //let urlString = chatListObject["swapInitiatorDefaultURL"]!
                    let withUID = chatListObject["swapWithUID"]!
                    self.otherInitSwapApprovedNewUI(cell: cell!, urlString: photoUrlString, otherUID: withUID)
                    
                } else if objectType == "selfInitSwapApprovedNew" {
                    //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                    //let displayName = chatListObject["swapRecipientFirstName"]!
                    let withUID = chatListObject["swapWithUID"]!
                    self.selfInitSwapApprovedNewUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, otherUID: withUID, withRequestID: requestID)
                    
                } else if objectType == "otherInitSwapApprovedPre" {
                    //let urlString = chatListObject["swapInitiatorDefaultURL"]!
//                    let lastMessageString = chatListObject["lastMessage"]!
                    var lastMessageString = ""
                    if let m = self.lastMessagesbyUID[userID] {
                        lastMessageString = m
                    }
                    let withUID = chatListObject["swapWithUID"]!
                    self.otherInitSwapApprovedPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
                    
                } else if objectType == "selfInitSwapApprovedPre" {
                    //let recipientURLString = chatListObject["swapRecipientDefaultURL"]!
                    //let displayName = chatListObject["swapRecipientFirstName"]!
//                    let lastMessageString = chatListObject["lastMessage"]!
                    var lastMessageString = ""
                    if let m = self.lastMessagesbyUID[userID] {
                        lastMessageString = m
                    }
                    let withUID = chatListObject["swapWithUID"]!
                    self.selfInitSwapApprovedPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
                }
                

            }
            
            //MARK: OneWaySwapRequestTableView
            var oneWaySwapObjectTypes = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre"]
            
            if chatListObject.keys.contains("oneWayRequestID") {
                let objectType = chatListObject["objectType"]!
                
                let othDisplayName = chatListObject["revealToDisplayName"]!
                let photoUrlString = chatListObject["revealToDefaultPhotoURL"]!
                let lastMessage = chatListObject["lastMessage"]!
                let userID = chatListObject["revealToUID"]!
                let requestID = chatListObject["oneWayRequestID"]!
                
                if objectType == "incomingReveal" {
                    let requestID = chatListObject["oneWayRequestID"]!
                    self.incomingRevealUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, withRequestID: requestID, withUID: userID)
                    
                } else if objectType == "outgoingReveal" {
                    self.outgoingRevealUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, withRequestID: requestID, withUID: userID)
                    
                } else if objectType == "incomingRevealAppr" {
                    let withUID = chatListObject["revealToUID"]!
                    var lastMessageString = ""
                    if let m = self.lastMessagesbyUID[withUID] {
                        lastMessageString = m
                    }
                    
                    self.incomingRevealApprUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: withUID)
                    
                } else if objectType == "outgoingRevealAppr" {
                    let withUID = chatListObject["revealToUID"]!
                    self.outgoingRevealApprUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, otherUID: withUID, withRequestID: requestID)
                    
                } else if objectType == "outgoingRevealApprPre" {
                    let withUID = chatListObject["revealToUID"]!
                    
                    var lastMessageString = ""
                    if let m = self.lastMessagesbyUID[withUID] {
                        lastMessageString = m
                    }
                    
                    self.outgoingRevealApprPreUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, userUID: withUID)
                    
                    //PROBLEM: DO NOT APPEND OUTGOING ONE WAY SHOW TO CHAT LIST OBJECTS IF NO LAST MESSAGE EXISTS
                } else if objectType == "outgoingOneWayShow" {
                    //print("CLVC detected outgoingOneWayShow")
                    let selectedUID = chatListObject["revealToUID"]!
                    //print("selectedUID", selectedUID)
                    //print("self.lastMessagesbyUID is,", lastMessagesbyUID)
                    let lastMessageString = self.lastMessagesbyUID[selectedUID]!
                    self.outgoingOneWayShowUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: lastMessageString, otherUID: selectedUID)
                    
                } else if objectType == "incomingOneWayShow" {
                    //print("CLVC detected incomingOneWayShow")
                    
                    let selectedUID = chatListObject["revealToUID"]!
                    //print("selectedUID", selectedUID)
                    //print("self.lastMessagesbyUID is,", lastMessagesbyUID)
                    if let m = self.lastMessagesbyUID[selectedUID] {
                        self.incomingOneWayShowUI(cell: cell!, nameLabel: othDisplayName, urlString: photoUrlString, lastMessage: m, otherUID: selectedUID)
                    } else if chatListObject["fromChatRequest"]! == "yes" {
                        
                    }
                }
            }
            //if cell is of chatRequest object type

            cell!.delegate = self
        } else {
            
            
        }
        
        if let cell = cell {
            return cell
        }
        
        return ChatListTableViewCell()
    }
    
    func hideRequestViews(forCell: ChatListTableViewCell) {
        forCell.requestMainLabel.isHidden = true
        forCell.requestTimeStamp.isHidden = true
    }
    
    func showRequestViews(forCell: ChatListTableViewCell) {
        forCell.requestMainLabel.isHidden = false
        forCell.requestTimeStamp.isHidden = false
    }
    
    func hideResponseButtonsStack(forCell: ChatListTableViewCell) {
//        forCell.chatAcceptButton.isHidden = true
//        forCell.chatDismissButton.isHidden = true
    }
    
    func showChatActiveStack(forCell: ChatListTableViewCell) {
        forCell.completeSwapSymbol.isHidden = false
        forCell.userName.isHidden = false
        forCell.lastMessageLabel.isHidden = false
    }
    
    func hideChatActiveStack(forCell: ChatListTableViewCell) {
        forCell.completeSwapSymbol.isHidden = true
        forCell.userName.isHidden = true
        forCell.lastMessageLabel.isHidden = true
    }
    
    //MARK: OneWayShow UI Functions
    
    func incomingOneWayShowUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String?, otherUID: String ) {
        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true
        cell.completeSwapSymbol.isHidden = true
        
        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        
        //Update Labels
        cell.userName.text = nameLabel
        cell.lastMessageLabel.text = lastMessage
        
        //
        if let didReadLastMessage = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadLastMessage != "true" {
                cell.newMessageSymbol.isHidden = false
            } else {
                cell.newMessageSymbol.isHidden = true
            }
        } else {
            cell.newMessageSymbol.isHidden = true
        }
        
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]! != "true" {
//            cell.newMessageSymbol.isHidden = false
//        } else {
//            //
//        }
//
    }
    
    func outgoingOneWayShowUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, otherUID: String) {
        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true
        cell.completeSwapSymbol.isHidden = true
        
        //Configure Image
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        
        //Update Labels
        cell.userName.text = nameLabel
        cell.lastMessageLabel.text = lastMessage

        //
        if let didReadLastMessage = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadLastMessage != "true" {
                cell.newMessageSymbol.isHidden = false
            } else {
                cell.newMessageSymbol.isHidden = true
            }
        } else {
            cell.newMessageSymbol.isHidden = true
        }
        
        //
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]! != "true" {
//            cell.newMessageSymbol.isHidden = false
//        } else {
//            //
//        }
    }
    
    func incomingRevealUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, withRequestID: String, withUID: String) {
        
        //print("func incomingRevealUI")
        
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.showRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true
        cell.thumbnailSymbol.isHidden = false
        
        //Configure Image
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "incomingReveal")!
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "\(nameLabel)" + "\n" + "Asked to See Your Profile"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Asked to See Your Profile")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
        
        //print("withRequestID", withRequestID)
        
        //need to update seen to true
        let selfUID = self.selfUID!
        
        //SETrecipientDidSeeRequest_incoming 1WincomingRevealUI
        if !self.ref_SelfModelController.didClearSwapAlertForUserID.contains(withUID) {
            self.ref_SelfModelController.didClearSwapAlertForUserID.append(withUID)
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/recipientDidSeeRequest").setValue("true")
            self.ref_SelfModelController.didSeeSwapRequestForUID["\(withUID)"] = "true"
        }
    }
    
    func formatAttributedString(fullString: String, nameTitle: String, requestBody: String) -> NSMutableAttributedString {
//        let largeFont = UIFont(name: "Avenir-Heavy", size: 16.0)!
//        let smallFont = UIFont(name: "Avenir-Book", size: 14.0)!
        
        let largeFont = UIFont(name: "AvenirNext-DemiBold", size: 16.0)!
        let smallFont = UIFont(name: "AvenirNext-Regular", size: 14.0)!
    
        let nameRange = (fullString as NSString).range(of: nameTitle)
        let requestRange = (fullString as NSString).range(of: requestBody)

        let attributedString = NSMutableAttributedString(string: fullString)
        attributedString.addAttribute(NSAttributedStringKey.font, value: largeFont, range: nameRange)
        attributedString.addAttribute(NSAttributedStringKey.font, value: smallFont, range: requestRange)
        
        return attributedString
    }

    var nameLabelInsetFromCompletionSymbol = 5 as CGFloat
    
    //func with Pre
    func incomingRevealApprUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, otherUID: String) {
        //print("incomingRevealApprUI")

        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true
        cell.lastMessageLabel.isHidden = false
        cell.completeSwapSymbol.isHidden = true
        cell.oneWaySwapCompleteMarker.isHidden = false
        
        //Configure Image
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        
        //Update Labels
        cell.userName.text = nameLabel
        cell.lastMessageLabel.text = lastMessage
        cell.oneWaySwapCompleteMarker.image = UIImage(named: "incomingOneWaySeen")!
        cell.dotsView.isHidden = true

        let tX = cell.oneWaySwapCompleteMarker.bounds.width + nameLabelInsetFromCompletionSymbol
        cell.userName.transform = CGAffineTransform(translationX: tX, y: 0)
        
        if let didReadLastMessageForUID = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadLastMessageForUID != "true" {
                cell.newMessageSymbol.isHidden = false
            }
        }
    }
    
    //replace assets:
    /*
     1. "outgoingRevealAppr"
     2.
 */

    //outgoing
    func outgoingRevealUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, withRequestID: String, withUID: String) {
        //print("func outgoingRevealUI")
        
        //Hide Views
        cell.requestMainLabel.isHidden = false
        cell.requestTimeStamp.isHidden = false
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true
        cell.thumbnailSymbol.isHidden = false
  
        //Configure Image
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "outgoingReveal")!
        
        //Update Labels
        //Update Labels
//        cell.dotsView.isHidden = false
        let requestText = "You" + "\n" + "Asked \(nameLabel) to Show his Profile"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: "You", requestBody: "Asked \(nameLabel) to Show his Profile")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
    }
    
    func outgoingRevealApprUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, otherUID: String, withRequestID: String) {
        
        //print("func outgoingRevealApprUI")
        
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true
        cell.thumbnailSymbol.isHidden = false

        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "outgoingRevealAppr")!
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "\(nameLabel)" + "\n" + "Showed his Profile"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Showed his Profile")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
        
        //Update Initiator didSee
        //CLEARSwapRequest_Outgoing 1Way Outgoing Approved
        if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(otherUID) {
            self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(otherUID)
            self.ref.child("globalSwapRequests/\(self.selfUID!)/\(withRequestID)/initiatorDidSeeRequest").setValue("true")
            //we always set value below to the MIRRORuser of request
            self.ref_SelfModelController.didSeeSwapRequestForUID["\(otherUID)"] = "true"
        }
    }
    
    //outgoingApprPre
    func outgoingRevealApprPreUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, userUID: String) {
        //print("outgoingRevealApprPreUI")
        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true
        cell.lastMessageLabel.isHidden = false
        cell.completeSwapSymbol.isHidden = true
        cell.oneWaySwapCompleteMarker.isHidden = false
        
        //Configure Image
        //cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        if let setImage = photoVault(otherUID: userUID) {
            cell.thumbnailView.image = setImage
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        //Update Labels
        cell.userName.text = nameLabel
        cell.lastMessageLabel.text = lastMessage
        
        cell.oneWaySwapCompleteMarker.image = UIImage(named: "outgoingOneWaySeen")!

        let tX = cell.oneWaySwapCompleteMarker.bounds.width + nameLabelInsetFromCompletionSymbol
        cell.userName.transform = CGAffineTransform(translationX: tX, y: 0)
        
        if let didReadLastMessageForUID = self.ref_SelfModelController.didReadLastMessageForUID[userUID] {
            if didReadLastMessageForUID != "true" {
                cell.newMessageSymbol.isHidden = false
            }
        }
        
    }
    
    var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    func photoVault(otherUID: String) -> UIImage? {
        //print("PV1. accesing photo vault")
        let fileName = "\(otherUID).jpeg"
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            //print("PV2. Successful try UIIMage")
            return UIImage(data: imageData)
        } catch {
            //print("PV2E. Error loading image : \(error)")
        }
        return nil
    }

    //ChatRequestUIConfig

    /* Cell Var Mirror
     //Thumbnail & Symbols
     @IBOutlet weak var thumbnailSuperView: UIView!
     @IBOutlet weak var thumbnailView: UIImageView!
     @IBOutlet weak var thumbnailSymbol: UIImageView!
     
     //Chat Stack & Symbol
     @IBOutlet weak var completeSwapSymbol: UIImageView!
     @IBOutlet weak var chatActiveStack: UIStackView!
     @IBOutlet weak var userName: UILabel!
     @IBOutlet weak var lastMessageLabel: UILabel!
     
     //Request Stack
     @IBOutlet weak var requestStatusStack: UIStackView!
     @IBOutlet weak var requestMainLabel: UILabel!
     @IBOutlet weak var requestTimeStamp: UILabel!
     
     //Response Action Stack
     @IBOutlet weak var responseButtonsStack: UIStackView!
     @IBOutlet weak var chatAcceptButton: UIButton!
     @IBOutlet weak var chatDismissButton: UIButton!
     */

    func acceptChatRequest(_ sender: ChatListTableViewCell) {
        //print("acceptChatRequest")
        
        guard let indexPath = chatListTableView.indexPath(for: sender) else { return }
        
        let selectedUserObject = self.chatListObjects[(indexPath as NSIndexPath).row]
        //print("selectedUserObject is", selectedUserObject)
        
        //guard let chatRequestId = self.preexistingRequestKey else { return }
        guard let chatRequestId = selectedUserObject["chatRequestID"] else { return }
        //print("chatRequestId to modify,", chatRequestId)
        
        let selfUID = self.selfUID!
        let otherUID = selectedUserObject["requestToUID"]!
        
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/\(selfUID)").setValue("true")
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/\(selfUID)").setValue("true")
        
        ref.child("globalChatRequests/\(selfUID)/\(chatRequestId)/chatRequestStatus").setValue("true")
        ref.child("globalChatRequests/\(otherUID)/\(chatRequestId)/chatRequestStatus").setValue("true")
    }
    
    func rejectChatRequest(_ sender: ChatListTableViewCell) {
        //print("rejectChatRequest")
        
    }
    
    func toNewChatButton(_ sender: ChatListTableViewCell) {
        //print("toNewChat")
        
        guard let indexPath = chatListTableView.indexPath(for: sender) else { return }
        
        let selectedUserObject = self.chatListObjects[(indexPath as NSIndexPath).row]
        self.currentlySelectedObject = selectedUserObject
        //print("selectedUserObject is", selectedUserObject)
        
        let selectedUID = selectedUserObject["requestToUID"]!
        let selectedUserDisplayName = selectedUserObject["requestToDisplayName"]!
        //Note: You need to change this to smallURL in the future
        let selectedUserDefaultURL = selectedUserObject["chatRequestWithDefaultPhotoURL"]!
        self.createNewMessageThreadID(withUID: selectedUID, withName: selectedUserDisplayName, withPhotoURL: selectedUserDefaultURL)
    }
    
    var absExistingMessageThreadID: String?
    var temporaryMessageThreadID: String?
    
    func createNewMessageThreadID(withUID: String, withName: String, withPhotoURL: String) {
        //print("createNewMessageThreadID")
        let selfUID = Auth.auth().currentUser!.uid
        let otherUID = withUID
        
        //print("sender ID is \(selfUID), and destination ID is \(otherUID)")
        
        let ref = Database.database().reference()
        
//        self.absExistingMessageThreadID = ref.child("globalChatThreads").childByAutoId().key //might not need this
//        let absExistingMessageThreadID = self.absExistingMessageThreadID!
        
        let temporaryMessageThreadID = ref.child("globalChatThreads").childByAutoId().key //might not need this
        self.temporaryMessageThreadID = temporaryMessageThreadID
        
        let chatInitiatorUID = self.selfUID!
        let chatInitiatorDisplayName = self.selfDisplayName!
//        let chatInitiatorPhotoURL = self.selfDefaultPhotoURL!
        let chatInitiatorPhotoURL = self.myProfileInfoDict["gridThumb_URL"]!
        let chatInitiatorVisus = self.selfVisus!
        
        let chatRecipientUID = otherUID
        let chatRecipientDisplayName = withName
        let chatRecipientPhotoURL = withPhotoURL
        let chatRecipientVisus = withUID //"GO BACK"
        
        var chatThreadBody = [String:String]()
        
        chatThreadBody["\(temporaryMessageThreadID)"] = "false"
        chatThreadBody["chatIsActive"] = "true"
        
        chatThreadBody["chatInitiatorUID"] = chatInitiatorUID
        chatThreadBody["chatInitiatorDisplayName"] = chatInitiatorDisplayName
        chatThreadBody["chatInitiatorPhotoURL"] = chatInitiatorPhotoURL
        chatThreadBody["chatInitiatorVisus"] = chatInitiatorVisus

        chatThreadBody["chatRecipientUID"] = chatRecipientUID
        chatThreadBody["chatRecipientDisplayName"] = chatRecipientDisplayName
        chatThreadBody["chatRecipientPhotoURL"] = chatRecipientPhotoURL
        chatThreadBody["chatRecipientVisus"] = chatRecipientVisus

        chatThreadBody["\(selfUID)_isPresent"] = "true"
        chatThreadBody["\(chatRecipientUID)_isPresent"] = "false"
        
        //for oneWaySwaps filtering
        chatThreadBody["didAskShow"] = "false"
        
        chatThreadBody["chatThreadFromRequest"] = "true"

        chatThreadBody["lastMessage"] = ""
        
        //for thread deletion
        chatThreadBody["\(selfUID)_didDeleteThread"] = "false"
        
        let childUpdates = [
            "/users/\(selfUID)/myActiveChatThreads/\(temporaryMessageThreadID)": chatThreadBody,
            "/users/\(otherUID)/myActiveChatThreads/\(temporaryMessageThreadID)": chatThreadBody,
            ]
        
        ref.updateChildValues(childUpdates)
        self.performSegue(withIdentifier: "chatListVCtochatScreenVC", sender: self)
    }
    
     func chatRequestOtherNewUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String) {
         //print("func chatRequestOtherNewUI")
         //Hide Views
         self.hideChatActiveStack(forCell: cell)
         cell.chatAcceptButton.isHidden = false
         cell.chatDismissButton.isHidden = false
         self.showRequestViews(forCell: cell)
          //cell.newChatSymb.isHidden = true
        
         //Configure Image
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
         cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailSymbol.image = UIImage(named: "incomingChatReqSymb")
        
         //Update Labels
        cell.dotsView.isHidden = true
        let requestText = "\(nameLabel)" + "\n" + "Wants to Chat"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Wants to Chat")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
     }
    
    //not shown in UI but just test for now
     func chatRequestSelfNewUI(cell: ChatListTableViewCell, urlString: String) {
     
        //print("func chatRequestSelfNewUI")
         //Hide Views
         self.hideChatActiveStack(forCell: cell)
         self.hideResponseButtonsStack(forCell: cell)
         self.showRequestViews(forCell: cell)
          //cell.newChatSymb.isHidden = true

         //Configure Image
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
         cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailSymbol.image = UIImage(named: "outgoingChatReqSymb")
        
         //Update Labels
        cell.dotsView.isHidden = true
        let requestText = "You" + "\n" + "Request Chat"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: "You", requestBody: "Requested Chat")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
     }

    func otherInitChatApprovedNewUI(cell: ChatListTableViewCell, urlString: String) {
     
        //print("func otherInitChatApprovedNewUI")
         //Hide Views
         self.hideChatActiveStack(forCell: cell)
         self.hideResponseButtonsStack(forCell: cell)
         self.showRequestViews(forCell: cell)
        
         //Configure Image
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
         cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailSymbol.image = UIImage(named: "incomingChatReqSymbAppr")
//         cell.newChatSymb.isHidden = false
        
         //Update Labels
        cell.dotsView.isHidden = true
        let requestText = "You" + "\n" + "Approved Chat"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: "You", requestBody: "Approved Chat")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
     }
    
     func selfInitChatApprovedNewUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String) {
     
         //print("func selfInitChatApprovedNewUI")
        
         //Hide Views
         self.hideChatActiveStack(forCell: cell)
         self.hideResponseButtonsStack(forCell: cell)
//         cell.newChatSymb.isHidden = false

         //Configure Image
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
         cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailSymbol.image = UIImage(named: "outgoingChatReqSymbAppr")

         //Update Labels
        cell.dotsView.isHidden = true
        let requestText = "\(nameLabel)" + "\n" + "Approved Chat"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Approved Chat")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
     }
    
     func otherInitChatApprovedPreUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String) {
        //print("func otherInitChatApprovedPreUI")

         //Hide Views
         self.showChatActiveStack(forCell: cell)
         self.hideRequestViews(forCell: cell)
         self.hideResponseButtonsStack(forCell: cell)
         cell.thumbnailSymbol.isHidden = true
          //cell.newChatSymb.isHidden = true

         //Configure Image
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
         cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        
         //Update Labels
         cell.userName.text = nameLabel
         cell.completeSwapSymbol.image = UIImage(named: "chatReqCompleteSymb")
         //print("otherInitChatApprovedPreUI currentlySelectedObject", self.currentlySelectedObject)
         cell.lastMessageLabel.text = lastMessage
     }
     
     func selfInitChatApprovedPreUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String) {
        
        //print("func selfInitChatApprovedPreUI")

         //Hide Views
         self.showChatActiveStack(forCell: cell)
         self.hideRequestViews(forCell: cell)
         self.hideResponseButtonsStack(forCell: cell)
         cell.thumbnailSymbol.isHidden = true
          //cell.newChatSymb.isHidden = true

         //Configure Image
         cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
         cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        
         //Update Labels
         cell.completeSwapSymbol.image = UIImage(named: "chatReqCompleteSymb")
         cell.userName.text = nameLabel
         //print("selfInitChatApprovedPreUI currentlySelectedObject", self.currentlySelectedObject)
         cell.lastMessageLabel.text = lastMessage
     }
 
    //var chatIconNames = ["otherChatSymbol", "selfChatSymbol", "chatReqCompleteSymbol", "chatReqBefore", "", "", ]

    //MARK: SwapRequestUIConfig

//    //Request Stack
//    @IBOutlet weak var requestStatusStack: UIStackView!
//    @IBOutlet weak var requestMainLabel: UILabel!
//    @IBOutlet weak var requestTimeStamp: UILabel!
    
    func swapRequestOtherNewUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, withRequestID: String, withUID: String) {
        //print("func swapRequestOtherNewUI")
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true
        
        //
        self.showRequestViews(forCell: cell)
        cell.requestMainLabel.isHidden = false
        cell.thumbnailSymbol.isHidden = false
        
        //Configure Image
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
       
        cell.thumbnailSymbol.image = UIImage(named: "othSwapSymbol")
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "\(nameLabel)" + "\n" + "Requested Profile Swap"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Requested Profile Swap")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
        
        let selfUID = self.selfUID!
        
        //SETrecipientDidSeeRequest_incoming 2swapRequestOtherNewUI
        if !self.ref_SelfModelController.didClearSwapAlertForUserID.contains(withUID) {
            self.ref_SelfModelController.didClearSwapAlertForUserID.append(withUID)
            self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/recipientDidSeeRequest").setValue("true")
            self.ref_SelfModelController.didSeeSwapRequestForUID["\(withUID)"] = "true"
        }
    }
    
    func swapRequestSelfNewUI(cell: ChatListTableViewCell, urlString: String) {
        
        //print("func swapRequestSelfNew")
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true

        //
        self.showRequestViews(forCell: cell)
        cell.requestMainLabel.isHidden = false
        cell.thumbnailSymbol.isHidden = false
        
        //Configure Image
        cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "selfSwapSymbol")
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "You" + "\n" + "Requested Profile Swap"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: "You", requestBody: "Requested Profile Swap")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
    }
    
    func otherInitSwapApprovedNewUI(cell: ChatListTableViewCell, urlString: String, otherUID: String) {
        //print("func otherInitSwapApprovedNewUI")
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true

        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "swapCompleteSymbol")
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "You" + "\n" + "Completed Profile Swap"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: "You", requestBody: "Completed Profile Swap")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
    }
    
    func selfInitSwapApprovedNewUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, otherUID: String, withRequestID: String) {
        
        //print("func selfInitSwapApprovedNewUI")
        
        //Hide Views
        self.hideChatActiveStack(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
         //cell.newChatSymb.isHidden = true
        
        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailSymbol.image = UIImage(named: "swapCompleteSymbol")
        
        //Update Labels
        cell.dotsView.isHidden = false
        let requestText = "\(nameLabel)" + "\n" + "Completed Profile Swap"
        let attributedString = self.formatAttributedString(fullString: requestText, nameTitle: nameLabel, requestBody: "Completed Profile Swap")
        cell.requestMainLabel.setAttributedTitle(attributedString, for: .normal)
        
        //CLEARSwapRequest_Outgoing 2Way
        if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(otherUID) {
            self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(otherUID)
            self.ref.child("globalSwapRequests/\(self.selfUID!)/\(withRequestID)/initiatorDidSeeRequest").setValue("true")
            //we always set value below to the MIRRORuser of request
            self.ref_SelfModelController.didSeeSwapRequestForUID["\(otherUID)"] = "true"
        }
        
    }
    
    func otherInitSwapApprovedPreUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, otherUID: String) {
        //print("otherInitSwapApprovedPreUI")
        
        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true

        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            //print("otherInitSwapApprovedPreUI A1")
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            //print("otherInitSwapApprovedPreUI A3")
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        if self.incomingDismissedUIDs.contains(otherUID) {
            //print("otherInitSwapApprovedPreUI A2")
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        
        //Update Labels
        cell.userName.text = nameLabel
        cell.completeSwapSymbol.image = UIImage(named: "swappedBeforeSymbol")
        cell.lastMessageLabel.text = lastMessage
        
        let tX = cell.completeSwapSymbol.bounds.width + nameLabelInsetFromCompletionSymbol
        cell.userName.transform = CGAffineTransform(translationX: tX, y: 0)
        
        if let didReadMessage = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadMessage != "true" {
                cell.newMessageSymbol.isHidden = false
            } else {
                //
            }
        }
    }
    
    func selfInitSwapApprovedPreUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, otherUID: String) {
        //print("selfInitSwapApprovedPreUI")

        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         //cell.newChatSymb.isHidden = true

        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            //print("selfInitSwapApprovedPreUI B1")
            if let setImage = photoVault(otherUID: otherUID) {
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            //print("selfInitSwapApprovedPreUI B3")
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        if self.incomingDismissedUIDs.contains(otherUID) {
            //print("selfInitSwapApprovedPreUI B2")
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2
        cell.thumbnailView.layer.masksToBounds = true
        
        //Update Labels
        cell.completeSwapSymbol.image = UIImage(named: "swappedBeforeSymbol")
        cell.userName.text = nameLabel
        //print("lastMessage", lastMessage)
        cell.lastMessageLabel.text = lastMessage
        
        let tX = cell.completeSwapSymbol.bounds.width + nameLabelInsetFromCompletionSymbol
        cell.userName.transform = CGAffineTransform(translationX: tX, y: 0)
        
        //ATTENTION: FIX TO UNWRAP ONLY OPTIONALLY
        if let didReadLastMessage = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadLastMessage != "true" {
                cell.newMessageSymbol.isHidden = false
            } else {
                cell.newMessageSymbol.isHidden = true
            }
        } else {
            cell.newMessageSymbol.isHidden = true
        }
        
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]! != "true" {
//            cell.newMessageSymbol.isHidden = false
//        } else {
//            //
//        }
        
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]
    }
    
    var didReadLastMessageForUID = [String : String]()

    func generalChatThreadUI(cell: ChatListTableViewCell, nameLabel: String, urlString: String, lastMessage: String, otherUID: String, userVisus: String) {
        //print("setting generalChatThreadUI")
        //Hide Views
        self.showChatActiveStack(forCell: cell)
        self.hideRequestViews(forCell: cell)
        self.hideResponseButtonsStack(forCell: cell)
        cell.thumbnailSymbol.isHidden = true
         // //cell.newChatSymb.isHidden = true
        cell.completeSwapSymbol.isHidden = true
        
        //Configure Image
        if self.unlockedUsers.contains(otherUID) {
            if userVisus == "false" {
                let setImage = photoVault(otherUID: otherUID)
                cell.thumbnailView.image = setImage
            } else {
                cell.thumbnailView.sd_setImage(with: URL(string: urlString))
            }
        } else {
            cell.thumbnailView.sd_setImage(with: URL(string: urlString))
        }
        
        cell.thumbnailView.layer.masksToBounds = true
        cell.thumbnailView.layer.cornerRadius = cell.thumbnailView.frame.height/2

        //Update Labels
        cell.userName.text = nameLabel
//        cell.completeSwapSymbol.image = UIImage(named: "swappedBeforeSymbol")
        cell.lastMessageLabel.text = lastMessage
        
        //print("generalChatThreadUI didReadLastMessageForUID", self.ref_SelfModelController.didReadLastMessageForUID)
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]! != "true" {
//            cell.newMessageSymbol.isHidden = false
//        } else {
//            //
//        }
        if let didReadLastMessage = self.ref_SelfModelController.didReadLastMessageForUID[otherUID] {
            if didReadLastMessage != "true" {
                cell.newMessageSymbol.isHidden = false
            } else {
                cell.newMessageSymbol.isHidden = true
            }
        } else {
            cell.newMessageSymbol.isHidden = true
        }
    }
    
    func updateDidReadLastMessageUI(forCell: ChatListTableViewCell, withUID: String) {
//        if self.ref_SelfModelController.didReadLastMessageForUID[otherUID]! != "true" {
//            cell.newMessageSymbol.isHidden = false
//        } else {
//            //
//        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        let hwRatio = 92 / 414 as CGFloat
        let finalCellHeight = hwRatio * screenWidth
        
        //print("tableView heightForRowAt finalCellHeight", finalCellHeight)
        return finalCellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("CHATLIST tableView didSelectRowAt")
        
        // Grab the DetailVC from Storyboard
        let chatScreenViewController = self.storyboard!.instantiateViewController(withIdentifier: "ChatScreenViewController") as! ChatScreenViewController
        
        //freeze UI
         self.arrestoMomento()
        
        //SWAPOBJECT METHOD 1
        
        //get selectedUser
        let selectedObject = chatListObjects[(indexPath as NSIndexPath).row]
        //print("ACCIO, selected Object is,", selectedObject)
        //print("ACCIO objectTYpe ,", selectedObject["objectType"]!)
        self.currentlySelectedObject = selectedObject
        //we want to press the button within the cell
        //for selected 
        
        chatScreenViewController.importedChatUser = selectedObject
        chatScreenViewController.unlockedUsers = self.unlockedUsers
        chatScreenViewController.ref_SelfModelController = self.ref_SelfModelController
        chatScreenViewController.sourceVC = "ChatListViewController"
        chatScreenViewController.didPresentFromChatList = true
        //pass these values only of object is of SwapType
        if selectedObject.keys.contains("swapWithUID") {
            
            let selectedObjectUID = selectedObject["swapWithUID"]
            
            chatScreenViewController.swapRequestDoesExist = true
            chatScreenViewController.absExistingSwapRequestID = selectedObject["swapRequestID"]!
            if selectedObject["swapInitiatorID"] == self.selfUID! {
                chatScreenViewController.selfInitiatedSwapRequest = true
            } else {
                chatScreenViewController.selfInitiatedSwapRequest = false
            }
            
             /*
            var selectedUserInfoDict = [String: String]()
            selectedUserInfoDict["UID"] = selectedObject["swapWithUID"]
            selectedUserInfoDict["visus"] = selectedObject["swapWithVisus"]
            
            if selectedObject["swapInitiatorID"] == self.selfUID! {
                selectedUserInfoDict["userDisplayName"] = selectedObject["swapRecipientFirstName"]
                selectedUserInfoDict["userThumbnailURLString"] = selectedObject["swapRecipientDefaultURL"]
                chatScreenViewController.selfInitiatedSwapRequest = true
            } else {
                selectedUserInfoDict["userDisplayName"] = selectedObject["swapInitiatorFirstName"]
                selectedUserInfoDict["userThumbnailURLString"] = selectedObject["swapInitiatorDefaultURL"]
                chatScreenViewController.selfInitiatedSwapRequest = false
            }
            */
            
            //print("SEGUE chatListVCtochatScreenVC selectedUserInfoDict,", selectedObject)
        } else {
            chatScreenViewController.swapRequestDoesExist = false
        }
        
        if selectedObject["objectType"] == "otherInitSwapApprovedNew" || selectedObject["objectType"] == "otherInitSwapApprovedPre" ||  selectedObject["objectType"] == "selfInitSwapApprovedNew" || selectedObject["objectType"] == "selfInitSwapApprovedNew" {
            //print("ACCIOswapRequestStatus, true")
            chatScreenViewController.swapRequestStatus = "true"
            
        } else if selectedObject["objectType"] == "swapRequestOtherNew" || selectedObject["objectType"] == "swapRequestSelfNew"  {
            //print("ACCIOswapRequestStatus, false")
            chatScreenViewController.swapRequestStatus = "false"
        }
        
        //pass info when object is chatRequestType
        if selectedObject.keys.contains("chatRequestID") {
            //print("1Confirm selected object is chatRequestType")

            if let absExistingMessageThreadID = selectedObject["existingChatThreadID"] {
                //print("2Confirm  selected existingChatThreadID", absExistingMessageThreadID)
                chatScreenViewController.absExistingMessageThreadID = absExistingMessageThreadID
                chatScreenViewController.absExistingChatRequestID = selectedObject["chatRequestID"]!
            }
            
            //NEED TO ALSO SET SWAP STATE WHEN CHATLIST OBJECT IS A NEWLY ACCEPTED CHAT REQUEST
            chatScreenViewController.swapRequestDoesExist = false
        }
        
        //pass info when object is generalChatObject
        if selectedObject["objectType"] == "generalChatThread" {
            chatScreenViewController.absExistingMessageThreadID = selectedObject["chatThreadID"]!
        }
        
        if selectedObject.keys.contains("oneWayRequestID") {
            //print("CLVC PASSING 1way swap request")
            chatScreenViewController.swapRequestDoesExist = true
            chatScreenViewController.swapRequestStatus = selectedObject["requestStatus"]!
            if selectedObject["requestInitiatorID"]! == self.selfUID! {
                chatScreenViewController.selfInitiatedSwapRequest = true
            } else {
                chatScreenViewController.selfInitiatedSwapRequest = false
            }
        }
    
        chatScreenViewController.presentingSegueIdentifier = "chatListVCtochatScreenVC"
//        chatScreenViewController.myProfileInfoDict = self.myProfileInfoDict
        chatScreenViewController.incomingDismissedUIDs = self.incomingDismissedUIDs

        var validSegueObjectTypes = ["otherInitChatApprovedPre", "otherInitChatApprovedNew", "selfInitChatApprovedPre", "swapRequestOtherNew", "swapRequestSelfNew", "otherInitSwapApprovedNew", "otherInitSwapApprovedPre", "selfInitSwapApprovedPre", "selfInitSwapApprovedNew", "selfInitSwapApprovedNew", "generalChatThread", "incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre", "outgoingOneWayShow", "incomingOneWayShow"]
    
        let selectedObjectType = selectedObject["objectType"]!
        //print("selectedObjectType is,", selectedObjectType)
        
        if validSegueObjectTypes.contains(selectedObjectType) {
//
//            let transition = CATransition()
//            transition.duration = 0.5
//            transition.type = kCATransitionPush
//            transition.subtype = kCATransitionFromRight
//            view.window!.layer.add(transition, forKey: kCATransition)
            
            let src = self
            let dst = chatScreenViewController
            
            src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
            dst.view.transform = CGAffineTransform(translationX: src.view.frame.size.width, y: 0) //Method call changed
            UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
            }) { (finished) in
                self.present(chatScreenViewController, animated: false, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            
            let selfUID = self.selfUID!
            
            let i = indexPath
            let row = indexPath.row
            let chatListObject = self.chatListObjects[row]
            
            //remove from array
            self.chatListObjects.remove(at: row)
            
            //remove from view
            self.chatListTableView!.deleteRows(at: [i], with: .left)
            
            //remove from DB
            //determine object type & remove entry accordingly
            //a 2way swap type
            if chatListObject.keys.contains("swapWithUID") {
                //print("CHATLISTTABLEWILLDELETE 2way swap type")
                let otherID = chatListObject["swapWithUID"]!
                let swapID = chatListObject["swapRequestID"]!
                let chatThreadID = chatListObject["chatThreadID"]!
                //print("chatThreadID from deleteTableObject swap2way,", chatThreadID)
                
//                self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//                self.ref.child("globalSwapRequests/\(otherID)/\(swapID)").removeValue()
//                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)").setValue("false")
//                self.ref.child("users/\(selfUID)/deletedChats/\(chatThreadID)").setValue(otherID)
//                self.ref.child("users/\(selfUID)/outgoingSwapRequests/\(swapID)").removeValue()
                
                let batchUpdates = [
                    "globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive" : "nil",
                    "globalSwapRequests/\(otherID)/\(swapID)/swapIsActive" : "nil",
                    "users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)" : "false",
                    "users/\(selfUID)/deletedChats/\(chatThreadID)" : otherID,
                ]

                let metaUpdates = [
                    "users/\(selfUID)/outgoingSwapRequests/\(swapID)" : NSNull(),
                ]

                if let index = self.allUniqueUIDsinChatList.index(of: otherID) {
                    self.allUniqueUIDsinChatList.remove(at: index)
                }
                
                self.ref.updateChildValues(batchUpdates)
                self.ref.updateChildValues(metaUpdates)
                
                self.getAllMessagesForThreadID(chatThreadID: chatThreadID)
            }
            
            //b 1way swap type
            if chatListObject.keys.contains("revealToUID") {
                //print("CHATLISTTABLEWILLDELETE 1way swap type")
                let otherID = chatListObject["revealToUID"]!
                let swapID = chatListObject["oneWayRequestID"]!
                let chatThreadID = chatListObject["chatThreadID"]!
                //print("chatThreadID from deleteTableObject swap1way,", chatThreadID)
                
//                self.ref.child("globalSwapRequests/\(selfUID)/\(swapID)").removeValue()
//                self.ref.child("globalSwapRequests/\(otherID)/\(swapID)").removeValue()
//                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)").setValue("false")
//                self.ref.child("users/\(selfUID)/deletedChats/\(chatThreadID)").setValue(otherID)
//                self.ref.child("users/\(selfUID)/outgoingSwapRequests/\(swapID)").removeValue()
                
                let batchUpdates = [
                    "globalSwapRequests/\(selfUID)/\(swapID)/swapIsActive" : "nil",
                    "globalSwapRequests/\(otherID)/\(swapID)/swapIsActive" : "nil",
                    "users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)" : "false",
                    "users/\(selfUID)/deletedChats/\(chatThreadID)" : otherID,
                    ]
                
                let metaUpdates = [
                    "users/\(selfUID)/outgoingSwapRequests/\(swapID)" : NSNull(),
                    ]
                
                if let index = self.allUniqueUIDsinChatList.index(of: otherID) {
                    self.allUniqueUIDsinChatList.remove(at: index)
                }
                
                self.ref.updateChildValues(batchUpdates)
                self.ref.updateChildValues(metaUpdates)
                
                self.getAllMessagesForThreadID(chatThreadID: chatThreadID)
            }
            
            //c chat type
            if chatListObject.keys.contains("chatWithUID") {
                //print("CHATLISTTABLEWILLDELETE chatObect type")
                let otherID = chatListObject["chatWithUID"]!
                let chatThreadID = chatListObject["chatThreadID"]!
                //print("CHATLISTTABLEWILLDELETE chatThreadID", chatThreadID)
                
                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)").setValue("false")
                self.ref.child("users/\(selfUID)/deletedChats/\(chatThreadID)").setValue(otherID)
                
                let batchUpdates = [
                    "users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(chatThreadID)" : "false",
                    "users/\(selfUID)/deletedChats/\(chatThreadID)" : otherID,
                ]
                
                self.ref.updateChildValues(batchUpdates)

                if let index = self.allUniqueUIDsinChatList.index(of: otherID) {
                    self.allUniqueUIDsinChatList.remove(at: index)
                }
                
                ///ATTENTION: SHOULD THIS METHOD BE CALLED FOR EVERY OBJECT TUYPE?
                self.getAllMessagesForThreadID(chatThreadID: chatThreadID)
                
    //                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)").removeValue()
    //                self.ref.child("users/\(selfUID)/myActiveChatThreads/\(chatThreadID)/\(selfUID)_didDeleteThread").setValue("true")
            }
            
            //ATTN: Delete case where user performs oneWayShow and then deletes row
            //ATTN: Should entries be deleted for both parties, or one-sided?
            
//            let userIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
                //                    userInfoObject["chatWithUID"] == messageSenderUID || userInfoObject["revealToUID"] == messageSenderUID || userInfoObject["swapWithUID"] == messageSenderUID
                //                })
                //
                //                if let userIndex = userIndex {
                //                    //print("XD3")
                //
                //                    //print("XD3 willchange chatlist object", self.chatListObjects.count[userIndex])
            
//            let userIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
//                userInfoObject["chatWithUID"] == outgoingBlockUID || userInfoObject["revealToUID"] == outgoingBlockUID || userInfoObject["swapWithUID"] == outgoingBlockUID
//            })
//
//            if let userIndex = userIndex {
//                //print("AD3")
//                let indexPath = IndexPath(row: userIndex, section: 0)
//                self.chatListObjects.remove(at: userIndex)
//                self.chatListTableView!.deleteRows(at: [indexPath], with: .none)
//            }
        }
    }
    
    var deletedMessagesArray = [String]()
    var _deletedMessagesHandle: DatabaseHandle!
    
    var deletedMessagesChatThreadID: String?
    
    func getAllMessagesForThreadID(chatThreadID: String) {
        
        self.deletedMessagesChatThreadID = chatThreadID
        
        _deletedMessagesHandle = ref.child("globalChatThreads/\(chatThreadID)").observe(.childAdded , with: { (snapshot: DataSnapshot) in
            let messageID = snapshot.key
            //try to see why the array is producing duplicates, and solve in another way. Are you recalling the same observer multiple times? Check this bitch
            if !self.deletedMessagesArray.contains(messageID) {
                self.deletedMessagesArray.append(messageID)
            } else {
                
            }
            
            self.markDeletedMessages(inChatThreadID: chatThreadID)
            
            //ATTENTION: MAKE SURE THAT CALLING THIS OBSERVER IN CLVC WILL NOT CAUSE ANY PROBLEMS WITH OBSERVER AT SAME NODE IN CSVC
            
            //DONT FORGET TO ADD A DEINITIALIZER FOR THIS LISTENER
        })
    }
    
    func markDeletedMessages(inChatThreadID: String) {
        let selfUID = self.selfUID!
        
        //ATTENTION: observer will continue observing messages that are received/sent and set delete value to true
        for messageID in self.deletedMessagesArray {
            self.ref.child("globalChatThreads/\(inChatThreadID)/\(messageID)/\(selfUID)_didDelete").setValue("true")
        }
    }
    
//    var chatRequestObjectTypes = ["chatRequestOtherNew", "chatRequestSelfNew", "otherInitChatApprovedNew", "otherInitChatApprovedPre", "selfInitChatApprovedNew", "selfInitChatApprovedPre"]
//
    var swapObjectTypes = ["swapRequestOtherNew", "swapRequestSelfNew", "otherInitSwapApprovedNew", "otherInitSwapApprovedPre", "selfInitSwapApprovedNew", "selfInitSwapApprovedNew"]
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }

    var freezeFrame: UIImageView? { didSet { print("freezeFrame didSet") }}
    var arrestoImago: UIImage?
    
    //Arresto Momento is called right before Transparency View is added. Check addTransparencyView()
    
    func arrestoMomento() {
        //print("CLVC arrestoMomento")
        
        let arrestoImago = self.takeScreenshot()
        self.arrestoImago = arrestoImago
        
        let freezeFrame = UIImageView(frame: UIScreen.main.bounds)
        freezeFrame.image = arrestoImago!

        self.view.addSubview(freezeFrame)
        self.freezeFrame = freezeFrame
    }
    
    func takeScreenshot() -> UIImage? {
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        let context = UIGraphicsGetCurrentContext()
        
        layer.render(in:context!)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    var userDictArray = [[String : String]]()
    var userDictArray_UID = [String]()
    var sortedChronoDict = [(key: String, value: Double)]() { didSet { print("CVC DDsortedChronoDict", sortedChronoDict)}}
    
    var ref_SelfModelController = SelfModelController()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatListVCtochatScreenVC" {
            let controller = segue.destination as! ChatScreenViewController
            controller.presentingSegueIdentifier = "chatListVCtochatScreenVC"
//            controller.myProfileInfoDict = self.myProfileInfoDict
            //print("IN PREPAREFORSENDERMETHOD, sending currentlySelectedObject,", self.currentlySelectedObject)
        
            controller.importedChatUser = self.currentlySelectedObject
            controller.ref_SelfModelController = self.ref_SelfModelController
            controller.sourceVC = "ChatListViewController"
            self.discoverGridNavButton?.isUserInteractionEnabled = false
            
            //send temporaryMessageThread for chatRequestObjects.
            if self.currentlySelectedObject.keys.contains("chatRequestID") {
                controller.temporaryMessageThreadID = self.temporaryMessageThreadID!
                //!!!!!!!!!!!!!!!!!!! NEW AND MAY NOT WORK
                controller.swapRequestDoesExist = false
            }
            
        } else if segue.identifier == "backToGrid" {
            let controller = segue.destination as! CollectionViewController
            
            //print("CLVC pass self.chatListObjects", self.chatListObjects.count)

            controller.userDictArray = self.userDictArray
            controller.userDictArray_UID = self.userDictArray_UID
            controller.ref_SelfModelController = self.ref_SelfModelController
            controller.chronoDict = self.chronoDict
            NotificationCenter.default.post(name: Notification.Name(rawValue: "manualClearChatAlertForCVC"), object: self)

        } else if segue.identifier == "chatsToGridUnwinder" {
            let controller = segue.destination as! CollectionViewController
            
            //print("CLVC pass self.chatListObjects", self.chatListObjects.count)
            
            controller.chatListObjects = self.chatListObjects
            controller.chatListObjectIDs = self.chatListObjectIDs
//            controller.didReadLastMessageForUID = self.ref_SelfModelController.didReadLastMessageForUID
            controller.chronoDict = self.chronoDict
            controller.allUniqueUIDsinChatList = self.allUniqueUIDsinChatList
            controller.sourceController = "ChatListViewController"
            self.unwindToGrid_RemoveObservers()
            
            if !self.ref_SelfModelController.didReadLastMessageForUID.values.contains("false") {
                //clear chatAlert when all values are true
//                controller.shouldClearNewChatAlert = true
                if self.ref_SelfModelController.swapAlertIsActive == false {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearChatAlertForCVC"), object: self)
                }
                self.ref_SelfModelController.chatAlertIsActive = false
                
            } else {
                controller.shouldClearNewChatAlert = false
            }
            
        }
    }
    
    func unwindToGrid_RemoveObservers() {
        self.ref.child("globalSwapRequests/\(selfUID!)").removeAllObservers()
        self.ref.child("globalSwapRequests/\(selfUID!)").removeObserver(withHandle: self._swapRefHandle)
        self.ref.child("globalSwapRequests/\(selfUID!)").removeObserver(withHandle: self._swapRefHandleChanges)
    }
    
    
    
    
    
    
    
    //MARK: Observer Functions
    
    
    
    
    
    
    
    
    
    
    func reloadTableViewData() {
        DispatchQueue.main.async {
            //print("indispatchFUNC")
            
            self.chatListTableView.reloadData()
//            self.chatListTableView.reloadSections([0], with: .automatic)
//            //print("TABLEVIEW CHATLIST NOW CONTAINS,", self.chatListObjects.count)
        }
    }
      
    //DispatchGroup
        
    func observeLastMessages(withThreadID: String, shouldReloadFullTableView: Bool, forIndexRow: Int?) {
        //print("CLVC: observeLastMessages withThreadID,", withThreadID)
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
        
        self.ref.child("globalChatThreadMetaData/\(withThreadID)").observe(.value , with: { (snapshot: DataSnapshot) in
            //print("CLVC: observeLastMessages snapshot is,", snapshot)
            
            //self.observeLastMessageChanges(withThreadID: withThreadID)
            guard let messageThreadBody = snapshot.value as? [String:String] else { return }
            
            guard let lastMessage = messageThreadBody["chatMessage"] else { return }
            //print("observeLastMessages lastMessage is,", lastMessage)
            
            guard let messageRecipientUID = messageThreadBody["messageRecipientUID"] else { return }
            
            guard let messageSenderUID = messageThreadBody["messageSenderUID"] else { return }
            guard let recipientDidReadLastMessage = messageThreadBody["recipientDidReadLastMessage"] else { return }

            guard let messageTimeStamp = messageThreadBody["messageTimeStamp"] else { return }

            //crete last Message array
            if messageSenderUID == selfUID {
                self.lastMessagesbyUID["\(messageRecipientUID)"] = lastMessage
                self.ref_SelfModelController.lastMessagesbyUID["\(messageRecipientUID)"] = lastMessage

                //new
                self.ref_SelfModelController.didReadLastMessageForUID["\(messageRecipientUID)"] = "true"
                //
                self.updateChronoDict(forObjectUID: messageRecipientUID, withObjectTimeStamp: messageTimeStamp)

            } else if messageSenderUID != selfUID {
                self.lastMessagesbyUID["\(messageSenderUID)"] = lastMessage
                self.ref_SelfModelController.lastMessagesbyUID["\(messageSenderUID)"] = lastMessage

                //new
//                self.ref_SelfModelController.didReadLastMessageForUID["\(messageSenderUID)"] = recipientDidReadLastMessage
                
                let userInfoDict = ["withMessageSenderUID" : messageSenderUID]
                
                self.updateChronoDict(forObjectUID: messageSenderUID, withObjectTimeStamp: messageTimeStamp)

                NotificationCenter.default.post(name: Notification.Name(rawValue: "unreadMessageObserved"), object: self, userInfo: userInfoDict)
            }
            
            //print("XD1")
            //update UI for unread last messages
//            var indexPath: IndexPath?
//            if messageRecipientUID == self.selfUID! && recipientDidReadLastMessage == "false" {
//                //compare the SenderID to the chatListObjectUID to get the relevant key
//                //pay attention: chatList contains multiple object types, each one has its own unique key for UserID
//                //1. general chat thread UID: "chatWithUID", 2. 1way swap uid: "revealToUID", 3. 2way swap uid:  "swapWithUID"
//
//                print("XD2")
//
//                let userIndex = self.chatListObjects.index(where: { (userInfoObject) -> Bool in
//                    userInfoObject["chatWithUID"] == messageSenderUID || userInfoObject["revealToUID"] == messageSenderUID || userInfoObject["swapWithUID"] == messageSenderUID
//                })
//
//                if let userIndex = userIndex {
//                    print("XD3")
//
//                    print("XD3 willchange chatlist object", self.chatListObjects.count[userIndex])
//                    indexPath = IndexPath(row: userIndex, section: 0)
//                    let cellRowToChange = self.chatListTableView!.cellForRow(at: indexPath!) as! ChatListTableViewCell
//
//                    print("CLVC WILLRELOAD UNREAD MESSAGE ROW")
//                    cellRowToChange.newMessageSymbol.isHidden = false
//                    self.chatListTableView!.reloadRows(at: [indexPath!], with: .none)
//                }
//            }
            
//            print("CLVC: observeLastMessages Dictionary is now,", self.lastMessagesbyUID)
            
            if shouldReloadFullTableView == true {
                //print("WILLRELOADFULLTABLE")
                self.reloadTableViewData()
            } else {
                //Only reload specified row
                guard let indexRow = forIndexRow else { return }
                let indexPath = IndexPath(row: indexRow, section: 0)
                //print("WILLRELOADSINGLEROW")
//                self.chatListTableView!.insertRows(at: [indexPath], with: .automatic)
            }
            
//            if let indexPath = indexPath {
//                self.chatListTableView!.reloadRows(at: [indexPath], with: .none)
//            }
        })
    }
    
    var chronoDict = [String : Double]() 
    
    //problem: not observing newly created message thread within ListVC
    func observeActiveMessageThreads() {
        //print("CLVC: viewDidLoad observeActiveMessageThreads")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded , with: { (snapshot: DataSnapshot) in
            //print("CLVC: observeActiveMessageThreads snapshot is,", snapshot)
            
//            self.observeActiveMessageThreadsHelper(selfUID: selfUID, snapshot: snapshot)
            
            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
            
            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
            //print("CLVC: observeActiveMessageThreads is,", chatThreadBody)
            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
            //print("CHAIN1. threadIDActive", threadIDActive)
            
            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            //print("CHAIN2. chatInitiatorUID", chatInitiatorUID)
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
            //print("CHAIN3. chatRecipientUID", chatRecipientUID)
            
            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            //print("CHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
            //print("CHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
            
            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
            //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
            
            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
            //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
            
            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
            //print("CHAIN10. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let didAskShow = chatThreadBody["didAskShow"] else { return }
            //print("CHAIN11. chatThreadFromRequest", chatThreadFromRequest)
            
            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
            //print("CHAIN12. lastMessage", lastMessage)
            
            guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
            guard chatIsActive != "nil" else { return }
            
            var generalChatObject = [String:String]()
            generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
            
            if threadIDActive == "true" {
                
                //print("777  observeLastMessages with newChatThreadID,", newChatThreadID)
                self.observeLastMessages(withThreadID: newChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
                
                guardPoint:
                    if chatThreadFromRequest == "false" {
                    
                    guard didAskShow != "true" else {
                        //print("GUARD: object is not a one-way swap")
                        break guardPoint
                    }
                    
                    var generalChatObject = [String:String]()
                    
                    if chatInitiatorUID == selfUID  { //when chatInitiator is self, we need he chatlistinfo to be of other
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        //user info
                        generalChatObject["chatWithUID"] = chatRecipientUID
                        generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatRecipientPhotoURL
                        generalChatObject["chatWithVisus"] = chatRecipientVisus
                        
                        //chatMetaData
                        generalChatObject["chatThreadID"] = newChatThreadID
                        //                    generalChatObject["lastMessage"] = lastMessage
                        generalChatObject["objectUID"] = chatRecipientUID

                        if !self.allUniqueUIDsinChatList.contains(chatRecipientUID) {
                            self.allUniqueUIDsinChatList.append(chatRecipientUID)
//                            self.chatListObjects.append(generalChatObject)
                            self.chatListObjects.insert(generalChatObject, at: 0)
                        }
                        
                        //PRINT
//                        //print("CLVC: self.chatListObjects", self.chatListObjects.count)
                        //                    self.chatListTableView.reloadData()
                        
                    } else if chatInitiatorUID != selfUID {
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        generalChatObject["chatWithUID"] = chatInitiatorUID
                        generalChatObject["chatWithDisplayName"] = chatInitiatorDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatInitiatorPhotoURL
                        generalChatObject["chatWithVisus"] = chatInitiatorVisus
                        
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["objectUID"] = chatInitiatorUID

                        //                    generalChatObject["lastMessage"] = lastMessage
                        
                        if !self.allUniqueUIDsinChatList.contains(chatInitiatorUID) {
                            self.allUniqueUIDsinChatList.append(chatInitiatorUID)
//                            self.chatListObjects.append(generalChatObject)
                            self.chatListObjects.insert(generalChatObject, at: 0)
                        }
                        
                        //PRINT
                        //print("CLVC: self.chatListObjects", self.chatListObjects.count)
                        //                    self.chatListTableView.reloadData()
                    }
                }
            }
            
            //print("WillNowGuard threadIDActive")
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            guard threadIDActive == "true" else { return }
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            let globalRequestsChatThreadID = newChatThreadID
            //print("CHAIN12. globalRequestsChatThreadID", globalRequestsChatThreadID)
            
            //print("CLVC: observeLastMessages")
            self.observeLastMessages(withThreadID: globalRequestsChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
            
            //        print("CHAIN13. sending to last message observer", globalRequestsChatThreadID)
            //        print("CLVC: END GENERALCHATTHREADOBSERVER,", self.chatListObjects.count)
            //        self.chatListTableView.reloadData()
        })
    }
    
    func observeActiveMessageThreadChanges() {
        //print("viewDidLoad observeActiveMessageThreadChanges")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            //print("observeActiveMessageThreadChanges snapshot is,", snapshot)
            
            let newChatThreadID = snapshot.key //chatThreadID observed in local user folder
            
            guard let chatThreadBody = snapshot.value as? [String:String] else { return }
            //print("CLVC: observeActiveMessageThreads is,", chatThreadBody)
            guard let threadIDActive = chatThreadBody["\(newChatThreadID)"] else { return }
            //print("CHAIN1. threadIDActive", threadIDActive)
            
            //UID
            guard let chatInitiatorUID = chatThreadBody["chatInitiatorUID"] else { return }
            //print("CHAIN2. chatInitiatorUID", chatInitiatorUID)
            guard let chatRecipientUID = chatThreadBody["chatRecipientUID"] else { return }
            //print("CHAIN3. chatRecipientUID", chatRecipientUID)
            
            //DisplayName
            guard let chatInitiatorDisplayName = chatThreadBody["chatInitiatorDisplayName"] else { return }
            //print("CHAIN4. chatInitiatorDisplayName", chatInitiatorDisplayName)
            guard let chatRecipientDisplayName = chatThreadBody["chatRecipientDisplayName"] else { return }
            //print("CHAIN5. chatRecipientDisplayName", chatRecipientDisplayName)
            
            //PhotoURL
            guard let chatInitiatorPhotoURL = chatThreadBody["chatInitiatorPhotoURL"] else { return }
            //print("CHAIN6. chatInitiatorPhotoURL", chatInitiatorPhotoURL)
            guard let chatRecipientPhotoURL = chatThreadBody["chatRecipientPhotoURL"] else { return }
            //print("CHAIN7. chatRecipientPhotoURL", chatRecipientPhotoURL)
            
            //Visus
            guard let chatInitiatorVisus = chatThreadBody["chatInitiatorVisus"] else { return }
            //print("CHAIN8. chatInitiatorVisus", chatInitiatorVisus)
            guard let chatRecipientVisus = chatThreadBody["chatRecipientVisus"] else { return }
            //print("CHAIN9. chatRecipientVisus", chatRecipientVisus)
            
            //Property observes whether or not objectType is generalChat or chatRequest
            guard let chatThreadFromRequest = chatThreadBody["chatThreadFromRequest"] else { return }
            //print("CHAIN10. chatThreadFromRequest", chatThreadFromRequest)
            
            guard let didAskShow = chatThreadBody["didAskShow"] else { return }
            //print("CHAIN11. chatThreadFromRequest", chatThreadFromRequest)
            
            //Message MetaData
            guard let lastMessage = chatThreadBody["lastMessage"] else { return }
            //print("CHAIN12. lastMessage", lastMessage)
            
            guard let chatIsActive = chatThreadBody["chatIsActive"] else { return }
            guard chatIsActive != "nil" else { return }
            
            var generalChatObject = [String:String]()
            generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
            
            if threadIDActive == "true" {
                
                guardPoint:
                    if chatThreadFromRequest == "false" {
                    
                    guard didAskShow != "true" else {
                        //print("GUARD: object is not a one-way swap")
                        break guardPoint
                    }
                    
                    var generalChatObject = [String:String]()
                    
                    if chatInitiatorUID == selfUID  { //when chatInitiator is self, we need he chatlistinfo to be of other
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        //user info
                        generalChatObject["chatWithUID"] = chatRecipientUID
                        generalChatObject["chatWithDisplayName"] = chatRecipientDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatRecipientPhotoURL
                        generalChatObject["chatWithVisus"] = chatRecipientVisus
                        
                        //chatMetaData
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["objectUID"] = chatRecipientUID

                        if !self.allUniqueUIDsinChatList.contains(chatRecipientUID) {
                            self.allUniqueUIDsinChatList.append(chatRecipientUID)
                            self.chatListObjects.insert(generalChatObject, at: 0)
//                            let indexPath = IndexPath(row: 0, section: 0)
                        }
                        
                        //PRINT
                        //print("CLVC: self.chatListObjects", self.chatListObjects.count)
                        //                    self.chatListTableView.reloadData()
                        
                    } else if chatInitiatorUID != selfUID {
                        generalChatObject["objectType"] = "generalChatThread"
                        
                        generalChatObject["chatWithUID"] = chatInitiatorUID
                        generalChatObject["chatWithDisplayName"] = chatInitiatorDisplayName
                        generalChatObject["chatWithPhotoURL"] = chatInitiatorPhotoURL
                        generalChatObject["chatWithVisus"] = chatInitiatorVisus
                        
                        generalChatObject["chatThreadID"] = newChatThreadID
                        generalChatObject["objectUID"] = chatInitiatorUID

                        //                    generalChatObject["lastMessage"] = lastMessage
                        
                        if !self.allUniqueUIDsinChatList.contains(chatInitiatorUID) {
                            self.allUniqueUIDsinChatList.append(chatInitiatorUID)
//                            self.chatListObjects.append(generalChatObject)
                            self.chatListObjects.insert(generalChatObject, at: 0)
//                            let indexPath = IndexPath(row: 0, section: 0)
                        }
                        
                        //PRINT
                        //print("CLVC: self.chatListObjects", self.chatListObjects.count)
                        //                    self.chatListTableView.reloadData()
                    }
                }
            }
            
            //print("WillNowGuard threadIDActive")
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            guard threadIDActive == "true" else { return }
            ///////?!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            
            let globalRequestsChatThreadID = newChatThreadID
            //print("CHAIN12. globalRequestsChatThreadID", globalRequestsChatThreadID)
            
            //print("CLVC: observeLastMessages")
            self.observeLastMessages(withThreadID: globalRequestsChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
            
            //        //print("CHAIN13. sending to last message observer", globalRequestsChatThreadID)
            //        //print("CLVC: END GENERALCHATTHREADOBSERVER,", self.chatListObjects.count)
            //        self.chatListTableView.reloadData()
        })
    }
    
    func observeActiveMessageThreadsHelper(selfUID: String, snapshot: DataSnapshot) {
        
       
    }
    
    func updateChronoDict(forObjectUID: String, withObjectTimeStamp: String) {
        //print("updateChronoDict")
        
        let newTimeStamp = Double(withObjectTimeStamp)!
        
        //for timestamp
        if self.chronoDict.keys.contains(forObjectUID) {
            //chronoDict may already contain instance appended from swapObject, if so, compare stamps & append larger value
            let existingTimeStamp = self.chronoDict["\(forObjectUID)"]!
            
            if newTimeStamp > existingTimeStamp {
                self.chronoDict[forObjectUID] = Double(newTimeStamp)
            } else {
                //
            }
            
            //print("chronoStamp doesExist,", existingTimeStamp)
            
        } else {
            //print("chronoStamp doesNotExist")
            self.chronoDict[forObjectUID] = newTimeStamp
        }
        
        //only called in CVC copy of this method
//        if !self.uidKeysInLastMessagesDict_Array.contains(forObjectUID) {
//            self.uidKeysInLastMessagesDict_Array.append(forObjectUID)
//        }
    }
    
    //MARK: Chat observers
    
    func observeActiveChatRequests() {
        //print("CLVC observeActiveChatRequests")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            guard let newChatRequest = snapshot.value as? [String:String] else { return }
            
            //Key
            let newChatRequestID = snapshot.key
            
            //UID
            guard let chatRequestSenderUID = newChatRequest["chatRequestSenderUID"] else { return }
            guard let chatRequestRecipientUID = newChatRequest["chatRequestRecipientUID"] else { return }
            
            //First Name
            guard let chatRequestSenderFirstName = newChatRequest["chatRequestSenderFirstName"] else { return }
            guard let chatRequestRecipientFirstName = newChatRequest["chatRequestRecipientFirstName"] else { return }
            
            //DidOpen
            guard let chatRequestSenderDidOpen = newChatRequest["chatRequestSenderDidOpen"] else { return }
            guard let chatRequestRecipientDidOpen = newChatRequest["chatRequestRecipientDidOpen"] else { return }
            
            //PhotoURL
            guard let chatRequestSenderDefaultPhotoURL = newChatRequest["chatRequestSenderDefaultPhotoURL"] else { return }
            guard let chatRequestRecipientDefaultPhotoURL = newChatRequest["chatRequestRecipientDefaultPhotoURL"] else { return }
            
            //Visus
            guard let chatRequestSenderVisus = newChatRequest["chatRequestSenderVisus"] else { return }
            guard let chatRequestRecipientVisus = newChatRequest["chatRequestRecipientVisus"] else { return }
            
            //ChatMetaData
            //            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return }
            //            guard let lastMessageString = newChatRequest["lastMessageString"] else { return }
            //RECONSTITUE
            guard let chatThreadDoesExist = newChatRequest["chatThreadDoesExist"] else { return } //bool
            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return } //string
            guard let lastMessageString = newChatRequest["lastMessageString"] else { return } //string
            
            //
            //            if chatThreadDoesExist == "true" {
            //                self.observeLastMessages(withThreadID: "\(existingChatThreadID)")
            //            }
            //
            
            //RequestStatus
            guard let chatRequestStatus = newChatRequest["chatRequestStatus"] else { return }
            
            //Swapped?
            guard let didSwap = newChatRequest["didSwap"] else { return }
            
            //for ProfileVC
            if chatRequestSenderUID == selfUID {
                
                //                self.selfInitiatedChatRequestsIDsDict[chatRequestRecipientUID] = newChatRequestID
                //                self.chatRequestPermissionsDictionary[chatRequestRecipientUID] = chatRequestStatus
                
            } else {
                //add incoming request to chatPermissionsDictionary with default value as false
                //                self.chatRequestPermissionsDictionary[chatRequestSenderUID] = chatRequestStatus
                
                //create array that links each chatUserRequester to the IDRequest they initiated
                //                self.senderUIDtoChatRequestIDExtrapDict[chatRequestSenderUID] = newChatRequestID
            }
            
            //SwapObjects for ChatlistVC
            //CR1. Incoming Chat Requests
            if chatRequestRecipientUID == selfUID && chatRequestStatus == "false" {
                //print("Incoming Chat Request")
                
                var newListObject = [String:String]()
                
                //request info
                newListObject["objectType"] = "chatRequestOtherNew"
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestSenderUID
                newListObject["requestToDisplayName"] = chatRequestSenderFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestSenderVisus
                
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen
                
                //are these duplicates of the above lines? TEST.
                
                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestSenderDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                
                newListObject["chatRequestRecipientFirstName"] = chatRequestRecipientFirstName
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["objectUID"] = chatRequestRecipientUID

                
                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    //do not append objects where users have swapped. These objects are accounted for as SwapObjectTypes
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                    
                    self.reloadTableViewData()
                }
                
                //append UID where swapper != selfUID
                if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                }
            }
            
            //CR2. Outgoing Chat Requests
            if chatRequestSenderUID == selfUID && chatRequestStatus == "false" {
                //print("Outgoing Chat Request")
                
                var newListObject = [String:String]()
                
                //request info
                newListObject["objectType"] = "chatRequestSelfNew"
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestRecipientUID
                newListObject["requestToDisplayName"] = chatRequestRecipientFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestRecipientVisus
                
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen
                
                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = "You"
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen //tracking swapInitiator since sender is self
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["chatRequestRecipientDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                
                newListObject["objectUID"] = chatRequestRecipientUID

                
                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(chatRequestRecipientUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestRecipientUID)
                }
                //
                self.reloadTableViewData()

            }
            
            //CR3. Incoming Chat Requests Approved
            if chatRequestRecipientUID == selfUID && chatRequestStatus == "true" {
                
                var newListObject = [String:String]()
                
                if chatThreadDoesExist == "false" {
                    newListObject["objectType"] = "otherInitChatApprovedNew"
                    //print("CLVC Detected 2way otherInitChatApprovedNew")
                } else if chatThreadDoesExist != "false" {
                    newListObject["objectType"] = "otherInitChatApprovedPre"
                    //print("CLVC Detected 2way otherInitChatApprovedPre")
                }
                
                //request info
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestSenderUID
                newListObject["requestToDisplayName"] = chatRequestSenderFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["requestToVisus"] = chatRequestSenderVisus
                
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen
                
                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestSenderDefaultPhotoURL"] = chatRequestSenderDefaultPhotoURL
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen

                newListObject["objectUID"] = chatRequestSenderUID
                
                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(chatRequestSenderUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestSenderUID)
                }
                
                self.reloadTableViewData()

                
            }
            
            //CR4. Outgoing Chat Requests Approved
            if chatRequestSenderUID == selfUID && chatRequestStatus == "true" {
                //print("Detected CHAT case where someone approved my previously initiated request")
                
                var newListObject = [String:String]()
                
                if chatThreadDoesExist == "false" {
                    newListObject["objectType"] = "selfInitChatApprovedNew"
                } else if chatThreadDoesExist != "false" {
                    newListObject["objectType"] = "selfInitChatApprovedPre"
                }
                
                //request info
                newListObject["chatRequestID"] = newChatRequestID
                newListObject["requestStatus"] = chatRequestStatus
                
                //profile info
                newListObject["requestToUID"] = chatRequestRecipientUID
                newListObject["requestToVisus"] = chatRequestRecipientVisus
                newListObject["requestToDisplayName"] = chatRequestRecipientFirstName
                newListObject["chatRequestWithDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                
                //chatMetaInfo
                newListObject["chatThreadDoesExist"] = chatThreadDoesExist
                newListObject["existingChatThreadID"] = existingChatThreadID
                newListObject["lastMessageString"] = lastMessageString
                
                //UIMarkers
                newListObject["selfDidOpen"] = chatRequestRecipientDidOpen
                
                newListObject["chatRequestRecipientUID"] = chatRequestRecipientUID
                newListObject["chatRequestSenderUID"] = chatRequestSenderUID
                newListObject["chatRequestSenderFirstName"] = chatRequestSenderFirstName
                newListObject["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                newListObject["chatRequestSenderDidOpen"] = chatRequestSenderDidOpen
                
                //tracking swapInitiator since sender is self //careful with these, need to reset all to default getter values
                
                newListObject["chatRequestRecipientFirstName"] = chatRequestRecipientFirstName
                newListObject["chatRequestRecipientDefaultPhotoURL"] = chatRequestRecipientDefaultPhotoURL
                
                newListObject["objectUID"] = chatRequestRecipientUID
                
                if !self.chatListObjectIDs.contains(newChatRequestID) {
                    guard didSwap != "true" else { return }
                    self.chatListObjectIDs.append(newChatRequestID)
                    self.chatListObjects.append(newListObject)
                }
                
                if !self.allUniqueUIDsinChatList.contains(chatRequestRecipientUID) {
                    self.allUniqueUIDsinChatList.append(chatRequestRecipientUID)
                }
                
                self.reloadTableViewData()

                
            }
            //print("END observeActiveChatRequests() chatListObjects now contains", self.chatListObjects.count)
            
        })
    }
    
    func observeChatRequestStatusChanges() {
        //print("CLVC: func observeChatRequestStatusChanges")
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        self.ref.child("globalChatRequests/\(user.uid)").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            
            guard let newChatRequest = snapshot.value as? [String:String] else { return }
            
            //Key
            let existingChatRequestID = snapshot.key
            
            //UID
            guard let chatRequestSenderUID = newChatRequest["chatRequestSenderUID"] else { return }
            //print("Change1")
            guard let chatRequestRecipientUID = newChatRequest["chatRequestRecipientUID"] else { return }
            //print("Change2")
            
            //First Name
            guard let chatRequestSenderFirstName = newChatRequest["chatRequestSenderFirstName"] else { return }
            //print("Change3")
            
            guard let chatRequestRecipientFirstName = newChatRequest["chatRequestRecipientFirstName"] else { return }
            //print("Change4")
            
            //DidOpen
            guard let chatRequestSenderDidOpen = newChatRequest["chatRequestSenderDidOpen"] else { return }
            //print("Change5")
            
            guard let chatRequestRecipientDidOpen = newChatRequest["chatRequestRecipientDidOpen"] else { return }
            //print("Change6")
            
            //PhotoURL
            guard let chatRequestSenderDefaultPhotoURL = newChatRequest["chatRequestSenderDefaultPhotoURL"] else { return }
            //print("Change7")
            
            guard let chatRequestRecipientDefaultPhotoURL = newChatRequest["chatRequestRecipientDefaultPhotoURL"] else { return }
            //print("Change8")
            
            //status
            guard let modifiedChatRequestStatus = newChatRequest["chatRequestStatus"] else { return }
            //print("Change9")
            
            //Chat MetaData
            guard let chatThreadDoesExist = newChatRequest["chatThreadDoesExist"] else { return } //bool
            //print("Change10")
            
            guard let existingChatThreadID = newChatRequest["existingChatThreadID"] else { return } //string
            //print("Change11")
            
            guard let lastMessageString = newChatRequest["lastMessageString"] else { return } //string
            //print("Change12")
            
            guard let didSwap = newChatRequest["didSwap"] else { return }
            //print("Change12 Changes, didSwap,", didSwap)
            
            if self.chatListObjectIDs.contains(existingChatRequestID) {
                //print("chatListObjectIDs contains existingChatRequestID")
                
                let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                    dictionaryOfInterest["chatRequestID"] == existingChatRequestID
                })
                
                //...Update chatListObjects ChatRequestInfo
                //When I approve someone's previous request
                if chatRequestRecipientUID == selfUID &&  modifiedChatRequestStatus == "true"  {
                    //print("ACCIO 2 chatListObjects", self.chatListObjects.count)
                    
                    if chatRequestRecipientDidOpen == "false" {
                        //print("CLVC Converting status otherInitChatApprovedNew")
                        (self.chatListObjects[i!])["objectType"] = "otherInitChatApprovedNew"
                        
                    //PROBLEM: CLVC observer is observing detection that self did open when it did not
                    } else if chatRequestRecipientDidOpen == "true" {
                        //print("CLVC Converting status otherInitChatApprovedPre")
                        (self.chatListObjects[i!])["objectType"] = "otherInitChatApprovedPre"
                    }
                    
                    (self.chatListObjects[i!])["chatThreadDoesExist"] = chatThreadDoesExist
                    (self.chatListObjects[i!])["existingChatThreadID"] = existingChatThreadID
                    (self.chatListObjects[i!])["lastMessageString"] = lastMessageString
                    
                    self.observeLastMessages(withThreadID: existingChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
                    self.reloadTableViewData()
                }
                
                //Self initiated chat requests that have been approved by other
                if chatRequestSenderUID == selfUID  && modifiedChatRequestStatus == "true" {
                    //that have been opened
                    if chatRequestSenderDidOpen == "false" {
                        //print("CLVC setting selfInitChatApprovedNew")
                        (self.chatListObjects[i!])["objectType"] = "selfInitChatApprovedNew"
                        
                    } else if chatRequestSenderDidOpen == "true" {
                        //print("CLVC setting selfInitChatApprovedPre")
                        
                        //print("CLVC sdfsdf self.chatListObjects is,", self.chatListObjects.count)
                        //PROBLEM: CODE IS NOT DETECTING i INDEX //BUG
//                        //print("CLVC self.chatListObjects[i!]", self.chatListObjects[i!])
                        (self.chatListObjects[i!])["objectType"] = "selfInitChatApprovedPre"
                    }
                    
                    (self.chatListObjects[i!])["requestStatus"] = modifiedChatRequestStatus
                    (self.chatListObjects[i!])["chatRequestRecipientDidOpen"] = chatRequestRecipientDidOpen
                    (self.chatListObjects[i!])["selfDidOpen"] = chatRequestSenderDidOpen
                    
                    (self.chatListObjects[i!])["chatThreadDoesExist"] = chatThreadDoesExist
                    (self.chatListObjects[i!])["existingChatThreadID"] = existingChatThreadID
                    (self.chatListObjects[i!])["lastMessageString"] = lastMessageString
                    
                    self.observeLastMessages(withThreadID: existingChatThreadID, shouldReloadFullTableView: true, forIndexRow: nil)
                    
                    self.reloadTableViewData()
                }

                //...Update chatListObjects ChatMetaData
                //print("CLVC END observeActiveChatRequestChanges() chatListObjects now contains", self.chatListObjects.count)
                
            }
            //print(" CLVC END observeActiveChatRequestChanges() chatListObjects now contains", self.chatListObjects.count)
        })
    }
    
    //BUG: ChatList not updating selfInitiated swap request from genchat id
    //BUG:: Chatlist not updating selfInitSwapRequestApproved from previous
    
    fileprivate var _swapRefHandle: DatabaseHandle!
    fileprivate var _swapRefHandleChanges: DatabaseHandle!
    
    func observeActiveSwapRequests() {
        //print("CLVC observeActiveSwapRequests")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid

        _swapRefHandle = self.ref.child("globalSwapRequests/\(selfUID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            //print("CLVC observeActiveSwapRequests snapshot is,", snapshot)
            
            let newSwapRequestID = snapshot.key
            
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            
            //UID
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }

            //First Name
            guard let swapInitiatorFirstName = newSwapRequestBody["swapInitiatorFirstName"] else { return }
            guard let swapRecipientFirstName = newSwapRequestBody["swapRecipientFirstName"] else { return }
            
            //Default Image URL
            guard let swapInitiatorDefaultURL = newSwapRequestBody["swapInitiatorDefaultURL"] else { return }
            guard let swapRecipientDefaultURL = newSwapRequestBody["swapRecipientDefaultURL"] else { return }

            //New Photo URL
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            
            //Did Open?
            guard let swapInitiatorDidOpen = newSwapRequestBody["swapInitiatorDidOpen"] else { return }
            guard let swapRecipientDidOpen = newSwapRequestBody["swapRecipientDidOpen"] else { return }

            //Visus Property

            guard let swapInitiatorVisus = newSwapRequestBody["swapInitiatorVisus"] else { return }
            guard let swapRecipientVisus = newSwapRequestBody["swapRecipientVisus"] else { return }

            //Request Status
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }

            //chatInfo
            guard let chatThreadID = newSwapRequestBody["chatThreadID"] else { return }
            //print()

            guard let lastMessage = newSwapRequestBody["lastMessage"] else { return }
            //ChatRequestMarker
            guard let fromChatRequest = newSwapRequestBody["fromChatRequest"] else { return }

            guard let swapRequestTimeStamp = newSwapRequestBody["swapRequestTimeStamp"] else { return }
            
            guard let swapIsActive = newSwapRequestBody["swapIsActive"] else { return }
            guard swapIsActive != "nil" else { return }
            
            
            //observe last message from chat requests
            self.observeLastMessages(withThreadID: "\(chatThreadID)", shouldReloadFullTableView: true, forIndexRow: nil)
            //
            
            //print("CLVC observeActiveSwapRequests PASSED parser")
            
            self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: newSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
            
            //CSVC Swap Configuration
            //            if swapInitiatorID == selfUID {
            //                self.selfInitiatedSwapRequestIndexDict[swapRecipientID] = newSwapRequestID
            //                self.swapRequestStatusDictionary[swapRecipientID] = "false"
            //            } else {
            //                self.otherInitiatedSwapRequestIndexDict[swapInitiatorID] = newSwapRequestID
            //                self.swapRequestStatusDictionary[swapInitiatorID] = "false"
            //            }
            //
            //CLVC Swap Configuration
            //S1. Incoming Swaps
            if swapRecipientID == selfUID && swapRequestStatus == "false" && swapInitiatorVisus == "false" && swapRecipientVisus == "false" {
                //print("1 SWPOBSRV Incoming Swap")

                var newListObject = [String:String]()

                //Request Info
                newListObject["objectType"] = "swapRequestOtherNew"
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["swapWithUID"] = swapInitiatorID
                newListObject["swapWithDisplayName"] = swapInitiatorFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["swapWithVisus"] = swapInitiatorVisus
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                newListObject["selfDidOpen"] = swapRecipientDidOpen
                
                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL

                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                newListObject["objectUID"] = swapInitiatorID

                
                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)

                } else {

                    let iCR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["requestToUID"] == swapInitiatorID
                    })
                    if let iCR = iCR {
                        //print("chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iCR)
                        //print("chatListObjects after removal,", self.chatListObjects.count)
                    }
                    
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                    })
                    if let iGR = iGR {
                        //print(" GEN chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iGR)
                        //print(" GEN chatListObjects after removal,", self.chatListObjects.count)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
//                    self.chatListObjects.append(newListObject)
                    self.chatListObjects.insert(newListObject, at: 0)
                    //clear cvc bubble
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)
                    
                    //New: Jul 21, 2018
                    self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
                }
                
                //Swap Alerts
                //CLVC swap observation means that self didSeeRequest -> activate true
                //handled in tableView UI methods
                
//                if let recipientDidSeeRequest = newSwapRequestBody["recipientDidSeeRequest"] {
//                    // in case of self unlocked, shouldn't update state if recipientDidSeeRequest = false (only update when true)
//                    if !self.ref_SelfModelController.didSeeSwapRequestForUID.keys.contains(swapInitiatorID) {
//                        if !self.unlockedUsers.contains(swapInitiatorID) {
//                            self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = recipientDidSeeRequest
//                        }
//                    } else {
//                        if self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] == "false" {
//                            //don't update change unless true
//                        } else {
//                            self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapInitiatorID)"] = recipientDidSeeRequest
//                        }
//
//                    }
//                }
                
            }

            //S2. Outgoing Swaps
            if swapInitiatorID == selfUID && swapRequestStatus == "false" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                //print("CLVC 2 SWPOBSRV Outgoing Swap")
                
                var newListObject = [String:String]()
                
                //Request Info
                newListObject["objectType"] = "swapRequestSelfNew"
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["swapWithUID"] = swapRecipientID
                newListObject["swapWithDisplayName"] = swapRecipientFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["swapWithVisus"] = swapRecipientVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage

                //UI Marker
                newListObject["selfDidOpen"] = swapInitiatorDidOpen

                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = "You"
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen //tracking swapInitiator since sender is self

                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["objectUID"] = swapRecipientID

                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                } else {
                    let iCR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["requestToUID"] == swapRecipientID
                    })
                    if let iCR = iCR {
                        //print("chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iCR)
                        //print("chatListObjects after removal,", self.chatListObjects.count)
                    }
                    
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["chatWithUID"] == swapRecipientID
                    })
                    if let iGR = iGR {
                        //print(" GEN chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iGR)
                        //print(" GEN chatListObjects after removal,", self.chatListObjects.count)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    //                    self.chatListObjects.append(newListObject)
                    self.chatListObjects.insert(newListObject, at: 0)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)
                }

                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                }
            }
            
            //S3. Incoming Swaps Accepted
            if swapRecipientID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapInitiatorVisus == "false" && swapRecipientVisus == "false" {
                //print("3 SWPOBSRV Incoming Swap Appr")

                var newListObject = [String:String]()
                
                if swapRecipientDidOpen == "false" {
                    newListObject["objectType"] = "otherInitSwapApprovedNew"
                } else if swapRecipientDidOpen == "true" {
                    newListObject["objectType"] = "otherInitSwapApprovedPre"
                }

                //Request Info
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["swapWithUID"] = swapInitiatorID
                newListObject["swapWithVisus"] = swapInitiatorVisus
                newListObject["swapWithDisplayName"] = swapInitiatorFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapInitiatorDefaultURL
                
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage

                //UI Marker
                newListObject["selfDidOpen"] = swapRecipientDidOpen
                
                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL

                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                newListObject["objectUID"] = swapInitiatorID

                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
//                    self.chatListObjects.append(newListObject)
                    self.chatListObjects.insert(newListObject, at: 0)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                }

                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                }
            }

            //S4. Outgoing Swaps Accepted
            if swapInitiatorID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                //print("4 SWPOBSRV Outgoing Swap Appr")

                var newListObject = [String:String]()

                if swapInitiatorDidOpen == "false" {
                    newListObject["objectType"] = "selfInitSwapApprovedNew"
                } else if swapInitiatorDidOpen == "true" {
                    newListObject["objectType"] = "selfInitSwapApprovedPre"
                }
                
                //Request Info
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                //Profile Info
                newListObject["swapWithUID"] = swapRecipientID
                newListObject["swapWithVisus"] = swapRecipientVisus
                newListObject["swapWithDisplayName"] = swapRecipientFirstName
                newListObject["swapWithDefaultPhotoURL"] = swapRecipientDefaultURL
                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage

                //UI Marker
                newListObject["selfDidOpen"] = swapInitiatorDidOpen

                newListObject["swapRecipientID"] = swapRecipientID
                newListObject["swapInitiatorID"] = swapInitiatorID
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self //careful with these, need to reset all to default getter values

                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                newListObject["objectUID"] = swapRecipientID

                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                } else {
                    let iCR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["requestToUID"] == swapRecipientID
                    })
                    if let iCR = iCR {
                        ////print("chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iCR)
                        ////print("chatListObjects after removal,", self.chatListObjects.count)
                    }
                    
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["chatWithUID"] == swapRecipientID
                    })
                    if let iGR = iGR {
                        ////print(" GEN chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iGR)
                        ////print(" GEN chatListObjects after removal,", self.chatListObjects.count)
                    }
                }
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    //                    self.chatListObjects.append(newListObject)
                    self.chatListObjects.insert(newListObject, at: 0)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                    
                }

                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                }
                
            }
            
            var oneWaySwapObjectTypes = ["incomingReveal", "incomingRevealAppr", "outgoingReveal", "outgoingRevealAppr", "outgoingRevealApprPre"]

            //MARK: 1Way Swaps
            //MARK: S5. incomingReveal Request
            if swapRecipientID == self.selfUID! && swapInitiatorPhotoURL == "" && swapInitiatorVisus == "true" {
                ////print("CLVC 5 SWPOBSRV incomingReveal")

                var newListObject = [String:String]()
                
                //Request Info
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Request Info
                if swapRequestStatus == "false" {
                    newListObject["objectType"] = "incomingReveal"
                } else if swapRequestStatus == "true" {
                    newListObject["objectType"] = "incomingRevealAppr"
                }
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["revealToUID"] = swapInitiatorID
                newListObject["revealToDisplayName"] = swapInitiatorFirstName
                newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["revealToVisus"] = swapInitiatorVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                //UI Marker
                //newListObject["selfDidOpen"] = swapInitiatorDidOpen
                newListObject["otherDidSee"] = swapInitiatorDidOpen //optionally used in UI

                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"

                newListObject["objectUID"] = swapInitiatorID

                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self

                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL

                if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                    self.allUniqueUIDsinChatList.append(swapInitiatorID)
                } else {
                    //remove old tableView record, in this case, remove old chatRequest record because it's being replaced with a new swapRecrod for the same user. Was this chatRequest initiated by self or other in the first place?
                    let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        //chatRequestUID key //!!!!! MAKE SURE THIS WORKS IN ALL DIRECTIONS OF WHO SENDS CHAT REQUEST/SWAPREQUEST SINCE THESE ARE INDEPENDENT i.e. ChatRequestInitiator can be Different form ChatRequestInitiator
                        dictionaryOfInterest["requestToUID"] == swapInitiatorID
                    })
                    if let i = i {
                        //print("chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: i)
                        //print("chatListObjects after removal,", self.chatListObjects.count)
                    }
                    
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        //locate entry in chatListObjects where
                        //                        generalChatObject["objectType"] = "generalChatThread"
                        
                        dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                    })
                    
                    if let iGR = iGR {
                        //print(" CLVC GEN chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iGR)
                        //print(" CLVC GEN chatListObjects after removal,", self.chatListObjects.count)
                    }
                }

                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    //                    self.chatListObjects.append(newListObject)
                    self.chatListObjects.insert(newListObject, at: 0)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                    
                    self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
                }
                
                self.reloadTableViewData()
                //print("CLVC 5 SWPOBSRV incomingReveal chatList objects is nowm, ", self.chatListObjects.count)
            }
            
            //MARK: S6. outgoingReveal

            if swapInitiatorID == selfUID && swapInitiatorVisus == "true" && swapRecipientVisus == "false" {
                //print("CLVC 6 SWPOBSRV outgoingRevealRequest")

                var newListObject = [String:String]()

                //Request Info
                if swapRequestStatus == "false" {
                    //print("S6 A")
                    newListObject["objectType"] = "outgoingReveal"
                } else if swapRequestStatus == "true" {
                    //UI Marker
                    //print("S6 B")

                    newListObject["selfDidOpen"] = swapInitiatorDidOpen
                    if swapInitiatorDidOpen == "false" {
                        //print("S6 B1")

                        newListObject["objectType"] = "outgoingRevealAppr"
                    } else if swapInitiatorDidOpen == "true" {
                        //print("S6 B2")

                        newListObject["objectType"] = "outgoingRevealApprPre"
                    }
                }

                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["revealToUID"] = swapRecipientID
                newListObject["revealToDisplayName"] = swapRecipientFirstName
                newListObject["revealToDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["revealToVisus"] = swapRecipientVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self

                newListObject["objectUID"] = swapRecipientID

                
                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                //need to remove case if from general chat, or renove case if from chat request
                
                //ATTN: WHY WAS THIS?
                /*
                 if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                 self.allUniqueUIDsinChatList.append(swapRecipientID)
                 */
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                    
                    if !self.chatListObjectIDs.contains(newSwapRequestID) {
                        self.chatListObjectIDs.append(newSwapRequestID)
                        //                    self.chatListObjects.append(newListObject)
                        self.chatListObjects.insert(newListObject, at: 0)
//                        NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                    }
                    
                } else {
                    //print("SearchingPastIndex")
                    let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["chatWithUID"] == swapRecipientID
                    })
                    
                    if let iGR = iGR {
                        //print("* GEN chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iGR)
                        //print(" GEN chatListObjects after removal,", self.chatListObjects.count)
                        
                        //NEW: Aug 1, after chat list deletes old instance, needs to add new one instead
                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                            self.chatListObjectIDs.append(newSwapRequestID)
                            //                    self.chatListObjects.append(newListObject)
                            self.chatListObjects.insert(newListObject, at: 0)
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                        }
                    }
                    
                    let iCR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                        dictionaryOfInterest["requestToUID"] == swapRecipientID
                    })
                    if let iCR = iCR {
                        //print("* chatListObjects before removal,", self.chatListObjects.count)
                        self.chatListObjects.remove(at: iCR)
                        //print("chatListObjects after removal,", self.chatListObjects.count)
                    }
                }
                
                //print("* CLVC WILLCALL RELOADTABLEVIEWDATA")
                self.reloadTableViewData()
            }

            //MARK: S7. oneWayShow
            if swapInitiatorID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && swapRequestStatus == "true" {
                //print("7 SWPOBSRV outgoing oneWayShow")

                var newListObject = [String:String]()
                
                newListObject["objectType"] = "outgoingOneWayShow"
                
                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["revealToUID"] = swapRecipientID
                newListObject["revealToDisplayName"] = swapRecipientFirstName
                newListObject["revealToDefaultPhotoURL"] = swapRecipientDefaultURL
                newListObject["revealToVisus"] = swapRecipientVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage
                
                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = "You"
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self

                newListObject["objectUID"] = swapRecipientID

                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                //DO NOT APPEND TO ARRAY if no message exists
//
                //REACTIVATE BLOCK
                if self.lastMessagesbyUID.keys.contains(swapRecipientID) {
                    if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {

                        self.allUniqueUIDsinChatList.append(swapRecipientID)
                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                            self.chatListObjectIDs.append(newSwapRequestID)
                            //                    self.chatListObjects.append(newListObject)
                            self.chatListObjects.insert(newListObject, at: 0)
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                            //print("CLVC DIDAPPENDONEWAYSHOW")
                        }
                    }
                } else {
                    //print("CLVC DIDNOTAPPENDONEWAYSHOW")
                }
            }
            
            //problem: Harry sends message. Object recorded as general chat thread?. Harry shows profile. Object recorded as?
            //problem: Harry requests chat. Enter chat request. Harry shows profiel with no message. Table does not record instance.
            
            //MARK: S8. Incoming Voluntary OneWayShow
            if swapRecipientID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && swapRequestStatus == "true" {
                //print("8.1 SWPOBSRV incomingOneWayShow")

                var newListObject = [String:String]()

                newListObject["objectType"] = "incomingOneWayShow"

                newListObject["oneWayRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus

                //Profile Info
                newListObject["revealToUID"] = swapInitiatorID
                newListObject["revealToDisplayName"] = swapInitiatorFirstName
                newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
                newListObject["revealToVisus"] = swapInitiatorVisus

                //Chat Info
                newListObject["chatThreadID"] = chatThreadID
                newListObject["lastMessage"] = lastMessage

                newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
                newListObject["requestInitiatorFirstName"] = swapInitiatorFirstName
                //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
        
                newListObject["objectUID"] = swapInitiatorID

                
                //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                if lastMessage != "" {
                    if !self.allUniqueUIDsinChatList.contains(swapInitiatorID) {
                        self.allUniqueUIDsinChatList.append(swapInitiatorID)
                        if !self.chatListObjectIDs.contains(newSwapRequestID) {
                            self.chatListObjectIDs.append(newSwapRequestID)
                            //                    self.chatListObjects.append(newListObject)
                            self.chatListObjects.insert(newListObject, at: 0)
//                            NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                            //print("DIDAPPENDONEWAYSHOW")
                        }

                    } else {

                        let iGR = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                            dictionaryOfInterest["chatWithUID"] == swapInitiatorID
                        })
                        
                        if let iGR = iGR {
                            //print(" GEN chatListObjects before removal,", self.chatListObjects.count)
                            self.chatListObjects.remove(at: iGR)
                            //print(" GEN chatListObjects after removal,", self.chatListObjects.count)
                            
                            //NEW: Aug 1, after chat list deletes old instance, needs to add new one instead
                            if !self.chatListObjectIDs.contains(newSwapRequestID) {
                                self.chatListObjectIDs.append(newSwapRequestID)
                                //                    self.chatListObjects.append(newListObject)
                                self.chatListObjects.insert(newListObject, at: 0)
//                                NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                            }
                        }

                    }

                } else {
                    //print("DIDNOTAPPENDONEWAYSHOW")
                }
            }
            
            if swapInitiatorID == self.selfUID! {
                //print("Z!! 1")
                //                self.chronoDict[swapRecipientID] = Double(swapRequestTimeStamp)
                self.updateChronoDict(forObjectUID: swapRecipientID, withObjectTimeStamp: swapRequestTimeStamp)
                
            } else if swapInitiatorID != self.selfUID! {
                //print("Z!! 2")
                //                self.chronoDict[swapInitiatorID] = Double(swapRequestTimeStamp)
                self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
            }
        })
    }
    
    func observeSwapRequestStatusChanges() {
        
        //print("func observeSwapRequestStatusChanges")
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _swapRefHandleChanges = self.ref.child("globalSwapRequests/\(user.uid)").observe(.childChanged , with: { info in
            
            
            self.updateSwapStatusChangesSnapshot(selfUID: selfUID, snapshot: info)
        })
    }
    
    func observeUnlockedUsers() {
        //print("observeUnlockedUsers")
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("users/\(selfUID)/unlockedUsers").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            //print("observeUnlockedUsers snapshot is,", snapshot.key)
            let unlockedUID = snapshot.key
            //print("observeUnlockedUsers unlockedUID,", unlockedUID)
            self.unlockedUsers.append(unlockedUID)
        })
    }
    
    func updateSwapStatusChangesSnapshot(selfUID: String, snapshot: DataSnapshot) {
        let existingSwapRequestID = snapshot.key
        //print("M1. existingSwapRequestID is,", existingSwapRequestID)
        
        guard let modifiedSwapRequestBody = snapshot.value as? [String:String] else { return }
        //print("M2. modifiedSwapRequestBody is,", modifiedSwapRequestBody)
        
        //UID
        guard let swapInitiatorID = modifiedSwapRequestBody["swapInitiatorID"] else { return }
        //print("M3. newswapInitiatorID is", swapInitiatorID )
        guard let swapRecipientID = modifiedSwapRequestBody["swapRecipientID"] else { return }
        //print("M4. newswapRecipientID is", swapRecipientID )
        
        //FirstName
        guard let swapInitiatorFirstName = modifiedSwapRequestBody["swapInitiatorFirstName"] else { return }
        //print("S3. newswapInitiatorID is", swapInitiatorFirstName )
        guard let swapRecipientFirstName = modifiedSwapRequestBody["swapRecipientFirstName"] else { return }
        //print("S11. swapRecipientFirstName is", swapRecipientFirstName)
        
        //Default Image URL
        guard let swapInitiatorDefaultURL = modifiedSwapRequestBody["swapInitiatorDefaultURL"] else { return }
        //print("S9. swapRequestStatus is", swapInitiatorDefaultURL)
        guard let swapRecipientDefaultURL = modifiedSwapRequestBody["swapRecipientDefaultURL"] else { return }
        //print("S10. ACCIO swapRequestStatus is", swapRecipientDefaultURL)
        
        //PhotoURL
        guard let swapInitiatorPhotoURL = modifiedSwapRequestBody["swapInitiatorPhotoURL"] else { return }
        //print("M5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL )
        guard let swapRecipientPhotoURL = modifiedSwapRequestBody["swapRecipientPhotoURL"] else { return }
        //print("M6. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
        
        //didOpen
        guard let swapInitiatorDidOpen = modifiedSwapRequestBody["swapInitiatorDidOpen"] else { return }
        //print("S7. swapRequestStatus is", swapInitiatorDidOpen)
        guard let swapRecipientDidOpen = modifiedSwapRequestBody["swapRecipientDidOpen"] else { return }
        //print("S7. swapRequestStatus is", swapRecipientDidOpen)
        
        //SwapStatus
        guard let modifiedSwapRequestStatus = modifiedSwapRequestBody["swapRequestStatus"] else { return }
        //print("M7. modifiedSwapRequestStatus is", modifiedSwapRequestStatus)
        
        //Visus Property
        guard let swapInitiatorVisus = modifiedSwapRequestBody["swapInitiatorVisus"] else { return }
        //print("S7. swapInitiatorVisus is", swapInitiatorVisus)
        guard let swapRecipientVisus = modifiedSwapRequestBody["swapRecipientVisus"] else { return }
        //print("S8. swapRecipientVisus is", swapRecipientVisus)
        
        //ChatData
        guard let chatThreadID = modifiedSwapRequestBody["chatThreadID"] else { return }
        guard let lastMessage = modifiedSwapRequestBody["lastMessage"] else { return }
        
        //ChatRequestMarker
        guard let fromChatRequest = modifiedSwapRequestBody["fromChatRequest"] else { return }
        guard let swapRequestTimeStamp = modifiedSwapRequestBody["swapRequestTimeStamp"] else { return }

        //didSee
        guard let initiatorDidSeeRequest = modifiedSwapRequestBody["initiatorDidSeeRequest"] else { return }

        guard let swapIsActive = modifiedSwapRequestBody["swapIsActive"] else { return }
        
        guard swapIsActive != "nil" else { return }
        
        //print("detectedNewchatThreadID IN SWAP")
        self.observeLastMessages(withThreadID: chatThreadID, shouldReloadFullTableView: true, forIndexRow: 0)
        
        
         self.liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: swapInitiatorID, swapRecipientID: swapRecipientID, swapInitiatorPhotoURL: swapInitiatorPhotoURL, swapRecipientPhotoURL: swapRecipientPhotoURL, swapID: existingSwapRequestID, swapInitiatorVisus: swapInitiatorVisus, swapRecipientVisus: swapRecipientVisus)
        
        //Swap Status Modifier
        //        if swapInitiatorID == selfUID {
        //            //print("M8. Self-initiated swap request status now has status,", modifiedSwapRequestStatus)
        //            self.swapRequestStatusDictionary[swapRecipientID] = modifiedSwapRequestStatus
        //        } else {
        //            print("M9. Other-initiated swap request status now has status,", modifiedSwapRequestStatus)
        //            self.swapRequestStatusDictionary[swapInitiatorID] = modifiedSwapRequestStatus
        //        }
        
        //..........Update chatListObjects for 2Wayswaps
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            //print("chatListObjectIDs contains existingSwapRequestID")
            
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapRequestID"] == existingSwapRequestID
            })
            
            //When I approve someone's previous request
            if swapRecipientID == selfUID &&  modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                
                //print("ACCIO 2 chatListObjects", self.chatListObjects.count)
                
                if swapRecipientDidOpen == "false" {
                    (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedNew"
                    
                } else if swapRecipientDidOpen == "true" {
                    (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedPre"
                    (self.chatListObjects[i!])["requestStatus"] = "true"
                }
                
                (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                (self.chatListObjects[i!])["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                //
                //update chatData
                (self.chatListObjects[i!])["fromChatRequest"] = fromChatRequest
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                (self.chatListObjects[i!])["lastMessage"] = lastMessage

                self.reloadTableViewData()
            }
            
            //2Way Outgoing Self initiated swap requests that have been approved by other
            if swapInitiatorID == selfUID  && modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" && swapRecipientVisus == "false" && swapInitiatorVisus == "false" {
                
                
                //that have been opened
                if swapInitiatorDidOpen == "false" {
                    (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedNew"
                } else if swapInitiatorDidOpen == "true" {
                    (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedPre"
                }
                
                (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                (self.chatListObjects[i!])["swapInitiatorDidOpen"] = swapInitiatorDidOpen
                
                //update chatData
                (self.chatListObjects[i!])["fromChatRequest"] = fromChatRequest
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                (self.chatListObjects[i!])["lastMessage"] = lastMessage
                
                self.reloadTableViewData()
                
                //CLEARSwapRequest_Outgoing
                if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "false" {
                    self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(swapRecipientID)
                    self.ref.child("globalSwapRequests/\(selfUID)/\(existingSwapRequestID)/initiatorDidSeeRequest").setValue("true")
                    self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapRecipientID)"] = "true"
                }
                
//                NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)
            }
        }
        
        //........
        //MARK: 1Way Swaps
        //MARK: S5. incomingReveal Request
        
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            //print("chatListObjectIDs contains existingSwapRequestID")
            //print("self.chatListObjectIDs", self.chatListObjectIDs)
            //print("self.chatListObjects", self.chatListObjects.count)
            
            
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["oneWayRequestID"] == existingSwapRequestID
            })
            
            //when incoming reveals, need to see if self approved the request
            
            if swapRecipientID == selfUID && swapInitiatorVisus == "true" {
                //print("Modifying incomingReveal")
                
                if modifiedSwapRequestStatus == "true" {
                    (self.chatListObjects[i!])["objectType"] = "incomingRevealAppr"
                    (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                    
                    self.reloadTableViewData()
                }
                
                //print("self.chatListObjects[i!]", self.chatListObjects[i!])
                (self.chatListObjects[i!])["selfDidOpen"] = swapRecipientDidOpen
                (self.chatListObjects[i!])["otherDidSee"] = swapInitiatorDidOpen
                
                (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                (self.chatListObjects[i!])["lastMessage"] = lastMessage
            }
            
            //S5. 1Way Outgoing when outgoing reveals,
            if swapInitiatorID == selfUID && swapInitiatorVisus == "true" && swapRecipientVisus == "false" {
                
                if modifiedSwapRequestStatus == "true" {
                    //print("modifying chat initiated by self and approved by other ")
                    
                    if swapInitiatorDidOpen == "false" {
                        (self.chatListObjects[i!])["objectType"] = "outgoingRevealAppr"
                        (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                        
                        (self.chatListObjects[i!])["selfDidOpen"] = swapInitiatorDidOpen
                        (self.chatListObjects[i!])["otherDidSee"] = swapRecipientDidOpen
                        
                    } else if swapInitiatorDidOpen == "true" {
                        (self.chatListObjects[i!])["objectType"] = "outgoingRevealApprPre"
                        (self.chatListObjects[i!])["requestStatus"] = modifiedSwapRequestStatus
                        
                        (self.chatListObjects[i!])["selfDidOpen"] = swapInitiatorDidOpen
                        (self.chatListObjects[i!])["otherDidSee"] = swapRecipientDidOpen
                    }
                    //
                    (self.chatListObjects[i!])["chatThreadID"] = chatThreadID
                    (self.chatListObjects[i!])["lastMessage"] = lastMessage
                    
                    self.reloadTableViewData()
                    
                    //CLEARSwapRequest_Outgoing 1Way outgoing
                    if !self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.contains(swapRecipientID) && initiatorDidSeeRequest == "false" {
                        self.ref_SelfModelController.didClearSwapAlertForUserID_Outgoing.append(swapRecipientID)
                        self.ref.child("globalSwapRequests/\(selfUID)/\(existingSwapRequestID)/initiatorDidSeeRequest").setValue("true")
                        self.ref_SelfModelController.didSeeSwapRequestForUID["\(swapRecipientID)"] = "true"
                    }
                    
                }
                
            }
        }
        
        //1WayShow
        if swapRecipientID == selfUID && swapInitiatorVisus == "false" && swapRecipientVisus == "true" && modifiedSwapRequestStatus == "true" {
            //print("8.2 SWPOBSRV incomingOneWayShow")
            
            var newListObject = [String:String]()
            
            newListObject["objectType"] = "incomingOneWayShow"
            
            newListObject["oneWayRequestID"] = existingSwapRequestID
            newListObject["requestStatus"] = modifiedSwapRequestStatus
            
            //Profile Info
            newListObject["revealToUID"] = swapInitiatorID
            newListObject["revealToDisplayName"] = swapInitiatorFirstName
            newListObject["revealToDefaultPhotoURL"] = swapInitiatorDefaultURL
            newListObject["revealToVisus"] = swapInitiatorVisus
            
            //Chat Info
            newListObject["chatThreadID"] = chatThreadID
            newListObject["lastMessage"] = lastMessage
            
            newListObject["objectUID"] = swapInitiatorID
            
            newListObject["requestInitiatorID"] = swapInitiatorID //this is used to determine if selfInitiatedRequest = T/F
            newListObject["requestInitiatorFirstName"] = swapInitiatorFirstName
            //newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
            
            //newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
            //                need to add condition: if UID is not associated with last message then DO NOT Append
            //print("CVC SWPOBSRV lastMessagesArray", self.lastMessagesbyUID)
            //print("CVC SWPOBSRV swapInitiatorID", swapInitiatorID)
            if lastMessage != "" {
                //print("GVC will add oneWayShow to Array")
                if !self.allUniqueUIDsinChatList.contains(swapRecipientID) {
                    self.allUniqueUIDsinChatList.append(swapRecipientID)
                    
                    if !self.chatListObjectIDs.contains(existingSwapRequestID) {
                        self.chatListObjectIDs.append(existingSwapRequestID)
                        self.chatListObjects.append(newListObject)
//                        NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)

                        self.reloadTableViewData()
                    }
                }
                
            } else {
                //print("GVC will NOT add oneWayShow to Array")
            }
        }
        
        //......update user info since chatListObject acts as userProfileInfoDict for GridVC->ChatListVC->ChatScreenVC
        guard swapRecipientVisus == "false" && swapInitiatorVisus == "false" else { return }
        
        if self.chatListObjectIDs.contains(existingSwapRequestID) {
            let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                dictionaryOfInterest["swapRequestID"] == existingSwapRequestID
            })
            if swapInitiatorID == selfUID {
                //update with new recipient values
                (self.chatListObjects[i!])["swapWithVisus"] = swapRecipientVisus
                (self.chatListObjects[i!])["swapWithDefaultURL"] = swapRecipientDefaultURL
                (self.chatListObjects[i!])["swapWithDisplayName"] = swapRecipientFirstName
                
            } else {
                //when other is initiator, update with new initiator values
                (self.chatListObjects[i!])["swapWithVisus"] = swapInitiatorVisus
                (self.chatListObjects[i!])["swapWithDefaultURL"] = swapInitiatorDefaultURL
                (self.chatListObjects[i!])["swapWithDisplayName"] = swapInitiatorFirstName
            }
        }
        
        if swapInitiatorID == self.selfUID! {
            //print("V! 1")
            //            self.chronoDict[swapRecipientID] = Double(swapRequestTimeStamp)
            self.updateChronoDict(forObjectUID: swapRecipientID, withObjectTimeStamp: swapRequestTimeStamp)
            
        } else if swapInitiatorID != self.selfUID! {
            //print("V! 2")
            //            self.chronoDict[swapInitiatorID] = Double(swapRequestTimeStamp)
            self.updateChronoDict(forObjectUID: swapInitiatorID, withObjectTimeStamp: swapRequestTimeStamp)
        }

    }
    
    //MARK: IMAGE LOADER
    func liveSaveUserProfileImageFromDBtoDisk(swapInitiatorID: String, swapRecipientID: String, swapInitiatorPhotoURL: String, swapRecipientPhotoURL: String, swapID: String, swapInitiatorVisus: String, swapRecipientVisus: String) {
        
        guard swapInitiatorVisus == "false" && swapRecipientVisus == "false" else { return }
        
        //print("PASS GUARD 2waySWAP liveSaveUserProfileImageFromDBtoDisk")
        
        if swapInitiatorID == self.selfUID! {
            
            if swapRecipientPhotoURL != "" && swapRecipientPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapRecipientPhotoURL, withRequestID: swapID, photoChildNode: "swapRecipientPhotoURL", fromUserID: swapRecipientID, requestType: "")
            }
            
        } else if swapInitiatorID != self.selfUID! {
            //swap initiator is other 2 way
            if swapInitiatorPhotoURL != "" && swapInitiatorPhotoURL != "Clear" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "")
            }
            
            if self.selfVisus! == "true" && swapInitiatorVisus == "false" {
                self.loadImageFromDatabaseToMemory_thenSave(withClearURL: swapInitiatorPhotoURL, withRequestID: swapID, photoChildNode: "swapInitiatorPhotoURL", fromUserID: swapInitiatorID, requestType: "incomingOneWayShow")
            }
        }
    }
    
    func loadImageFromDatabaseToMemory_thenSave(withClearURL: String, withRequestID: String, photoChildNode: String, fromUserID: String, requestType: String) {
        //print("CVC ONEWAYSHOW loadImageToMemory()")
        let newPhotoURL = URL(string: withClearURL)
        self.imageLoaderView!.sd_setImage(with: newPhotoURL) { (loadedImage, error, _, savedURL) in
            
            if error == nil {
                self.saveUnlockedImageToDisk(imageObject: loadedImage!, fromUserID: fromUserID)
                //print("revelaro successfully loaded image")
                
                let selfUID = self.selfUID!
                let otherUID = fromUserID
                
                //print("CVC ONEWAYSHOW loadImageFromDatabaseToMemory_thenSave will clear photoURL")
                    if requestType == "incomingOneWayShow" {
                        //print("CLVC incomingOneWayShow CLEAR")
                        //clear only on one-way & let the voilaVC clear all other cases
                        self.ref.child("globalSwapRequests/\(selfUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                        self.ref.child("globalSwapRequests/\(otherUID)/\(withRequestID)/\(photoChildNode)").setValue("Clear")
                    }
            } else {
                //print("CVC phot=o loader error,", error)
            }
        }
    }
    
    func saveUnlockedImageToDisk(imageObject: UIImage, fromUserID: String) {
        //print("CVC  OneWayShow writing to vault")
        let fileName = "\(fromUserID).jpeg"
        if let data = UIImageJPEGRepresentation(imageObject, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            //print("CVC OneWayShow photo written to vault with filename,", fileName)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    var imageLoaderView: UIImageView!
    
    func addImageLoader() {
        let imageX = 0 as CGFloat
        let imageY = 0 as CGFloat
        let imageWidth = 5 as CGFloat
        let imageHeight = 5 as CGFloat
        
        let imageFrame = CGRect(x: imageX, y: imageY, width: imageWidth, height: imageHeight)
        let imageLoaderView = UIImageView(frame: imageFrame)
        
        view.addSubview(imageLoaderView)
        self.imageLoaderView = imageLoaderView
        self.imageLoaderView.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        if let chatThreadID = self.deletedMessagesChatThreadID {
            self.ref.child("globalChatThreads/\(chatThreadID)").removeAllObservers()
        }
        
        if let freezeFrame = self.freezeFrame {
            //print("CLVC WILLRMV FF")
            freezeFrame.removeFromSuperview()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
