//
//  File.swift
//  Ximo
//
//  Created by Asaad on 7/24/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

//Bugs for last:
/*
 
 
 */

/*
import Foundation

class ViewControllerA: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: Data Structures
    var userDictionary = [[String: String]]() { didSet { print("userDictionary.count from DIDSET,", userDictionary.count)}}
    //prints out: 9
    
    //MARK: Initialization
    override func viewDidLoad() {
        self.loadUserDictionaryFromDatabase()
    }
    
    func loadUserDictionaryFromDatabase() {
        //code that loads information from a database
        //var objectInstance["name"] = firstName
        //self.userDictionary.append(objectInstance)
        print("loader observed numberOfItems", numberOfItems)
        //...once data is fully loaded
        self.syncDataFromDatabase()
    }
    
    func syncDataFromDatabase() {
        //sync new data from database
        //var newObjectInstance["newName"] = newFirstName
        //self.userDictionary.append(newName)
        print("syncer observed newNumberOfItems", newNumberOfItems)
    }
    
    //MARK: View Controller Transitions
    @IBAction func segueAtoB(_ sender: Any) {
        self.performSegue(withIdentifier: "segueAtoB", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //Segue from VCA to VCB
        if segue.identifier == "segueAtoB" {
            
            let controller = segue.destination as! ViewControllerA
            
            //Pass initialized userDictionary from VCA to VCB
            controller.userDictionary = self.userDictionary
        }
    }
    
    //MARK: Dummy Test Number of Items in UserDictionary
    @IBAction func printMostUpToDateNumberofItemsInDictionary(_ sender: Any) {
        print("userDictionary.count from TEST,", userDictionary.count)
    }
}

class ViewControllerB: UIViewController {
    
    //MARK: Data Structures
    var userDictionary = [[String: String]]()
    
    //MARK: View Controller Transitions
    @IBAction func segueBtoA(_ sender: Any) {
        self.performSegue(withIdentifier: "segueBtoA", sender: self)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //1. Segue to Home-Screen Controller
        if segue.identifier == "segueBtoA" {
            
            let controller = segue.destination as! ViewControllerB
            controller.userDictionary = self.userDictionary
        }
    }
}

//PRINT RESULTS:
//Step 1: Start in View Controller A
//1. loader observed numberOfItems = 9
//-- userDictionary.count from DIDSET, 9
//2. syncer observed newNumberOfItems = 1
//-- userDictionary.count from DIDSET, 10 (10 = 9 + 1)
//3. userDictionary.count from TEST = 10 (10 = 9 + 1)
//
////Step 2: Segue From A to B
//viewControllerB.userDictArray = viewControllerA.userDictArray
//
////Step 3: Segue From B to A
//viewControllerA.userDictArray = viewControllerB.userDictArray
//
////Step 4: Observe Debugger Output
//-- userDictionary.count from DIDSET, 10
//1. syncer observed newNumberOfItems = 1
//-- userDictionary.count from DIDSET, 11 (10 = 10 + 1)
//3. userDictionary.count from TEST = 10 (10 != 10 + 1) (How on Earth is this line happening when diSet just updated userDictionary.count?)

*/



