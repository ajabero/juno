//
//  TrustInfoViewController.swift
//  Juno
//
//  Created by Asaad on 5/9/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

class TrustInfoViewController: UIViewController {

    var ref_AuthTracker = AuthTracker()
    
    var withInfo = ""
    var sourceController: String?
    var copyContainer: UIImageView!
    var yesFBPermissions = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.addCopyContainer()
        self.addBackButtonView()
        
        if self.withInfo == "verifyUnlink" {
            if self.yesFBPermissions == false {
                self.copyContainer.image = UIImage(named: "unlinkInfo_noFB")!
            } else {
                self.copyContainer.image = UIImage(named: "unlinkInfo")!
            }
            
        } else if self.withInfo == "whatFBInfo"  {
            if self.yesFBPermissions == false {
                self.copyContainer.image = UIImage(named: "facebookInfo_noFB")!
            } else {
                self.copyContainer.image = UIImage(named: "facebookInfo")!
            }
            
        } else if self.withInfo == "veriVSFB" {
            if self.yesFBPermissions == false {
                self.copyContainer.image = UIImage(named: "verivsFacebook_noFB")!
            } else {
                self.copyContainer.image = UIImage(named: "verivsFacebook")!
            }
            
        } else if self.withInfo == "noFacebook" {
            UserDefaults.standard.set("true", forKey: "didSeeNoFacebookPrompt")
            if self.yesFBPermissions == false {
//                self.copyContainer.image = UIImage(named: "noFacebook_noFB")!
                self.copyContainer.image = UIImage(named: "noFacebook_Restart")!
            } else {
                self.copyContainer.image = UIImage(named: "noFacebook")!
            }
        } else if self.withInfo == "usernameGuidelines" {
             self.copyContainer.image = UIImage(named: "usernameGuidelines")!
        }
        
        if let sourceController = self.sourceController {
            if sourceController == "profileImageVC" {
                self.copyContainer.image = UIImage(named: "photoVaultInfo")!
            } else if sourceController == "locationVC" {
                self.copyContainer.image = UIImage(named: "locationInfo")!
            }
        } else {

        }
        
        let shadowWidthR = -7 / 414 as CGFloat
        let shadowWidth = shadowWidthR * UIScreen.main.bounds.width
//        let shadowHeight = UIScreen.main.bounds.height
        self.view.layer.shadowOpacity = 0.2
        self.view.layer.shadowOffset = CGSize(width: 0, height: shadowWidth)
        self.view.layer.shadowRadius = 30
    }

    override func viewWillAppear(_ animated: Bool) {
        
        if UIScreen.main.bounds.height > 750 {
            //print("will set top constraint")
            let sBarH = UIApplication.shared.statusBarFrame.size.height
            //print("sBarH is,", sBarH)
        }
        
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        //print("iphoneX sbarH, ", sBarH)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var backButton: UIButton!
    
    func addCopyContainer() {
        
        let cont = UIImageView()
        let width = UIScreen.main.bounds.width
        let hWRatio = 736 / 414 as CGFloat
        let height = width * hWRatio
        
        var y = 0 as CGFloat
        if UIScreen.main.bounds.height > 750 {
            //print("will set top constraint")
            let sBarH = 44 as CGFloat
            //print("sBarH is,", sBarH)
            
//            let topInsetRatio = 22 / 736 as CGFloat
//            let topInsetSpace = topInsetRatio * UIScreen.main.bounds.height
            y = sBarH
        }
        
        cont.frame = CGRect(x: 0, y: y, width: width, height: height)
        self.view.addSubview(cont)
        self.copyContainer = cont
    }
        
    func addBackButtonView() {
        let sWidth = UIScreen.main.bounds.width
        let sHeight = UIScreen.main.bounds.height
        var sBarH = UIApplication.shared.statusBarFrame.size.height
        
        let leftInsetRatio = 38 / 414 as CGFloat
        let topInsetRatio = 22 / 736 as CGFloat
        sBarH = 20 as CGFloat
        
        var topInset = (topInsetRatio * sHeight) + sBarH
        //print("topInset", topInset, "sBarH", sBarH)
        
        if sHeight > 750 {
            sBarH = 44 as CGFloat
            topInset = (topInsetRatio * 736) + sBarH
        }
        
        let leftInset = leftInsetRatio * sWidth
        
        let xPos = leftInset
        let yPos = topInset
        let w = 32 as CGFloat
        let h = 32 as CGFloat
        
        let frame = CGRect(x: xPos, y: yPos, width: w, height: h)
        let frameView = UIButton(frame: frame)
//        frameView.setImage(UIImage(named: "infoBackButton")!, for: .normal)
        frameView.setImage(UIImage(named: "cancelInfo")!, for: .normal)

        frameView.contentMode = .center
        frameView.backgroundColor = UIColor.clear
        
        self.view.addSubview(frameView)
        self.backButton = frameView
        
        backButton.addTarget(self, action: #selector(TrustInfoViewController.goBack(_:)), for: .touchUpInside)
        backButton.isUserInteractionEnabled = true
    }
    
    @objc func goBack(_ sender: UIButton) {
        //print("goBackgoBack")
        
        if self.sourceController == "trustVC" {
            self.performSegue(withIdentifier: "unwindToTrust", sender: self)
        }
        
        if self.sourceController == "profileImageVC" {
            self.performSegue(withIdentifier: "unwindToProfileVC", sender: self)
        }
        
        if self.sourceController == "locationVC" {
            self.performSegue(withIdentifier: "unwindToLocationVC", sender: self)
        }
        
        if self.sourceController == "usernameGuidelines" {
            self.performSegue(withIdentifier: "unwindToMyInfo", sender: self)
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        
        if segue.identifier == "unwindToMyInfo" {
            
            let controller = segue.destination as! MyInfoViewController
            controller.sourceVC = "trustInfoVC"

        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}
