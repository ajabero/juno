//
//  PushSegue.swift
//  Juno
//
//  Created by Asaad on 3/22/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

class PushSegue: UIStoryboardSegue {

    override func perform() {
        push()
    }
    
    func push() {
        // get reference to our fromView, toView and the container view that we should perform the transition in
        if let container = self.source.view.superview {
            let toView = self.destination.view!
            let fromView = self.source.view!
            
            // set up from 2D transforms that we'll use in the animation
            let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
            let offScreenLeft = CGAffineTransform(translationX: -(container.frame.width), y: 0)
            
            // start the toView to the right of the screen
            
            toView.transform = offScreenRight
            // add the both views to our view controller
            container.addSubview(toView)
            container.addSubview(fromView)
            
            //set duration
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                fromView.transform = offScreenLeft
                toView.transform = CGAffineTransform.identity
                
            }, completion: { success in
                self.source.present(self.destination, animated: false, completion: nil)

            })
        }
    }
}

class UnwindPushSegue: UIStoryboardSegue {
    override func perform() {
        push()
    }
    
    func push() {
        // get reference to our fromView, toView and the container view that we should perform the transition in
        let container = self.source.view.superview!
        
        let toView = self.destination.view!
        let fromView = self.source.view!
        
        // set up from 2D transforms that we'll use in the animation
        let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -(container.frame.width), y: 0)
        
        // start the toView to the right of the screen
        
        toView.transform = offScreenLeft
        
        // add the both views to our view controller
        container.addSubview(toView)
        container.addSubview(fromView)
        
        //set duration
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            fromView.transform = offScreenRight
            toView.transform = CGAffineTransform.identity
            
        }, completion: { success in
            self.source.present(self.destination, animated: false, completion: nil)
        })
    }
}

class SegueFromLeft: UIStoryboardSegue {
    
    override func perform() {
        let src = self.source       //new enum
        let dst = self.destination  //new enum
        
        src.view.superview?.insertSubview(dst.view, aboveSubview: src.view)
        dst.view.transform = CGAffineTransform(translationX: src.view.frame.size.width, y: 0) //Method call changed
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }) { (finished) in
            src.present(dst, animated: false, completion: nil) //Method call changed
        }
    }
}

class SegueFromRight: UIStoryboardSegue {
    
//    var didBlockOrDismiss = false { didSet { print("PushSegue didBlockOrDismiss is,", didBlockOrDismiss) }}
    
    override func perform() {
        let src = self.source       //new enum
        let dst = self.destination  //new enum

        src.view.superview?.insertSubview(dst.view, belowSubview: src.view)
        //        dst.view.superview?.insertSubview(src.view, aboveSubview: dst.view)
        dst.view.transform = CGAffineTransform.identity
        
        //        src.view.transform = CGAffineTransform(translationX: -(src.view.frame.size.width), y: 0) //Method call changed
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            
        dst.view.transform = CGAffineTransform.identity
        src.view.transform = CGAffineTransform(translationX: (src.view.frame.size.width), y: 0)
            
//            if let didBlockOrDismiss = UserDefaults.standard.string(forKey: "selfDidBlockOrDismissUser") {
//
//                if didBlockOrDismiss == "true" {
//                    print("PUSHSEGUE CASE 1")
//                    dst.view.transform = CGAffineTransform(translationX: (src.view.frame.size.width), y: 0)
//                    src.view.transform = CGAffineTransform(translationX: (src.view.frame.size.width), y: 0)
//                    UserDefaults.standard.set("false", forKey: "selfDidBlockOrDismissUser")
//
//                } else {
//                    print("PUSHSEGUE CASE 2")
//                    dst.view.transform = CGAffineTransform.identity
//                    src.view.transform = CGAffineTransform(translationX: (src.view.frame.size.width), y: 0)
//                }
//
//            } else {
//                print("PUSHSEGUE CASE 3")
//                dst.view.transform = CGAffineTransform.identity
//                src.view.transform = CGAffineTransform(translationX: (src.view.frame.size.width), y: 0)
//            }

        }) { (finished) in
            src.dismiss(animated: false, completion: nil) //Method call changed
        }
    }
}

class UnwindHomeToGrid: UIStoryboardSegue {
    
    override func perform() {
        push()
    }
    
    func push() {
        // get reference to our fromView, toView and the container view that we should perform the transition in
        if let container = self.source.view.superview {
            let toView = self.destination.view!
            let fromView = self.source.view!
            
            // set up from 2D transforms that we'll use in the animation
            let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
            let offScreenLeft = CGAffineTransform(translationX: -(container.frame.width), y: 0)
            
            // start the toView to the right of the screen
            
            toView.transform = offScreenRight
            // add the both views to our view controller
            container.addSubview(toView)
            container.addSubview(fromView)
            
            //set duration
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
                fromView.transform = offScreenLeft
                toView.transform = CGAffineTransform.identity
                
            }, completion: { success in
                self.source.dismiss(animated: false, completion: nil)
            })
        }
    }
}

//"UnwindChatsToGrid"

class UnwindChatsToGrid: UIStoryboardSegue {
    override func perform() {
        push()
    }
    
    func push() {
        // get reference to our fromView, toView and the container view that we should perform the transition in
        let container = self.source.view.superview!
        
        let toView = self.destination.view!
        let fromView = self.source.view!
        
        // set up from 2D transforms that we'll use in the animation
        let offScreenRight = CGAffineTransform(translationX: container.frame.width, y: 0)
        let offScreenLeft = CGAffineTransform(translationX: -(container.frame.width), y: 0)
        
        // start the toView to the right of the screen
        
        toView.transform = offScreenLeft
        
        // add the both views to our view controller
        container.addSubview(toView)
        container.addSubview(fromView)
        
        //set duration
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            fromView.transform = offScreenRight
            toView.transform = CGAffineTransform.identity
            
        }, completion: { success in
            self.source.dismiss(animated: false, completion: nil)
        })
    }
}


