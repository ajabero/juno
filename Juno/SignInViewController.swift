//
//  SignInViewController.swift
//  Juno
//
//  Created by Asaad on 2/16/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import UserNotifications

class SignInViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate, UNUserNotificationCenterDelegate {

    //MARK: Global Properties
    var signInEmail: String?
    var signInPassword: String?
    fileprivate var _authHandle: AuthStateDidChangeListenerHandle!
    var user: User?
    var ref: DatabaseReference!
    
    let spaceToKeyboard = 25 as CGFloat
    let screen = UIScreen.main.bounds

    var sBarHeight: CGFloat?
    
    //MARK: Global Outlets
    @IBOutlet weak var tapAreaOutside: UIButton!
//    @IBOutlet weak var signInEmailTextField: UITextField!
//    @IBOutlet weak var signInPasswordTextField: UITextField!
    @IBOutlet weak var superStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.superStack.isHidden = true
        self.ref = Database.database().reference()

        self.addTitleLabel()
        self.addEmailTitleHeader()
        self.addEmailTextField()
        self.addLineView()
        
        self.addPasswordTitleHeader()
        self.addPasswordTextField()
        self.addPasswordLineView()
        
        self.addEmailErrorLabel()
        self.addPasswordErrorLabel()
        
        self.emailErrorLabel?.isHidden = true
        self.passwordErrorLabel?.isHidden = true
        
        self.addActivityIndicator()
        self.addShowHideNameButton()
        
//        self.addBackButton()
        
        //why does disabling button in viewdidload cause it to not work any more?
        //print("1 continueButton.isEnabled = false")
        self.addContinueButton()
        self.continueButton.isEnabled = true
        
        self.configureAuth()
        self.addToggleAuthButton()
        
        //Draw Phone Auth Views
        self.addPhoneNumberHeader()
        self.addPhoneNumberTextField()
        self.addPhoneNumberLineView()
        self.addPhoneNumberErrorLabel()
        
        //Draw VeriCode Views
        self.addVeriCodeHeader()
        self.addVeriCodeTextField()
        self.addVeriCodeLineView()
        self.addVeriCodeErrorLabel()
        
        //displace phone auth views on initial presentation
        let displaceBy = UIScreen.main.bounds.width
        
        self.phoneNumberHeader.transform = CGAffineTransform(translationX: displaceBy, y: 0)
        self.phoneNumberTextField.transform = CGAffineTransform(translationX: displaceBy, y: 0)
        self.phoneNumberLineView.transform = CGAffineTransform(translationX: displaceBy, y: 0)
        self.phoneNumberErrorLabel!.transform = CGAffineTransform(translationX: displaceBy, y: 0)
        
        self.veriCodeHeaderView.transform = CGAffineTransform(translationX: 2 * displaceBy, y: 0)
        self.veriCodeTextField.transform = CGAffineTransform(translationX: 2 * displaceBy, y: 0)
        self.veriCodeLineView.transform = CGAffineTransform(translationX: 2 * displaceBy, y: 0)
        self.veriCodeErrorLabel!.transform = CGAffineTransform(translationX: 2 * displaceBy, y: 0)
        
        self.phoneNumberErrorLabel.isHidden = true
        self.veriCodeErrorLabel.isHidden = true
        
        self.phoneNumberTextField.keyboardType = .phonePad
        self.veriCodeTextField.keyboardType = .decimalPad
        
        self.addNotificationRequiredAlert()
        self.addNotificationRequired_toSettings()
        self.addBackgroundRefreshAlert()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SignInViewController.willEnterForeground), name: .UIApplicationWillEnterForeground , object: nil)
        
        
        var emailAuthSource = ""
        var phoneAuthSource = ""
        
        if let authEmail = UserDefaults.standard.string(forKey: "emailAuthSource") {
            emailAuthSource = authEmail
        }
        
        if let phoneSource = UserDefaults.standard.string(forKey: "phoneAuthSource") {
            phoneAuthSource = phoneSource
        }
        
        if phoneAuthSource == "true" && emailAuthSource == "false"  {
            self.animateViews_towardsVolume()
            self.signInEmailTextField.text = ""
            self.toggleAuth?.text = "Sign In Using Email"
            self.authIsEmail = false
        }
        
    }

    @objc func willEnterForeground(_ notification: Notification) {
        //print("app willEnterForeground state")
        
        DispatchQueue.main.async {
            self.retrieveBackgroundRefresh_Status()
        }
        
    }
    
    
    var titleLabel: UILabel!
    var emailTitleHeader: UILabel!
    var signInEmailTextField: UITextField!
    var signInPasswordTextField: UITextField!
    var emailLineView: UIImageView!
    var passwordLineView: UIImageView!
    
    var statusBarHeight: CGFloat?
    var bottomPadding: CGFloat?
    
    func addTitleLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 35 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let titleLabel = UILabel(frame: tF)
        
        //init
        self.view.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.titleLabel!.text = "Sign In"
        self.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 22)!
        self.titleLabel!.textColor = UIColor.black
        self.titleLabel!.textAlignment = .center
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.trySwitch(_:)))
//        tapGesture.delegate = self
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        self.titleLabel.addGestureRecognizer(tapGesture)
//        self.titleLabel.isUserInteractionEnabled = true
    }
    
    func addEmailTitleHeader() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
//        let lY = (2 * superInsetHeight) - (0.5 * lH)
        let lY = superInsetHeight + 2 * gridUnitHeight

        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let emailTitleHeader = UILabel(frame: tF)
        
        emailTitleHeader.text = "Email Address"
        emailTitleHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        emailTitleHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(emailTitleHeader)
        self.emailTitleHeader = emailTitleHeader
    }
    
    func addEmailTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
//        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = 2 * superInsetHeight

        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let emailTextField = UITextField(frame: tF)
        
        emailTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        emailTextField.minimumFontSize = 17.0
        emailTextField.adjustsFontSizeToFitWidth = true
        
        emailTextField.autocapitalizationType = .none
        emailTextField.autocorrectionType = .no
        emailTextField.keyboardType = .emailAddress
        emailTextField.clearButtonMode = .whileEditing
        
        emailTextField.delegate = self
        
        self.view.addSubview(emailTextField)
        self.signInEmailTextField = emailTextField
    }
    
    var lineViewHeight = 0.6 as CGFloat
    
    func addLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = lineViewHeight
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.signInEmailTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineView = UIImageView(frame: f)
        lineView.backgroundColor = UIColor.clear
        lineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(lineView)
        self.emailLineView = lineView
    }

    var  passwordTitleHeader: UILabel?
    
    //2
    func addPasswordTitleHeader() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
//        let lY = (3 * superInsetHeight) - (0.5 * lH)
        let lY = (2 * superInsetHeight) + (2 * gridUnitHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let passwordTitleHeader = UILabel(frame: tF)
        
        passwordTitleHeader.text = "Password"
        passwordTitleHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        passwordTitleHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(passwordTitleHeader)
        self.passwordTitleHeader = passwordTitleHeader
    }
    
    func addPasswordTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        let lY = (3 * superInsetHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let signInPasswordTextField = UITextField(frame: tF)
        
        signInPasswordTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        signInPasswordTextField.minimumFontSize = 17.0
        signInPasswordTextField.adjustsFontSizeToFitWidth = true
        
        signInPasswordTextField.autocapitalizationType = .none
        signInPasswordTextField.autocorrectionType = .no
        signInPasswordTextField.keyboardType = .default
        signInPasswordTextField.clearButtonMode = .whileEditing
        signInPasswordTextField.isSecureTextEntry = true
        
        signInPasswordTextField.delegate = self
        
        self.view.addSubview(signInPasswordTextField)
        self.signInPasswordTextField = signInPasswordTextField
    }
    
    func addPasswordLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = lineViewHeight
        let lX = gridUnitWidth
        let lY = self.signInPasswordTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineView = UIImageView(frame: f)
        lineView.backgroundColor = UIColor.white
        lineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(lineView)
        self.passwordLineView = lineView
    }
    
    var emailErrorLabel: UITextView?
    var passwordErrorLabel: UITextView?
    
    func addEmailErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Email Address. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.emailLineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let emailErrorLabel = UITextView(frame: f)
        
        //
        emailErrorLabel.text = errorString
        self.view.addSubview(emailErrorLabel)
        self.emailErrorLabel = emailErrorLabel
        
        self.emailErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.emailErrorLabel!.textAlignment = .center
        self.emailErrorLabel!.isEditable = false
        self.emailErrorLabel!.isScrollEnabled = false
        self.emailErrorLabel!.isSelectable = false
        self.emailErrorLabel!.textContainer.lineFragmentPadding = 0
        self.emailErrorLabel!.textContainerInset = .zero
        self.emailErrorLabel!.textColor = UIColor.red
        self.emailErrorLabel!.backgroundColor = UIColor.clear
    }
    
    func addPasswordErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Email Address. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.passwordLineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let passwordErrorLabel = UITextView(frame: f)
        
        //
        passwordErrorLabel.text = errorString
        self.view.addSubview(passwordErrorLabel)
        self.passwordErrorLabel = passwordErrorLabel
        
        self.passwordErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.passwordErrorLabel!.textAlignment = .center
        self.passwordErrorLabel!.isEditable = false
        self.passwordErrorLabel!.isScrollEnabled = false
        self.passwordErrorLabel!.isSelectable = false
        self.passwordErrorLabel!.textContainer.lineFragmentPadding = 0
        self.passwordErrorLabel!.textContainerInset = .zero
        self.passwordErrorLabel!.textColor = UIColor.red
        self.passwordErrorLabel!.backgroundColor = UIColor.clear
    }
    
    //MARK: START Add Phone Auth Views
    
    //A. Phone Number
    
    var phoneNumberHeader: UILabel!
    
    func addPhoneNumberHeader() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) - (0.5 * lH)
        let lY = superInsetHeight + 2 * gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let phoneNumberHeader = UILabel(frame: tF)
        
        phoneNumberHeader.text = "Phone Number"
        phoneNumberHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        phoneNumberHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(phoneNumberHeader)
        self.phoneNumberHeader = phoneNumberHeader
    }
    
    var phoneNumberTextField: UITextField!
    
    func addPhoneNumberTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = 2 * superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let phoneNumberTextField = UITextField(frame: tF)
        
        phoneNumberTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        phoneNumberTextField.minimumFontSize = 17.0
        phoneNumberTextField.adjustsFontSizeToFitWidth = true
        
        phoneNumberTextField.autocapitalizationType = .none
        phoneNumberTextField.autocorrectionType = .no
        phoneNumberTextField.keyboardType = .emailAddress
        phoneNumberTextField.clearButtonMode = .whileEditing
        
        phoneNumberTextField.delegate = self
        
        self.view.addSubview(phoneNumberTextField)
        self.phoneNumberTextField = phoneNumberTextField
    }
    
    var phoneNumberLineView: UIImageView!
    
    func addPhoneNumberLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = lineViewHeight
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.phoneNumberTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let phoneNumberLineView = UIImageView(frame: f)
        phoneNumberLineView.backgroundColor = UIColor.clear
        phoneNumberLineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(phoneNumberLineView)
        self.phoneNumberLineView = phoneNumberLineView
    }
    
    var phoneNumberErrorLabel: UITextView!
    
    func addPhoneNumberErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Phone Number. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.phoneNumberLineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let phoneNumberErrorLabel = UITextView(frame: f)
        
        //
        phoneNumberErrorLabel.text = errorString
        self.view.addSubview(phoneNumberErrorLabel)
        self.phoneNumberErrorLabel = phoneNumberErrorLabel
        
        self.phoneNumberErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.phoneNumberErrorLabel!.textAlignment = .center
        self.phoneNumberErrorLabel!.isEditable = false
        self.phoneNumberErrorLabel!.isScrollEnabled = false
        self.phoneNumberErrorLabel!.isSelectable = false
        self.phoneNumberErrorLabel!.textContainer.lineFragmentPadding = 0
        self.phoneNumberErrorLabel!.textContainerInset = .zero
        self.phoneNumberErrorLabel!.textColor = UIColor.red
        self.phoneNumberErrorLabel!.backgroundColor = UIColor.clear
    }
    
    //B. VeriCode
    
    var veriCodeHeaderView: UILabel!
    
    func addVeriCodeHeader() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
//        let lY = (2 * superInsetHeight) + (2 * gridUnitHeight)
        let lY = superInsetHeight + 2 * gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let veriCodeHeaderView = UILabel(frame: tF)
        
        veriCodeHeaderView.text = "Verification Code"
        veriCodeHeaderView.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        veriCodeHeaderView.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(veriCodeHeaderView)
        self.veriCodeHeaderView = veriCodeHeaderView
    }
    
    var veriCodeTextField: UITextField!
    
    func addVeriCodeTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
//        let lY = (3 * superInsetHeight)
        let lY = 2 * superInsetHeight

        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let veriCodeTextField = UITextField(frame: tF)
        
        veriCodeTextField.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        veriCodeTextField.minimumFontSize = 17.0
        veriCodeTextField.adjustsFontSizeToFitWidth = true
        
        veriCodeTextField.autocapitalizationType = .none
        veriCodeTextField.autocorrectionType = .no
        veriCodeTextField.keyboardType = .emailAddress
        veriCodeTextField.clearButtonMode = .whileEditing
        
        veriCodeTextField.delegate = self
        
        self.view.addSubview(veriCodeTextField)
        self.veriCodeTextField = veriCodeTextField
    }
    
    var veriCodeLineView: UIImageView!
    
    func addVeriCodeLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = lineViewHeight
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.veriCodeTextField!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let veriCodeLineView = UIImageView(frame: f)
        veriCodeLineView.backgroundColor = UIColor.clear
        veriCodeLineView.image = UIImage(named: "GrayLine")!
        
        self.view.addSubview(veriCodeLineView)
        self.veriCodeLineView = veriCodeLineView
    }
    
    var veriCodeErrorLabel: UITextView!
    
    func addVeriCodeErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "Invalid Phone Number. Please try again."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.veriCodeLineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let veriCodeErrorLabel = UITextView(frame: f)
        
        //
        veriCodeErrorLabel.text = errorString
        self.view.addSubview(veriCodeErrorLabel)
        self.veriCodeErrorLabel = veriCodeErrorLabel
        
        self.veriCodeErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.veriCodeErrorLabel!.textAlignment = .center
        self.veriCodeErrorLabel!.isEditable = false
        self.veriCodeErrorLabel!.isScrollEnabled = false
        self.veriCodeErrorLabel!.isSelectable = false
        self.veriCodeErrorLabel!.textContainer.lineFragmentPadding = 0
        self.veriCodeErrorLabel!.textContainerInset = .zero
        self.veriCodeErrorLabel!.textColor = UIColor.red
        self.veriCodeErrorLabel!.backgroundColor = UIColor.clear
    }
    
    //MARK: END Add Phone Auth Views
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
        
        if self.sourceVC == "SettingsVC" {
            self.emailErrorLabel?.text = "Please sign in. Then delete your account."
            self.emailErrorLabel?.isHidden = false
        }
        
        if !self.signInPasswordTextField.isFirstResponder && !self.signInEmailTextField.isFirstResponder {
            emailLineView.image = UIImage(named: "GrayLine")!
            passwordLineView.image = UIImage(named: "GrayLine")!
        }
        
        self.signInEmailTextField.textContentType = UITextContentType("")
        self.signInPasswordTextField.textContentType = UITextContentType("")
//        self.signInEmailTextField.keyboardType = .emailAddress
        self.signInEmailTextField.returnKeyType = .next
        self.signInEmailTextField.enablesReturnKeyAutomatically = true
        self.signInPasswordTextField.returnKeyType = .done
        self.signInPasswordTextField.enablesReturnKeyAutomatically = true
        
        DispatchQueue.main.async {
            self.retrieveBackgroundRefresh_Status()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "AvenirNext-DemiBold", size: fontSize)! ],
            context: nil).size
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        //print("textFieldShouldClear called")
        self.emailErrorLabel?.isHidden = true
        self.passwordErrorLabel?.isHidden = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.signInEmailTextField {
            self.signInPasswordTextField.becomeFirstResponder()
            
            return true
        } else if textField == self.signInPasswordTextField {
            self.signInEmailTextField.resignFirstResponder()
            self.signInPasswordTextField.resignFirstResponder()
            
            return true
        }
        
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if self.authIsEmail == true {
            return self.configureEmailView_charRange(textField: textField, range: range, string: string)
        } else {
            return self.configurePhoneView_charRange(textField: textField,range: range, string: string)
        }
    }
    
    func configureEmailView_charRange(textField: UITextField, range: NSRange, string: String) -> Bool {
        
        let transitionDuration = 0.20

        if string == "" {
            self.emailErrorLabel?.isHidden = true
            self.passwordErrorLabel?.isHidden = true
            self.continueButton.backgroundColor = self.inactiveGrayColor
            self.showHideButton?.isUserInteractionEnabled = false
        } else {
            self.showHideButton?.isUserInteractionEnabled = true
        }
        
        if string == " " {
            return false
            
        } else {
            //FBConfition
            let characterCountLimit = 80
            let startingLength = textField.text?.count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace = range.length
            let stringLength = startingLength + lengthToAdd - lengthToReplace
            if stringLength >= 1 {
                
                if signInPasswordTextField.text! != "" && signInEmailTextField.text! != "" {
                    
                    UIView.transition(with: self.continueButton!, duration: transitionDuration, options: .transitionCrossDissolve, animations: {
                        self.continueButton.backgroundColor = self.activeGreenColor
                    }, completion: nil)
                    
                    //print("True1")
                    self.continueButton.isUserInteractionEnabled = true
                }
                
            } else {
                UIView.transition(with: self.continueButton!, duration: transitionDuration, options: .transitionCrossDissolve, animations: {
                    self.continueButton.backgroundColor = self.inactiveGrayColor
                }, completion: nil)
                
                //print("2 continueButton.isEnabled = false")
                
                self.continueButton.isUserInteractionEnabled = false
            }
            return stringLength <= characterCountLimit
        }
    }
    
    func configurePhoneView_charRange(textField: UITextField, range: NSRange, string: String) -> Bool {
        
        if textField == self.phoneNumberTextField {
            
            let numberArr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ""]
            
            if string == " " {
                self.phoneNumberErrorLabel?.isHidden = true
                return false
            } else if !numberArr.contains(string)  {
                return false
            } else {
                //FBCondition
                self.phoneNumberErrorLabel?.isHidden = true
                let characterCountLimit = 10
                let startingLength = textField.text?.count ?? 0
                let lengthToAdd = string.count
                let lengthToReplace = range.length
                let stringLength = startingLength + lengthToAdd - lengthToReplace
                if stringLength >= 10 {
                    self.continueButton.backgroundColor = activeGreenColor
                    self.emailErrorLabel!.isHidden = true
                    self.continueButton.isUserInteractionEnabled = true
                    
                } else {
                    //                sbContinueButton.setImage(UIImage(named: "GrayButton"), for: UIControlState.normal)
                    //                sbContinueButton.isEnabled = false
                    self.continueButton.backgroundColor = inactiveGrayColor
                    self.continueButton.isUserInteractionEnabled = false
                    
                }

                return stringLength <= characterCountLimit
            }
        }
        
        if textField == self.veriCodeTextField {
            if string == " " {
                self.veriCodeErrorLabel?.isHidden = true
                return false
            } else {
                //FBCondition
                
                let characterCountLimit = 6
                let startingLength = textField.text?.count ?? 0
                let lengthToAdd = string.count
                let lengthToReplace = range.length
                let stringLength = startingLength + lengthToAdd - lengthToReplace
                if stringLength >= 6 {
                    self.continueButton.backgroundColor = activeGreenColor
                    self.continueButton.isUserInteractionEnabled = true
                } else {
                    self.continueButton.backgroundColor = inactiveGrayColor
                    self.continueButton.isUserInteractionEnabled = false
                }
                self.veriCodeErrorLabel?.isHidden = true
                return stringLength <= characterCountLimit
            }
        }
        
        return true
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == signInEmailTextField {
            emailLineView.image = UIImage(named: "BlueLine")!
            passwordLineView.image = UIImage(named: "GrayLine")!
        }
        
        if textField == signInPasswordTextField {
            emailLineView.image = UIImage(named: "GrayLine")!
            passwordLineView.image = UIImage(named: "BlueLine")!
        }
        
        if textField == phoneNumberTextField {
            veriCodeLineView.image = UIImage(named: "GrayLine")!
            phoneNumberLineView.image = UIImage(named: "BlueLine")!
        }
        
        if textField == veriCodeTextField {
            veriCodeLineView.image = UIImage(named: "BlueLine")!
            phoneNumberLineView.image = UIImage(named: "GrayLine")!
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailLineView.image = UIImage(named: "GrayLine")!
        passwordLineView.image = UIImage(named: "GrayLine")!

        phoneNumberLineView.image = UIImage(named: "GrayLine")!
        veriCodeLineView.image = UIImage(named: "GrayLine")!
    }
    
//    let softSpaceGray = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    let softSpaceGray = UIColor(displayP3Red: 190/255, green: 190/255, blue: 190/255, alpha: 1)

    func addShowHideNameButton() {
        //method called after FBMkRequest
        let gridUnitX = 45.5 / 414 as CGFloat
        let sWidth = UIScreen.main.bounds.width
        let gridUnitWidth = gridUnitX * sWidth
        
        let buttonWidth = 50 as CGFloat
        let bH = 20 as CGFloat
        let bX = self.passwordLineView!.frame.maxX - buttonWidth
//        let bX = self.passwordLineView!.frame.minX
        let bY = self.passwordLineView!.frame.maxY + 2 as CGFloat
        
        let bF = CGRect(x: bX, y: bY, width: buttonWidth, height: bH)
        let bView = UIButton(frame: bF)
        
        bView.setTitle("SHOW", for: .normal)
        bView.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)!
        bView.setTitleColor(softSpaceGray, for: .normal)
        bView.titleLabel!.addCharacterSpacing(withValue: 1.0)

        self.view.addSubview(bView)
        self.showHideButton = bView
        
        showHideButton!.addTarget(self, action: #selector(SignInViewController.showHidePassword(_:)), for: .touchUpInside)
        showHideButton!.isUserInteractionEnabled = true
    }
    
    var showHideButton: UIButton?
    var passwordIsHidden = true
    let hyperBlue = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
    var userFirstName: String?
    var userFirstInitial: String?
    
    @objc func showHidePassword(_ sender: UIButton) {
        //print("showHidePassword detected")
        
        if self.passwordIsHidden == true {
            self.signInPasswordTextField.isSecureTextEntry = false
            self.passwordIsHidden = false
            showHideButton!.setTitle("HIDE", for: .normal)
            
        } else {
            self.signInPasswordTextField.isSecureTextEntry = true
            self.passwordIsHidden = true
            showHideButton!.setTitle("SHOW", for: .normal)
            
        }
    }
    
    var backButton: UIButton?
    
    func addBackButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
    
        //qw
        let fW = 50 as CGFloat
        let fH = fW as CGFloat
        let fX = 0 as CGFloat
        let fY = 0 as CGFloat
        
        let fF = CGRect(x: fX, y: fY, width: fW, height: fH)
        let fView = UIButton(frame: fF)
        
        fView.setImage(UIImage(named: "accountBackButton")!, for: .normal)
        fView.backgroundColor = UIColor.clear
        fView.contentMode = .center
        
        fView.addTarget(self, action: #selector(AuthViewController.goBack(_:)), for: .touchUpInside)
        fView.isUserInteractionEnabled = true
        
        self.view.addSubview(fView)
        self.backButton = fView
        
        let cX = gridUnitWidth * 0.5
        let centerPoint = CGPoint(x: cX, y: gridUnitHeight * 1.5)
        
        self.backButton!.layer.position = centerPoint
    }
    
    @objc func goBack(_ sender: UIButton) {
        self.rotateBackArrow_Animate()
    }
    
    func rotateBackArrow_Animate() {
        //print("rotateBackArrow_Animation")
        
        UIView.animate(withDuration: 0.08) {
            self.backButton!.transform = self.backButton!.transform.rotated(by: -CGFloat(M_PI_2))
        }
        
        let when = DispatchTime.now() + 0.04
        DispatchQueue.main.asyncAfter(deadline: when) {
            //note: MUST ALWAYS CONFIGURE backToWelcome as unwind OR Dismiss && not as a new VC instance
//            self.performSegue(withIdentifier: "backToWelcome2", sender: self)
            self.dismiss(animated: true, completion: nil)
        }
    }
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "backToWelcome2" {
            
        } else if segue.identifier == "toGridVC" {
//            //print("SVC passing statusBarHeight,", self.statusBarHeight!)
            let controller = segue.destination as! CollectionViewController
//            controller.statusBarHeight = self.statusBarHeight!
        }
    }
    
    
    func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification:Notification) {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let spaceToKeyboard = 1.5 * gridUnitHeight
        
        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton.frame.height
        let keyboardHeight = getKeyboardHeight(notification)
        let risingStackViewPosition = screenHeight - keyboardHeight - buttonHeight - spaceToKeyboard
        self.continueButton.frame.origin.y = risingStackViewPosition
        
        self.toggleAuth?.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        self.tapFrame.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let spaceToKeyboard = 1.5 * gridUnitHeight
        
        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton.frame.height
        let restingStackViewPosition = screenHeight - superInsetHeight
        self.continueButton.frame.origin.y = restingStackViewPosition
        
        if self.signInEmailTextField.text! == "" || self.signInPasswordTextField.text! == "" {
                self.continueButton.backgroundColor = self.inactiveGrayColor
            
                //print("3 continueButton.isEnabled = false")
                continueButton.isUserInteractionEnabled = false
        } else {
            //print("True2")
            self.continueButton.isUserInteractionEnabled = true
        }
        
        self.toggleAuth?.frame.origin.y = screenHeight - bottomButtonPadding - self.toggleAuth!.frame.height
        self.tapFrame?.frame.origin.y = screenHeight - bottomButtonPadding - self.toggleAuth!.frame.height
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        buttonStackView.superview!.layoutIfNeeded()
//        buttonStackView.superview!.setNeedsLayout()
        // Now modify bottomView's frame here
    }
    
    func unsubscribeFromKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        //should also add condition for keyboardwillhide here?
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unsubscribeFromKeyboardNotifications()
    }
    
    //MARK: Sign In User
    var activityIndicatorView: UIActivityIndicatorView?
    let activeGreenColor = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    var inactiveGrayColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    var continueButton: UIButton!
    
    var superInsetHeight: CGFloat?
    
    func addContinueButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
      
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = 3 * gridUnitWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
        //high-screen position
//        let bY = sHeight - superInsetHeight - bH
        //lo-screen position
        let bY = sHeight - superInsetHeight
        self.superInsetHeight = superInsetHeight
        //
        let f = CGRect(x: bX, y: bY, width: bW, height:bH)
        let continueButton = UIButton(frame: f)
//        continueButton.setImage(UIImage(named: "greenButtonBackground")!, for: .normal)
        continueButton.setBackgroundImage(UIImage(named: "darkGreenBackGround")!, for: .highlighted)

        continueButton.contentMode = .scaleAspectFill
        continueButton.clipsToBounds = true
        continueButton.addTarget(self, action: #selector(SignInViewController.signInUser(_:)), for: .touchUpInside)
        continueButton.backgroundColor = self.inactiveGrayColor
        
//        self.tapAreaOutside!.addSubview(continueButton)
//        self.tapAreaOutside.isUserInteractionEnabled = true
        
        self.view.addSubview(continueButton)
        self.continueButton = continueButton
        self.continueButton!.isUserInteractionEnabled = true
        
        self.continueButton!.layer.cornerRadius = 8
        self.continueButton!.layer.masksToBounds = true
        
        self.continueButton!.setTitle("Continue", for: .normal)
        self.continueButton!.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 16.0)!
        self.continueButton!.setTitleColor(UIColor.white, for: .normal)
    }
    
    var toggleAuth: UITextView?
    var bottomButtonPadding: CGFloat!
    var tapFrame: UIView!
    
    func addToggleAuthButton() {
        
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let tFontSize = 12.0 as CGFloat
        let buttonCopy = "Sign In Using Phone"
        let tWidth = sWidth - (2 * gridUnitWidth)
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let marginSpace = 2 as CGFloat
        let tY = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let toggleAuth = UITextView(frame: tFrame)
        toggleAuth.text = buttonCopy
        toggleAuth.font = UIFont(name: "Avenir-Medium", size: tFontSize)
        
        let tapFrameWidth = 4 * gridUnitWidth
        let tapFrameX = (sWidth - tapFrameWidth) * 0.5
        let tapFrame = UIView(frame: CGRect(x: tapFrameX, y: tY, width: tapFrameWidth, height: tH * 2))
        tapFrame.backgroundColor = UIColor.clear
        //
        self.view.addSubview(toggleAuth)
        self.toggleAuth = toggleAuth
        
        self.view.addSubview(tapFrame)
        self.tapFrame = tapFrame
        
        self.toggleAuth!.textAlignment = .center
        self.toggleAuth!.isEditable = false
        self.toggleAuth!.isScrollEnabled = false
        self.toggleAuth!.isSelectable = false
        self.toggleAuth!.textContainer.lineFragmentPadding = 0
        self.toggleAuth!.textContainerInset = .zero
        self.toggleAuth!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.toggleAuth!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignInViewController.switchAuth_UI(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.tapFrame.addGestureRecognizer(tapGesture)
        self.tapFrame.isUserInteractionEnabled = true
        //        self.toggleAuth!.addGestureRecognizer(tapGesture)
        //        self.toggleAuth!.isUserInteractionEnabled = true
        
        self.bottomButtonPadding = UIScreen.main.bounds.height - self.toggleAuth!.frame.maxY
    }
    
    var sourceVC = ""
    
    var authIsEmail = true

    @objc func switchAuth_UI(_ sender: UITapGestureRecognizer) {
        
        if self.nowInConfirmationScene == true {
            
            self.veriCodeTextField.text = ""
            self.veriCodeTextField.resignFirstResponder()
            self.animateCode_To_Phone_Reverse()
            
            return
        }
        
        if self.authIsEmail == true {
            self.animateViews_towardsVolume()
            self.signInEmailTextField.text = ""
            self.signInPasswordTextField.text = ""

            self.toggleAuth?.text = "Sign In Using Email"
            self.authIsEmail = false
        } else {
            self.animateViews_awayFromVolume()
            self.phoneNumberTextField.text = ""
            self.toggleAuth?.text = "Sign In Using Phone"
            self.authIsEmail = true
        }
        
        //resign ALL
        self.signInEmailTextField.resignFirstResponder()
        self.signInPasswordTextField.resignFirstResponder()
        self.phoneNumberTextField.resignFirstResponder()
        self.veriCodeTextField.resignFirstResponder()
        
        //Note: DidEndEditing handles ALL LineUI Code
//        emailLineView.image = UIImage(named: "GrayLine")!
//        passwordLineView.image = UIImage(named: "GrayLine")!
//
//        phoneNumberLineView.image = UIImage(named: "GrayLine")!
//        veriCodeLineView.image = UIImage(named: "GrayLine")!
    }
    
// Transformations occur relative to the view's anchor point. By default, the anchor point is equal to the center point of the frame rectangle. To change the anchor point, modify the anchorPoint property of the view's underlying CALayer object.
    
    func animateViews_towardsVolume() {
        
        let sWidth = (UIScreen.main.bounds.width)
        
//        self.showHideButton?.isHidden = true
        
        //email views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
//            self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.emailTitleHeader.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.signInEmailTextField.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.emailLineView.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.emailErrorLabel?.transform = CGAffineTransform(translationX: -sWidth, y: 0)

            self.passwordTitleHeader!.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.signInPasswordTextField.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.passwordLineView.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.passwordErrorLabel?.transform = CGAffineTransform(translationX: -sWidth, y: 0)

            self.showHideButton?.transform = CGAffineTransform(translationX: -sWidth, y: 0)
        }) { (true) in

        }
        
        //phone views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            //            self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberHeader.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberTextField.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberLineView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            
        }) { (true) in
            
        }
        
        //confirmation views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
//            self.veriCodeHeaderView.transform = CGAffineTransform(translationX: -sWidth, y: 0)
//            self.veriCodeTextField.transform = CGAffineTransform(translationX: -sWidth, y: 0)
//            self.veriCodeLineView.transform = CGAffineTransform(translationX: -sWidth, y: 0)
//            self.veriCodeErrorLabel!.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            
        }) { (true) in
            
        }
        
    }
    
    func animateViews_awayFromVolume() {
        let sWidth = (UIScreen.main.bounds.width)
        
//        self.showHideButton?.isHidden = false
        
        //email views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            self.emailTitleHeader.transform = CGAffineTransform(translationX: 0, y: 0)
            self.signInEmailTextField.transform = CGAffineTransform(translationX: 0, y: 0)
            self.emailLineView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.emailErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            
            self.passwordTitleHeader!.transform = CGAffineTransform(translationX: 0, y: 0)
            self.signInPasswordTextField.transform = CGAffineTransform(translationX: 0, y: 0)
            self.passwordLineView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.passwordErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            
            self.showHideButton?.transform = CGAffineTransform(translationX: 0, y: 0)
            
        }) { (true) in
            
        }
        
        //phone views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            //            self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberLineView.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
            
//            self.veriCodeHeaderView.transform = CGAffineTransform(translationX: 0, y: 0)
//            self.veriCodeTextField.transform = CGAffineTransform(translationX: 0, y: 0)
//            self.veriCodeLineView.transform = CGAffineTransform(translationX: 0, y: 0)
//            self.veriCodeErrorLabel!.transform = CGAffineTransform(translationX: 0, y: 0)
            
        }) { (true) in
            
//            self.showHideButton?.isHidden = false

        }
    }
    
    var nowInConfirmationScene = false
    
    func animatePhone_To_Code() {
        
        let sWidth = (UIScreen.main.bounds.width)
        self.nowInConfirmationScene = true

        self.titleLabel.text = "Confirm Your Code"
        
        //phone views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            //            self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberHeader.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.phoneNumberTextField.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.phoneNumberLineView.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: -sWidth, y: 0)
            
        }) { (true) in
            
            self.toggleAuth?.text = "Go Back"
        }
        
        //confirmation views
        self.veriCodeHeaderView.text = "Enter the code we sent to " + self.phoneNumberString_10
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            self.veriCodeHeaderView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.veriCodeTextField.transform = CGAffineTransform(translationX: 0, y: 0)
            self.veriCodeLineView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.veriCodeErrorLabel!.transform = CGAffineTransform(translationX: 0, y: 0)
            
        }) { (true) in

        }
    }
    
    func animateCode_To_Phone_Reverse() {
        
        let sWidth = (UIScreen.main.bounds.width)
        self.nowInConfirmationScene = false
        
        
        //confirmation views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            self.veriCodeHeaderView.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.veriCodeTextField.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.veriCodeLineView.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.veriCodeErrorLabel!.transform = CGAffineTransform(translationX: sWidth, y: 0)
            
        }) { (true) in
            self.toggleAuth?.text = "Sign In With Email"
        }
        
        //phone views
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            
            //            self.emailTitleHeader.transform = CGAffineTransform(translationX: sWidth, y: 0)
            self.phoneNumberHeader.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberTextField.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberLineView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.phoneNumberErrorLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            
        }) { (true) in
            self.titleLabel.text = "Sign In"
        }
    }
    
    var yes = true
    
    @objc func trySwitch(_ sender: UITapGestureRecognizer) {
//        if yes == true {
//            yes = false
//            self.animatePhone_To_Code()
//
//        } else {
//            yes = true
//            self.animateCode_To_Phone_Reverse()
//        }
    }
    
    @objc func signInUser(_ sender: UIButton) {
        //print("signInUser detected")
        
        //3
        if self.nowInConfirmationScene == true {
            //sign in user
            self.signInUser_Phone_Step2()
            
            return
        }
        
        //
        if self.authIsEmail == true {
            self.signInUser_Email()
        } else {
//            DispatchQueue.main.async {
                self.signInUser_Phone_Step1()
//            }
        }
    }
    
    func signInUser_Email() {
        
        guard let signInEmail = self.signInEmailTextField.text else { return }
        guard let signInPassword = self.signInPasswordTextField.text else { return }
        
        if signInEmail == "" || signInPassword == "" {
            //
        } else {
            
            self.signInEmailTextField.resignFirstResponder()
            self.signInPasswordTextField.resignFirstResponder()
            
            self.activityIndicatorView?.startAnimating()
            
            Auth.auth().signIn(withEmail: signInEmail, password: signInPassword) { (user, error) in
                
                if let user = user {
                    //print("user is", user)
                    self.activityIndicatorView?.stopAnimating()
                    
                    if self.sourceVC == "SettingsVC" {
                        self.performSegue(withIdentifier: "unwindToSettings", sender: self)
                    } else {
                        UserDefaults.standard.set("true", forKey: "emailAuthSource")
                        UserDefaults.standard.set("false", forKey: "phoneAuthSource")
                        self.performSegue(withIdentifier: "toGridVC", sender: self)
                    }
                    
                } else {
                    //print("error is", error)
                    self.activityIndicatorView?.stopAnimating()
                    
                    if let errorCode = AuthErrorCode(rawValue: error!._code) {
                        switch errorCode {
                        case .invalidEmail :
                            //print("ERROR: invalid email entered found")
                            self.emailErrorLabel!.isHidden = false
                            self.emailErrorLabel!.text = "That doesn't look right. Please try again."
                            
                        case .userNotFound :
                            //print("ERROR: user not found")
                            self.emailErrorLabel!.isHidden = false
                            self.emailErrorLabel!.text = "User not found. Please try again."
                            
                        case .wrongPassword :
                            //print("ERROR: wrong password")
                            self.passwordErrorLabel!.isHidden = false
                            self.passwordErrorLabel!.text = "Invalid password. Please try again."
                            
                        default:
                            //print("Create User Error: \(error!)")
                            break
                        }
                    }
                }
            }
        }
    }
    
    var phoneNumberString_10 = ""
    
    var phoneNumberDidReturnError = false
    
    var cachedPhoneStringFromTrial = ""
    
    func signInUser_Phone_Step1() {
        //The I
        guard let phoneNumberString = self.phoneNumberTextField!.text else { return }
        guard phoneNumberString.count >= 10 else { return }
        self.phoneNumberString_10 = phoneNumberString
        self.cachedPhoneStringFromTrial = self.phoneNumberString_10
        let finalPhoneString = "+1" + phoneNumberString
        
//        Request Notifications
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings { (settings) in
            if settings.authorizationStatus == .notDetermined {

                //Present User with Notification Request 1st time
                DispatchQueue.main.async {
                    self.phoneNumberTextField.resignFirstResponder()
                    self.present(self.notificationsPrompt!, animated: true)
                }

                return

            } else if settings.authorizationStatus == .denied {
                //When User Has Previously Denied Request, prompt to Settings
                DispatchQueue.main.async {
                    self.phoneNumberTextField.resignFirstResponder()
                    self.present(self.notificationsPrompt_toSettings!, animated: true)
                }

                return

            } else if settings.authorizationStatus == .authorized {
                
                //Authorized -> Continue
                if self.backgroundRefreshIsAvailable == true {
                    DispatchQueue.main.async {
                        self.signInUser_Phone_Step1_Helper()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.phoneNumberTextField.resignFirstResponder()
                        self.present(self.backgroundRefresh_Alert!, animated: true)
                    }
                }
            }
        }
    }
    
    var veriCodeDidReturnError = false
    
    var backgroundRefreshIsAvailable = false
    
    func retrieveBackgroundRefresh_Status() -> Bool {
        switch UIApplication.shared.backgroundRefreshStatus {
        case .available:
            //print("Refresh available")
            self.backgroundRefreshIsAvailable = true
            return true
        case .denied:
            //print("Refresh denied")
            self.backgroundRefreshIsAvailable = false
            return false
            
        case .restricted:
            //print("Refresh restricted")
            self.backgroundRefreshIsAvailable = false
            return true
        }
    }
    
    func signInUser_Phone_Step1_Helper() {
        
        //The I
        guard let phoneNumberString = self.phoneNumberTextField!.text else { return }
        guard phoneNumberString.count >= 10 else { return }
        self.phoneNumberString_10 = phoneNumberString
        self.cachedPhoneStringFromTrial = self.phoneNumberString_10
        let finalPhoneString = "+1" + phoneNumberString
        
        self.phoneNumberTextField.resignFirstResponder()
        
        self.activityIndicatorView?.startAnimating()
        
        //disable ancillary views
        self.continueButton.isUserInteractionEnabled = false
        self.tapFrame.isUserInteractionEnabled = false
        
        PhoneAuthProvider.provider().verifyPhoneNumber(finalPhoneString) { (verificationID, error) in
            
            if let error = error {
                
                self.activityIndicatorView?.stopAnimating()
                self.phoneNumberDidReturnError = true
                
                self.phoneNumberErrorLabel?.isHidden = false
                self.phoneNumberErrorLabel?.text = "Error. Try Again, or Use Email Instead."
                self.phoneNumberTextField.isUserInteractionEnabled = true
                
                self.tapFrame.isUserInteractionEnabled = true
                
                if let errorCode = AuthErrorCode(rawValue: error._code) {
                    switch errorCode {
                    case .captchaCheckFailed :
                        
                        print("captchaCheckFailed")
                        
                    case .invalidPhoneNumber :
                        print("invalidPhoneNumber")
                        
                    case .missingPhoneNumber :
                        print("missingPhoneNumber")
                        
                    case .quotaExceeded :
                        print("quotaExceeded")
                        
                    case .userDisabled  :
                        print("userDisabled")
                        
                    case .credentialAlreadyInUse  :
                        print("credentialAlreadyInUse")
                        self.phoneNumberErrorLabel?.text = "Number Already in Use. Try Again."
                        
                    case .internalError  :
                        print("internalError")
                        
                    case .invalidAppCredential  :
                        print("invalidAppCredential")
                        
                    case .invalidUserToken  :
                        print("invalidUserToken")
                        
                    case .sessionExpired :
                        print("sessionExpired")
                        
                    case .tooManyRequests :
                        print("tooManyRequests")
                        self.phoneNumberErrorLabel?.text = "Trials exceeded. Try again later."
                        
                    default:
                        //print("default")
                        self.phoneNumberErrorLabel?.isHidden = false
                        self.phoneNumberErrorLabel?.text = "Error. Try Again, or Use Email Instead."
                    }
                    
                    self.activityIndicatorView?.stopAnimating()
                    
                    //reenable views on error
                } else {
                    
                }
                
                return
            } else {
                //print("phoneAUthSuccess")
                self.phoneNumberDidReturnError = false
                self.phoneNumberErrorLabel?.isHidden = true
                self.tapFrame.isUserInteractionEnabled = true
                
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                self.activityIndicatorView?.stopAnimating()
                
                self.phoneNumberTextField.resignFirstResponder()
                self.animatePhone_To_Code()
                //                self.veriCodeTextField.isEnabled = true
                //                self.veriCodeTextField.becomeFirstResponder()
            }
        }
    }
    
    func signInUser_Phone_Step2() {
        //Sign In After veriCode is received
        
        if let user = Auth.auth().currentUser {
            //print("authUID", user.uid)
        } else {
            //print("noUser")
        }
        
        guard let verificationCode = self.veriCodeTextField.text else { return }
        //print("verificationCode", verificationCode)
        guard verificationCode.count >= 6 else { return }
        guard let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") else { return }
        
        self.veriCodeTextField.resignFirstResponder()
        veriCodeLineView.image = UIImage(named: "GrayLine")!
        self.veriCodeTextField.isUserInteractionEnabled = false
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        self.activityIndicatorView?.isHidden = false
        self.activityIndicatorView?.startAnimating()
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                //print("phoneAuthError")
                //handke code errors
                self.veriCodeErrorLabel?.isHidden = false
                self.veriCodeErrorLabel?.text = "Invalid Code. Please Try Again."
                self.activityIndicatorView?.stopAnimating()
                self.continueButton.isUserInteractionEnabled = false
                self.continueButton.backgroundColor = self.inactiveGrayColor
                
                self.veriCodeTextField.becomeFirstResponder()
                self.veriCodeLineView.image = UIImage(named: "BlueLine")!
                
                self.veriCodeTextField.isUserInteractionEnabled = true
                self.veriCodeDidReturnError = true
                
                if let errorCode = AuthErrorCode(rawValue: error._code) {
                    switch errorCode {
                    case .captchaCheckFailed :
                        
                        print("captchaCheckFailed")
                        
                    case .invalidPhoneNumber :
                        print("invalidPhoneNumber")
                        
                    case .missingPhoneNumber :
                        print("missingPhoneNumber")
                        
                    case .quotaExceeded :
                        print("quotaExceeded")
//                    self.veriCodeErrorLabel?.text = "We're experiencing unusually high traffic with this sign up method at the moment. Please try again later, or sign up with email instead."
                        
                    case .userDisabled  :
                        self.veriCodeErrorLabel?.text = "Account disabled. Please contact support."
                        
                    case .credentialAlreadyInUse  :
                        //print("credentialAlreadyInUse")
                        self.veriCodeErrorLabel?.text = "Number Already in Use. Try Again."
                        
                    case .internalError  :
                        //print("internalError")
                        self.veriCodeErrorLabel?.text = "We've run into a problem."

                    case .invalidAppCredential  :
                        print("invalidAppCredential")
                        
                    case .invalidUserToken  :
                        print("invalidUserToken")
                        
                    case .sessionExpired :
                        print("sessionExpired")
                        self.veriCodeErrorLabel?.text = "Code Expired. Please Request a New Code."
                        
                    default:
                        self.veriCodeErrorLabel?.text = "Error. Try Again, or Use Email Instead."
                    }
                    
                    return
                }
            } else {
                //print("phoneAuthSuccessful")
                self.veriCodeDidReturnError = false
                
                if let user = Auth.auth().currentUser {
                    //print("2authUID", user.uid)
                    self.checkForExistingUserInstance(withUID: user.uid)
                } else {
                    //print("2noUser")
                }
                
            }
        }
    }
    
    var uidCheckDidResolve = false
    var gfKeyCheckDidResolve = false
    
    var userGFKeyDoesExist = false
    var userUIDKeyDoesExist = false
    
    var finalUserAuthState = false
    
    func checkForExistingUserInstance(withUID: String) {
        
        self.ref.child("users/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("user does exist in DB")
                self.userUIDKeyDoesExist = true
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("user doesNotExist in DB")
                self.userUIDKeyDoesExist = false
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            }
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("error checking snapshot")
            self.userUIDKeyDoesExist = false
            self.uidCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
        
        self.ref.child("userLocationGF/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("GF user does exist in DB")
                self.userGFKeyDoesExist = true
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("GF user doesNotExist in DB")
                self.userGFKeyDoesExist = false
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()

            }
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("GF error checking snapshot")
            self.userGFKeyDoesExist = false
            self.gfKeyCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
    }
    
    var didAdmitUser = false
    
    func returnFinalUserAuthState() -> Bool {
    
        if self.uidCheckDidResolve == true && self.gfKeyCheckDidResolve == true {
            
            self.activityIndicatorView?.stopAnimating()
            
            if self.userUIDKeyDoesExist == true && self.userGFKeyDoesExist == true {
                self.finalUserAuthState = true
                self.veriCodeDidReturnError = false

                //segue to CVC
                if self.didAdmitUser == false{
                    self.didAdmitUser = true
                    UserDefaults.standard.set("false", forKey: "emailAuthSource")
                    UserDefaults.standard.set("true", forKey: "phoneAuthSource")
                    self.performSegue(withIdentifier: "toGridVC", sender: self)
                }
            } else {
                self.finalUserAuthState = false
                //-> this phone number record does exist, but no user record was fully assigned to number -> should delete account?
                
                self.veriCodeTextField.isUserInteractionEnabled = true

                self.veriCodeDidReturnError = true
                self.veriCodeErrorLabel?.isHidden = false
                self.veriCodeErrorLabel?.text = "Unable to locate account. Sign up, or try again."
            }
        }
        return false
    }
    
    func configureAuth() {
        // listen for changes in the authorization state
        _authHandle = Auth.auth().addStateDidChangeListener { (auth: Auth, user: User?) in
            // refresh table data
//            self.messages.removeAll(keepingCapacity: false)
//            self.messagesTable.reloadData()
            // check if there is a current user
            if let activeUser = user {
                // check if the current app user is the current FIRUser
                if self.user != activeUser {
                    self.user = activeUser
                }
            } else {
                // user must sign in
//                self.loginSession()
//                self.user = nil
            }
        }
    }
    
    func addActivityIndicator() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 7/414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = ((gridUnitWidth - aW) * 0.5) - 2
        let aY = (gridUnitWidth - aW) * 0.5
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        //        activityIndicatorView.color = UIColor.white
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        let centerX = sWidth * 0.5
        let centerY = (5 * superInsetHeight) + gridUnitHeight
        let centerPosition = CGPoint(x: centerX, y: centerY)
        
        //        self.continueButton2!.addSubview(activityIndicatorView)
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
        
        self.activityIndicatorView!.layer.position = centerPosition
        
        //        activityIndicatorView.startAnimating()
    }
        
    @IBAction func TapAreaOutside(_ sender: Any) {
        if self.authIsEmail == true {
            self.configureTapAreaOutsideUI_EmailView()
        } else {
            if self.nowInConfirmationScene == false {
                self.configureTapAreaOutsideUI_PhoneView()
            } else {
                self.tapArea_veriCode()
            }
        }
    }
    
    //1
    func configureTapAreaOutsideUI_EmailView() {
        if signInEmailTextField.isFirstResponder || signInPasswordTextField.isFirstResponder {
            signInEmailTextField.resignFirstResponder()
            signInPasswordTextField.resignFirstResponder()
            emailLineView.image = UIImage(named: "GrayLine")!
            passwordLineView.image = UIImage(named: "GrayLine")!
        } else {
            self.signInEmailTextField?.becomeFirstResponder()
        }
    }
    
    //2
    func tapArea_veriCode() {
        if self.veriCodeTextField.isFirstResponder {
            self.veriCodeTextField.resignFirstResponder()
            veriCodeLineView.image = UIImage(named: "GrayLine")!
            if let code = self.veriCodeTextField.text {
                if code.count >= 6 {
                    if self.veriCodeDidReturnError == false {
                        self.continueButton.isUserInteractionEnabled = true
                        self.continueButton.backgroundColor = self.activeGreenColor
                    } else {
                        self.veriCodeErrorLabel?.isHidden = true
                        self.veriCodeTextField.text = ""
                        self.veriCodeDidReturnError = false
                        self.continueButton.isUserInteractionEnabled = false
                        self.continueButton.backgroundColor = self.inactiveGrayColor
                    }
                }
            }
        } else {
            if self.veriCodeDidReturnError == true {
                self.veriCodeTextField.becomeFirstResponder()
                self.veriCodeLineView.image = UIImage(named: "BlueLine")!
                self.veriCodeTextField?.text = ""
                self.veriCodeErrorLabel?.isHidden = true
                self.continueButton.isUserInteractionEnabled = false
                self.continueButton.backgroundColor = self.inactiveGrayColor
                self.veriCodeDidReturnError = false
            } else {
                self.veriCodeTextField.becomeFirstResponder()
                self.veriCodeLineView.image = UIImage(named: "BlueLine")!
                
            }
        }
        
    }
    
    //3
    func configureTapAreaOutsideUI_PhoneView() {
//        if phoneNumberTextField.isFirstResponder || veriCodeTextField.isFirstResponder {
//            phoneNumberTextField.resignFirstResponder()
//            veriCodeTextField.resignFirstResponder()
//            phoneNumberLineView.image = UIImage(named: "GrayLine")!
//            veriCodeLineView.image = UIImage(named: "GrayLine")!
//        } else {
//            self.phoneNumberTextField?.becomeFirstResponder()
//        }
        
        if self.phoneNumberTextField.isFirstResponder {
            self.phoneNumberTextField.resignFirstResponder()
            if let numberString = self.phoneNumberTextField!.text {
                if numberString.count >= 10 {
                    self.continueButton.backgroundColor = self.activeGreenColor
                    self.continueButton.isUserInteractionEnabled = true
                } else {
                    self.continueButton.isUserInteractionEnabled = false
                    self.continueButton.backgroundColor = self.inactiveGrayColor
                }
            }
        } else {
            if self.phoneNumberDidReturnError == false {
                //no error
                if let numberString = self.phoneNumberTextField!.text {
                    if numberString.count >= 10 {
                        self.continueButton.backgroundColor = self.activeGreenColor
                        self.continueButton.isUserInteractionEnabled = true
                    } else {
                        self.continueButton.isUserInteractionEnabled = false
                        self.continueButton.backgroundColor = self.inactiveGrayColor
                    }
                }
                self.continueButton.isUserInteractionEnabled = true
                self.continueButton.backgroundColor = self.activeGreenColor
            } else {
                //yes error
                self.phoneNumberErrorLabel?.isHidden = true
                self.phoneNumberTextField.text = ""
                self.phoneNumberDidReturnError = false
                self.continueButton.isUserInteractionEnabled = false
                self.continueButton.backgroundColor = self.inactiveGrayColor
            }
            
            self.phoneNumberTextField.becomeFirstResponder()
            if let numberString = self.phoneNumberTextField!.text {
                if numberString.count >= 10 {
                    self.continueButton.backgroundColor = self.activeGreenColor
                    self.continueButton.isUserInteractionEnabled = true
                } else {
                    self.continueButton.isUserInteractionEnabled = false
                    self.continueButton.backgroundColor = self.inactiveGrayColor
                }
            }
        }

    }
    
    //MARK: START Add UIAlerts
    
    var notificationsPrompt: UIAlertController?
    
    func addNotificationRequiredAlert() {
        let alertTitle = "One-Time SMS Notification"
        //        let alertMessage = "Ximo needs permission to send notifications to your device so we can send you an SMS confirmation code." + "\n" + "You will only receive notifications from Ximo when you need a verification code to sign in again in the future."
        let alertMessage = "Please enable notifications to receive an SMS confirmation code." + "\n" + "\n" + "You will only receive notifications from us again when you need to sign in with a verification code in future."
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let continueAction = UIAlertAction(title: "Continue", style: .default) { (action) in
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            
            //Apple: The first time your app makes this authorization request, the system prompts the user to grant or deny the request and records the user’s response. Subsequent authorization requests don't prompt the user.
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (authResult, error) in
                if error != nil {
                    //print("notification error", error)
                    
                } else {
                    if authResult == true {
                        //print("permission granted", authResult)
                        if self.backgroundRefreshIsAvailable == true {
                            DispatchQueue.main.async {
                                self.signInUser_Phone_Step1_Helper()
                            }
                        } else {
                            DispatchQueue.main.async {
                                self.present(self.backgroundRefresh_Alert!, animated: true)
                            }
                        }
                    } else {
                        //Denied
                        //print("permission denied", authResult)
                        DispatchQueue.main.async {
                            self.continueButton.isUserInteractionEnabled = true
                            self.continueButton.backgroundColor = self.activeGreenColor
                        }
                    }
                }
            })
            
            self.notificationsPrompt!.dismiss(animated: true, completion: nil)
        }
        
        let goBackAction = UIAlertAction(title: "Go Back", style: .default) { (action) in
            
            self.continueButton.isUserInteractionEnabled = true
            self.continueButton.backgroundColor = self.activeGreenColor
            self.notificationsPrompt!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(goBackAction)
        alert.addAction(continueAction)
        alert.preferredAction = continueAction
        self.notificationsPrompt = alert
    }
    
    var notificationsPrompt_toSettings: UIAlertController?
    
    func addNotificationRequired_toSettings() {
        let alertTitle = "One-Time SMS Notification"
        
        let alertMessage = "Please enable Notification Banner Alerts so you can receive an SMS confirmation code." + "\n" + "\n" + "You will only receive notifications from us again when you need a verification code in future."
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            self.notificationsPrompt_toSettings!.dismiss(animated: true, completion: nil)
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (result) in })
        }
        
        let goBackAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.notificationsPrompt_toSettings!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(goBackAction)
        alert.addAction(settingsAction)
        alert.preferredAction = settingsAction
        self.notificationsPrompt_toSettings = alert
    }
    
    var backgroundRefresh_Alert: UIAlertController?
    
    func addBackgroundRefreshAlert() {
        
        let alertTitle = "Enable Background Refresh"
        
        let alertMessage = "To receive an SMS confirmation code to your phone number, please enable Background Refresh on your device."
        
        //        let alertMessage = "To sign up using a phone number, please enable Background Refresh on your device." + "\n" + "Go to Settings -> General -> Background App Refresh."
        //        let alertMessage = "For phone-number sign-up to work properly on your device, please enable Background Refresh." + "\n" + "To enable Background App Refresh for Ximo: Go to Settings -> General -> Background App Refresh."
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            self.backgroundRefresh_Alert!.dismiss(animated: true, completion: nil)
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (result) in })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
            self.backgroundRefresh_Alert!.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        alert.addAction(settingsAction)
        alert.preferredAction = settingsAction
        self.backgroundRefresh_Alert = alert
    }
    
    
    
    
    
    
    //Other UI Adjustments
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    deinit {
        Auth.auth().removeStateDidChangeListener(_authHandle)
    }
    
}
