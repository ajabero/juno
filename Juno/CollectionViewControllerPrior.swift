/*//
//  CollectionViewController.swift
//  Juno
//
//  Created by Asaad on 2/10/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseStorage
import GeoFire

private let reuseIdentifier = "Cell"

class CollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, CLLocationManagerDelegate {
    //TODO:
    /*
     1. Add condition for when swapRequest is deleted (i.e. when swapRequest is deleted manually by user, or when user deletes account).
     make sure observe(.childRemoved) fires to update all locaitons in self and other accordingly.
     
     */
    //MARK: DB setup
    
    var ref: DatabaseReference!
    fileprivate var _refHandle: DatabaseHandle!
    
    //MARK: Global Properties
    
    //Location
    
    let locationManager = CLLocationManager()
    
    var importedUserLocation: CLLocation?
    var userLocation: CLLocation?
    var center: CLLocation?
    var userLongitude = ""
    var userLatitude = ""
    var userTimeStamp = ""
    
    //Control Flow Variables
    var selectedUserPrivacyPreference: String?
    var existingChatThreadID: String?
    var preexistingChatRequest: Bool? ; var preexistingChatRequestKey: String? ; var requestInitiatedBySelf: Bool?
    var selectedUserResponseState = "" //True (Accepted), False (Pending), Nil (Rejected/Ignored)
    
    //MARK: Data Structures
    
    //GeoLocation
    var uidinProximityRawArray = [usersinProximityStruct]()
    var sortedUIDinProximityArray = [String]()
    var photoURLStringArray = [String]()
    var userDictArray = [[String : String]]()
    
    struct usersinProximityStruct {
        var otherUserID = "a1x"
        var toOtherUserDistance = 0123.4567890
    }
    
    //Chat Configuration
    var allChatThreadIDsDictionary = [String : [String: [String: AnyObject]]]()
    var userChatThreadObjectsArrayofDicts = [[String : String]]()
    var uidtoChatThreadIDIndexDictionary = [String: String]() //where String1 is UID & String2 is ChatThreadID
    var allChatThreadIDMarkersArray = [String]()
    var selfInitiatedChatRequestsIDsDict = [String:String]()
    
    //new
    var chatRequestPermissionsDictionary = [String:String]()
    var senderUIDtoChatRequestIDExtrapDict = [String:String]()
    var chatRequestsApprovedUIDArray = [String]()
    var chatRequestStatusDictionary = [String : String]()
    var selfInitiatedChatThreadIDsDictionary = [String : String]()
    
    //swap
    var selfInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID == selfUID, Maps ["otherUID" : "requestID"]
    var otherInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID != selfUID, Maps ["initiatorUID" : "requestID"]
    
    var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
    var selfInitiatedSwapRequest: Bool?
    var swapRequestStatus: String?
    var swapRequestStatusDictionary = [String:String]()
    
    
    //chatListVC
    //swap requests
    var chatListObjects = [[String : String]]()
    var chatListObjectIDs = [String]()
    
    //MARK: User Preferences
    
    var isHiddenPreference = true
    
    //MARK: Global Outlets
    
    
    @IBAction func generateNewKey(_ sender: Any) {
        self.generateNewPrivateKey()
    }
    
    func generateNewPrivateKey() {
        print("now generating private key")
        
        let privateKey = ref.child("globalChatThreads").childByAutoId().key
        print("private key is", privateKey )
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        self.configureDatabase()
        self.configureLocation()
        
        self.configureFlowLayout()
        
        self.observeActiveChatRequests()
        self.observeSentRequestsChanges()
        
        self.observeActiveSwapRequests()
        self.observeSwapRequestStatusChanges()
        
    }
    
    //MARK: UI Configuration
    
    func configureFlowLayout() {
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 9, 0, 9)
        flowLayout.itemSize = CGSize(width: 126, height: 182)
    }
    
    //MARK: Configure Location
    
    func setLocationParameters() {
        print("1.setting location parameters")
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; //~3
        locationManager.distanceFilter = 500 //The minimum distance (measured in meters) a device must move horizontally before an update event is generated. //~4
        locationManager.startUpdatingLocation() //~5
        //locationManager.startMonitoringSignificantLocationChanges()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Will update location coordinates")
        if let lastLocation = locations.last {
            print("2. getting location coordinates")
            let lat = lastLocation.coordinate.latitude
            let long = lastLocation.coordinate.longitude
            let time = lastLocation.timestamp
            
            self.userLongitude = String(describing: long)
            self.userLatitude = String(describing: lat)
            self.userTimeStamp = String(describing: time)
            
            self.userLocation = CLLocation(latitude: lat, longitude: long)
            print("user location has been set", self.userLocation!)
            self.updateUserLocationRecord(userNewLat: String(describing: lat), userNewLon: String(describing: long), userNewTimeStamp: String(describing: time))
        }
    }
    
    func configureLocation() {
        locationManager.delegate = self
        
        //Control-flow: if user is accessing Grid for first time from Account Setup, use importedUserLocation is initial value for user location, else, retrieve last location from DB
        if importedUserLocation != nil {
            self.observeKeysEntered()
            self.observeKeysReady()
        } else {
            self.getUserLastLocation()
            if CLLocationManager.locationServicesEnabled() {
                print("true, location services are enabled")
                self.setLocationParameters()
            } else {
                print("false, location services not enabled")
            }
        }
    }
    
    func getUserLastLocation() {
        guard let user = Auth.auth().currentUser else { return }
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        geoFire.getLocationForKey("\(user.uid)") { (location, error) in
            if (error != nil) {
                print("An error occurred getting the location for \"firebase-hq\": \(String(describing: error?.localizedDescription))")
            } else if (location != nil) {
                print("Location for user is [\(String(describing: location?.coordinate.latitude)), \(String(describing: location?.coordinate.longitude))]")
                self.userLocation = CLLocation(latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
                print("Geofire retrieved userlocation is,", self.userLocation!)
                self.observeKeysEntered()
                self.observeKeysReady()
            }
        }
    }
    
    //MARK: swapRequestObservers
    
    //Swap observer function: (1) If a user is selected, observer gets the SwapIDrequest (if any) associated with select users to pass information to UPVC and CSVC as needed.
    
    func observeActiveSwapRequests() {
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("globalSwapRequests/\(selfUID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            print("X3. snapshot value is,", snapshot.value)
            
            let newSwapRequestID = snapshot.key
            print("S1. newSwapRequestID is,", newSwapRequestID)
            
            guard let newSwapRequestBody = snapshot.value as? [String:String] else { return }
            print("S2. newSwapRequestBody is,", newSwapRequestBody)
            
            //UID
            guard let swapInitiatorID = newSwapRequestBody["swapInitiatorID"] else { return }
            print("S3. newswapInitiatorID is", swapInitiatorID )
            
            guard let swapRecipientID = newSwapRequestBody["swapRecipientID"] else { return }
            print("S4. newswapRecipientID is", swapRecipientID )
            
            //First Name
            guard let swapInitiatorFirstName = newSwapRequestBody["swapInitiatorFirstName"] else { return }
            print("S3. newswapInitiatorID is", swapInitiatorFirstName )
            
            guard let swapRecipientFirstName = newSwapRequestBody["swapRecipientFirstName"] else { return }
            print("S11. swapRecipientFirstName is", swapRecipientFirstName)
            
            //Default Image URL
            guard let swapInitiatorDefaultURL = newSwapRequestBody["swapInitiatorDefaultURL"] else { return }
            print("S9. swapRequestStatus is", swapInitiatorDefaultURL)
            
            guard let swapRecipientDefaultURL = newSwapRequestBody["swapRecipientDefaultURL"] else { return }
            print("S10. ACCIO swapRequestStatus is", swapRecipientDefaultURL)
            
            //New Photo URL
            guard let swapInitiatorPhotoURL = newSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            print("S5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL )
            
            guard let swapRecipientPhotoURL = newSwapRequestBody["swapRecipientPhotoURL"] else { return }
            print("S6. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
            
            //Did Open?
            guard let swapInitiatorDidOpen = newSwapRequestBody["swapInitiatorDidOpen"] else { return }
            print("S7. swapRequestStatus is", swapInitiatorDidOpen)
            
            guard let swapRecipientDidOpen = newSwapRequestBody["swapRecipientDidOpen"] else { return }
            print("S8. swapRequestStatus is", swapRecipientDidOpen)
            
            //Request Status
            guard let swapRequestStatus = newSwapRequestBody["swapRequestStatus"] else { return }
            print("S7. swapRequestStatus is", swapRequestStatus)
            
            //CSVC Swap Configuration
            if swapInitiatorID == selfUID {
                print("swapInitiator is self", swapInitiatorID)
                self.selfInitiatedSwapRequestIndexDict[swapRecipientID] = newSwapRequestID
                self.swapRequestStatusDictionary[swapRecipientID] = "false"
            } else {
                print("swapInitiator is other", swapInitiatorID)
                self.otherInitiatedSwapRequestIndexDict[swapInitiatorID] = newSwapRequestID
                self.swapRequestStatusDictionary[swapInitiatorID] = "false"
            }
            
            //CLVC Swap Configuration
            //L. ABSOLUTE Incoming requests other Notify me when someone wants to swap with me newly (2way only, we dont want 1way in UI)
            if swapRecipientID == selfUID && swapRequestStatus == "false" {
                print("Detected 2way request from other. Outcome: Adam Wants to Swap")
                
                var newListObject = [String:String]()
                newListObject["objectType"] = "swapRequestOtherNew"
                
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL
                
                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
            }
            
            //L. ABSOLUTE Outgoing requests from self
            if swapInitiatorID == selfUID && swapRequestStatus == "false" {
                print("Detected 2way request from self. Outcome: You Requested Swap")
                
                var newListObject = [String:String]()
                newListObject["objectType"] = "swapRequestSelfNew"
                
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                
                newListObject["swapInitiatorFirstName"] = "You"
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                
                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
            }
            
            //L. ABSOLUTE Accepted Requests by self
            if swapRecipientID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" {
                
                var newListObject = [String:String]()
                
                newListObject["requestStatus"] = swapRequestStatus
                if swapRecipientDidOpen == "false" {
                    newListObject["objectType"] = "otherInitSwapApprovedNew"
                } else if swapRecipientDidOpen == "true" {
                    newListObject["objectType"] = "otherInitSwapApprovedPre"
                }
                
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDefaultURL"] = swapInitiatorDefaultURL
                
                newListObject["swapRecipientDidOpen"] = swapRecipientDidOpen
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
            }
            
            //L. ABSOLUTE
            if swapInitiatorID == selfUID && swapRequestStatus == "true" && swapRecipientPhotoURL != "" {
                print("Detected case where someone approved my previously initiated request")
                
                var newListObject = [String:String]()
                
                newListObject["swapRequestID"] = newSwapRequestID
                newListObject["requestStatus"] = swapRequestStatus
                if swapInitiatorDidOpen == "false" {
                    newListObject["objectType"] = "selfInitSwapApprovedNew"
                } else if swapInitiatorDidOpen == "true" {
                    newListObject["objectType"] = "selfInitSwapApprovedPre"
                }
                newListObject["swapInitiatorFirstName"] = swapInitiatorFirstName
                newListObject["swapInitiatorDidOpen"] = swapInitiatorDidOpen //tracking swapInitiator since sender is self
                
                newListObject["swapRecipientFirstName"] = swapRecipientFirstName
                newListObject["swapRecipientDefaultURL"] = swapRecipientDefaultURL
                
                if !self.chatListObjectIDs.contains(newSwapRequestID) {
                    self.chatListObjectIDs.append(newSwapRequestID)
                    self.chatListObjects.append(newListObject)
                }
            }
            
        })
    }
    
    func observeSwapRequestStatusChanges() {
        print("func observeSwapRequestStatusChanges")
        
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("globalSwapRequests/\(user.uid)").observe(.childChanged , with: { (snapshot: DataSnapshot) in
            
            let existingSwapRequestID = snapshot.key
            print("M1. existingSwapRequestID is,", existingSwapRequestID)
            
            guard let modifiedSwapRequestBody = snapshot.value as? [String:String] else { return }
            print("M2. modifiedSwapRequestBody is,", modifiedSwapRequestBody)
            
            guard let swapInitiatorID = modifiedSwapRequestBody["swapInitiatorID"] else { return }
            print("M3. newswapInitiatorID is", swapInitiatorID )
            
            guard let swapRecipientID = modifiedSwapRequestBody["swapRecipientID"] else { return }
            print("M4. newswapRecipientID is", swapRecipientID )
            
            guard let swapInitiatorPhotoURL = modifiedSwapRequestBody["swapInitiatorPhotoURL"] else { return }
            print("M5. newswapInitiatorPhotoURL is", swapInitiatorPhotoURL )
            
            guard let swapRecipientPhotoURL = modifiedSwapRequestBody["swapRecipientPhotoURL"] else { return }
            print("M6. newswapRecipientPhotoURL is", swapRecipientPhotoURL )
            
            guard let swapInitiatorDidOpen = modifiedSwapRequestBody["swapInitiatorDidOpen"] else { return }
            print("S7. swapRequestStatus is", swapInitiatorDidOpen)
            
            guard let swapRecipientDidOpen = modifiedSwapRequestBody["swapRecipientDidOpen"] else { return }
            print("S7. swapRequestStatus is", swapRecipientDidOpen)
            
            guard let modifiedSwapRequestStatus = modifiedSwapRequestBody["swapRequestStatus"] else { return }
            print("M7. modifiedSwapRequestStatus is", modifiedSwapRequestStatus)
            
            //Swap Status Modifier
            if swapInitiatorID == selfUID {
                print("M8. Self-initiated swap request status now has status,", modifiedSwapRequestStatus)
                self.swapRequestStatusDictionary[swapRecipientID] = modifiedSwapRequestStatus
                
            } else {
                print("M9. Other-initiated swap request status now has status,", modifiedSwapRequestStatus)
                self.swapRequestStatusDictionary[swapInitiatorID] = modifiedSwapRequestStatus
                
            }
            
            //Update chatListObjects for swaps
            if self.chatListObjectIDs.contains(existingSwapRequestID) {
                print("chatListObjectIDs contains existingSwapRequestID")
                
                let i = self.chatListObjects.index(where: { (dictionaryOfInterest) -> Bool in
                    dictionaryOfInterest["swapRequestID"] == existingSwapRequestID
                })
                
                //When I approve someone's previous reqyest
                if swapRecipientID == selfUID &&  modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" {
                    print("ACCIO 2 chatListObjects", self.chatListObjects)
                    
                    if swapRecipientDidOpen == "false" {
                        (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedNew"
                        (self.chatListObjects[i!])["swapRequestStatus"] = "true"
                        (self.chatListObjects[i!])["swapRecipientDidOpen"] = swapRecipientDidOpen
                        
                    } else if swapRecipientDidOpen == "true" {
                        (self.chatListObjects[i!])["objectType"] = "otherInitSwapApprovedPre"
                        (self.chatListObjects[i!])["swapRequestStatus"] = "true"
                        (self.chatListObjects[i!])["swapRecipientDidOpen"] = swapRecipientDidOpen
                        
                    }
                }
                
                //Self initiated swap requests that have been approved by other
                if swapInitiatorID == selfUID  && modifiedSwapRequestStatus == "true" && swapRecipientPhotoURL != "" {
                    //that have been opened
                    if swapInitiatorDidOpen == "false" {
                        (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedNew"
                        (self.chatListObjects[i!])["requestStatus"] = "true"
                        (self.chatListObjects[i!])["swapInitiatorDidOpen"] = "false"
                        
                    } else if swapInitiatorDidOpen == "true" {
                        (self.chatListObjects[i!])["objectType"] = "selfInitSwapApprovedPre"
                        (self.chatListObjects[i!])["requestStatus"] = "true"
                        (self.chatListObjects[i!])["swapInitiatorDidOpen"] = "true"
                    }
                }
            }
        })
        
    }
    
    //MARK: Configure Chat Thread observers
    
    func configureDatabase() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        //this handle observes for newChatThreadID's in self & stores both chats initiated by self and by other
        _refHandle = self.ref.child("users/\(user.uid)/myActiveChatThreads").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            let newChatThreadIDMarker = snapshot.key
            
            //this allChatThreadIDMarkersArray all chatThreadID Markers stored in the user's local DB folder
            self.allChatThreadIDMarkersArray.append(newChatThreadIDMarker) //~unused
            
            let globalRequestsChatThreadID = newChatThreadIDMarker
            
            self.ref.child("globalChatThreads/\(globalRequestsChatThreadID)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
                print("I1. snapshot is,", snapshot)
                
                //let newMessageID = snapshot.key
                
                guard let messageBodyDictionary = snapshot.value as? NSDictionary else { return }
                print("I2. snapshot dictionary is", messageBodyDictionary)
                
                guard let messageSenderUID = messageBodyDictionary["messageSenderUID"] as? String else { return }
                print("I3. sender UID is,", messageSenderUID)
                
                guard let messageRecipientUID = messageBodyDictionary["messageRecipientUID"] as? String else { return }
                print("I4. recipient UID is,", messageRecipientUID)
                
                if messageSenderUID == selfUID {
                    print("I5. this chatThreadID was initiated by self")
                    
                    self.selfInitiatedChatThreadIDsDictionary[messageRecipientUID] = globalRequestsChatThreadID
                    print("I6.new selfInitiatedChatThreadsDictionary is,", self.selfInitiatedChatThreadIDsDictionary)
                    
                } else {
                    //
                }
                
                //this dictionary contains senderID:chatThreadID key-value pairs, including self
                
                if !self.uidtoChatThreadIDIndexDictionary.keys.contains(messageSenderUID) { //note: "!" condition reversal
                    print("I7. dictionary does not already contain UID")
                    
                    self.uidtoChatThreadIDIndexDictionary[messageSenderUID] = globalRequestsChatThreadID
                    print("I8. dictionary is now", self.uidtoChatThreadIDIndexDictionary)
                    
                } else {
                    print("I9. dictionary contains UID")
                }
            })
        })
    }
    
    func observeActiveChatRequests() {
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            
            let newChatRequestID = snapshot.key
            
            guard let newChatRequest = snapshot.value as? [String:String] else { return }
            
            let chatRequestFromUID = newChatRequest["chatRequestSenderUID"]!
            
            let chatRequestToUID = newChatRequest["chatRequesttoUID"]!
            
            if chatRequestFromUID == selfUID {
                
                self.selfInitiatedChatRequestsIDsDict[chatRequestToUID] = newChatRequestID
                self.chatRequestPermissionsDictionary[chatRequestToUID] = "false"
                
            } else {
                //add incoming request to chatPermissionsDictionary with default value as false
                self.chatRequestPermissionsDictionary[chatRequestFromUID] = "false"
                
                //create array that links each chatUserRequester to the IDRequest they initiated
                self.senderUIDtoChatRequestIDExtrapDict[chatRequestFromUID] = snapshot.key
                print("senderUIDtoChatRequestIDExtrapDict now contains", self.senderUIDtoChatRequestIDExtrapDict)
            }
            
        })
    }
    
    func observeSentRequestsChanges() {
        //note: these observers seem to keep observing even after current VC is exited (meaning observer keeps observing in background). Confirm this observation && decide if you need the observer to keep observing in background, otherwise remove the listener
        guard let user = Auth.auth().currentUser else { return }
        let selfUID = user.uid
        
        _refHandle = self.ref.child("globalChatRequests/\(user.uid)").observe(.childAdded, with: { (snapshot: DataSnapshot) in
            print("snapshot is,", snapshot)
            
            //let modifiedChatRequestID = snapshot.key
            
            guard let modifiedChatRequest = snapshot.value as? [String:String] else { return }
            
            guard let modifyingChatRequestSender = modifiedChatRequest["chatRequestSenderUID"] else { return }
            
            //we're only observing for changes in request status where we initiated requests, therefore we only account for cases where we are the requestSender
            //guard requestSenderUID == selfUID else { return } //WARNING!: THIS STATEMENT WAS COMMENTED OUT BECAUSE WHEN A USER WAS SELECTED, CODE WAS FAILING TO OBSERVE THE CHANGE IN REQUEST STATUS FOR USERS WHERE REQUEST SENDER WAS NOT SELF //ADD CONDITION LATER WHEN NEEDED THAT if requestSenderUID != self to omit case as needed.
            
            guard let chatRequestStatus = modifiedChatRequest["chatRequestStatus"] else { return }
            //print result
            print("new chatRequestStatus is,", chatRequestStatus)
            
            let chatRequesttoUID = modifiedChatRequest["chatRequesttoUID"]!
            
            //update result
            
            if modifyingChatRequestSender == user.uid {
                self.chatRequestPermissionsDictionary[chatRequesttoUID] = chatRequestStatus //resolved?
            } else {
                self.chatRequestPermissionsDictionary[modifyingChatRequestSender] = chatRequestStatus
            }
            
            self.chatRequestStatusDictionary[chatRequesttoUID] = chatRequestStatus
            
        })
    }
    
    //MARK: Discover Grid Configuration
    
    func observeKeysEntered() {
        print("4.now observing keys entered")
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        //guard let center = myLocation else { return }
        if importedUserLocation != nil {
            self.center = importedUserLocation
        } else {
            self.center = userLocation
        }
        
        let circleQuery = geoFire.query(at: self.center!, withRadius: 10) //radius in km
        
        //Observe keys within radius
        circleQuery.observe(.keyEntered, with: { (key:String!, location:CLLocation!) in
            
            //calculate self's distance to retrieved user distance
            let locationAcross = self.center?.distance(from: location)
            
            //instantiate structure containing ID of user within proximity & add UID and distanceTo values to this structure
            var userInstance = usersinProximityStruct()
            userInstance.otherUserID = key
            userInstance.toOtherUserDistance = locationAcross!
            
            //build array of all users within proximity, where each array entry is a struct containing the UID and distanceTo vlaues of each instance
            
            self.uidinProximityRawArray.append(userInstance)
        })
    }
    
    func observeKeysReady() {
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        //let center = userLocation
        
        let circleQuery = geoFire.query(at: center!, withRadius: 10) //radius in km
        
        circleQuery.observeReady({
            self.sortUserinProximity()
            self.populateArrayofDictionaries()
        })
    }
    
    func sortUserinProximity() {
        
        let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
        
        //initiate first here: for i in 0..<100 in sortedArray
        for i in 0..<sortedStructArray.count {
            self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
        }
        
        self.buildPhotoURLStringArray()
        //print("Test 1: \(self.urlStringArray)")
        //self.populateArrayofDictionaries()
    }
    
    func buildPhotoURLStringArray() {
        
        for UID in self.sortedUIDinProximityArray {
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                if let imageStringURL = value?["full_URL"] as? String {
                    self.photoURLStringArray.append(imageStringURL)
                }
            })
        }
    }
    
    func populateArrayofDictionaries() {
        //check that our sortedUIDinProximityArray contains all UID values sorted by distance
        for UID in self.sortedUIDinProximityArray {
            print("getting value for UID", UID)
            //Get the datasnapshot for user
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else { return }
                guard let userDisplayName = value["userDisplayName"] as? String else { return }
                guard let userThumbnailURLString = value["full_URL"] as? String else { return }
                guard let userPrivacyPreference = value["privacy"] as? String else { return }
                guard let visus = value["visus"] as? String else { return }
                //guard let userAge = value["userAge"] as? String else { return }
                
                var userGridThumbInfoDict = [String: String]()
                
                //Create key-value pairs for each user
                userGridThumbInfoDict["UID"] = snapshot.key
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                //dictionary["userAge"] = userAge
                userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
                userGridThumbInfoDict["visus"] = visus
                
                self.userDictArray.append(userGridThumbInfoDict)
                print("User Array Dictionary now contains: \(self.userDictArray)")
                self.collectionView!.reloadSections([0])
            })
        }
    }
    
    // MARK: UICollectionViewDataSource
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    /*func numberOfSections(in collectionView: UICollectionView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }*/
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.userDictArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        
        //Set cell corner radius
        cell.contentView.layer.cornerRadius = 2.0
        cell.contentView.layer.masksToBounds = true
        
        //let imageView = cell.profilePhotoThumbnailView!
        let imageView = cell.profilePhotoThumbnailView as UIImageView
        
        //~imageView.sd_setImage(with: URL(string: importedURLStringArray[indexPath.row]))
        
        //imageView!.sd_setImage(with: stringURLArray[indexPath.row], completed: nil)
        //imageView!.sd_setImage(with: urlArray[indexPath.row], completed: nil)
        
        
        //Dictionary code
        let user = self.userDictArray[(indexPath as NSIndexPath).row]
        
        imageView.sd_setImage(with: URL(string: user["userThumbnailURLString"]!))
        //cell.userDisplayNameLabel.text = user["userDisplayName"]! + ", " + user["userAge"]!
        cell.userDisplayNameLabel.text = user["userDisplayName"]!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath:IndexPath) {
        
        // Grab the DetailVC from Storyboard
        let userProfileViewController = self.storyboard!.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        
        //get selectedUser
        let selectedUser = userDictArray[(indexPath as NSIndexPath).row]
        let selectedUserUID = selectedUser["UID"]!
        
        self.swapRequestControlFlow(selectedUserUID: selectedUserUID)
        
        //1. Set user privacy preference value: "true" or "false"
        self.selectedUserPrivacyPreference = selectedUser["userPrivacyPreference"]!
        print("CVC 1: selected user privacy pref is,", self.selectedUserPrivacyPreference)
        
        //2. Set existing chatThread id value
        if self.uidtoChatThreadIDIndexDictionary.keys.contains(selectedUserUID) {
            print("Therefore chat request with this user was previously initiated by other")
            
            guard let existingChatThreadID = self.uidtoChatThreadIDIndexDictionary[selectedUserUID] else { return }
            self.existingChatThreadID = existingChatThreadID
            print("CVC 2a1: existing chatThreadID initiated by other is,", self.existingChatThreadID)
            
        } else if self.selfInitiatedChatThreadIDsDictionary.keys.contains(selectedUserUID) {
            print("Therefore, chat request for this user was previously initiated by self")
            
            guard let existingChatThreadID = self.selfInitiatedChatThreadIDsDictionary[selectedUserUID] else { return }
            self.existingChatThreadID = existingChatThreadID
            print("CVC 2a2: existing chatThreadID initiated by self is,", self.existingChatThreadID)
            
        } else {
            print("CVC 2b: no preexisting chatThreadID detected, need to create new chatThreadID")
        }
        
        //check if there is a prexisting chat request for selected user whether from self or other
        
        //check if passed user has requested to chat with us or if we have requested passed user
        if self.chatRequestPermissionsDictionary.keys.contains(selectedUserUID) {
            self.preexistingChatRequest = true
            print("CVC 3: preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = self.senderUIDtoChatRequestIDExtrapDict[selectedUserUID]
            print("CVC 4: preexistingChatRequestKey exists,", self.preexistingChatRequestKey)
            
        } else if selfInitiatedChatRequestsIDsDict.keys.contains(selectedUserUID)  {
            self.preexistingChatRequest = true
            print("CVC 3: preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = self.selfInitiatedChatRequestsIDsDict[selectedUserUID]
            print("CVC 4: preexistingChatRequestKey exists,", self.preexistingChatRequestKey)
            
        } else {
            self.preexistingChatRequest = false
            print("CVC 3: no preexisting chat request exists,", self.preexistingChatRequest)
            
            self.preexistingChatRequestKey = nil
            print("CVC 4: no preexistingChatRequestKey exists")
            
            print("CVC 5: no request initiated")
        }
        
        //check to see if request initiated by self or other
        print("checking if request initiator is self or other")
        if selfInitiatedChatRequestsIDsDict.keys.contains(selectedUserUID) {
            print("chat Request initiated by self")
            self.requestInitiatedBySelf = true
            
        } else {
            print("chatr Request initiated by other")
            self.requestInitiatedBySelf = false
            
        }
        
        //set selected user response state
        if let userResponseState = self.chatRequestPermissionsDictionary[selectedUserUID] {
            print("CVC6: observed response state is,", userResponseState, "now setting self value")
            self.selectedUserResponseState = userResponseState
            print("CVC 6: user response state is,", self.selectedUserResponseState)
        } else {
            self.selectedUserResponseState = "false"
            print("CVC 6: response sate set to default false value")
        }
        
        print("Passing values CHECKPOINT:", "user is", userDictArray[(indexPath as NSIndexPath).row], "WITH privacy pref", self.selectedUserPrivacyPreference, "WITH existing ID?", self.existingChatThreadID, "WITH existing request?", self.preexistingChatRequest, "WITH request key?", self.preexistingChatRequestKey, "WHERE initiated byt self? ", self.requestInitiatedBySelf, "WHERE user response state", self.selectedUserResponseState)
        
        userProfileViewController.selectedUser = userDictArray[(indexPath as NSIndexPath).row]
        userProfileViewController.selectedUserPrivacyPreference = self.selectedUserPrivacyPreference
        userProfileViewController.existingChatThreadID = self.existingChatThreadID
        userProfileViewController.preexistingChatRequest = self.preexistingChatRequest
        userProfileViewController.preexistingChatRequestKey = self.preexistingChatRequestKey
        userProfileViewController.requestInitiatedBySelf = self.requestInitiatedBySelf
        userProfileViewController.selectedUserResponseState = self.selectedUserResponseState
        
        
        print("Passing SWAP values CHECKPOINT:", "1", self.swapRequestDoesExist, "2", self.absExistingSwapRequestID, "3", self.selfInitiatedSwapRequest, "4", self.swapRequestStatus)
        
        userProfileViewController.swapRequestDoesExist = self.swapRequestDoesExist
        if self.swapRequestDoesExist == true {
            print("ACCIO. swapRequestDoesExist is,", self.swapRequestDoesExist!, self.absExistingSwapRequestID!)
            userProfileViewController.absExistingSwapRequestID = self.absExistingSwapRequestID!
        } else {
            //
        }
        userProfileViewController.selfInitiatedSwapRequest = self.selfInitiatedSwapRequest
        userProfileViewController.swapRequestStatus = self.swapRequestStatus
        // Present the view controller using navigation
        self.present(userProfileViewController, animated: true, completion: nil)
    }
    
    //var selfInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID == selfUID, Maps ["otherUID" : "requestID"]
    //var otherInitiatedSwapRequestIndexDict = [String:String]() //If initiatorUID != selfUID, Maps ["initiatorUID" : "requestID"]
    
    //var swapRequestDoesExist: Bool?; var absExistingSwapRequestID: String?
    //var selfInitiatedSwapRequest: Bool?
    //var swapRequestStatus: String?
    //var swapRequestStatusDictionary = [String:String]()
    
    func swapRequestControlFlow(selectedUserUID: String) {
        
        print("Executing Swap Request Control Flow for UID,", selectedUserUID)
        
        //Checking if previous swapRequestID exists and who is initiator
        if self.otherInitiatedSwapRequestIndexDict.keys.contains(selectedUserUID) {
            //request does exist, update requestID accordingly
            self.swapRequestDoesExist = true //(1)
            self.absExistingSwapRequestID = self.otherInitiatedSwapRequestIndexDict[selectedUserUID] //(2)
            
            //request initiated by other
            self.selfInitiatedSwapRequest = false //(1)
            
        } else if self.selfInitiatedSwapRequestIndexDict.keys.contains(selectedUserUID) {
            //request does exist, update requestID accordingly
            self.swapRequestDoesExist = true
            self.absExistingSwapRequestID = self.selfInitiatedSwapRequestIndexDict[selectedUserUID]
            
            //request initiated by self
            self.selfInitiatedSwapRequest = true
        } else {
            self.swapRequestDoesExist = false
        }
        
        //Checking swap status
        if swapRequestDoesExist == true {
            //can observe request state
            //get swapRequestStatus
            guard let swapRequestStatus = self.swapRequestStatusDictionary[selectedUserUID] else { return }
            self.swapRequestStatus = swapRequestStatus
            print("P1. selected user request status is,", self.swapRequestStatus!)
            
        } else {
            //no need to observe request state
        }
        
    }
    
    func updateUserLocationRecord(userNewLat: String, userNewLon: String, userNewTimeStamp: String) {
        print("3. Now updating user location record")
        if let user = Auth.auth().currentUser {
            //update DB location coordinates
            print("updating user location in DB")
            self.ref.child("users/\(user.uid)/userLongitude").setValue(userNewLat)
            self.ref.child("users/\(user.uid)/userLatitude").setValue(userNewLon)
            self.ref.child("users/\(user.uid)/userLocationTimeStamp").setValue(userNewTimeStamp)
            
            //update geofire location
            let geoFireRef = Database.database().reference().child("userLocationGF")
            let geoFire = GeoFire(firebaseRef: geoFireRef)
            
            //set location of user in GeofireFolder in DB
            print("updating user location in Geofire folder")
            geoFire.setLocation(self.userLocation!, forKey: "\(user.uid)")
            print("Geofire location updated successfully")
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! ChatListViewController
        //pass dictionary of list objects
        controller.chatListObjects = self.chatListObjects
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        locationManager.stopUpdatingLocation()
    }
    
    //Still need to:
    /*
     Create a thumbnail for the bluirred image and upload that as well to the user's profile picture. Should pictures be in a folder?
     Then update the code in the table view to check for blurred prpofile condition if T/F and then request the url depending on the user's choice
     Must write T/F condition to user's profile folder
     
     Temporary location solution:
     Get user's last updated location from DB file, and and use that location to populate collection view. In the meantime, retrieve user location updates in the code in background, and update location record when viewWillDisappear. That way, user has to either reload the collection view for data to reload, or user has to go to another screen and come back
     */
    
}
// MARK: UICollectionViewDelegate

/*
 // Uncomment this method to specify if the specified item should be highlighted during tracking
 override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment this method to specify if the specified item should be selected
 override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
 return true
 }
 */

/*
 // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
 override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
 return false
 }
 
 override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
 
 }
 }
 */
*/
