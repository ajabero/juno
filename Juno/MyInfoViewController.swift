//
//  MyInfoViewController.swift
//  Juno
//
//  Created by Asaad on 4/28/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class MyInfoViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {
    
    var ref: DatabaseReference!
    
    var ref_AuthTracker = AuthTracker()

    var user: User?
    var userAge: String?
    var userDOB: String?
    var userDOB_Database: String?
    var userName: String?
    
    var userFirstName: String?
    var userInitial: String?

    //ATTN: DEFAULT ALWAYS SHOULD BE FALSE
    var shouldPresentNameView = false //TrustVC sets this value depending on authFlowFactor combinations

    override func loadView() {
        
        //FBCondition
        if self.minAge == nil {
            self.minAge = 18
        }
        
        //Note: no longer using FB Age Range - so set to minimum age required
        self.minAge = 18
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        mainView.backgroundColor = UIColor.white
        self.view = mainView
        self.addTapAreaOutside()
        self.addTitleLabel()
        self.addDOBTitleHeader()
        self.addBirthdayLabel()
        self.addLineView()
        self.addDatePicker()
        self.addAgeErrorLabel()
        self.addContinueButton()
    }
    
    var datePickerView: UIDatePicker?
    
    var titleLabel: UILabel?
    var dobHeader: UILabel?
    var lineView: UIImageView?
    var kbHeight = 226 as CGFloat

    var minAge: Int?
    var birthdayLabel: UILabel?

    var ageErrorTextView: UITextView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        self.populateMinAgeArray()
        self.populateMaxAgeArray()
        
        // Do any additional setup after loading the view.
        self.ageErrorTextView?.isHidden = true
        self.continueButton!.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        
        if self.shouldPresentNameView == true {
            self.configureNameViewPresentation()
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
        }
    }
    
    var sourceVC = ""
    
    var shouldNameShouldStayFirstResponder = false
    
    override func viewWillAppear(_ animated: Bool) {
        if self.shouldNameShouldStayFirstResponder == true {
            self.nameTextField?.becomeFirstResponder()

//            if self.sourceVC == "trustInfoVC" {
//            }
        }
    }
    
    func configureNameViewPresentation() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        self.dobHeader?.transform = CGAffineTransform(translationX: sWidth, y: 0)
        self.birthdayLabel?.transform = CGAffineTransform(translationX: sWidth, y: 0)
        self.lineView?.transform = CGAffineTransform(translationX: sWidth, y: 0)
//        self.datePickerView?.transform = CGAffineTransform(translationX: sWidth, y: 0)

        //hide picker view to screenBottom
        self.datePickerView?.transform = CGAffineTransform(translationX: 0, y: self.datePickerView!.frame.height)
        self.datePickerView?.isHidden = true
        
        //
        self.addFirstNameHeader()
        self.addNameTextField()
        self.addNameLineView()
        self.addNameErrorView()
        self.tapAreaOutside.isUserInteractionEnabled = true

        //
        self.continueButton?.frame.origin.y = sHeight - superInsetHeight
        self.nameErrorLabel?.isHidden = true
        
        self.addNameGuidelinesButton()
        self.nameTextField?.autocapitalizationType = .words
    }
    
    var tapAreaOutside: UIButton!
    
    func addTapAreaOutside( ) {
        let tapFrame = UIButton(frame: UIScreen.main.bounds)
        tapFrame.addTarget(self, action: #selector(MyInfoViewController.tapAreaOutside(_:)), for: .touchUpInside)
        tapFrame.isUserInteractionEnabled = false
        self.view.addSubview(tapFrame)
        self.tapAreaOutside = tapFrame
    }
    
    @objc func tapAreaOutside(_ sender: UIButton) {
        
        guard self.nameTextField != nil else { return }
        
        if self.shouldPresentNameView == true {
            if !self.nameTextField!.isFirstResponder {
                self.nameTextField!.becomeFirstResponder()
            }
        }
    }
    
    func addTitleLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = sWidth
        let lH = 35 as CGFloat
        var lX = 0 as CGFloat
        let lY = superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let titleLabel = UILabel(frame: tF)
    
        //init
        self.view.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.titleLabel!.text = "My Info"
        self.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 24)!
        self.titleLabel!.textColor = UIColor.black
        self.titleLabel!.textAlignment = .center
    }

    func addDOBTitleHeader() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
        let lY = superInsetHeight + 2 * gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let dobHeader = UILabel(frame: tF)
        
        dobHeader.text = "Date of Birth"
        dobHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        dobHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(dobHeader)
        self.dobHeader = dobHeader
    }
    
    
    func addBirthdayLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        let lY = 2 * superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let birthday = UILabel(frame: tF)
        
        birthday.text = "For age above " + String(self.minAge!)
        birthday.font = UIFont(name: "Avenir-Light", size: 14.0)
        birthday.textColor = UIColor.lightGray

        //init
        self.view.addSubview(birthday)
        self.birthdayLabel = birthday
    }
    
    func addLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = 0.5 as CGFloat
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.birthdayLabel!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineView = UIImageView(frame: f)
        lineView.backgroundColor = UIColor(displayP3Red: 65/255, green: 178/255, blue: 255/255, alpha: 1)
        
        self.view.addSubview(lineView)
        self.lineView = lineView
    }
    
    //MARK: NAME VIEWS
    var firstNameHeader: UILabel!
    
    func addFirstNameHeader() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth
        let lY = superInsetHeight + 2 * gridUnitHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let firstNameHeader = UILabel(frame: tF)
        
        firstNameHeader.text = "First Name"
        firstNameHeader.font = UIFont(name: "AvenirNext-Regular", size: 12)!
        firstNameHeader.textColor = UIColor(hue: 0, saturation: 0, brightness: 0.7, alpha: 1)
        
        //init
        self.view.addSubview(firstNameHeader)
        self.firstNameHeader = firstNameHeader
    }
    
    //add a name textField
    var nameTextField: UITextField?
    
    func addNameTextField() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 7 * gridUnitWidth
        let lH = 27 as CGFloat
        var lX = gridUnitWidth as CGFloat
        let lY = 2 * superInsetHeight
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let nameTextField = UITextField(frame: tF)
        nameTextField.autocapitalizationType = .words
        nameTextField.font = UIFont(name: "AvenirNext-Regular", size: 14.0)
        nameTextField.minimumFontSize = 12.0
        nameTextField.adjustsFontSizeToFitWidth = true
        nameTextField.placeholder = "What's the name you go by in everyday life?"
        nameTextField.autocapitalizationType = .none
        nameTextField.autocorrectionType = .no
        nameTextField.keyboardType = .emailAddress
        nameTextField.clearButtonMode = .whileEditing
        nameTextField.delegate = self
        
        self.view.addSubview(nameTextField)
        self.nameTextField = nameTextField
    }

    var nameLineView: UIImageView!
    
    func addNameLineView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let lW = 7 * gridUnitWidth
        let lH = 0.5 as CGFloat
        let lX = gridUnitWidth
        //        let lY = (2 * superInsetHeight) + gridUnitHeight
        let lY = self.birthdayLabel!.frame.maxY
        
        //
        let f = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineView = UIImageView(frame: f)
        lineView.backgroundColor = UIColor(displayP3Red: 65/255, green: 178/255, blue: 255/255, alpha: 1)
        
        self.view.addSubview(lineView)
        self.nameLineView = lineView
    }
    
    var nameErrorLabel: UITextView!
    
    func addNameErrorView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "That doesn't look right. Please make sure your name meets our guidelines."
        let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
        let th = ceil(textFieldCGSize.height)
        h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.lineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let nameErrorView = UITextView(frame: f)
        
        //
        nameErrorView.text = errorString
        self.view.addSubview(nameErrorView)
        self.nameErrorLabel = nameErrorView
        
        self.nameErrorLabel!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.nameErrorLabel!.textAlignment = .center
        self.nameErrorLabel!.isEditable = false
        self.nameErrorLabel!.isScrollEnabled = false
        self.nameErrorLabel!.isSelectable = false
        self.nameErrorLabel!.textContainer.lineFragmentPadding = 0
        self.nameErrorLabel!.textContainerInset = .zero
        self.nameErrorLabel!.textColor = UIColor.red
        self.nameErrorLabel!.backgroundColor = UIColor.clear
    }
    
    var nameGuidelines: UITextView?
    var bottomButtonPadding: CGFloat!
    var tapFrame: UIView!
    
    func addNameGuidelinesButton() {
        
        //
        let widthRatio = 244 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let tFontSize = 12.0 as CGFloat
        let buttonCopy = "Username Guidelines"
        let tWidth = sWidth - (2 * gridUnitWidth)
        let textFieldCGSize = self.sizeOfCopy(string: buttonCopy, constrainedToWidth: Double(tWidth), fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let marginSpace = 2 as CGFloat
        let tY = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)

        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let nameGuidelines = UITextView(frame: tFrame)
        nameGuidelines.text = buttonCopy
        nameGuidelines.font = UIFont(name: "Avenir-Medium", size: tFontSize)
        
        let tapFrameWidth = 4 * gridUnitWidth
        let tapFrameX = (sWidth - tapFrameWidth) * 0.5
        let tapFrame = UIView(frame: CGRect(x: tapFrameX, y: tY, width: tapFrameWidth, height: tH * 2))
        tapFrame.backgroundColor = UIColor.clear
        
        //
        self.view.addSubview(nameGuidelines)
        self.nameGuidelines = nameGuidelines
        
        self.view.addSubview(tapFrame)
        self.tapFrame = tapFrame
        
        self.nameGuidelines!.textAlignment = .center
        self.nameGuidelines!.isEditable = false
        self.nameGuidelines!.isScrollEnabled = false
        self.nameGuidelines!.isSelectable = false
        self.nameGuidelines!.textContainer.lineFragmentPadding = 0
        self.nameGuidelines!.textContainerInset = .zero
        self.nameGuidelines!.textColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        self.nameGuidelines!.backgroundColor = UIColor.clear
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MyInfoViewController.presentNameGuidelines(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.tapFrame.addGestureRecognizer(tapGesture)
        self.tapFrame.isUserInteractionEnabled = true
        
        self.bottomButtonPadding = UIScreen.main.bounds.height - self.nameGuidelines!.frame.maxY
    }
    
    var keyboardShouldHideViews = false
    
    @objc func presentNameGuidelines(_ sender: UITapGestureRecognizer) {
        //presentinfo VC with name guidelines
        
        guard self.nameTextField != nil else { return }
        
        self.keyboardShouldHideViews = true
        
        if self.nameTextField!.isFirstResponder {
            self.shouldNameShouldStayFirstResponder = true
        }
        
        self.performSegue(withIdentifier: "myInfotoInfoVC", sender: self)
    }
    
    //MARK: Configure textfield
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //set new font size
        nameTextField?.placeholder = ""
        nameTextField?.font = UIFont(name: "AvenirNext-Regular", size: 17.0)
        nameTextField?.minimumFontSize = 16.0
        self.nameLineView?.image = UIImage(named: "BlueLine")!
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.nameLineView.image = UIImage(named: "GrayLine")!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        let resrictedNumbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        let restrictedCharactersSet1 = ["$", "!", "~", "&", "=", "#", "[", "]", ".", "_", "-", "+", "@", " "]
        let restrictedCharactersSet2 = ["`", "|", "{", "}", "?", "%", "^", "*", "/", "'", "-", "+", "@", " "]
        
        let setA = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
        let setB =  ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        let setC = ["à", "á", "â", "ä", "è", "é", "ê", "ë", "ę", "î", "ï", "ś", "š", "ÿ", "û", "ü", "ù", "ú", "ū", "õ", "ō", "ø", "œ", "ó", "ò", "ö", "ô", "o", "ł", "ž", "ź", "ż", "ç", "ć", "ñ", "ń"]
        
        let fullSet = setA + setB + setC
        
        //lowercase the string and compare against set
       
        //print("textField strimg", string)
        
        if !fullSet.contains(string) {
            return false
        }
        
        if string == " " {
            return false
        } else {
            let characterCountLimit = 35
            let startingLength = textField.text?.count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace = range.length
            let stringLength = startingLength + lengthToAdd - lengthToReplace
            if stringLength >= 3 {

                self.continueButton!.backgroundColor = activeGreenColor
                self.nameErrorLabel!.isHidden = true
                self.continueButton!.isUserInteractionEnabled = true
                
            } else {


            }
            return stringLength <= characterCountLimit
        }
        
        return true
    }
    
//    func getMinDate() -> Date {
////        var dateComponents = DateComponents()
////        dateComponents.year = 1800
////        dateComponents.month = 1
////        dateComponents.day = 1
////        dateComponents.timeZone = TimeZone(abbreviation: "EST") // Japan Standard Time
////        dateComponents.hour = 0
////        dateComponents.minute = 0
////
////        // Create date from components
////        let userCalendar = Calendar.current // user calendar
////        let someDateTime = userCalendar.date(byAdding: <#T##DateComponents#>, to: <#T##Date#>)
////        return someDateTime!
//    }
   
    func addDatePicker() {
        let picker = UIDatePicker()
        let pH = picker.frame.height
//        let pY = UIScreen.main.bounds.height - pH - (self.kbHeight - pH)
        let pY = UIScreen.main.bounds.height - pH
        let frame = CGRect(x: 0, y: pY, width: UIScreen.main.bounds.width, height: pH)
        picker.frame = frame
        
        let currentDate = Date()
        var dateComponents = DateComponents()
        let calendar = Calendar.init(identifier: .gregorian)
        dateComponents.year = -(self.minAge!)
        let estimatedBirthday = calendar.date(byAdding: dateComponents, to: currentDate)
        picker.date = estimatedBirthday!
        
        let now = NSDate()
//        picker.minimumDate = self.getMinDate()
//        picker.minimumDate = now as Date

        picker.maximumDate = now as Date
        picker.addTarget(self, action: #selector(MyInfoViewController.dateChanged(_:)), for: .valueChanged)
        
        picker.datePickerMode = .date
        self.view.addSubview(picker)
        self.datePickerView = picker
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        
        self.continueButton?.backgroundColor = UIColor.green
        self.continueButton?.isUserInteractionEnabled = true
        
        let day = String(describing: components.day!)
        let month = String(describing: components.month!)
        let year = String(describing: components.year!)
        
        //mm/dd/yy
        let monthString = self.monthByInt[month]!
        let dobString = "\(monthString) \(day), \(year)"
        self.birthdayLabel!.text = dobString
        self.birthdayLabel!.font = UIFont(name: "AvenirNext-Regular", size: 17)
        self.birthdayLabel!.textColor = UIColor.black

        //get user age
        let now = NSDate()
        let calendar : NSCalendar = NSCalendar.current as NSCalendar
        let ageComponents = calendar.components(.year, from: sender.date, to: now as Date, options: [])
        let userAge = ageComponents.year!
        
        //check that age is higher than the minimum age range for the user
        if userAge >= self.minAge! && userAge >= 18 && userAge < 95 {
            //print("user age is,", userAge )
            //print("AGE SUCCESS")
            self.userAge = String(userAge)
            self.userDOB = month + day + year
            self.userDOB_Database = "m\(month)d\(day)y\(year)"
            self.ageErrorTextView?.isHidden = true
            self.continueButton?.backgroundColor = UIColor(displayP3Red: 81/255, green: 209/255, blue: 0, alpha: 1)
            self.continueButton?.isEnabled = true
            
            self.getUserAutoAge()
            
        } else if userAge < 18 {
            //print("AGE under limit")
            
            self.ageErrorTextView?.isHidden = false
            self.ageErrorTextView?.text = "Age under limit. Only 18+ allowed."
            self.continueButton?.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
            self.continueButton?.isEnabled = false
            
        } else if userAge > 95 {
            
            self.ageErrorTextView?.isHidden = false
            self.ageErrorTextView?.text = "That doesn't look right. Please try again."
            self.continueButton?.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
            self.continueButton?.isEnabled = false
            
        } else {
            //print("AGE not in bounds")
            let errorString = "The age for the date of birth you entered does not match your verified age range. Please try again."
            self.ageErrorTextView?.text = errorString
            self.ageErrorTextView?.isHidden = false
            self.continueButton?.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
            self.continueButton?.isEnabled = false
        }
        
    }
    
    func addAgeErrorLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 12.0 as CGFloat
        var w = 272 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 10.0 as CGFloat
            w = 226 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        var h = 0 as CGFloat
        let errorString = "The age for the date of birth you entered does not match your verified age range. Please try again."
            let textFieldCGSize = self.sizeOfCopy(string: errorString, constrainedToWidth: Double(w), fontSize: tFontSize)
            let th = ceil(textFieldCGSize.height)
            h = th
        
        let x = (sWidth - w) * 0.5
        let insetPadding = 5 as CGFloat
        let y = self.lineView!.frame.maxY + insetPadding
        
        //
        let f = CGRect(x: x, y: y, width: w, height: h)
        let nameLabel = UITextView(frame: f)
        
        //
        nameLabel.text = errorString
        self.view.addSubview(nameLabel)
        self.ageErrorTextView = nameLabel
        
        self.ageErrorTextView!.font = UIFont(name: "Avenir-Light", size: tFontSize)!
        self.ageErrorTextView!.textAlignment = .center
        self.ageErrorTextView!.isEditable = false
        self.ageErrorTextView!.isScrollEnabled = false
        self.ageErrorTextView!.isSelectable = false
        self.ageErrorTextView!.textContainer.lineFragmentPadding = 0
        self.ageErrorTextView!.textContainerInset = .zero
        self.ageErrorTextView!.textColor = UIColor.red
        self.ageErrorTextView!.backgroundColor = UIColor.clear
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "AvenirNext-DemiBold", size: fontSize)! ],
            context: nil).size
    }
    
    var continueButton: UIButton?
    var yFrame_ContButton_datePicker: CGFloat!
    
    func addContinueButton() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = 3 * gridUnitWidth
        let bH = 2 * gridUnitHeight
        let bX = (sWidth - bW) * 0.5
//        let pickerBtmIns = self.kbHeight - self.datePickerView!.bounds.height
        let bY = self.datePickerView!.frame.minY - (0.5 * gridUnitHeight) - bH
        self.yFrame_ContButton_datePicker = bY
        
        //
        let f = CGRect(x: bX, y: bY, width: bW, height:bH)
        let button = UIButton(frame: f)
        
        button.addTarget(self, action: #selector(MyInfoViewController.continueButton(_:)), for: .touchUpInside)
        button.backgroundColor = UIColor(displayP3Red: 81/255, green: 209/255, blue: 0, alpha: 1)
        button.setBackgroundImage(UIImage(named: "darkGreenBackGround")!, for: .highlighted)

        button.isUserInteractionEnabled = false
        self.view.addSubview(button)
        self.continueButton = button
        
        self.continueButton!.layer.cornerRadius = 8
        self.continueButton!.layer.masksToBounds = true
        
        self.continueButton!.setTitle("Continue", for: .normal)
        self.continueButton!.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 16.0)!
        self.continueButton!.setTitleColor(UIColor.white, for: .normal)
    }
    
    let allowableSingleFilterWords = ["Dick", "Al", "Jo", "Li", "Lu", "Xi", "Ty", "Bo"]

    @objc func continueButton(_ sender: UIButton) {
        //print("continueButton tap detected")
        
        if self.shouldPresentNameView == true {
            
            //1. Block UI if name does not meet guidelines
            //note continuebutton willnever be active unless >=3 characters, so safe to unwrap

            guard nameTextField != nil else { return }
            guard let finalNameString = self.nameTextField!.text else { return }
            let lowerCaseString = finalNameString.lowercased()
            
            
            //GUARD filter full words
            if self.filterForTerms.contains(lowerCaseString) {
                if self.allowableSingleFilterWords.contains(lowerCaseString) {
                    
                } else {
                    self.nameErrorLabel.isHidden = false
                    return
                }
                
            }
            
            //GUARD range filter words
            for filterWord in self.filterForTerms {
                if lowerCaseString.range(of: filterWord) != nil {
                    if self.allowableSingleFilterWords.contains(lowerCaseString) {
                        
                    } else {
                        self.nameErrorLabel.isHidden = false
                        return
                    }
                }
            }
            
            //PASS ALL GUARDS
            self.ref_AuthTracker.performBatchUpdates_Trust(withName: finalNameString, withAgeRange: "18", didAuthFB: "nil", didUnlinkFB: "nil", didResolveUnlinkFB: "nil")
            self.presentDatePickerUI()
            
            //RESET CONTINUE BUTTON
            self.shouldPresentNameView = false
            
        } else if self.shouldPresentNameView == false {
            let selfUID = Auth.auth().currentUser!.uid
//        self.updateUserAgeValue()
            self.getUserAutoAge()
        
            //Source call instantiates age filter AS DB string values
            self.updateAgeFilter()
            UserDefaults.standard.set(self.userAge!, forKey: "\(selfUID)_myAgeString")
            
            //
            self.ref_AuthTracker.performBatchUpdates_Info(withAge: self.userAge!, withDOB: self.userDOB_Database!, withMinAge: self.minAgeSelected, withMaxAge: self.maxAgeSelected)
            self.performSegue(withIdentifier: "infoToPVC", sender: self)
        }
    }
    
    func presentDatePickerUI() {
        
        
        let screenWidth = -UIScreen.main.bounds.width
        
        //
        //            self.animateDatePicker(present: true)
        

        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.dobHeader?.transform = CGAffineTransform(translationX: 0, y: 0)
            self.birthdayLabel?.transform = CGAffineTransform(translationX: 0, y: 0)
            self.lineView?.transform = CGAffineTransform(translationX: 0, y: 0)
            self.datePickerView?.transform = CGAffineTransform(translationX: 0, y: 0)
            
            self.continueButton?.isUserInteractionEnabled = false
            self.continueButton?.backgroundColor = self.inactiveGrayColor
            
        }) { (true) in
            
        }
        
        //
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
            self.firstNameHeader?.transform = CGAffineTransform(translationX: screenWidth, y: 0)
            self.nameTextField?.transform = CGAffineTransform(translationX: screenWidth, y: 0)
            self.nameLineView?.transform = CGAffineTransform(translationX: screenWidth, y: 0)
            self.nameErrorLabel?.transform = CGAffineTransform(translationX: screenWidth, y: 0)
            
        }) { (true) in
            
        }
        
        //picker & button displacement & hide guidelinesbutton
        
        self.nameTextField?.resignFirstResponder()
        let duration = 0.25
        
        self.datePickerView?.isHidden = false
        self.tapFrame.removeFromSuperview()
        self.nameGuidelines?.removeFromSuperview()
        
        UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            self.continueButton!.frame.origin.y = self.yFrame_ContButton_datePicker
            self.datePickerView?.transform = CGAffineTransform(translationX: 0, y: 0)
        }, completion: nil)
        
        
    }
    
    func animateDatePicker(present: Bool) {
        
        let datePicker = self.datePickerView!
        
        //declare pickerFrame
        let pW = UIScreen.main.bounds.width
        let pH = datePicker.frame.height
        let pX = 0 as CGFloat
        var pY = UIScreen.main.bounds.height - pH
        
        if present == false {
            pY = UIScreen.main.bounds.height
        } else {

        }
        
        let pickerFrame = CGRect(x: pX, y: pY, width: pW, height: pH)
        
        //declare barViewFrame
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let barW = UIScreen.main.bounds.width
        let barH = 2 * gridUnitHeight
        let barX = 0 as CGFloat
        var barY = pY - barH
        
        if present == false {
            barY = UIScreen.main.bounds.height
        }
        
        let barF = CGRect(x: barX, y: barY, width: barW, height: barH)
        
        let duration = 0.25
        //Animate frames
        datePicker.isHidden = false
        
        UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            //print("in animator")
//            datePicker.frame = pickerFrame
            self.datePickerView?.transform = CGAffineTransform(translationX: sWidth, y: -self.datePickerView!.frame.height)

        }, completion: nil)
    }
    
    
    /*
    func updateUserAgeValue() {
        if let user = Auth.auth().currentUser {
            
            let userAge = self.userAge!
            let userDOB_Database = self.userDOB_Database!
            
            self.ref = Database.database().reference()
            self.ref.child("users/\(user.uid)/userAge").setValue(userAge)
            self.ref.child("users/\(user.uid)/dateOfBirth").setValue(userDOB_Database)
            
            //set ancillary values
            let emptyString = ""
            self.ref.child("users/\(user.uid)/School").setValue(emptyString)
            self.ref.child("users/\(user.uid)/Work").setValue(emptyString)
            self.ref.child("users/\(user.uid)/AboutMe").setValue(emptyString)
            self.ref.child("users/\(user.uid)/Height").setValue(emptyString)
            self.ref.child("users/\(user.uid)/LookingFor").setValue(emptyString)
            self.ref.child("users/\(user.uid)/isOnline").setValue("true")
            
            self.ref.child("users/\(user.uid)/showMeMaxAge").setValue(emptyString)
            self.ref.child("users/\(user.uid)/showMeMinAge").setValue(emptyString)
            
            //self Age
            UserDefaults.standard.set(userAge, forKey: "\(user.uid)_myAgeString")
        }
    }
    */
    
    var minAgeArray = [String]()
    var maxAgeArray = [String]()
    
    func populateMinAgeArray() {
        self.minAgeArray.append("Minimum")
        for age in 18...60 {
            var ageString = String(age)
            if age == 60 {
                ageString = "60+"
            }
            self.minAgeArray.append(ageString)
            //print("self.minAgeArray is now,", self.minAgeArray)
        }
    }
    
    func populateMaxAgeArray() {
        self.maxAgeArray.append("Maximum")
        for age in 18...60 {
            var ageString = String(age)
            if age == 60 {
                ageString = "60+"
            }
            self.maxAgeArray.append(ageString)
            //print("self.maxAgeArray is now,", self.maxAgeArray)
        }
    }
    
    var userMinAutoAgeString = ""
    var userMaxAutoAgeString = ""
    
    var minAgeSelected = "Minimum"
    var maxAgeSelected = "Maximum" 
    
    func getUserAutoAge() {
        
        guard let myAge = self.userAge else { return }
        //print("myAge", myAge)
        
        let intMyAge = (myAge as NSString).integerValue

        var autoMinAgeInt = 18
        var autoMaxAgeInt = 100
        
        let minAgeSelectable = 18
        let maxAgeSelectable = 100
        
        let plusUpperRange = 12
        let minusLowerRange = 12
        
        let maxAgeBound = [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60]
        
        let lowerAgeDifference = intMyAge - minAgeSelectable
        
        if !maxAgeBound.contains(intMyAge) {
            //user is under 60
            if lowerAgeDifference < 12 {
                autoMinAgeInt = intMyAge - lowerAgeDifference
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
                
            } else {
                autoMinAgeInt = intMyAge - minusLowerRange
                //print("autoMinAgeInt is,", autoMinAgeInt)
                autoMaxAgeInt = intMyAge + plusUpperRange
            }
            
            let stringMinAge = String(autoMinAgeInt)
//            let minRowIndex = self.minAgeArray.index(of: stringMinAge)
            self.minAgeSelected = stringMinAge
            
            let stringMaxAge = String(autoMaxAgeInt)
//            let maxRowIndex = self.maxAgeArray.index(of: stringMaxAge)
            self.maxAgeSelected = stringMaxAge
            
            //print("autoMaxAgeInt is,", autoMaxAgeInt)

        } else {
            //print("user is 56 or older")
            
            autoMinAgeInt = intMyAge - minusLowerRange
            //print("autoMinAgeInt is,", autoMinAgeInt)
            autoMaxAgeInt = intMyAge + plusUpperRange
            //print("autoMaxAgeInt is,", autoMaxAgeInt)
            
            let stringMinAge = String(autoMinAgeInt)
//            let minRowIndex = self.minAgeArray.index(of: stringMinAge)
            self.minAgeSelected = stringMinAge
            
            //
//            let maxRowIndex = self.maxAgeArray.index(of: "Maximum")
            self.maxAgeSelected = "Maximum"
        }
    }
    
    func updateAgeFilter() {
        
        //print("updateAgeFilter")
        let minAgeSelected = self.minAgeSelected
        let maxAgeSelected = self.maxAgeSelected
        
        var showMeMinAge = ""
        var showMeMaxAge = ""
        
        if minAgeSelected == "Minimum" && maxAgeSelected == "Maximum" {
            //print("Case1: NO AGE FILTER SELECTED")
            showMeMinAge = ""
            showMeMaxAge = ""
        }
        
        if minAgeSelected == "60+" && maxAgeSelected == "Maximum" {
            //print("Case2: All above 60")
            showMeMinAge = "60"
            showMeMaxAge = ""
        }
        
        if minAgeSelected == "Minimum" && maxAgeSelected == "60+" {
            //print("Case3: NO AGE FILTER SELECTED")
            showMeMinAge = ""
            showMeMaxAge = ""
        }
        
        if minAgeSelected != "Minimum" && maxAgeSelected != "Maximum" && minAgeSelected != "60+" {
            //print("Case4: RANGE AGE FILTER SELECTED")
            
            if maxAgeSelected == "60+" {
                showMeMinAge = minAgeSelected
                showMeMaxAge = ""
            } else {
                showMeMinAge = minAgeSelected
                showMeMaxAge = maxAgeSelected
            }
        }
        
        if maxAgeSelected == "Maximum" && minAgeSelected != "Minimum" {
            //print("Case5: RANGE AGE FILTER SELECTED")
            if minAgeSelected == "60+" {
                showMeMinAge = "60"
                showMeMaxAge = ""
            } else {
                showMeMinAge = minAgeSelected
                showMeMaxAge = ""
            }
        }
        
        if minAgeSelected == "Minimum" && maxAgeSelected != "Maximum" && maxAgeSelected != "60+" && minAgeSelected != "60+" {
            showMeMinAge = ""
            showMeMaxAge = maxAgeSelected
        }
        
        //print("FINAL AGE FILTER IS: minAge,", showMeMinAge, "maxAge", showMeMaxAge)
        
        let selfUID = Auth.auth().currentUser!.uid
        
        //print("INFO VC WILLSET FINAL VALUES minAge,", showMeMinAge)
        //print("INFO VC WILLSET FINAL VALUES maxAge,", showMeMaxAge)

//        self.ref.child("users/\(selfUID)/showMeMinAge").setValue(showMeMinAge)
//        self.ref.child("users/\(selfUID)/showMeMaxAge").setValue(showMeMaxAge)
        
        UserDefaults.standard.set(showMeMinAge, forKey: "\(selfUID)_showMeMinAge")
        UserDefaults.standard.set(showMeMaxAge, forKey: "\(selfUID)_showMeMaxAge")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "infoToPVC" {
            let controller = segue.destination as! ProfileImageViewController
//            controller.userName = self.userName!
//            controller.userFirstName = self.userFirstName!
//            controller.userInitial = self.userInitial!
//            controller.userAge = self.userAge!
            
            controller.userName = " "
            controller.userFirstName = " "
            controller.userInitial = " "
            controller.userAge = " "
            
            controller.ref_AuthTracker = self.ref_AuthTracker
        } else if segue.identifier == "myInfotoInfoVC" {
            
            let controller = segue.destination as! TrustInfoViewController
            controller.sourceController = "usernameGuidelines"
            controller.withInfo = "usernameGuidelines"
        }
    }
    
    @IBAction func prepareForUnwindMyInfo (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    let activeGreenColor = UIColor(displayP3Red: 74/255, green: 191/255, blue: 0, alpha: 1)
    var inactiveGrayColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    
    @objc func keyboardWillShow(_ notification:Notification) {

        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let spaceToKeyboard = 1.5 * gridUnitHeight
        
        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton!.frame.height
        let keyboardHeight = getKeyboardHeight(notification)
        let risingStackViewPosition = screenHeight - keyboardHeight - buttonHeight - spaceToKeyboard
        self.continueButton!.frame.origin.y = risingStackViewPosition
        
        self.nameGuidelines?.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
        self.tapFrame.frame.origin.y = self.continueButton!.frame.maxY + (gridUnitHeight * 0.3)
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        if self.nameTextField?.text == "" {
            self.continueButton!.backgroundColor = inactiveGrayColor
            self.nameLineView!.image = UIImage(named: "GrayLine")!
            self.nameErrorLabel!.isHidden = true
        }
        
        let screenHeight = screen.size.height
        let buttonHeight = self.continueButton!.frame.height
        let restingStackViewPosition = screenHeight - superInsetHeight
        
        //transform if usercandismisskeyboard manually
        
        //transform to original continuebutton frame for datepicker
        //        self.toggleAuth?.frame.origin.y = restingStackViewPosition
        
        if self.keyboardShouldHideViews == true {
            self.keyboardShouldHideViews = false
            self.continueButton!.frame.origin.y = restingStackViewPosition
            self.nameGuidelines?.frame.origin.y = screenHeight - bottomButtonPadding - self.nameGuidelines!.frame.height
            self.tapFrame?.frame.origin.y = screenHeight - bottomButtonPadding - self.nameGuidelines!.frame.height
        }
    }
    
    var keyBoardHeight: CGFloat?
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        let keyBoardHeight = keyboardSize.cgRectValue.height
        self.keyBoardHeight = keyBoardHeight
        return keyBoardHeight
    }

    var monthByInt = [
        "1" : "January",
        "2" : "February",
        "3" : "March",
        "4" : "April",
        "5" : "May",
        "6" : "June",
        "7" : "July",
        "8" : "August",
        "9" : "September",
        "10" : "October",
        "11" : "November",
        "12" : "December",
    ]
    
    let filterForTerms = [
        "sex", "sexy", "sexboy", "sexman", "sexdude", "sexcraze", "penis", "dick", "bigd", "bigdick", "suck", "wnasuck", "wannasuck", "needhead", "head", "lookingforhead", "givehead", "givinghead", "dad", "daddy", "twink", "bear", "scruff", "scruffy", "ginger", "older", "younger", "youngforold", "oldforyoung", "boy", "sexyboy", "cunt", "trans", "femme", "transfemme", "maletrans", "transgenderman", "transmale", "male", "masc", "masculine", "nofemme", "justmasc", "onlymasc", "onlymasculine", "twink", "bear", "slim", "smooth", "hard", "bottom", "top", "topforbottom", "bottomfortop", "lookingfortop", "lookingforbottom", "nasty", "filthy", "sub", "dom", "dominant", "submissive", "cum", "sperm", "group", "groupsex", "facial", "menage", "threesome", "trio", "couple", "thick", "hot", "peach", "eggplant", "butt", "cheeks", "cheek", "fuckr", "fuck", "fuckboy", "lingus", "fellatio", "fellate", "fist", "anal", "anus", "asshole", "buthole", "jerk", "jerkoff", "wank", "wankr", "wanker", "jackoff", "getoff", "lick", "eatout", "eat", "eatyou", "eatyouout", "piss", "feet", "underwear", "cuddle", "movie", "host", "hosting", "travel", "hoster", "hotel", "myplace", "yourplace", "bathroom", "boo", "boobs", "breasts", "maleboobs", "malebreasts", "chesthair", "feet", "warmjizz", "jizz", "dicpic", "dicpics", "sendnudes", "hotfuck", "hottonight", "wannafuck", "wannalick", "wannasuck", "wild", "faggot", "boysgonewild", "wildboy", "wilderness", "forest", "backyard", "backdoor", "frontdoor", "sexposition", "position", "meforyou", "youforme", "facial", "massage", "massageyou", "rub", "bodyrub", "milf", "dilf", "pride", "flame", "flaming", "queen", "king", "bedroom", "bathroom", "toilet", "gaydar", "cock", "cocks", "cocksucker", "condom", "deep", "deepthroat", "throat", "fist", "drugs", "coke", "friendly", "touch", "shit", "prince", "princess", "fella", "fellow", "lads", "pussy", "thick", "fantasy", "roles", "horny", "horned", "hornedup", "misty", "mister", "mistress", "pimp", "whore", "slut", "slutbag", "slutface", "readbio", "read", "below", "hard", "weed", "hole", "destroy", "save", "virgin", "bubble", "porn", "porno", "pornographic", "single", "relationship", "network", "friends", "netflix", "chill", "infinity", "forceful", "fatcock", "pepsi", "beercan", "baseball", "btm", "bttm", "virginbtm", "vrgn", "virgn", "vrgin", "wank", "wanker", "wankr", "wanking", "grindr", "snowjob", "blowjob", "hung", "hungover", "rightnow", "now", "rn", "open", "meetup", "meat", "sausage", "valentine", "lanadelgay", "beercock", "beercan", "stiff", "frothy", "froth", "daredevil", "dirtdevil", "cumm", "lapdance", "dance", "strip", "stripper", "hooker", "reply", "scruff", "beard", "bearded", "chesthair", "love", "addict", "lkn", "lkng", "nw", "santa", "happy", "gay", "sadistic", "thirsty", "thirst", "first", "asian", "black", "arab", "whiteboy", "cute", "arse", "arsewhole", "areshole", "frigid", "spunk", "spy", "london", "boston", "newyork", "curious", "crious", "bicurious", "bis", "bisexual", "straight", "ugly", "fit", "hairy", "hair", "banger", "bang", "banging", "you", "pump", "thrust", "deep", "girl", "woman", "sugar", "sugardaddy", "sugarbaby", "sweet", "racy", "pokemon", "jiggle", "wiggle", "jiggly", "jigglypuff", "magic", "wand", "lord", "voldemort", "dump", "shootr", "heavy", "hvy", "hiv", "shooter", "shtr", "guess", "mrwho", "anonymous", "noname", "name", "username", "user", "abuse", "abuser", "child", "college", "jock", "jck", "jocks", "frat", "fratty", "bloker", "burly", "big", "swinging", "dick", "burleigh", "bro", "brother", "fraternity", "mainly", "bullmeat", "horse", "bull", "love", "orl", "oral", "bang", "pervy", "perved", "juice", "juicy", "balls", "cuff", "cuffs", "jacked", "jackr", "jackedup", "blk", "fatty", "fatties", "trans", "piss", "gimme", "latino", "super", "suceur", "someone", "somebody", "fake", "beyonce", "britney", "merry", "crack", "party", "loads", "load", "horn", "hornd", "torso", "send", "monster", "mnstr", "monstr", "cck", "dck", "lck", "cxs", "lostchats", "chat", "chats", "blow", "truck", "cruise", "cruisin", "delete", "deleting", "big", "huntr", "manhunt", "boobs", "titties", "tits", "spit", "fag", "faggot", "orgasm", "orgsm", "feet", "foot", "underwear", "money", "pimp", "hoe", "whore", "cash", "yummy", "sniff", "boner", "bonr", "bone", "boned", "boning", "slave", "master", "slv", "thrsm", "threesm", "three", "two", "four", "five", "morning", "hard", "hrd", "shades", "gym", "handjob", "job", "hnd", "only", "dickslip", "bedroom", "place", "finger", "toes", "fist", "cucumber", "eggplant", "peach", "carrot", "banana", "bannana", "bananna", "bnanan", "banana", "bnana", "grandma", "grandpa", "daddy", "prick", "penetrate", "penetration", "anal", "anl", "downlow", "downl", "dnlw", "bisexual", "pansexual", "cismale", "cisman", "leather", "lthr", "metal", "mtl", "skull", "fuck", "delete", "bored", "hulu", "netflix", "nflx", "ntflx", "ntlx", "chill", "fourtwnty", "frtwnty", "four", "twenty", "poppr", "ppr", "poppr", "popper", "poppers", "pprs", "pprz", "poprz", "poprs", "princess", "prince", "dreamboy", "dream", "marry", "marriage", "chat", "chats", "network", "friends", "here", "relationship", "love", "dates", "drinks", "drink", "bar", "beer", "wine", "alcoholr", "whiskey", "snake", "serpent", "lion", "horse", "tiger", "dawg", "animal", "savage", "tower", "urine", "tonight", "tnt", "evening", "mdnight", "night", "midnight", "wet", "face", "hell", "pussy", "bite", "biting", "scratch", "bang", "banging", "bangr", "smooth", "hunk", "college", "bro", "mance"
        ]
}
