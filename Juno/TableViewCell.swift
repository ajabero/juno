//
//  TableViewCell.swift
//  Juno
//
//  Created by Asaad on 2/15/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "chatCell")
        
        //add subviews
        self.addCellSubviews()
        
    }
    
    override func prepareForReuse() {
        //
        super.prepareForReuse()
        
        
    }
    
    func addCellSubviews() {
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

