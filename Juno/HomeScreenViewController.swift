//
//  HomeScreenViewController.swift
//  Juno
//
//  Created by Asaad on 3/24/18.
//  Copyright © 2018 Animata Inc. All rights reserved.

import UIKit
import Firebase
import FirebaseAuth

class HomeScreenViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //home screen needs to keep observing for changes in profile info, otherwise editor will not keep reference to newly updated values properly.
    var ref: DatabaseReference!
    var profileChangesUIDsInProximity = [String]()

    override func loadView() {
        self.ref = Database.database().reference()
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height

        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        mainView.backgroundColor = UIColor.white
        self.view = mainView

        //add views
        self.addLayerMask()
        
        self.addHomeIconButton()
        self.addDiscoveryIconButton()
        
        self.addMainScreenPhotoView()
        self.addMainScreenName()
        
        self.addEditProfileButton()
        self.addSettingsButton()
        self.addLogOutButton()
    }
    
    var layerMaskView: UIImageView?
    var homeIconButton: UIButton?
    var discoveryIconButton: UIButton?
    var mainScreenPhotoView: UIImageView?
    var mainScreenNameLabel: UITextView?
    
    var editProfileIconButton: UIButton?
    var settingsIconButton: UIButton?

    var editProfileLabel: UIButton?
    var settingsLabel: UIButton?
    var logoutLabelButton: UIButton?
    
    var photoFromEditor: UIImage?
    var myProfileInfoDict = [String:String]()

    var sourceController = "" //GridVC or EditorVC
    var unlockedUsers = [String]()
    
    func loadUserImage() {
        if let photoURL = self.myProfileInfoDict["selfDefaultPhotoURL"] {
            self.mainScreenPhotoView!.sd_setImage(with: URL(string: photoURL))
        }
    }
    
    override func viewDidLoad() {
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        super.viewDidLoad()
        if sourceController == "EditorVC" {
            if let photoFromEditor = photoFromEditor {
                //print("Z will get from editor")
                self.mainScreenPhotoView!.image = photoFromEditor
            } else {
                self.loadUserImage()
            }
        } else {
            self.loadUserImage()
        }
        
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        //print("sBAR HEIGHT is", statusBarHeight)
        self.updateUserPhotoOnChange()
        
        //add UserDictArrayChanges observer
//        NotificationCenter.default.addObserver(self, selector: #selector(HomeScreenViewController.updateOthProfileChanges), name: NSNotification.Name(rawValue: "updateOthProfileChanges"), object: nil)
    }
    
//    @objc func updateOthProfileChanges(_ notification: Notification) {
//        //print("HVC updateOthProfileChanges")
//
//        guard notification.name.rawValue == "updateOthProfileChanges" else { return }
//
//        if let userID = notification.userInfo?["forUID"] as? String {
//            //get object index
//            let i = self.userDictArray.index(where: { (dictionaryOfInterest) -> Bool in
//                dictionaryOfInterest["UID"] == userID
//            })
//
//            guard let index = i else { return }
//
//            //main
//            if let gridURL = notification.userInfo?["forUID"] as? String {
//                (self.userDictArray[index])["gridThumb_URL"] = gridURL
//            }
//            if let fullURL = notification.userInfo?["fullURL"] as? String {
//                (self.userDictArray[index])["userThumbnailURLString"] = fullURL
//            }
//            if let visus = notification.userInfo?["visus"] as? String {
//                (self.userDictArray[index])["visus"] = visus
//            }
//
//            //extra
//            if let url2 = notification.userInfo?["url2"] as? String {
//                (self.userDictArray[index])["full_URL_2"] = url2
//            }
//
//            if let url3 = notification.userInfo?["url3"] as? String {
//                (self.userDictArray[index])["full_URL_3"] = url3
//            }
//
//            if let url4 = notification.userInfo?["url4"] as? String {
//                (self.userDictArray[index])["full_URL_4"] = url4
//            }
//
//            //print("newval2", ((self.userDictArray[index])["full_URL_2"]))
//            //print("newval3", ((self.userDictArray[index])["full_URL_3"]))
//            //print("newval4", ((self.userDictArray[index])["full_URL_4"]))
//        }
//    }

    var statusBarHeight: CGFloat?
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.myProfileInfoDict = self.ref_SelfModelController.myProfileInfoDict
        
        self.statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        if sourceController == "EditorVC" {
            if let photoFromEditor = photoFromEditor {
                //print("Z will get from editor")
                self.mainScreenPhotoView!.image = photoFromEditor
            } else {
                self.loadUserImage()
            }
        } else {
            self.loadUserImage()
        }
        
        if let userDisplayName = self.myProfileInfoDict["selfDisplayName"] {
            let sWidth = UIScreen.main.bounds.width
            
            self.mainScreenNameLabel!.text = userDisplayName
            
            if let userAge = self.myProfileInfoDict["userAge"] {
                
                var tFontSize = 26.0 as CGFloat
                if sWidth > 350 as CGFloat {
                    //
                } else {
                    tFontSize = 22.0 as CGFloat
                }
                
                let fullString = "\(userDisplayName)," + " " + userAge
                
                let nameRange = (fullString as NSString).range(of: "\(userDisplayName),")
                let ageRange = (fullString as NSString).range(of: userAge)
                
                let boldFont = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
                let thinFont = UIFont(name: "Avenir-Light", size: tFontSize)!
                
                let attributedString = NSMutableAttributedString(string: fullString)
                
                attributedString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: nameRange)
                attributedString.addAttribute(NSAttributedStringKey.font, value: thinFont, range: ageRange)
                
                self.mainScreenNameLabel!.attributedText = attributedString
                self.mainScreenNameLabel!.textAlignment = .center
            }
        }
        
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        //print("sBAR HEIGHT is", statusBarHeight)
        
        //print("HOMESCREEN VIEWILLAPPEAR WILLFIRE getSelfProfileInfo")

//        self.updateUserPhotoOnChange()
        //ATTN: Switching on this method is producing choppyUI while CVC transitions
        
//        let when = DispatchTime.now() + 1
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            //arresto momento should be called when modalController touches top of view
//            self.getSelfProfileInfo()
//        }
//        self.getSelfProfileInfo()
        
//        self.mainScreenPhotoView?.image = UIImage(named: "pixlArt")
    }
    
    //THE OBSERVER IS NOT PROPERLY UPDATING VALUES AFTER UNWIND SEGUE. MAKE SURE THIS IS PROPERLY BEING SET.
    func getSelfProfileInfo() {
        //print("HOMESCREEN FIRING getSelfProfileInfo")
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)").observeSingleEvent(of: .value , with: { (snapshot: DataSnapshot) in
            guard let value = snapshot.value as? [String: Any] else { return }
            
            //print("HOME SCREEN FIRING OBSERVER selfINFO value", value)
            
            //            guard let userDateOfBirth = value["dateOfBirth"] as? String else { return }
            guard let userAge = value["userAge"] as? String else { return }
            guard let userDisplayName = value["userDisplayName"] as? String else { return }
            guard let userThumbnailURLString = value["full_URL"] as? String else { return }
            guard let userPrivacyPreference = value["privacy"] as? String else { return }
            guard let visus = value["visus"] as? String else { return }
            guard let userPhotoStorageRef = value["userDisplayPhoto"] as? String else { return }
            
            guard let school = value["School"] as? String else { return }
            guard let work = value["Work"] as? String else { return }
            guard let aboutMe = value["AboutMe"] as? String else { return }
            guard let height = value["Height"] as? String else { return }
            
            guard let blurState = value["blurState"] as? String else { return }
            guard let lookingFor = value["LookingFor"] as? String else { return }
            
            //guard let userAge = value["userAge"] as? String else { return }
            
            self.myProfileInfoDict["selfUID"] = snapshot.key
            self.myProfileInfoDict["privacy"] = userPrivacyPreference
            self.myProfileInfoDict["selfDisplayName"] = userDisplayName
            self.myProfileInfoDict["selfVisus"] = visus
            self.myProfileInfoDict["selfDefaultPhotoURL"] = userThumbnailURLString
            self.myProfileInfoDict["userAge"] = userAge
            self.myProfileInfoDict["blurState"] = blurState
            self.myProfileInfoDict["lookingFor"] = lookingFor
            self.myProfileInfoDict["userPhotoStorageRef"] = userPhotoStorageRef
            
            self.myProfileInfoDict["school"] = school
            self.myProfileInfoDict["work"] = work
            self.myProfileInfoDict["aboutMe"] = aboutMe
            self.myProfileInfoDict["height"] = height
            
            //additional photos
            if let photoURL2 = value["full_URL_2"] as? String {
                self.myProfileInfoDict["full_URL_2"] = photoURL2
            }
            
            if let photoURL3 = value["full_URL_3"] as? String {
                self.myProfileInfoDict["full_URL_3"] = photoURL3
            }
            
            if let photoURL4 = value["full_URL_4"] as? String {
                self.myProfileInfoDict["full_URL_4"] = photoURL4
            }
            
            //            self.selfVisus = visus
            //print("HOMESCREEN DIDLOAD SELFINFO self.myporifleInfo dict now contains,", self.myProfileInfoDict)
        })
//        self.mainScreenPhotoView!.sd_setImage(with: URL(string: myProfileInfoDict["selfDefaultPhotoURL"]!))
    }
    
    func updateUserPhotoOnChange() {
        //print("updateUserPhotoOnChange ")
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)").observe(.childChanged) { (snapshot) in
            //print("updateUserPhotoOnChange snapshot,", snapshot)
            
            //print("updateUserPhotoOnChange snapshot key,", snapshot.key)
            if snapshot.key == "full_URL" {
                guard let newURL = snapshot.value as? String else { return }
                //print("updateUserPhotoOnChange newURL is,", newURL)
                self.mainScreenPhotoView!.sd_setImage(with: URL(string: newURL))
                self.myProfileInfoDict["selfDefaultPhotoURL"] = newURL
            }
        }
    }
    
    func addLayerMask() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let vW = sWidth
        let vH = (5 * superInsetHeight) + (1 * gridUnitHeight)
        let vX = 0 as CGFloat
        let vY = 0 as CGFloat
        
        let f = CGRect(x: vX, y: vY, width: vW, height: vH)
        let layerMaskView = UIImageView(frame: f)
        
        //
        let tint = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
        layerMaskView.backgroundColor = tint
        
        //
        self.view.addSubview(layerMaskView)
        self.layerMaskView = layerMaskView
    }
        
    func addHomeIconButton() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
//        let rRatio = 37 / 414 as CGFloat
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = (sWidth - hW) * 0.5
        let insetFromStatusBar = 5 as CGFloat
        let hY = sBarH + insetFromStatusBar
//        let hY = sBarH + ((navSpaceHeight - sBarH - hH) * 0.5)
        
        //
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let homeIconButton = UIButton(frame: f)
        homeIconButton.contentMode = .scaleAspectFit
        homeIconButton.clipsToBounds = false
        homeIconButton.setImage(UIImage(named: "homeIcon_Main"), for: .normal)
        homeIconButton.addTarget(self, action: #selector(HomeScreenViewController.homeIconButton(_:)), for: .touchUpInside)
        homeIconButton.isUserInteractionEnabled = false
        self.view.addSubview(homeIconButton)
        self.homeIconButton = homeIconButton
    }
    
    @objc func homeIconButton(_ sender: UIButton) {
        //print("homeIconButton tap detected")
    }
    
    func addDiscoveryIconButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height

        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 33 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth

        //
        let dW = rRatio * sWidth
        let dH = dW
        let dX = sWidth - marginInset - dW
        var dY = 0 as CGFloat
            let centerAxis = self.homeIconButton!.frame.minY + (self.homeIconButton!.bounds.height * 0.5)
            dY = centerAxis - (dH * 0.5)
        
        //
        let f = CGRect(x: dX, y: dY, width: dW, height: dH)
        let discoveryButton = UIButton(frame: f)
        
        discoveryButton.setImage(UIImage(named: "discoveryIconSmall")!, for: .normal)
        discoveryButton.addTarget(self, action: #selector(HomeScreenViewController.toDiscoveryGrid(_:)), for: .touchUpInside)
        
        self.view.addSubview(discoveryButton)
        self.discoveryIconButton = discoveryButton
    }
    
    func addMainScreenPhotoView() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        //
        let pW = 4 * gridUnitWidth
        let pH = pW
        let pX = (sWidth - pW) * 0.5
        let pY = superInsetHeight + gridUnitHeight
        
        //
        let f = CGRect(x: pX, y: pY, width: pW, height: pH)
        let userPhotoView = UIImageView(frame: f)
        userPhotoView.layer.cornerRadius = pW * 0.5
        userPhotoView.layer.masksToBounds = true
        userPhotoView.backgroundColor = UIColor.lightGray
        userPhotoView.contentMode = .scaleAspectFill
        userPhotoView.clipsToBounds = true
//        userPhotoView.layer.borderWidth = 3
//        userPhotoView.layer.borderColor = UIColor.white.cgColor

     
        
        self.view.addSubview(userPhotoView)
        self.mainScreenPhotoView = userPhotoView
        
        //add gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeScreenViewController.editProfileButton(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.mainScreenPhotoView!.addGestureRecognizer(tapGesture)
        self.mainScreenPhotoView!.isUserInteractionEnabled = true
        
        self.addImageBorder()
    }
    
    var imageBorder: CAShapeLayer!
    
    func addImageBorder() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let cR = self.mainScreenPhotoView!.frame.width * 0.5
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: cR, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
       
        //shadowLayer
        let backGroundLayer = CAShapeLayer()
        backGroundLayer.path = circularPath.cgPath
        backGroundLayer.fillColor = UIColor.lightGray.cgColor
        backGroundLayer.lineWidth = 0
//        backGroundLayer.strokeColor = UIColor.white.cgColor
        
        let cPoint = self.mainScreenPhotoView!.center
        backGroundLayer.position = cPoint
        
//        backGroundLayer.shadowOpacity = 0.2
//        backGroundLayer.shadowOffset = CGSize(width: 0, height: 0)
//        backGroundLayer.shadowRadius = 3
        
        self.view.layer.addSublayer(backGroundLayer)
        
        //reposition
        self.view.bringSubview(toFront: self.mainScreenPhotoView!)

        //ringlayer
        let cLayer = CAShapeLayer()
        cLayer.path = circularPath.cgPath
        cLayer.fillColor = UIColor.clear.cgColor
        cLayer.lineWidth = 3.0
        cLayer.strokeColor = UIColor.white.cgColor
        cLayer.position = cPoint

        //        self.mainScreenPhotoView!.layer.addSublayer(cLayer)
        self.view.layer.addSublayer(cLayer)
        self.imageBorder = cLayer
    }
        
    
    func addMainScreenName() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 37 / 414 as CGFloat
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        
        // Font Size
        var tFontSize = 26.0 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 22.0 as CGFloat
        }
        //print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        let nW = 5 * gridUnitWidth
        var nH = 0 as CGFloat
            let testString = "MyNameIs"
            let textFieldCGSize = self.sizeOfCopy(string: testString, constrainedToWidth: Double(nW), fontSize: tFontSize)
            let tH = ceil(textFieldCGSize.height)
            nH = tH
        
        let nX = (sWidth - nW) * 0.5
        let nY = self.mainScreenPhotoView!.frame.maxY + gridUnitHeight
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let nameLabel = UITextView(frame: f)
    
        //
        nameLabel.text = "--"
        self.view.addSubview(nameLabel)
        self.mainScreenNameLabel = nameLabel
        
        self.mainScreenNameLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.mainScreenNameLabel!.textAlignment = .center
        self.mainScreenNameLabel!.isEditable = false
        self.mainScreenNameLabel!.isScrollEnabled = false
        self.mainScreenNameLabel!.isSelectable = false
        self.mainScreenNameLabel!.textContainer.lineFragmentPadding = 0
        self.mainScreenNameLabel!.textContainerInset = .zero
        self.mainScreenNameLabel!.textColor = UIColor.black
        self.mainScreenNameLabel!.backgroundColor = UIColor.clear
    }
    


    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: "AvenirNext-DemiBold", size: fontSize)! ],
            context: nil).size
    }
    
    var settingsLabelButton: UIButton?
    
    func addSettingsButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 30 / 414 as CGFloat
    
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = (sWidth - hW) * 0.5
        let hY = (6 * superInsetHeight) - hH
        
        //button View
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let settingsIconButton = UIButton(frame: f)
        
        settingsIconButton.setImage(#imageLiteral(resourceName: "settingsIcon"), for: .normal)
        settingsIconButton.addTarget(self, action: #selector(HomeScreenViewController.settingsIconButton(_:)), for: .touchUpInside)
        settingsIconButton.isUserInteractionEnabled = true
        self.view.addSubview(settingsIconButton)
        self.settingsIconButton = settingsIconButton
        
        //label View
        let lW = 55 as CGFloat
        let lH = 20 as CGFloat
        let lX = (sWidth - lW) * 0.5
            let labelSpace = 5 as CGFloat
        let lY = self.settingsIconButton!.frame.maxY + labelSpace
        
        let lFrame = CGRect(x: lX, y: lY, width: lW, height: lH)

        let settingsLabelButton = UIButton()
        settingsLabelButton.frame = lFrame
        settingsLabelButton.addTarget(self, action: #selector(HomeScreenViewController.settingsIconButton(_:)), for: .touchUpInside)
        self.view.addSubview(settingsLabelButton)
        self.settingsLabelButton = settingsLabelButton
        
        self.settingsLabelButton!.setTitle("Settings", for: .normal)
        self.settingsLabelButton!.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)!
        self.settingsLabelButton!.setTitleColor(UIColor.black, for: .normal)
    }

    func addEditProfileButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let rRatio = 30 / 414 as CGFloat
        
        //
        let hW = rRatio * sWidth
        let hH = hW
        let hX = (2 * gridUnitWidth) - (hW * 0.5)
        let hY = (6 * superInsetHeight) - hH
        
        //button View
        let f = CGRect(x: hX, y: hY, width: hW, height: hH)
        let settingsIconButton = UIButton(frame: f)
        
        settingsIconButton.setImage(#imageLiteral(resourceName: "editProfileIcon"), for: .normal)
        settingsIconButton.addTarget(self, action: #selector(HomeScreenViewController.editProfileButton(_:)), for: .touchUpInside)
        settingsIconButton.isUserInteractionEnabled = true
        self.view.addSubview(settingsIconButton)
        self.settingsIconButton = settingsIconButton
        
        //label View
        let lW = 75 as CGFloat
        let lH = 20 as CGFloat
        let lX = (2 * gridUnitWidth) - (lW * 0.5)
        let labelSpace = 5 as CGFloat
        let lY = self.settingsIconButton!.frame.maxY + labelSpace
        
        let lFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        
        let settingsLabelButton = UIButton()
        settingsLabelButton.frame = lFrame
        settingsLabelButton.addTarget(self, action: #selector(HomeScreenViewController.editProfileButton(_:)), for: .touchUpInside)
        self.view.addSubview(settingsLabelButton)
        self.settingsLabelButton = settingsLabelButton
        
        self.settingsLabelButton!.setTitle("Edit Profile", for: .normal)
        self.settingsLabelButton!.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 14.0)!
        self.settingsLabelButton!.setTitleColor(UIColor.black, for: .normal)
    }

    func addLogOutButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitY * sHeight
        let superInsetHeight = 4 * gridUnitHeight
        
        //label View
        let lW = 75 as CGFloat
        let lH = 20 as CGFloat
        let lX = (7 * gridUnitWidth) - (lW * 0.5)
        let labelSpace = 5 as CGFloat
        let lY = self.settingsIconButton!.frame.maxY + labelSpace
        
        let lFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        
        let logoutLabelButton = UIButton()
        logoutLabelButton.frame = lFrame
        logoutLabelButton.addTarget(self, action: #selector(HomeScreenViewController.logoutButton(_:)), for: .touchUpInside)
        self.view.addSubview(logoutLabelButton)
        self.logoutLabelButton = logoutLabelButton
        
        self.logoutLabelButton!.setTitle("Log Out", for: .normal)
        self.logoutLabelButton!.titleLabel!.font = UIFont(name: "AvenirNext-Bold", size: 14.0)!
        self.logoutLabelButton!.setTitleColor(UIColor.red, for: .normal)
    }
    
    @objc func toDiscoveryGrid(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "homeUnwinder", sender: self)

//        if self.ref_SelfModelController.cvcShouldReloadView == true {
//            self.performSegue(withIdentifier: "homeScreenToGrid", sender: self)
//            self.ref_SelfModelController.cvcShouldReloadView = false
//        } else {
//            self.performSegue(withIdentifier: "homeUnwinder", sender: self)
//        }
    }
    
    @objc func settingsIconButton(_ sender: UIButton) {
        //print("settingsIconButton tap detected")
        
        self.performSegue(withIdentifier: "homeToSettings", sender: self)
    }
    
    @objc func editProfileButton(_ sender: UIButton) {
        //print("editProfileButton tap detected")
        self.performSegue(withIdentifier: "toProfileEditor", sender: self)
    }
    
    //ATTENTION: NEED TO SET USER PRESENCE TO FALSE WHEN USER LOGS OUT - APP IS CURRENTLY NOT EXECUTING THIS FUNCTION AUTOMATICALLY
    @objc func logoutButton(_ sender: UIButton) {
        //print("logoutButton tap detected")
        
        let firebaseAuth = Auth.auth()
        self.activityIndicatorView?.startAnimating()
        let selfUID = Auth.auth().currentUser!.uid

        do {
            try firebaseAuth.signOut()
            //print("signout successful")
            self.performSegue(withIdentifier: "homeScreentoLogin", sender: self)
            //update DB presence
            
            self.ref.child("globalUserPresence/\(selfUID)/isOnline").setValue("false")
            self.ref.child("users/\(selfUID)/isOnline").setValue("false")

        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            self.activityIndicatorView?.stopAnimating()
        }
    }
    
    @IBAction func prepareForUnwind (segue: UIStoryboardSegue) {
        
    }
    
    override func unwind(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        let segue = UnwindPushSegue(identifier: unwindSegue.identifier, source: unwindSegue.source, destination: unwindSegue.destination)
        segue.perform()
    }
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    func addActivityIndicator() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let radiusRatio = 14 / 414 as CGFloat
        let cR = radiusRatio * sWidth
        
        let aW = cR * 2
        let aH = aW
        let aX = ((gridUnitWidth - aW) * 0.5) - 2
        let aY = (4 * superInsetHeight) + gridUnitHeight
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        //        activityIndicatorView.color = UIColor.white
        activityIndicatorView.color = UIColor.lightGray
        activityIndicatorView.hidesWhenStopped = true
        
        let centerX = sWidth * 0.5
        let centerY = (5 * superInsetHeight) + gridUnitHeight
        let centerPosition = CGPoint(x: centerX, y: centerY)
        
        self.view.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
    }
    
    var userDictArray = [[String : String]]()
    var userDictArray_UID = [String]()

    var ref_ModelController = ModelController()
    var ref_SelfModelController = SelfModelController()
//    var ref_SourceController = CollectionViewController()
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProfileEditor" {
            let controller = segue.destination as! EditorViewController
//            controller.myProfileInfoDict = self.myProfileInfoDict
            controller.unlockedUsers = self.unlockedUsers
            
            controller.ref_SelfModelController = self.ref_SelfModelController
            
            //passback the same photo if it was passedforward previously
            if self.sourceController == "EditorVC" {
                if let photoFromEditor = self.photoFromEditor {
                    controller.returnedImage = photoFromEditor
                }
            }
            
        } else if segue.identifier == "homeScreenToGrid" {
            //PUSH Segue to Grid (instantiate as new ViewController)
            let controller = segue.destination as! CollectionViewController
            
            controller.ref_SelfModelController = self.ref_SelfModelController

        } else if segue.identifier == "homeToSettings" {
            let controller = segue.destination as! SettingsViewController
            controller.ref_SelfModelController = self.ref_SelfModelController
            
        } else if segue.identifier == "homeScreentoLogin" {
            //this segues to WELCOME NOT LOGIN

        } else if segue.identifier == "homeUnwinder" {
            //UNWIND Segue to Grid
            
        }
        
    }
}
