//
//  WelcomeViewController.swift
//  Juno
//
//  Created by Asaad on 2/5/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import QuartzCore

class WelcomeViewController: UIViewController {
    
    var logoView: UIImageView!
    var joinButton: UIButton!
    var signInButton: UIButton!

    var ref_SelfModelController = SelfModelController()

    fileprivate var _authHandle: AuthStateDidChangeListenerHandle!
    var user: User?
    var ref: DatabaseReference!
    
    override func loadView() {
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height

        //initialize view
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        mainView.backgroundColor = UIColor.white
        self.view = mainView

        //add views
        self.drawLogoView()
        self.drawSignInButton()
        self.drawJoinButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var statusBarHeight: CGFloat?
    
    override func viewWillAppear(_ animated: Bool) {
        self.statusBarHeight = UIApplication.shared.statusBarFrame.height
        self.ref = Database.database().reference()
        self.checkAuth()
    }
    
    func drawLogoView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //proprotions
        let wR = 113 / sWidth
        let wHR = (27 / 113) as CGFloat
        
        //frame props
        let w = wR * sWidth
        let wH = w * wHR
        let wX = (sWidth - w) * 0.5
        let wY = (sHeight * 0.5) - wH
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let logoView = UIImageView(frame: f)
        logoView.image = UIImage(named: "logoVector")!
        
        self.view.addSubview(logoView)
        self.logoView = logoView
    }
    
    func drawSignInButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        //frame props
        let w = 58 as CGFloat
        let wH = 25 as CGFloat
        let wX = (sWidth - w) * 0.5
        let wY = sHeight - superInsetHeight - (wH * 0.5)
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let signInButton = UIButton(frame: f)
        signInButton.setTitle("Sign In", for: .normal)
        signInButton.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 18.0)!
        let blueColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        signInButton.setTitleColor(blueColor, for: .normal)
    
        self.view.addSubview(signInButton)
        self.signInButton = signInButton
        
        signInButton.addTarget(self, action: #selector(WelcomeViewController.signIn(_:)), for: .touchUpInside)
        signInButton.isUserInteractionEnabled = true
    }
    
    func drawJoinButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        //frame props
        let w = 58 as CGFloat
        let wH = 25 as CGFloat
        let wX = (sWidth - w) * 0.5
        let wY = self.signInButton.frame.maxY + (gridUnitHeight * 0.75)
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let joinButton = UIButton(frame: f)
        joinButton.setTitle("Join", for: .normal)
        joinButton.titleLabel!.font = UIFont(name: "Avenir-Medium", size: 18.0)!
        let blueColor = UIColor(displayP3Red: 0, green: 147/255, blue: 1, alpha: 1)
        joinButton.setTitleColor(blueColor, for: .normal)
        
        self.view.addSubview(joinButton)
        self.joinButton = joinButton
        
        joinButton.addTarget(self, action: #selector(WelcomeViewController.join(_:)), for: .touchUpInside)
        joinButton.isUserInteractionEnabled = true
    }
    
    
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    /*
    func drawJoinButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        //proprotions
        let wR = 160 / sWidth
        let wHR = 46 / 160 as CGFloat
        
        //frame props
        let w = wR * sWidth
        let wH = w * wHR
        let wX = (sWidth - w) * 0.5
        let wY = sHeight - superInsetHeight - wH
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let joinButton = UIButton(frame: f)
        joinButton.setImage(UIImage(named: "joinButton"), for: .normal)
        
        self.view.addSubview(joinButton)
        self.joinButton = joinButton
        
        joinButton.addTarget(self, action: #selector(WelcomeViewController.join(_:)), for: .touchUpInside)
        joinButton.isUserInteractionEnabled = true
    }
    */
    
    /*
    func drawSignInButton() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        //proprotions
        let wR = 82 / sWidth
        let wHR = 46 / 82 as CGFloat
        
        //frame props
        let w = wR * sWidth
        let wH = w * wHR
        let wX = (sWidth - w) * 0.5
        let insetRatio = (13 / wH) as CGFloat
        let wY = self.joinButton.frame.maxY + (insetRatio * wH)
        
        let f = CGRect(x: wX, y: wY, width: w, height: wH)
        let signInButton = UIButton(frame: f)
        signInButton.setImage(UIImage(named: "signInButton"), for: .normal)
        
        self.view.addSubview(signInButton)
        self.signInButton = signInButton
        
        signInButton.addTarget(self, action: #selector(WelcomeViewController.signIn(_:)), for: .touchUpInside)
        signInButton.isUserInteractionEnabled = true
    }
 */
    
    @objc func join(_ sender: UIButton) {
        self.performSegue(withIdentifier: "signUpSegue", sender: self)
    }
    
    @objc func signIn(_ sender: UIButton) {
        self.performSegue(withIdentifier: "signInSegue", sender: self)
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
//    func checkAuth() {
//        _authHandle = Auth.auth().addStateDidChangeListener { (auth: Auth, user: User?) in
//            // check if there is a current user
//            if let activeUser = user {
//                // check if the current app user is the current FIRUser
//                if self.user != activeUser {
//                    self.user = activeUser
//                    self.performSegue(withIdentifier: "welcomeToGrid", sender: self)
//
//                } else {
//
//
//                }
//            } else {
//                // user must sign in
//                //                self.loginSession()
//                //                self.user = nil
//            }
//        }
//    }

    func checkAuth() {
        Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                let userID = user.uid
                //Segue to CVC & let ref_SelfModelController determine if user record exists for current auth record
                //ATTN: Will ref_SelfModelController ever post authResult before CVC viewLoads?
                self.performSegue(withIdentifier: "welcomeToGrid", sender: self)
//                self.ref_SelfModelController.checkForExistingUserInstance(withUID: userID)
            } else {
                // No user is signed in.
            }
        }
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signInSegue" {
            let destinationController = segue.destination as! SignInViewController
//            destinationController.statusBarHeight = self.statusBarHeight!
        } else if segue.identifier == "signUpSegue" {
            let destinationController = segue.destination as! AuthViewController
            destinationController.statusBarHeight = self.statusBarHeight!
            
        } else if segue.identifier == "welcomeToGrid" {
            let destinationController = segue.destination as! CollectionViewController
            destinationController.ref_SelfModelController = self.ref_SelfModelController
        }
    }
    
}


