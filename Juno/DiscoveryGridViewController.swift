//
//  DiscoveryGridViewController.swift
//  Juno
//
//  Created by Asaad on 2/9/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//
/*
import UIKit
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseStorage
import GeoFire

class DiscoveryGridViewController: UIViewController {
    
    //DB setup
    var ref: DatabaseReference!
    
    //Arrays
    var uidinProximityRawArray = [usersinProximityStruct]()
    var sortedUIDinProximityArray = [String]()
    var photoURLStringArray = [String]()
    var userDictArray = [[String: String]]()
    
    var userLocation: CLLocation?

    struct usersinProximityStruct {
        var otherUserID = "lloppi3988jisjdf"
        var toOtherUserDistance = 3232.234349
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ref = Database.database().reference()
        
        self.observeKeysEntered()
        self.observeKeysReady()
    }
    
    func observeKeysEntered() {
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        guard let center = userLocation else { return }
        
        let circleQuery = geoFire.query(at: center, withRadius: 10) //radius in km
        
        //Observe keys within radius
        circleQuery.observe(.keyEntered, with: { (key:String!, location:CLLocation!) in
            
            //calculate self's distance to retrieved user distance
            let locationAcross = self.userLocation!.distance(from: location)
            
            //instantiate structure containing ID of user within proximity & add UID and distanceTo values to this structure
            var userInstance = usersinProximityStruct()
            userInstance.otherUserID = key
            userInstance.toOtherUserDistance = locationAcross
            
            //build array of all users within proximity, where each array entry is a struct containing the UID and distanceTo vlaues of each instance
            self.uidinProximityRawArray.append(userInstance)
        })
    }
    
    func observeKeysReady() {
        let geoFireRef = Database.database().reference().child("userLocationGF")
        let geoFire = GeoFire(firebaseRef: geoFireRef)
        
        guard let center = userLocation else { return }
        
        let circleQuery = geoFire.query(at: center, withRadius: 10) //radius in km
        
        circleQuery.observeReady({
            print(self.uidinProximityRawArray)
            self.sortUserinProximity()
            self.populateArrayofDictionaries()
        })
    }
    
    func sortUserinProximity() {
        
        let sortedStructArray = self.uidinProximityRawArray.sorted(by: {$0.toOtherUserDistance < $1.toOtherUserDistance})
        
        //initiate first here: for i in 0..<100 in sortedArray
        for i in 0..<sortedStructArray.count {
            self.sortedUIDinProximityArray.append(sortedStructArray[i].otherUserID)
        }
        
        print(sortedUIDinProximityArray)
        self.buildPhotoURLStringArray()
        //print("Test 1: \(self.urlStringArray)")
        //self.populateArrayofDictionaries()
    }
    
    func buildPhotoURLStringArray() {
        
        for UID in self.sortedUIDinProximityArray {
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? NSDictionary
                print(value!)
                if let imageStringURL = value?["full_URL"] as? String {
                    self.photoURLStringArray.append(imageStringURL)
                }
            })
        }
        print("EXPORT URL STRING ARRAY CONTAINING: \(self.photoURLStringArray)")
    }
    
    func populateArrayofDictionaries() {
        //check that our sortedUIDinProximityArray contains all UID values sorted by distance
        for UID in self.sortedUIDinProximityArray {
            //Get the datasnapshot for user
            ref.child("users").child(UID).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                guard let value = snapshot.value as? [String: Any] else { return }
                guard let userDisplayName = value["userDisplayName"] as? String else { return }
                guard let userThumbnailURLString = value["full_URL"] as? String else { return }
                //guard let userAge = value["userAge"] as? String else { return }
                
                var userGridThumbInfoDict = [String: String]()
                
                //Create key-value pairs for each user
                userGridThumbInfoDict["UID"] = snapshot.key
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                //dictionary["userAge"] = userAge
                userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
                
                self.userDictArray.append(userGridThumbInfoDict)
            })
        }
        print("User Array Dictionary now contains: \(self.userDictArray)")
    }
    
    
}

*/
