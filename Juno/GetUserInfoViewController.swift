//
//  GetUserInfoViewController.swift
//  Juno
//
//  Created by Asaad on 2/5/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import FBSDKLoginKit
import FBSDKCoreKit

class GetUserInfoViewController: UIViewController {
    
    var currentFBAccessToken: FBSDKAccessToken?
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0.3, alpha: 1.0)
        self.makeUIButton()
        //Get current access token
        if let currentFBAccessToken = FBSDKAccessToken.current()  {
            self.makeRequest()
        }
    }

    func makeRequest() {
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name, email, friends"])
        graphRequest?.start { (_, result, error) in
            if let error = error {
                print("Error fetching Facebook user data: \(error)")
            } else {
            //result is saved into dictionary before passed to completion handler
            let resultDictionary = result as? [String: Any]
            guard let id = resultDictionary!["id"] as? String else { return }
                
            let name = resultDictionary!["name"] as? String
            print("name is \(name!)")
            }
        }
    }
    
    func makeUIButton() {
        let buttonFont: UIFont = UIFont(name: "Avenir-Medium", size: 14)!

        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let widthRatio = 46.0/414.0
        let heightRatio = 92.0/726.0
    
        let buttonWidth = 7.00 * widthRatio
        let buttonHeight = 1.00 * heightRatio
        
        let newButton = UIButton(type: .custom)
        newButton.frame = CGRect(x: 0, y: 0, width: buttonWidth, height: buttonHeight)
        newButton.backgroundColor = UIColor.green
        newButton.center = view.center;
        newButton.setTitle("Proceed", for: .normal)
        newButton.titleLabel?.font = buttonFont
        newButton.titleLabel?.textColor = UIColor.white
        //add rounded corners
        newButton.layer.cornerRadius = 5.0
        newButton.layer.masksToBounds = true;
        
        view.addSubview(newButton)
        
    }
    
}
