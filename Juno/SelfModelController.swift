//
//  SelfModelController.swift
//  Ximo
//
//  Created by Asaad on 7/20/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import Firebase
import CoreLocation
import FirebaseAuth

class SelfModelController {

    var finalsBarH: CGFloat?
    var distanceToByUID = [String: String]()
    var ref: DatabaseReference!
    var myProfileInfoDict = [String : String]()
    var selfProfileInfoDidLoad = false
    var applicationDidCallSwapObserver = false
    var gridDidLoad = false
    var cvcShouldReloadView = false
    
    //
    var showMeMinAgeInt: Int?
    var showMeMaxAgeInt: Int?
    var myAge = 0
    //
    var chatAlertIsActive = false
    var swapAlertIsActive = false
    
    //incoming swaps
    var didSetSwapAlertForUserID = [String]()
    var didClearSwapAlertForUserID = [String]()
    
    //outgoing swaps
    var didSetSwapAlertForUserID_Outgoing = [String]()
     var didClearSwapAlertForUserID_Outgoing = [String]()
    //2
    
    var lastMessagesbyUID = [String : String]()
    
    var didClearOutgoingRequestForID = [String]()
    
    var didSeeSwapRequestForUID = [String: String]() { didSet {
            if didSeeSwapRequestForUID.values.contains("false") {
                //SET ALERT
                NotificationCenter.default.post(name: Notification.Name(rawValue: "setSwapAlertForCVC"), object: self)
                self.swapAlertIsActive = true
            } else {
                //CLEAR ALERT
                if self.chatAlertIsActive == false {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearSwapAlertForCVC"), object: self)
                }

                self.swapAlertIsActive = false
            }
        }
    }
    
    var didReadLastMessageForUID = [String : String]() { didSet {
            if didReadLastMessageForUID.values.contains("false") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "setChatAlertForCVC"), object: self)
                self.chatAlertIsActive = true

            } else {
                if self.swapAlertIsActive == false {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "clearChatAlertForCVC"), object: self)
                }
                self.chatAlertIsActive = false
            }
        }
    }

    var selfModelDidPostNotification = false
    
    func getSelfProfileInfo_ParserMethod(snapshot: DataSnapshot) {
   
        ref = Database.database().reference()

        //required
        guard let value = snapshot.value as? [String: Any] else { self.forceLogout(); return }
        let userID = snapshot.key
        guard let userDisplayName = value["userDisplayName"] as? String else { self.forceLogout(); return  }
        guard let userAge = value["userAge"] as? String else { self.forceLogout(); return  }
        guard let showMeMinAge = value["showMeMinAge"] as? String else { self.forceLogout(); return  }
        guard let showMeMaxAge = value["showMeMaxAge"] as? String else { self.forceLogout(); return  }
        guard let userThumbnailURLString = value["full_URL"] as? String else { self.forceLogout(); return  }
        guard let userPhotoStorageRef = value["userDisplayPhoto"] as? String else { self.forceLogout(); return  }
        guard let gridThumbURL = value["gridThumb_URL"] as? String else { self.forceLogout(); return  }
        guard let visus = value["visus"] as? String else { self.forceLogout(); return  }
//        guard let blurState = value["blurState"] as? String else { return }
        
        self.myProfileInfoDict["selfUID"] = userID
        self.myProfileInfoDict["selfDisplayName"] = userDisplayName
        self.myProfileInfoDict["userAge"] = userAge
        self.myProfileInfoDict["showMeMinAge"] = showMeMinAge
        self.myProfileInfoDict["showMeMaxAge"] = showMeMaxAge
        
        self.myProfileInfoDict["selfDefaultPhotoURL"] = userThumbnailURLString
        self.myProfileInfoDict["gridThumb_URL"] = gridThumbURL
        self.myProfileInfoDict["userPhotoStorageRef"] = userPhotoStorageRef

//        self.myProfileInfoDict["blurState"] = blurState
        self.myProfileInfoDict["selfVisus"] = visus

        //optionals
        if let aboutMe = value["AboutMe"] as? String {
            self.myProfileInfoDict["aboutMe"] = aboutMe
        } else {
            self.myProfileInfoDict["aboutMe"] = ""
        }
        
        if let school = value["School"] as? String {
            self.myProfileInfoDict["school"] = school
        } else {
            self.myProfileInfoDict["school"] = ""
        }
        
        if let work = value["Work"] as? String {
            self.myProfileInfoDict["work"] = work
        } else {
            self.myProfileInfoDict["work"] = ""
        }
        
        if let lookingFor = value["LookingFor"] as? String {
            self.myProfileInfoDict["lookingFor"] = lookingFor
        } else {
            self.myProfileInfoDict["lookingFor"] = ""
        }
        
        if let height = value["Height"] as? String {
            self.myProfileInfoDict["height"] = height
        } else {
            self.myProfileInfoDict["height"] = ""
        }
        
        if let userPrivacyPreference = value["privacy"] as? String {
            self.myProfileInfoDict["privacy"] = userPrivacyPreference
        } else {
            self.myProfileInfoDict["privacy"] = ""
        }
       
        if let blurState = value["blurState"] as? String {
            self.myProfileInfoDict["blurState"] = blurState
        } else {
            self.myProfileInfoDict["blurState"] = ""
        }

        //userNotificationToken
        if let pushID = value["pushID"] as? String {
            self.myProfileInfoDict["pushID"] = pushID
        } else {
            self.myProfileInfoDict["pushID"] = ""
        }
        
        //additional photos
        if let photoURL2 = value["full_URL_2"] as? String {
            self.myProfileInfoDict["full_URL_2"] = photoURL2
        } else {
            self.myProfileInfoDict["full_URL_2"] = ""
        }
        
        if let photoURL3 = value["full_URL_3"] as? String {
            self.myProfileInfoDict["full_URL_3"] = photoURL3
        } else {
            self.myProfileInfoDict["full_URL_3"] = ""
        }
        
        if let photoURL4 = value["full_URL_4"] as? String {
            self.myProfileInfoDict["full_URL_4"] = photoURL4
        } else {
            self.myProfileInfoDict["full_URL_4"] = ""
        }
        
        self.selfProfileInfoDidLoad = true
        
//        if self.selfModelDidPostNotification == false && self.gridDidLoad == false {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "selfProfileInfoDidLoad"), object: self)
//            self.selfModelDidPostNotification = true
//        }
    }
    
    func forceLogout() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "forceLogout_IncompleteSelfInfo"), object: self)
    }
    
    //Note: must not allow age value to be less than 17 or greater than 100 in DB
    
    func setSelfAgeFilterValues() {
        guard let selfUID = Auth.auth().currentUser?.uid else { return }
        
        //initialize my minimum age filter value
        if let minAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMinAge") {
            //print("setAgeFilterValues _showMeMinAge is,", minAge)
            if minAge != "" {
                self.showMeMinAgeInt = (minAge as NSString).integerValue
            } else {
                //
                //print("setAgeFilterValues willSET empty")
                self.showMeMinAgeInt = 18
            }
        } else {
            self.showMeMinAgeInt = 18
        }
        
        //initialize my maximum age filter value
        if let maxAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMaxAge") {
            //print("setAgeFilterValues _showMeMaxAge is,", maxAge)
            if maxAge != "" {
                self.showMeMaxAgeInt = (maxAge as NSString).integerValue
            } else {
                //
                //print("setAgeFilterValues willSET empty")
                self.showMeMaxAgeInt = 100
            }
        } else {
            self.showMeMaxAgeInt = 100
        }
        
        //Initialize my age int
        if let myAgeDefault = UserDefaults.standard.string(forKey: "\(selfUID)_myAgeString") {
            let ageInt = (myAgeDefault as NSString).integerValue
            self.myAge = ageInt
        } else {
            if let ageString = self.myProfileInfoDict["userAge"] {
                let ageInt = (ageString as NSString).integerValue
                self.myAge = ageInt
            }
        }
    }
    
    //MARK: Check User Auth
    var uidCheckDidResolve = false
    var gfKeyCheckDidResolve = false
    
    var userGFKeyDoesExist = false
    var userUIDKeyDoesExist = false
    
    var finalUserAuthState = false
    
    func checkForExistingUserInstance(withUID: String) {
        
        self.ref = Database.database().reference()
        
        self.ref.child("users/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("user does exist in DB")
                self.userUIDKeyDoesExist = true
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("user doesNotExist in DB")
                self.userUIDKeyDoesExist = false
                self.uidCheckDidResolve = true
                self.returnFinalUserAuthState()
            }
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("error checking snapshot")
            self.userUIDKeyDoesExist = false
            self.uidCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
        
        self.ref.child("userLocationGF/\(withUID)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //print("GF user does exist in DB")
                self.userGFKeyDoesExist = true
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()
            } else {
                //print("GF user doesNotExist in DB")
                self.userGFKeyDoesExist = false
                self.gfKeyCheckDidResolve = true
                self.returnFinalUserAuthState()
            }
            
        }) { (error) in
            //Note: User will be denied .read permission if their user node does not exist in DB. THEREFORE, THIS IS THE NODE THAT WILL EVALUATE IF NO UID EXISTS FOR THIS PHONE RECORD
            //print("GF error checking snapshot")
            self.userGFKeyDoesExist = false
            self.gfKeyCheckDidResolve = true
            self.returnFinalUserAuthState()
        }
    }
    
    var didAdmitUser = false
    
    func returnFinalUserAuthState() {
        var userInfoDict = [String:String]()

        if self.uidCheckDidResolve == true && self.gfKeyCheckDidResolve == true {
            if self.userUIDKeyDoesExist == true && self.userGFKeyDoesExist == true {
                self.finalUserAuthState = true
                //segue to CVC
                if self.didAdmitUser == false{
                    self.didAdmitUser = true
//                   Notify CVC to continue loading view
                    userInfoDict["didAdmitUser"] = "true"
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "observeFinalAuthState"), object: self, userInfo: userInfoDict)
                }
            } else {
                self.finalUserAuthState = false
                userInfoDict["didAdmitUser"] = "false"
                NotificationCenter.default.post(name: Notification.Name(rawValue: "observeFinalAuthState"), object: self, userInfo: userInfoDict)
//                  Notify CVC to force logout
            }
        }
    }
    
    var shouldQuery = true
    
    //CALLED IN CVC
    func observeOrderQuery() {
        
        self.ref = Database.database().reference()
        
        self.ref.child("queryOnline").observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let value = snapshot.value as? [String: String] {
                if let shouldQuery = value["shouldQuery"] {
                    if shouldQuery == "false" {
                       self.shouldQuery = false
                    } else if  shouldQuery == "true" {
                        self.shouldQuery = true
                    }
                }
            }
            
            }) { (error) in
                
            }
    }
    
    func firebaseConnect() {
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false {
                //print("Connected")
            } else {
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: "firebaseDisconnect"), object: self)
                //print("Not connected")
            }
        })
    }
}

//func print(_ items: Any...) {
//    #if DEBUG
//    if !items.isEmpty {
//        Swift.print(items[0])
//    }
//    #endif
//}
