//
//  ChatListTableViewCell.swift
//  Juno
//
//  Created by Asaad on 3/2/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.initializeCellFrame()
//        self.addCellSubviews()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "chatListCell")
        
//        self.addTestView()
        self.initializeCellFrame()
        self.addCellSubviews()
    }
        
    func addLineSeparator() {
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = self.frame.height
        
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //1. Add frameViewOutline
        let fW = cellHeight
        let fH = fW
        let fX = marginInset
        let fY = 0 as CGFloat
        
        let lW = sWidth - (2 * marginInset) - fW
        let lH = 50 as CGFloat
        let lX = self.thumbnailSuperView.frame.maxX + marginInset
        let lY = cellHeight
        
        let lineFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineSeparator = UIView(frame: lineFrame)
        let lineColor = UIColor(displayP3Red: 225/250, green: 225/250, blue: 225/250, alpha: 225/250)
        lineSeparator.backgroundColor = UIColor.green
        
        self.contentView.addSubview(lineSeparator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    weak var delegate: chatListCellDelegate?
    
//    @IBAction func acceptChatRequestButton(_ sender: Any) {
//        delegate?.acceptChatRequest(self)
//    }
    
//    @IBAction func dismissChatRequestButton(_ sender: Any) {
//        delegate?.rejectChatRequest(self)
//    }
    
//    @IBAction func newChatButton(_ sender: Any) {
//        delegate?.toNewChatButton(self)
//    }
    
//Thumbnail & Symbols
//    @IBOutlet weak var thumbnailSuperView: UIView!
//    @IBOutlet weak var thumbnailView: UIImageView!
//    @IBOutlet weak var thumbnailSymbol: UIImageView!

//Response Action Stack
//    @IBOutlet weak var chatAcceptButton: UIButton!
//    @IBOutlet weak var chatDismissButton: UIButton!
//    @IBOutlet weak var newChatSymb: UIButton!
    
    @objc func acceptChatRequestButton(_ sender: UIButton) {
        delegate?.acceptChatRequest(self)
    }

    @objc func dismissChatRequestButton(_ sender: UIButton) {
        delegate?.rejectChatRequest(self)
    }
    
    @objc func newChatButton(_ sender: UIButton) {
        delegate?.toNewChatButton(self)
    }
    
    var thumbnailSuperView: UIView!
    var thumbnailView: UIImageView!
    var thumbnailSymbol: UIImageView!
    var newMessageSymbol: UIImageView!
    
    //Chat Stack & Symbol
    var completeSwapSymbol: UIImageView!
    var userName: UILabel!
    var lastMessageLabel: UILabel!
    var oneWaySwapCompleteMarker: UIImageView!
    
    //Request Stack
//    var requestMainLabel: UILabel!
    var requestMainLabel: UIButton!
    var requestTimeStamp: UILabel!
    var dotsView: UIImageView!
    var cellLineSeparator: UIView!
    
    //
    func initializeCellFrame() {
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let hwRatio = 92 / 414 as CGFloat
        let assignedCellHeight = hwRatio * sWidth
        let newFrame = CGRect(x: 0, y: 0, width: sWidth, height: assignedCellHeight)
        self.frame = newFrame
    }
    
    func addCellSubviews() {
        self.addImageView()
        self.addRequestViews()
//        self.addActionButtonViews()
        self.addMessageViews()
    }
    
    func addTestView() {
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let boxBounds = UIView(frame: CGRect(x: 0, y: 0, width: sWidth, height: 100))
        boxBounds.backgroundColor = UIColor.blue
        
        self.contentView.addSubview(boxBounds)
        self.addSubview(boxBounds)
    }
    
    func addImageView() {
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = self.frame.height
        
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //1. Add frameViewOutline
        let fW = cellHeight
        let fH = fW
        let fX = marginInset
        let fY = 0 as CGFloat
        
        let frame = CGRect(x: fX, y: fY, width: fW, height: fH)
        let frameView = UIView(frame: frame)
        //        frameView.backgroundColor = UIColor.yellow
        
        self.contentView.addSubview(frameView)
        thumbnailSuperView = frameView

        //2. Then addImageView
        let imageWidthRatio = 76 / 414 as CGFloat
        
        let iW = imageWidthRatio * sWidth
        let iH = iW
        let iX = (self.thumbnailSuperView.bounds.width - iW) * 0.5
        let iY = iX
        
        let iFrame = CGRect(x: iX, y: iY, width: iW, height: iH)
        let userThumbnailView = UIImageView(frame: iFrame)
        userThumbnailView.layer.cornerRadius = userThumbnailView.frame.height * 0.5
        userThumbnailView.layer.masksToBounds = true
        userThumbnailView.backgroundColor = UIColor.clear
        userThumbnailView.contentMode = .scaleAspectFill
        
        self.contentView.addSubview(userThumbnailView)
        self.thumbnailView = userThumbnailView
        //        self.backgroundColor = UIColor.yellow
        
        //3. then add thumbnailSymbol view
        let symbWidthRatio = 38 / 414 as CGFloat
        let symbHeightRatio = 25 / 414 as CGFloat
        let symbAspectRatio = 25 / 38 as CGFloat
        
        let sW = symbWidthRatio * sWidth
        let sH = sW * symbAspectRatio
        let sX = (self.thumbnailSuperView.bounds.maxX) - sW
        let sY = (self.thumbnailSuperView.bounds.maxY) - sH
        
        let sF = CGRect(x: sX, y: sY, width: sW, height: sH)
        let thumbnailSymbol = UIImageView(frame: sF)
        thumbnailSymbol.backgroundColor = UIColor.clear
        thumbnailSymbol.contentMode = .center
        
        self.contentView.addSubview(thumbnailSymbol)
        self.thumbnailSymbol = thumbnailSymbol
        
        //add line separator
        let lW = sWidth - (2 * marginInset) - fW
        let lH = 0.25 as CGFloat
        let lX = self.thumbnailSuperView.frame.maxX + marginInset
        let lY = cellHeight - lH
        
        let lineFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lineSeparator = UIView(frame: lineFrame)
        let lineColor = UIColor(displayP3Red: 225/250, green: 225/250, blue: 225/250, alpha: 225/250)
        lineSeparator.backgroundColor = lineColor
        
        self.cellLineSeparator = lineSeparator
        self.addSubview(lineSeparator)
        
        //add unread message view
        let uRatio = 17 / sWidth
        let frameSpace = (fW - iW) * 0.5
        
        //add frame
        let uWidth = uRatio * sWidth
        let uHeight = uWidth
        let uX = self.thumbnailSuperView.frame.maxX - uWidth - marginInset - frameSpace
        let uY = self.thumbnailSuperView.frame.minY + marginInset
        
        let uFrame = CGRect(x: uX, y: uY, width: uWidth, height: uHeight)
        let uView = UIImageView(frame: uFrame)
        uView.image = UIImage(named: "chatListNewMessageSymbol")
        uView.contentMode = .center
        
        self.contentView.addSubview(uView)
        self.newMessageSymbol = uView
        self.newMessageSymbol.isHidden = true
    }
    
    func addRequestViews() {
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = self.frame.height
        
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add toSwap View
        let dWRatio = 23 / 414 as CGFloat
        let hRatio = 7 / 23 as CGFloat
        let rightInsetRatio = 17 / 414 as CGFloat
        let rightInset = rightInsetRatio * sWidth
        
        let dW = dWRatio * sWidth
        let dH = hRatio * dW
        let dX = sWidth - rightInset - dW
        let dY = (cellHeight - dH) * 0.5
        
        let dFrame = CGRect(x: dX, y: dY, width: dW, height: dH)
        let dotsView = UIImageView(frame: dFrame)
        dotsView.image = UIImage(named: "toSwapDots")
        
        self.contentView.addSubview(dotsView)
        self.dotsView = dotsView
        self.dotsView.isHidden = true
        
        //add request label
        var rWidth = sWidth - self.thumbnailSuperView!.frame.width - (1 * marginInset) - rightInset - dW
        let rHeight = cellHeight
        var rX = self.thumbnailSuperView!.frame.width + (1 * marginInset)
        let rY = 0 as CGFloat
        
        if sWidth < 350 {
            //adjust label width to not touch image view when screen is smaller than 6/7 Class
            rWidth = sWidth - self.thumbnailSuperView!.frame.width - (2 * marginInset) - rightInset - dW
            rX = self.thumbnailSuperView!.frame.width + (2 * marginInset)
        }
        
        let requestLabel = UIButton()
        requestLabel.frame = CGRect(x: rX, y: rY, width: rWidth, height: rHeight)
        requestLabel.backgroundColor = UIColor.white
        //        requestLabel.setTitle("You" + "\n" + "Asked Gabrielle to Show His Profile" , for: .normal)
        requestLabel.titleLabel?.numberOfLines = 2
        requestLabel.titleLabel?.lineBreakMode = .byWordWrapping
        self.contentView.addSubview(requestLabel)
        requestLabel.titleLabel?.textAlignment = .center
        requestLabel.isUserInteractionEnabled = false
        let textGray = UIColor(displayP3Red: 200/255, green: 200/255, blue: 200/255, alpha: 1)
        requestLabel.setTitleColor(textGray, for: .normal)
        requestLabel.titleLabel?.textColor = textGray
        self.requestMainLabel = requestLabel
        
        //
        let acceptButtonWidth = 63 as CGFloat
        
        //add timestamp
        let timeStampFrame = CGRect(x: rX, y: rY + (rHeight), width: rWidth, height: rHeight)
        let timeStampLabel = UILabel(frame: timeStampFrame)
        timeStampLabel.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        timeStampLabel.textColor = UIColor.lightGray
        timeStampLabel.textAlignment = .center
        timeStampLabel.backgroundColor = UIColor.clear
        
        self.contentView.addSubview(timeStampLabel)
        self.requestTimeStamp = timeStampLabel
    }
    
    func addActionButtonViews() {
        //add chat accept
        //add chat dismiss
        //add new chat symbol
        //add toSwap symbol
        
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = self.frame.height
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add chat accept
        let acceptButtonWidth = 63 as CGFloat
        let aH = 28 as CGFloat
        let aX = sWidth - marginInset - acceptButtonWidth
        let aY = (cellHeight * 0.5) - aH
        
        let aFrame = CGRect(x: aX, y: aY, width: acceptButtonWidth, height: aH)
        let aView = UIButton(frame: aFrame)
        aView.contentMode = .center
        aView.setImage(UIImage(named: "requestAcceptButton")!, for: .normal)
        //        aView.backgroundColor = UIColor.green
        
        self.contentView.addSubview(aView)
        self.chatAcceptButton = aView
        
        //add dismiss button
        let dFrame = CGRect(x: aX, y: aY + aH, width: acceptButtonWidth, height: aH)
        let dView = UIButton(frame: dFrame)
        dView.contentMode = .center
        dView.setImage(UIImage(named: "requestDismissButton")!, for: .normal)
        //        dView.backgroundColor = UIColor.orange
        
        self.contentView.addSubview(dView)
        self.chatDismissButton = dView
        
        //add toChat view
        let toChatWidth = 36 as CGFloat
        let cH = 22 as CGFloat
        let cX = sWidth - marginInset - toChatWidth
        let cY = self.thumbnailView.frame.minY
        
        let cF = CGRect(x: cX, y: cY, width: toChatWidth, height: cH)
        let cView = UIButton(frame: cF)
        cView.contentMode = .center
        cView.setImage(UIImage(named: "newChatSymb")!, for: .normal)
        
        self.contentView.addSubview(cView)
        self.newChatSymb = cView
    }
    
    func addMessageViews() {
        //add main label
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let cellHeight = self.frame.height
        let marginInsetRatio = 9 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        let messageInsetR = 8 / 414 as CGFloat
        let messageInset = messageInsetR * sWidth
        
        //1. add userNameLabel
        let nRatio = 200 / 414 as CGFloat
        let nameLabelWidth = nRatio * sWidth
        let nH = 30 as CGFloat
        let nX = self.thumbnailSuperView!.frame.maxX + messageInset
        let nY = self.thumbnailView.frame.minY
        
        let nFrame = CGRect(x: nX, y: nY, width: nameLabelWidth, height: nH)
        let nLabel = UILabel(frame: nFrame)
        
        //        nLabel.backgroundColor = UIColor.yellow
        nLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        nLabel.textColor = UIColor.black
        nLabel.textAlignment = .left
        
        self.contentView.addSubview(nLabel)
        self.userName = nLabel
        
        //2. add oneway swap symbol
        let oneWaySymWidth = (18 / 414) * UIScreen.main.bounds.width
        let oH = oneWaySymWidth as CGFloat
        let oX = self.thumbnailSuperView!.frame.maxX + messageInset
        let oY = self.thumbnailView.frame.minY
        
        let oF = CGRect(x: oX, y: oY, width: oneWaySymWidth, height: oH)
        let oView = UIImageView(frame: oF)
        oView.contentMode = .scaleAspectFit
        //        oView.backgroundColor = UIColor.yellow
        
        self.contentView.addSubview(oView)
        self.oneWaySwapCompleteMarker = oView
        self.oneWaySwapCompleteMarker.isHidden = true
        
        let centerX = self.oneWaySwapCompleteMarker.frame.maxX - (0.5 * oneWaySymWidth)
        let centerY = self.userName.frame.minY + (self.userName.frame.height * 0.5)
        self.oneWaySwapCompleteMarker.layer.position = CGPoint(x: centerX, y: centerY)
        
        //1. add completeSwapSymbol
        let symbWidth = (27 / 414) * UIScreen.main.bounds.width
        let sH = (30 / 27) * symbWidth
        let sX = self.thumbnailSuperView!.frame.maxX + messageInset
        let sY = self.thumbnailView.frame.minY
        
        let sFrame = CGRect(x: sX, y: sY, width: symbWidth, height: sH)
        let sView = UIImageView(frame: sFrame)
        //        sView.backgroundColor = UIColor.yellow
        sView.contentMode = .center
        
        self.contentView.addSubview(sView)
        self.completeSwapSymbol = sView
        
        let center2X = self.completeSwapSymbol.frame.maxX - (0.5 * symbWidth)
        let center2Y = self.userName.frame.minY + (self.userName.frame.height * 0.5)
        self.completeSwapSymbol.layer.position = CGPoint(x: center2X, y: center2Y)
        
        //3. addLastMessageLabel
        let lW = nameLabelWidth
        let lH = sH
        let lX = nX
        let lY = (cellHeight - lH) * 0.5
        
        let lFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let lLabel = UILabel(frame: lFrame)
        
        //        lLabel.backgroundColor = UIColor.clear
        lLabel.font = UIFont(name: "AvenirNext-Regular", size: 14.0)
        lLabel.textColor = UIColor.black
        lLabel.textAlignment = .left
        
        self.contentView.addSubview(lLabel)
        self.lastMessageLabel = lLabel
    }
    
    //
    var chatAcceptButton: UIButton!  { didSet {
        chatAcceptButton.addTarget(self, action: #selector(ChatListTableViewCell.acceptChatRequestButton(_:)), for: .touchUpInside)
        chatAcceptButton.isUserInteractionEnabled = true
        }
    }
    
    var chatDismissButton: UIButton! { didSet {
        chatDismissButton.addTarget(self, action: #selector(ChatListTableViewCell.dismissChatRequestButton(_:)), for: .touchUpInside)
        chatDismissButton.isUserInteractionEnabled = true
        }
    }

    var newChatSymb: UIButton! { didSet {
        newChatSymb.addTarget(self, action: #selector(ChatListTableViewCell.newChatButton(_:)), for: .touchUpInside)
        newChatSymb.isUserInteractionEnabled = true
        }
    }
}
