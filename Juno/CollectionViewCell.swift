//
//  CollectionViewCell.swift
//  Juno
//
//  Created by Asaad on 2/10/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

//class ImageCell: UICollectionViewCell {
//
//    var imageView: UIImageView!
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        imageView = UIImageView()
//        imageView.contentMode = .scaleAspectFill
//        imageView.isUserInteractionEnabled = false
//        contentView.addSubview(imageView)
//    }
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        var frame = imageView.frame
//        frame.size.height = self.frame.size.height
//        frame.size.width = self.frame.size.width
//        frame.origin.x = 0
//        frame.origin.y = 0
//        imageView.frame = frame
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var profilePhotoThumbnailView: UIImageView!
    @IBOutlet weak var userDisplayNameLabel: UILabel!
    @IBOutlet weak var swapCompletePlaceHolder: UIImageView!
    
    let softSpaceGray = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)

    var swapSymbView: UIImageView!
    var swapSymbView2: UIImageView!
    var presenceSymbol: UIImageView!
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
////        imageView = UIImageView()
////        imageView.contentMode = .scaleAspectFill
////        imageView.isUserInteractionEnabled = false
////        contentView.addSubview(imageView)
//        self.configureCellLayout()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //custom logic goes here
        self.addCellSubviews()
    }
    
    //ATTN: for smaller screen Sizes than in SB, seems as though self is referencing cellSize from SB and from programattic settings
    //updateNeedsLayout will resolve? Make sure you're retrieving the correct cellSize information 
    
    func addCellSubviews() {
        //set screen properties
        let screen = UIScreen.main.bounds
        let screenHeight = screen.height
        let screenWidth = screen.width

        //set inset properties
        let insetRatio = 9/414 as CGFloat
        let insetSpacing = insetRatio * screenWidth

        //set cell properties
        let cellDimension = 182/126 as CGFloat
        let cellWidth = (screenWidth - (4 * insetSpacing)) / 3.00
        let cellHeight = cellWidth * cellDimension
        
        self.frame = CGRect(x: 0, y: 0, width: cellWidth, height: cellHeight)
        //set Grid properties
        let unitsInGrid = 18 as CGFloat
        let unitSide = cellWidth / unitsInGrid

        //set Frames properties
        let statusHeight = (9 / 414) * screenWidth as CGFloat
        let statusWidth = statusHeight
        let boundingRectangle = CGRect(x: 0, y: 0, width: statusWidth, height: statusHeight)
        let statusImageView = UIImageView(frame: boundingRectangle)

        //1. Add presenceSymb
        let offset = unitSide
        let xPos = bounds.maxX - statusImageView.bounds.width - offset
        let yPos = self.bounds.maxY - statusImageView.bounds.height - offset

        statusImageView.frame.origin.x = xPos
        statusImageView.frame.origin.y = yPos

        statusImageView.layer.shadowOpacity = 0.3
        statusImageView.layer.shadowOffset = CGSize(width: 0, height: 0)
        statusImageView.layer.shadowRadius = 0.8
//        statusImageView.backgroundColor = UIColor.red
        
        contentView.addSubview(statusImageView)
        self.presenceSymbol = statusImageView

        //2. Add 2 Way swap indicator
        let swRatio = 22 / 414 as CGFloat
        let shRatio = 13.6 / 22 as CGFloat

        let swapSymbWidth = swRatio * screenWidth
        let swapSymbHeight = swapSymbWidth * shRatio
        let swapSymbX = self.bounds.maxX - unitSide - swapSymbWidth
        let swapSymbY = unitSide

        let swapSymbFrame = CGRect(x: swapSymbX, y: swapSymbY, width: swapSymbWidth, height: swapSymbHeight)
        let swapSymbView = UIImageView(frame: swapSymbFrame)
        swapSymbView.image = UIImage(named: "2WaySwapGridSymbol_N")!

        swapSymbView.layer.shadowOpacity = 0.4
        swapSymbView.layer.shadowOffset = CGSize(width: 0, height: 0)
        swapSymbView.layer.shadowRadius = 0.8

        contentView.addSubview(swapSymbView)
        //.isHidden property will change to false when 2Way swaps are detected
        self.swapSymbView = swapSymbView
        self.swapSymbView.isHidden = true

        //3. Add 1 Way swap Indicator
        let swRatio2 = 13.7 / 414 as CGFloat

        let swapSymbWidth2 = swRatio2 * screenWidth
        let swapSymbHeight2 = swapSymbWidth2
        let swapSymbX2 = self.bounds.maxX - unitSide - swapSymbWidth2
        let swapSymbY2 = unitSide

        let swapSymbFrame2 = CGRect(x: swapSymbX2, y: swapSymbY2, width: swapSymbWidth2, height: swapSymbHeight2)
        let swapSymbView2 = UIImageView(frame: swapSymbFrame2)
        swapSymbView2.image = UIImage(named: "1WaySwapGridSymb_Outgoing_N")!

        swapSymbView2.layer.shadowOpacity = 0.4
        swapSymbView2.layer.shadowOffset = CGSize(width: 0, height: 0)
        swapSymbView2.layer.shadowRadius = 0.8

        contentView.addSubview(swapSymbView2)

        //.isHidden property will change to false when 2Way swaps are detected
        self.swapSymbView2 = swapSymbView2
        self.swapSymbView2.isHidden = true
        
        //
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.masksToBounds = true
        self.backgroundColor = self.softSpaceGray

        //add shadow
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0) //shadowOffset controls the "cast" of the shadow
        self.layer.shadowRadius = 3.0 //radius increases "feather" of shadow
        self.layer.shadowOpacity = 0.4 //
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
    }
}


