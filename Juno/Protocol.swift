//
//  Protocol.swift
//  Juno
//
//  Created by Asaad on 3/11/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit

protocol chatListCellDelegate: class {
    func acceptChatRequest(_ sender: ChatListTableViewCell)
    func rejectChatRequest(_ sender: ChatListTableViewCell)
    func toNewChatButton(_ sender: ChatListTableViewCell)
}
