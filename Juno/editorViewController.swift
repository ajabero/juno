//
//  EditorViewController.swift
//  Juno
//
//  Created by Asaad on 3/10/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import UIKit
import Vision
import Firebase
import FirebaseAuth
import CoreImage

class EditorViewController: UIViewController, UIGestureRecognizerDelegate, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    //need:
//    1. height as feet & inch
//    2. height as meters & cm
//    3. fix additional line view displacement
//    4. all textfield character limits
//    5. age number pad / year day month? & edit limit
//    6. about me character count
//    7. highlight lines on and off selection
//    8. adjust textView height when user backspaces lines

    var ref: DatabaseReference! // self.ref = Database.database().reference()

    var storageRef: StorageReference!
    
    var myProfileInfoDict = [String:String]()
    
//    self.myProfileInfoDict["selfUID"] = snapshot.key
//    self.myProfileInfoDict["privacy"] = userPrivacyPreference
//    self.myProfileInfoDict["selfDisplayName"] = userDisplayName
//    self.myProfileInfoDict["selfVisus"] = visus
//    self.myProfileInfoDict["selfDefaultPhotoURL"] = userThumbnailURLString
//    self.myProfileInfoDict["userAge"] = userAge
    
    //1. If error occurs with writing, respond to error by blocking UI From performing segue. DO LATER. BUT DO DO,
    //2. When user does change photo, send this photo to the homescreen manually through unwind segue. UPDATE FROM SELF PROFILE INFO DICT when user segues from GridVC. DO IT NOW.
    
    override func loadView() {
        
        //screen properties
        let screen = UIScreen.main.bounds
        let screenWidth = screen.width
        let screenHeight = screen.height
        
        //initialize view
        let myInputView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        myInputView.backgroundColor = UIColor.white
        self.view = myInputView
        
        //set UILayout properties
        
        //set final scrollView height
//        let finalScrollViewHeight = self.getFinalScrollViewHeight()
        let finalScrollViewHeight = 2500 as CGFloat
        
        //initialize scrollView
        let scrollView = UIScrollView(frame: screen)
        scrollView.contentSize = CGSize(width: screenWidth, height: finalScrollViewHeight)
        scrollView.showsVerticalScrollIndicator = false
        
        //add scrollView as Subview
        self.view.addSubview(scrollView)
        self.scrollView = scrollView
        self.scrollView!.delegate = self
        
        //add subviews
        self.addNavSpaceLayer()
        self.addLayerMask()
        self.addScreenTitle()
        self.addMainImageView()
        self.addNoFaceImageView()
        self.addProfileToggler()
        self.addVisibilityLabel()
        self.addDidNotDetectFaceTextView()
        self.addPulsatingLayer()

//        self.addProfileImageView_1()
//        self.addProfileImageView_2()
//        self.addProfileImageView_3()
        
        //add Info Cells
        self.addMyInfoSectionTitle()
        self.addAgeTitle()
        self.addSchoolCell()
        self.addAboutMeSection()
        self.addAdditionalInfoLabel()
        self.addHeightCell()
        self.addHereForX5()
    
        self.addNavBarButtons()
        self.addHeightPicker()
        
        self.addActivityIndicator()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("EDITORVC VIEWDIDLOAD")
        //        self.heightPicker?.isHidden = true
        //        self.heightPicker?.resignFirstResponder()
        //Configure DB and Storage
        self.ref = Database.database().reference()
        storageRef = Storage.storage().reference()
        
        self.selfUID = Auth.auth().currentUser!.uid

        self.heightPicker?.isHidden = true
        self.barView?.isHidden = true
        
        if #available(iOS 11.0, *) {
            self.scrollView!.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            self.view.insetsLayoutMarginsFromSafeArea = false
        } else {
            // Fallback on earlier versions
        }
        
        self.noFaceImageView?.isHidden = true
        self.imageErrorTextView?.isHidden = true
        self.pulsatingLayer?.isHidden = true
        
        //set information
        self.populateAllFields()
        
        //configure height picker to current value
        
        if self.myProfileInfoDict["height"] == "" {
            self.heightPicker?.selectRow(0, inComponent: 0, animated: false)
        } else {
            let currentValue = self.myProfileInfoDict["height"]!
            let iSel = self.heightArray.index(of: currentValue)
            print("heightArray count is", self.heightArray.count)
            print("iSel", iSel)
            self.heightPicker?.selectRow(iSel!, inComponent: 0, animated: false)
        }
        
        self.activityIndicatorView?.isHidden = true
        self.getSelfProfileInfo()
        
        self.observePendingSwapRequests()
        self.observeOutgoingSwapRequests()
        
        //INIT profileImageisVisible from DB && current Blurstate setting
        if self.myProfileInfoDict["selfVisus"]! == "true" {
            self.profileImageisVisible = true
        } else if self.myProfileInfoDict["selfVisus"]! == "false" {
            self.profileImageisVisible = false
            self.blurState = self.myProfileInfoDict["blurState"]!
        }
        
//        if let returnedImage = self.returnedImage {
//            self.mainImageView!.image = returnedImage
//        }
        
        //load user's visible image to memory
        if self.myProfileInfoDict["selfVisus"]! == "false" {
            let clearImage = self.loadClearPhotoFromDisk_Self()
            self.originalClearImage = clearImage
        }
        
        self.addAlert_makePrivate()
        self.addAlert_makePublic()
        
    }
    
    var returnedImage: UIImage?
    
    var originalClearImage: UIImage? { didSet {print("DIDSET originalClearImage") }}
    
    override func viewWillAppear(_ animated: Bool) {
//        if let returnedImage = self.returnedImage {
//            self.mainImageView!.image = returnedImage
//        }
//
//        //load user's visible image to memory
//        if self.myProfileInfoDict["selfVisus"]! == "false" {
//            let clearImage = self.loadClearPhotoFromDisk_Self()
//            self.originalClearImage = clearImage
//        }
        
        if let makePrivateAlertSeen = UserDefaults.standard.string(forKey: "makePrivateAlert_Seen") {
            print("makePrivateAlertSeen from vault is,", makePrivateAlertSeen)
            self.makePrivateAlert_Seen = makePrivateAlertSeen
        }
        
//        self.makePrivateAlert_Seen = "false"
        
        
    }
    
//    func loadClearPhotoFromDisk_Self() -> UIImage? {
//        print("getClearPhoto accesing photo vault")
//        let fileName = "\(selfUID!).jpeg"
//        let fileURL = documentsUrl.appendingPathComponent(fileName)
//        do {
//            let imageData = try Data(contentsOf: fileURL)
//            let uiImage = UIImage(data: imageData)
//            return uiImage
//        } catch {
//            print("Error loading image : \(error)")
//        }
//        return nil
//    }
//
//    func getDocumentsDirectory() -> URL {
//        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        return paths[0]
//    }
    
    var activityIndicatorView: UIActivityIndicatorView?
    
    var pendingSwapsToUID = [String]()
    var pendingSwapsToUID_RequestID = [String]()
    
    var outgoingSwapRequest_SwapIDArray = [String]()
    var outgoingSwapRequest_UIDArray = [String]()
    
    func observeOutgoingSwapRequests() {
        let selfUID = self.selfUID!
        
        self.ref.child("users/\(selfUID)/outgoingSwapRequests").observe(.childAdded) { (snapshot) in
            print("R Editor observeOutgoingSwapRequests snapshot", snapshot)
            print("R Editor observeOutgoingSwapRequests snapshot value", snapshot.value)
            
            guard let requestBody = snapshot.value as? [String:String] else { return }
            print("R requestBody,", requestBody)
            
            guard let swapID = requestBody["swapID"] else { return }
            print("R1,swapID",swapID )
            
            guard let swapInitiatorID = requestBody["swapInitiatorID"] else { return }
            print("R2,swapInitiatorID",swapInitiatorID )
            
            guard let swapRecipientID = requestBody["swapRecipientID"] else { return }
            print("R3,swapRecipientID",swapRecipientID )
            
            guard let swapRequestStatus = requestBody["swapRequestStatus"] else { return }
            print("R4,swapRequestStatus",swapRequestStatus )

            if swapRequestStatus == "false" {
                self.outgoingSwapRequest_SwapIDArray.append(swapID)
                self.outgoingSwapRequest_UIDArray.append(swapRecipientID)
            }
        }
    }
    
    func deleteOutgoingSwapRequestsOnReveal() {
        
        let selfUID = self.selfUID!
        
        for swapRequestID in self.outgoingSwapRequest_SwapIDArray {
            
            let index = self.outgoingSwapRequest_SwapIDArray.index(of: swapRequestID)
            let recipientID = self.outgoingSwapRequest_UIDArray[index!]
            
            //1
            self.ref.child("globalSwapRequests/\(selfUID)/\(swapRequestID)").removeValue()
            self.ref.child("globalSwapRequests/\(recipientID)/\(swapRequestID)").removeValue()
            
            //2
            self.ref.child("users/\(selfUID)/outgoingSwapRequests/\(swapRequestID)").removeValue()
            
            //3
            self.ref.child("users/\(recipientID)/pendingSwapRequests/\(swapRequestID)").removeValue()
        }
    }
    
    func observePendingSwapRequests() {
        let selfUID = self.selfUID!
        
        self.ref.child("users/\(selfUID)/pendingSwapRequests").observe(.childAdded) { (snapshot) in
            print("Editor observeAllSwapRequests snapshot", snapshot)
            print("Editor observeAllSwapRequests snapshot value", snapshot.value)
            
//            let requestID = snapshot.key
//            print("requestID key is,", requestID)
//
//            let userID = snapshot.value
            guard let requestBody = snapshot.value as? [String:String] else { return }
            print("requestBody,", requestBody)
            
            guard let swapID = requestBody["swapID"] as? String else { return }
            print("E1,swapID",swapID )
            
            guard let swapInitiatorID = requestBody["swapInitiatorID"] as? String else { return }
            print("E2,swapInitiatorID",swapInitiatorID )

            guard let swapRecipientID = requestBody["swapRecipientID"] as? String else { return }
            print("E3,swapRecipientID",swapRecipientID )

            if swapInitiatorID == selfUID {
                self.pendingSwapsToUID.append(swapRecipientID)
                self.pendingSwapsToUID_RequestID.append(swapID)
            } else {
                self.pendingSwapsToUID.append(swapInitiatorID)
                self.pendingSwapsToUID_RequestID.append(swapID)
            }
        }
    }
    
    var unlockedUsers = [String]()
    
    func revealAll() {
        
        for userID in self.pendingSwapsToUID {
            if !self.unlockedUsers.contains(userID) {
                print("user did not unlock, need to reveal")
                
                let userID = userID
                let index = self.pendingSwapsToUID.index(of: userID)
                if let i = index {
                    let swapID = self.pendingSwapsToUID_RequestID[i]
                    if let imageToReveal = self.originalClearImage {
                        self.scarletMessenger(swapStatus: "true", withUserID: userID, withSwapID: swapID, withImageToReveal: imageToReveal, selfInitiatedSwapRequest: false)
                    }
                }
            } else {
                
            }
        }
    }
    
    func scarletMessenger(swapStatus: String?, withUserID: String, withSwapID: String, withImageToReveal: UIImage, selfInitiatedSwapRequest: Bool) {
        
        let selfUID = self.selfUID!
        
        guard let imageData = UIImageJPEGRepresentation(withImageToReveal, 0.4) else { return } //not returning photo
        print("scarletMessenger imageData successfully loaded PASS")
        
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        storageRef!.child(imageStoragePath).putData(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                print("Error uploading: \(error)")
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        
                        let visibleURL = metadata?.downloadURLs?[0].absoluteString
                        
                        //if initiator != self, write visible image @swapRecipient x 2, else, write visible image @swapInitiatorx2
                        var imageStorageNode = ""
                        if selfInitiatedSwapRequest == true {
                            imageStorageNode = "swapInitiatorPhotoURL"
                        } else {
                            imageStorageNode = "swapRecipientPhotoURL"
                        }
                        
                        //write URL x2
                        self.ref.child("globalSwapRequests/\(selfUID)/\(withSwapID)/\(imageStorageNode)").setValue(visibleURL)
                        self.ref.child("globalSwapRequests/\(withUserID)/\(withSwapID)/\(imageStorageNode)").setValue(visibleURL)
                        
                        //write status x2
                        guard let swapStatus = swapStatus else { return }
                        self.ref.child("globalSwapRequests/\(selfUID)/\(withSwapID)/swapRequestStatus").setValue(swapStatus)
                        self.ref.child("globalSwapRequests/\(withUserID)/\(withSwapID)/swapRequestStatus").setValue(swapStatus)
                    }
                }
            }
        }
    }
    
    func addActivityIndicator() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        let widthRatio = 26 / 414 as CGFloat
        let width = widthRatio * sWidth
        
        let aW = width
        let aH = aW
        let aX = (self.mainImageView!.bounds.width - aW) * 0.5
        let aY = aX
        
        let aF = CGRect(x: aX, y: aY, width: aW, height: aH)
        let activityIndicatorView = UIActivityIndicatorView(frame: aF)
        activityIndicatorView.color = UIColor.white
        activityIndicatorView.hidesWhenStopped = true
        
        self.mainImageView!.addSubview(activityIndicatorView)
        self.activityIndicatorView = activityIndicatorView
    }
    
    
    //ATTENTION: MAKE SURE THIS FUNCTION IS PROPERLY UPDATING THE SELFINO DICTIONARY AS NEEDED BY THE PROGRAM. CURRENTLY NOT ACTIVELY CALLED>
    func getSelfProfileInfo() {
        print("getSelfProfileInfo")
        let user = Auth.auth().currentUser!
        let selfUID = user.uid
        
        self.ref.child("users/\(selfUID)").observeSingleEvent(of: .value , with: { (snapshot: DataSnapshot) in
            guard let value = snapshot.value as? [String: Any] else { return }
            
            print("EDITOR VC FIRING OBSERVER selfINFO value", value)
            
            //            guard let userDateOfBirth = value["dateOfBirth"] as? String else { return }
            guard let userAge = value["userAge"] as? String else { return }
            guard let userDisplayName = value["userDisplayName"] as? String else { return }
            guard let userThumbnailURLString = value["full_URL"] as? String else { return }
            guard let userPrivacyPreference = value["privacy"] as? String else { return }
            guard let visus = value["visus"] as? String else { return }
            guard let userPhotoStorageRef = value["userDisplayPhoto"] as? String else { return }
            
            guard let school = value["School"] as? String else { return }
            guard let work = value["Work"] as? String else { return }
            guard let aboutMe = value["AboutMe"] as? String else { return }
            guard let height = value["Height"] as? String else { return }
            
            guard let blurState = value["blurState"] as? String else { return }
            guard let lookingFor = value["LookingFor"] as? String else { return }
            
            
            //guard let userAge = value["userAge"] as? String else { return }
            
            self.myProfileInfoDict["selfUID"] = snapshot.key
            self.myProfileInfoDict["privacy"] = userPrivacyPreference
            self.myProfileInfoDict["selfDisplayName"] = userDisplayName
            self.myProfileInfoDict["selfVisus"] = visus
            self.myProfileInfoDict["selfDefaultPhotoURL"] = userThumbnailURLString
            self.myProfileInfoDict["userAge"] = userAge
            self.myProfileInfoDict["blurState"] = blurState
            self.myProfileInfoDict["lookingFor"] = lookingFor
            self.myProfileInfoDict["userPhotoStorageRef"] = userPhotoStorageRef
            
            self.myProfileInfoDict["school"] = school
            self.myProfileInfoDict["work"] = work
            self.myProfileInfoDict["aboutMe"] = aboutMe
            self.myProfileInfoDict["height"] = height
            
//            self.selfVisus = visus
            
            print("DIDLOAD SELFINFO self.myporifleInfo dict now contains,", self.myProfileInfoDict)
        })
    }
    
    
//    var heightArray = ["Do Not Show", "4'0\"", "4'1\"", "4'2\"", "4'3\"", "4'4\"", "4'5\"", "4'6\"", "4'7\"", "4'8\"", "4'9\"", "4'10\"", "4'11\"", "5'0\"", "5'1\"", "5'2\"", "5'3\"", "5'4\"", "5'5\"", "5'6\"", "5'7\"", "5'8\"", "5'9\"", "5'10\"", "5'11\"", "6'0\"", "6'1\"", "6'2\"", "6'3\"", "6'4\"", "6'5\"", "6'6\"", "6'7\"", "6'8\"", "6'9\"", "6'10\"", "6'11\"", "7'0\"", "7'1\"", "7'2\"", "7'3\"", "7'4\"", "7'5\"", "7'6\"", "7'7\"", "7'8\"", "7'9\"", "7'10\"", "7'11\""]
//
    func populateAllFields() {
        self.mainImageView!.sd_setImage(with: URL(string: myProfileInfoDict["selfDefaultPhotoURL"]!))
        
        //age
        if let age = self.myProfileInfoDict["userAge"] {
            self.ageTextField?.placeholder = age
            self.ageCellLabel?.textColor = UIColor.lightGray
            self.ageTextField?.isUserInteractionEnabled = false
        }
        
        //school
        if let school = self.myProfileInfoDict["school"] {
            self.schoolTextField?.text = school
        }
        
        //about me
        if let aboutMe = self.myProfileInfoDict["aboutMe"] {
            self.aboutMeTextView?.text = aboutMe
        }
        
        //height
        if let height = self.myProfileInfoDict["height"] {
            if height == "Do Not Show" {
                self.heightTextField?.text = ""
            } else {
                self.heightTextField?.text = height
            }
        }
        
        //here for
        if let hereForString = self.myProfileInfoDict["lookingFor"] {
            print("DB hereForString is", hereForString)
            let hereForArray = hereForString.components(separatedBy: ", ")
            print("hereForArray is", hereForArray)
            
            for i in hereForArray {
                self.lookingForArray.append(i)
                print("i is", i)
//                if hereForArray.contains(i) {
                    if i == "Chats" {
                        self.chatsSelected = true
                        self.chatsLabel?.textColor = self.selectionColor
                    } else if i == "Dates" {
                        self.datesSelected = true
                        self.datesLabel?.textColor = self.selectionColor
                        
                    } else if i == "Networking" {
                        self.networkingSelected = true
                        self.networkingLabel?.textColor = self.selectionColor
                        
                    } else if i == "Friends" {
                        self.friendsSelected = true
                        self.friendsLabel?.textColor = self.selectionColor
                        
                    } else if i == "Relationship" {
                        self.relationshipSelected = true
                        self.relationshipLabel?.textColor = self.selectionColor
                    }
            }
        }
        
        if myProfileInfoDict["selfVisus"]! == "true" {
            //initialize required photo properties
            let clearUIImage = (self.mainImageView!.image)!
            let unblurredCIImage = CIImage(image: clearUIImage)!
            self.unblurredCIImage = unblurredCIImage
            self.clearProfilePhoto = clearUIImage
                
            self.createBlurredImages(withClearCIImage: unblurredCIImage)
            self.currentProfileSetting = "Public Profile"
//            self.animateTogglerView(to: "Right")
            self.profileImageisVisible = true

        } else if myProfileInfoDict["selfVisus"]! == "false" {
            print("setting photo state selfVisus false")
            self.profileImageisVisible = false
            self.currentProfileSetting = "Private Profile"
            self.blurState = myProfileInfoDict["blurState"]!
            self.animateTogglerView(to: "Left")
            self.loadClearPhotoFromDisk_Self()
        }
    }
    
    var documentsUrl: URL { return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! }
    
    func loadClearPhotoFromDisk_Self() -> UIImage? {
        print("loadClearPhotoFromDisk_Self")
        print("getClearPhoto accesing photo vault")
        let fileName = "\(self.selfUID!).jpeg"
        print("fileName is, fileName", fileName)
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            
            let imageData = try Data(contentsOf: fileURL)
            let uiImage = UIImage(data: imageData)
            
            //1. set clear photo
            self.clearProfilePhoto = uiImage
            
            //2. set blurred image
            let ciImageObject = CIImage(image: uiImage!)
            self.unblurredCIImage = ciImageObject
            
            //3. create blurred images
            self.createBlurredImages(withClearCIImage: ciImageObject!)
            
            self.originalClearImage = uiImage
            return uiImage
        } catch {
            print("Error loading image : \(error)")
        }
        return nil
    }
    
    func createBlurredImages(withClearCIImage: CIImage) {
        self.blurredImageLight = withClearCIImage.applyingGaussianBlur(sigma: lightBlurSigma)
        self.blurredImageMedium = withClearCIImage.applyingGaussianBlur(sigma: mediumBlurSigma)
        self.blurredImageHeavy = withClearCIImage.applyingGaussianBlur(sigma: heavyBlurSigma)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    //MARK: Configure Scroll View Properties
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //disable bounce only at the top of the screen
//        self.scrollView!.bounces = scrollView.contentOffset.y > 100
    }
    
    //MARK: Draw-View Methods
    //View
    var scrollView: UIScrollView?
    var mainImageView: UIImageView?
    var noFaceImageView: UIImageView?
    var profilePhoto1: UIImageView?
    var profilePhoto2: UIImageView?
    var profilePhoto3: UIImageView?
    var profileTogglerView: UIImageView? { didSet { print("didset togglerview")}}
    
    var layerMaskView: UIImageView?
    var titleLabel: UILabel?

    var navEdge: UIView?
    var navSpaceLayer: UIView?
    
    var myInfoSectionTitle: UITextView?
    
    //age
    var ageCellLabel: UITextView?
    var ageLineView: UIView?
    var ageTextField: UITextField?
    
    //school
    var schoolCellLabel: UITextView?
    var schoolLineView: UIView?
    var schoolTextField: UITextField?
    
    //About Me
    var aboutMeCellLabel: UITextView?
    var aboutMeLineView: UIView?
    var aboutMeTextView: UITextView?
    
    //height
    var heightCellLabel: UITextView?
    var heightLineView: UIView?
    var heightTextField: UILabel?
    //    var heightTextField: UITextField?
    
    var insetFromLine: CGFloat?
    var additionalInfoLabel: UITextView?
    
    //About Me
    var maxTextViewHeight: CGFloat?
    var doneEditingButton: UIButton?
    var doneButtonView: UIImageView?

    let notification = UISelectionFeedbackGenerator()
    
    func setNotificationAlert() {
        self.notification.selectionChanged()
    }

    //Colors
    var navBarColor = UIColor(displayP3Red: 246/255, green: 246/255, blue: 248/255, alpha: 1)
    var hyperBlue = UIColor(displayP3Red: 0, green: 108/255, blue: 255/255, alpha: 1)
    
    func addNavSpaceLayer() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
    
        //
        let navX = 0 as CGFloat
        let navY = 0 as CGFloat
        let navW = sWidth
        let navH = navSpaceHeight
        
        let navF = CGRect(x: navX, y: navY, width: navW, height: navH)
        let navView = UIView(frame: navF)
        navView.backgroundColor = self.navBarColor
        self.navSpaceLayer = navView
        
        self.view!.addSubview(navView)
        
        //& Line View
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = navSpaceHeight
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.view!.addSubview(lineView)
        self.navEdge = lineView
    }
    
    func addLayerMask() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let vW = sWidth
//        let vH = ((5 * superInsetHeight) + (3 * gridUnitHeight)) - self.navSpaceLayer!.bounds.height
        let vH = (3 * superInsetHeight) + (2 * gridUnitHeight)
        let vX = 0 as CGFloat
        let vY = self.navEdge!.frame.maxY
        
        let f = CGRect(x: vX, y: vY, width: vW, height: vH)
        let layerMaskView = UIImageView(frame: f)
        
        //
        let tint = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
        layerMaskView.backgroundColor = tint
        
        //
        self.scrollView!.addSubview(layerMaskView)
        self.layerMaskView = layerMaskView
        
        let maxString = "Hey! My name is Asaad. I live in Cambridge Massachussetts. I’m originally from Beirut, Lebanon. I spend most of my summers in Romania. I studied at Babson College here in Boston. Where I did a Masters in Management. Hey! My name is Asaad. I live in Cambridge Massachussetts. I’m originally from Beirut, Lebanon. I spend most of my summers in Romania. I studied at Babson College here in Boston. Where I did a Masters in Management. I studied at Babson College here in Boston. Where I did a Masters …"
        
        // Font Size
        var tFontSize = cellFontSize_Large as CGFloat
        let tW = sWidth - 18
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
        }
        
        let maxTextFieldCGSize = self.sizeOfCopy(string: maxString, constrainedToWidth: Double(tW), fontName: "AvenirNext-Regular", fontSize: tFontSize)
        let maxH = ceil(maxTextFieldCGSize.height)
        self.scrollView?.contentSize = CGSize(width: sWidth, height: ((vH + self.navSpaceLayer!.bounds.height) * 3) + (maxH * 0.8))
    }
    
    let cellFontSize_Large = 14 as CGFloat
    let cellFontSize_Small = 14 as CGFloat

//    let cellFontSize_Large = 14 as CGFloat
//    let cellFontSize_Small = 12 as CGFloat
 
//    let cellFontSize_Large = 16 as CGFloat
//    let cellFontSize_Small = 14 as CGFloat
    
    
    func addMyInfoSectionTitle() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        // Font Size
        var tFontSize = 20.0 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 18.0 as CGFloat
        }
        print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        let nW = 3 * gridUnitWidth
        var nH = 0 as CGFloat
        let titleString = "My Info"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
            let tH = ceil(textFieldCGSize.height)
            nH = tH
        
        let nX = marginInset
        let nY = self.layerMaskView!.frame.maxY + marginInset
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let myInfoSectionTitle = UITextView(frame: f)
        
        //
        myInfoSectionTitle.text = titleString
        self.scrollView!.addSubview(myInfoSectionTitle)
        self.myInfoSectionTitle = myInfoSectionTitle
        
        self.myInfoSectionTitle!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.myInfoSectionTitle!.textAlignment = .left
        self.myInfoSectionTitle!.isEditable = false
        self.myInfoSectionTitle!.isScrollEnabled = false
        self.myInfoSectionTitle!.isSelectable = false
        self.myInfoSectionTitle!.textContainer.lineFragmentPadding = 0
        self.myInfoSectionTitle!.textContainerInset = .zero
        self.myInfoSectionTitle!.textColor = UIColor.black
        self.myInfoSectionTitle!.backgroundColor = UIColor.clear
    }
    
    func addAboutMeSection() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add Title
        var aFontSize = cellFontSize_Large as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            aFontSize = cellFontSize_Small as CGFloat
        }
        
        //label width
        var aW = 2 * gridUnitWidth
        var aH = 0 as CGFloat
        let titleString = "About Me"
            let labelSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(aW), fontName: "AvenirNext-DemiBold", fontSize: aFontSize)
            let h = ceil(labelSize.height)
            aH = h
        
        let aX = marginInset
        let insetFromLine = (3 * gridUnitHeight - aH) * 0.5
        let aY = self.schoolLineView!.frame.maxY + insetFromLine
        self.insetFromLine = insetFromLine
        //
        let af = CGRect(x: aX, y: aY, width: aW, height: aH)
        let aboutMeLabel = UITextView(frame: af)
        
        //
        aboutMeLabel.text = titleString
        self.scrollView!.addSubview(aboutMeLabel)
        self.aboutMeCellLabel = aboutMeLabel
        
        //
        self.aboutMeCellLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: aFontSize)!
        self.aboutMeCellLabel!.textAlignment = .left
        self.aboutMeCellLabel!.isEditable = false
        self.aboutMeCellLabel!.isScrollEnabled = false
        self.aboutMeCellLabel!.isSelectable = false
        self.aboutMeCellLabel!.textContainer.lineFragmentPadding = 0
        self.aboutMeCellLabel!.textContainerInset = .zero
        self.aboutMeCellLabel!.textColor = UIColor.black
        self.aboutMeCellLabel!.backgroundColor = UIColor.clear

        //add textField space
        let maxString = "Hey! My name is Asaad. I live in Cambridge Massachussetts. I’m originally from Beirut, Lebanon. I spend most of my summers in Romania. I studied at Babson College here in Boston. Where I did a Masters in Management. Hey! My name is Asaad. I live in Cambridge Massachussetts. I’m originally from Beirut, Lebanon. I spend most of my summers in Romania. I studied at Babson College here in Boston. Where I did a Masters in Management. I studied at Babson College here in Boston. Where I did a Masters …"
        
        // Font Size
        var tFontSize = cellFontSize_Large as CGFloat
        let tW = sWidth - 2 * marginInset
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
        }
        
        let maxTextFieldCGSize = self.sizeOfCopy(string: maxString, constrainedToWidth: Double(tW), fontName: "AvenirNext-Regular", fontSize: tFontSize)
        let maxH = ceil(maxTextFieldCGSize.height)
        self.maxTextViewHeight = maxH
        
        var tH = 0 as CGFloat
        let aboutMeString = self.myProfileInfoDict["aboutMe"]!
        if aboutMeString != "" {
            let textFieldCGSize = self.sizeOfCopy(string: aboutMeString, constrainedToWidth: Double(tW), fontName: "AvenirNext-Regular", fontSize: tFontSize)
            let newHeight = ceil(textFieldCGSize.height)
            tH = newHeight
        } else {
            tH = 2 * aH
        }
        
        let tX = marginInset
        let tY = self.aboutMeCellLabel!.frame.maxY + (insetFromLine)

        //
        let tFrame = CGRect(x: tX, y: tY, width: tW, height: tH)
        let tBox = UITextView(frame: tFrame)
        
        self.scrollView?.addSubview(tBox)
        self.aboutMeTextView = tBox
        self.aboutMeTextView?.delegate = self
        self.aboutMeTextView!.backgroundColor = UIColor.clear
        self.aboutMeTextView!.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)!
        self.aboutMeTextView!.textAlignment = .left
        self.aboutMeTextView!.isEditable = true
        self.aboutMeTextView!.isScrollEnabled = false
        self.aboutMeTextView!.isSelectable = true
        self.aboutMeTextView!.textContainer.lineFragmentPadding = 0
        self.aboutMeTextView!.textContainerInset = .zero
        self.aboutMeTextView!.textColor = UIColor.black
        self.aboutMeTextView!.backgroundColor = UIColor.clear
        self.aboutMeTextView!.spellCheckingType = .no
    
        //add Line Border
        let lineW = sWidth
        let lineH = 0.35 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.aboutMeTextView!.frame.maxY + insetFromLine
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.scrollView!.addSubview(lineView)
        self.aboutMeLineView = lineView
    }
    
    //MARK: Configure About Me TextView Properties
    var didSetMaxHeight = false
    var previousRect = CGRect.zero
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        print("textView shouldChangeTextIn")
        
        if (text == "\n") && self.aboutMeTextView!.text == "" {
            print("detected \n")
            textView.resignFirstResponder()
        } else if (self.aboutMeTextView?.bounds.height)! >= self.maxTextViewHeight! && self.didSetMaxHeight == false  {
            textView.resignFirstResponder()
            self.didSetMaxHeight = true
        } else {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            let numberOfChars = newText.characters.count // for Swift use count(newText)
            
            return numberOfChars < 500;
        }
        
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        print("textViewDidBeginEditing")
        self.animateHeightPicker(present: false)
    }
    
    //NOTE: SHOULD ALSO ADJUST TEXTVIEW TO CHANGE HEIGHT WHEN USER BACKSPACES!
    func textViewDidChange(_ textView: UITextView) {
        print("textView textViewDidChange")
        let pos = textView.endOfDocument
        let currentRect = textView.caretRect(for: pos)
        self.previousRect = self.previousRect.origin.y == 0.0 ? currentRect : previousRect
        
        if(currentRect.origin.y > previousRect.origin.y) {
            
            print("Linebreak detected")
            
            //
            let gridUnitX = 45.5 / 414 as CGFloat
            let gridUnitY = 22.5 / 736 as CGFloat
            
            //
            let screen = UIScreen.main.bounds
            let sWidth = screen.width
            let sHeight = screen.height
            let sBarH = UIApplication.shared.statusBarFrame.size.height
            
            //
            let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
            let gridUnitWidth = gridUnitX * sWidth
            let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
            let superInsetHeight = 4 * gridUnitHeight
            
            //
            let marginInsetRatio = 30 / 414 as CGFloat
            let marginInset = marginInsetRatio * sWidth
            
            // Font Size
            var tFontSize = cellFontSize_Large as CGFloat
            var tW = sWidth - 2 * marginInset
            
            if sWidth > 350 as CGFloat {
                //
            } else {
                tFontSize = cellFontSize_Small as CGFloat
            }
            
            let string = "Hello!"
            let textFieldCGSize = self.sizeOfCopy(string: string, constrainedToWidth: Double(tW), fontName: "AvenirNext-Regular", fontSize: tFontSize)
            let newSpace = ceil(textFieldCGSize.height)
            let newString = self.aboutMeTextView!.text!
            let newTextFieldCGSize = self.sizeOfCopy(string: newString, constrainedToWidth: Double(tW), fontName: "AvenirNext-Regular", fontSize: tFontSize)
            let newHeight = ceil(newTextFieldCGSize.height) + newSpace
            print("new hiehgt is", newHeight)
            
            //textView lineview displacement
            //add Line Border
            let lineW = sWidth
            let lineH = 0.5 as CGFloat
            let lineX = 0 as CGFloat
            print("insetFromLine is", insetFromLine)
            let lineY = self.aboutMeLineView!.frame.maxY + newSpace
            let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
            
            //additional info frame displacement
            let adX = marginInset
            let adY = self.additionalInfoLabel!.frame.minY + newSpace
            let adW = self.additionalInfoLabel!.bounds.width
            let adH = self.additionalInfoLabel!.bounds.height
            let aFrame = CGRect(x: adX, y: adY, width: adW, height: adH)
            
            //height cell info displacement
            let hX = marginInset
            let hY = self.heightCellLabel!.frame.minY + newSpace
            let hW = self.heightCellLabel!.bounds.width
            let hH = self.heightCellLabel!.bounds.height
            let hFrame = CGRect(x: hX, y: hY, width: hW, height: hH)
            
            //height lineview displacement
            let lX = 0 as CGFloat
            let lY = self.heightLineView!.frame.maxY + newSpace
            let lW = sWidth
            let lH = 0.5 as CGFloat
            let lframe = CGRect(x: lX, y: lY, width: lW, height: lH)
            
            //text field displacement
            let zX = self.heightTextField!.frame.minX
            let zY = self.heightTextField!.frame.minY + newSpace
            let zW = self.heightTextField!.frame.width
            let zH = self.heightTextField!.frame.height
            let zF = CGRect(x: zX, y: zY, width: zW, height: zH)
            
            //here for title displacement
            let uX = marginInset
            let uY = self.hereForTitle!.frame.minY + newSpace
            let uW = self.hereForTitle!.frame.width
            let uH = self.hereForTitle!.frame.height
            
            let uFrame = CGRect(x: uX, y: uY, width: uW, height: uH)
            
            //container view displacement
            let cX = marginInset
            let cY = self.hereForView!.frame.minY + newSpace
            let cW = self.hereForView!.bounds.width
            let cH = self.hereForView!.bounds.height
            
            let cF = CGRect(x: cX, y: cY, width: cW, height: cH)
            
            if newHeight >= self.aboutMeTextView!.bounds.height {
                print("higher now")
                let tH = newHeight
                let tX = marginInset
                let tY = self.aboutMeCellLabel!.frame.maxY + insetFromLine!
                //
                let tFrame = CGRect(x: tX, y: tY, width: tW, height: tH)
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
                    self.aboutMeTextView!.frame = tFrame
                    self.aboutMeLineView!.frame = lineFrame
                    self.additionalInfoLabel!.frame = aFrame
                    self.heightCellLabel!.frame = hFrame
                    self.heightLineView!.frame = lframe
                    self.heightTextField!.frame = zF
                    
                    self.hereForTitle!.frame = uFrame
                    self.hereForView!.frame = cF
                    
                }, completion: nil)
            } else {
                //                UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear , animations: {
                //                    self.aboutMeLineView!.frame = lineFrame
                //                }, completion: nil)
            }
        }
        previousRect = currentRect
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
        self.animateHeightPicker(present: false)
        if textField == self.heightTextField {
            self.heightPicker?.isHidden = true
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        print("textViewDidEndEditing")
        
        
        print("relationship label frame", self.relationshipCellView!.frame.maxY)
        
//        self.scrollView?.contentSize = CGSize(width: UIScreen.main.bounds.width, height: self.relationshipCellView!.frame.maxY + self.insetFromLine! + (self.navSpaceLayer!.frame.height * 2))

        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return false
    }
    
    //    func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
    //        if action == "paste:" {
    //            return false
    //        }
    //        return super.canPerformAction(action, withSender: sender)
    //    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let characterCountLimit = 35
        let startingLength = textField.text?.count ?? 0
        let lengthToAdd = string.count
        let lengthToReplace = range.length
        let stringLength = startingLength + lengthToAdd - lengthToReplace
        
        return stringLength <= characterCountLimit
    }
    
    
    func addAdditionalInfoLabel() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        // Font Size
        var tFontSize = 20.0 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = 18.0 as CGFloat
        }
        print("FINAL TFONT SIZE IS,", tFontSize)
        
        //
        let nW = 3 * gridUnitWidth
        var nH = 0 as CGFloat
        let titleString = "Additional"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
        
        let nX = marginInset
        let nY = self.aboutMeLineView!.frame.maxY + marginInset
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let additionalInfoLabel = UITextView(frame: f)
        
        //
        additionalInfoLabel.text = titleString
        self.scrollView!.addSubview(additionalInfoLabel)
        self.additionalInfoLabel = additionalInfoLabel
        
        self.additionalInfoLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.additionalInfoLabel!.textAlignment = .left
        self.additionalInfoLabel!.isEditable = false
        self.additionalInfoLabel!.isScrollEnabled = false
        self.additionalInfoLabel!.isSelectable = false
        self.additionalInfoLabel!.textContainer.lineFragmentPadding = 0
        self.additionalInfoLabel!.textContainerInset = .zero
        self.additionalInfoLabel!.textColor = UIColor.black
        self.additionalInfoLabel!.backgroundColor = UIColor.clear
    }
    
    //MARK: Configure Height Picker
    
    func addHeightCell() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.additionalInfoLabel!.frame.maxY + (3 * gridUnitHeight)
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.scrollView!.addSubview(lineView)
        self.heightLineView = lineView
        
        // Font Size
        var tFontSize = cellFontSize_Large as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
        }
        print("FINAL TFONT SIZE IS,", tFontSize)
        
        //label width
        var nW = 57 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
            nW = 46 as CGFloat
        }
        
        var nH = 0 as CGFloat
        let titleString = "Height"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
        
        let nX = marginInset
        let insetFromLine = (3 * gridUnitHeight - tH) * 0.5
        let nY = lineY - insetFromLine - tH
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let heightCellLabel = UITextView(frame: f)
        
        //
        heightCellLabel.text = titleString
        self.scrollView!.addSubview(heightCellLabel)
        self.heightCellLabel = heightCellLabel
        
        //
        self.heightCellLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.heightCellLabel!.textAlignment = .left
        self.heightCellLabel!.isEditable = false
        self.heightCellLabel!.isScrollEnabled = false
        self.heightCellLabel!.isSelectable = false
        self.heightCellLabel!.textContainer.lineFragmentPadding = 0
        self.heightCellLabel!.textContainerInset = .zero
        self.heightCellLabel!.textColor = UIColor.black
        self.heightCellLabel!.backgroundColor = UIColor.clear
        
        //test tF
        //        let heightTextField = UITextField()
        let heightTextField = UILabel()
        let tW = 7 * gridUnitWidth
        let tX = 2 * gridUnitWidth
        let tf = CGRect(x: tX, y: nY, width: tW, height: nH)
        heightTextField.frame = tf
        
        self.scrollView!.addSubview(heightTextField)
        self.heightTextField = heightTextField
        
        self.heightTextField!.backgroundColor = UIColor.clear
        self.heightTextField!.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)!
        //        self.heightTextField!.placeholder = "ft' in"
        //        self.heightTextField!.keyboardType = .numbersAndPunctuation
        
        //add tap gesture to label
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.heightSelected(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.heightTextField!.addGestureRecognizer(tapGesture)
        self.heightTextField!.isUserInteractionEnabled = true
    }
    
    @objc func heightSelected(_ sender: UITapGestureRecognizer) {
        print("heightSelected")
        //        self.heightPicker?.isHidden = false
        //        self.heightPicker?.becomeFirstResponder()
        //        self.heightPicker?.backgroundColor = UIColor.white
        //        self.addHeightPicker()
        
        self.ageTextField?.resignFirstResponder()
        self.schoolTextField?.resignFirstResponder()
        self.aboutMeTextView?.resignFirstResponder()
        
        self.animateHeightPicker(present: true)
    }
    
    var heightArray = ["Do Not Show", "4'0\"", "4'1\"", "4'2\"", "4'3\"", "4'4\"", "4'5\"", "4'6\"", "4'7\"", "4'8\"", "4'9\"", "4'10\"", "4'11\"", "5'0\"", "5'1\"", "5'2\"", "5'3\"", "5'4\"", "5'5\"", "5'6\"", "5'7\"", "5'8\"", "5'9\"", "5'10\"", "5'11\"", "6'0\"", "6'1\"", "6'2\"", "6'3\"", "6'4\"", "6'5\"", "6'6\"", "6'7\"", "6'8\"", "6'9\"", "6'10\"", "6'11\"", "7'0\"", "7'1\"", "7'2\"", "7'3\"", "7'4\"", "7'5\"", "7'6\"", "7'7\"", "7'8\"", "7'9\"", "7'10\"", "7'11\""]
    
    var heightPicker: UIPickerView?
    
    var barView: UIView?
    
    var purpleShade = UIColor(displayP3Red: 128/255, green: 0, blue: 1, alpha: 1)
    
    func addHeightPicker() {
        //        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        
        let heightPicker = UIPickerView()
        heightPicker.delegate = self
        heightPicker.dataSource = self
        heightPicker.backgroundColor = UIColor.white
        //        heightPicker.addTarget(self, action: #selector(MyInfoViewController.heightChanged(_:)), for: .valueChanged)
        
        let pH = heightPicker.frame.height
        //        let pY = UIScreen.main.bounds.height - pH
        let barH = 2 * gridUnitHeight
        let pY = sHeight + barH
        
        let frame = CGRect(x: 0, y: pY, width: UIScreen.main.bounds.width, height: pH)
        heightPicker.frame = frame
        
        
        //ADD TOOLBAR VIEW
        let barW = UIScreen.main.bounds.width
        //        let barH = 2 * gridUnitHeight
        let barX = 0 as CGFloat
        //        let barY = pY - barH
        let barY = sHeight
        
        let barF = CGRect(x: barX, y: barY, width: barW, height: barH)
        let barView = UIView(frame: barF)
        barView.backgroundColor = self.purpleShade
        
        self.view.addSubview(barView)
        self.barView = barView
        
        //ADD TOOLBAR DONE BUTTON
        //
        let bW = 37 as CGFloat
        let bH = 17 as CGFloat
        let bX = UIScreen.main.bounds.maxX - (marginInset * 0.5) - bW
        let bY = (barH - bH) * 0.5
        
        //
        let fB = CGRect(x: bX, y: bY, width: bW, height: bH)
        let doneB = UIButton(frame: fB)
        
        doneB.setTitle("Done", for: .normal)
        doneB.titleLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: cellFontSize_Small)!
        doneB.setTitleColor(UIColor.white, for: .normal)
        doneB.addTarget(self, action: #selector(EditorViewController.doneHeightButton(_:)), for: .touchUpInside)
        doneB.isUserInteractionEnabled = true
        
        self.barView?.addSubview(doneB)
        
        //        let toolBar = UIToolbar()
        //        toolBar.barStyle = UIBarStyle.default
        //        toolBar.isTranslucent = false
        //        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        //        toolBar.sizeToFit()
        //
        //        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: "donePicker")
        ////        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        ////        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "donePicker")
        //
        ////        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        //        toolBar.setItems([doneButton], animated: false)
        //
        //        toolBar.isUserInteractionEnabled = true
        //
        //        textField1.inputView = picker
        //        textField1.inputAccessoryView = toolBar
        //
        self.view.addSubview(heightPicker)
        self.heightPicker = heightPicker
    }
    
    
    
    @objc func heightChanged(_ sender: UIPickerView) {
        print("heightChanged")
        //        self.heightTextField?.text =
    }
    
    //FOCUS:
    //1. update label according to selected value
    //2. remove subview when pressed
    //
    
    @objc func doneHeightButton(_ sender: UIButton) {
        print("doneHeightButton tap detected")
        //        self.heightPicker!.removeFromSuperview()
        //        self.barView?.removeFromSuperview()
        self.animateHeightPicker(present: false)
    }
    
    func donePicker() {
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        //number of wheels
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.heightArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let title = self.heightArray[row]
        
        print("current title is", title )
        
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("selected row is", self.heightArray[row])
        
        let selectedHeight = self.heightArray[row]
        
        if selectedHeight == "Do Not Show" {
            self.heightTextField?.text = ""
        } else {
            self.heightTextField?.text = selectedHeight
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.font = UIFont(name: "AvenirNext-Regular", size: 22)
            pickerLabel?.textAlignment = .center
        }
        
        pickerLabel?.text = self.heightArray[row]
        pickerLabel?.textColor = UIColor.black
        
        return pickerLabel!
    }
    
    func animateHeightPicker(present: Bool) {
        
        let heightPicker = self.heightPicker!
        let barView = self.barView!
        
        //declare pickerFrame
        let pW = UIScreen.main.bounds.width
        let pH = heightPicker.frame.height
        let pX = 0 as CGFloat
        var pY = UIScreen.main.bounds.height - pH
        
        if present == false {
            pY = UIScreen.main.bounds.height + barView.frame.height
        } else {
            //            self.scrollView!.scrollRectToVisible(<#T##rect: CGRect##CGRect#>, animated: )
        }
        
        let pickerFrame = CGRect(x: pX, y: pY, width: pW, height: pH)
        //        let pickerFrameBottom = CGRect(x: pX, y: pY, width: pW, height: pH)
        
        //declare barViewFrame
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //
        let barW = UIScreen.main.bounds.width
        let barH = 2 * gridUnitHeight
        let barX = 0 as CGFloat
        var barY = pY - barH
        
        if present == false {
            barY = UIScreen.main.bounds.height
        }
        
        let barF = CGRect(x: barX, y: barY, width: barW, height: barH)
        
        let duration = 0.25
        //Animate frames
        self.heightPicker!.isHidden = false
        self.barView!.isHidden = false
        
        UIView.animate(withDuration: duration, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            //            self.contentView.frame = CGRectMake(0, 0, keyboardFrameEnd.size.width, keyboardFrameEnd.origin.y);
            print("in animator")
            heightPicker.frame = pickerFrame
            barView.frame = barF
        }, completion: nil)
    }
    
    //MARK: Resume Draw-View Methods

    var cancelButton: UIButton?
    var cancelGray = UIColor(displayP3Red: 190/255, green: 190/255, blue: 190/255, alpha: 1)
    
    func addNavBarButtons() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)

        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let marginInsetRatio = 19 / 414 as CGFloat
        let marginInset = marginInsetRatio * UIScreen.main.bounds.width
        
        let dW = 47 as CGFloat
        let dH = 35 as CGFloat
        let dX = UIScreen.main.bounds.width - dW - marginInset
        let titleInsetFromBottom = ((self.navSpaceLayer!.bounds.height - statusBarHeight) * 0.5) - (dH * 0.5)
        let dY = statusBarHeight + titleInsetFromBottom
        
        let dF = CGRect(x: dX, y: dY, width: dW, height: dH)
        let doneButton = UIButton(frame: dF)
        
        self.navSpaceLayer!.addSubview(doneButton)
        self.doneEditingButton = doneButton
        
        self.doneEditingButton!.setTitle("Done", for: .normal)
        self.doneEditingButton!.titleLabel!.font = UIFont(name: "AvenirNext-Medium", size: 18.0)!
        self.doneEditingButton!.setTitleColor(self.hyperBlue, for: .normal)
        self.doneEditingButton!.addTarget(self, action: #selector(EditorViewController.doneEditing(_:)), for: .touchUpInside)
        self.doneEditingButton!.isUserInteractionEnabled = true
        
        //add cancel button
        let cX = marginInset
        let cF = CGRect(x: cX, y: dY, width: 60, height: dH)
        let cancelButton = UIButton(frame: cF)
        cancelButton.backgroundColor = UIColor.clear
        
        self.navSpaceLayer!.addSubview(cancelButton)
        self.cancelButton = cancelButton

        self.cancelButton!.setTitle("Cancel", for: .normal)
        self.cancelButton!.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 18.0)!
        self.cancelButton!.titleLabel!.textAlignment = .left
        self.cancelButton!.setTitleColor(self.hyperBlue, for: .normal)
        self.cancelButton!.addTarget(self, action: #selector(EditorViewController.cancelEditing(_:)), for: .touchUpInside)
        self.cancelButton!.isUserInteractionEnabled = true
    }
    
    

    //MARK: "Looking For" Table - Configure
    
    var hereForView: UIView?
    var hereForTitle: UITextView?
    
    var chatCellView: UIView?
    var chatsLabel: UITextView?
    
    var datesCellView: UIView?
    var datesLabel: UITextView?
    
    var networkingCellView: UIView?
    var networkingLabel: UITextView?
    
    var friendsCellView: UIView?
    var friendsLabel: UITextView?
    
    var relationshipCellView: UIView?
    var relationshipLabel: UITextView?
    
    func addHereForX5() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add Title
        var aFontSize = cellFontSize_Large
        if sWidth > 350 as CGFloat {
            //
        } else {
            aFontSize = cellFontSize_Small
        }
        
        //label width
        var aW = 2 * gridUnitWidth
        var aH = 0 as CGFloat
        let titleString = "Here For"
        let labelSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(aW), fontName: "AvenirNext-DemiBold", fontSize: aFontSize)
        let th = ceil(labelSize.height)
        aH = th
        
        let aX = marginInset
        let insetFromLine = (3 * gridUnitHeight - aH) * 0.5
        let aY = self.heightLineView!.frame.maxY + insetFromLine
        self.insetFromLine = insetFromLine
        
        //
        let af = CGRect(x: aX, y: aY, width: aW, height: aH)
        let hereForTitle = UITextView(frame: af)
        
        //
        hereForTitle.text = titleString
        self.scrollView!.addSubview(hereForTitle)
        self.hereForTitle = hereForTitle
        
        //
        self.hereForTitle!.font = UIFont(name: "AvenirNext-DemiBold", size: 16.0)!
        self.hereForTitle!.textAlignment = .left
        self.hereForTitle!.isEditable = false
        self.hereForTitle!.isScrollEnabled = false
        self.hereForTitle!.isSelectable = false
        self.hereForTitle!.textContainer.lineFragmentPadding = 0
        self.hereForTitle!.textContainerInset = .zero
        self.hereForTitle!.textColor = UIColor.black
        self.hereForTitle!.backgroundColor = UIColor.clear
        
        //ADD HERE FOR CONTAINER VIEW........................................................................................
        let hereForContainer = UIView()
        let containerWidth = 7 * gridUnitWidth
        let singleCellHeight = 2 * gridUnitHeight
        let noFields = 5 as CGFloat
        let totalContainerHeight = noFields * singleCellHeight
        
        let cX = marginInset
        //get half the cell frame negative
        let cY = self.hereForTitle!.frame.maxY + (insetFromLine - containerNegativeSpace!)
        let cFrame = CGRect(x: cX, y: cY, width: containerWidth, height: totalContainerHeight)
        hereForContainer.frame = cFrame
        
        self.scrollView?.addSubview(hereForContainer)
        self.hereForView = hereForContainer
        self.hereForView!.backgroundColor = UIColor.clear
        
        //1. ADD HERE FOR CONTAINER CELLS..............................................................................
        let cellButtonFontSize = 16.0 as CGFloat
        
        
        let w = containerWidth
        let h = singleCellHeight
        let x = 0 as CGFloat
        let y = (totalContainerHeight / noFields) - h
        
        let chatCellFrame1 = CGRect(x: x, y: y, width: w, height: h)
        let chatCellView = UIView(frame: chatCellFrame1 )
        chatCellView.backgroundColor = UIColor.clear
        self.hereForView!.addSubview(chatCellView)
        self.chatCellView = chatCellView
        
        //create label
        let lW = containerWidth
        let lH = th
        let lX = 0 as CGFloat
        let lY = (h - lH) * 0.5
        
        let cellLabelFrame = CGRect(x: lX, y: lY, width: lW, height: lH)
        let chatsLabel = UITextView(frame: cellLabelFrame)
        self.chatsLabel = chatsLabel
        
        self.chatsLabel!.text = "Chats"
        self.chatsLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: cellButtonFontSize)!
        self.chatsLabel!.textAlignment = .left
        self.chatsLabel!.isEditable = false
        self.chatsLabel!.isScrollEnabled = false
        self.chatsLabel!.isSelectable = false
        self.chatsLabel!.textContainer.lineFragmentPadding = 0
        self.chatsLabel!.textContainerInset = .zero
        self.chatsLabel!.textColor = self.deselectionColor
        self.chatsLabel!.backgroundColor = UIColor.clear
        
        self.chatCellView!.addSubview(chatsLabel)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.chatsSelected(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.chatCellView!.addGestureRecognizer(tapGesture)
        self.hereForView!.isUserInteractionEnabled = true
        self.chatCellView!.isUserInteractionEnabled = true
        
        //2. DATES: ADD HERE FOR CONTAINER CELLS..............................................................................
        let w2 = containerWidth
        let h2 = singleCellHeight
        let x2 = 0 as CGFloat
        let y2 = (totalContainerHeight / noFields)

        let cellFrame2 = CGRect(x: x2, y: y2, width: w2, height: h2)
        let cellView2 = UIView(frame: cellFrame2 )
        cellView2.backgroundColor = UIColor.clear
        self.hereForView!.addSubview(cellView2)
        self.datesCellView = cellView2

        //create label
        let lW2 = containerWidth
        let lH2 = th
        let lX2 = 0 as CGFloat
        let lY2 = (h - lH) * 0.5

        let cellLabelFrame2 = CGRect(x: lX2, y: lY2, width: lW2, height: lH2)
        let datesLabel = UITextView(frame: cellLabelFrame2)
        self.datesLabel = datesLabel

        self.datesLabel!.text = "Dates"
        self.datesLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)!
        self.datesLabel!.textAlignment = .left
        self.datesLabel!.isEditable = false
        self.datesLabel!.isScrollEnabled = false
        self.datesLabel!.isSelectable = false
        self.datesLabel!.textContainer.lineFragmentPadding = 0
        self.datesLabel!.textContainerInset = .zero
        self.datesLabel!.textColor = self.deselectionColor
        self.datesLabel!.backgroundColor = UIColor.clear

        self.datesCellView!.addSubview(datesLabel)

        //add gesture recognizer
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.datesSelected(_:)))
        tapGesture2.delegate = self
        tapGesture2.numberOfTapsRequired = 1
        tapGesture2.numberOfTouchesRequired = 1
        self.datesCellView!.addGestureRecognizer(tapGesture2)
        self.hereForView!.isUserInteractionEnabled = true
        self.datesCellView!.isUserInteractionEnabled = true
        
        //3. Networking: ADD HERE FOR CONTAINER CELLS..............................................................................
        let w3 = containerWidth
        let h3 = singleCellHeight
        let x3 = 0 as CGFloat
        let y3 = (totalContainerHeight / noFields) + h
        
        let cellFrame3 = CGRect(x: x3, y: y3, width: w3, height: h3)
        let cellView3 = UIView(frame: cellFrame3)
        cellView3.backgroundColor = UIColor.clear
        self.hereForView!.addSubview(cellView3)
        self.networkingCellView = cellView3
        
        //create label
        let lW3 = containerWidth
        let lH3 = th
        let lX3 = 0 as CGFloat
        let lY3 = (h - lH) * 0.5
        
        let cellLabelFrame3 = CGRect(x: lX3, y: lY3, width: lW3, height: lH3)
        let networkingLabel = UITextView(frame: cellLabelFrame3)
        self.networkingLabel = networkingLabel
        
        self.networkingLabel!.text = "Networking"
        self.networkingLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: cellButtonFontSize)!
        self.networkingLabel!.textAlignment = .left
        self.networkingLabel!.isEditable = false
        self.networkingLabel!.isScrollEnabled = false
        self.networkingLabel!.isSelectable = false
        self.networkingLabel!.textContainer.lineFragmentPadding = 0
        self.networkingLabel!.textContainerInset = .zero
        self.networkingLabel!.textColor = self.deselectionColor
        self.networkingLabel!.backgroundColor = UIColor.clear
        
        self.networkingCellView!.addSubview(networkingLabel)
        
        //add gesture recognizer
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.networkingSelected(_:)))
        tapGesture3.delegate = self
        tapGesture3.numberOfTapsRequired = 1
        tapGesture3.numberOfTouchesRequired = 1
        self.networkingCellView!.addGestureRecognizer(tapGesture3)
        self.hereForView!.isUserInteractionEnabled = true
        self.networkingCellView!.isUserInteractionEnabled = true
        
        //4. Friends: ADD HERE FOR CONTAINER CELLS..............................................................................
        let w4 = containerWidth
        let h4 = singleCellHeight
        let x4 = 0 as CGFloat
        let y4 = (totalContainerHeight / noFields) + 2 * h

        let cellFrame4 = CGRect(x: x4, y: y4, width: w4, height: h4)
        let cellView4 = UIView(frame: cellFrame4)
        cellView4.backgroundColor = UIColor.clear
        self.hereForView!.addSubview(cellView4)
        self.friendsCellView = cellView4

        //create label
        let lW4 = containerWidth
        let lH4 = th
        let lX4 = 0 as CGFloat
        let lY4 = (h - lH) * 0.5

        let cellLabelFrame4 = CGRect(x: lX4, y: lY4, width: lW4, height: lH4)
        let friendsLabel = UITextView(frame: cellLabelFrame4)
        self.friendsLabel = friendsLabel

        self.friendsLabel!.text = "Friends"
        self.friendsLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: cellButtonFontSize)!
        self.friendsLabel!.textAlignment = .left
        self.friendsLabel!.isEditable = false
        self.friendsLabel!.isScrollEnabled = false
        self.friendsLabel!.isSelectable = false
        self.friendsLabel!.textContainer.lineFragmentPadding = 0
        self.friendsLabel!.textContainerInset = .zero
        self.friendsLabel!.textColor = self.deselectionColor
        self.friendsLabel!.backgroundColor = UIColor.clear

        self.friendsCellView!.addSubview(friendsLabel)

        //add gesture recognizer
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.friendsSelected(_:)))
        tapGesture4.delegate = self
        tapGesture4.numberOfTapsRequired = 1
        tapGesture4.numberOfTouchesRequired = 1
        self.friendsCellView!.addGestureRecognizer(tapGesture4)
        self.hereForView!.isUserInteractionEnabled = true
        self.friendsCellView!.isUserInteractionEnabled = true
        
        //5. Relationship: ADD HERE FOR CONTAINER CELLS..............................................................................
        let w5 = containerWidth
        let h5 = singleCellHeight
        let x5 = 0 as CGFloat
        let y5 = (totalContainerHeight / noFields) + 3 * h
        
        let cellFrame5 = CGRect(x: x5, y: y5, width: w5, height: h5)
        let cellView5 = UIView(frame: cellFrame5)
        cellView5.backgroundColor = UIColor.clear
        self.hereForView!.addSubview(cellView5)
        self.relationshipCellView = cellView5
        
        //create label
        let lW5 = containerWidth
        let lH5 = th
        let lX5 = 0 as CGFloat
        let lY5 = (h - lH) * 0.5
        
        let cellLabelFrame5 = CGRect(x: lX5, y: lY5, width: lW5, height: lH5)
        let relationshipLabel = UITextView(frame: cellLabelFrame5)
        self.relationshipLabel = relationshipLabel
        
        self.relationshipLabel!.text = "Relationship"
        self.relationshipLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: cellButtonFontSize)!
        self.relationshipLabel!.textAlignment = .left
        self.relationshipLabel!.isEditable = false
        self.relationshipLabel!.isScrollEnabled = false
        self.relationshipLabel!.isSelectable = false
        self.relationshipLabel!.textContainer.lineFragmentPadding = 0
        self.relationshipLabel!.textContainerInset = .zero
        self.relationshipLabel!.textColor = self.deselectionColor
        self.relationshipLabel!.backgroundColor = UIColor.clear
        
        self.relationshipCellView!.addSubview(relationshipLabel)
        
        //add gesture recognizer
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.relationshipSelected(_:)))
        tapGesture5.delegate = self
        tapGesture5.numberOfTapsRequired = 1
        tapGesture5.numberOfTouchesRequired = 1
        self.relationshipCellView!.addGestureRecognizer(tapGesture5)
        self.hereForView!.isUserInteractionEnabled = true
        self.relationshipCellView!.isUserInteractionEnabled = true
        
        //
//        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(EditorViewController.relationshipSelected_L(_:)))
//        longPress.delegate = self
//        longPress.numberOfTouchesRequired = 1
//        longPress.minimumPressDuration = 1
//        longPress.allowableMovement = 30 as CGFloat
//        self.relationshipCellView!.addGestureRecognizer(longPress)
//        self.relationshipCellView!.isUserInteractionEnabled = true

    }
    
    //MARK: Looking - For Action-Target Methods
    
    var lookingForArray = [String]()
    
    var chatsSelected = false
    var datesSelected = false
    var networkingSelected = false
    var friendsSelected = false
    var relationshipSelected = false
    
    var selectionColor = UIColor.blue
    var deselectionColor = UIColor.lightGray
    var holdSelectionColor = UIColor(displayP3Red: 255/255, green: 253/255, blue: 193/255, alpha: 1)
    
    @objc func chatsSelected(_ sender: UITapGestureRecognizer) {
        print("chatsSelected")
        print("printing tapState", sender.state)
        
        if self.chatsSelected == false {
            self.lookingForArray.append("Chats")
            self.chatsSelected = true
            self.chatsLabel!.textColor = self.selectionColor
            
            print("lookingForArray now contains", lookingForArray)
            
            self.setNotificationAlert()
            
        } else if self.chatsSelected == true {
            let i = self.lookingForArray.index(of: "Chats")
            self.lookingForArray.remove(at: i!)
            self.chatsLabel!.textColor = self.deselectionColor
            self.chatsSelected = false
            
            print("lookingForArray now contains", lookingForArray)
        }
        
        self.resignAllFirstResponders()
        
    }
    
    @objc func datesSelected(_ sender: UITapGestureRecognizer) {
        print("datesSelected")
        if self.datesSelected == false {
            self.lookingForArray.append("Dates")
            self.datesSelected = true
            self.datesLabel!.textColor = self.selectionColor
            
            print("lookingForArray now contains", lookingForArray)
            
            self.setNotificationAlert()
            
        } else if self.datesSelected == true {
            let i = self.lookingForArray.index(of: "Dates")
            self.lookingForArray.remove(at: i!)
            self.datesLabel!.textColor = self.deselectionColor
            self.datesSelected = false
            
            print("lookingForArray now contains", lookingForArray)
        }
        
        self.resignAllFirstResponders()

    }
    
    @objc func networkingSelected(_ sender: UITapGestureRecognizer) {
        print("networkingSelected")
        if self.networkingSelected == false {
            self.lookingForArray.append("Networking")
            self.networkingSelected = true
            self.networkingLabel!.textColor = self.selectionColor
            
            print("lookingForArray now contains", lookingForArray)
            
            self.setNotificationAlert()
            
        } else if self.networkingSelected == true {
            let i = self.lookingForArray.index(of: "Networking")
            self.lookingForArray.remove(at: i!)
            self.networkingLabel!.textColor = self.deselectionColor
            self.networkingSelected = false
    
            print("lookingForArray now contains", lookingForArray)
        }
        
        self.resignAllFirstResponders()

    }
    
    @objc func friendsSelected(_ sender: UITapGestureRecognizer) {
        print("friendsSelected")
        
        if self.friendsSelected == false {
            self.lookingForArray.append("Friends")
            self.friendsSelected = true
            self.friendsLabel!.textColor = self.selectionColor
            
            print("lookingForArray now contains", lookingForArray)
            
            self.setNotificationAlert()
            
        } else if self.friendsSelected == true {
            let i = self.lookingForArray.index(of: "Friends")
            self.lookingForArray.remove(at: i!)
            self.friendsLabel!.textColor = self.deselectionColor
            self.friendsSelected = false
            
            print("lookingForArray now contains", lookingForArray)
        }
        
        self.resignAllFirstResponders()

    }
    
    @objc func relationshipSelected(_ sender: UITapGestureRecognizer) {
        print("relationshipSelected")
        
        self.relationshipLabel!.textColor = self.holdSelectionColor
        
        if self.relationshipSelected == false {
            self.lookingForArray.append("Relationship")
            self.relationshipSelected = true
            self.relationshipLabel!.textColor = self.selectionColor
            print("lookingForArray now contains", lookingForArray)
            self.setNotificationAlert()
        } else if self.relationshipSelected == true {
            let i = self.lookingForArray.index(of: "Relationship")
            self.lookingForArray.remove(at: i!)
            self.relationshipLabel!.textColor = self.deselectionColor
            self.relationshipSelected = false
            
            print("lookingForArray now contains", lookingForArray)
        }
        self.resignAllFirstResponders()
    }
   
    @objc func relationshipSelected_L(_ sender: UITapGestureRecognizer) {
        print("longPress relationshipSelected")

        if self.relationshipSelected == false {
            self.lookingForArray.append("Relationship")
            self.relationshipSelected = true
            self.relationshipLabel!.textColor = self.selectionColor
            
            print("lookingForArray now contains", lookingForArray)
            
            self.setNotificationAlert()
            
        } else if self.relationshipSelected == true {
            let i = self.lookingForArray.index(of: "Relationship")
            self.lookingForArray.remove(at: i!)
            self.relationshipLabel!.textColor = self.deselectionColor
            self.relationshipSelected = false
            
            print("lookingForArray now contains", lookingForArray)
        }
        
        self.resignAllFirstResponders()
    }
    
    func resignAllFirstResponders() {
        self.ageTextField?.resignFirstResponder()
        self.schoolTextField?.resignFirstResponder()
        self.aboutMeTextView?.resignFirstResponder()
        self.animateHeightPicker(present: false)
    }
    
    //MARK: Action-Target Methods
    
    func startAnimatingActivityIdicator_PhotoAwaitingUpload() {
        self.activityIndicatorView!.isHidden = false
        self.activityIndicatorView?.startAnimating()
    }
    
    @objc func cancelEditing(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
    }
    
    @objc func doneEditing(_ sender: UIButton) {
        print("doneEditing detected")
        
        //finish segue
        //SEGUE CALL MOVED TO PHOTOUPLOADER FUNCTION> VIEW WILL SEGUE WHEN PHOTO IS SUCCESSFULLY UPLOADED
        self.updateUserProfileInfo()
        
        //
//        self.setDisplayProfilePhotoURL()
//        self.setUserVisibilityPreference()
//        if self.profileImageisVisible == true {
//            //
//        } else if self.profileImageisVisible == false {
//            self.setVisibleImageURL()
//        }
        
        print("DB VISUS SET TO", self.myProfileInfoDict["selfVisus"]!)
        print("Newly set visus to,", self.profileImageisVisible)
        
        self.schoolTextField?.resignFirstResponder()
        self.aboutMeTextView?.resignFirstResponder()
        self.heightPicker?.resignFirstResponder()
        
        self.profileTogglerView?.isUserInteractionEnabled = false
        self.bView1?.isUserInteractionEnabled = false
        
        //CASE 1: user is visible and wants to SWITCH to invisible
        if self.myProfileInfoDict["selfVisus"]! == "true" && self.profileImageisVisible == false {
            print("CASE 1. Visible to Invisible Change")
            //1. delete visible image from storage
//            self.deleteClearPhotoFromDB()

            //2. save clear image to disk
            
            //3. update new blurredimage in DB
            
            //4. update db reference of visus
            
            //5. update blurPreference
            if self.makePrivateAlert_Seen != "true" {
                self.present(self.makePrivate_Alert!, animated: true)
                UserDefaults.standard.set("true", forKey: "makePrivateAlert_Seen")
                self.makePrivateAlert_Seen = "true"
            } else {
                self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
                self.setDisplayProfilePhotoURL()
                self.setUserVisibilityPreference()
                self.deleteOutgoingSwapRequestsOnReveal()
            }
        }
        
        //User is visible and wants to stay visible
        if self.myProfileInfoDict["selfVisus"]! == "true" && self.profileImageisVisible == true {
            //user changed visible picture. How can we detect if user kept same image or if user changed image?
            if self.userDidSwitchPhotos == true {
                
                print("CASE 2. ")
                
                //user changed photos need to reupload
                print("user changed photos need to reupload")
                self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
                self.setDisplayProfilePhotoURL() //reactivate
                self.setUserVisibilityPreference()
            } else {
                //user did not change photos no need to upload
                print("user did not change photos no need to upload")
                self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
            }
            //user kept same visible picture
        }
        
        //CASE 2: user is invisible
        if self.myProfileInfoDict["selfVisus"]! == "false" {
            
            //...2A && Wants to switch to visible
            if self.profileImageisVisible == true {
                print("CASE 2A. Visible to Invisible Change")
                
                if self.makePrivateAlert_Seen != "true" {
                    self.present(self.makePublic_Alert!, animated: true)
                    UserDefaults.standard.set("true", forKey: "makePublicAlert_Seen")
                    makePublicAlert_Seen = "true"
                    
                } else {
                    self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
                    self.setDisplayProfilePhotoURL()
                    self.setUserVisibilityPreference()
                    //            self.setVisibleImageURL()
                }
            }
            
            //...2B && Wants to switch blur states
            if self.profileImageisVisible == false {
                print("CASE 2B. Visible to Invisible Change")
                
                //...B1 - Wants to switch blurStates
                if self.myProfileInfoDict["blurState"]! != self.blurState {
                    print("CASE 2B1. blur states are different need to update in DB")
                    
                    //need to update photo & URL
                    self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
                    self.setDisplayProfilePhotoURL() //reactivate
                    self.setUserVisibilityPreference()
                    //need to update state
                    
                } else if self.myProfileInfoDict["blurState"]! == self.blurState {
                    print("CASE 2B2. blur states are the same, do not do anyting")
                    self.updateUserProfileInfo()
                    self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
                }
            }
        }
        //...2B && Wants to switch to stay invisible but switch blurStates
        
        //
        
        if self.myProfileInfoDict["selfVisus"]! == "false" {
            self.revealAll()
        }
        
        //if user was previously visible and has now switched to invisible && user has any pending outgoing reveal requests
        if self.myProfileInfoDict["selfVisus"]! == "true" && self.profileImageisVisible == false {
            print("WILLCALL deleteOutgoingSwapRequestsOnReveal", deleteOutgoingSwapRequestsOnReveal)
            
            
//            self.deleteOutgoingSwapRequestsOnReveal()
            
            
        }
        
    }
  
    
    func deleteClearPhotoFromDB() {
        // Create a reference to the file to delete
        let storageURL = self.myProfileInfoDict["userPhotoStorageRef"]!
        print("storageURL is,", storageURL)

        let photoRef = storageRef.child(storageURL)

        // Delete the file
        photoRef.delete { error in
            if let error = error {
                // Uh-oh, an error occurred!
            } else {
                // File deleted successfully
            }
        }
    }
    
    //Pass photo info
    func setDisplayProfilePhotoURL() {
        print("setting profile display photo")
        
        var imageData: Data?
        if self.profileImageisVisible == true {
            print("1 setDisplayProfilePhotoURL willSet profileImage to, clearProfilePhoto")
//            self.photoToUpload = self.alwaysClearPhoto!
            
//            imageData = UIImageJPEGRepresentation(self.alwaysClearPhoto!, 0.4)
            imageData = UIImageJPEGRepresentation(self.clearProfilePhoto!, 0.4)
            
        } else if self.profileImageisVisible == false {
            print("2 setDisplayProfilePhotoURL willSet profileImage to, uiCreatedImage")

//            self.photoToUpload = self.uiCreatedImage!
            
            imageData = UIImageJPEGRepresentation(self.uiCreatedImage!, 0.4)

            self.photoVault()
        }
        
//        guard let imageData = UIImageJPEGRepresentation(self.photoToUpload!, 0.6) else { return }
        
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        let uploadTask = storageRef!.child(imageStoragePath).putData(imageData!, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                print("Error uploading: \(error)")
                return
            } else {
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/userDisplayPhoto").setValue(self.storageRef!.child((metadata?.path)!).description)
                        
                        let fullURL = metadata?.downloadURLs?[0].absoluteString
                        //let fullstorageUri = self.storageRef.child((metadata?.path!)!).description
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/full_URL").setValue(fullURL)
                        print("EDITOR VC NEW PhotoURL Successfully written")
                        
                        //CALL SEGUE HERE
//                        self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)

                    }
                }
            }
        }
        
        uploadTask.observe(.success) { (snapshot) in
            print("UPLOAD snapshot SUCCESS")
            self.activityIndicatorView?.stopAnimating()
            self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
        }
        
    }
    
    func photoVault() {
        print("Editor. writing to vault")
        let selfUID = Auth.auth().currentUser!.uid
        let localPhoto = self.clearProfilePhoto!
        let fileName = "\(selfUID).jpeg"
        if let data = UIImageJPEGRepresentation(localPhoto, 0.4) {
            let fileURL = self.getDocumentsDirectory().appendingPathComponent(fileName)
            try? data.write(to: fileURL, options: .atomic)
            print("PV2. photo written to vault")
        }
    }
    
    /*
    func setVisibleImageURL() {
        //called when profileImageisVisible == true
        print("sending second image")
        let imageData = UIImageJPEGRepresentation(self.clearProfilePhoto!, 0.2)!
        let imageStoragePath = "userDisplayPhotos/" + Auth.auth().currentUser!.uid + "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        storageRef!.child(imageStoragePath).putData(imageData, metadata: metadata) { (metadata, error) in
            if let error = error {
                //handle error uploading photo
                print("Error uploading: \(error)")
                return
            } else {
                print("storing file")
                self.storageRef!.child(imageStoragePath).downloadURL { url, error in
                    if let error = error {
                        //handle downloadURL retrieval error
                    } else {
                        //self.ref.child("users/\(Auth.auth().currentUser!.uid)/userDisplayPhoto").setValue(self.storageRef!.child((metadata?.path)!).description)
                        
                        //should array value be 0,1?
                        let visibleImageURL = metadata?.downloadURLs?[0].absoluteString
                        //let fullstorageUri = self.storageRef.child((metadata?.path!)!).description
                        self.ref.child("users/\(Auth.auth().currentUser!.uid)/visibleImageURL").setValue(visibleImageURL)
                    }
                }
            }
        }
        print("second image sent")
    }
    */
    
    func setUserVisibilityPreference() {
        guard let user = Auth.auth().currentUser else { return }
        let blurState = self.blurState
        if self.profileImageisVisible == true {
            self.ref.child("users/\(user.uid)/visus").setValue("true")
            self.ref.child("users/\(user.uid)/blurState").setValue("nil")
        } else {
            self.ref.child("users/\(user.uid)/visus").setValue("false")
            self.ref.child("users/\(user.uid)/blurState").setValue("\(blurState)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! HomeScreenViewController
        destination.myProfileInfoDict = self.myProfileInfoDict
        destination.sourceController = "EditorVC"
        
//        if let photoToUpload = self.photoToUpload {
//            destination.photoFromEditor = photoToUpload
//        }
    }
    
//    @objc func allDone(_ sender: UITapGestureRecognizer) {
//        self.performSegue(withIdentifier: "toHome", sender: self)
//    }
//
    var containerNegativeSpace: CGFloat?
    
    func addSchoolCell() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.ageLineView!.frame.maxY + (3 * gridUnitHeight)
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.scrollView!.addSubview(lineView)
        self.schoolLineView = lineView
        
        // Font Size
        var tFontSize = cellFontSize_Large as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
        }
        print("FINAL TFONT SIZE IS,", tFontSize)
        
        //label width
        var nW = 57 as CGFloat
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = cellFontSize_Small as CGFloat
            nW = 46 as CGFloat
        }
        
        var nH = 0 as CGFloat
        let titleString = "School"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH
        
        let nX = marginInset
        let insetFromLine = (3 * gridUnitHeight - tH) * 0.5
        self.containerNegativeSpace = insetFromLine
        let nY = lineY - insetFromLine - tH
        
        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let schoolCellLabel = UITextView(frame: f)
        
        //
        schoolCellLabel.text = titleString
        self.scrollView!.addSubview(schoolCellLabel)
        self.schoolCellLabel = schoolCellLabel
        
        //
        self.schoolCellLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.schoolCellLabel!.textAlignment = .left
        self.schoolCellLabel!.isEditable = false
        self.schoolCellLabel!.isScrollEnabled = false
        self.schoolCellLabel!.isSelectable = false
        self.schoolCellLabel!.textContainer.lineFragmentPadding = 0
        self.schoolCellLabel!.textContainerInset = .zero
        self.schoolCellLabel!.textColor = UIColor.black
        self.schoolCellLabel!.backgroundColor = UIColor.clear
        
        //test tF
        let schoolTextField = UITextField()
        let tW = 7 * gridUnitWidth
        let tX = 2 * gridUnitWidth
        let tf = CGRect(x: tX, y: nY, width: tW, height: nH)
        schoolTextField.frame = tf
        
        self.scrollView!.addSubview(schoolTextField)
        self.schoolTextField = schoolTextField
        self.schoolTextField!.delegate = self
        
        self.schoolTextField!.backgroundColor = UIColor.clear
        self.schoolTextField!.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)!
    }
    
    func addAgeTitle() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let marginInsetRatio = 30 / 414 as CGFloat
        let marginInset = marginInsetRatio * sWidth
        
        //add Line Border
        let lineW = sWidth
        let lineH = 0.5 as CGFloat
        let lineX = 0 as CGFloat
        let lineY = self.myInfoSectionTitle!.frame.maxY + (3 * gridUnitHeight)
        
        let lineFrame = CGRect(x: lineX, y: lineY, width: lineW, height: lineH)
        let lineView = UIView(frame: lineFrame)
        
        lineView.backgroundColor = UIColor.lightGray
        self.scrollView!.addSubview(lineView)
        self.ageLineView = lineView
        
        // Font Size
        var tFontSize = self.cellFontSize_Large
        if sWidth > 350 as CGFloat {
            //
        } else {
            tFontSize = self.cellFontSize_Small
        }
        print("FINAL TFONT SIZE IS,", tFontSize)

        //
        let nW = 1 * gridUnitWidth
        var nH = 0 as CGFloat
        let titleString = "Age"
        let textFieldCGSize = self.sizeOfCopy(string: titleString, constrainedToWidth: Double(nW), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)
        let tH = ceil(textFieldCGSize.height)
        nH = tH

        let nX = marginInset
        let insetFromLine = (3 * gridUnitHeight - tH) * 0.5
        let nY = lineY - insetFromLine - tH

        //
        let f = CGRect(x: nX, y: nY, width: nW, height: nH)
        let ageCellLabel = UITextView(frame: f)

        //
        ageCellLabel.text = titleString
        self.scrollView!.addSubview(ageCellLabel)
        self.ageCellLabel = ageCellLabel

        //
        self.ageCellLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)!
        self.ageCellLabel!.textAlignment = .left
        self.ageCellLabel!.isEditable = false
        self.ageCellLabel!.isScrollEnabled = false
        self.ageCellLabel!.isSelectable = false
        self.ageCellLabel!.textContainer.lineFragmentPadding = 0
        self.ageCellLabel!.textContainerInset = .zero
        self.ageCellLabel!.textColor = UIColor.black
        self.ageCellLabel!.backgroundColor = UIColor.clear
        
        //test tF
        let ageTextField = UITextField()
        let tW = 7 * gridUnitWidth
        let tX = 2 * gridUnitWidth
        let tf = CGRect(x: tX, y: nY, width: tW, height: nH)
        ageTextField.frame = tf
        
        self.scrollView!.addSubview(ageTextField)
        self.ageTextField = ageTextField
        self.ageTextField!.delegate = self
        self.ageTextField!.keyboardType = .numberPad
        
        self.ageTextField!.backgroundColor = UIColor.clear
        self.ageTextField!.font = UIFont(name: "AvenirNext-Regular", size: tFontSize)!
    }
    
    func sizeOfCopy(string: String, constrainedToWidth width: Double, fontName: String, fontSize: CGFloat) -> CGSize {
        return NSString(string: string).boundingRect(
            with: CGSize(width: width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: UIFont.init(name: fontName, size: fontSize)! ],
            context: nil).size
    }
    
    func addScreenTitle() {
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        
        //
        let navSpaceRatio = 76 / 736 as CGFloat
        let navSpaceHeight = sHeight * navSpaceRatio
        let statusBarSize = UIApplication.shared.statusBarFrame.size
        let statusBarHeight = Swift.min(statusBarSize.width, statusBarSize.height)
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //header frame properties
        let lW = 3 * gridUnitWidth
        let lH = 35 as CGFloat
        let lX = (sWidth - lW) * 0.5
        let titleInsetFromBottom = ((self.navSpaceLayer!.bounds.height - statusBarHeight) * 0.5) - (lH * 0.5)
        let lY = statusBarHeight + titleInsetFromBottom
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let titleLabel = UILabel(frame: tF)
        
        //init
        self.navSpaceLayer!.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        self.titleLabel!.text = "Edit Profile"
        self.titleLabel!.font = UIFont(name: "AvenirNext-Regular", size: 20)!
        self.titleLabel!.textColor = UIColor.black
        self.titleLabel!.textAlignment = .center
    }
    
    var layerTintColor = UIColor(displayP3Red: 248/255, green: 247/255, blue: 247/255, alpha: 1)
    
    func addMainImageView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = 4 * gridUnitWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (sWidth - imageViewWidth) * 0.5
        let imageView_Y = self.navEdge!.frame.maxY + gridUnitHeight
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let mainImageView = UIImageView(frame: imageViewFrame)
        
        mainImageView.contentMode = .scaleAspectFill
        mainImageView.clipsToBounds = true
        mainImageView.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        mainImageView.layer.cornerRadius = 15.0 as CGFloat
        mainImageView.layer.masksToBounds = true
        
        self.mainImageView = mainImageView
        self.scrollView!.addSubview(mainImageView)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.mainProfileImage(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.mainImageView!.addGestureRecognizer(tapGesture)
        self.mainImageView!.isUserInteractionEnabled = true
    }
    
    func addNoFaceImageView() {
        //
        let widthRatio = 75 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let imageViewWidth = widthRatio * sWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (self.mainImageView!.bounds.height - imageViewHeight) * 0.5
        let imageView_Y = imageView_X
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let noFaceView = UIImageView(frame: imageViewFrame)
        
        noFaceView.contentMode = .scaleAspectFill
        noFaceView.backgroundColor = UIColor.clear
        noFaceView.image = UIImage(named: "noFaceView_Editor")
        //
        
        self.noFaceImageView = noFaceView
        self.mainImageView!.addSubview(noFaceView)
    }
    
    var imageErrorTextView: UITextView?
    
    func addDidNotDetectFaceTextView() {
        //
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var tFontSize = 12.0 as CGFloat
        var tWidth = 270 as CGFloat
        let errorCopy = "Unable to clearly detect your face in this picture. Please try again using another image."

        if sWidth > 350 as CGFloat {
            print("LARGESCREEN")
            //
        } else {
            print("SMALLSCREEN")
            tWidth = 225 as CGFloat
            tFontSize = 10.0 as CGFloat
        }
        var textFieldCGSize = self.sizeOfCopy(string: errorCopy, constrainedToWidth: Double(tWidth), fontName: "AvenirNext-DemiBold", fontSize: tFontSize)

        let tH = ceil(textFieldCGSize.height)
        let tX = (sWidth - tWidth) * 0.5
        let baseLineY = 10 as CGFloat
        let tY = self.mainImageView!.frame.maxY + (gridUnitHeight * 0.5)
        
        //
        let tFrame = CGRect(x: tX, y: tY, width: tWidth, height: tH)
        let imageErrorTextView = UITextView(frame: tFrame)
        
        imageErrorTextView.textColor = UIColor.red
        imageErrorTextView.text = errorCopy
        //
        self.scrollView!.addSubview(imageErrorTextView)
        
        self.imageErrorTextView = imageErrorTextView
        self.imageErrorTextView!.textAlignment = .center
        self.imageErrorTextView!.font = UIFont(name: "AvenirNext-DemiBold", size: tFontSize)
        self.imageErrorTextView!.isEditable = false
        self.imageErrorTextView!.isScrollEnabled = false
        self.imageErrorTextView!.isSelectable = false
        self.imageErrorTextView!.textContainer.lineFragmentPadding = 0
        self.imageErrorTextView!.textContainerInset = .zero
        self.imageErrorTextView!.textColor = UIColor.red
        self.imageErrorTextView!.backgroundColor = UIColor.clear
        
        self.imageErrorTextView = imageErrorTextView
    }
    
    @objc func mainProfileImage(_ sender: UITapGestureRecognizer) {
        print("mainProfileImage")
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: Cofigure Photo Selection /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //Profile Photo Properties
    var clearProfilePhoto: UIImage?
    
    var unblurredCIImage: CIImage?
    var blurredImageLight: CIImage? { didSet { print("DIDSET blurredImageLight")}}
    var blurredImageMedium: CIImage? { didSet { print("DIDSET blurredImageMedium")}}
    var blurredImageHeavy: CIImage? { didSet { print("DIDSET blurredImageHeavy")}}
    var uiCroppedBlurredImage: UIImage?
    
    var profileImageisVisible: Bool?
    var photoToUpload: UIImage?
    var user: User?
    var selfUID: String?
    
    //trial
    var uiCreatedImage: UIImage?
    
    //user preferences
    var isHiddenPreference = true
    
    //    let lightBlurSigma = 20.0
    //    let mediumBlurSigma = 35.0
    //    let heavyBlurSigma = 50.0
    
    let lightBlurSigma = 20.0
    let mediumBlurSigma = 40.0
    let heavyBlurSigma = 70.0
    
    var userDidSwitchPhotos: Bool?
    
//    func compareImages(existingImage: UIImage, isEqualTo newlySelectedImage: UIImage) -> Bool {
//        let data1: NSData = UIImagePNGRepresentation(existingImage)! as NSData
//        let data2: NSData = UIImagePNGRepresentation(newlySelectedImage)! as NSData
//        return data1.isEqual(data2)
//    }

    //this method will not properly commpare two uiimages unless they are selected in same VC instance
    //need to use cgmethods
    
    func compareImagesUI(existingImage: UIImage, newlySelectedImage: UIImage) -> Bool {
        return existingImage == newlySelectedImage
    }
    
    var alwaysClearPhoto: UIImage?
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //imagePickerController method's job is simply to check current photo & visibiliyt settings and update view accordingly
        //AND NOT to update visibility settings
        //always reset to clear
        
        print("2 detecting profileTogglerView position Y", self.profileTogglerView!.frame.minY)

        
        if let clearSelectedProfilePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            
            print("UIImageselected new image")
            
            let ciImageObject = CIImage(image: info[UIImagePickerControllerOriginalImage] as! UIImage)
            self.unblurredCIImage = ciImageObject
            self.createBlurredImages(withClearCIImage: ciImageObject!)

            self.mainImageView!.image = clearSelectedProfilePhoto
            print("imagePickerController willSet new clearProfilePhoto")
            self.clearProfilePhoto = clearSelectedProfilePhoto

            if #available(iOS 11.0, *) {
                self.faciemAspicio()
            } else {
                self.fdCoreImage()
            }
            
            self.alwaysClearPhoto = clearSelectedProfilePhoto
            self.profileImageisVisible = true
            self.userDidSwitchPhotos = true
            self.visibilityLabel?.text = "Public Profile"
        }
        
        picker.dismiss(animated: true, completion: nil)
           
    }
    
    /*
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        //imagePickerController method's job is simply to check current photo & visibiliyt settings and update view accordingly
        //AND NOT to update visibility settings
        //always reset to clear
        
        if let clearSelectedProfilePhoto = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            self.clearProfilePhoto = clearSelectedProfilePhoto
            let ciImageObject = CIImage(image: info[UIImagePickerControllerOriginalImage] as! UIImage)
            self.unblurredCIImage = ciImageObject
            self.createBlurredImages(withClearCIImage: ciImageObject!)
            
            
            
            //error:
            // user switching blurred photo to new photo, not preserving new photo selection
            if self.myProfileInfoDict["selfVisus"]! == "true" {
                
                if self.profileImageisVisible == true {
                    
                    
                } else {
                    //0 New profile setting is invisible
                    
                }
                
                //1 New profile setting is visible
                //then preserve visible image
                
                self.mainImageView!.image = clearSelectedProfilePhoto
                
                if #available(iOS 11.0, *) {
                    self.faciemAspicio()
                } else {
                    self.fdCoreImage()
                }
                
                self.profileImageisVisible = true
                self.userDidSwitchPhotos = true
                
            } else if self.myProfileInfoDict["selfVisus"]! == "false" {
                
                if self.profileImageisVisible == true {
                    //1 New profile setting is visible
                    
                } else {
                    //0 New profile setting is invisible
                    
                    let blurState = self.blurState
                    
                    if blurState == "lightBlur" {
                        let ciCroppedImage = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
                        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
                        self.mainImageView!.image = uiCroppedBlurredImage
                        
                    } else if blurState == "mediumBlur" {
                        let ciCroppedImage = self.blurredImageMedium!.cropped(to: (self.unblurredCIImage!.extent))
                        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
                        self.mainImageView!.image = uiCroppedBlurredImage
                        
                    } else if blurState == "heavyBlur" {
                        let ciCroppedImage = self.blurredImageHeavy!.cropped(to: (self.unblurredCIImage!.extent))
                        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
                        self.mainImageView!.image = uiCroppedBlurredImage
                    }
                }
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    */
    
    
    //
    @available(iOS 11.0, *)
    //import Vision SDK
    func faciemAspicio() {
        print("CREATING face detection request")
        //create detect face request
        let faceDetectRequest = VNDetectFaceRectanglesRequest { (vnRequest, error) in
            if let error = error {
                print("\(error) could not detect face")
            }
            
            if (vnRequest.results?.isEmpty)! {
                print("NO FACE DETECTED")
                self.configurePhotoError_UI()
            } else {
                vnRequest.results?.forEach({ (detectionResult) in
                    print("FACE SUCCESSFULLY DETECTED. DETECTION RESULT IS:", detectionResult)
                    self.configurePhotoSuccess_UI()
                })
            }
        }
        self.handleRequest(visionRequest: faceDetectRequest)
    }
    
    //add label & add error label
    let errorNotification = UINotificationFeedbackGenerator()

    func configurePhotoSuccess_UI() {
        //        self.transparencyView!.isHidden = true
        self.imageErrorTextView!.isHidden = true
        self.profileTogglerView?.isHidden = false
        self.visibilityLabel?.isHidden = false

        self.profileTogglerView!.isUserInteractionEnabled = true
        self.noFaceImageView!.isHidden = true
        self.doneEditingButton?.setTitleColor(self.hyperBlue, for: .normal)
        self.doneEditingButton?.isUserInteractionEnabled = true
    }
    
    func configurePhotoError_UI() {
        //        self.transparencyView!.isHidden = false
        self.errorNotification.notificationOccurred(.error)
        self.imageErrorTextView!.isHidden = false
        self.profileTogglerView?.isHidden = true
        self.visibilityLabel?.isHidden = true
        self.noFaceImageView!.isHidden = false
        self.bView1?.isHidden = true
        self.doneEditingButton?.setTitleColor(UIColor.lightGray, for: .normal)
        self.doneEditingButton?.isUserInteractionEnabled = false
    }
    
//    self.mainImageView!.image = clearProfilePhoto
//    self.clearProfilePhoto = clearProfilePhoto
    
    //need to handle our request
    @available(iOS 11.0, *)
    func handleRequest(visionRequest: VNDetectFaceRectanglesRequest) {
        print("HANDLING face detection request")
        guard let cgImage = self.mainImageView!.image?.cgImage else { return }
        let requestHandler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        do {
            try requestHandler.perform([visionRequest])
        } catch let requestError {
            print("Failed to perform request:", requestError)
        }
    }
    
    func fdCoreImage() {
        print("Detecting face using Core Image")
        let context = CIContext()
        //guard let ciImage = self.profileImageView.image?.ciImage else { return }
        guard let ciImage = CIImage(image: (mainImageView!.image)!) else { return }
        print("image succesfully retrieved")
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: context, options: accuracy)
        let faces = faceDetector?.features(in: ciImage)
        print("checking array values")
        if (faces?.isEmpty)! {
            print("CORE IMAGE RESULT: no face detected")
            self.configurePhotoError_UI()
        } else {
            print("CORE IMAGE RESULT: Face successfully detected")
            self.configurePhotoSuccess_UI()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Resume Draw-View Methods /////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    var finalTogglerView_Y: CGFloat?
    
    func addProfileToggler() {
        //
        let widthRatio = 70 / 414 as CGFloat
        let heightRatio = 60 / 70 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat

        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        print("addsBarH", sBarH)
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight

        let imageViewWidth = widthRatio * sWidth
        let imageViewHeight = imageViewWidth * heightRatio
        let imageView_X = (sWidth - imageViewWidth) * 0.5
//        let imageView_Y = (3 * superInsetHeight) +  (1 * gridUnitHeight)
//        let imageView_Y = 0 as CGFloat

        let visibilityLabelH = 20 as CGFloat
        let newYRelative = 0.5 * (self.layerMaskView!.frame.maxY - self.mainImageView!.frame.maxY - imageViewHeight - visibilityLabelH)
        let imageView_Y = self.mainImageView!.frame.maxY + newYRelative
        self.finalTogglerView_Y = imageView_Y
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileTogglerView = UIImageView(frame: imageViewFrame)
        profileTogglerView.image = UIImage(named: "publicProfile")
        profileTogglerView.contentMode = .scaleAspectFit
        profileTogglerView.backgroundColor = UIColor.clear

        self.profileTogglerView = profileTogglerView
        self.scrollView!.addSubview(profileTogglerView)

        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.toggleProfileVisibility(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.profileTogglerView!.addGestureRecognizer(tapGesture)
        self.profileTogglerView!.isUserInteractionEnabled = true
        
        //        self.navEdge!.frame.maxY
        //        let imageView_Y = self.navEdge!.frame.maxY
        
        //        self.scrollView!.addSubview(profileTogglerView)
        
        //        let centerX = sWidth * 0.5
        //        let centerY = self.layerMaskView!.frame.maxY - imageViewHeight
        //        self.profileTogglerView!.layer.position = CGPoint(x: centerX, y: centerY)
//        self.layerMaskView?.addSubview(profileTogglerView)
        
        print("detecting profileTogglerView position Y", self.profileTogglerView!.frame.minY)
    }

    var currentProfileSetting = "Public Profile"
    var visibilityLabel: UILabel?
    
    //bug updated switch from heavy to clear returns light image instead of returning clear image
    @objc func toggleProfileVisibility(_ sender: UITapGestureRecognizer) {
        print("toggleProfileVisibility")
        
        self.resignAllFirstResponders()
        
        if self.currentProfileSetting == "Public Profile" {
            self.animateTogglerView(to: "Left")
            self.currentProfileSetting = "Private Profile"
            self.visibilityLabel!.text = "Private Profile"
            self.profileTogglerView!.image = UIImage(named: "privateProfile")

            let ciCroppedImage = self.blurredImageLight!.cropped(to: (self.unblurredCIImage!.extent))
            self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
            let lightP = uiCroppedBlurredImage

            UIView.transition(with: self.mainImageView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                self.mainImageView!.image = lightP
            }, completion: nil)

            self.updateImageToUpload(withBlurSigma: lightBlurSigma)

        } else {
       
            self.pulsatingLayer!.removeAnimation(forKey: "pulse")
            self.pulsatingLayer!.removeAnimation(forKey: "fade")
            self.pulsatingLayer!.isHidden = true

            self.animateTogglerView(to: "Right")
            self.currentProfileSetting = "Public Profile"
            self.visibilityLabel!.text = "Public Profile"
            self.profileTogglerView!.image = UIImage(named: "publicProfile")

            UIView.transition(with: self.mainImageView!, duration: 0.25, options: .transitionCrossDissolve, animations: {
                if self.userDidSwitchPhotos == true {
                    self.mainImageView!.image = self.alwaysClearPhoto!
                } else {
                    self.mainImageView!.image = self.clearProfilePhoto
                }
            }, completion: nil)
            self.photoToUpload = self.clearProfilePhoto!
            self.profileImageisVisible = true
        }
    }
    
    func updateImageToUpload(withBlurSigma: Double) {
        self.contextCreateImage(withBlurSigma: withBlurSigma)
        self.profileImageisVisible = false
    }
    
    func contextCreateImage(withBlurSigma: Double) {
        let context = CIContext()                                           // 1
        
        let filter = CIFilter(name: "CIGaussianBlur")!                         // 2
        filter.setValue(withBlurSigma, forKey: "inputRadius")
        
        let image = self.unblurredCIImage                                    // 3
        filter.setValue(image, forKey: kCIInputImageKey)
        
        let result = filter.outputImage!                                    // 4
        
        //test
        let croppedResult = result.cropped(to: (self.unblurredCIImage!.extent))
        let cgImage = context.createCGImage(croppedResult, from: croppedResult.extent)// 5
        self.uiCreatedImage = UIImage(cgImage: cgImage!)
    }
    
    func animateTogglerView(to: String) {
        let buttonView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        var horizontalDisplacement = -gridUnitWidth as CGFloat
        if to == "Left" {
            horizontalDisplacement = -gridUnitWidth
            self.visibilityLabel!.text = "Private Profile"
        } else if to == "Right" {
            horizontalDisplacement = 0
            self.visibilityLabel!.text = "Public Profile"
        }

        let bW = buttonView.bounds.width
        let bH = buttonView.bounds.height
        let bX = ((sWidth - bW) * 0.5) + horizontalDisplacement
        let bY = self.profileTogglerView!.frame.minY
        
        let lW = self.profileTogglerView!.bounds.width
        let lH = 20 as CGFloat
        let lX = ((sWidth - lW) * 0.5) + horizontalDisplacement
        let lY = self.profileTogglerView!.frame.maxY + (0 * gridUnitHeight)

        UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut, .beginFromCurrentState], animations: {
            self.bView1?.isHidden = true
            buttonView.frame = CGRect(x: bX, y: bY, width: bW, height: bH)
            vLabel.frame = CGRect(x: lX, y: lY, width: lW, height: lH)
        }) { (true) in
            if to == "Left" {
                self.addBlurControlView()
                self.animatePulsatingLayer_N()
            } else if to == "Right" {
                if let bView1 = self.bView1 {
                    bView1.removeFromSuperview()
                }
            }
        }
    }
    
    var bView1: UIImageView?
    
    func addBlurControlView() {
        let rRatio = 46.5 / 414 as CGFloat
        let togglerView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = rRatio * sWidth
        let bH = bW
        let finalX_togglerView = ((sWidth - togglerView.bounds.width) * 0.5) - gridUnitWidth
        let bX1 = finalX_togglerView + togglerView.bounds.width + (gridUnitWidth * 0.5)
//        let bY = togglerView.frame.minY + (togglerView.bounds.height - bH)
        let bY = togglerView.frame.maxY - bH
        
        //
        let bFrame1 = CGRect(x: bX1, y: bY, width: bW, height: bH)
        let bView1 = UIImageView(frame: bFrame1)
        bView1.contentMode = .scaleAspectFit
        bView1.image = UIImage(named: "blurLight")
        bView1.backgroundColor = UIColor.clear
        bView1.layer.masksToBounds = true
        bView1.layer.cornerRadius = bW * 0.5
            
        bView1.layer.borderWidth = 2
        bView1.layer.borderColor = self.layerTintColor.cgColor
        
        self.bView1 = bView1
        
        //final frames
        self.scrollView!.addSubview(bView1)
        
        //add gesture recognizer
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.switchBlurStates(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.bView1!.addGestureRecognizer(tapGesture)
        self.bView1!.isUserInteractionEnabled = true
    }
    
    var blurState = "lightBlur"
    let blurStates = ["lightBlur", "mediumBlur", "heavyBlur"]
    
    @objc func switchBlurStates(_ sender: UITapGestureRecognizer) {
        print("switchBlurStates")
        
        self.visibilityLabel?.text = "Private Profile"
        
        if self.blurState == "lightBlur" {
            self.bView1!.image = UIImage(named: "blurMedium")
            self.blurState = "mediumBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageMedium)
            self.updateImageToUpload(withBlurSigma: mediumBlurSigma)
            
        } else if self.blurState == "mediumBlur" {
            self.bView1!.image = UIImage(named: "blurHeavy")
            self.blurState = "heavyBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageHeavy)
            self.updateImageToUpload(withBlurSigma: heavyBlurSigma)
            
        } else if self.blurState == "heavyBlur" {
            self.bView1!.image = UIImage(named: "blurLight")
            self.blurState = "lightBlur"
            self.setProfileImageView(withBlurImage: self.blurredImageLight)
            self.updateImageToUpload(withBlurSigma: lightBlurSigma)
        }
    }
    
    func setProfileImageView(withBlurImage: CIImage?) {
        let ciCroppedImage = withBlurImage!.cropped(to: (self.unblurredCIImage!.extent))
        self.uiCroppedBlurredImage = UIImage(ciImage: ciCroppedImage)
        let toImage = uiCroppedBlurredImage
        
        UIView.transition(with: self.mainImageView!, duration: 0.35, options: .transitionCrossDissolve, animations: {
            self.mainImageView!.image = toImage
        }, completion: nil)
        
        self.profileImageisVisible = false
        
    }
    
    func addVisibilityLabel() {
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        //header frame properties
        let lW = self.profileTogglerView!.bounds.width
        let lH = 20 as CGFloat
        var lX = (sWidth - lW) * 0.5
        let lY = self.profileTogglerView!.frame.maxY + (0 * gridUnitHeight)
        
        //init label
        let tF = CGRect(x: lX, y: lY, width: lW, height: lH)
        let visibilityLabel = UILabel(frame: tF)
        
        visibilityLabel.text = self.currentProfileSetting
        visibilityLabel.textColor = UIColor.black
        visibilityLabel.textAlignment = .center
        
        //init
        self.scrollView!.addSubview(visibilityLabel)
        self.visibilityLabel = visibilityLabel
        self.visibilityLabel!.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)!
        self.visibilityLabel!.adjustsFontSizeToFitWidth = true
    }
    
    @objc func changeMainImage(_ sender: UITapGestureRecognizer) {
        print("changeMainImage")
    
    }
    
    var pulsatingLayer: CAShapeLayer?
    
    func addPulsatingLayer() {
        let rRatio = 46.5 / 414 as CGFloat
        let togglerView = self.profileTogglerView!
        let vLabel = self.visibilityLabel!
        
        //
        let widthRatio = 322 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        //
        let bW = rRatio * sWidth
        let bH = bW
        let finalX_togglerView = ((sWidth - togglerView.bounds.width) * 0.5) - gridUnitWidth
        let bX1 = finalX_togglerView + togglerView.bounds.width + (gridUnitWidth * 0.5)
        let bX2 = bX1 + bW + (gridUnitWidth * 0.5)
        let bX3 = bX2 + bW + (gridUnitWidth * 0.5)
//        let bY = togglerView.frame.minY + (togglerView.bounds.height - bH)
        let bY = togglerView.frame.maxY - bH

        //
        let circularPath = UIBezierPath(arcCenter: .zero, radius: bW * 0.5, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        
        pulsatingLayer = CAShapeLayer()
        pulsatingLayer!.path = circularPath.cgPath
        
        pulsatingLayer!.fillColor = UIColor.clear.cgColor
        pulsatingLayer!.lineWidth = 3
        pulsatingLayer!.strokeColor = UIColor.lightGray.cgColor
        
        let cX = bX1 + (bW * 0.5)
        let cY = bY + (bW * 0.5)
        let cPoint = CGPoint(x: cX, y: cY)
        
        pulsatingLayer!.position = cPoint
        
        self.scrollView!.layer.addSublayer(pulsatingLayer!)
    }
    
    func animatePulsatingLayer_N() {
        print("animatePulsatingLayer_N")
        //should also call animator when textFieldBeginsEditing
        
        //faster ring animation expand
        //slower ring animation contract
        //MUST add pause between flashes. It's currently too fast and tells the user "you MUST reveal your profile". It needs to be slower and more suggestive.
        
        self.pulsatingLayer!.isHidden = false
        
        let pulse = CABasicAnimation(keyPath: "transform.scale")
        pulse.toValue = 1.2
        pulse.duration = 0.35
        pulse.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        pulse.autoreverses = false
        pulse.repeatCount = 3
        
        let fade = CABasicAnimation(keyPath: "opacity")
        fade.fromValue = 1
        fade.toValue = 0
        fade.duration = 0.35
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        fade.autoreverses = false
        fade.repeatCount = 3
        
        pulsatingLayer!.add(pulse, forKey: "pulse")
        pulsatingLayer!.add(fade, forKey: "fade")
        
        let when = DispatchTime.now() + (3 * 0.27)
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.pulsatingLayer!.isHidden = true
        }
    }
    
    //
    var profileImageView_1: UIImageView?
    var profileImageView_2: UIImageView?
    var profileImageView_3: UIImageView?

    var addIcon_1: UIImageView?
    var addIcon_2: UIImageView?
    var addIcon_3: UIImageView?

    func addProfileImageView_1() {
        //
        let wRatio = 100 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        let imageViewWidth = wRatio * sWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (sWidth - imageViewWidth) * 0.5
        let imageView_Y = (4 * superInsetHeight) + gridUnitHeight
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView_1 = UIImageView(frame: imageViewFrame)
        
        profileImageView_1.contentMode = .scaleAspectFill
        profileImageView_1.clipsToBounds = true
        profileImageView_1.backgroundColor = UIColor.clear
        profileImageView_1.layer.cornerRadius = imageViewWidth * 0.5
        profileImageView_1.layer.masksToBounds = true
        profileImageView_1.layer.borderWidth = 1
        profileImageView_1.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        self.profileImageView_1 = profileImageView_1
        self.scrollView!.addSubview(profileImageView_1)
        
        //
        let xPos = 1.5 * gridUnitWidth
        let yPos = imageView_Y + (0.5 * imageViewHeight)
        self.profileImageView_1!.layer.position = CGPoint(x: xPos, y: yPos)
        
        //add+
        let w = imageViewWidth * 0.25
        let h = w
        let x = self.profileImageView_1!.frame.maxX - w
        let y = self.profileImageView_1!.frame.maxY - h
        
        let f = CGRect(x: x, y: y, width: w, height: h)
        let addIcon_1 = UIImageView(frame: f)
        self.addIcon_1 = addIcon_1
        
        addIcon_1.backgroundColor = self.layerTintColor
        addIcon_1.layer.cornerRadius = w * 0.5
        addIcon_1.layer.masksToBounds = true
        addIcon_1.layer.borderWidth = 1
        addIcon_1.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        addIcon_1.image = UIImage(named: "addImageSymbol")
        self.scrollView!.addSubview(addIcon_1)
    }
    
    func addProfileImageView_2() {
        //
        let wRatio = 100 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        
        let imageViewWidth = wRatio * sWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (sWidth - imageViewWidth) * 0.5
        let imageView_Y = (4 * superInsetHeight) + gridUnitHeight
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView_2 = UIImageView(frame: imageViewFrame)
        
        profileImageView_2.contentMode = .scaleAspectFill
        profileImageView_2.clipsToBounds = true
        profileImageView_2.backgroundColor = UIColor.clear
        profileImageView_2.layer.cornerRadius = imageViewWidth * 0.5
        profileImageView_2.layer.masksToBounds = true
        profileImageView_2.layer.borderWidth = 1
        profileImageView_2.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        self.profileImageView_2 = profileImageView_2
        self.scrollView!.addSubview(profileImageView_2)
        
        //
        let xPos = sWidth * 0.5
        let yPos = imageView_Y + (0.5 * imageViewHeight)
        self.profileImageView_2!.layer.position = CGPoint(x: xPos, y: yPos)
        
        //add+
        let w = imageViewWidth * 0.25
        let h = w
        let x = self.profileImageView_2!.frame.maxX - w
        let y = self.profileImageView_2!.frame.maxY - h
        
        let f = CGRect(x: x, y: y, width: w, height: h)
        let addIcon_2 = UIImageView(frame: f)
        self.addIcon_2 = addIcon_2
        
        addIcon_2.backgroundColor = self.layerTintColor
        addIcon_2.layer.cornerRadius = w * 0.5
        addIcon_2.layer.masksToBounds = true
        addIcon_2.layer.borderWidth = 1
        addIcon_2.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        addIcon_2.image = UIImage(named: "addImageSymbol")
        
        self.scrollView!.addSubview(addIcon_2)
        self.addIcon_2 = addIcon_2
    }
    
    func addProfileImageView_3() {
        //
        let wRatio = 100 / 414 as CGFloat
        let gridUnitX = 45.5 / 414 as CGFloat
        let gridUnitY = 22.5 / 736 as CGFloat
        
        //
        let screen = UIScreen.main.bounds
        let sWidth = screen.width
        let sHeight = screen.height
        let sBarH = UIApplication.shared.statusBarFrame.size.height
        
        //
        let gridUnitAspectRatio = 22.5 / 45.5 as CGFloat
        let gridUnitWidth = gridUnitX * sWidth
        let gridUnitHeight = gridUnitWidth * gridUnitAspectRatio
        let superInsetHeight = 4 * gridUnitHeight
        
        let imageViewWidth = wRatio * sWidth
        let imageViewHeight = imageViewWidth
        let imageView_X = (sWidth - imageViewWidth) * 0.5
        let imageView_Y = (4 * superInsetHeight) + gridUnitHeight
        
        //initialize view
        let imageViewFrame = CGRect(x: imageView_X, y: imageView_Y, width: imageViewWidth, height: imageViewHeight)
        let profileImageView_3 = UIImageView(frame: imageViewFrame)
        
        profileImageView_3.contentMode = .scaleAspectFill
        profileImageView_3.clipsToBounds = true
        profileImageView_3.backgroundColor = UIColor.clear
        profileImageView_3.layer.cornerRadius = imageViewWidth * 0.5
        profileImageView_3.layer.masksToBounds = true
        profileImageView_3.layer.borderWidth = 1
        profileImageView_3.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        self.profileImageView_3 = profileImageView_3
        self.scrollView!.addSubview(profileImageView_3)
        
        //
        let xPos = sWidth - (1.5 * gridUnitWidth)
        let yPos = imageView_Y + (0.5 * imageViewHeight)
        self.profileImageView_3!.layer.position = CGPoint(x: xPos, y: yPos)
        
        //add+
        let w = imageViewWidth * 0.25
        let h = w
        let x = self.profileImageView_3!.frame.maxX - w
        let y = self.profileImageView_3!.frame.maxY - h
        
        let f = CGRect(x: x, y: y, width: w, height: h)
        let addIcon_3 = UIImageView(frame: f)
        self.addIcon_3 = addIcon_3
        
        addIcon_3.backgroundColor = self.layerTintColor
        addIcon_3.layer.cornerRadius = w * 0.5
        addIcon_3.layer.masksToBounds = true
        addIcon_3.layer.borderWidth = 1
        addIcon_3.layer.borderColor  = UIColor(displayP3Red: 226/255, green: 226/255, blue: 226/255, alpha: 1).cgColor
        
        addIcon_3.image = UIImage(named: "addImageSymbol")
        
        self.scrollView!.addSubview(addIcon_3)
        self.addIcon_3 = addIcon_3
        
        //
        //add gesture
        //remove old gesture recognizer and add new gesture recognizer that allows button to unlock
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditorViewController.addImage_3(_:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.profileImageView_3!.addGestureRecognizer(tapGesture)
        self.profileImageView_3!.isUserInteractionEnabled = true
    }
    
    @objc func addImage_1(_ sender: UITapGestureRecognizer) {
        print("addImage_1")
    }
   
    @objc func addImage_2(_ sender: UITapGestureRecognizer) {
        print("addImage_2")
    }
    
    @objc func addImage_3(_ sender: UITapGestureRecognizer) {
        print("addImage_3")
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    var updatedSchool = ""
//    var updatedAboutMe = ""
//    var updatedHeight = ""
//    var updatedHereFor = ""
//    var updatedUserVisus = ""
//    var updatedBlurState = ""
    
    func updateUserProfileInfo() {
        self.ref = Database.database().reference()
     
        //set ancillary values
        if let school = self.schoolTextField?.text {
            self.ref.child("users/\(self.selfUID!)/School").setValue(school)
        }
        
        if let aboutMe = self.aboutMeTextView?.text {
            self.ref.child("users/\(self.selfUID!)/AboutMe").setValue(aboutMe)
        }
        
        if let height = self.heightTextField?.text {
            self.ref.child("users/\(self.selfUID!)/Height").setValue(height)
        }
        
        if self.lookingForArray.isEmpty {
            self.ref.child("users/\(self.selfUID!)/LookingFor").setValue("")
        } else {
            var lookingForString = ""
            for i in self.lookingForArray {
                
                if lookingForString == "" {
                    lookingForString = lookingForString + "\(i)"
                    print("lookingForString is", lookingForString)
                } else {
                    print("lookingForString is", lookingForString)
                    lookingForString = lookingForString + ", \(i)"
                }
                
            }
            self.ref.child("users/\(self.selfUID!)/LookingFor").setValue(lookingForString)
        }
    }
    
    //two types of alerts: (1) Public to private -> deletes previous requests | (2) Private to public -> Unlocks previous requests
    var makePrivate_Alert: UIAlertController?
    var makePrivateAlert_Seen = "false" { didSet {print("makePrivateAlert_Seen didSet,", makePrivateAlert_Seen) }}
    
    
    //store user name on local drive
//    UserDefaults.standard.set(self.userFirstName!, forKey: "nameVault")
//    //retrieve user name from local drive
//    if let clearName = UserDefaults.standard.string(forKey: "nameVault") {
//        print("clearName from vault is,", clearName)
//    }
//
    
    func addAlert_makePrivate() {
        
//        let alertMessage = "Making your profile private will delete any previous profile unlock requests you made to others."
//        let alertTitle = "Private Visibility"
        
        let alertTitle = "Private Visibility"
        let alertMessage = "You are about to make your profile Private. This will clear any profile unlock requests you previously made to others."
        
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
//        "Changing your visibility preferences will delete any previous profile unlock requests you made to others."
        
        let cancelChanges = UIAlertAction(title: "Cancel Changes", style: .destructive) { (action) in
            // Respond to user selection of the action.
            //exit to home without saving profile changes
            self.makePrivate_Alert!.dismiss(animated: true, completion: nil)
            self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
        }
        
        let keepChanges = UIAlertAction(title: "Keep Changes", style: .default) { (action) in
            // Respond to user selection of the action.
            //go through regular update process
            self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
            self.setDisplayProfilePhotoURL()
            self.setUserVisibilityPreference()
            self.deleteOutgoingSwapRequestsOnReveal()
        }
    
        alert.addAction(cancelChanges)
        alert.addAction(keepChanges)

        self.makePrivate_Alert = alert
    }
    
    var makePublic_Alert: UIAlertController?
    var makePublicAlert_Seen = "false" { didSet {print("makePublic_Seen didSet,", makePublicAlert_Seen) }}
    
    func addAlert_makePublic() {
//        let alertMessage = "Making your profile public will automatically unlock your pfoile to any previous requests."
//        let alertMessage = "Making your profile public will automatically approve any previous requests made by others to you."
        
        let alertTitle = "Public Visibility"
        let alertMessage = "You are about to make your profile Public. This will automatically approve any unlock requests you previously received from others."
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        //        "Changing your visibility preferences will delete any previous profile unlock requests you made to others."
        
        let cancelChanges = UIAlertAction(title: "Cancel Changes", style: .destructive) { (action) in
            // Respond to user selection of the action.
            //exit to home without saving profile changes
            self.makePublic_Alert!.dismiss(animated: true, completion: nil)
            self.performSegue(withIdentifier: "unwindEditorToHome", sender: self)
        }
        
        let keepChanges = UIAlertAction(title: "Keep Changes", style: .default) { (action) in
            // Respond to user selection of the action.
            //go through regular update process
            self.startAnimatingActivityIdicator_PhotoAwaitingUpload()
            self.setDisplayProfilePhotoURL()
            self.setUserVisibilityPreference()
        }
        
        alert.addAction(cancelChanges)
        alert.addAction(keepChanges)
        
        self.makePublic_Alert = alert
    }
    
    
    
    //    @IBAction func agreeToTerms() {
    //        // Create the action buttons for the alert.
    //        let defaultAction = UIAlertAction(title: "Agree",
    //                                          style: .default) { (action) in
    //                                            // Respond to user selection of the action.
    //        }
    //        let cancelAction = UIAlertAction(title: "Disagree",
    //                                         style: .cancel) { (action) in
    //                                            // Respond to user selection of the action.
    //        }
    //
    //        // Create and configure the alert controller.
    //        let alert = UIAlertController(title: "Terms and Conditions”,
    //            message: "Click Agree to accept the terms and conditions.”,
    //            preferredStyle: .alert)
    //        alert.addAction(defaultAction)
    //        alert.addAction(cancelAction)
    //
    //        self.present(alert, animated: true) {
    //            // The alert was presented
    //        }
    //    }
    //
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    */
    
   
}

