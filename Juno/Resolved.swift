//
//  Resolved.swift
//  Ximo
//
//  Created by Asaad on 7/22/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import Foundation






/*
//Up Next:
5. - QA testing
3. - calling Cloud fx on vals
4. - firebase node verficiation type
6. - Final UI retouching

 
 //when user deletes app:
 - if app doesnt find photoVault - means deleted, so then erase respective requests? -> 1. use default url 2. remove swap request 3. remove from unlockedNode


*/
//REDUCED LIST:
/*

 
 
 Code Logic:
 
 24. Load Full Images of Cells in View to Cache to access quickly if user taps cell

 BE & Server:
 11. Gonna need to handle this last - do everything with the UI && code logic first. Finishing all features. THEN see if you have enough time to offload necessary path writes to Google Cloud Functions. i.e.:
    a. implement: ".write" : "$user_id === auth.id" at /app/users/node -> this will secure userData by only allowing currentUser == auth.user to write to data at said node
    b. write cloud functions to offset all data mirroring methods to server ["chatID", "swapID", "blockID",
    "photoURLUpdates", "scarletMessenger" ...]
    c. You need to be sure that people can't just "erase" all user data at specified node by implementing proper security rules
    d. Make sure account deletion can only be called with appropriate securty rules
 10. WRITE THOSE MOTHERFUCKING SECURITY RULES
 11. Write data verification rules (string length, data type, geoFire && UID...)
 
 UI:
 -- editorphotoFail 
 9.  Add multiple photo indicator for ProfileVC
 11. Shade is too strong behind the green status symb
 12. Here for buttons need to be streamlined - something is off
 13. polish the profile toggler somehow
 14. MORE Actions needs to be embossed somehow - feels too light for the rest of the UI- bolden darken enlarge do something
 16. revers the settings and edit profile positionsn
 19. Online button should align to write of title (or left... but should be consistent with collectionView orientation of presence symb)
 20. placeholder for chatlist cells
 21. GridVC didNot show blueNotification when otherApproves outgoing 2Way swap
 22. Profile View (go back to main photo should always bring up banner. i.e. banner MUST always be visible on main image view)

 UX:
 16. What happens when user forgets password?
 17. Add dismissor to About Me Text View in Editor Controller
 18. Let user "Don't show me again" for alerts in Editor Controller
 19. Back Button highlights to all clickable elements (where appropriate)
 20. Block the Profile Editor Done Button *while EXTRA PHOTOS ARE UPLOADING* then RELEASE WHEN COMPLETE
 22. Make FB alternative work, or don't include at all
 // handle all alerts
 -- make editor background button to dismiss all active
 
 SafetyChecks:
 -- make sure NSNotification.observers are called appropriately in VDD && NOT IN VWA (so they don't get called again everytime same VC is accessed)
 */

///////////////

/*
 New Features:
 1. Let users hide name with hidden profile
 2. Name first Initial + optional second initial be a bigger success than blur feature)
 
 Nice to Have:
 1. Add "Show My Profile to Ages" feature
 2. Auto-gallery mode (cycles through photos automatically)
 3. PAID $$$ FEATURES
 4. Add XimoBlue highlight to buttons when pressed
 6. Inset chatBubble bottom by bottomSafeMargin for iPhoneX
 7. FACEBOOK What Info we use list
 18. PAID$$: GridThumbnailReelsYourPhotos (Like Harry Potter Magazine)
 19. PAID$$: Allow users to make their photo hidden again - even if they just want to continue a conversation
 
 DATA SYNC ARCHITECTURE: (THIS IS SUPER FUCKING IMPORTANT)
 1. MAKE SURE YOU're not firing observers randomly.
 i.e. observe that all DataLoaders() [observeSwaps, observeThreads...] Fire only once in viewDidLoad (ONLY AS MANY TIMES AS NEEDED)
 2. Make sure all Syncers() fire appropriately in VWA or as needed (and ONLY AS MANY TIMES AS NEEDED)
 i.e. You're swapObserver Loader() is calling more than once across the same lifecycle . THIS SHOULDNT HAPPEN - OTHERWISE YOURE MAIN THREAD WILL BE FUCKED
 */

 //
 //ATTN: Reverse all block/dismiss logic to ->
 //1. Declare observers before observingPopulateArrDicts
 //2. Build Array of UID's of blocked/dismissed
 //3. of array.contains(uid) -> don't append item to list
 //4. this will save you trouble of deleting indexes & is a safer approach AS LONG AS array populates fully before populateArrDict is called
 //5. Your current approach of reading for ids in unsafe since its clashing with indexes in CollectionView

//Pre-release:
/*
 - export ~100 profiles
 
 */

 //V2:
 /*
 26. Update User Dict & CVC on keys exited
 -- CVC not detecting last message by UID for outgoingOneWayShow -> not appending to ChatList
 -- handle offline state
 -- sending a notification message to a user
 -- sending notifications for messages configuration
 -- look into FB offline persistence
 -- purge chatThreadID's that are == false if unused
 */
 



