//
//  UserModelController.swift
//  Ximo
//
//  Created by Asaad on 7/18/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import MapKit
import Firebase
import CoreLocation
import FirebaseAuth

/*
class UserModelController {
    
    var myProfileInfoDict = [String : String]()
    var userDictArray = [[String : String]]()
    var userDictArray_UID = [String]()
    var distanceToByUID = [String: String]()
    var minimumUserCountThreshold = 0
    
    var ref: DatabaseReference!

    //1. declaring the object model
    struct User {
        var userDisplayName: String?
        var userThumbnailURLString: String?
        var gridThumb_URL: String?
        var visus: String?
        var userAge: String?
        
        var userMinAge: String?
        var userMaxAge: String?
        
        var school: String?
        var work: String?
        var aboutMe: String?
        var height: String?
        var lookingFor: String?
        
        var userPrivacyPreference: String?
        var blurState: String?
        var isOnline: String?
        
        var full_URL_2: String?
        var full_URL_3: String?
        var full_URL_4: String?
        
        var distanceTo: String?
        var UID: String?
    }
    
    //2. declaring struct instance
    var user = User()

    //3. struct initializer method
    
    var completeInfo = true

    func userDictArray_ParserMethod(snapshot: DataSnapshot, center: CLLocation, othLocation: CLLocation)  {
        
        self.setSelfAgeFilterValues()
        
        guard let value = snapshot.value as? [String: Any] else {
            //escape method if geoFire key is not mirrored in UID
            //also you might want to write a validation rule that enforces that geoFire key is written with mirror in uID atomically
            print("error for snapshot,", snapshot.key)
            print("ESCAPE AT 0")
            self.cleanDB(withUID: snapshot.key)
            self.decrementMinimumThreshold()
            return
        }
        
        let userUID = snapshot.key
        var userGridThumbInfoDict = [String: String]()
        
        if !self.userDictArray_UID.contains(userUID) {
            //New instance -> Proceed
            //
            
        } else {
            //Existing instance -> send to modifier method
            self.userDictArray_UpdaterMethod(snapshot: snapshot)
            
            return
        }
        
        //userName
        if let userDisplayName = value["userDisplayName"] as? String {
            if userDisplayName != "DisplayName" {
                userGridThumbInfoDict["userDisplayName"] = userDisplayName
                
                self.completeInfo = true
            } else {
                self.cleanDB(withUID: userUID)
                self.completeInfo = false
            }
        } else {
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userFullURL
        if let userThumbnailURLString = value["full_URL"] as? String {
            userGridThumbInfoDict["userThumbnailURLString"] = userThumbnailURLString
            print("populateArrayofDictionaries userThumbnailURLString", userThumbnailURLString)
        } else {
            print("ESCAPE AT 1")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userGridURL
        if let gridThumbURL = value["gridThumb_URL"] as? String {
            print("CHECK gridThumbURL")
            userGridThumbInfoDict["gridThumb_URL"] = gridThumbURL
        } else {
            print("FAIL gridThumbURL")
            //                    print("ESCAPE AT 2")
            //                    self.cleanDB(withUID: userUID)
            //                    self.completeInfo = false
        }
        
        //userVisus
        if let visus = value["visus"] as? String {
            userGridThumbInfoDict["visus"] = visus
        } else {
            print("ESCAPE AT 3")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userAge
        var userAge = ""
        if let uAge = value["userAge"] as? String {
            userAge = uAge
            userGridThumbInfoDict["userAge"] = userAge
        } else {
            print("ESCAPE AT 4")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userMinAge Filter
        var userMinAge = ""
        if let uMinAge = value["showMeMinAge"] as? String {
            userMinAge = uMinAge
            userGridThumbInfoDict["userMinAge"] = userMinAge
        } else {
            print("ESCAPE AT 5")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //userMaxAge Filter
        var userMaxAge = ""
        if let uMaxAge = value["showMeMaxAge"] as? String {
            userMaxAge = uMaxAge
            userGridThumbInfoDict["userMaxAge"] = userMaxAge
        } else {
            print("ESCAPE AT 6")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //School
        if let school = value["School"] as? String {
            userGridThumbInfoDict["school"] = school
        } else {
            print("ESCAPE AT 7")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //Work
        if let work = value["Work"] as? String {
            userGridThumbInfoDict["work"] = work
        } else {
            print("ESCAPE AT 8")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //AboutMe
        if let aboutMe = value["AboutMe"] as? String {
            userGridThumbInfoDict["aboutMe"] = aboutMe
        } else {
            print("ESCAPE AT 9")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //Height
        if let height = value["Height"] as? String {
            userGridThumbInfoDict["height"] = height
        } else {
            print("ESCAPE AT 10")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //HereFor
        if let lookingFor = value["LookingFor"] as? String {
            userGridThumbInfoDict["lookingFor"] = lookingFor
        } else {
            print("ESCAPE AT 11")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //Privacy
        if let userPrivacyPreference = value["privacy"] as? String {
            userGridThumbInfoDict["userPrivacyPreference"] = userPrivacyPreference
        } else {
            print("ESCAPE AT 12")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //BlurDegree
        if let blurState = value["blurState"] as? String {
            userGridThumbInfoDict["blurState"] = blurState
        } else {
            print("ESCAPE AT 13")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //Presence
        if let isOnline = value["isOnline"] as? String {
            userGridThumbInfoDict["isOnline"] = isOnline
        } else {
            print("ESCAPE AT 14")
            self.cleanDB(withUID: userUID)
            self.completeInfo = false
        }
        
        //additional photo URLs
        if let photoURL2 = value["full_URL_2"] as? String {
            userGridThumbInfoDict["full_URL_2"] = photoURL2
            print("GET userGridThumbInfoDict photoURL2", photoURL2)
        }
        
        if let photoURL3 = value["full_URL_3"] as? String {
            userGridThumbInfoDict["full_URL_3"] = photoURL3
            print("GET userGridThumbInfoDict photoURL3", photoURL3)
        }
        
        if let photoURL4 = value["full_URL_4"] as? String {
            userGridThumbInfoDict["full_URL_4"] = photoURL4
            print("GET userGridThumbInfoDict photoURL4", photoURL4)
        }
        
        //Create key-value pairs for each user
        userGridThumbInfoDict["UID"] = userUID
        //
        
        //DistanceTo
        let distanceToUser = self.getLocationTo(uid: userUID, center: center, othLocation: othLocation)
        userGridThumbInfoDict["distanceTo"] = distanceToUser
        
        let selfUID = Auth.auth().currentUser!.uid
        
        //self props
        let showMeMinAge = self.showMeMinAgeInt!
        let showMeMaxAge = self.showMeMaxAgeInt!
        let myAgeInt = self.myAge
        
        //oth props
        let userAgeInt = (userAge as NSString).integerValue
        var userMinAgeFilter = 0
        var userMaxAgeFilter = 100
        
        if userMinAge != "" {
            userMinAgeFilter = (userMinAge as NSString).integerValue
        }
        
        if userMaxAge != "" {
            userMaxAgeFilter = (userMaxAge as NSString).integerValue
        }
        
        //
        if self.completeInfo == true {
            print("completeInfo CHECK")
            if userAgeInt < showMeMinAge || userAgeInt > showMeMaxAge || myAgeInt > userMaxAgeFilter || myAgeInt < userMinAgeFilter {
                //remove user from required GFarray
                self.decrementMinimumThreshold()
                print("userAgeInt,", userAgeInt, "not in bounds will NOT APPEND USER")
            } else {
                print("noMinAge detected, will default to appending user object")
                print("")
                self.userDictArray_UID.append(userUID)
                self.userDictArray.append(userGridThumbInfoDict)
            }
            
        } else if self.completeInfo == false {
            print("will decrement threshold")
            self.decrementMinimumThreshold()
        }
        
        //2. filter out users who's age range I am outside of myself
        print("ACCIO self.userDictArray.count", self.userDictArray.count)
        print("ACCIO self.self.minimumUserCountThreshold", self.minimumUserCountThreshold)
        //
        
        
    }
    
    //Helper Methods
    //1. Age Setter
    var showMeMinAgeInt: Int? { didSet {print("CVC didSet showMeMinAgeInt", showMeMinAgeInt!) } }
    var showMeMaxAgeInt: Int? { didSet {print("CVC didSet showMeMaxAgeInt", showMeMaxAgeInt!) } }
    var myAge = 0 { didSet {print("CVC didSet myAge", myAge) } }
    
    func setSelfAgeFilterValues() {
        let selfUID = Auth.auth().currentUser!.uid
        
        //initialize my minimum age filter value
        if let minAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMinAge") {
            print("setAgeFilterValues _showMeMinAge is,", minAge)
            if minAge != "" {
                self.showMeMinAgeInt = (minAge as NSString).integerValue
            } else {
                //
                print("setAgeFilterValues willSET empty")
                self.showMeMinAgeInt = 18
            }
        } else {
            self.showMeMinAgeInt = 18
        }
        
        //initialize my maximum age filter value
        if let maxAge = UserDefaults.standard.string(forKey: "\(selfUID)_showMeMaxAge") {
            print("setAgeFilterValues _showMeMaxAge is,", maxAge)
            if maxAge != "" {
                self.showMeMaxAgeInt = (maxAge as NSString).integerValue
            } else {
                //
                print("setAgeFilterValues willSET empty")
                self.showMeMaxAgeInt = 100
            }
        } else {
            self.showMeMaxAgeInt = 100
        }
        
        //Initialize my age int
        if let myAgeDefault = UserDefaults.standard.string(forKey: "\(selfUID)_myAgeString") {
            let ageInt = (myAgeDefault as NSString).integerValue
            self.myAge = ageInt
            print("CVC didSet myAge from defaults", myAgeDefault)
        } else {
            if let ageString = self.myProfileInfoDict["userAge"] {
                let ageInt = (ageString as NSString).integerValue
                self.myAge = ageInt
                print("CVC didSet myAge from infoDict", ageString)
            }
        }
    }
    
    //2. Location Calculator
    func getLocationTo(uid: String, center: CLLocation, othLocation: CLLocation ) -> String {
        let locationAcross = center.distance(from: othLocation)
        
        let distanceToInMeters = locationAcross
        let distanceToInKM = distanceToInMeters / 1000
        let distanceToInMi = distanceToInKM / 1.609344
        
        let formatter = MKDistanceFormatter()
        formatter.units = .imperial
        formatter.unitStyle = .abbreviated
        
        let formattedDistanceString = formatter.string(fromDistance: locationAcross)
        print("formattedDistanceString is,", formattedDistanceString)
        
        return formattedDistanceString
    }
    
    //3. DB Cleaner
    func cleanDB(withUID: String) {
        print("populateArrayofDictionaries willcleanDB withUID,", withUID)
        //we don't need to delete the instance from
        self.ref.child("userLocationGF/\(withUID)").removeValue()
        self.ref.child("users/\(withUID)").removeValue()
    }
    
    //
    func decrementMinimumThreshold() {
        self.minimumUserCountThreshold = self.minimumUserCountThreshold - 1
    }
    
    //4. struct updater method
    func userDictArray_UpdaterMethod(snapshot: DataSnapshot) {
        
    }
    
}
*/
