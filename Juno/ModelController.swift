//
//  ModelController.swift
//  Juno
//
//  Created by Asaad on 3/16/18.
//  Copyright © 2018 Animata Inc. All rights reserved.
//

import Firebase

class ModelController {
     
    var refPoint = "ModelController"
    
    //1. declaring the struct
    struct MyUserInfo {
        var selfUID: String?
        var selfDisplayName: String?
        var selfVisus: String? //always inherited from GridVC at source
        var selfDefaultPhotoURL: String?
        var selfPrivacy: String?
    }
    
    //2. declaring an instance of the struct
    var myUserInfo = MyUserInfo()
    
    //Method Call A
    func populateSelfProfileInfo(snapshot: DataSnapshot) {
        
        guard let value = snapshot.value as? [String: Any] else { return }
        
        let selfUID = snapshot.key
        guard let userDisplayName = value["userDisplayName"] as? String else { return }
        guard let userThumbnailURLString = value["full_URL"] as? String else { return }
        guard let userPrivacyPreference = value["privacy"] as? String else { return }
        guard let visus = value["visus"] as? String else { return }
        //guard let userAge = value["userAge"] as? String else { return }
        
        myUserInfo = MyUserInfo(selfUID: selfUID, selfDisplayName: userDisplayName, selfVisus: visus, selfDefaultPhotoURL: userThumbnailURLString, selfPrivacy: userPrivacyPreference)
    }
    
    //Method Call B
    func updateSelfProfileInfo(snapshot: DataSnapshot) {
        
        let propertyKey = snapshot.key
        
        guard let propertyValue = snapshot.value! as? String else { return }
        
        if propertyKey == "userDisplayName" {
            myUserInfo.selfDisplayName = propertyValue
        }
    }
    
    //1. Declare struct
    struct GridProperties {
        var gridDidLoad: Bool?
    }
    
    //2. create instance of struct
    var myGridProperties = GridProperties()
    
    //Method Call
    func initGridProperties(withBool: Bool) {
        myGridProperties = GridProperties(gridDidLoad: withBool)
    }
}





